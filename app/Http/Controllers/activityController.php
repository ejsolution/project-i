<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\model\activityLogs;
use App\model\useraccount;
use Auth;
class activityController extends Controller
{
    public static function insertActivity($thisactivity){
      $activity = new activityLogs;
      $user = useraccount::find(Auth::guard('myuser')->id());
      $activity->activity = $thisactivity;
      $activity->name = $user->firstName.' '.$user->lastName;
      //$activity->name = Auth::guard('myuser')->id();
      $activity->save();
    }
}
