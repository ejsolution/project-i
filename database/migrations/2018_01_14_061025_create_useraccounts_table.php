<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUseraccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('useraccounts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('userName')->unique();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('verify_token')->nullable();
            $table->rememberToken();
            $table->boolean('verified')->default(0);
            $table->string('firstName');
            $table->string('lastName');
            $table->string('userType');
            $table->string('designation');
            $table->string('designationLevel');
            $table->tinyInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('useraccounts');
    }
}
