@component('components.modal')
    @slot('modalid')
    chooseCounselorModal
    @endslot
    @slot('modalsize')
    modal-lg
    @endslot
    @slot('modaltitle')
    Counselors
    @endslot
    @slot('modalcontent')
   
    <table class='table table-striped'>
        <thead>
            <tr>
                <th>Name</th>
                <th>Counselor Type</th>
                <th>Designation</th>
                <th>Select</th>
            </tr>
        </thead>
        <tbody id ='counselorTableForCounseling'>
        </tbody>
    </table>
    @endslot
    @slot('modalbutton')
   
    @endslot
@endcomponent