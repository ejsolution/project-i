var totalRow;
var totalPages;
var visiblePages;
var mypage;
$(document).ready(function(){
    tableLoader('3', 'activityLogsTable');
    $('#searchActivityLog').on('keydown', function(){
        $('#pagination').twbsPagination('destroy');
    
        $.get('/activitylogs/totalrows/'+$('#searchActivityLog').val(), function(data){
            initializePages(data);
            $('#pagination').twbsPagination({
                totalPages: totalPages,
                visiblePages: visiblePages,
                onPageClick: function(event, page){
                    mypage = page;
                    tableLoader('3', 'activityLogsTable');
                    getAjax('/activitylogs/pages/'+page+'/'+$('#searchActivityLog').val(), 'activityLogsTable');
                }
            });
        });
    });  
}); 
$.get('/activitylogs/totalrows/'+$('#searchActivityLog').val(), function(data){
    initializePages(data);
    $('#pagination').twbsPagination({
        totalPages: totalPages,
        visiblePages: visiblePages,
        onPageClick: function(event, page){
            mypage = page;
            tableLoader('3', 'activityLogsTable');
            getAjax('/activitylogs/pages/'+page+'/'+$('#searchActivityLog').val(), 'activityLogsTable');
        }
    });
});
function initializePages(data){
    totalRow = data;
    if(totalRow<=5){
        totalPages = 1;
    }else{
        var remainder = totalRow%10;
        if(remainder >0){
            totalPages = ((totalRow-remainder)/10)+1;
        }else{
            totalPages = totalRow/10;
        }
        if(totalPages<=5){
            visiblePages =totalPages;
        }else{
            visiblePages = 5;
        }    
    }
}