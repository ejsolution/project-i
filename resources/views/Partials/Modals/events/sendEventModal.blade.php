@component('components.modal')
  @slot('modalid')
    sendEventModal
  @endslot
  @slot('modalsize')
    modal-md
  @endslot
  @slot('modaltitle')
    Add Event
  @endslot
  @slot('modalcontent')
    <div class='row'>
      <div class='col-md-2'>To:</div>
      <div class='col-md-10'>
        <input type = 'text' disabled class='form-control' id = 'toReceiverEventText' name = 'toReceiverEventText'>
      </div>
    </div>
    <div class='row genericDivTop'>
      <div class='col-md-12 alignRight'>
        <button class='btn btn-dark'>All</button><button class='btn btn-dark'>Filter</button>
      </div>
    </div>
    <div class='row'>
      <div class='col-md-12'>
        <label>Message</label>
      </div>
    </div>
    <div class='row'>
      <div class='col-md-12 well'>
        <div class='row'>
          <div class='col-md-2'>
            <label for = 'titleMessageTextEvent'>Title:</label>
          </div>
          <div class='col-md-10'>
            <input type = 'text' disabled class='form-control' id = 'titleMessageTextEvent' name = 'titleMessageTextEvent' disabled>
          </div>
        </div>
        <div class='row genericDivTop'>
          <div class='col-md-2'>
            <label for = 'whenEventText'>When:</label>
          </div>
          <div class='col-md-10'>
            <input type = 'text' disabled class='form-control' id = 'whenEventText' name = 'whenEventText' disabled>
          </div>
        </div>
        <div class='row genericDivTop'>
          <div class='col-md-2'>
            <label for = 'whereEventText'>Where:</label>
          </div>
          <div class='col-md-10'>
            <input type = 'text' disabled class='form-control' id = 'whereEventText' name = 'whereEventText' disabled>
          </div>
        </div>
        <div class='row'>
          <div class='col-md-12'>
            <label for = 'messageTextAreaEvent'>Message:</label>
            <textarea disabled id = 'messageTextAreaEvent' name = 'messageTextAreaEvent' class='form-control'></textarea>
          </div>
        </div>
      </div>
    </div>
  @endslot
  @slot('modalbutton')
      <button type="button" class="btn btn-danger" data-dismiss="modal"><i class='fa fa-forward'></i>&nbsp;Send</button>
  @endslot
@endcomponent
