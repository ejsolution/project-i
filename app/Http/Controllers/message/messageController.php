<?php

namespace App\Http\Controllers\message;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\student\studentController;
use App\Http\Controllers\activityController;
use App\Http\Controllers\notification\notificationController;
use App\model\schoolYear;
use App\model\message;
use App\model\student;
use App\model\family;
use Auth;
class messageController extends Controller
{
    public function __construct(){
        $this->middleware('auth:myuser');
    }
    public function getMessagePage(){
        $schoolyear = schoolYear::all();
        $notif = (new notificationController)->getNotifs();
        return view('admin/messages/messages', ['schoolYear' => $schoolyear,
                                                'notif'=>$notif]);
    }
    public function getSchoolYearType($schoolyearid){
        $schoolyear = schoolYear::find($schoolyearid);
        return $schoolyear->semesterType;
    }
    public function messageList($viewby, $search ='', $semester = ''){
        if($viewby =='all'){
            $message = message::where('studentID', 'LIKE', "%$search%")
                            ->orWhere('created_at', 'LIKE', "%$search%")
                            ->get();
            if(count($message)==0){
                return "<h2>There are no messages yet!</h2>";
            }else{
                $html = '';
                foreach ($message as $messages) {
                    $html .= $this->getMessageRow($messages);
                }
                return $html;
            }
        }else{
            
            $message = message::where([
                                        ['schoolYear', '=', $viewby],
                                        ['semesterID', '=', $semester],
                                    ])
                            ->orWhere('studentID', 'LIKE', "%$search%")
                            ->orWhere('created_at', 'LIKE', "%$search%")
                            ->get();
            error_log(message::where([
                                    ['schoolYear', '=', $viewby],
                                    ['semesterID', '=', $semester],
                                        ])
                                    ->orWhere('studentID', 'LIKE', "%$search%")
                                    ->orWhere('created_at', 'LIKE', "%$search%")->toSql());
            if(count($message)==0){
                return "<h2>There are no messages yet!</h2>";
            }else{
                $html = '';
                foreach ($message as $messages) {
                    $html .= $this->getMessageRow($messages);
                }
                return $html;
            }
        }
    }
    public function messageListWithNoSearch($viewby, $semester=''){
        if($viewby =='all'){
            $message = message::all();
            if(count($message)==0){
                return "<h2>There are no messages yet!</h2>";
            }else{
                $html = '';
                foreach ($message as $messages) {
                    $html .= $this->getMessageRow($messages);
                }
                return $html;
            }
        }else{
            
            $message = message::where([
                                        ['schoolYear', '=', $viewby],
                                        ['semesterID', '=', $semester],
                                    ])
                            ->get();
            /*error_log(message::where([
                                    ['schoolYear', '=', $viewby],
                                    ['semesterID', '=', $semester],
                                        ])->toSql());*/
            if(count($message)==0){
                return "<h2>There are no messages yet!</h2>";
            }else{
                $html = '';
                foreach ($message as $messages) {
                    $html .= $this->getMessageRow($messages);
                }
                return $html;
            }
        }
    }
    public function getMessageRow($message){
        $time = strtotime($message->created_at);
        $timeSent = date("m/d/y g:i A", $time);
        return "<tr>
                    <td>$timeSent</td>
                    <td>$message->studentID</td>
                    <td>$message->details</td>
                </tr>";
    }
    public function studentRecipientList($viewby, $search = ''){
        $mystudent =  new studentController;
        switch ($viewby) {
            case 'all':
                $student  =student::where('firstName', 'LIKE', "%$search%")
                                    ->orWhere('lastName', 'LIKE', "%$search%")
                                    ->orWhere('contactNo', 'LIKE', "%$search%")
                                    ->get();
                if(count($student)==0){
                    return "<h2>There are no students to be selected</h2>";
                }
                $html ='';
                
                foreach ($student as $students) {
                    $html .= $this->getStudentRecipientRow($students);
                }
                return $html;
            break;
            case 'college':
                $student  =student::where('firstName', 'LIKE', "%$search%")
                                    ->orWhere('lastName', 'LIKE', "%$search%")
                                    ->orWhere('contactNo', 'LIKE', "%$search%")
                                    ->get();
                if(count($student)==0){
                    return "<h2>There are no students to be selected</h2>";
                }
                $html ='';
                
                foreach ($student as $students) {
                    if($mystudent->getStudentType($students->id) != 'college'){
                        continue;
                    }
                    $html .= $this->getStudentRecipientRow($students);
                }
                return $html;
            break;
            case 'seniorhigh':
                $student  =student::where('firstName', 'LIKE', "%$search%")
                                    ->orWhere('lastName', 'LIKE', "%$search%")
                                    ->orWhere('contactNo', 'LIKE', "%$search%")
                                    ->get();
                if(count($student)==0){
                    return "<h2>There are no students to be selected</h2>";
                }
                $html ='';
                
                foreach ($student as $students) {
                    if($mystudent->getStudentType($students->id) != 'seniorhigh'){
                        continue;
                    }
                    $html .= $this->getStudentRecipientRow($students);
                }
                return $html;
            break;
            case 'juniorhigh':
                $student  =student::where('firstName', 'LIKE', "%$search%")
                                    ->orWhere('lastName', 'LIKE', "%$search%")
                                    ->orWhere('contactNo', 'LIKE', "%$search%")
                                    ->get();
                if(count($student)==0){
                    return "<h2>There are no students to be selected</h2>";
                }
                $html ='';
                
                foreach ($student as $students) {
                    if($mystudent->getStudentType($students->id) != 'juniorhigh'){
                        continue;
                    }
                    $html .= $this->getStudentRecipientRow($students);
                }
                return $html;
            break;
            case 'elementary':
                $student  =student::where('firstName', 'LIKE', "%$search%")
                                    ->orWhere('lastName', 'LIKE', "%$search%")
                                    ->orWhere('contactNo', 'LIKE', "%$search%")
                                    ->get();
                if(count($student)==0){
                    return "<h2>There are no students to be selected</h2>";
                }
                $html ='';
                
                foreach ($student as $students) {
                    if($mystudent->getStudentType($students->id) != 'elementary'){
                        continue;
                    }
                    $html .= $this->getStudentRecipientRow($students);
                }
                return $html;
            break;
            default:
                
            break;
        }
        
    }
    public function getStudentRecipientRow($students){
        return "<tr id ='studentSelect-$students->id'>
                    <td>$students->firstName $students->lastName</td>
                    <td>$students->contactNo</td>
                    <td>
                        <button class='btn btn-dark selectStudent' id ='selectStudent-$students->id'>
                            <i class='fa fa-paw'></i>
                        </button>
                    </td>
                </tr>";
    }
    public function getStudentFamilyList($viewby, $search =''){
        $mystudent =  new studentController;
        $family = family::join('students', 'students.id', '=', 'families.studentID')
                        ->select('students.firstName', 'students.lastName', 
                        'families.studentID', 'families.name', 'families.contactNo',
                        'families.relationship','families.id')
                        ->where('students.firstName', 'LIKE', "%$search%")
                        ->orWhere('students.lastName', 'LIKE', "%$search%")
                        ->orWhere('families.name', 'LIKE', "%$search%")
                        ->orWhere('families.relationship', 'LIKE', "%$search%")
                        ->get();
        switch($viewby){
            case 'all':
                if(count($family)==0){
                    return "<h2>There are no students with guardians yet</h2>";
                }
                $html = '';
                foreach ($family as $families) {
                    $html .= $this->getStudentFamilyRow($families);
                }
                return $html;
            break;
            case 'college':
                $html = '';
                foreach ($family as $families) {
                    if($mystudent->getStudentType($families->studentID) != 'college'){
                        continue;
                    }
                    $html .= $this->getStudentFamilyRow($families);
                }
                if($html ==''){
                    return "<h2>There are no college with family contacts</h2>";
                }
                return $html;
            break;
            case 'seniorhigh':
                $html = '';
                foreach ($family as $families) {
                    if($mystudent->getStudentType($families->studentID) != 'seniorhigh'){
                        continue;
                    }
                    $html .= $this->getStudentFamilyRow($families);
                }
                if($html ==''){
                    return "<h2>There are no seniorhigh with family contacts</h2>";
                }
                return $html;
            break;
            case 'juniorhigh':
                $html = '';
                foreach ($family as $families) {
                    if($mystudent->getStudentType($families->studentID) != 'juniorhigh'){
                        continue;
                    }
                    $html .= $this->getStudentFamilyRow($families);
                }
                if($html ==''){
                    return "<h2>There are no juniorhigh with family contacts</h2>";
                }
                return $html;
            break;
            case 'elementary':
                $html = '';
                foreach ($family as $families) {
                    if($mystudent->getStudentType($families->studentID) != 'elementary'){
                        continue;
                    }
                    $html .= $this->getStudentFamilyRow($families);
                }
                if($html ==''){
                    return "<h2>There are no elementary with family contacts</h2>";
                }
                return $html;
            break;
        }
    }
    public function getStudentFamilyRow($family){
        
        return "<tr id ='selectGuardianRecipient-$family->id'>
                    <td>$family->firstName $family->lastName</td>
                    <td>$family->name</td>
                    <td>$family->relationship</td>
                    <td>$family->contactNo</td>
                    <td>
                        <button class='btn btn-dark selectGuardian' id ='selectGuardian-$family->id'>
                            <i class='fa fa-paw'></i>
                        </button>
                    </td>
                </tr>";
    }
    public function sendMessageIndividual(Request $request){
        $this->validate($request, array(
            'messageNewMessageModalText'=>'required|max:100'
        ));
        if($request->sendTo=='student'){
            $student = student::find($request->recipient);
            $ch = curl_init();
			$itexmo = array('1' => $student->contactNo, '2' => $request->messageNewMessageModalText, '3' => 'TR-EPHRA940314_T1W6X');
			curl_setopt($ch, CURLOPT_URL,"https://www.itexmo.com/php_api/api.php");
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, 
			          http_build_query($itexmo));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec ($ch);
            error_log($response);
            curl_close ($ch);
            if($response==0){
                $message = new message;
                $message->studentID = $student->firstName.' '.$student->lastName;
                $message->userID = Auth::guard('myuser')->id();
                $message->from = session()->get('user')['userName'];
                $message->details = $request->messageNewMessageModalText;
                $message->schoolYear=(new studentController)->getCurrentSchoolYear();
                $message->semesterID = (new studentController)->getCurrentSem();
                $message->save();
                $message->data = "Message sent to ".$student->firstName." ".$student->lastName;
                $message->html = $this->getMessageRow($message);
                activityController::insertActivity('Sent a message:'.$request->messageNewMessageModalText);
                return $message;
            }else{
                $message = new message;
                $message->data = "Something went wrong in sending message to".$student->firstName." ".$student->lastName;
                return $message;
            }
            
        }else{
            $family = family::find($request->recipient);
            $ch = curl_init();
			$itexmo = array('1' => $family->contactNo, '2' => $request->messageNewMessageModalText, '3' => 'TR-EPHRA940314_T1W6X');
			curl_setopt($ch, CURLOPT_URL,"https://www.itexmo.com/php_api/api.php");
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, 
			          http_build_query($itexmo));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$response = curl_exec ($ch);
            curl_close ($ch);
            error_log($response);
            if($response==0){
                $message = new message;
                $message->studentID = $family->name;
                $message->userID = Auth::guard('myuser')->id();
                $message->from = session()->get('user')['userName'];
                $message->details = $request->messageNewMessageModalText;
                $message->schoolYear=(new studentController)->getCurrentSchoolYear();
                $message->semesterID = (new studentController)->getCurrentSem();
                $message->save();
                $message->data = "Message sent to ".$family->name;
                $message->html = $this->getMessageRow($message);
                activityController::insertActivity('Sent a message:'.$request->messageNewMessageModalText);
                return $message;
            }else{
                $message = new message;
                $message->data = "Something went wrong in sending message to".$family->name;
                return $message;
            }
            
        }
    }
    public function getGroupList($studenttype){
        $mystudent =  new studentController();
        switch ($studenttype) {
            case 'college':
                $student =student::all();
                foreach ($student as $key => $students) {
                    if($mystudent->getStudentType($student[$key]->id)!='college'){
                        $student->forget($key);
                    }
                }
                return $student;
            break;
            case 'seniorhigh':
                $student =student::all();
                foreach ($student as $key => $students) {
                    if($mystudent->getStudentType($student[$key]->id)!='seniorhigh'){
                        $student->forget($key);
                    }
                }
                return $student;
            break;
            case 'juniorhigh':
                $student =student::all();
                foreach ($student as $key => $students) {
                    if($mystudent->getStudentType($student[$key]->id)!='juniorhigh'){
                        $student->forget($key);
                    }
                }
                return $student;
            break;
            case 'elementary':
                $student =student::all();
                foreach ($student as $key => $students) {
                    if($mystudent->getStudentType($student[$key]->id)!='elementary'){
                        $student->forget($key);
                    }
                }
                return $student;
            break;
            default:
                # code...
                break;
        }
    }
    public function sendMessageGroup(Request $request){
        $this->validate($request, array(
            'messageNewGroupMessageModalText'=>'required|max:100'
        ));
        $mystudent =  new studentController();
        $html ='';
        if(isset($request->college)){
            $student =student::all();
            foreach ($student as $key => $students) {
                if($mystudent->getStudentType($student[$key]->id)=='college'){
                    $ch = curl_init();
                    $itexmo = array('1' => $student[$key]->contactNo, '2' => $request->messageNewGroupMessageModalText, '3' => 'TR-EPHRA940314_T1W6X');
                    curl_setopt($ch, CURLOPT_URL,"https://www.itexmo.com/php_api/api.php");
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, 
                            http_build_query($itexmo));
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $response = curl_exec ($ch);
                    curl_close ($ch);
                    $message = new message;
                    $message->studentID = $student[$key]->firstName.' '.$student[$key]->lastName;
                    $message->userID = Auth::guard('myuser')->id();
                    $message->from = session()->get('user')['userName'];
                    $message->details = $request->messageNewGroupMessageModalText;
                    $message->schoolYear=(new studentController)->getCurrentSchoolYear();
                    $message->semesterID = (new studentController)->getCurrentSem();
                    $message->save();
                    $html .= $this->getMessageRow($message);
                    activityController::insertActivity('Sent a message:'.$request->messageNewGroupMessageModalText);
                }
            }
        }
        if(isset($request->seniorhigh)){
            $student =student::all();
            foreach ($student as $key => $students) {
                if($mystudent->getStudentType($student[$key]->id)=='seniorhigh'){
                    $ch = curl_init();
                    $itexmo = array('1' => $student[$key]->contactNo, '2' => $request->messageNewGroupMessageModalText, '3' => 'TR-EPHRA940314_T1W6X');
                    curl_setopt($ch, CURLOPT_URL,"https://www.itexmo.com/php_api/api.php");
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, 
                            http_build_query($itexmo));
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $response = curl_exec ($ch);
                    curl_close ($ch);
                    $message = new message;
                    $message->studentID = $student[$key]->firstName.' '.$student[$key]->lastName;
                    $message->userID = Auth::guard('myuser')->id();
                    $message->from = session()->get('user')['userName'];
                    $message->details = $request->messageNewGroupMessageModalText;
                    $message->schoolYear=(new studentController)->getCurrentSchoolYear();
                    $message->semesterID = (new studentController)->getCurrentSem();
                    $message->save();
                    $html .= $this->getMessageRow($message);
                    activityController::insertActivity('Sent a message:'.$request->messageNewGroupMessageModalText);
                }
            }
        }
        if(isset($request->juniorhigh)){
            $student =student::all();
            foreach ($student as $key => $students) {
                if($mystudent->getStudentType($student[$key]->id)=='juniorhigh'){
                    $ch = curl_init();
                    $itexmo = array('1' => $student[$key]->contactNo, '2' => $request->messageNewGroupMessageModalText, '3' => 'TR-EPHRA940314_T1W6X');
                    curl_setopt($ch, CURLOPT_URL,"https://www.itexmo.com/php_api/api.php");
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, 
                            http_build_query($itexmo));
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $response = curl_exec ($ch);
                    curl_close ($ch);
                    $message = new message;
                    $message->studentID = $student[$key]->firstName.' '.$student[$key]->lastName;
                    $message->userID = Auth::guard('myuser')->id();
                    $message->from = session()->get('user')['userName'];
                    $message->details = $request->messageNewGroupMessageModalText;
                    $message->schoolYear=(new studentController)->getCurrentSchoolYear();
                    $message->semesterID = (new studentController)->getCurrentSem();
                    $message->save();
                    $html .= $this->getMessageRow($message);
                    activityController::insertActivity('Sent a message:'.$request->messageNewGroupMessageModalText);
                }
            }
        }
        if(isset($request->elementary)){
            $student =student::all();
            foreach ($student as $key => $students) {
                if($mystudent->getStudentType($student[$key]->id)=='elementary'){
                    $ch = curl_init();
                    $itexmo = array('1' => $student[$key]->contactNo, '2' => $request->messageNewGroupMessageModalText, '3' => 'TR-EPHRA940314_T1W6X');
                    curl_setopt($ch, CURLOPT_URL,"https://www.itexmo.com/php_api/api.php");
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, 
                            http_build_query($itexmo));
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $response = curl_exec ($ch);
                    curl_close ($ch);
                    $message = new message;
                    $message->studentID = $student[$key]->firstName.' '.$student[$key]->lastName;
                    $message->userID = Auth::guard('myuser')->id();
                    $message->from = session()->get('user')['userName'];
                    $message->details = $request->messageNewGroupMessageModalText;
                    $message->schoolYear=(new studentController)->getCurrentSchoolYear();
                    $message->semesterID = (new studentController)->getCurrentSem();
                    $message->save();
                    $html .= $this->getMessageRow($message);
                    activityController::insertActivity('Sent a message:'.$request->messageNewGroupMessageModalText);
                }
            }
        }
        return $html;
    }
}
