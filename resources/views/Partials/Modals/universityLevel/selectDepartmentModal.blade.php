@component('components.modal')
  @slot('modalid')
    departmentSelectModal
  @endslot
  @slot('modalsize')
    modal-md
  @endslot
  @slot('modaltitle')
    college
  @endslot
  @slot('modalcontent')
    <div class='row'>
      <div class="form-group has-feedback">
          <input type="text" class="form-control"
          placeholder="Search College Code Or College Name" id = 'searchDepartmentsSelect'/>
          <i class="fa fa-search form-control-feedback"></i>
      </div>
    </div>
    {!! Form::open([
            'id'=>'departmentSelectForm']) !!}

    {!! Form::close() !!}
    <div class='row tableModalBigContainer'>
      <table class='table table-striped'>
        <thead>
          <tr>
            <th>Department Code</th>
            <th>Department Name</th>
            <th>Assign</th>
          </tr>
        </thead>
        <tbody id = 'departmentSelectTable'>
        </tbody>
      </table>
    </div>
  @endslot
  @slot('modalbutton')

  @endslot
@endcomponent
