function init_chart_doughnut(){
    $.get('/contract/top5/', function(data){
        //console.log(data);
        if(data!='0'){
            var chart_doughnut_settings = {
                type: 'doughnut',
                tooltipFillColor: "rgba(51, 51, 51, 0.55)",
                data: {
                    labels: [
                        
                    ],
                    datasets: [{
                        data: [],
                        backgroundColor: [
                            
                        ],
                        hoverBackgroundColor: [
                            
                        ]
                    }]
                },
                options: {  
                    legend: false, 
                    responsive: false 
                }
            }
            for (let index = 0; index < data.num; index++) {
                switch (index) {
                    case 0:
                        chart_doughnut_settings.data.labels.push(data.firstC);
                        chart_doughnut_settings.data.datasets[0].data.push(data.firstCount);
                        chart_doughnut_settings.data.datasets[0].backgroundColor.push("red");
                        chart_doughnut_settings.data.datasets[0].hoverBackgroundColor.push("#CFD4D8");
                        $('#counselingTopTable').append("<tr><td>"+data.firstC+"</td><td>"+data.firstCount+"</td></tr>")
                    break;
                    case 1:
                        chart_doughnut_settings.data.labels.push(data.secondC);
                        chart_doughnut_settings.data.datasets[0].data.push(data.secondCount);
                        chart_doughnut_settings.data.datasets[0].backgroundColor.push("green");
                        chart_doughnut_settings.data.datasets[0].hoverBackgroundColor.push("#B370CF");
                        $('#counselingTopTable').append("<tr><td>"+data.secondC+"</td><td>"+data.secondCount+"</td></tr>")
                    break;
                    case 2:
                        chart_doughnut_settings.data.labels.push(data.thirdC);
                        chart_doughnut_settings.data.datasets[0].data.push(data.thirdCount);
                        chart_doughnut_settings.data.datasets[0].backgroundColor.push("yellow");
                        chart_doughnut_settings.data.datasets[0].hoverBackgroundColor.push("#E95E4F");
                        $('#counselingTopTable').append("<tr><td>"+data.thirdC+"</td><td>"+data.thirdCount+"</td></tr>")
                    break;
                    case 3:
                        chart_doughnut_settings.data.labels.push(data.fourthC);
                        chart_doughnut_settings.data.datasets[0].data.push(data.fourthCount);
                        chart_doughnut_settings.data.datasets[0].backgroundColor.push("blue");
                        chart_doughnut_settings.data.datasets[0].hoverBackgroundColor.push("#36CAAB");
                        $('#counselingTopTable').append("<tr><td>"+data.fourthC+"</td><td>"+data.fourthCount+"</td></tr>")
                    break;
                    case 4:
                        chart_doughnut_settings.data.labels.push(data.fifthC);
                        chart_doughnut_settings.data.datasets[0].data.push(data.fifthCount);
                        chart_doughnut_settings.data.datasets[0].backgroundColor.push("#3498DB");
                        chart_doughnut_settings.data.datasets[0].hoverBackgroundColor.push("#49A9EA");
                        $('#counselingTopTable').append("<tr><td>"+data.fifthC+"</td><td>"+data.fifthCount+"</td></tr>")
                    break;
                    default:
                        break;
                }
            }
            $('.contractDoughnut').each(function(){
                
                var chart_element = $(this);
                var chart_doughnut = new Chart( chart_element, chart_doughnut_settings);
                
            });	

        }
    });		
}
function initCounseling(){
    $.get('/counselingActive', function(dataCounseling){
        //console.log(dataCounseling);
        var horizontalBarChartData = {
            labels: ["January", "February", "March", "April", "May", "June", "August", "September", "October", "November", "December"],
            datasets: [{
                backgroundColor: "#00b0f0",
                data: [dataCounseling.january,
                        dataCounseling.february,
                        dataCounseling.march,
                        dataCounseling.april,
                        dataCounseling.may,
                        dataCounseling.june,
                        dataCounseling.july,
                        dataCounseling.august,
                        dataCounseling.september,
                        dataCounseling.october,
                        dataCounseling.november,
                        dataCounseling.december]
            }]

        };
        var ctx = document.getElementById("counselingBarChart").getContext("2d");
        var myHorizontalBar = new Chart(ctx, {
            type: 'horizontalBar',
            data: horizontalBarChartData,
            options: {
            
                scales: {
                yAxes:[{ 
                    barThickness: 20,
                    ticks: {
                        beginAtZero:false,
                        mirror: false,
                    },
                }],
                },
                responsive: true,
                legend: {
                    display: false,
                },
                title: {
                    display: true,
                    text: 'Counseling Bar Chart'
                }}
        });
    });
}
function initReferral(){
    $.get('/referralStatistics', function(dataCounseling){
       
        var horizontalBarChartData = {
            labels: ["January", "February", "March", "April", "May", "June", "August", "September", "October", "November", "December"],
            datasets: [{
                backgroundColor: "#00b0f0",
                data: [dataCounseling.january,
                        dataCounseling.february,
                        dataCounseling.march,
                        dataCounseling.april,
                        dataCounseling.may,
                        dataCounseling.june,
                        dataCounseling.july,
                        dataCounseling.august,
                        dataCounseling.september,
                        dataCounseling.october,
                        dataCounseling.november,
                        dataCounseling.december]
            }]

        };
        var ctx = document.getElementById("referralGraphBody").getContext("2d");
        var myHorizontalBar = new Chart(ctx, {
            type: 'bar',
            data: horizontalBarChartData,
            options: {
            
                scales: {
                yAxes:[{ 
                    barThickness: 20,
                    ticks: {
                        beginAtZero:false,
                        mirror: false,
                    },
                }],
                },
                responsive: true,
                legend: {
                    display: false,
                },
                title: {
                    display: true,
                    text: 'Referral Bar Chart'
                }}
        });
    });
}
function initEvents(){
    $.get('/upcomingevents', function(data){
        $(".eventsBody").html(data);
    });
}
initCounseling();
initReferral();
initEvents();