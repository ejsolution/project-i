<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\useraccount;
use Session;
use Image;
use Illuminate\Support\Facades\DB;
use Mail;
use App\Mail\userMail;
use Illuminate\Support\Str;
use App\Rules\AdminKey;
class registerUser extends Controller
{
  private $request;
  private $user;
  public function __construct(){
    //$this->middleware('guest:myuser');
  }
  public function getRegisterPage(){
    if(useraccount::select()->where('userType', 'admin')->count()>0){
      return view('error', ['error'=>'Unauthorized Access']);
    }else{
      return view('authViews/register');
    }
  }
  public function store(Request $request){
    $this->validate($request, array(
          'userName'=>'required|max:100|unique:useraccounts,userName',
          'password'=>'required|max:100|confirmed',
          'firstname'=>'required|max:100',
          'lastname'=>'required|max:100',
          'email'=>'required|max:100|unique:useraccounts,email|email',
          'adminKey'=>['required', new Adminkey],
          'uploadProfileImage' => 'required|image|mimes:jpeg,png,jpg|max:2048'
    ));
    $this->request = $request;
    $this->user = new useraccount;
    DB::transaction(function() {
      $this->user= new useraccount;
      $this->user->userName = $this->request->userName;
      $this->user->password = bcrypt($this->request->password);
      $this->user->email = $this->request->email;
      $this->user->firstName = $this->request->firstname;
      $this->user->userType = 'admin';
      $this->user->status = '1';
      $this->user->lastName = $this->request->lastname;
      $image = $this->request->file('uploadProfileImage');
      $this->user->imageType = $image->getClientOriginalExtension();
      $this->user->verify_token = Str::random(40);
      $this->user->save();
      $filename = $this->user->id.'.'.$image->getClientOriginalExtension();
      //error_log($filename);
      $location = public_path("images-database/admin/". $filename);
      Image::make($image)->resize(200,200)->save($location);
    });
    $this->verifyEmail(useraccount::find($this->user->id));
    //save image to folder
    //redirect
    //return redirect()->route('admin.show', $this->admin->id);
    return redirect()->route('verify.admin', $this->user->id);
  }

  public function verifyEmail($thisAdmin){
    Mail::to($thisAdmin['email'])->send(new userMail($thisAdmin));
  }
  public function getVerifyPage(){
    return view('authViews/emailVerification');
  }
  public function sendEmailDone($email, $verifytoken){
    $admin = useraccount::where(['email'=>$email, 'verify_token'=>$verifytoken])->first();
    if($admin){
      useraccount::where(['email'=>$email, 'verify_token'=>$verifytoken])->update(['verified'=>'1', 'verify_token'=>null]);
      Session::flash('success', 'Admin registered!');
      return redirect()->route('show.admin', $admin['id']);
    }else{
      return 'user not found';
    }
  }
  public function show($id)
  {
      $admin = useraccount::find($id);
      return view('authViews.adminSuccessRegistration', [
                                                          'userName'=>$admin->userName,
                                                          'email' =>$admin->email
                                                        ]);
  }

}
