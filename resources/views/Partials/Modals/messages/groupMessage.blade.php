<div id="newGroupMessageModal" class="compose col-md-6" style = 'display:none; z-index:101;' >
  <div class='compose-header'>
    New Group Message
    <button class='close' id = 'closeNewGroupMessage'>
      <span>x</span>
    </button>

  </div>
  <div class='compose-body'>
    <div class='container-fluid'>
      <div class='row'>
        <div class='col-md-12'>
          <label for='receiverNewMessageModalText'>To:</label>
          <input type = 'text' id = 'receiverNewGroupMessageModalText' class='form-control' disabled>
        </div>
        <div class='col-md-12 alignRight genericDivTop'>
          <button class='btn btn-default' id = 'addRecipientNewGroupMessageModalButton'>Add Recipient</button>
        </div>
        {!! Form::open([
          'id'=>'sendGroupMessageForm']) !!}
          <div class='col-md-12'>
            <label for = 'messageNewGroupMessageModalText'>Message:</label>
            <textarea class='form-control' id = 'messageNewGroupMessageModalText' name ='messageNewGroupMessageModalText' rows = '8' required maxlength='100'></textarea>
          </div>
        {!! Form::close() !!}
      </div>
      <div class='row'>
        <div class='col-md-1'>
          From:
        </div>
        <div class='col-md-11'>
          <div>
            <strong>Name: {{session()->get('user')['userName']}}</strong>
          </div>
          <!--div>
            <strong>Position</strong>
          </div>
          <div>
            <strong>College</strong>
          </div-->
        </div>
      </div>
    </div>
  </div>
  <div class='compose-footer'>
    <button class="btn btn-danger" form ='sendGroupMessageForm'><i class='fa fa-reply'></i>&nbsp;Send</button>
    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
  </div>
</div>
<!--div id="newMessageModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">

    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">New Message</h4>
      </div>
      <div class="modal-body">
        <div class='container-fluid'>
          <div class='row'>
            <div class='col-md-12'>
              <label for='receiverNewMessageModalText'>To:</label>
              <input type = 'text' id = 'receiverNewMessageModalText' class='form-control'>
            </div>
            <div class='col-md-12 alignRight genericDivTop'>
              <button class='btn btn-default' id = 'addRecipientNewMessageModalButton'>Add Recipient</button>
            </div>
            <div class='col-md-12'>
              <label for = 'messageNewMessageModalText'>Message:</label>
              <textarea class='form-control' id = 'messageNewMessageModalText' rows = '8'>
              </textarea>
            </div>
          </div>
          <div class='row'>
            <div class='col-md-1'>
              From:
            </div>
            <div class='col-md-11'>
              <div>
                <strong>Name</strong>
              </div>
              <div>
                <strong>Position</strong>
              </div>
              <div>
                <strong>College</strong>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class='fa fa-reply'></i>&nbsp;Send</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>

  </div>
</div-->
