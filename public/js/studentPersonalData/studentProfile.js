$(document).ready(function(){
    var action ='';
    var familyid =''; 
    var schoolbgid ='';
    var favsubjid='';
    var dissubjid ='';
    var leisureid ='';
    var healthrecordid ='';
    var makeupassistanceid = '';
    var upgrade = '';
    $.get('/student/profilearray/'+$('#studentid').val(), function(student){
        //console.log(student);
        $('#profileImage').attr('src', '/images-database/students/'+student.id+'.'+student.imageType);
        $('#dateEnrolledText').val(student.dateEnrolled);
        $('#studentNoTextModal').val(student.studNo);
        $('#dobTextModal').val(student.dateOfBirth);
        $('#nationalityTextModal').val(student.nationality);
        $('#lastNameTextModal').val(student.lastName);
        $('#firstNameTextModal').val(student.firstName);
        $('#religionTextModal').val(student.religion);
        $('#middleNameTextModal').val(student.middleName);
        $('#ethnicityTextModal').val(student.ethnicity);
        $('#languageSpokenTextModal').val(student.language);
        $('#maritalStatusComboModal').val(student.status);
        $('#contactNoTextModal').val(student.contactNo);
        $('#cityAddressTextModal').val(student.cityAddress);
        $('#provincialAddressTextModal').val(student.provincialAddress);
        if(student.gender =='male'){
            $('#maleRadioButton').attr('checked', true);
        }else{
            $('#femaleRadioButton').attr('checked', true);
        }
        
        switch (student.type) {
            case 'college':
                $.get('/student/getcourses/'+student.college, function(data){
                    if(data=="nocourses"){
                        new PNotify({
                            title: 'Error!',
                            text: 'This college still has no courses assigned on it!',
                            type: 'error',
                            styling: 'bootstrap3'
                        });
                    }else{
                        $('#courseComboModal').html(data);
                        $.get('/student/getcourseyear/'+student.course, function(data){
                            $('#yearLevelComboModal').html(data);
                            $('#seniorHighStrand').attr('disabled', true);
                            $('#collegeComboModal').val(student.college);
                            $('#courseComboModal').val(student.course);
                            $('#studentTypeComboModal').val(student.type);
                            $('#seniorHighStrand').attr('required', false);
                            $('#yearLevelComboModal').val(student.gradeLevel);
                        });
                    } 
                });
            break;
            case 'seniorhigh':
                $.get('/student/getcourses/'+$('#collegeComboModal').val(), function(data){
                    $('#courseComboModal').html(data);
                    var html = "<option value ='12'>12</option>"+
                    "<option value ='11'>11</option>";
                    $('#yearLevelComboModal').html(html);
                    $('#seniorHighStrand').attr('disabled', false);
                    $('#seniorHighStrand').attr('required', true);
                    $('#seniorHighStrand').val(student.strand);
                    $('#studentTypeComboModal').val(student.type);
                    $('#yearLevelComboModal').val(student.gradeLevel);
                    $('#collegeComboModal').attr('disabled',true);
                    $('#courseComboModal').attr('disabled',true);
                    $('#collegeComboModal').attr('required',false);
                    $('#courseComboModal').attr('required',false);
                });
                
            break;
            case 'juniorhigh':
                $('#seniorHighStrand').attr('disabled', true);
                $.get('/student/getcourses/'+$('#collegeComboModal').val(), function(data){
                    $('#courseComboModal').html(data);
                    var html = "<option value ='10'>10</option>"+
                                "<option value ='9'>9</option>"+
                                "<option value ='8'>8</option>"+
                                "<option value ='7'>7</option>";
                    $('#yearLevelComboModal').html(html);
                    $('#studentTypeComboModal').val(student.type);
                    $('#yearLevelComboModal').val(student.gradeLevel);
                    $('#collegeComboModal').attr('disabled',true);
                    $('#courseComboModal').attr('disabled',true);
                    $('#collegeComboModal').attr('required',false);
                    $('#courseComboModal').attr('required',false);
                });
            break;
            case 'elementary':
                $('#seniorHighStrand').attr('disabled', true);
                $.get('/student/getcourses/'+$('#collegeComboModal').val(), function(data){
                    $('#courseComboModal').html(data);
                    var html = "<option value ='6'>6</option>"+
                                "<option value ='5'>5</option>"+
                                "<option value ='4'>4</option>"+
                                "<option value ='3'>3</option>"+
                                "<option value ='2'>2</option>"+
                                "<option value ='1'>1</option>";
                    $('#yearLevelComboModal').html(html);
                    $('#studentTypeComboModal').val(student.type);
                    $('#yearLevelComboModal').val(student.gradeLevel);
                    $('#collegeComboModal').attr('disabled',true);
                    $('#courseComboModal').attr('disabled',true);
                    $('#collegeComboModal').attr('required',false);
                    $('#courseComboModal').attr('required',false);
                });
            break;
            default:
                break;
        } 
    }); 
    if($("#studenttype").val() != "College"){
        $("#leisurePresentEducationText").attr("readonly", true);
        $("#leisureCourseChoiceText").attr("readonly", true);
        $("#leisureHowDidYouMakeThisChoiceText").attr("readonly", true);
        $("#leisurePresentEducationTextEdit").attr("readonly", true);
        $("#leisureCourseChoiceTextEdit").attr("readonly", true);
        $("#leisureHowDidYouMakeThisChoiceTextEdit").attr("readonly", true);
    }
    tableLoader('12', 'tableOfFamilies');
    tableLoader('6', 'educationalBackgroundTable');
    tableLoader('4', 'favSubjectTable');
    tableLoader('4', 'dislikedSubjectTable'); 
    tableLoader('6', 'studentLeisureTable');
    tableLoader('12', 'tableHealthRecordBody');
    tableLoader('3', 'personalityTableCheck');
    tableLoader('4', 'makeUpAssistanceTable'); 
    getAjax('/makeupassistance/list/'+$('#studentid').val(), 'makeUpAssistanceTable');
    getAjax('/personality/checkbox/list', 'personalityTableCheck');
    getAjax('/student/healthrecord/list/'+$('#studentid').val(), 'tableHealthRecordBody');
    getAjax('/student/leisure/list/'+$('#studentid').val(), 'studentLeisureTable');
    getAjax('/student/disliked/list/'+$('#studentid').val(), 'dislikedSubjectTable');
    getAjax('/student/favsubject/list/'+$('#studentid').val(), 'favSubjectTable');
    getAjax('/student/educationalbackground/list/'+$('#studentid').val(), 'educationalBackgroundTable');
    getAjax('/student/family/list/'+$('#studentid').val(), 'tableOfFamilies');
    switch($('#studenttype').val()){
        case 'college':
            $('#leisureMajorText').attr('disabled', false);
            $('#leisurePresentEducationText').attr('disabled', false);
            $('#leisureCourseChoiceText').attr('disabled', false);
            $('#leisureHowDidYouMakeThisChoiceText').attr('disabled', false);
            $('#leisureMajorText').attr('required', '');
            $('#leisurePresentEducationText').attr('required', '');
            $('#leisureCourseChoiceText').attr('required', '');
            $('#leisureHowDidYouMakeThisChoiceText').attr('required', '');

            $('#leisureMajorTextEdit').attr('disabled', false);
            $('#leisurePresentEducationTextEdit').attr('disabled', false);
            $('#leisureCourseChoiceTextEdit').attr('disabled', false);
            $('#leisureHowDidYouMakeThisChoiceTextEdit').attr('disabled', false);
            $('#leisureMajorTextEdit').attr('required', '');
            $('#leisurePresentEducationTextEdit').attr('required', '');
            $('#leisureCourseChoiceTextEdit').attr('required', '');
            $('#leisureHowDidYouMakeThisChoiceTextEdit').attr('required', '');
        break;
        case 'seniorhigh':
            $('#leisureMajorText').attr('disabled', false);
            $('#leisurePresentEducationText').attr('disabled', true);
            $('#leisureCourseChoiceText').attr('disabled', true);
            $('#leisureHowDidYouMakeThisChoiceText').attr('disabled', true);
            
            $('#leisureMajorTextEdit').attr('disabled', false);
            $('#leisurePresentEducationTextEdit').attr('disabled', true);
            $('#leisureCourseChoiceTextEdit').attr('disabled', true);
            $('#leisureHowDidYouMakeThisChoiceTextEdit').attr('disabled', true);
        break;
        case 'juniorhigh':
            $('#leisureMajorText').attr('disabled', true);
            $('#leisurePresentEducationText').attr('disabled', true);
            $('#leisureCourseChoiceText').attr('disabled', true);
            $('#leisureHowDidYouMakeThisChoiceText').attr('disabled', true);
            
            $('#leisureMajorTextEdit').attr('disabled', true);
            $('#leisurePresentEducationTextEdit').attr('disabled', true);
            $('#leisureCourseChoiceTextEdit').attr('disabled', true);
            $('#leisureHowDidYouMakeThisChoiceTextEdit').attr('disabled', true);
        break;
        case 'elementary':
            $('#leisureMajorText').attr('disabled', true);
            $('#leisurePresentEducationText').attr('disabled', true);
            $('#leisureCourseChoiceText').attr('disabled', true);
            $('#leisureHowDidYouMakeThisChoiceText').attr('disabled', true);

            $('#leisureMajorTextEdit').attr('disabled', true);
            $('#leisurePresentEducationTextEdit').attr('disabled', true);
            $('#leisureCourseChoiceTextEdit').attr('disabled', true);
            $('#leisureHowDidYouMakeThisChoiceTextEdit').attr('disabled', true);
        break;
    } 
    $('#editProfile').on('click', function(){
        $('#studentFormModal').modal('show');
    });
    $('#addNewFamily').on('click', function(){
        $('#addFamilyStudentModal').modal('show');
        $('#addFamilyStudentModal .modal-title').text('Add A Student Family');
        action = 'add';
    });
    $('#tableOfFamilies').on('click', '.editFamilyStudent', function(){
        familyid =  $(this).prop('id').split('-')[1];
        $('#addFamilyStudentModal').modal('show');
        $('#addFamilyStudentModal .modal-title').text("Edit A Student's family");
        $.get('/student/getfamilyinfo/'+familyid, function(data){
            var obj = JSON.parse(data);
            //console.log(obj);
            $("#nameOfFamilyMember").val(obj.name);
            $("#addressFamily").val(obj.address);
            $("#dateOfBirthFamily").val(obj.dateOfBirth);
            $("#contactNoFamily").val(obj.contactNo);
            $("#religionFamily").val(obj.religion);
            $("#nationalityFamily").val(obj.nationality);
            $("#employerFamily").val(obj.employer);
            $("#languageFamily").val(obj.language);
            $("#relationshipFamily").val(obj.relationship);
            $("#occupationFamily").val(obj.occupation);
        });
        action ='edit';
    });
    $('#studentFamilyForm').on('submit', function(e){
        e.preventDefault(); 
        if(action =='add'){
            var form = document.getElementById('studentFamilyForm');
            myform = new FormData(form);
            myform.append('action', 'add');
            myform.append('id', $('#studentid').val());
            prependPostAjaxErrorCallback('/student/family/add', myform, function(data){
                $('.noStudentFamily').remove();
                
                $('#tableOfFamilies').prepend(data);
                new PNotify({
                    title: 'Success!',
                    text: "Family saved!",
                    type: 'success',
                    styling: 'bootstrap3'
                });
                $('#addFamilyStudentModal').modal('hide');
                $('#studentFamilyForm').find("input[type=text], textarea").val("");
            });
        }else{
            var form = document.getElementById('studentFamilyForm');
            myform = new FormData(form);
            myform.append('familyid', familyid);
            myform.append('action', 'edit');
            myform.append('id', $('#studentid').val());
            prependPostAjaxErrorCallback('/student/family/add', myform, function(data){
                data = data.replace("<tr id ='familyStudent-"+familyid+"'>", "");
                data = data.replace("</tr>", "");
                $('#familyStudent-'+familyid).html(data);
                new PNotify({
                    title: 'Success!',
                    text: "Family saved!",
                    type: 'success',
                    styling: 'bootstrap3'
                });
                $('#addFamilyStudentModal').modal('hide');
                $('#studentFamilyForm').find("input[type=text], textarea").val("");
            });
        }
    });
    $('#tableOfFamilies').on('click', '.deleteFamilyStudent', function(){
        $('#deleteModal').modal('show');
        $('#deleteMessage').text("Are you sure you want to delete this family of student?");
        mydelete ="deletefamily";
        globalfamilyid = $(this).prop("id").split("-")[1];
    });
    $("#addStudentButton").on('click', function(){
        $("#studentFormModal").modal("show");
        action ='add';
    });
    $('#collegeComboModal').on('change', function(){
        $.get('/student/getcourses/'+$('#collegeComboModal').val(), function(data){
          if(data=="nocourses"){
            new PNotify({
              title: 'Error!',
              text: 'This college still has no courses assigned on it!',
              type: 'error',
              styling: 'bootstrap3' 
            });
          }else{
            $('#courseComboModal').html(data);
            
            $.get('/student/getcourseyear/'+$('#courseComboModal').val(), function(data){
                $('#yearLevelComboModal').html(data);
            });
          }
        });
    });
    $('#courseComboModal').on('change', function(){
        $.get('/student/getcourseyear/'+$('#courseComboModal').val(), function(data){
          $('#yearLevelComboModal').html(data);
        });
    });
    $('#studentTypeComboModal').on('change', function(){
        if($(this).val()=='college'){
          $('#collegeComboModal').attr('required', '');
          $('#courseComboModal').attr('required', '');
          $('#seniorHighStrand').attr('disabled', true);
          $('#collegeComboModal').attr('disabled',false);
          $('#courseComboModal').attr('disabled',false);
          //////
          $.get('/student/getcourses/'+$('#collegeComboModal').val(), function(data){
            if(data=="nocourses"){
                new PNotify({
                    title: 'Error!',
                    text: 'This college still has no courses assigned on it!',
                    type: 'error',
                    styling: 'bootstrap3'
                });
            }else{
                $('#courseComboModal').html(data);
                $.get('/student/getcourseyear/'+$('#courseComboModal').val(), function(data){
                    $('#yearLevelComboModal').html(data);
                });
            }
        });
        }else{
          $('#collegeComboModal').removeAttr('required');
          $('#courseComboModal').removeAttr('required');
          $('#collegeComboModal').attr('disabled',true);
          $('#courseComboModal').attr('disabled',true);
          if($(this).val()=='seniorhigh'){
            var html = "<option value ='12'>12</option>"+
                      "<option value ='11'>11</option>";
            $('#seniorHighStrand').attr('disabled', false);
           
          }else if($(this).val()=='juniorhigh'){
            $('#seniorHighStrand').attr('disabled', true);
            var html = "<option value ='10'>10</option>"+
                       "<option value ='9'>9</option>"+
                       "<option value ='8'>8</option>"+
                       "<option value ='7'>7</option>";
          }else if($(this).val()=='elementary'){
            $('#seniorHighStrand').attr('disabled', true);
            var html = "<option value ='6'>6</option>"+
                       "<option value ='5'>5</option>"+
                       "<option value ='4'>4</option>"+
                       "<option value ='3'>3</option>"+
                       "<option value ='2'>2</option>"+
                       "<option value ='1'>1</option>";
          }
          $('#yearLevelComboModal').html(html);
        }
    });
    $('#studentForm').on('submit', function(e){
        e.preventDefault();
        var form = document.getElementById('studentForm');
        var myform = new FormData(form);
        myform.append('id', $('#studentid').val());
       
        prependPostAjaxErrorCallback('/student/update', myform, function(data){
            if(data == 'There is no activated school year yet!'){
              new PNotify({
                title: 'Error!',
                text: data,
                type: 'error',
                styling: 'bootstrap3'
              });
            }else if (data == 'There is no assigned courses in this college!') {
              new PNotify({
                title: 'Error!',
                text: data,
                type: 'error',
                styling: 'bootstrap3'
              });
            } else if(data == 'student number already exists!'){
              new PNotify({
                title: 'Error!',
                text: data,
                type: 'error',
                styling: 'bootstrap3'
              });
            }else if (data == 'This student cannot change to college because he has existing college records!') {
                new PNotify({
                    title: 'Error!',
                    text: data,
                    type: 'error',
                    styling: 'bootstrap3'
                  });
            }else if (data == 'This student cannot change to seniorhigh because he has existing seniorhigh records!') {
                new PNotify({
                    title: 'Error!',
                    text: data,
                    type: 'error',
                    styling: 'bootstrap3'
                  });
            }else if (data == 'This student cannot change to juniorhigh because he has existing juniorhigh records!') {
                new PNotify({
                    title: 'Error!',
                    text: data,
                    type: 'error',
                    styling: 'bootstrap3'
                  });
            } else if (data == 'This student cannot change to elementary because he has existing elementary records!') {
                new PNotify({
                    title: 'Error!',
                    text: data,
                    type: 'error',
                    styling: 'bootstrap3'
                  });
            }else if(data =='cannotchange'){
                new PNotify({
                    title: 'Error!',
                    text: "This students student type cannot be changed because it has existing records",
                    type: 'error',
                    styling: 'bootstrap3'
                  });
            }else{
              $('#studenttype').val($('#studentTypeComboModal').val());
              switch ($('#studenttype').val()) {
                case 'college':
                    $('#leisureMajorText').attr('required', true);
                    $('#leisurePresentEducationText').attr('required', true);
                    $('#leisureCourseChoiceText').attr('required', true);
                    $('#leisureHowDidYouMakeThisChoiceText').attr('required', true);
                    $('#leisureMajorText').attr('disabled', false);
                    $('#leisurePresentEducationText').attr('disabled', false);
                    $('#leisureCourseChoiceText').attr('disabled', false);
                    $('#leisureHowDidYouMakeThisChoiceText').attr('disabled', false);

                    $('#leisureMajorTextEdit').attr('required', true);
                    $('#leisurePresentEducationTextEdit').attr('required', true);
                    $('#leisureCourseChoiceTextEdit').attr('required', true);
                    $('#leisureHowDidYouMakeThisChoiceTextEdit').attr('required', true);
                    $('#leisureMajorTextEdit').attr('disabled', false);
                    $('#leisurePresentEducationTextEdit').attr('disabled', false);
                    $('#leisureCourseChoiceTextEdit').attr('disabled', false);
                    $('#leisureHowDidYouMakeThisChoiceTextEdit').attr('disabled', false);
                break;
                case 'seniorhigh':
                    $('#leisureMajorText').attr('required', true);
                    $('#leisurePresentEducationText').attr('required', false);
                    $('#leisureCourseChoiceText').attr('required', false);
                    $('#leisureHowDidYouMakeThisChoiceText').attr('required', false);
                    $('#leisureMajorText').attr('disabled', false);
                    $('#leisurePresentEducationText').attr('disabled', true);
                    $('#leisureCourseChoiceText').attr('disabled', true);
                    $('#leisureHowDidYouMakeThisChoiceText').attr('disabled', true);

                    $('#leisureMajorTextEdit').attr('required', true);
                    $('#leisurePresentEducationTextEdit').attr('required', false);
                    $('#leisureCourseChoiceTextEdit').attr('required', false);
                    $('#leisureHowDidYouMakeThisChoiceTextEdit').attr('required', false);
                    $('#leisureMajorTextEdit').attr('disabled', false);
                    $('#leisurePresentEducationTextEdit').attr('disabled', true);
                    $('#leisureCourseChoiceTextEdit').attr('disabled', true);
                    $('#leisureHowDidYouMakeThisChoiceTextEdit').attr('disabled', true);
                break;
                
                case 'juniorhigh':
                    $('#leisureMajorText').attr('required', false);
                    $('#leisurePresentEducationText').attr('required', false);
                    $('#leisureCourseChoiceText').attr('required', false);
                    $('#leisureHowDidYouMakeThisChoiceText').attr('required', false);
                    $('#leisureMajorText').attr('disabled', true);
                    $('#leisurePresentEducationText').attr('disabled', true);
                    $('#leisureCourseChoiceText').attr('disabled', true);
                    $('#leisureHowDidYouMakeThisChoiceText').attr('disabled', true);

                    $('#leisureMajorTextEdit').attr('required', false);
                    $('#leisurePresentEducationTextEdit').attr('required', false);
                    $('#leisureCourseChoiceTextEdit').attr('required', false);
                    $('#leisureHowDidYouMakeThisChoiceTextEdit').attr('required', false);
                    $('#leisureMajorTextEdit').attr('disabled', true);
                    $('#leisurePresentEducationTextEdit').attr('disabled', true);
                    $('#leisureCourseChoiceTextEdit').attr('disabled', true);
                    $('#leisureHowDidYouMakeThisChoiceTextEdit').attr('disabled', true);
                break;
                case 'elementary':
                    $('#leisureMajorText').attr('required', false);
                    $('#leisurePresentEducationText').attr('required', false);
                    $('#leisureCourseChoiceText').attr('required', false);
                    $('#leisureHowDidYouMakeThisChoiceText').attr('required', false);
                    $('#leisureMajorText').attr('disabled', true);
                    $('#leisurePresentEducationText').attr('disabled', true);
                    $('#leisureCourseChoiceText').attr('disabled', true);
                    $('#leisureHowDidYouMakeThisChoiceText').attr('disabled', true);

                    $('#leisureMajorTextEdit').attr('required', false);
                    $('#leisurePresentEducationTextEdit').attr('required', false);
                    $('#leisureCourseChoiceTextEdit').attr('required', false);
                    $('#leisureHowDidYouMakeThisChoiceTextEdit').attr('required', false);
                    $('#leisureMajorTextEdit').attr('disabled', true);
                    $('#leisurePresentEducationTextEdit').attr('disabled', true);
                    $('#leisureCourseChoiceTextEdit').attr('disabled', true);
                    $('#leisureHowDidYouMakeThisChoiceTextEdit').attr('disabled', true);
                break;
              
                  default:
                      break;
              }
              new PNotify({
                title: 'Success!',
                text: 'Student saved!',
                type: 'success',
                styling: 'bootstrap3'
              });
              $('#studentFormModal').modal('hide');
              //console.log(data);
              //$('#studentTable').append(data);
            }
        });
    });
    $('#removeImage').on('click', function(){
        $('#studentImage').val('');
        $('#profileImage').attr('src', '/images/defaultimage.png');
    });
    $('#familyInfoForm').on('submit', function(e){
        e.preventDefault();
        var form = document.getElementById('familyInfoForm');
        var myform = new FormData(form);
        myform.append('id', $('#studentid').val());
        prependPostAjaxErrorCallback('/student/familyinfo/submit', myform, function(data){
            new PNotify({
                title: 'Success!',
                text: 'Student saved!',
                type: 'success',
                styling: 'bootstrap3'
            });
           
            if(data.marriedCivil == '1'){
                $('#marriedCivillyRadio').attr('checked', true);
            }
            if(data.marriedChurch == '1'){
                $('#marriedChurchRadio').attr('checked', true);
            }
            if(data.livingTogether == '1'){
                $('#parentsLivingTogetherRadio').attr('checked', true);
            }
            if(data.seperated == '1'){
                $('#parentsSeperatedRadio').attr('checked', true);
            }
            if(data.fatherRemarried == '1'){
                $('#fatherRemarriedRadio').attr('checked', true);
            }
            if(data.motherRemarried == '1'){
                $('#motherRemarriedRadio').attr('checked', true);
            }
            $('#noOfRelatives').val(data.relatives);
            $('#noOfHelpers').val(data.helpers);
        });
    });
    $('#studentEducationalBackgroundForm').on('submit', function(e){
        e.preventDefault();
        var form = document.getElementById('studentEducationalBackgroundForm');
        var myform = new FormData(form);
        myform.append('id', $('#studentid').val());
        prependPostAjaxErrorCallback('/student/educationalbackground/insert', myform, function(data){
            $('#educationalBackgroundTable').children('h2').each(function(){
                $(this).remove();
            });
            $('#educationalBackgroundTable').prepend(data);
            new PNotify({
                title: 'Success!',
                text: "Educational background saved!",
                type: 'success',
                styling: 'bootstrap3'
            });
            $('#studentEducationalBackgroundForm').find("input[type=text], input[type=number], textarea").val("");
        });
    });
    $('#educationalBackgroundTable').on('click', '.editEducationalBackground', function(){
        schoolbgid=  $(this).prop('id').split('-')[1];
        $('#editStudentBackgroundForm').modal('show');
        var temp = [];
        var i =0;
        $('#educationalBackground-'+schoolbgid).children('td').each(function(){
            temp[i] = $(this).html();
            i++;
        });
        
        $('#schoolBackgroundNameEditText').val(temp[0]);
        $('#schoolBackgroundHonorsEditText').val(temp[1]);
        $('#schoolBackgroundYearLevelEdit').val(temp[2]);
        $('#schoolBackgroundYearAttendedEdit').val(temp[3]);
    });
    $('#educationalBackgroundTable').on('click', '.deleteEducationalBackground', function(){
        $('#deleteModal').modal('show');
        $('#deleteMessage').text("Are you sure you want to delete this educational background?");
        mydelete ="deleteeducationalbackground";
        globalschoolbackgroundid = $(this).prop("id").split("-")[1];
    });
    $('#schoolBackgroundEditForm').on('submit', function(e){
        e.preventDefault();
        var form = document.getElementById('schoolBackgroundEditForm');
        var myform = new FormData(form);
        myform.append('id', schoolbgid);
        myform.append('studentID', $('#studentid').val());
        $("#editStudentBackgroundForm").modal("hide");
        prependPostAjaxErrorCallback('/student/schoolbackground/update', myform, function(data){
            new PNotify({
                title: 'Success!',
                text: 'School Background saved!',
                type: 'success',
                styling: 'bootstrap3'
            });
            data = data.replace("<tr id ='educationalBackground-"+schoolbgid+"'>", "");
            data = data.replace("</tr>", "");
            $('#educationalBackground-'+schoolbgid).html(data);
        });
    });
    $('#likedSubjectsForm').on('submit', function(e){
        e.preventDefault();
        var form = document.getElementById('likedSubjectsForm');
        var myform = new FormData(form);
            myform.append('studentID', $('#studentid').val());
        $('#favSubjectTable').children('h2').each(function(){
           $(this).remove(); 
        });
        prependPostAjaxErrorCallback('/student/favsubject/insert', myform, function(data){
            if(data.status == 'success'){
                new PNotify({
                    title: 'Success!',
                    text: 'Favorite Subject saved!',
                    type: 'success',
                    styling: 'bootstrap3'
                });
                $('#favSubjectTable').append(data.html);
            }else{
                if(data=='cannotinsertfav'){
                    new PNotify({
                        title: 'Error!',
                        text: 'This student already has an existing favorite subject!',
                        type: 'error',
                        styling: 'bootstrap3'
                    });
                 }else{
                    new PNotify({
                        title: 'Error!',
                        text: 'This student already has an existing disliked subject!',
                        type: 'error',
                        styling: 'bootstrap3'
                    });
                 }
            }
        });
    });
    $('#favSubjectTable').on('click', '.editFavSubject', function(){
        $('#editFavSubjectModal').modal('show');
        favsubjid = $(this).prop('id').split('-')[1];
        $.get('/subject/list/combo', function(data){
            $('#favSubjectsEdit').html(data);
            var temp = [];
            var i =0;
            $('#favsubject-'+favsubjid).children('td').each(function(){
                if(i==0){
                    temp[i] = $(this).attr('data-value');
                    console.log(temp[i]);
                    i++;
                }else{
                    temp[i] = $(this).html();
                    i++;
                }
            });
            $('#favSubjectsEdit').val(temp[0]);
            $('#favSubjectGradeEdit').val(temp[1]);
        });
        
    });
    $('#favSubjectTable').on('click', '.deleteFavSubject', function(){
        globalfavsubjid = $(this).prop('id').split('-')[1];
        $('#deleteModal').modal('show');
        $('#deleteMessage').text("Are you sure you want to delete this favorite subject?");
        mydelete ="deletefavoritesubject";
    });
    $('#editFavSubjectForm').on('submit', function(e){
        e.preventDefault();
        var form = document.getElementById('editFavSubjectForm');
        var myform = new FormData(form);
        myform.append('favsubjid', favsubjid);
        myform.append('studentID', $('#studentid').val());
        $("#editFavSubjectModal").modal("hide");
        prependPostAjaxErrorCallback('/student/favsubject/update', myform, function(data){
            if(data=='cannotinsert'){
                new PNotify({
                    title: 'Error!',
                    text: 'This subject is already a favorite or disliked subject of a student!',
                    type: 'error',
                    styling: 'bootstrap3'
                });
            }else{
                new PNotify({
                    title: 'Success!',
                    text: 'Subject saved!',
                    type: 'success',
                    styling: 'bootstrap3'
                });
                var y =0;
                $('#favsubject-'+favsubjid).children('td').each(function(){
                    if(y>1){
                        return false;
                    }
                    switch (y) {
                        case 1:
                            $(this).text($('#favSubjectGradeEdit').val());
                        break;
                        case 0:
                            $(this).text($("#favSubjectsEdit option[value='"+$("#favSubjectsEdit").val()+"']").text());
                        break;
                        default:
                            break;
                    }
                    y++;
                });
            }
        });
    });
    $('#dislikedSubjectsForm').on('submit', function(e){
        e.preventDefault();
        var form = document.getElementById('dislikedSubjectsForm');
        var myform = new FormData(form);
        myform.append('studentID', $('#studentid').val());
        $('#dislikedSubjectTable').children('h2').each(function(){
            $(this).remove(); 
         });
         prependPostAjaxErrorCallback('/student/disliked/insert', myform, function(data){
             if(data.status == 'success'){
                 new PNotify({
                     title: 'Success!',
                     text: 'Favorite Subject saved!',
                     type: 'success',
                     styling: 'bootstrap3'
                 });
                 $('#dislikedSubjectTable').append(data.html);
             }else if(data.status == undefined){
                 if(data=='cannotinsertfav'){
                    new PNotify({
                        title: 'Error!',
                        text: 'This student already has an existing favorite subject!',
                        type: 'error',
                        styling: 'bootstrap3'
                    });
                 }else{
                    new PNotify({
                        title: 'Error!',
                        text: 'This student already has an existing disliked subject!',
                        type: 'error',
                        styling: 'bootstrap3'
                    });
                 }
                 
             }
        });
    });
    $('#dislikedSubjectTable').on('click', '.editDislikedSubject', function(){
        $('#editDislikedSubjectModal').modal('show');
        dissubjid = $(this).prop('id').split('-')[1];
        $.get('/subject/list/combo', function(data){
            $('#dislikedSubjectsEdit').html(data);
            var temp = [];
            var i =0;
            $('#dislikedsubject-'+dissubjid).children('td').each(function(){
                if(i==0){
                    temp[i] = $(this).attr('data-value');
                    console.log(temp[i]);
                    i++;
                }else{
                    temp[i] = $(this).html();
                    i++;
                }
            });
            $('#dislikedSubjectsEdit').val(temp[0]);
            $('#dislikedSubjectGradeEdit').val(temp[1]);
        });
    }); 
    $('#editDislikedSubjectForm').on('submit', function(e){
        e.preventDefault();
        var form = document.getElementById('editDislikedSubjectForm');
        var myform = new FormData(form);
        myform.append('dissubjid', dissubjid);
        myform.append('studentID', $('#studentid').val());
        $("#editDislikedSubjectModal").modal("hide");
        prependPostAjaxErrorCallback('/student/disliked/update', myform, function(data){
            if(data=='cannotinsert'){
                new PNotify({
                    title: 'Error!',
                    text: 'This subject is already a favorite or disliked subject of a student!',
                    type: 'error',
                    styling: 'bootstrap3'
                });
            }else{
                new PNotify({
                    title: 'Success!',
                    text: 'Subject saved!',
                    type: 'success',
                    styling: 'bootstrap3'
                });
                var y =0;
                $('#dislikedsubject-'+dissubjid).children('td').each(function(){
                    if(y>1){
                        return false;
                    }
                    switch (y) {
                        case 1:
                            $(this).text($('#dislikedSubjectGradeEdit').val());
                        break;
                        case 0:
                            $(this).text($("#dislikedSubjectsEdit option[value='"+$("#dislikedSubjectsEdit").val()+"']").text());
                        break;
                        default:
                            break;
                    }
                    y++;
                });
            }
        });
    });
    $('#dislikedSubjectTable').on('click', '.deleteDislikedSubject', function(){
        globaldislikedsubjid = $(this).prop('id').split('-')[1];
        $('#deleteModal').modal('show');
        $('#deleteMessage').text("Are you sure you want to delete this disliked subject?");
        mydelete ="deletedislikedsubject";
    });
    $('#studentLeisureForm').on('submit', function(e){
        e.preventDefault();
        form = document.getElementById('studentLeisureForm');
        var myform = new FormData(form);
        myform.append('studentID', $('#studentid').val());
        checked = $("#studentLeisureForm input[type=checkbox]:checked").length;
        if(checked ==0){
            new PNotify({
                title: 'Error!',
                text: 'Atleast one checkbox should be checked!',
                type: 'error',
                styling: 'bootstrap3'
            });
            return;
        }
        prependPostAjaxErrorCallback('/student/leisure/insert', myform, function(data){
            new PNotify({
                title: 'Success!',
                text: 'Leisure successfully saved!',
                type: 'success',
                styling: 'bootstrap3'
            });
            $('#studentLeisureTable').children('h2').each(function(){
                $(this).remove();
            });
            $('#studentLeisureTable').prepend(data);
        });
    });
    $('#studentLeisureTable').on('click', '.leisureedit',function(){
        $('#editStudentLeisureModal').modal('show');
        leisureid=$(this).prop('id').split('-')[1];
        $.get('/student/leisureinfo/'+leisureid+'/'+$('#studentid').val(), function(student){
            console.log($('#studenttype').val());

            switch($('#studenttype').val()){
                case 'College':
                    switch (student.getInfo) {
                        case 'verymuch':
                            $('#editStudentLeisureForm :radio[value=verymuch]').attr('checked', true);
                        break;
                        case 'much':
                            $('#editStudentLeisureForm :radio[value=much]').attr('checked', true);
                        break;
                        case 'enough':
                            $('#editStudentLeisureForm :radio[value=enough]').attr('checked', true);
                        break;
                        case 'verylittle':
                            $('#editStudentLeisureForm :radio[value=verylittle]').attr('checked', true);
                        break;
                        case 'none':
                            $('#editStudentLeisureForm :radio[value=none]').attr('checked', true);
                        break;
                    
                        default:
                            break;
                    }
                    
                    if(student.selfEval1 == '1'){
                        $('#barelyPassedSubjectsCheckEdit').attr('checked', true);
                    }else{
                        $('#barelyPassedSubjectsCheckEdit').attr('checked', false);
                    }
                    if(student.selfEval2 == '1'){
                        $('#failedSubjectsCheckEdit').attr('checked', true);
                    }else{
                        $('#failedSubjectsCheckEdit').attr('checked', false);
                    }
                    if(student.selfEval3 == '1'){
                        $('#fearSubjectsCheckEdit').attr('checked', true);
                    }else{
                        $('#fearSubjectsCheckEdit').attr('checked', false);
                    }
                    if(student.selfEval4 == '1'){
                        $('#hardSubjectsCheckEdit').attr('checked', true);
                    }else{
                        $('#hardSubjectsCheckEdit').attr('checked', false);
                    }
                    if(student.selfEval5 == '1'){
                        $('#dificultySubjectsCheckEdit').attr('checked', true);
                    }else{
                        $('#dificultySubjectsCheckEdit').attr('checked', false);
                    }
                    if(student.selfEval6 == '1'){
                        $('#confidentCheckEdit').attr('checked', true);
                    }else{
                        $('#confidentCheckEdit').attr('checked', false);
                    }
                    $('#leisureMajorTextEdit').val(student.major);
                    $('#leisurePresentEducationTextEdit').val(student.presentEducation);
                    $('#leisureCourseChoiceTextEdit').val(student.courseChoice);
                    $('#leisureHowDidYouMakeThisChoiceTextEdit').val(student.howDidYouMakeThisChoice);
                    $('#leisureSchoolChoiceTextEdit').val(student.schoolChoice);
                    $('#leisureHowDidYouComeToThisSchoolTextEdit').val(student.howDidYouComeToThisSchool);
                    $('#leisureWhereDidYouGetThisInfoEdit').val(student.whereDidYouGetThisInfo);
                    $('#leisureFinancialSupportTextEdit').val(student.financialSupport);
                    $('#leisureScholarshipTextEdit').val(student.scholarship);
                    $('#leisureRemarksTextModalEdit').val(student.otherRemarks);
                    $('#leisureRankClassEdit').val(student.rankClass);
                    $('#leisureAverageTextEdit').val(student.average);
                
                break;
                case 'Senior High':
                    switch (student.getInfo) {
                        case 'verymuch':
                            $('#editStudentLeisureForm :radio[value=verymuch]').attr('checked', true);
                        break;
                        case 'much':
                            $('#editStudentLeisureForm :radio[value=much]').attr('checked', true);
                        break;
                        case 'enough':
                            $('#editStudentLeisureForm :radio[value=enough]').attr('checked', true);
                        break;
                        case 'verylittle':
                            $('#editStudentLeisureForm :radio[value=verylittle]').attr('checked', true);
                        break;
                        case 'none':
                            $('#editStudentLeisureForm :radio[value=none]').attr('checked', true);
                        break;
                    
                        default:
                            break;
                    }
                    
                    if(student.selfEval1 == '1'){
                        $('#barelyPassedSubjectsCheckEdit').attr('checked', true);
                    }else{
                        $('#barelyPassedSubjectsCheckEdit').attr('checked', false);
                    }
                    if(student.selfEval2 == '1'){
                        $('#failedSubjectsCheckEdit').attr('checked', true);
                    }else{
                        $('#failedSubjectsCheckEdit').attr('checked', false);
                    }
                    if(student.selfEval3 == '1'){
                        $('#fearSubjectsCheckEdit').attr('checked', true);
                    }else{
                        $('#fearSubjectsCheckEdit').attr('checked', false);
                    }
                    if(student.selfEval4 == '1'){
                        $('#hardSubjectsCheckEdit').attr('checked', true);
                    }else{
                        $('#hardSubjectsCheckEdit').attr('checked', false);
                    }
                    if(student.selfEval5 == '1'){
                        $('#dificultySubjectsCheckEdit').attr('checked', true);
                    }else{
                        $('#dificultySubjectsCheckEdit').attr('checked', false);
                    }
                    if(student.selfEval6 == '1'){
                        $('#confidentCheckEdit').attr('checked', true);
                    }else{
                        $('#confidentCheckEdit').attr('checked', false);
                    }
                    $('#leisureMajorTextEdit').val(student.major);
                    /*$('#leisurePresentEducationTextEdit').val(student.presentEducation);
                    $('#leisureCourseChoiceTextEdit').val(student.courseChoice);
                    $('#leisureHowDidYouMakeThisChoiceTextEdit').val(student.howDidYouMakeThisChoice);*/
                    $('#leisureSchoolChoiceTextEdit').val(student.schoolChoice);
                    $('#leisureHowDidYouComeToThisSchoolTextEdit').val(student.howDidYouComeToThisSchool);
                    $('#leisureWhereDidYouGetThisInfoEdit').val(student.whereDidYouGetThisInfo);
                    $('#leisureFinancialSupportTextEdit').val(student.financialSupport);
                    $('#leisureScholarshipTextEdit').val(student.scholarship);
                    $('#leisureRemarksTextModalEdit').val(student.otherRemarks);
                    $('#leisureRankClassEdit').val(student.rankClass);
                    $('#leisureAverageTextEdit').val(student.average);
                break;
                case 'Junior High':
                    switch (student.getInfo) {
                        case 'verymuch':
                            $('#editStudentLeisureForm :radio[value=verymuch]').attr('checked', true);
                        break;
                        case 'much':
                            $('#editStudentLeisureForm :radio[value=much]').attr('checked', true);
                        break;
                        case 'enough':
                            $('#editStudentLeisureForm :radio[value=enough]').attr('checked', true);
                        break;
                        case 'verylittle':
                            $('#editStudentLeisureForm :radio[value=verylittle]').attr('checked', true);
                        break;
                        case 'none':
                            $('#editStudentLeisureForm :radio[value=none]').attr('checked', true);
                        break;
                    
                        default:
                            break;
                    }
                    
                    if(student.selfEval1 == '1'){
                        $('#barelyPassedSubjectsCheckEdit').attr('checked', true);
                    }else{
                        $('#barelyPassedSubjectsCheckEdit').attr('checked', false);
                    }
                    if(student.selfEval2 == '1'){
                        $('#failedSubjectsCheckEdit').attr('checked', true);
                    }else{
                        $('#failedSubjectsCheckEdit').attr('checked', false);
                    }
                    if(student.selfEval3 == '1'){
                        $('#fearSubjectsCheckEdit').attr('checked', true);
                    }else{
                        $('#fearSubjectsCheckEdit').attr('checked', false);
                    }
                    if(student.selfEval4 == '1'){
                        $('#hardSubjectsCheckEdit').attr('checked', true);
                    }else{
                        $('#hardSubjectsCheckEdit').attr('checked', false);
                    }
                    if(student.selfEval5 == '1'){
                        $('#dificultySubjectsCheckEdit').attr('checked', true);
                    }else{
                        $('#dificultySubjectsCheckEdit').attr('checked', false);
                    }
                    if(student.selfEval6 == '1'){
                        $('#confidentCheckEdit').attr('checked', true);
                    }else{
                        $('#confidentCheckEdit').attr('checked', false);
                    }
                    /*$('#leisureMajorTextEdit').val(student.major);
                    $('#leisurePresentEducationTextEdit').val(student.presentEducation);
                    $('#leisureCourseChoiceTextEdit').val(student.courseChoice);
                    $('#leisureHowDidYouMakeThisChoiceTextEdit').val(student.howDidYouMakeThisChoice);*/
                    $('#leisureSchoolChoiceTextEdit').val(student.schoolChoice);
                    $('#leisureHowDidYouComeToThisSchoolTextEdit').val(student.howDidYouComeToThisSchool);
                    $('#leisureWhereDidYouGetThisInfoEdit').val(student.whereDidYouGetThisInfo);
                    $('#leisureFinancialSupportTextEdit').val(student.financialSupport);
                    $('#leisureScholarshipTextEdit').val(student.scholarship);
                    $('#leisureRemarksTextModalEdit').val(student.otherRemarks);
                    $('#leisureRankClassEdit').val(student.rankClass);
                    $('#leisureAverageTextEdit').val(student.average);
                break;
                case 'Elementary':
                    switch (student.getInfo) {
                        case 'verymuch':
                            $('#editStudentLeisureForm :radio[value=verymuch]').attr('checked', true);
                        break;
                        case 'much':
                            $('#editStudentLeisureForm :radio[value=much]').attr('checked', true);
                        break;
                        case 'enough':
                            $('#editStudentLeisureForm :radio[value=enough]').attr('checked', true);
                        break;
                        case 'verylittle':
                            $('#editStudentLeisureForm :radio[value=verylittle]').attr('checked', true);
                        break;
                        case 'none':
                            $('#editStudentLeisureForm :radio[value=none]').attr('checked', true);
                        break;
                    
                        default:
                            break;
                    }
                    
                    if(student.selfEval1 == '1'){
                        $('#barelyPassedSubjectsCheckEdit').attr('checked', true);
                    }else{
                        $('#barelyPassedSubjectsCheckEdit').attr('checked', false);
                    }
                    if(student.selfEval2 == '1'){
                        $('#failedSubjectsCheckEdit').attr('checked', true);
                    }else{
                        $('#failedSubjectsCheckEdit').attr('checked', false);
                    }
                    if(student.selfEval3 == '1'){
                        $('#fearSubjectsCheckEdit').attr('checked', true);
                    }else{
                        $('#fearSubjectsCheckEdit').attr('checked', false);
                    }
                    if(student.selfEval4 == '1'){
                        $('#hardSubjectsCheckEdit').attr('checked', true);
                    }else{
                        $('#hardSubjectsCheckEdit').attr('checked', false);
                    }
                    if(student.selfEval5 == '1'){
                        $('#dificultySubjectsCheckEdit').attr('checked', true);
                    }else{
                        $('#dificultySubjectsCheckEdit').attr('checked', false);
                    }
                    if(student.selfEval6 == '1'){
                        $('#confidentCheckEdit').attr('checked', true);
                    }else{
                        $('#confidentCheckEdit').attr('checked', false);
                    }
                    /*$('#leisureMajorTextEdit').val(student.major);
                    $('#leisurePresentEducationTextEdit').val(student.presentEducation);
                    $('#leisureCourseChoiceTextEdit').val(student.courseChoice);
                    $('#leisureHowDidYouMakeThisChoiceTextEdit').val(student.howDidYouMakeThisChoice);*/
                    $('#leisureSchoolChoiceTextEdit').val(student.schoolChoice);
                    $('#leisureHowDidYouComeToThisSchoolTextEdit').val(student.howDidYouComeToThisSchool);
                    $('#leisureWhereDidYouGetThisInfoEdit').val(student.whereDidYouGetThisInfo);
                    $('#leisureFinancialSupportTextEdit').val(student.financialSupport);
                    $('#leisureScholarshipTextEdit').val(student.scholarship);
                    $('#leisureRemarksTextModalEdit').val(student.otherRemarks);
                    $('#leisureRankClassEdit').val(student.rankClass);
                    $('#leisureAverageTextEdit').val(student.average);
                break;
            }
        });
    });
    $('#editStudentLeisureForm').on('submit', function(e){
        e.preventDefault();
        form = document.getElementById('editStudentLeisureForm');
        var myform = new FormData(form);
        myform.append('studentID', $('#studentid').val());
        myform.append('id', leisureid);
        checked = $("#editStudentLeisureForm input[type=checkbox]:checked").length;
        if(checked ==0){
            new PNotify({
                title: 'Error!',
                text: 'Atleast one checkbox should be checked!',
                type: 'error',
                styling: 'bootstrap3'
            });
            return;
        }
        prependPostAjaxErrorCallback('/student/leisure/update', myform, function(data){
            new PNotify({
                title: 'Success!',
                text: 'Leisure successfully saved!',
                type: 'success',
                styling: 'bootstrap3'
            });
            data = data.replace("<tr id ='leisurerow-"+leisureid+"'>", "");
            data = data.replace("</tr>", "");
            $('#leisurerow-'+leisureid).html(data);
        });
    });
    $('#studentLeisureTable').on('click', '.leisuredelete',function(){
        globalleisureid = $(this).prop('id').split('-')[1];
        globalleisurestudentid = $('#studentid').val();
        $('#deleteModal').modal('show');
        $('#deleteMessage').text("Are you sure you want to delete this leisure?");
        mydelete ="deleteleisure";
    });
    $('#studentHealthRecordForm').on('submit', function(e){
        e.preventDefault();
        var form = document.getElementById('studentHealthRecordForm');
        var myform = new FormData(form);
        myform.append('studentID', $('#studentid').val());
        prependPostAjaxErrorCallback('/student/healthrecord/insert', myform, function(data){
            new PNotify({
                title: 'Success!',
                text: 'Leisure successfully saved!',
                type: 'success',
                styling: 'bootstrap3'
            });
            $('.noHealthRecordStudent').remove();
            $('#tableHealthRecordBody').prepend(data);
            $("#studentHealthRecordForm input[type = 'text']").val("");
            $("#studentHealthRecordForm input[type = 'number']").val("");
        });
    });
    $('#tableHealthRecordBody').on('click', '.healthRecordEdit', function(){
        healthrecordid = $(this).prop('id').split('-')[1];
        var temp =[];
        var i =0;
        $('#healthrecord-'+healthrecordid).children('td').each(function(){
            temp[i]=$(this).html();
            i++;
        });
        $('#healthRecordHeightEdit').val(temp[0]);
        $('#healthRecordWeightEdit').val(temp[1]);
        $('#healthRecordComplexionEdit').val(temp[2]);
        if(temp[3]=='Yes'){
            $('#yesWearGlassesRadioEdit').attr('checked', true);
        }else{
            $('#noWearGlassesRadioEdit').attr('checked', true);
        }
        $('#healthRecordProgramsParticipatedEdit').val(temp[4]);
        $('#healthRecordPhysicalAilmentEdit').val(temp[5]);
        $('#healthRecordNumPeopleHouseEdit').val(temp[7]);
        $('#healthRecordNumPeopleRoomEdit').val(temp[8]);
        $('#healthRecordLiveEdit').val(temp[6]);
        $('#editHealthRecordModal').modal('show');
    });
    $('#editHealthRecordForm').on('submit', function(e){
        e.preventDefault();
        var form = document.getElementById('editHealthRecordForm');
        var myform = new FormData(form);
        myform.append('id', healthrecordid);
        $("#editHealthRecordModal").modal("hide");
        prependPostAjaxErrorCallback('/student/healthrecord/update', myform, function(data){
            new PNotify({
                title: 'Success!',
                text: 'Health Record Successfully saved!',
                type: 'success',
                styling: 'bootstrap3'
            });
            data = data.replace("<tr id ='healthrecord-"+healthrecordid+"'>", "");
            data = data.replace("</tr>", "");
            $('#healthrecord-'+healthrecordid).html(data);
        });
    });
    $('#tableHealthRecordBody').on('click', '.healthRecordDelete', function(){
        globalhealthrecordid = $(this).prop('id').split('-')[1];
        $('#deleteModal').modal('show');
        $('#deleteMessage').text("Are you sure you want to delete this healthrecord?");
        mydelete ="deletehealthrecord";
    });
    $('#makeUpAssistanceForm').on('submit', function(e){
        e.preventDefault();
        form = document.getElementById('makeUpAssistanceForm');
        myform = new FormData(form);
        myform.append('studentID', $('#studentid').val());
        var personalities = [];
        var i = 0;
        $(".radioPersonality").each(function(){
            if($(this).is(":checked")){
                personalities[i] = $(this).val();
                i++;
            }
        });
        if(i==0){
            new PNotify({
                title: 'Error!',
                text: "Please select atleast one personality",
                type: 'error',
                styling: 'bootstrap3'
            });
            return;
        }
        myform.append("personalities", personalities);
        prependPostAjaxErrorCallback('/makeupassistance/insert', myform, function(data){
            if(data =="There is no active schoolyear yet. Please activate one in settings"){
                new PNotify({
                    title: 'Error!',
                    text: data,
                    type: 'error',
                    styling: 'bootstrap3'
                });
                return;
            }
            new PNotify({
                title: 'Success!',
                text: 'Make Up Assistance Successfully saved!',
                type: 'success',
                styling: 'bootstrap3'
            });
            $('#makeUpAssistanceTable').children('h2').each(function(){
                $(this).remove();
            });
            $('#makeUpAssistanceTable').prepend(data);
        });
    });
    $('#makeUpAssistanceTable').on('click', '.editMakeUpAssistance', function(){
        makeupassistanceid =  $(this).prop('id').split('-')[1];
        $('#editMakeUpAssistanceModal').modal('show');
        tableLoader('3', 'personalityTableCheckEdit');
        getAjax('/personality/checkbox/list', 'personalityTableCheckEdit');
        $.get('/makeupassistance/data/'+makeupassistanceid, function(data){
            $('#personalityTableCheckEdit :radio[value='+data.personalityID+']').attr('checked', true);
            $('#makeUpAssistanceSignificantEventsTextEdit').val(data.significantEvents);
            $('#makeUpAssistanceHelpNeededTextEdit').val(data.helpNeeded);
        });
    });
    $('#editMakeUpAssistanceForm').on('submit', function(e){
        e.preventDefault();
        var form = document.getElementById('editMakeUpAssistanceForm');
        var myform = new FormData(form);
        myform.append('id', makeupassistanceid);
        myform.append('studentID', $('#studentid').val());
        $("#editMakeUpAssistanceModal").modal("hide");
        prependPostAjaxErrorCallback('/makeupassistance/update', myform, function(data){
            if(data =="There is no active schoolyear yet. Please activate one in settings"){
                new PNotify({
                    title: 'Error!',
                    text: data,
                    type: 'error',
                    styling: 'bootstrap3'
                });
                return;
            }
            new PNotify({
                title: 'Success!',
                text: 'Make Up Assistance Successfully saved!',
                type: 'success',
                styling: 'bootstrap3'
            });
            
            data = data.replace("<tr id ='makeUpAssistanceRow-"+makeupassistanceid+"'>", "");
            data = data.replace("</tr>", "");
            $('#makeUpAssistanceRow-'+makeupassistanceid).html(data);
        });
    });
    $('#makeUpAssistanceTable').on('click', '.deleteMakeUpAssistance', function(){
        globalmakeupassistanceid = $(this).prop('id').split('-')[1];
        $('#deleteModal').modal('show');
        $('#deleteMessage').text("Are you sure you want to delete this makeupassitance?");
        mydelete ="deletemakeupassistance";
    });
    $('#upgradeStudent').on('click', function(){
        $('#upgradeStudentModal').modal("show");
        $('#collegeCourseStudentUpgrade').hide();
        $('#strandContainerUpgrade').hide();
        $.get('/studentprofile/getgradelevel/type/'+$('#studentid').val(), function(data){
            $.get('/student/getcourses/'+$('#upgradeCollegeCombo').val(), function(data){
                if(data!="nocourses"){
                    $('#upgradeCourseCombo').html(data);
                }
            });
            switch (data.split("-")[0]) {
                case 'college':
                    if(data.split("-")[2]=='graduating'){
                        // new PNotify({
                        //     title: 'Error!',
                        //     text: 'This college student has already reached the last year of his/her course.',
                        //     type: 'warning',
                        //     styling: 'bootstrap3'
                        // });
                        //return;
                        $("#UpgradeStudentMessage").html("This college student has already reached the last year of his/her course."+
                        "Do you still want to enroll this student at current school year and sem?");
                        $('#upgradeStudentConfirm').attr("disabled", false);
                    }else{
                        $("#UpgradeStudentMessage").html("Are you sure you want to upgrade this "+data.split("-")[0]+
                        " student to yearlevel "+(parseFloat(data.split("-")[1])+parseFloat(1)));
                        $('#upgradeStudentConfirm').attr("disabled", false);
                    }  
                break;
                case 'seniorhigh':
                    if(data.split("-")[1]=='12'){
                        $('#collegeCourseStudentUpgrade').show();
                        upgrade = "college";
                        $("#UpgradeStudentMessage").html("Are you sure you want to upgrade this "+data.split("-")[0]+
                        " student to college");
                    }else{
                        $("#UpgradeStudentMessage").html("Are you sure you want to upgrade this "+data.split("-")[0]+
                        " student to gradelevel "+(parseFloat(data.split("-")[1])+parseFloat(1)));
                    }
                    $('#upgradeStudentConfirm').attr("disabled", false);
                break;
                case 'juniorhigh':
                if(data.split("-")[1]=='10'){
                    $('#strandContainerUpgrade').show();
                    upgrade = "seniorhigh";
                }
                $("#UpgradeStudentMessage").html("Are you sure you want to upgrade this "+data.split("-")[0]+
                    " student to gradelevel "+(parseFloat(data.split("-")[1])+parseFloat(1)));
                $('#upgradeStudentConfirm').attr("disabled", false);
                
                break;
                case 'elementary':
                    $("#UpgradeStudentMessage").html("Are you sure you want to upgrade this "+data.split("-")[0]+
                    " student to gradelevel "+(parseFloat(data.split("-")[1])+parseFloat(1)));
                    $('#upgradeStudentConfirm').attr("disabled", false);
                break;
                default:
                break;
            }
            
        });
    });
    $('#upgradeStudentConfirm').on('click', function(){
        if(upgrade == "college"){
            if($("#upgradeCourseCombo").val()==""){
                new PNotify({
                    title: 'Error!',
                    text: 'Course is required to upgrade a seniorhigh to collegestudent.',
                    type: 'error',
                    styling: 'bootstrap3'
                });
                return;
            }
            window.location.href = "/student/upgradeprofilecollege/"+$('#studentid').val()+"/"+$('#upgradeCourseCombo').val();
        }else if (upgrade == "seniorhigh") {
            if($('#upgradeStrandCombo').val()==""){
                new PNotify({
                    title: 'Error!',
                    text: 'Strand is required to upgrade a juniorhigh to seniorhigh.',
                    type: 'error',
                    styling: 'bootstrap3'
                });
                return;
            }
            window.location.href = "/student/upgradeprofileseniorhigh/"+$('#studentid').val()+"/"+$('#upgradeStrandCombo').val();
        }else{
            window.location.href = "/student/upgradeprofile/"+$('#studentid').val();
        }
        
    });
    $("#deleteLatestRecord").on('click', function(){
       
        $.get('/student/profile/number/of/record/'+$('#studentid').val(), function(data){
            if(data != "true"){
                new PNotify({
                    title: 'Error!',
                    text: 'This student only has one record and cannot be removed!',
                    type: 'error',
                    styling: 'bootstrap3'
                });
            }else{
                globalmakeupassistanceid = $('#studentid').val();
                $('#deleteModal').modal('show');
                $('#deleteMessage').text("Are you sure you want to delete the latest record of the student?");
                mydelete ="deletestudentrecord";
                
            }
        });
    });
    $("#saveAllButton").on("click", function(){
        if(validateAll()==false){
            new PNotify({
                title: 'Error!',
                text: 'Please provide all the necessary details!',
                type: 'error',
                styling: 'bootstrap3'
            });
            return;
        }
        var form = document.getElementById('saveAllForm');
        myform = new FormData(form);
        myform.append("family_maritalStatus", $("input:radio[name='maritalStatus']").val());
        myform.append("family_noOfRelatives", $("#noOfRelatives").val());
        myform.append("family_noOfHelpers", $("#noOfHelpers").val());
        myform.append("schoolBackground_schoolBackgroundNameText", $("#schoolBackgroundNameText").val());
        myform.append("schoolBackground_honorsText", $("#honorsText").val());
        myform.append("schoolBackground_schoolBackgroundYearLevel", $("#schoolBackgroundYearLevel").val());
        myform.append("schoolBackground_schoolBackgroundYearAttended", $("#schoolBackgroundYearAttended").val());
        myform.append("favSubject_likedSubjectsCombo", $("#likedSubjectsCombo").val());
        myform.append("favSubject_gradeLikedSubjects", $("#gradeLikedSubjects").val());
        myform.append("dislikedSubject_dislikedSubjectsCombo", $("#dislikedSubjectsCombo").val());
        myform.append("dislikedSubject_gradeDislikedSubjects", $("#gradeDislikedSubjects").val());
        myform.append("leisure_leisureMajorText", $("#leisureMajorText").val());
        myform.append("leisure_leisurePresentEducationText", $("#leisurePresentEducationText").val());
        myform.append("leisure_leisureCourseChoice", $("#leisureCourseChoiceText").val());
        myform.append("leisure_leisureHowDidYouMakeThisChoiceText", $("#leisureHowDidYouMakeThisChoiceText").val());
        myform.append("leisure_leisureSchoolChoiceText", $("#leisureSchoolChoiceText").val());
        myform.append("leisure_leisureHowDidYouComeToThisSchoolText", $("#leisureHowDidYouComeToThisSchoolText").val());
        myform.append("leisure_getInfo", $("input:radio[name='getInfo']").val());
        myform.append("leisure_leisureWhereDidYouGetThisInfo", $("#leisureWhereDidYouGetThisInfo").val());
        myform.append("leisure_leisureFinancialSupportText", $("#leisureFinancialSupportText").val());
        myform.append("leisure_leisureScholarshipText", $("#leisureScholarshipText").val());
        myform.append("leisure_barelyPassedSubjectsCheck", checkboxCheck("#barelyPassedSubjectsCheck"));
        myform.append("leisure_failedSubjectsCheck", checkboxCheck("#failedSubjectsCheck"));
        myform.append("leisure_fearSubjectsCheck", checkboxCheck("#fearSubjectsCheck"));
        myform.append("leisure_hardSubjectsCheck", checkboxCheck("#hardSubjectsCheck"));
        myform.append("leisure_dificultySubjectsCheck", checkboxCheck("#dificultySubjectsCheck"));
        myform.append("leisure_confidentCheck",checkboxCheck("#confidentCheck"));
        myform.append("leisure_leisureRemarksTextModal", $("#noOfRelatives").val());
        myform.append("leisure_leisureRankClass", $("#noOfRelatives").val());
        myform.append("leisure_leisureAverageText", $("#noOfRelatives").val());
        myform.append("healthRecord_healthRecordHeight", $("#healthRecordHeight").val());
        myform.append("healthRecord_healthRecordWeight", $("#healthRecordWeight").val());
        myform.append("healthRecord_healthRecordComplexion", $("#healthRecordComplexion").val());
        myform.append("healthRecord_wearGlasses",  $("input:radio[name='wearGlasses']").val());
        myform.append("healthRecord_healthRecordProgramsParticipated", $("#healthRecordProgramsParticipated").val());
        myform.append("healthRecord_healthRecordPhysicalAilment", $("#healthRecordPhysicalAilment").val());
        myform.append("healthRecord_healthRecordNumPeopleHouse", $("#healthRecordNumPeopleHouse").val());
        myform.append("healthRecord_healthRecordNumPeopleRoom", $("#healthRecordNumPeopleRoom").val());
        myform.append("healthRecord_healthRecordLive", $("#healthRecordLive").val());
        myform.append("healthRecord_makeUpAssistanceSignificantEventsText", $("#makeUpAssistanceSignificantEventsText").val());
        myform.append("healthRecord_makeUpAssistanceHelpNeededText", $("#makeUpAssistanceHelpNeededText").val());
        myform.append("healthRecord_radioPersonality", $("input:radio[name='radioPersonality']").val());
        myform.append('id', $('#studentid').val());
        prependPostAjaxErrorCallback('/student/saveall', myform, function(data){
            new PNotify({
                title: 'Success!',
                text: 'All details saved!',
                type: 'success',
                styling: 'bootstrap3'
            });
            setTimeout(function(){ location.reload(); }, 1000);
        });
    });
});
function validateAll(){
    if(!$("input:radio[name='maritalStatus']").is(":checked")){
        console.log(1);
        return false;
    }
    if($("#noOfRelatives").val()==""){
        console.log(2);
        return false;
    }
    if($("#noOfHelpers").val()==""){
        console.log(3);
        return false;
    }
    if($("#schoolBackgroundNameText").val()==""){
        console.log(4);
        return false;
    }
    if($("#noOfHelpers").val()==""){
        console.log(5);
        return false;
    }
    if($("#schoolBackgroundYearLevel").val()==""){
        console.log(6);
        return false;
    }
    if($("#schoolBackgroundYearAttended").val()==""){
        console.log(7);
        return false;
    }
    if($("#likedSubjectsCombo").val()==""){
        console.log(8);
        return false;
    }
    if($("#gradeLikedSubjects").val()==""){
        console.log(9);
        return false;
    }
    if($("#dislikedSubjectsCombo").val()==""){
        console.log(10);
        return false;
    }
    if($("#gradeDislikedSubjects").val()==""){
        console.log(11);
        return false;
    }
    if($("#leisureMajorText").val()==""){
        console.log(12);
        return false;
    }
    if($("#leisurePresentEducationText").val()==""){
        console.log(13);
        return false;
    }
    if($("#leisureCourseChoiceText").val()==""){
        console.log(14);
        return false;
    }
    if($("#leisureHowDidYouMakeThisChoiceText").val()==""){
        console.log(15);
        return false;
    }
    if($("#leisureSchoolChoiceText").val()==""){
        console.log(16);
        return false;
    }
    if($("#leisureHowDidYouComeToThisSchoolText").val()==""){
        console.log(17);
        return false;
    }
    if(!$("input:radio[name='getInfo']").is(":checked")){
        console.log(18);
        return false;
    }
    if($("#leisureWhereDidYouGetThisInfo").val()==""){
        console.log(19);
        return false;
    }
    if($("#leisureFinancialSupportText").val()==""){
        console.log(20);
        return false;
    }
    if($("#leisureScholarshipText").val()==""){
        console.log(21);
        return false;
    }
    if(!$("#studentLeisureForm input:checkbox").is(":checked")){
        console.log(22);
        return false;
    }
    // if($("#barelyPassedSubjectsCheck").val()==""){
    //     return false;
    // }
    // if($("#failedSubjectsCheck").val()==""){
    //     return false;
    // }
    // if($("#fearSubjectsCheck").val()==""){
    //     return false;
    // }
    // if($("#hardSubjectsCheck").val()==""){
    //     return false;
    // }
    // if($("#dificultySubjectsCheck").val()==""){
    //     return false;
    // }
    // if($("#confidentCheck").val()==""){
    //     return false;
    // }

    if($("#leisureRemarksTextModal").val()==""){
        return false;
    }
    if($("#leisureRankClass").val()==""){
        return false;
    }
    if($("#leisureAverageText").val()==""){
        return false;
    }
    if($("#healthRecordHeight").val()==""){
        return false;
    }
    if($("#healthRecordWeight").val()==""){
        return false;
    }
    if($("#healthRecordComplexion").val()==""){
        return false;
    }
    if(!$("input:radio[name='wearGlasses']").is(":checked")){
        return false;
    }
    if($("#healthRecordProgramsParticipated").val()==""){
        return false;
    }
    if($("#healthRecordPhysicalAilment").val()==""){
        return false;
    }
    if($("#healthRecordNumPeopleHouse").val()==""){
        return false;
    }
    if($("#healthRecordNumPeopleRoom").val()==""){
        return false;
    }
    if($("#healthRecordLive").val()==""){
        return false;
    }
    if($("#makeUpAssistanceSignificantEventsText").val()==""){
        return false;
    }
    if($("#makeUpAssistanceHelpNeededText").val()==""){
        return false;
    }
    if(!$("input:radio[name='radioPersonality']").is(":checked")){
        return false;
    }
    return true;
}
function checkboxCheck(checkboxid){
    if($(checkboxid).attr("checked")){
        return "true";
    }else{
        return "false";
    }
}
document.getElementById('studentImage').onchange = function(e) {
    // Get the first file in the FileList object
    var imageFile = this.files[0];
    // get a local URL representation of the image blob
    var url = window.URL.createObjectURL(imageFile);
    // Now use your newly created URL!
    var someImageTag = document.getElementById('profileImage');
    someImageTag.src = url;
  }