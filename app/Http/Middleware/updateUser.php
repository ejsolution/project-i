<?php

namespace App\Http\Middleware;
use App\model\useraccount;
use Carbon\Carbon;
use Closure;
use Auth;
use Session;
class updateUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       if(Auth::guard('myuser')->id()!= ""){
          
           $user = useraccount::find(Auth::guard('myuser')->id());
           $user->logged= Carbon::now();
           $user->save();
       }
       
        return $next($request);
    }
}
