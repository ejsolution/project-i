<?php
namespace App\Http\Controllers\dashboard;
use App\model\subject;
use App\model\favSubjectList;
use App\model\systemDate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class subjectController extends Controller
{
  public function getSubjects(){
    $subjects = subject::all();
    //$systemdate = systemDate::all();
    //$subjects = subject::where('created_at', '<=', $systemdate[0]->systemDate)->get();
    if(count($subjects)==0){
        return "<h2>There are no subjects yet</h2>";
    }else{
        $html = '';
        foreach ($subjects as $subjects) {
            $html .=$this->subjectRow($subjects);
        }
        return $html;
    }
  }
  public function subjectRow($subject){
      return "<tr id ='subjectRow-$subject->id'>
                <td>$subject->subjectName</td>
                <td>$subject->subjectCode</td>
                <td>$subject->description</td>
                <td><button class='btn btn-dark editSubjectRow' id ='editSubjectRow-$subject->id'><i class='fa fa-edit'></i></button></td>
                <td><button class='btn btn-warning deleteSubjectRow' id ='deleteSubjectRow-$subject->id'><i class='fa fa-trash-o'></i></button></td>  
            </tr>";
  }
  public function subjectInsert(Request $request){
    $this->validate($request, array(
        'subjectCode'=>'required|unique:subjects,subjectCode',
        'subjectNameAdd'=>'required|max:100',
        'subjectDescriptionAdd'=>'required|max:100'
    ));
    $subject = new subject;
    $subject->subjectCode = $request->subjectCode;
    $subject->subjectName = $request->subjectNameAdd;
    $subject->description = $request->subjectDescriptionAdd;
    $subject->save();
    
    $subject->html=$this->subjectRow($subject);
    
    return $subject;
  }
  public function subjectUpdate(Request $request){
    $subject = subject::find($request->id);
    $this->validate($request, array(
        'subjectName'=>'required|max:100',
        'description'=>'required|max:100'
    ));
    //subjectCode
    if($subject->subjectCode != $request->subjectCode){
        $this->validate($request, array(
            'subjectCode'=>'required|unique:subjects,subjectCode',
        ));
        $subject->subjectName = $request->subjectName;
        $subject->description = $request->description;
        $subject->save();
    }else{
        $subject->subjectName = $request->subjectName;
        $subject->subjectCode = $request->subjectCode;
        $subject->description = $request->description;
        $subject->save();
    }
    return "saved!";
  }
  public function deleteSubject($subjectid){
    $count = favSubjectList::where('subjectID', '=', $subjectid)->count();
    if($count >0){
        return "cannotdelete";
    }else{
        $subject = subject::find($subjectid);
        $subject->forceDelete();
        return "deleted";
    }
  }
}
