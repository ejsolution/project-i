@component('components.modal')
  @slot('modalid')
    referralFormModal
  @endslot
  @slot('modalsize')
    modal-lg 
  @endslot
  @slot('modaltitle')
    Referral Form
  @endslot
  @slot('modalcontent')
    <div class='row'>
      <div class='col-md-12'>
        @component('components.x_panel')
          @slot('title')
              Student Information
          @endslot
          @slot('content')
            <div class='row'>
              <div class='col-md-6'>
                <label for = 'studentNameReferralFormText'>Name</label>
                <div class="input-group">
                  <input type="text" id = 'nameOfStudents' name='nameOfStudents' disabled class="form-control">
                  <input type='hidden' name = 'studentID' id ='studentNameReferralFormText'>
                  <span class="input-group-btn">
                    <button type="button" class="btn btn-primary" id ='chooseStudent'><i class='fa fa-plus'></i></button>
                  </span>
                </div>
                {{--  <select id = 'studentNameReferralFormText'
                name = 'studentID' class='form-control'>
                  @foreach ($studentstable as $studentstables)
                    <option value ='{{$studentstables->id}}'>{{$studentstables->firstName}} {{$studentstables->lastName}}</option>
                  @endforeach
                </select>  --}}
              </div>
              <div class='col-md-6'>
                <div class='row'>
                  <div class='col-md-6'>
                    <label for = 'schoolYearReferralFormText'>School Year</label>
                    <input type = 'text' id = 'schoolYearReferralFormText'
                    name = 'schoolYearReferralFormText' disabled class='form-control' value ='{{$schoolyear}}'>
                  </div>
                  <div class='col-md-6'>
                    <label for = 'semesterReferralFormText'>Semester</label>
                    <input type = 'text' id = 'semesterReferralFormText'
                    name = 'semesterReferralFormText' disabled class='form-control' value ='{{$currentsem}}'>
                  </div>
                </div>
              </div>
            </div>
            <div class='row'>
              <div class='col-md-12'>
                <label for = 'homeAddressReferralFormText'>Home Address</label>
                <input type = 'text' id = 'homeAddressReferralFormText'
                name = 'homeAddressReferralFormText' class='form-control' disabled>
              </div>
            </div>
            <div class='row'>
              <div class='col-md-6'>
                <label for = 'courseReferralFormText'>Course</label>
                <input type = 'text' id = 'courseReferralFormText'
                name = 'courseReferralFormText' class='form-control' disabled>
              </div>
              <div class='col-md-6'>
                <label for = 'contactReferralFormText'>Tel/Contact No.</label>
                <input type = 'text' id = 'contactReferralFormText'
                name = 'contactReferralFormText' class='form-control' disabled>
              </div>
            </div>
            <div class='row'>
              <div class='col-md-6'>
                <label for = 'yearLevelReferralFormText'>Year Level/Grade Level</label>
                <input type = 'text' id = 'yearLevelReferralFormText'
                name = 'yearLevelReferralFormText' class='form-control' disabled>
              </div>
              <div class='col-md-6'>
                <label for = 'dateReferralFormText'>Referral Date</label>
                <input type = 'date' id = 'dateReferralFormText'
              name = 'dateReferralFormText' class='form-control' form ='referralAddForm' value ="{{$systemdate}}" disabled>
              </div>
            </div> 
            {!! Form::open([
              'id'=>'referralAddForm']) !!}
            {!! Form::close() !!}
          @endslot
        @endcomponent
      </div>
    </div>
    <div class='row'>
      <div class='col-md-12 well'>
        <div class='genericDivBottom alignRight'>
          <button id = 'addBehavior' class='btn btn-dark'>Add</button>
        </div>
        <div class='row'>
          <div class='col-md-12 tableModalContainer'>
            <table class='table table-striped'>
              <thead>
                <tr>
                  <th>Observable Behaviours</th>
                  <th>Frequency</th>
                  <th>Remarks</th>
                  <th>Remove</th>
                </tr>
              </thead>
              <tbody id ='behaviorReferralTable'>
              </tbody>
            </table>
          </div>
        </div>
        
      </div>
    </div>
    <div class='row'>
      <div class='col-md-6'>
        <div class='row'>
          <div class='col-md-4'>
            <label for = 'referredToReferralFormText'>Referred To:</label>
          </div>
          <div class='col-md-8'>
            {{--  <input type = 'text' id = 'referredToReferralFormText' name ='referredToReferralFormText'
            class='form-control' required form ='referralAddForm'>  --}}
            <div class="input-group">
              <input type="text" id = 'referredToReferralFormText' name='referredToReferralFormText' disabled class="form-control">
              <span class="input-group-btn">
                <button type="button" class="btn btn-primary" id ='chooseStudentReferredTo'><i class='fa fa-plus'></i></button>
              </span>
            </div>
          </div>
        </div>
      </div>
      <div class='col-md-6'>
        <div class='row'>
          <div class='col-md-4'>
              <label for = 'referredByReferralFormText'>Referred By:</label>
              <label for = 'refferedByUniversityCollege'>College:</label>
          </div>
          <div class='col-md-8'>
            <input type = 'text' id = 'referredByReferralFormText' name ='referredByReferralFormText'
            class='form-control' required form ='referralAddForm'>
            <select class='form-control' required form ='referralAddForm' 
              id = 'refferedByUniversityCollege' name = 'unviersityCollege' style = 'margin-top:10px;'>

            </select>
          </div>
        </div>
      </div>
    </div>
  @endslot
  @slot('modalbutton')
      <button class="btn btn-warning" form ='referralAddForm'><i class='fa fa-save'></i>&nbsp;Save</button>
  @endslot
@endcomponent
