<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssignmentCollegesTable extends Migration
{
    /** 
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assignment_colleges', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('collegeID')->unsigned();
            $table->foreign('collegeID')->references('id')->on('colleges');
            $table->integer('userID')->unsigned();
            $table->foreign('userID')->references('id')->on('useraccounts');
            $table->date('dateAssigned');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assignment_colleges');
    }
}
