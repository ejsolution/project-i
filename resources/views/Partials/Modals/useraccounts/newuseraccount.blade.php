@component('components.modal')
  @slot('modalid')
    addUserAccountModal
  @endslot
  @slot('modalsize')
    modal-md
  @endslot
  @slot('modaltitle')
    Add User Account
  @endslot
  @slot('modalcontent')
  {!! Form::open(['id' => 'addEditUserForm',
  'files'=>true]) !!}
    <div class='row'>
      <div class='col-md-6'>
        <label for = 'lastNameUserAccount'>Last Name *</label>
        <input type = 'text' id = 'lastNameUserAccount' name ='lastNameUserAccount'
        class='form-control' required maxlength = '50'>
        <label for = 'firstNameUserAccount'>First Name *</label>
        <input type = 'text' id = 'firstNameUserAccount' name ='firstNameUserAccount'
        class='form-control' required maxlength = '50'>
        <label for = 'emailUserAccount'>Email *</label>
        <input type = 'text' id = 'emailUserAccount' name ='emailUserAccount'
        class='form-control' required maxlength = '50'>
        <label for = 'usernameUserAccount'>Username *</label>
        <input type = 'text' id = 'usernameUserAccount' name ='usernameUserAccount'
        class='form-control' required maxlength = '50'>
        <label for = 'designationUserAccount'>Designation *</label>
        <select id = 'designationUserAccount' name ='designationUserAccount'
        class='form-control'>
          <option value = 'college'>College</option>
          <option value = 'seniorhigh'>Senior High</option>
          <option value = 'juniorhigh'>Junior High</option>
          <option value = 'elementary'>Elementary</option>
        </select>
        <label for='collegesDesignation'>Colleges</label>
        <select id = 'collegesDesignation' name ='collegesDesignation'
        class='form-control'>
          @foreach ($college as $colleges)
            <option value = '{{$colleges->id}}'>{{$colleges->collegeName}}</option>
          @endforeach
        </select>
        <label for = 'designationLevel'>Designation Level *</label>
        <select id = 'designationLevel' name ='designationLevel'
        class='form-control' disabled>
          <option value = 'grade-12'>grade 12</option>
          <option value = 'grade-11'>grade 11</option>
        </select>
        <div class='row genericDivTop'>
          <div class='col-md-12 well'>
            <label for = 'usertype'>Usertype *</label>
            <select class='form-control' id = 'usertype' name = 'usertype' required>
              <option value = 'coordinator'>Coordinator</option>
              <option value = 'staff'>staff</option>
            </select>
          </div>
        </div>
      </div>
      <div class='col-md-6'>
        <img src = '{{asset('images/defaultimage.png')}}'
        class='img-circle profile_img' id ='viewProfileImage'>
        <div class='row'>
          <div class='col-md-12'>
              <input type = 'file' class='form-control' required 
              name = 'imageUserAccount' id = 'imageUserAccount' accept=".png,.jpg,.gif">
          </div>
          <div class='col-md-12'>
            <input type ='button' id = 'removeImageButton' class='btn btn-block btn-danger removeImage' value ='Remove'>
          </div>
        </div>

        <div class='row'>
          <div class='col-md-12'>
            <label for = 'passwordUserAccount'>Password *</label>
            <input type = 'text' id = 'passwordUserAccount' name = 'passwordUserAccount' required maxlength='50' class='form-control'>
          </div>
          <div class='col-md-12 genericDivTop alignRight'>
            <input type='button' id ='editPasswordButton' value = 'changePassword' class='btn btn-dark'>
          </div>
        </div>
        
      </div>
    </div>
    
    {!! Form::close() !!}
  @endslot
  @slot('modalbutton')
    <button class="btn btn-warning" form = 'addEditUserForm' id = 'submitUserAccount'><i class='fa fa-save'></i>&nbsp;Save</button>
  @endslot
@endcomponent
