<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class storeUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
                'usernameUserAccount'=>'required|max:50|unique:useraccounts,userName',
                'passwordUserAccount'=>'required|max:50',
                'firstNameUserAccount'=>'required|max:50',
                'lastNameUserAccount'=>'required|max:50',
                'emailUserAccount'=>'required|max:50|unique:useraccounts,email|email',
                'imageUserAccount' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
                'designationUserAccount'=> 'required',
                'usertype' => 'required' 
       
        ];
    }
}
