<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditStudentTableMakeNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('students', function (Blueprint $table) {
           
           
            $table->string('religion')->nullable()->change();
            $table->string('ethnicity')->nullable()->change();
            $table->string('language')->nullable()->change();
            $table->string('contactNo')->nullable()->change();
            $table->mediumText('cityAddress')->nullable()->change();
            $table->mediumText('provincialAddress')->nullable()->change();
        });
            
           
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
