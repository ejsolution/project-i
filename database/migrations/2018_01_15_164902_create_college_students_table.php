<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCollegeStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('college_students', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('studentID')->unsigned();
            $table->foreign('studentID')->references('id')->on('students');
            $table->string('studentNo');
            $table->string('yearLevel');
            $table->date('dateEnrolled');
            $table->integer('courseID')->unsigned();
            $table->foreign('courseID')->references('id')->on('courses');
            $table->integer('semesterID')->unsigned();
            $table->foreign('semesterID')->references('id')->on('semesters');
            $table->integer('schoolYearID')->unsigned();
            $table->foreign('schoolYearID')->references('id')->on('school_years');
            $table->integer('organizationID')->unsigned();
            $table->foreign('organizationID')->references('id')->on('organizations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('college_students');
    }
}
