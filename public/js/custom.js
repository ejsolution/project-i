function init_sidebar() {
    var e=function() {
        $RIGHT_COL.css("min-height", $(window).height());
        var e=$BODY.outerHeight(),
        a=$BODY.hasClass("footer_fixed")?-10: $FOOTER.height(), t=$LEFT_COL.eq(1).height()+$SIDEBAR_FOOTER.height(), o=t>e?t: e;
        o-=$NAV_MENU.height()+a,
        $RIGHT_COL.css("min-height", o)
    }
    ;
    $SIDEBAR_MENU.find("a").on("click", function(a) {
        var t=$(this).parent();
        t.is(".active")?(t.removeClass("active active-sm"), $("ul:first", t).slideUp(function() {
            e()
        }
        )):(t.parent().is(".child_menu")?$BODY.is(".nav-sm")&&($SIDEBAR_MENU.find("li").removeClass("active active-sm"), $SIDEBAR_MENU.find("li ul").slideUp()):($SIDEBAR_MENU.find("li").removeClass("active active-sm"), $SIDEBAR_MENU.find("li ul").slideUp()), t.addClass("active"), $("ul:first", t).slideDown(function() {
            e()
        }
        ))
    }
    ),
    $MENU_TOGGLE.on("click", function() {
        $BODY.hasClass("nav-md")?($SIDEBAR_MENU.find("li.active ul").hide(), $SIDEBAR_MENU.find("li.active").addClass("active-sm").removeClass("active")): ($SIDEBAR_MENU.find("li.active-sm ul").show(), $SIDEBAR_MENU.find("li.active-sm").addClass("active").removeClass("active-sm")), $BODY.toggleClass("nav-md nav-sm"), e()
    }
    ),
    $SIDEBAR_MENU.find('a[href="'+CURRENT_URL+'"]').parent("li").addClass("current-page"),
    $SIDEBAR_MENU.find("a").filter(function() {
        return this.href==CURRENT_URL
    }
    ).parent("li").addClass("current-page").parents("ul").slideDown(function() {
        e()
    }
    ).parent().addClass("active"),
    $(window).smartresize(function() {
        e()
    }
    ),
    e(),
    $.fn.mCustomScrollbar&&$(".menu_fixed").mCustomScrollbar( {
        autoHideScrollbar:!0, theme:"minimal", mouseWheel: {
            preventDefault: !0
        }
    }
    )
}

function countChecked() {
    "all"===checkState&&$(".bulk_action input[name='table_records']").iCheck("check"),
    "none"===checkState&&$(".bulk_action input[name='table_records']").iCheck("uncheck");
    var e=$(".bulk_action input[name='table_records']:checked").length;
    e?($(".column-title").hide(), $(".bulk-actions").show(), $(".action-cnt").html(e+" Records Selected")): ($(".column-title").show(), $(".bulk-actions").hide())
}

function gd(e, a, t) {
    return new Date(e, a-1, t).getTime()
}

function init_flot_chart() {
    if("undefined"!=typeof $.plot) {
        console.log("init_flot_chart");
        for(var e=[[gd(2012, 1, 1), 17], [gd(2012, 1, 2), 74], [gd(2012, 1, 3), 6], [gd(2012, 1, 4), 39], [gd(2012, 1, 5), 20], [gd(2012, 1, 6), 85], [gd(2012, 1, 7), 7]], a=[[gd(2012, 1, 1), 82], [gd(2012, 1, 2), 23], [gd(2012, 1, 3), 66], [gd(2012, 1, 4), 9], [gd(2012, 1, 5), 119], [gd(2012, 1, 6), 6], [gd(2012, 1, 7), 9]], t=[], o=[[0, 1], [1, 9], [2, 6], [3, 10], [4, 5], [5, 17], [6, 6], [7, 10], [8, 7], [9, 11], [10, 35], [11, 9], [12, 12], [13, 5], [14, 3], [15, 4], [16, 9]], n=0;
        30>n;
        n++)t.push([new Date(Date.today().add(n).days()).getTime(), randNum()+n+n+10]);
        var i= {
            series: {
                lines: {
                    show: !1, fill: !0
                }
                ,
                splines: {
                    show: !0, tension: .4, lineWidth: 1, fill: .4
                }
                ,
                points: {
                    radius: 0, show: !0
                }
                ,
                shadowSize:2
            }
            ,
            grid: {
                verticalLines: !0, hoverable: !0, clickable: !0, tickColor: "#d5d5d5", borderWidth: 1, color: "#fff"
            }
            ,
            colors:["rgba(38, 185, 154, 0.38)",
            "rgba(3, 88, 106, 0.38)"],
            xaxis: {
                tickColor: "rgba(51, 51, 51, 0.06)", mode: "time", tickSize: [1, "day"], axisLabel: "Date", axisLabelUseCanvas: !0, axisLabelFontSizePixels: 12, axisLabelFontFamily: "Verdana, Arial", axisLabelPadding: 10
            }
            ,
            yaxis: {
                ticks: 8, tickColor: "rgba(51, 51, 51, 0.06)"
            }
            ,
            tooltip:!1
        }
        ,
        r= {
            grid: {
                show: !0, aboveData: !0, color: "#3f3f3f", labelMargin: 10, axisMargin: 0, borderWidth: 0, borderColor: null, minBorderMargin: 5, clickable: !0, hoverable: !0, autoHighlight: !0, mouseActiveRadius: 100
            }
            ,
            series: {
                lines: {
                    show: !0, fill: !0, lineWidth: 2, steps: !1
                }
                ,
                points: {
                    show: !0, radius: 4.5, symbol: "circle", lineWidth: 3
                }
            }
            ,
            legend: {
                position:"ne",
                margin:[0,
                -25],
                noColumns:0,
                labelBoxBorderColor:null,
                labelFormatter:function(e, a) {
                    return e+"&nbsp;&nbsp;"
                }
                ,
                width:40,
                height:1
            }
            ,
            colors:["#96CA59",
            "#3F97EB",
            "#72c380",
            "#6f7a8a",
            "#f7cb38",
            "#5a8022",
            "#2c7282"],
            shadowSize:0,
            tooltip:!0,
            tooltipOpts: {
                content:"%s: %y.0",
                xDateFormat:"%d/%m",
                shifts: {
                    x: -30, y: -50
                }
                ,
                defaultTheme:!1
            }
            ,
            yaxis: {
                min: 0
            }
            ,
            xaxis: {
                mode: "time", minTickSize: [1, "day"], timeformat: "%d/%m/%y", min: t[0][0], max: t[20][0]
            }
        }
        ,
        l= {
            series: {
                curvedLines: {
                    apply: !0, active: !0, monotonicFit: !0
                }
            }
            ,
            colors:["#26B99A"],
            grid: {
                borderWidth: {
                    top: 0, right: 0, bottom: 1, left: 1
                }
                ,
                borderColor: {
                    bottom: "#7F8790", left: "#7F8790"
                }
            }
        }
        ;
        $("#chart_plot_01").length&&(console.log("Plot1"), $.plot($("#chart_plot_01"), [e, a], i)),
        $("#chart_plot_02").length&&(console.log("Plot2"), $.plot($("#chart_plot_02"), [ {
            label:"Email Sent", data:t, lines: {
                fillColor: "rgba(150, 202, 89, 0.12)"
            }
            , points: {
                fillColor: "#fff"
            }
        }
        ], r)),
        $("#chart_plot_03").length&&(console.log("Plot3"), $.plot($("#chart_plot_03"), [ {
            label:"Registrations", data:o, lines: {
                fillColor: "rgba(150, 202, 89, 0.12)"
            }
            , points: {
                fillColor: "#fff"
            }
        }
        ], l))
    }
}

function init_starrr() {
    "undefined"!=typeof starrr&&(console.log("init_starrr"), $(".stars").starrr(), $(".stars-existing").starrr( {
        rating: 4
    }
    ), $(".stars").on("starrr:change", function(e, a) {
        $(".stars-count").html(a)
    }
    ), $(".stars-existing").on("starrr:change", function(e, a) {
        $(".stars-count-existing").html(a)
    }
    ))
}

function init_JQVmap() {
    "undefined"!=typeof jQuery.fn.vectorMap&&(console.log("init_JQVmap"), $("#world-map-gdp").length&&$("#world-map-gdp").vectorMap( {
        map: "world_en", backgroundColor: null, color: "#ffffff", hoverOpacity: .7, selectedColor: "#666666", enableZoom: !0, showTooltip: !0, values: sample_data, scaleColors: ["#E6F2F0", "#149B7E"], normalizeFunction: "polynomial"
    }
    ), $("#usa_map").length&&$("#usa_map").vectorMap( {
        map: "usa_en", backgroundColor: null, color: "#ffffff", hoverOpacity: .7, selectedColor: "#666666", enableZoom: !0, showTooltip: !0, values: sample_data, scaleColors: ["#E6F2F0", "#149B7E"], normalizeFunction: "polynomial"
    }
    ))
}

function init_skycons() {
    if("undefined"!=typeof Skycons) {
        console.log("init_skycons");
        var e,
        a=new Skycons( {
            color: "#73879C"
        }
        ),
        t=["clear-day",
        "clear-night",
        "partly-cloudy-day",
        "partly-cloudy-night",
        "cloudy",
        "rain",
        "sleet",
        "snow",
        "wind",
        "fog"];
        for(e=t.length;
        e--;
        )a.set(t[e], t[e]);
        a.play()
    }
}

function init_chart_doughnut() {
    if("undefined"!=typeof Chart&&(console.log("init_chart_doughnut"), $(".canvasDoughnut").length)) {
        var e= {
            type:"doughnut",
            tooltipFillColor:"rgba(51, 51, 51, 0.55)",
            data: {
                labels:["Symbian",
                "Blackberry",
                "Other",
                "Android",
                "IOS"],
                datasets:[ {
                    data: [15, 20, 30, 10, 30], backgroundColor: ["#BDC3C7", "#9B59B6", "#E74C3C", "#26B99A", "#3498DB"], hoverBackgroundColor: ["#CFD4D8", "#B370CF", "#E95E4F", "#36CAAB", "#49A9EA"]
                }
                ]
            }
            ,
            options: {
                legend: !1, responsive: !1
            }
        }
        ;
        $(".canvasDoughnut").each(function() {
            var a=$(this);
            new Chart(a, e)
        }
        )
    }
}

function init_gauge() {
    if("undefined"!=typeof Gauge) {
        console.log("init_gauge ["+$(".gauge-chart").length+"]"),
        console.log("init_gauge");
        var e= {
            lines:12,
            angle:0,
            lineWidth:.4,
            pointer: {
                length: .75, strokeWidth: .042, color: "#1D212A"
            }
            ,
            limitMax:"false",
            colorStart:"#1ABC9C",
            colorStop:"#1ABC9C",
            strokeColor:"#F0F3F3",
            generateGradient:!0
        }
        ;
        if($("#chart_gauge_01").length)var a=document.getElementById("chart_gauge_01"),
        t=new Gauge(a).setOptions(e);
        if($("#gauge-text").length&&(t.maxValue=6e3, t.animationSpeed=32, t.set(3200), t.setTextField(document.getElementById("gauge-text"))), $("#chart_gauge_02").length)var o=document.getElementById("chart_gauge_02"),
        n=new Gauge(o).setOptions(e);
        $("#gauge-text2").length&&(n.maxValue=9e3, n.animationSpeed=32, n.set(2400), n.setTextField(document.getElementById("gauge-text2")))
    }
}

function init_sparklines() {
    "undefined"!=typeof jQuery.fn.sparkline&&(console.log("init_sparklines"), $(".sparkline_one").sparkline([2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 5, 6, 4, 5, 6, 3, 5, 4, 5, 4, 5, 4, 3, 4, 5, 6, 7, 5, 4, 3, 5, 6], {
        type:"bar", height:"125", barWidth:13, colorMap: {
            7: "#a1a1a1"
        }
        , barSpacing:2, barColor:"#26B99A"
    }
    ), $(".sparkline_two").sparkline([2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 5, 6, 7, 5, 4, 3, 5, 6], {
        type:"bar", height:"40", barWidth:9, colorMap: {
            7: "#a1a1a1"
        }
        , barSpacing:2, barColor:"#26B99A"
    }
    ), $(".sparkline_three").sparkline([2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 5, 6, 7, 5, 4, 3, 5, 6], {
        type: "line", width: "200", height: "40", lineColor: "#26B99A", fillColor: "rgba(223, 223, 223, 0.57)", lineWidth: 2, spotColor: "#26B99A", minSpotColor: "#26B99A"
    }
    ), $(".sparkline11").sparkline([2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 6, 2, 4, 3, 4, 5, 4, 5, 4, 3], {
        type:"bar", height:"40", barWidth:8, colorMap: {
            7: "#a1a1a1"
        }
        , barSpacing:2, barColor:"#26B99A"
    }
    ), $(".sparkline22").sparkline([2, 4, 3, 4, 7, 5, 4, 3, 5, 6, 2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 6], {
        type: "line", height: "40", width: "200", lineColor: "#26B99A", fillColor: "#ffffff", lineWidth: 3, spotColor: "#34495E", minSpotColor: "#34495E"
    }
    ), $(".sparkline_bar").sparkline([2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 5, 6, 4, 5, 6, 3, 5], {
        type:"bar", colorMap: {
            7: "#a1a1a1"
        }
        , barColor:"#26B99A"
    }
    ), $(".sparkline_area").sparkline([5, 6, 7, 9, 9, 5, 3, 2, 2, 4, 6, 7], {
        type: "line", lineColor: "#26B99A", fillColor: "#26B99A", spotColor: "#4578a0", minSpotColor: "#728fb2", maxSpotColor: "#6d93c4", highlightSpotColor: "#ef5179", highlightLineColor: "#8ba8bf", spotRadius: 2.5, width: 85
    }
    ), $(".sparkline_line").sparkline([2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 5, 6, 4, 5, 6, 3, 5], {
        type: "line", lineColor: "#26B99A", fillColor: "#ffffff", width: 85, spotColor: "#34495E", minSpotColor: "#34495E"
    }
    ), $(".sparkline_pie").sparkline([1, 1, 2, 1], {
        type: "pie", sliceColors: ["#26B99A", "#ccc", "#75BCDD", "#D66DE2"]
    }
    ), $(".sparkline_discreet").sparkline([4, 6, 7, 7, 4, 3, 2, 1, 4, 4, 2, 4, 3, 7, 8, 9, 7, 6, 4, 3], {
        type: "discrete", barWidth: 3, lineColor: "#26B99A", width: "85"
    }
    ))
}

function init_autocomplete() {
    if("undefined"!=typeof autocomplete) {
        console.log("init_autocomplete");
        var e= {
            AD: "Andorra", A2: "Andorra Test", AE: "United Arab Emirates", AF: "Afghanistan", AG: "Antigua and Barbuda", AI: "Anguilla", AL: "Albania", AM: "Armenia", AN: "Netherlands Antilles", AO: "Angola", AQ: "Antarctica", AR: "Argentina", AS: "American Samoa", AT: "Austria", AU: "Australia", AW: "Aruba", AX: "Åland Islands", AZ: "Azerbaijan", BA: "Bosnia and Herzegovina", BB: "Barbados", BD: "Bangladesh", BE: "Belgium", BF: "Burkina Faso", BG: "Bulgaria", BH: "Bahrain", BI: "Burundi", BJ: "Benin", BL: "Saint Barthélemy", BM: "Bermuda", BN: "Brunei", BO: "Bolivia", BQ: "British Antarctic Territory", BR: "Brazil", BS: "Bahamas", BT: "Bhutan", BV: "Bouvet Island", BW: "Botswana", BY: "Belarus", BZ: "Belize", CA: "Canada", CC: "Cocos [Keeling] Islands", CD: "Congo - Kinshasa", CF: "Central African Republic", CG: "Congo - Brazzaville", CH: "Switzerland", CI: "Côte d’Ivoire", CK: "Cook Islands", CL: "Chile", CM: "Cameroon", CN: "China", CO: "Colombia", CR: "Costa Rica", CS: "Serbia and Montenegro", CT: "Canton and Enderbury Islands", CU: "Cuba", CV: "Cape Verde", CX: "Christmas Island", CY: "Cyprus", CZ: "Czech Republic", DD: "East Germany", DE: "Germany", DJ: "Djibouti", DK: "Denmark", DM: "Dominica", DO: "Dominican Republic", DZ: "Algeria", EC: "Ecuador", EE: "Estonia", EG: "Egypt", EH: "Western Sahara", ER: "Eritrea", ES: "Spain", ET: "Ethiopia", FI: "Finland", FJ: "Fiji", FK: "Falkland Islands", FM: "Micronesia", FO: "Faroe Islands", FQ: "French Southern and Antarctic Territories", FR: "France", FX: "Metropolitan France", GA: "Gabon", GB: "United Kingdom", GD: "Grenada", GE: "Georgia", GF: "French Guiana", GG: "Guernsey", GH: "Ghana", GI: "Gibraltar", GL: "Greenland", GM: "Gambia", GN: "Guinea", GP: "Guadeloupe", GQ: "Equatorial Guinea", GR: "Greece", GS: "South Georgia and the South Sandwich Islands", GT: "Guatemala", GU: "Guam", GW: "Guinea-Bissau", GY: "Guyana", HK: "Hong Kong SAR China", HM: "Heard Island and McDonald Islands", HN: "Honduras", HR: "Croatia", HT: "Haiti", HU: "Hungary", ID: "Indonesia", IE: "Ireland", IL: "Israel", IM: "Isle of Man", IN: "India", IO: "British Indian Ocean Territory", IQ: "Iraq", IR: "Iran", IS: "Iceland", IT: "Italy", JE: "Jersey", JM: "Jamaica", JO: "Jordan", JP: "Japan", JT: "Johnston Island", KE: "Kenya", KG: "Kyrgyzstan", KH: "Cambodia", KI: "Kiribati", KM: "Comoros", KN: "Saint Kitts and Nevis", KP: "North Korea", KR: "South Korea", KW: "Kuwait", KY: "Cayman Islands", KZ: "Kazakhstan", LA: "Laos", LB: "Lebanon", LC: "Saint Lucia", LI: "Liechtenstein", LK: "Sri Lanka", LR: "Liberia", LS: "Lesotho", LT: "Lithuania", LU: "Luxembourg", LV: "Latvia", LY: "Libya", MA: "Morocco", MC: "Monaco", MD: "Moldova", ME: "Montenegro", MF: "Saint Martin", MG: "Madagascar", MH: "Marshall Islands", MI: "Midway Islands", MK: "Macedonia", ML: "Mali", MM: "Myanmar [Burma]", MN: "Mongolia", MO: "Macau SAR China", MP: "Northern Mariana Islands", MQ: "Martinique", MR: "Mauritania", MS: "Montserrat", MT: "Malta", MU: "Mauritius", MV: "Maldives", MW: "Malawi", MX: "Mexico", MY: "Malaysia", MZ: "Mozambique", NA: "Namibia", NC: "New Caledonia", NE: "Niger", NF: "Norfolk Island", NG: "Nigeria", NI: "Nicaragua", NL: "Netherlands", NO: "Norway", NP: "Nepal", NQ: "Dronning Maud Land", NR: "Nauru", NT: "Neutral Zone", NU: "Niue", NZ: "New Zealand", OM: "Oman", PA: "Panama", PC: "Pacific Islands Trust Territory", PE: "Peru", PF: "French Polynesia", PG: "Papua New Guinea", PH: "Philippines", PK: "Pakistan", PL: "Poland", PM: "Saint Pierre and Miquelon", PN: "Pitcairn Islands", PR: "Puerto Rico", PS: "Palestinian Territories", PT: "Portugal", PU: "U.S. Miscellaneous Pacific Islands", PW: "Palau", PY: "Paraguay", PZ: "Panama Canal Zone", QA: "Qatar", RE: "Réunion", RO: "Romania", RS: "Serbia", RU: "Russia", RW: "Rwanda", SA: "Saudi Arabia", SB: "Solomon Islands", SC: "Seychelles", SD: "Sudan", SE: "Sweden", SG: "Singapore", SH: "Saint Helena", SI: "Slovenia", SJ: "Svalbard and Jan Mayen", SK: "Slovakia", SL: "Sierra Leone", SM: "San Marino", SN: "Senegal", SO: "Somalia", SR: "Suriname", ST: "São Tomé and Príncipe", SU: "Union of Soviet Socialist Republics", SV: "El Salvador", SY: "Syria", SZ: "Swaziland", TC: "Turks and Caicos Islands", TD: "Chad", TF: "French Southern Territories", TG: "Togo", TH: "Thailand", TJ: "Tajikistan", TK: "Tokelau", TL: "Timor-Leste", TM: "Turkmenistan", TN: "Tunisia", TO: "Tonga", TR: "Turkey", TT: "Trinidad and Tobago", TV: "Tuvalu", TW: "Taiwan", TZ: "Tanzania", UA: "Ukraine", UG: "Uganda", UM: "U.S. Minor Outlying Islands", US: "United States", UY: "Uruguay", UZ: "Uzbekistan", VA: "Vatican City", VC: "Saint Vincent and the Grenadines", VD: "North Vietnam", VE: "Venezuela", VG: "British Virgin Islands", VI: "U.S. Virgin Islands", VN: "Vietnam", VU: "Vanuatu", WF: "Wallis and Futuna", WK: "Wake Island", WS: "Samoa", YD: "People's Democratic Republic of Yemen", YE: "Yemen", YT: "Mayotte", ZA: "South Africa", ZM: "Zambia", ZW: "Zimbabwe", ZZ: "Unknown or Invalid Region"
        }
        ,
        a=$.map(e, function(e, a) {
            return {
                value: e, data: a
            }
        }
        );
        $("#autocomplete-custom-append").autocomplete( {
            lookup: a
        }
        )
    }
}

function init_autosize() {
    "undefined"!=typeof $.fn.autosize&&autosize($(".resizable_textarea"))
}

function init_parsley() {
    if("undefined"!=typeof parsley) {
        console.log("init_parsley"),
        $("parsley:field:validate", function() {
            e()
        }
        ),
        $("#demo-form .btn").on("click", function() {
            $("#demo-form").parsley().validate(), e()
        }
        );
        var e=function() {
            !0===$("#demo-form").parsley().isValid()?($(".bs-callout-info").removeClass("hidden"), $(".bs-callout-warning").addClass("hidden")): ($(".bs-callout-info").addClass("hidden"), $(".bs-callout-warning").removeClass("hidden"))
        }
        ;
        $("parsley:field:validate", function() {
            e()
        }
        ),
        $("#demo-form2 .btn").on("click", function() {
            $("#demo-form2").parsley().validate(), e()
        }
        );
        var e=function() {
            !0===$("#demo-form2").parsley().isValid()?($(".bs-callout-info").removeClass("hidden"), $(".bs-callout-warning").addClass("hidden")): ($(".bs-callout-info").addClass("hidden"), $(".bs-callout-warning").removeClass("hidden"))
        }
        ;
        try {
            hljs.initHighlightingOnLoad()
        }
        catch(e) {}
    }
}

function onAddTag(e) {
    alert("Added a tag: "+e)
}

function onRemoveTag(e) {
    alert("Removed a tag: "+e)
}

function onChangeTag(e, a) {
    alert("Changed a tag: "+a)
}

function init_TagsInput() {
    "undefined"!=typeof $.fn.tagsInput&&$("#tags_1").tagsInput( {
        width: "auto"
    }
    )
}

function init_select2() {
    "undefined"!=typeof select2&&(console.log("init_toolbox"), $(".select2_single").select2( {
        placeholder: "Select a state", allowClear: !0
    }
    ), $(".select2_group").select2( {}
    ), $(".select2_multiple").select2( {
        maximumSelectionLength: 4, placeholder: "With Max Selection limit 4", allowClear: !0
    }
    ))
}

function init_wysiwyg() {
    function e(e, a) {
        var t="";
        "unsupported-file-type"===e?t="Unsupported format "+a: console.log("error uploading file", e, a), $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button><strong>File upload error</strong> '+t+" </div>").prependTo("#alerts")
    }
    "undefined"!=typeof $.fn.wysiwyg&&(console.log("init_wysiwyg"), $(".editor-wrapper").each(function() {
        var a=$(this).attr("id");
        $(this).wysiwyg( {
            toolbarSelector: '[data-target="#'+a+'"]', fileUploadError: e
        }
        )
    }
    ), window.prettyPrint, prettyPrint())
}

function init_cropper() {
    if("undefined"!=typeof $.fn.cropper) {
        console.log("init_cropper");
        var e=$("#image"),
        a=$("#download"),
        t=$("#dataX"),
        o=$("#dataY"),
        n=$("#dataHeight"),
        i=$("#dataWidth"),
        r=$("#dataRotate"),
        l=$("#dataScaleX"),
        s=$("#dataScaleY"),
        d= {
            aspectRatio:16/9,
            preview:".img-preview",
            crop:function(e) {
                t.val(Math.round(e.x)),
                o.val(Math.round(e.y)),
                n.val(Math.round(e.height)),
                i.val(Math.round(e.width)),
                r.val(e.rotate),
                l.val(e.scaleX),
                s.val(e.scaleY)
            }
        }
        ;
        $('[data-toggle="tooltip"]').tooltip(),
        e.on( {
            "build.cropper":function(e) {
                console.log(e.type)
            }
            , "built.cropper":function(e) {
                console.log(e.type)
            }
            , "cropstart.cropper":function(e) {
                console.log(e.type, e.action)
            }
            , "cropmove.cropper":function(e) {
                console.log(e.type, e.action)
            }
            , "cropend.cropper":function(e) {
                console.log(e.type, e.action)
            }
            , "crop.cropper":function(e) {
                console.log(e.type, e.x, e.y, e.width, e.height, e.rotate, e.scaleX, e.scaleY)
            }
            , "zoom.cropper":function(e) {
                console.log(e.type, e.ratio)
            }
        }
        ).cropper(d),
        $.isFunction(document.createElement("canvas").getContext)||$('button[data-method="getCroppedCanvas"]').prop("disabled", !0),
        "undefined"==typeof document.createElement("cropper").style.transition&&($('button[data-method="rotate"]').prop("disabled", !0), $('button[data-method="scale"]').prop("disabled", !0)),
        "undefined"==typeof a[0].download&&a.addClass("disabled"),
        $(".docs-toggles").on("change", "input", function() {
            var a, t, o=$(this), n=o.attr("name"), i=o.prop("type");
            e.data("cropper")&&("checkbox"===i?(d[n]=o.prop("checked"), a=e.cropper("getCropBoxData"), t=e.cropper("getCanvasData"), d.built=function() {
                e.cropper("setCropBoxData", a), e.cropper("setCanvasData", t)
            }
            ):"radio"===i&&(d[n]=o.val()), e.cropper("destroy").cropper(d))
        }
        ),
        $(".docs-buttons").on("click", "[data-method]", function() {
            var e, t, o=$(this), n=o.data();
            if(!o.prop("disabled")&&!o.hasClass("disabled")&&i.data("cropper")&&n.method) {
                if(n=$.extend( {}
                , n), "undefined"!=typeof n.target&&(e=$(n.target), "undefined"==typeof n.option))try {
                    n.option=JSON.parse(e.val())
                }
                catch(i) {
                    console.log(i.message)
                }
                switch(t=i.cropper(n.method, n.option, n.secondOption), n.method) {
                    case"scaleX": case"scaleY": $(this).data("option", -n.option);
                    break;
                    case"getCroppedCanvas": t&&($("#getCroppedCanvasModal").modal().find(".modal-body").html(t), a.hasClass("disabled")||a.attr("href", t.toDataURL()))
                }
                if($.isPlainObject(t)&&e)try {
                    e.val(JSON.stringify(t))
                }
                catch(i) {
                    console.log(i.message)
                }
            }
        }
        ),
        $(document.body).on("keydown",
        function(a) {
            if(e.data("cropper")&&!(this.scrollTop>300))switch(a.which) {
                case 37: a.preventDefault(), e.cropper("move", -1, 0);
                break;
                case 38: a.preventDefault(), e.cropper("move", 0, -1);
                break;
                case 39: a.preventDefault(), e.cropper("move", 1, 0);
                break;
                case 40: a.preventDefault(), e.cropper("move", 0, 1)
            }
        }
        );
        var c,
        u=$("#inputImage"),
        m=window.URL||window.webkitURL;
        m?u.change(function() {
            var a,
            t=this.files;
            e.data("cropper")&&t&&t.length&&(a=t[0], /^image\/\w+$/.test(a.type)?(c=m.createObjectURL(a), e.one("built.cropper", function() {
                m.revokeObjectURL(c)
            }
            ).cropper("reset").cropper("replace", c), u.val("")):window.alert("Please choose an image file."))
        }
        ):u.prop("disabled",
        !0).parent().addClass("disabled")
    }
}

function init_knob() {
    if("undefined"!=typeof $.fn.knob) {
        console.log("init_knob"),
        $(".knob").knob( {
            change:function(e) {}
            ,
            release:function(e) {
                console.log("release : "+e)
            }
            ,
            cancel:function() {
                console.log("cancel : ", this)
            }
            ,
            draw:function() {
                if("tron"==this.$.data("skin")) {
                    this.cursorExt=.3;
                    var e,
                    a=this.arc(this.cv),
                    t=1;
                    return this.g.lineWidth=this.lineWidth,
                    this.o.displayPrevious&&(e=this.arc(this.v), this.g.beginPath(), this.g.strokeStyle=this.pColor, this.g.arc(this.xy, this.xy, this.radius-this.lineWidth, e.s, e.e, e.d), this.g.stroke()),
                    this.g.beginPath(),
                    this.g.strokeStyle=t?this.o.fgColor: this.fgColor, this.g.arc(this.xy, this.xy, this.radius-this.lineWidth, a.s, a.e, a.d), this.g.stroke(), this.g.lineWidth=2, this.g.beginPath(), this.g.strokeStyle=this.o.fgColor, this.g.arc(this.xy, this.xy, this.radius-this.lineWidth+1+2*this.lineWidth/3, 0, 2*Math.PI, !1), this.g.stroke(), !1
                }
            }
        }
        );
        var e,
        a=0,
        t=0,
        o=0,
        n=$("div.idir"),
        i=$("div.ival"),
        r=function() {
            o++,
            n.show().html("+").fadeOut(),
            i.html(o)
        }
        ,
        l=function() {
            o--,
            n.show().html("-").fadeOut(),
            i.html(o)
        }
        ;
        $("input.infinite").knob( {
            min:0,
            max:20,
            stopper:!1,
            change:function() {
                e>this.cv?a?(l(), a=0): (a=1, t=0): e<this.cv&&(t?(r(), t=0): (t=1, a=0)), e=this.cv
            }
        }
        )
    }
}

function init_InputMask() {
    "undefined"!=typeof $.fn.inputmask&&(console.log("init_InputMask"),
    $(":input").inputmask())
}

function init_ColorPicker() {
    "undefined"!=typeof $.fn.colorpicker&&(console.log("init_ColorPicker"),
    $(".demo1").colorpicker(),
    $(".demo2").colorpicker(),
    $("#demo_forceformat").colorpicker( {
        format: "rgba", horizontal: !0
    }
    ),
    $("#demo_forceformat3").colorpicker( {
        format: "rgba"
    }
    ),
    $(".demo-auto").colorpicker())
}

function init_IonRangeSlider() {
    "undefined"!=typeof $.fn.ionRangeSlider&&(console.log("init_IonRangeSlider"),
    $("#range_27").ionRangeSlider( {
        type: "double", min: 1e6, max: 2e6, grid: !0, force_edges: !0
    }
    ),
    $("#range").ionRangeSlider( {
        hide_min_max: !0, keyboard: !0, min: 0, max: 5e3, from: 1e3, to: 4e3, type: "double", step: 1, prefix: "$", grid: !0
    }
    ),
    $("#range_25").ionRangeSlider( {
        type: "double", min: 1e6, max: 2e6, grid: !0
    }
    ),
    $("#range_26").ionRangeSlider( {
        type: "double", min: 0, max: 1e4, step: 500, grid: !0, grid_snap: !0
    }
    ),
    $("#range_31").ionRangeSlider( {
        type: "double", min: 0, max: 100, from: 30, to: 70, from_fixed: !0
    }
    ),
    $(".range_min_max").ionRangeSlider( {
        type: "double", min: 0, max: 100, from: 30, to: 70, max_interval: 50
    }
    ),
    $(".range_time24").ionRangeSlider( {
        min:+moment().subtract(12, "hours").format("X"), max:+moment().format("X"), from:+moment().subtract(6, "hours").format("X"), grid:!0, force_edges:!0, prettify:function(e) {
            var a=moment(e, "X");
            return a.format("Do MMMM, HH:mm")
        }
    }
    ))
}

function init_daterangepicker() {
    if("undefined"!=typeof $.fn.daterangepicker) {
        console.log("init_daterangepicker");
        var e=function(e,
        a,
        t) {
            console.log(e.toISOString(),
            a.toISOString(),
            t),
            $("#reportrange span").html(e.format("MMMM D, YYYY")+" - "+a.format("MMMM D, YYYY"))
        }
        ,
        a= {
            startDate:moment().subtract(29,
            "days"),
            endDate:moment(),
            minDate:"01/01/2012",
            maxDate:"12/31/2015",
            dateLimit: {
                days: 60
            }
            ,
            showDropdowns:!0,
            showWeekNumbers:!0,
            timePicker:!1,
            timePickerIncrement:1,
            timePicker12Hour:!0,
            ranges: {
                Today: [moment(), moment()], Yesterday: [moment().subtract(1, "days"), moment().subtract(1, "days")], "Last 7 Days": [moment().subtract(6, "days"), moment()], "Last 30 Days": [moment().subtract(29, "days"), moment()], "This Month": [moment().startOf("month"), moment().endOf("month")], "Last Month": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")]
            }
            ,
            opens:"left",
            buttonClasses:["btn btn-default"],
            applyClass:"btn-small btn-primary",
            cancelClass:"btn-small",
            format:"MM/DD/YYYY",
            separator:" to ",
            locale: {
                applyLabel: "Submit", cancelLabel: "Clear", fromLabel: "From", toLabel: "To", customRangeLabel: "Custom", daysOfWeek: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"], monthNames: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"], firstDay: 1
            }
        }
        ;
        $("#reportrange span").html(moment().subtract(29, "days").format("MMMM D, YYYY")+" - "+moment().format("MMMM D, YYYY")),
        $("#reportrange").daterangepicker(a,
        e),
        $("#reportrange").on("show.daterangepicker",
        function() {
            console.log("show event fired")
        }
        ),
        $("#reportrange").on("hide.daterangepicker",
        function() {
            console.log("hide event fired")
        }
        ),
        $("#reportrange").on("apply.daterangepicker",
        function(e, a) {
            console.log("apply event fired, start/end dates are "+a.startDate.format("MMMM D, YYYY")+" to "+a.endDate.format("MMMM D, YYYY"))
        }
        ),
        $("#reportrange").on("cancel.daterangepicker",
        function(e, a) {
            console.log("cancel event fired")
        }
        ),
        $("#options1").click(function() {
            $("#reportrange").data("daterangepicker").setOptions(a, e)
        }
        ),
        $("#options2").click(function() {
            $("#reportrange").data("daterangepicker").setOptions(optionSet2, e)
        }
        ),
        $("#destroy").click(function() {
            $("#reportrange").data("daterangepicker").remove()
        }
        )
    }
}

function init_daterangepicker_right() {
    if("undefined"!=typeof $.fn.daterangepicker) {
        console.log("init_daterangepicker_right");
        var e=function(e,
        a,
        t) {
            console.log(e.toISOString(),
            a.toISOString(),
            t),
            $("#reportrange_right span").html(e.format("MMMM D, YYYY")+" - "+a.format("MMMM D, YYYY"))
        }
        ,
        a= {
            startDate:moment().subtract(29,
            "days"),
            endDate:moment(),
            minDate:"01/01/2012",
            maxDate:"12/31/2020",
            dateLimit: {
                days: 60
            }
            ,
            showDropdowns:!0,
            showWeekNumbers:!0,
            timePicker:!1,
            timePickerIncrement:1,
            timePicker12Hour:!0,
            ranges: {
                Today: [moment(), moment()], Yesterday: [moment().subtract(1, "days"), moment().subtract(1, "days")], "Last 7 Days": [moment().subtract(6, "days"), moment()], "Last 30 Days": [moment().subtract(29, "days"), moment()], "This Month": [moment().startOf("month"), moment().endOf("month")], "Last Month": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")]
            }
            ,
            opens:"right",
            buttonClasses:["btn btn-default"],
            applyClass:"btn-small btn-primary",
            cancelClass:"btn-small",
            format:"MM/DD/YYYY",
            separator:" to ",
            locale: {
                applyLabel: "Submit", cancelLabel: "Clear", fromLabel: "From", toLabel: "To", customRangeLabel: "Custom", daysOfWeek: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"], monthNames: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"], firstDay: 1
            }
        }
        ;
        $("#reportrange_right span").html(moment().subtract(29, "days").format("MMMM D, YYYY")+" - "+moment().format("MMMM D, YYYY")),
        $("#reportrange_right").daterangepicker(a,
        e),
        $("#reportrange_right").on("show.daterangepicker",
        function() {
            console.log("show event fired")
        }
        ),
        $("#reportrange_right").on("hide.daterangepicker",
        function() {
            console.log("hide event fired")
        }
        ),
        $("#reportrange_right").on("apply.daterangepicker",
        function(e, a) {
            console.log("apply event fired, start/end dates are "+a.startDate.format("MMMM D, YYYY")+" to "+a.endDate.format("MMMM D, YYYY"))
        }
        ),
        $("#reportrange_right").on("cancel.daterangepicker",
        function(e, a) {
            console.log("cancel event fired")
        }
        ),
        $("#options1").click(function() {
            $("#reportrange_right").data("daterangepicker").setOptions(a, e)
        }
        ),
        $("#options2").click(function() {
            $("#reportrange_right").data("daterangepicker").setOptions(optionSet2, e)
        }
        ),
        $("#destroy").click(function() {
            $("#reportrange_right").data("daterangepicker").remove()
        }
        )
    }
}

function init_daterangepicker_single_call() {
    "undefined"!=typeof $.fn.daterangepicker&&(console.log("init_daterangepicker_single_call"),
    $("#single_cal1").daterangepicker( {
        singleDatePicker: !0, singleClasses: "picker_1"
    }
    , function(e, a, t) {
        console.log(e.toISOString(), a.toISOString(), t)
    }
    ),
    $("#single_cal2").daterangepicker( {
        singleDatePicker: !0, singleClasses: "picker_2"
    }
    , function(e, a, t) {
        console.log(e.toISOString(), a.toISOString(), t)
    }
    ),
    $("#single_cal3").daterangepicker( {
        singleDatePicker: !0, singleClasses: "picker_3"
    }
    , function(e, a, t) {
        console.log(e.toISOString(), a.toISOString(), t)
    }
    ),
    $("#single_cal4").daterangepicker( {
        singleDatePicker: !0, singleClasses: "picker_4"
    }
    , function(e, a, t) {
        console.log(e.toISOString(), a.toISOString(), t)
    }
    ))
}

function init_daterangepicker_reservation() {
    "undefined"!=typeof $.fn.daterangepicker&&(console.log("init_daterangepicker_reservation"),
    $("#reservation").daterangepicker(null, function(e, a, t) {
        console.log(e.toISOString(), a.toISOString(), t)
    }
    ),
    $("#reservation-time").daterangepicker( {
        timePicker:!0, timePickerIncrement:30, locale: {
            format: "MM/DD/YYYY h:mm A"
        }
    }
    ))
}

function init_SmartWizard() {
    "undefined"!=typeof $.fn.smartWizard&&(console.log("init_SmartWizard"),
    $("#wizard").smartWizard(),
    $("#wizard_verticle").smartWizard( {
        transitionEffect: "slide"
    }
    ),
    $(".buttonNext").addClass("btn btn-success"),
    $(".buttonPrevious").addClass("btn btn-primary"),
    $(".buttonFinish").addClass("btn btn-default"))
}

function init_validator() {
    "undefined"!=typeof validator&&(console.log("init_validator"),
    validator.message.date="not a real date",
    $("form").on("blur", "input[required], input.optional, select.required", validator.checkField).on("change", "select.required", validator.checkField).on("keypress", "input[required][pattern]", validator.keypress),
    $(".multi.required").on("keyup blur", "input", function() {
        validator.checkField.apply($(this).siblings().last()[0])
    }
    ),
    $("form").submit(function(e) {
        e.preventDefault();
        var a=!0;
        return validator.checkAll($(this))||(a=!1), a&&this.submit(), !1
    }
    ))
}

function init_PNotify() {
    "undefined"!=typeof PNotify&&(console.log("init_PNotify"),
    new PNotify( {
        title:"PNotify", type:"info", text:"Welcome. Try hovering over me. You can click things behind me, because I'm non-blocking.", nonblock: {
            nonblock: !0
        }
        , addclass:"dark", styling:"bootstrap3", hide:!1, before_close:function(e) {
            return e.update( {
                title: e.options.title+" - Enjoy your Stay", before_close: null
            }
            ), e.queueRemove(), !1
        }
    }
    ))
}

function init_CustomNotification() {
    if(console.log("run_customtabs"),
    "undefined"!=typeof CustomTabs) {
        console.log("init_CustomTabs");
        var e=10;
        TabbedNotification=function(a) {
            var t="<div id='ntf"+e+"' class='text alert-"+a.type+"' style='display:none'><h2><i class='fa fa-bell'></i> "+a.title+"</h2><div class='close'><a href='javascript:;' class='notification_close'><i class='fa fa-close'></i></a></div><p>"+a.text+"</p></div>";
            document.getElementById("custom_notifications")?($("#custom_notifications ul.notifications").append("<li><a id='ntlink"+e+"' class='alert-"+a.type+"' href='#ntf"+e+"'><i class='fa fa-bell animated shake'></i></a></li>"),
            $("#custom_notifications #notif-group").append(t),
            e++,
            CustomTabs(a)): alert("doesnt exists")
        }
        ,
        CustomTabs=function(e) {
            $(".tabbed_notifications > div").hide(),
            $(".tabbed_notifications > div:first-of-type").show(),
            $("#custom_notifications").removeClass("dsp_none"),
            $(".notifications a").click(function(e) {
                e.preventDefault();
                var a=$(this),
                t="#"+a.parents(".notifications").data("tabbed_notifications"),
                o=a.closest("li").siblings().children("a"),
                n=a.attr("href");
                o.removeClass("active"),
                a.addClass("active"),
                $(t).children("div").hide(),
                $(n).show()
            }
            )
        }
        ,
        CustomTabs();
        var a=idname="";
        $(document).on("click",
        ".notification_close",
        function(e) {
            idname=$(this).parent().parent().attr("id"),
            a=idname.substr(-2),
            $("#ntf"+a).remove(),
            $("#ntlink"+a).parent().remove(),
            $(".notifications a").first().addClass("active"),
            $("#notif-group div").first().css("display", "block")
        }
        )
    }
}

function init_EasyPieChart() {
    if("undefined"!=typeof $.fn.easyPieChart) {
        console.log("init_EasyPieChart"),
        $(".chart").easyPieChart( {
            easing:"easeOutElastic",
            delay:3e3,
            barColor:"#26B99A",
            trackColor:"#fff",
            scaleColor:!1,
            lineWidth:20,
            trackWidth:16,
            lineCap:"butt",
            onStep:function(e, a, t) {
                $(this.el).find(".percent").text(Math.round(t))
            }
        }
        );
        var e=window.chart=$(".chart").data("easyPieChart");
        $(".js_update").on("click",
        function() {
            e.update(200*Math.random()-100)
        }
        );
        var a=$.fn.popover.Constructor.prototype.leave;
        $.fn.popover.Constructor.prototype.leave=function(e) {
            var t,
            o,
            n=e instanceof this.constructor?e: $(e.currentTarget)[this.type](this.getDelegateOptions()).data("bs."+this.type);
            a.call(this,
            e),
            e.currentTarget&&(t=$(e.currentTarget).siblings(".popover"),
            o=n.timeout,
            t.one("mouseenter", function() {
                clearTimeout(o), t.one("mouseleave", function() {
                    $.fn.popover.Constructor.prototype.leave.call(n, n)
                }
                )
            }
            ))
        }
        ,
        $("body").popover( {
            selector:"[data-popover]",
            trigger:"click hover",
            delay: {
                show: 50, hide: 400
            }
        }
        )
    }
}

function init_charts() {
    if(console.log("run_charts  typeof ["+typeof Chart+"]"),
    "undefined"!=typeof Chart) {
        if(console.log("init_charts"),
        Chart.defaults.global.legend= {
            enabled: !1
        }
        ,
        $("#canvas_line").length&&new Chart(document.getElementById("canvas_line"), {
            type:"line", data: {
                labels:["January", "February", "March", "April", "May", "June", "July"], datasets:[ {
                    label: "My First dataset", backgroundColor: "rgba(38, 185, 154, 0.31)", borderColor: "rgba(38, 185, 154, 0.7)", pointBorderColor: "rgba(38, 185, 154, 0.7)", pointBackgroundColor: "rgba(38, 185, 154, 0.7)", pointHoverBackgroundColor: "#fff", pointHoverBorderColor: "rgba(220,220,220,1)", pointBorderWidth: 1, data: [31, 74, 6, 39, 20, 85, 7]
                }
                , {
                    label: "My Second dataset", backgroundColor: "rgba(3, 88, 106, 0.3)", borderColor: "rgba(3, 88, 106, 0.70)", pointBorderColor: "rgba(3, 88, 106, 0.70)", pointBackgroundColor: "rgba(3, 88, 106, 0.70)", pointHoverBackgroundColor: "#fff", pointHoverBorderColor: "rgba(151,187,205,1)", pointBorderWidth: 1, data: [82, 23, 66, 9, 99, 4, 2]
                }
                ]
            }
        }
        ),
        $("#canvas_line1").length&&new Chart(document.getElementById("canvas_line1"), {
            type:"line", data: {
                labels:["January", "February", "March", "April", "May", "June", "July"], datasets:[ {
                    label: "My First dataset", backgroundColor: "rgba(38, 185, 154, 0.31)", borderColor: "rgba(38, 185, 154, 0.7)", pointBorderColor: "rgba(38, 185, 154, 0.7)", pointBackgroundColor: "rgba(38, 185, 154, 0.7)", pointHoverBackgroundColor: "#fff", pointHoverBorderColor: "rgba(220,220,220,1)", pointBorderWidth: 1, data: [31, 74, 6, 39, 20, 85, 7]
                }
                , {
                    label: "My Second dataset", backgroundColor: "rgba(3, 88, 106, 0.3)", borderColor: "rgba(3, 88, 106, 0.70)", pointBorderColor: "rgba(3, 88, 106, 0.70)", pointBackgroundColor: "rgba(3, 88, 106, 0.70)", pointHoverBackgroundColor: "#fff", pointHoverBorderColor: "rgba(151,187,205,1)", pointBorderWidth: 1, data: [82, 23, 66, 9, 99, 4, 2]
                }
                ]
            }
        }
        ),
        $("#canvas_line2").length&&new Chart(document.getElementById("canvas_line2"), {
            type:"line", data: {
                labels:["January", "February", "March", "April", "May", "June", "July"], datasets:[ {
                    label: "My First dataset", backgroundColor: "rgba(38, 185, 154, 0.31)", borderColor: "rgba(38, 185, 154, 0.7)", pointBorderColor: "rgba(38, 185, 154, 0.7)", pointBackgroundColor: "rgba(38, 185, 154, 0.7)", pointHoverBackgroundColor: "#fff", pointHoverBorderColor: "rgba(220,220,220,1)", pointBorderWidth: 1, data: [31, 74, 6, 39, 20, 85, 7]
                }
                , {
                    label: "My Second dataset", backgroundColor: "rgba(3, 88, 106, 0.3)", borderColor: "rgba(3, 88, 106, 0.70)", pointBorderColor: "rgba(3, 88, 106, 0.70)", pointBackgroundColor: "rgba(3, 88, 106, 0.70)", pointHoverBackgroundColor: "#fff", pointHoverBorderColor: "rgba(151,187,205,1)", pointBorderWidth: 1, data: [82, 23, 66, 9, 99, 4, 2]
                }
                ]
            }
        }
        ),
        $("#canvas_line3").length&&new Chart(document.getElementById("canvas_line3"), {
            type:"line", data: {
                labels:["January", "February", "March", "April", "May", "June", "July"], datasets:[ {
                    label: "My First dataset", backgroundColor: "rgba(38, 185, 154, 0.31)", borderColor: "rgba(38, 185, 154, 0.7)", pointBorderColor: "rgba(38, 185, 154, 0.7)", pointBackgroundColor: "rgba(38, 185, 154, 0.7)", pointHoverBackgroundColor: "#fff", pointHoverBorderColor: "rgba(220,220,220,1)", pointBorderWidth: 1, data: [31, 74, 6, 39, 20, 85, 7]
                }
                , {
                    label: "My Second dataset", backgroundColor: "rgba(3, 88, 106, 0.3)", borderColor: "rgba(3, 88, 106, 0.70)", pointBorderColor: "rgba(3, 88, 106, 0.70)", pointBackgroundColor: "rgba(3, 88, 106, 0.70)", pointHoverBackgroundColor: "#fff", pointHoverBorderColor: "rgba(151,187,205,1)", pointBorderWidth: 1, data: [82, 23, 66, 9, 99, 4, 2]
                }
                ]
            }
        }
        ),
        $("#canvas_line4").length&&new Chart(document.getElementById("canvas_line4"), {
            type:"line", data: {
                labels:["January", "February", "March", "April", "May", "June", "July"], datasets:[ {
                    label: "My First dataset", backgroundColor: "rgba(38, 185, 154, 0.31)", borderColor: "rgba(38, 185, 154, 0.7)", pointBorderColor: "rgba(38, 185, 154, 0.7)", pointBackgroundColor: "rgba(38, 185, 154, 0.7)", pointHoverBackgroundColor: "#fff", pointHoverBorderColor: "rgba(220,220,220,1)", pointBorderWidth: 1, data: [31, 74, 6, 39, 20, 85, 7]
                }
                , {
                    label: "My Second dataset", backgroundColor: "rgba(3, 88, 106, 0.3)", borderColor: "rgba(3, 88, 106, 0.70)", pointBorderColor: "rgba(3, 88, 106, 0.70)", pointBackgroundColor: "rgba(3, 88, 106, 0.70)", pointHoverBackgroundColor: "#fff", pointHoverBorderColor: "rgba(151,187,205,1)", pointBorderWidth: 1, data: [82, 23, 66, 9, 99, 4, 2]
                }
                ]
            }
        }
        ),
        $("#lineChart").length) {
            var e=document.getElementById("lineChart");
            new Chart(e,
            {
                type:"line",
                data: {
                    labels:["January",
                    "February",
                    "March",
                    "April",
                    "May",
                    "June",
                    "July"],
                    datasets:[ {
                        label: "My First dataset", backgroundColor: "rgba(38, 185, 154, 0.31)", borderColor: "rgba(38, 185, 154, 0.7)", pointBorderColor: "rgba(38, 185, 154, 0.7)", pointBackgroundColor: "rgba(38, 185, 154, 0.7)", pointHoverBackgroundColor: "#fff", pointHoverBorderColor: "rgba(220,220,220,1)", pointBorderWidth: 1, data: [31, 74, 6, 39, 20, 85, 7]
                    }
                    ,
                    {
                        label: "My Second dataset", backgroundColor: "rgba(3, 88, 106, 0.3)", borderColor: "rgba(3, 88, 106, 0.70)", pointBorderColor: "rgba(3, 88, 106, 0.70)", pointBackgroundColor: "rgba(3, 88, 106, 0.70)", pointHoverBackgroundColor: "#fff", pointHoverBorderColor: "rgba(151,187,205,1)", pointBorderWidth: 1, data: [82, 23, 66, 9, 99, 4, 2]
                    }
                    ]
                }
            }
            )
        }
        if($("#mybarChart").length) {
            var e=document.getElementById("mybarChart");
            new Chart(e,
            {
                type:"bar",
                data: {
                    labels:["January",
                    "February",
                    "March",
                    "April",
                    "May",
                    "June",
                    "July"],
                    datasets:[ {
                        label: "# of Votes", backgroundColor: "#26B99A", data: [51, 30, 40, 28, 92, 50, 45]
                    }
                    ,
                    {
                        label: "# of Votes", backgroundColor: "#03586A", data: [41, 56, 25, 48, 72, 34, 12]
                    }
                    ]
                }
                ,
                options: {
                    scales: {
                        yAxes:[ {
                            ticks: {
                                beginAtZero: !0
                            }
                        }
                        ]
                    }
                }
            }
            )
        }
        if($("#canvasDoughnut").length) {
            var e=document.getElementById("canvasDoughnut"),
            a= {
                labels:["Dark Grey",
                "Purple Color",
                "Gray Color",
                "Green Color",
                "Blue Color"],
                datasets:[ {
                    data: [120, 50, 140, 180, 100], backgroundColor: ["#455C73", "#9B59B6", "#BDC3C7", "#26B99A", "#3498DB"], hoverBackgroundColor: ["#34495E", "#B370CF", "#CFD4D8", "#36CAAB", "#49A9EA"]
                }
                ]
            }
            ;
            new Chart(e,
            {
                type: "doughnut", tooltipFillColor: "rgba(51, 51, 51, 0.55)", data: a
            }
            )
        }
        if($("#canvasRadar").length) {
            var e=document.getElementById("canvasRadar"),
            a= {
                labels:["Eating",
                "Drinking",
                "Sleeping",
                "Designing",
                "Coding",
                "Cycling",
                "Running"],
                datasets:[ {
                    label: "My First dataset", backgroundColor: "rgba(3, 88, 106, 0.2)", borderColor: "rgba(3, 88, 106, 0.80)", pointBorderColor: "rgba(3, 88, 106, 0.80)", pointBackgroundColor: "rgba(3, 88, 106, 0.80)", pointHoverBackgroundColor: "#fff", pointHoverBorderColor: "rgba(220,220,220,1)", data: [65, 59, 90, 81, 56, 55, 40]
                }
                ,
                {
                    label: "My Second dataset", backgroundColor: "rgba(38, 185, 154, 0.2)", borderColor: "rgba(38, 185, 154, 0.85)", pointColor: "rgba(38, 185, 154, 0.85)", pointStrokeColor: "#fff", pointHighlightFill: "#fff", pointHighlightStroke: "rgba(151,187,205,1)", data: [28, 48, 40, 19, 96, 27, 100]
                }
                ]
            }
            ;
            new Chart(e,
            {
                type: "radar", data: a
            }
            )
        }
        if($("#pieChart").length) {
            var e=document.getElementById("pieChart"),
            a= {
                datasets:[ {
                    data: [120, 50, 140, 180, 100], backgroundColor: ["#455C73", "#9B59B6", "#BDC3C7", "#26B99A", "#3498DB"], label: "My dataset"
                }
                ],
                labels:["Dark Gray",
                "Purple",
                "Gray",
                "Green",
                "Blue"]
            }
            ;
            new Chart(e,
            {
                data:a,
                type:"pie",
                otpions: {
                    legend: !1
                }
            }
            )
        }
        if($("#polarArea").length) {
            var e=document.getElementById("polarArea"),
            a= {
                datasets:[ {
                    data: [120, 50, 140, 180, 100], backgroundColor: ["#455C73", "#9B59B6", "#BDC3C7", "#26B99A", "#3498DB"], label: "My dataset"
                }
                ],
                labels:["Dark Gray",
                "Purple",
                "Gray",
                "Green",
                "Blue"]
            }
            ;
            new Chart(e,
            {
                data:a,
                type:"polarArea",
                options: {
                    scale: {
                        ticks: {
                            beginAtZero: !0
                        }
                    }
                }
            }
            )
        }
    }
}

function init_compose() {
    "undefined"!=typeof $.fn.slideToggle&&(console.log("init_compose"),
    $("#compose, .compose-close").click(function() {
        $(".compose").slideToggle()
    }
    ))
}

function init_DataTables() {
    if(console.log("run_datatables"),
    "undefined"!=typeof $.fn.DataTable) {
        console.log("init_DataTables");
        var e=function() {
            $("#datatable-buttons").length&&$("#datatable-buttons").DataTable( {
                dom:"Blfrtip",
                buttons:[ {
                    extend: "copy", className: "btn-sm"
                }
                ,
                {
                    extend: "csv", className: "btn-sm"
                }
                ,
                {
                    extend: "excel", className: "btn-sm"
                }
                ,
                {
                    extend: "pdfHtml5", className: "btn-sm"
                }
                ,
                {
                    extend: "print", className: "btn-sm"
                }
                ],
                responsive:!0
            }
            )
        }
        ;
        TableManageButtons=function() {
            "use strict";
            return {
                init:function() {
                    e()
                }
            }
        }
        (),
        $("#datatable").dataTable(),
        $("#datatable-keytable").DataTable( {
            keys: !0
        }
        ),
        $("#datatable-responsive").DataTable(),
        $("#datatable-scroller").DataTable( {
            ajax: "js/datatables/json/scroller-demo.json", deferRender: !0, scrollY: 380, scrollCollapse: !0, scroller: !0
        }
        ),
        $("#datatable-fixed-header").DataTable( {
            fixedHeader: !0
        }
        );
        var a=$("#datatable-checkbox");
        a.dataTable( {
            order:[[1,
            "asc"]],
            columnDefs:[ {
                orderable: !1, targets: [0]
            }
            ]
        }
        ),
        a.on("draw.dt",
        function() {
            $("checkbox input").iCheck( {
                checkboxClass: "icheckbox_flat-green"
            }
            )
        }
        ),
        TableManageButtons.init()
    }
}

function init_morris_charts() {
    "undefined"!=typeof Morris&&(console.log("init_morris_charts"),
    $("#graph_bar").length&&Morris.Bar( {
        element:"graph_bar", data:[ {
            device: "iPhone 4", geekbench: 380
        }
        , {
            device: "iPhone 4S", geekbench: 655
        }
        , {
            device: "iPhone 3GS", geekbench: 275
        }
        , {
            device: "iPhone 5", geekbench: 1571
        }
        , {
            device: "iPhone 5S", geekbench: 655
        }
        , {
            device: "iPhone 6", geekbench: 2154
        }
        , {
            device: "iPhone 6 Plus", geekbench: 1144
        }
        , {
            device: "iPhone 6S", geekbench: 2371
        }
        , {
            device: "iPhone 6S Plus", geekbench: 1471
        }
        , {
            device: "Other", geekbench: 1371
        }
        ], xkey:"device", ykeys:["geekbench"], labels:["Geekbench"], barRatio:.4, barColors:["#26B99A", "#34495E", "#ACADAC", "#3498DB"], xLabelAngle:35, hideHover:"auto", resize:!0
    }
    ),
    $("#graph_bar_group").length&&Morris.Bar( {
        element:"graph_bar_group", data:[ {
            period: "2016-10-01", licensed: 807, sorned: 660
        }
        , {
            period: "2016-09-30", licensed: 1251, sorned: 729
        }
        , {
            period: "2016-09-29", licensed: 1769, sorned: 1018
        }
        , {
            period: "2016-09-20", licensed: 2246, sorned: 1461
        }
        , {
            period: "2016-09-19", licensed: 2657, sorned: 1967
        }
        , {
            period: "2016-09-18", licensed: 3148, sorned: 2627
        }
        , {
            period: "2016-09-17", licensed: 3471, sorned: 3740
        }
        , {
            period: "2016-09-16", licensed: 2871, sorned: 2216
        }
        , {
            period: "2016-09-15", licensed: 2401, sorned: 1656
        }
        , {
            period: "2016-09-10", licensed: 2115, sorned: 1022
        }
        ], xkey:"period", barColors:["#26B99A", "#34495E", "#ACADAC", "#3498DB"], ykeys:["licensed", "sorned"], labels:["Licensed", "SORN"], hideHover:"auto", xLabelAngle:60, resize:!0
    }
    ),
    $("#graphx").length&&Morris.Bar( {
        element:"graphx", data:[ {
            x: "2015 Q1", y: 2, z: 3, a: 4
        }
        , {
            x: "2015 Q2", y: 3, z: 5, a: 6
        }
        , {
            x: "2015 Q3", y: 4, z: 3, a: 2
        }
        , {
            x: "2015 Q4", y: 2, z: 4, a: 5
        }
        ], xkey:"x", ykeys:["y", "z", "a"], barColors:["#26B99A", "#34495E", "#ACADAC", "#3498DB"], hideHover:"auto", labels:["Y", "Z", "A"], resize:!0
    }
    ).on("click", function(e, a) {
        console.log(e, a)
    }
    ),
    $("#graph_area").length&&Morris.Area( {
        element:"graph_area", data:[ {
            period: "2014 Q1", iphone: 2666, ipad: null, itouch: 2647
        }
        , {
            period: "2014 Q2", iphone: 2778, ipad: 2294, itouch: 2441
        }
        , {
            period: "2014 Q3", iphone: 4912, ipad: 1969, itouch: 2501
        }
        , {
            period: "2014 Q4", iphone: 3767, ipad: 3597, itouch: 5689
        }
        , {
            period: "2015 Q1", iphone: 6810, ipad: 1914, itouch: 2293
        }
        , {
            period: "2015 Q2", iphone: 5670, ipad: 4293, itouch: 1881
        }
        , {
            period: "2015 Q3", iphone: 4820, ipad: 3795, itouch: 1588
        }
        , {
            period: "2015 Q4", iphone: 15073, ipad: 5967, itouch: 5175
        }
        , {
            period: "2016 Q1", iphone: 10687, ipad: 4460, itouch: 2028
        }
        , {
            period: "2016 Q2", iphone: 8432, ipad: 5713, itouch: 1791
        }
        ], xkey:"period", ykeys:["iphone", "ipad", "itouch"], lineColors:["#26B99A", "#34495E", "#ACADAC", "#3498DB"], labels:["iPhone", "iPad", "iPod Touch"], pointSize:2, hideHover:"auto", resize:!0
    }
    ),
    $("#graph_donut").length&&Morris.Donut( {
        element:"graph_donut", data:[ {
            label: "Jam", value: 25
        }
        , {
            label: "Frosted", value: 40
        }
        , {
            label: "Custard", value: 25
        }
        , {
            label: "Sugar", value: 10
        }
        ], colors:["#26B99A", "#34495E", "#ACADAC", "#3498DB"], formatter:function(e) {
            return e+"%"
        }
        , resize:!0
    }
    ),
    $("#graph_line").length&&(Morris.Line( {
        element:"graph_line", xkey:"year", ykeys:["value"], labels:["Value"], hideHover:"auto", lineColors:["#26B99A", "#34495E", "#ACADAC", "#3498DB"], data:[ {
            year: "2012", value: 20
        }
        , {
            year: "2013", value: 10
        }
        , {
            year: "2014", value: 5
        }
        , {
            year: "2015", value: 5
        }
        , {
            year: "2016", value: 20
        }
        ], resize:!0
    }
    ), $MENU_TOGGLE.on("click", function() {
        $(window).resize()
    }
    )))
}



!function(e,
a) {
    var t=function(e,
    a,
    t) {
        var o;
        return function() {
            function n() {
                t||e.apply(i,
                r),
                o=null
            }
            var i=this,
            r=arguments;
            o?clearTimeout(o):t&&e.apply(i,
            r),
            o=setTimeout(n,
            a||100)
        }
    }
    ;
    jQuery.fn[a]=function(e) {
        return e?this.bind("resize",
        t(e)): this.trigger(a)
    }
}

(jQuery,
"smartresize");
var CURRENT_URL=window.location.href.split("#")[0].split("?")[0],
$BODY=$("body"),
$MENU_TOGGLE=$("#menu_toggle"),
$SIDEBAR_MENU=$("#sidebar-menu"),
$SIDEBAR_FOOTER=$(".sidebar-footer"),
$LEFT_COL=$(".left_col"),
$RIGHT_COL=$(".right_col"),
$NAV_MENU=$(".nav_menu"),
$FOOTER=$("footer"),
randNum=function() {
    return Math.floor(21*Math.random())+20
}

;
$(document).ready(function() {
    $(".collapse-link").on("click", function() {
        var e=$(this).closest(".x_panel"), a=$(this).find("i"), t=e.find(".x_content");
        e.attr("style")?t.slideToggle(200, function() {
            e.removeAttr("style")
        }
        ):(t.slideToggle(200), e.css("height", "auto")), a.toggleClass("fa-chevron-up fa-chevron-down")
    }
    ),
    $(".close-link").click(function() {
        var e=$(this).closest(".x_panel");
        e.remove()
    }
    )
}

),
$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip( {
        container: "body"
    }
    )
}

),
$(".progress .progress-bar")[0]&&$(".progress .progress-bar").progressbar(),
$(document).ready(function() {
    if($(".js-switch")[0]) {
        var e=Array.prototype.slice.call(document.querySelectorAll(".js-switch"));
        e.forEach(function(e) {
            new Switchery(e, {
                color: "#26B99A"
            }
            )
        }
        )
    }
}

),
$(document).ready(function() {
    $("input.flat")[0]&&$(document).ready(function() {
        $("input.flat").iCheck( {
            checkboxClass: "icheckbox_flat-green", radioClass: "iradio_flat-green"
        }
        )
    }
    )
}

),
$("table input").on("ifChecked",
function() {
    checkState="",
    $(this).parent().parent().parent().addClass("selected"),
    countChecked()
}

),
$("table input").on("ifUnchecked",
function() {
    checkState="",
    $(this).parent().parent().parent().removeClass("selected"),
    countChecked()
}

);
var checkState="";
$(".bulk_action input").on("ifChecked",
function() {
    checkState="",
    $(this).parent().parent().parent().addClass("selected"),
    countChecked()
}

),
$(".bulk_action input").on("ifUnchecked",
function() {
    checkState="",
    $(this).parent().parent().parent().removeClass("selected"),
    countChecked()
}

),
$(".bulk_action input#check-all").on("ifChecked",
function() {
    checkState="all",
    countChecked()
}

),
$(".bulk_action input#check-all").on("ifUnchecked",
function() {
    checkState="none",
    countChecked()
}

),
$(document).ready(function() {
    $(".expand").on("click", function() {
        $(this).next().slideToggle(200), $expand=$(this).find(">:first-child"), "+"==$expand.text()?$expand.text("-"): $expand.text("+")
    }
    )
}

),
"undefined"!=typeof NProgress&&($(document).ready(function() {
    NProgress.start()
}

),
$(window).load(function() {
    NProgress.done()
}

));
var originalLeave=$.fn.popover.Constructor.prototype.leave;
$.fn.popover.Constructor.prototype.leave=function(e) {
    var a,
    t,
    o=e instanceof this.constructor?e: $(e.currentTarget)[this.type](this.getDelegateOptions()).data("bs."+this.type);
    originalLeave.call(this,
    e),
    e.currentTarget&&(a=$(e.currentTarget).siblings(".popover"),
    t=o.timeout,
    a.one("mouseenter", function() {
        clearTimeout(t), a.one("mouseleave", function() {
            $.fn.popover.Constructor.prototype.leave.call(o, o)
        }
        )
    }
    ))
}

,
$("body").popover( {
    selector:"[data-popover]",
    trigger:"click hover",
    delay: {
        show: 50, hide: 400
    }
}

),
$(document).ready(function() {
    init_sparklines(),
    init_flot_chart(),
    init_sidebar(),
    init_wysiwyg(),
    init_InputMask(),
    init_JQVmap(),
    init_cropper(),
    init_knob(),
    init_IonRangeSlider(),
    init_ColorPicker(),
    init_TagsInput(),

    init_daterangepicker(),
    init_daterangepicker_right(),
    init_daterangepicker_single_call(),
    init_daterangepicker_reservation(),
    init_SmartWizard(),
    init_EasyPieChart(),
    init_charts(),
    init_morris_charts(),
    init_skycons(),
    init_select2(),
    init_validator(),
    init_DataTables(),
    init_chart_doughnut(),
    init_gauge(),
  
    init_starrr(),
    init_compose(),
    init_CustomNotification(),
    init_autosize(),
    init_autocomplete()
}

);
