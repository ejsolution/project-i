@extends('admin.adminTemplate')

@section('title', 'AdminDashboard')
@section('stylesheet')
	<link href="{{asset('css/admin/dashboard/collegeStudents.css')}}" rel="stylesheet">
	<link href="{{asset('css/admin/dashboard/dashboard.css')}}" rel="stylesheet">
@stop
@section('content')
	<div class='container-fluid'>
    <div class='row'>
      <div class='col-md-12'>
        <h1>List Of College Students</h1>
      </div>
    </div>
    <div class='row'>
      <div class='col-md-12 well tableContainer'>
        @component('components.headerSearchFilter', ['user'=>$user,
                                                    'collegeOfUser'=>$collegeOfUser])
          @slot('comboLabel')
            View By Student Type
          @endslot
          @slot('schoolyears')
            @foreach($schoolyears as $year)
              @if($year->status == '1')
                <option value ='{{$year->id}}'>{{explode('-', $year->schoolYearStart)[0]}} - {{explode('-', $year->schoolYearEnd)[0]}} (Active)</option>
              @else
                <option value ='{{$year->id}}'>{{explode('-', $year->schoolYearStart)[0]}} - {{explode('-', $year->schoolYearEnd)[0]}}</option>
              @endif
            @endforeach
          @endslot
          @slot('searchID')
            searchCollegeStudent
          @endslot
          @slot('placeholder')
            Name
          @endslot
          @slot('button')
           
          @endslot
        @endcomponent
        <div class='row'>
          <div class='col-md-12'>
            <table class='table table-condensed'>
              <thead> 
                <tr>
                  <th>No</th>
                  <th>Student No.</th>
                  <th>Name</th>
                  
                  <th>Course</th>
                  <th>Year Level</th>
                  <th>School Year & Semester</th>
                </tr>
              </thead>
              <tbody id ='collegeStudentsTable'>
              </tbody>
            </table>
          </div>
        </div>

      </div>
      <div class='row'>
        <div class='col-md-10'>
        </div>
        <div class='col-md-1'>
          {{--  <button class='btn btn-danger'><i class='fa fa-print'></i>Print</button>  --}}
        </div>
        <div class='col-md-1'>
          <a class='btn btn-info' href = '{{route('admin.home')}}'>Back</a>
        </div>
      </div>
    </div>

	</div>

@endSection
@section('scripts')
  <script src = '{{asset('js/dashboard/collegeStudents.js')}}'></script>
@stop