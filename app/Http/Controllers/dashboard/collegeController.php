<?php

namespace App\Http\Controllers\dashboard;
use App\model\college;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\model\department;
use App\model\departmentCollegeHist;
use App\model\course;
use App\model\courseDepartmentHists;
use App\model\collegeStudent;
use App\Http\Controllers\activityController;
use App\model\assignmentCollege;
use App\model\systemDate;
class collegeController extends Controller
{
  private $department;
  private $request;
  private $course;
  public function __construct(){
    $this->middleware('auth:myuser');
  }
  public function getCollegeList(){
    //$systemdate = systemDate::all();
    $college=college::orderBy('collegeName', 'asc')->get();
    //$college=college::where('created_at', '<=', $systemdate[0]->systemDate)->get();
    if(count($college)==0){
      return "<h2>No colleges yet.</h2>";
    }else{
      $html="";
      foreach ($college as $colleges) {
        $html .= $this->collegeNewRow($colleges);
      }
      return $html;
    }
  }
  public function collegeInsert(Request $request){
    $this->validate($request, array(
          'collegeNameText'=>'required|max:100',
          'collegeCodeText'=>'required|max:100|unique:colleges,collegeCode',
          'collegeDescription'=>'required|max:250'
    ));
    $college = new college;
    $college->collegeCode = $request->collegeCodeText;
    $college->collegeName = $request->collegeNameText;
    $college->description = $request->collegeDescription;
    $college->save();
    $html = $this->collegeNewRow($college);
    activityController::insertActivity('Inserted new college with college code:'.$college->collegeCode);
    return $html;
  }
  public function collegeNewRow($college){
    return "<tr id = 'college-".$college->id."'>
      <td>".$college->collegeCode."</td>
      <td>".$college->collegeName."</td>
      <td>".$college->description."</td>
      <td><button class='viewDepartmentCollege btn btn-warning' id = 'viewDepartmentCollege-".$college->id."'><i class='fa fa-eye'></i></button></td>
      <td><button class='editCollege btn btn-info' id = 'myEditCollege-".$college->id."'><i class='fa fa-edit'></i></button></td>
      <td><button class='deleteCollege btn btn-danger' id = 'deleteCollege-".$college->id."'><i class='fa fa-trash-o'></i></button></td>
    </tr>";
  }
  public function collegeUpdate(Request $request){
    $this->validate($request, array(
          'collegeNameText'=>'required|max:100',
          'collegeDescription'=>'required|max:250'
    ));
    $college =  college::find($request->id);
    $college2 = college::where('collegeCode', $request->collegeCodeText)->get();
    error_log($college2);
    if($college->collegeCode == $college2[0]->collegeCode){
      $college->collegeName = $request->collegeNameText;
      $college->description = $request->collegeDescription;
      $college->save();
    }else{
      $this->validate($request, array(
            'collegeCodeText'=>'required|max:5|unique:colleges,collegeCode'
      ));
      $college->collegeCode = $request->collegeCodeText;
      $college->collegeName = $request->collegeNameText;
      $college->description = $request->collegeDescription;
      $college->save();
    }
    activityController::insertActivity('Updated a college with college code:'.$college->collegeCode);
    //return "save";
  }
  public function getCollegeComboList(){
    //$systemdate = systemDate::all();
    $college=college::all();
    //$college=college::where('created_at', '<=', $systemdate[0]->systemDate)->get();
    
    $html="";
    foreach ($college as $colleges) {

      $html .= "<option value = '".$colleges->id."'>
                  ".$colleges->collegeName."
                </option>";
    }
    return $html;
  }
  public function getCollegeDepartment($collegeid){
      $department = DB::select(DB::raw("SELECT d.deptName, d.id, d.deptCode FROM departments d
          INNER JOIN (SELECT id, departmentID FROM
          department_college_hists INNER JOIN
          (SELECT max(id) AS maxid FROM department_college_hists
          group by departmentID) cp ON cp.maxid =
          department_college_hists.id WHERE department_college_hists.collegeID =
          '".$collegeid."') t ON d.id = t.departmentID"));
          //return "Hello";
        if(count($department)==0){
          return "<h2>No Departments yet.</h2>";
        }else{
          $html="
          ";
          foreach ($department as $departments) {
            $html .= $this->newDepartmentRow($departments);
          }
          return $html;
        }

  }
  public function newDepartmentRow($department){
    return "<tr id = 'department-".$department->id."'>
      <td>".$department->deptCode."</td>
      <td>".$department->deptName."</td>
      <td><button class='viewCoursesDepartment btn btn-warning' id = 'viewCoursesDepartment-".$department->id."'><i class='fa fa-pencil-square-o'></i></button></td>
      <td><button class='editDepartment btn btn-info' id = 'editCourseDepartment-".$department->id."'><i class='fa fa-edit'></i></button></td>
      <td><button class='deleteDepartment btn btn-danger' id = 'deleteDepartment-".$department->id."'><i class='fa fa-trash-o'></i></button></td>
      <td><button class='changeCollege btn btn-primary' id = 'changeCollege-".$department->id."'><i class='fa fa-pencil-square-o'></i></button></td>
    </tr>";
  }
  public function insertDepartment(Request $request){
    $this->validate($request, array(
          'collegeComboBox'=>'required',
          'departmentNameText'=>'required|max:50',
          'departmentCodeText'=>'required|max:250|unique:departments,deptCode'
    ));
    $this->department = new department;
    $this->request = $request;
    DB::transaction(function(){
      $this->department->deptName = $this->request->departmentNameText;
      $this->department->deptCode = $this->request->departmentCodeText;
      $this->department->save();
      $dephist =  new departmentCollegeHist;
      $dephist->departmentID = $this->department->id;
      $dephist->collegeID = $this->request->collegeComboBox;
      $dephist->dateAssigned = date('Y-m-d');
      $dephist->save();
    });
    return $this->newDepartmentRow($this->department);
  }
  public function updateDepartment(Request $request){
    $this->validate($request, array(
          'departmentName'=>'required|max:50'
    ));
    $department =  department::find($request->id);
    //$department2 = department::where('deptCode', $request->departmentCode)->get();
    //error_log($college2);
    if($department->deptCode == $request->departmentCode){
      $department->deptName = $request->departmentName;
      $department->save();
    }else{
      $this->validate($request, array(
            'departmentCode'=>'required|max:5|unique:departments,deptCode'
      ));
      $department->deptCode = $request->departmentCode;
      $department->deptName = $request->departmentName;
      $department->save();
    }
    activityController::insertActivity('Updated a department with department code:'.$department->deptCode);
  }
  public function newCourseRow($course){
    return "<tr class='course' id ='course-$course->id'>
      <td>$course->courseCode</td>
      <td>$course->courseName</td>
      <td>$course->courseYear</td>
      <td><button class='editCourse btn btn-info' id ='editCollegeCourse-$course->id'><i class='fa fa-edit'></i></button></td>
      <td><button class='changeDepartment btn btn-dark' id ='changeDepartment-$course->id'><i class='fa fa-edit'></i></button></td>
      <td><button class='deleteCourse btn btn-warning' id ='deleteCourse-$course->id'><i class='fa fa-trash-o'></i></button></td>
    </tr>";
  }
  public function getCollegeCourse($courseid){
    $course = DB::select(DB::raw("SELECT c.courseCode, c.id, c.courseName, c.courseYear FROM courses c
        INNER JOIN (SELECT id, courseID FROM
        course_department_hists INNER JOIN
        (SELECT max(id) AS maxid FROM course_department_hists
        group by courseID) cp ON cp.maxid =
        course_department_hists.id WHERE course_department_hists.departmentID =
        '".$courseid."') t ON c.id = t.courseID"));
    if(count($course)==0){
      return "<h2>No Courses in this department yet.</h2>";
    }else{
      $html="
      ";
      foreach ($course as $courses) {
        $html .= $this->newCourseRow($courses);
      }
      return $html;
    }
  }
  public function courseInsert(Request $request){
    $this->validate($request, array(
          'courseNameText'=>'required|max:50',
          'courseCodeText'=>'required|max:100|unique:courses,courseCode',
          'courseCodeYear'=>'required|max:10'
    ));
    $this->request = $request;
    //$code = '';
    DB::transaction(function(){
      $course = new course;
      $course->courseName = $this->request->courseNameText;
      $course->courseCode = $this->request->courseCodeText;
      $course->courseYear = $this->request->courseCodeYear;
      $course->save();
      $coursehist =  new courseDepartmentHists;
      $coursehist->courseID = $course->id;
      $coursehist->departmentID = $this->request->id;
      $coursehist->dateAssigned = date('Y-m-d');
      $coursehist->save();
      $this->course = $course;
    });
    activityController::insertActivity('Inserted a new course with code:'.$this->course->courseCode);
    return $this->newCourseRow($this->course);
  }
  public function courseEdit(Request $request){
    $this->validate($request, array(
          'courseName'=>'required|max:50'
    ));
    $course =  course::find($request->id);
    $course2 = course::where('courseCode', $request->courseCode)->get();
    //error_log($college2);
    if(count($course2)>0){
      if($course->courseCode == $course2[0]->courseCode){
        $course->courseName = $request->courseName;
        $course->courseYear = $request->courseYear;
        $course->save();
      }
    }else{
      $this->validate($request, array(
            'courseCode'=>'required|max:5|unique:courses,courseCode'
      ));
      $course->courseCode = $request->courseCode;
      $course->courseName = $request->courseName;
      $course->courseYear = $request->courseYear;
      $course->save();
    }
    activityController::insertActivity('Update course with code:'.$course->courseCode);
  }
  public function collegeForSelect($college){
    return "<tr>
      <td>$college->collegeCode</td>
      <td>$college->collegeName</td>
      <td><button class='btn btn-dark collegeSelectButton'  id = 'collegeSelectButton-$college->id'><i class='fa fa-check'></i></button></td>
    </tr>";
  }
  public function getCollegeForSelect($collegeid){
    //$systemdate = systemDate::all();
    $college=college::all();
    //$college=college::where('created_at', '<=', $systemdate[0]->systemDate)->get();
    
    if(count($college)==0){
      return "<h2>No colleges yet.</h2>";
    }else{
      $html="";
      foreach ($college as $colleges) {
        if($collegeid ==$colleges->id){
          continue;
        }
        $html.=$this->collegeForSelect($colleges);
      }
      return $html;
    }
  }
  public function changeDepartmentCollege(Request $request){
      $hist = new departmentCollegeHist;
      $department = department::find($request->departmentID);
      $college = college::find($request->collegeID);
      error_log(date('Y-m-d'));
      $hist->departmentID = $request->departmentID;
      $hist->collegeID = $request->collegeID;
      $hist->dateAssigned = date('Y-m-d');
      activityController::insertActivity('Transfered department:'.$department->deptCode.' to college:'.$college->collegeCode);
      $hist->save();
  }
  public function departmentForCourse($department){
    return "<tr>
      <td>$department->deptCode</td>
      <td>$department->deptName</td>
      <td><button class='btn btn-dark departmentSelectButton'  id = 'departmentSelectButton-$department->id'><i class='fa fa-check'></i></button></td>
    </tr>";
  }
  public function getDepartmentsForCourse($departmentid){
    $department=department::all();
    //error_log($college);
    if(count($department)==0){
      return "<h2>No Departments yet.</h2>";
    }else{
      $html="";
      foreach ($department as $departments) {
        if($departmentid ==$departments->id){
          continue;
        }
        $html.=$this->departmentForCourse($departments);
      }
      return $html;
    }
  }
  public function changeCourseDepartment(Request $request){
    $hist = new courseDepartmentHists;
    $course = course::find($request->courseID);
    $department = department::find($request->departmentID);
    $hist->departmentID = $request->departmentID;
    $hist->courseID = $request->courseID;
    $hist->dateAssigned = date('Y-m-d');
    activityController::insertActivity('Transfered course:'.$course->courseCode.' to department:'.$department->deptCode);
    $hist->save();
  }
  public function getCollegeListSearch($search=''){
    $college=college::where('collegeName', 'LIKE', "%$search%")->orWhere('collegeCode', 'LIKE', "%$search%")
    ->orWhere('description', 'LIKE', "%$search%")->get();
    error_log($college);
    if(count($college)==0){
      return "<h2>No colleges found.</h2>";
    }else{
      $html="";
      foreach ($college as $colleges) {
        $html .= $this->collegeNewRow($colleges);
      }
      return $html;
    }
  }
  public function getCollegeListSearchSelect($search, $collegeid){
    $college=college::where('collegeName', 'LIKE', "%$search%")->orWhere('collegeCode', 'LIKE', "%$search%")
    ->orWhere('description', 'LIKE', "%$search%")->get();
    //error_log($college);
    if(count($college)==0){
      return "<h2>No colleges found.</h2>";
    }else{
      $html="";
      foreach ($college as $colleges) {
        if($collegeid ==$colleges->id){
          continue;
        }
        $html.=$this->collegeForSelect($colleges);
      }
      return $html;
    }
  }
  public function searchDepartmentForSelect($search, $departmentid){
    $department=department::where('deptName', 'LIKE', "%$search%")->orWhere('deptName', 'LIKE', "%$search%")->get();
    //error_log($college);
    if(count($department)==0){
      return "<h2>No Departments yet.</h2>";
    }else{
      $html="";
      foreach ($department as $departments) {
        if($departmentid ==$departments->id){
          continue;
        }
        $html.=$this->departmentForCourse($departments);
      }
      return $html;
    }
  }
  public function deleteCourse($courseid){
    $courseCollege = collegeStudent::where('courseID', '=', $courseid)->get();
    $courseHist = courseDepartmentHists::where('courseID', '=', $courseid)->get();
    if(count($courseCollege)>0){
      return "cannotdelete";
    }
    if(count($courseHist)>1){
      return "cannotdelete";
    }
    $courseHist[0]->forceDelete();
    $course = course::find($courseid);
    activityController::insertActivity('deleted course with code:'.$course->courseCode);
    $course->forceDelete();
    return "deleted";
  }
  public function deleteDepartment($departmentid){
    $departmentCollege = departmentCollegeHist::where('departmentID', '=', $departmentid)->get();
    $departmentCourse= courseDepartmentHists::where('departmentID', '=', $departmentid)->get();
    if(count($departmentCourse)>0){
      return "cannotdelete";
    }
    if(count($departmentCollege)>1){
      return "cannotdelete";
    }
    $departmentCollege[0]->forceDelete();
    $department = department::find($departmentid);
    activityController::insertActivity('deleted department with code:'.$department->deptCode);
    $department->forceDelete();
    return "deleted";
  }
  public function deleteCollege($collegeid){
    $collegedepartment = departmentCollegeHist::where('collegeID', '=', $collegeid)->get();
    $collegeassignment = assignmentCollege::where('collegeID', '=', $collegeid)->get();
    if(count($collegedepartment)>0){
      return "cannotdelete";
    }
    if(count($collegeassignment)>0){
      return "cannotdelete";
    }
    $college = college::find($collegeid);
    activityController::insertActivity('deleted college with code:'.$college->collegeCode);
    $college->forceDelete();
    return "deleted";
  }
}
