<?php

namespace App\Http\Controllers\users;
use App\Http\Controllers\activityController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\useraccount;
use App\model\assignmentCollege;
use App\model\college;
use App\Http\Requests\storeUser;
use App\Http\Controllers\notification\notificationController;
use DB;
use Auth;
use Image;
use Session;
class userController extends Controller
{
    private $request;
    private $user;
    private $update;
    private $search;
    private $rowCounter;
    public function __construct(){
        $this->middleware('auth:myuser');
    }
    public function getUserAccountsPage(){
        $college =  college::all();
        $notif = (new notificationController)->getNotifs();
        return view('admin/useraccounts/useraccounts', ['college'=>$college,
                                                        'notif'=>$notif]);
    }
    public function getUsers(){
        //$user = useraccount::all();
        $user = useraccount::where('status', '=', '1')->get();
        $html = "";
        if(count($user)==1){
            return "<h2>No users added yet</h2>";
        }
        $this->rowCounter = 0;
        foreach($user as $users){
            
            if(Auth::guard('myuser')->id() == $users->id){
                continue;
            }
            $this->rowCounter++;
            $html .=$this->userRow($users, 'active');
        }
        return $html;
    } 
    public function userRow($user, $status){
        
        $designation = '';
        if($user->designation ==''){
            $collegedesig = assignmentCollege::join('colleges', 'assignment_colleges.collegeID', '=', 'colleges.id')
            ->where('assignment_colleges.userID','=', $user->id)
            ->select('colleges.collegeName')->get();
            if(count($collegedesig)>0){
                $designation = $collegedesig[0]->collegeName;
            }
        }else{
            $designation2 = '';
            switch ($user->designation) {
                case 'seniorhigh':
                    $designation2 = 'Senior High';
                break;
                case 'juniorhigh':
                    $designation2 = 'Junior High';
                break;
                case 'elementary':
                    $designation2 = 'Elementary';
                break;
                default:
                
                    break;
            }
            $designation = $designation2.':'.ucfirst($user->designationLevel);
        }
       
        $html = "<tr id ='useraccount-$user->id'>
                    <td>$this->rowCounter</td>
                    <td>$user->firstName $user->lastName</td>
                    <td>$user->userName</td> 
                    <td>$user->email</td>
                       
                    <td>".ucfirst($user->userType)."</td>
                    <td>$designation</td>    
                    <td><button class='editUseraccount btn btn-info' id = 'editUseraccount-$user->id'>
                    <i class='fa fa-edit'></i>
                    </button></td>";
        if($status == 'active'){
            $html .="<td><button class='setToInactive btn btn-dark' id = 'setToInactive-$user->id'>
                        <i class='fa fa-trash-o'></i>
                        </button></td>
                    </tr>";
        }else{
            $html .="<td><button class='setToActive btn btn-dark' id = 'setToActive-$user->id'>
                        <i class='fa fa-trash-o'></i>
                        </button></td>
                    </tr>";
        }
         return $html;           
        
    }
    public function userCell($user, $status){
        
        $designation = '';
        if($user->designation ==''){
            $collegedesig = assignmentCollege::join('colleges', 'assignment_colleges.collegeID', '=', 'colleges.id')
            ->where('assignment_colleges.userID','=', $user->id)
            ->select('colleges.collegeName')->get();
            if(count($collegedesig)>0){
                $designation = $collegedesig[0]->collegeName;
            }
        }else{
            $designation = $user->designation.':'.$user->designationLevel;
        }
        $html = "
        <td>$this->rowCounter</td>
        <td>$user->firstName $user->lastName</td>
        <td>$user->email</td>
        <td>$user->userName</td>    
        <td>$user->userType</td>
        <td>$designation</td>    
        <td><button class='editUseraccount btn btn-info' id = 'editUseraccount-$user->id'>
        <i class='fa fa-edit'></i>
        </button></td>";
        if($status =='active'){
            $html.="<td><button class='setToInactive btn btn-dark' id = 'setToInactive-$user->id'>
                    <i class='fa fa-trash-o'></i>
                    </button></td>";
        }else{
            $html.="<td><button class='setToActive btn btn-dark' id = 'setToActive-$user->id'>
                    <i class='fa fa-trash-o'></i>
                    </button></td>";
        }
        return $html;
    }
    public function getUserDistinct($userid){
        $user = useraccount::find($userid);
        //return $user[0]->firstName;
        //return $user;
        $designation = $user->designation;
        $designationLevel = $user->designationLevel;
        $designation2 = 'lower';
        if($user->designation==''){
            $desig = assignmentCollege::join('colleges', 'assignment_colleges.collegeID', '=', 'colleges.id')
            ->where('assignment_colleges.userID','=', $user->id)
            ->select('colleges.id')->get();
            if(count($desig)>0){
                $designation = $desig[0]->id;
                $designation2 = 'college';
            }
        }
        
        return ['userName'=>$user->userName,
                'firstName'=>$user->firstName,
                'lastName'=>$user->lastName,
                'userType'=>$user->userType,
                'designation'=>$designation,
                'designation2'=>$designation2,
                'designationLevel'=>$designationLevel,
                'image'=>'/images-database/admin/'.$user->id.'.'.$user->imageType,
                'email'=>$user->email
                ];
    }
    public function insertUser(Request $request){
      if($request->action =='add'){
        $this->validate($request, array(
            'usernameUserAccount'=>'required|max:50|unique:useraccounts,userName',
            'passwordUserAccount'=>'required|max:50',
            'firstNameUserAccount'=>'required|max:50',
            'lastNameUserAccount'=>'required|max:50',
            'emailUserAccount'=>'required|max:50|unique:useraccounts,email|email',
            'imageUserAccount' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
            'designationUserAccount'=> 'required',
            'usertype' => 'required' 
        ));
        $collegedesig = [];
        if($request->usertype=='coordinator'){
            
            switch ($request->designationUserAccount) {
                case 'seniorhigh':
                    $collegedesig = useraccount::where('designation', '=', 'seniorhigh')
                    ->where('useraccounts.designationLevel', '=', $request->designationLevel)
                    ->where('useraccounts.status', '=','1')
                    ->get();
                break;
                case 'college':
                    $collegedesig = assignmentCollege::join('colleges', 'assignment_colleges.collegeID', '=', 'colleges.id')
                    ->join('useraccounts', 'assignment_colleges.userID', '=', 'useraccounts.id')
                    ->where('useraccounts.userType', '=', 'coordinator')
                    ->where('colleges.id', '=', $request->collegesDesignation)
                    ->where('useraccounts.status', '=','1')
                    ->select('useraccounts.userType')->get();
                break;
                case 'juniorhigh':
                    $collegedesig = useraccount::where('designation', '=', 'juniorhigh')
                    ->where('useraccounts.designationLevel', '=', $request->designationLevel)
                    ->where('useraccounts.status', '=','1')
                    ->get();
                    
                break;
                case 'elementary':
                    $collegedesig = useraccount::where('designation', '=', 'elementary')
                    ->where('useraccounts.designationLevel', '=', $request->designationLevel)
                    ->where('useraccounts.status', '=','1')
                    ->get();
                break;
                default:
                    # code...
                    break;
            }
        }
        
        if(count($collegedesig)>0){
            return "errorCoordinator";
        }
        $this->request = $request;
        $this->user = new useraccount;
        DB::transaction(function() {
            $this->user= new useraccount;
            $this->user->userName = $this->request->usernameUserAccount;
            $this->user->password = bcrypt($this->request->passwordUserAccount);
            $this->user->email = $this->request->emailUserAccount;
            $this->user->firstName = $this->request->firstNameUserAccount;
            $this->user->userType = $this->request->usertype;
            $this->user->verified = 1;
            $this->user->status = 1;
            if($this->request->designationUserAccount != 'college'){
                $this->user->designationLevel = $this->request->designationLevel;
                $this->user->designation = $this->request->designationUserAccount;
            }
            $this->user->lastName = $this->request->lastNameUserAccount;
            $image = $this->request->file('imageUserAccount');
            $this->user->imageType = $image->getClientOriginalExtension();
            $this->user->save();
            if($this->request->designationUserAccount == 'college'){
                $assign = new assignmentCollege;
                $assign->collegeID = $this->request->collegesDesignation;
                $assign->userID = $this->user->id;
                $assign->dateAssigned = date('Y-m-d');
                $assign->save();
            }
            $filename = $this->user->id.'.'.$image->getClientOriginalExtension();
            $location = public_path("images-database/admin/". $filename);
            Image::make($image)->resize(200,200)->save($location);
        });
        activityController::insertActivity('Added user with username:'.$this->user->userName);
        $this->rowCounter = $request->rowCounter;
        return $this->userRow($this->user, 'active');                               
      }else{
        
        $this->validate($request, array(
            
            'firstNameUserAccount'=>'required|max:50',
            'lastNameUserAccount'=>'required|max:50',
            
            'designationUserAccount'=> 'required',
            'usertype' => 'required' 
        ));
        $myuser = useraccount::find($request->id);
        if($myuser->userName != $request->usernameUserAccount){
            $this->validate($request, array(
                'usernameUserAccount'=>'required|max:50|unique:useraccounts,userName'
            ));
        }
        if($myuser->email != $request->emailUserAccount){
            $this->validate($request, array(
                'emailUserAccount'=>'required|max:50|unique:useraccounts,email|email'
            ));
        }
        $collegedesig = [];
        if($request->usertype=='coordinator'){
            
            switch ($request->designationUserAccount) {
                case 'seniorhigh':
                    $collegedesig = useraccount::where('designation', '=', 'seniorhigh')
                    ->where('useraccounts.designationLevel', '=', $request->designationLevel)->get();
                break;
                case 'college':
                    $collegedesig = assignmentCollege::join('colleges', 'assignment_colleges.collegeID', '=', 'colleges.id')
                    ->join('useraccounts', 'assignment_colleges.userID', '=', 'useraccounts.id')
                    ->where('useraccounts.userType', '=', 'coordinator')
                    ->where('colleges.id', '=', $request->collegesDesignation)
                    ->select('useraccounts.id')->get();
                break;
                case 'juniorhigh':
                    $collegedesig = useraccount::where('designation', '=', 'juniorhigh')
                    ->where('useraccounts.designationLevel', '=', $request->designationLevel)->get();
                break;
                case 'elementary':
                    $collegedesig = useraccount::where('designation', '=', 'elementary')
                    ->where('useraccounts.designationLevel', '=', $request->designationLevel)->get();
                break;
                default:
                    # code...
                    break;
            }
        }
        $this->update =false;
        if($myuser->userType==$request->usertype){
            if($myuser->userType=='coordinator'){
                if(count($collegedesig)>0){
                    if($collegedesig[0]->id!=$myuser->id){
                        return "errorCoordinator";
                    }
                }else{
                    if($request->designationUserAccount=='college'){
                        $this->update = true;
                    }
                }    
            }else{
                if($myuser->designation != $request->designationUserAccount){
                    if($request->designationUserAccount=='college'){
                        $this->update = true;
                    }
                }
                
            }
        }else{
            if($myuser->userType=='coordinator'){
                if(count($collegedesig)>0){
                    return "errorCoordinator";
                }    
            }else{
                if($request->designationUserAccount=='college'){
                    $this->update = true;
                }
            }
        }
        /*if(count($collegedesig)>0){
            return "errorCoordinator";
        }*/
        $this->request = $request;
        $this->user = useraccount::find($request->id);
        DB::transaction(function() {
            //$this->user= new useraccount;
            $this->user->userName = $this->request->usernameUserAccount;
            if($this->request->passwordUserAccount !=''){
                $this->user->password = bcrypt($this->request->passwordUserAccount);
            }
            $this->user->email = $this->request->emailUserAccount;
            $this->user->firstName = $this->request->firstNameUserAccount;
            $this->user->userType = $this->request->usertype;
            $this->user->verified = 1;
            if($this->request->designationUserAccount != 'college'){
                $this->user->designationLevel = $this->request->designationLevel;
                $this->user->designation = $this->request->designationUserAccount;
            }
            $this->user->lastName = $this->request->lastNameUserAccount;
            if( $this->request->hasFile('imageUserAccount')){
                $image = $this->request->file('imageUserAccount');
                $this->user->imageType = $image->getClientOriginalExtension();
            }
            $this->user->save();
            if( $this->update){
                if($this->request->designationUserAccount == 'college'){
                    $thatuser = useraccount::find($this->request->id);
                    $thatuser->designation = null;
                    $thatuser->designationLevel = null;
                    $thatuser->save();
                    $assign2 = assignmentCollege::where('userID', '=', $this->request->id)->get();
                    if(count($assign2)>0){
                        $assign2[0]->forceDelete();
                    }
                    
                    $assign = new assignmentCollege;
                    $assign->collegeID = $this->request->collegesDesignation;
                    $assign->userID = $this->user->id;
                    $assign->dateAssigned = date('Y-m-d');
                    $assign->save();
                }
            }
            // else{
            //     //error_log("Hello wordl!");
            //     $assign2 = assignmentCollege::where('userID', '=', $this->request->id)->get();
            //     if(count($assign2)>0){
            //         $assign2[0]->forceDelete();
            //     }
            // }
            
            if( $this->request->hasFile('imageUserAccount')){
                $filename = $this->user->id.'.'.$image->getClientOriginalExtension();
                $location = public_path("images-database/admin/". $filename);
                Image::make($image)->resize(200,200)->save($location);
            }
        });
        $this->user=useraccount::find($request->id);
        activityController::insertActivity('Updated user with username:'.$this->user->userName);
        $this->rowCounter = $request->rowCounter;
        return $this->userCell($this->user, $request->status);    
        
      }
    }
    public function getUsersInactive(){
        $user = useraccount::where('status', '=', '0')->get();
        $html = "";
        if(count($user)==0){
            return "<h2>No users yet</h2>";
        }
        foreach($user as $users){
           
            if(Auth::guard('myuser')->id() == $users->id){
                continue;
            }
            $this->rowCounter++;
            $html .=$this->userRow($users, 'inactive');
        }
        return $html;
    }
    public function setInactiveUser($userid){
        $user  = useraccount::find($userid);
        $user->status = 0;
        $user->save();
        activityController::insertActivity('Set user to inactive with username:'.$user->userName);
        return "success";
    }
    public function setActiveUser($userid){
        $user  = useraccount::find($userid);
        if($user->userType == 'coordinator'){
            $collegedesig = [];
            switch ($user->designation) {
                case 'seniorhigh':
                    $collegedesig = useraccount::where('designation', '=', 'seniorhigh')
                    ->where('useraccounts.designationLevel', '=', $request->designationLevel)
                    ->where('useraccounts.status', '=','1')
                    ->get();
                break;
                case '':
                    $collegedesig = assignmentCollege::join('colleges', 'assignment_colleges.collegeID', '=', 'colleges.id')
                    ->join('useraccounts', 'assignment_colleges.userID', '=', 'useraccounts.id')
                    ->where('useraccounts.userType', '=', 'coordinator')
                    ->where('colleges.id', '=', $request->collegesDesignation)
                    ->where('useraccounts.status', '=','1')
                    ->select('useraccounts.userType')->get();
                break;
                case 'juniorhigh':
                    $collegedesig = useraccount::where('designation', '=', 'juniorhigh')
                    ->where('useraccounts.designationLevel', '=', $request->designationLevel)
                    ->where('useraccounts.status', '=','1')
                    ->get();
                    
                break;
                case 'elementary':
                    $collegedesig = useraccount::where('designation', '=', 'elementary')
                    ->where('useraccounts.designationLevel', '=', $request->designationLevel)
                    ->where('useraccounts.status', '=','1')
                    ->get();
                break;
                default:
                    # code...
                    break;
            }

            if(count($collegedesig)>0){
                return "errorCoordinator";
            }else{
                $user->status = 1;
                $user->save();
                activityController::insertActivity('Set user to active with username:'.$user->userName);
                return "success";
            }
        }else{
            $user->status = 1;
            $user->save();
            activityController::insertActivity('Set user to active with username:'.$user->userName);
            return "success";
        }
    }
    public function searchUser($status, $search=''){
        $this->search = $search;
        if($status=='active'){
            $user = useraccount::where('status', '=', "1")
                                ->where(function($query){
                                    $query->where('email', 'LIKE', "%$this->search%")
                                    ->orWhere('username', 'LIKE', "%$this->search%")
                                    ->orWhere('firstName','LIKE', "%$this->search%")
                                    ->orWhere('lastName','LIKE', "%$this->search%");
                                })->get();
            $html = "";
            if(count($user)==0){
                return "<h2>No users found</h2>";
            }
            foreach($user as $users){
    
                if(Auth::guard('myuser')->id() == $users->id){
                    continue;
                }
                $html .=$this->userRow($users, 'active');
            }
            return $html;
        }else{
            $user = useraccount::where('status', '=', "0")
            ->where(function($query){
                $query->where('email', 'LIKE', "%$this->search%")
                ->orWhere('username', 'LIKE', "%$this->search%")
                ->orWhere('firstName','LIKE', "%$this->search%")
                ->orWhere('lastName','LIKE', "%$this->search%");
            })->get();
            $html = "";
            if(count($user)==0){
                return "<h2>No users found</h2>";
            }
            foreach($user as $users){
                if(Auth::guard('myuser')->id() == $users->id){
                    continue;
                }
                $html .=$this->userRow($users, 'inactive');
            }
            return $html;
        }
        
    } 
    public function getUserDesignation($userid){
        $user = useraccount::find($userid);
        if($user->designation == ''){
            return ["college", ""];
        }else{
            return [$user->designation, explode("-",$user->designationLevel)[1]]; 
        }
    }
    
    public function getUsersOfSameDesignation($userid){
        $designation = $this->getUserDesignation($userid);
        if(session()->get('user')['userType']=='admin'){
            return ['designation'=>'', 'users'=>'', 'level'=>''];
        }else{
            if($designation[0]== 'college'){
                $college = DB::select(DB::raw("SELECT assignment_colleges.userID, assignment_colleges.collegeID,
                useraccounts.firstName, useraccounts.lastName,
                useraccounts.id, useraccounts.userType FROM assignment_colleges INNER JOIN (SELECT max(id) AS maxid FROM 
                assignment_colleges GROUP BY userID) ac3 ON ac3.maxid = assignment_colleges.id INNER JOIN 
                (SELECT useraccounts.id, ac2.collegeID, useraccounts.firstName, useraccounts.lastName  FROM useraccounts
                INNER JOIN (SELECT userID, id, collegeID FROM assignment_colleges
                INNER JOIN (SELECT MAX(id) as maxid FROM assignment_colleges GROUP BY userID
                ) ac ON ac.maxid = assignment_colleges.id) ac2 ON ac2.userID 
                = useraccounts.id WHERE useraccounts.id = '$userid') col ON col.collegeID = 
                assignment_colleges.collegeID
                INNER JOIN useraccounts ON useraccounts.id = assignment_colleges.userID"));
                return ['designation'=>'college', 'users'=>$college, 'level'=>$college[0]->collegeID];
            }else{
                $users = useraccount::where("designation",'=', $designation[0])
                                    ->where("designationLevel", '=', $designation[1])->get();
                return ['designation'=>$designation[0], 'users'=>$users, 'level'=>$designation[1]];
            }
        }
        
    }
}
