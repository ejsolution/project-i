var totalRow;
var totalPages;
var visiblePages;
var mypage;
$(document).ready(function(){
  var eventid;
  var action;
  if($("#idEventView").val()!= ""){
    $.get("/events/data/"+$("#idEventView").val(), function(data){
      $('#titleEventTextEdit').val(data.eventsTitle);
      $('#dateEventStartEdit').val(data.dateStart);
      $('#dateEventEndEdit').val(data.dateEnd);
      $('#timeEventStartEdit').val(data.timeStart);
      $('#timeEventEndEdit').val(data.timeEnd);
      $('#locationEventTextEdit').val(data.location);
      $('#descriptionEventTextAreaEdit').val(data.details);
      $('#editEventModal').modal('show');
    });
  }
  var systemDate =  new Date($("#systemDate").val());
  $('#calendar').fullCalendar({
      selectable: true,
      selectHelper: true,
      renderEvent:true,
      defaultDate: systemDate,
      select: function(start, end, allDay) {
        $('#fc_create').click();
        var started = new Date(start);
        var ended = new Date(end);
        
        var dayOfMonth = ended.getDate();  
        ended.setDate(dayOfMonth - 1);   
       
        startDate = started.getFullYear()+'-'+formatMonth(started.getMonth()+1)+'-'+formatDay(started.getDate());
        endDate = ended.getFullYear()+'-'+formatMonth(ended.getMonth()+1)+'-'+formatDay(ended.getDate());
        
        $("#addEventModal").modal('show');
        $('#dateEventEnd').val(endDate);
        $('#dateEventStart').val(startDate);
        /*var event = {
          id:'myevent',
          title:'ThisEvent',
          allDay:true,
          start:started,
          end:ended
        };
        $('#calendar').fullCalendar('renderEvent', event, true);*/
      },
      eventClick: function(calEvent, jsEvent, view) {  
        //this is how you remove an event
        //$("#calendar").fullCalendar('removeEvents', calEvent.id);
        eventid = (calEvent.id).split('-')[1];
        //console.log(eventid);
        $.get("/events/data/"+eventid, function(data){
          $('#titleEventTextEdit').val(data.eventsTitle);
          $('#dateEventStartEdit').val(data.dateStart);
          $('#dateEventEndEdit').val(data.dateEnd);
          $('#timeEventStartEdit').val(data.timeStart);
          $('#timeEventEndEdit').val(data.timeEnd);
          $('#locationEventTextEdit').val(data.location);
          $('#descriptionEventTextAreaEdit').val(data.details);
          $('#editEventModal').modal('show');
        });
      },
      viewRender: function (view, element) {
        var b = $('#calendar').fullCalendar('getDate');
        var month = b.format('L').split('/')[0];
        
        $.get("/events/list/"+month, function(data){
          
          $('#calendar').fullCalendar('removeEvents');
          for (let index = 0; index < data.length; index++) {
            data[index].id = 'event-'+data[index].id;
            $('#calendar').fullCalendar('renderEvent', data[index], true);
          }
        });
      }
    });
    
    $('#searchEvents').on('keydown', function(){
      tableLoader('6', 'eventsListTable');
      $('#pagination').twbsPagination('destroy');
      $.get('/events/listtable/totalrow/'+$('#searchEvents').val(), function(data){
        initializePages(data);
        $('#pagination').twbsPagination({
            totalPages: totalPages,
            visiblePages: visiblePages,
            onPageClick: function(event, page){
                mypage = page;
                tableLoader('7', 'contractTable');
                getAjax('/events/listtable/'+page+'/'+$('#searchEvents').val(), 'eventsListTable');
            }
        });
      });
    });
    $("#activateAddEventModal").on('click', function(){
      $("#addEventModal").modal('show');
    });
    $("#activateSendModal").on('click', function(){
      $('#sendEventModal').modal('show');
    });
    $('#eventAddForm').on('submit', function(e){
      
      e.preventDefault();
      var form = document.getElementById('eventAddForm');
      var mydata = new FormData(form);
      var started = new Date($('#dateEventStart').val());
      var ended = new Date($('#dateEventEnd').val());
      //console.log();
      //return;
      if(ended<started){
        new PNotify({
          title: 'Error!',
          text: 'Date event end must be greater than  start date!',
          type: 'error',
          styling: 'bootstrap3'
        });
        return;
      }else{
        if(ended.getTime()===started.getTime()){
         
          var startHour = parseInt($('#timeEventStart').val().split(':')[0]);
          var startMinute = parseInt($('#timeEventStart').val().split(':')[1]);
          var endHour = parseInt($('#timeEventEnd').val().split(':')[0]);
          var endMinute = parseInt($('#timeEventEnd').val().split(':')[1]);
          if(endHour<startHour){
            new PNotify({
              title: 'Error!',
              text: 'End time must be greater than  start time!',
              type: 'error',
              styling: 'bootstrap3'
            });
            return;
          }else{
            if(endHour==startHour){
              if(endMinute<startMinute){
                new PNotify({
                  title: 'Error!',
                  text: 'End time must be greater than  start time!',
                  type: 'error',
                  styling: 'bootstrap3'
                });
                return;
              }
            }
          }
        }
      }
      action ='insert';
      prependPostAjaxErrorCallback('/event/checkconflick', mydata, function(check){
        if(check == 'true'){
          prependPostAjaxErrorCallback('/event/insert', mydata, function(data) {
            $('#calendar').fullCalendar('renderEvent', data, true);
            new PNotify({
              title: 'Success!',
              text: 'Event Successfully Saved!',
              type: 'success',
              styling: 'bootstrap3'
            });
            $("#eventAddForm input[type=text], textarea").val("");
            $('#addEventModal').modal('hide');
          });
        }else{
          $('#checkModal').modal('show');
          $('#checkModal #checkMessage').html('<h2>This conflicts with another events! Save anyway?</h2>');
        }
      });
      
    });
    $('#confirmEvent').on('click', function(){
      $('#checkModal').modal('hide');
      if(action =='insert'){
        $("#eventAddForm input[type=text], textarea").val("");
        var form = document.getElementById('eventAddForm');
        var mydata = new FormData(form);
        prependPostAjaxErrorCallback('/event/insert', mydata, function(data) {
          $('#calendar').fullCalendar('renderEvent', data, true);
          new PNotify({
            title: 'Success!',
            text: 'Event Successfully Saved!',
            type: 'success',
            styling: 'bootstrap3'
          });
        });
      }else{
        var form = document.getElementById('eventEditForm');
        var mydata = new FormData(form);
        mydata.append('id', eventid);
        prependPostAjaxErrorCallback('/event/update', mydata, function(data) {
          $("#calendar").fullCalendar('removeEvents', 'event-'+eventid);
          $('#calendar').fullCalendar('renderEvent', data, true);
          new PNotify({
            title: 'Success!',
            text: 'Event Successfully Saved!',
            type: 'success',
            styling: 'bootstrap3'
          });
          
          $("#eventEditForm input[type=text], textarea").val("");
          $('#editEventModal').modal('hide');
        });
      }
    });
    $('#deleteEventButtom').on('click', function(){
      //$("#calendar").fullCalendar('removeEvents', 'event-'+eventid);
      globaleventid = eventid;
      $("#deleteModal").modal('show');
      $("#deleteMessage").html("<h3>Are you sure you want to delete this event?</h3>");
      mydelete ="deleteEvent";
    });
    $('#eventEditForm').on('submit', function(e){
      e.preventDefault();
      var form = document.getElementById('eventEditForm');
      var mydata = new FormData(form);
      mydata.append('id', eventid);
      var started = new Date($('#dateEventStartEdit').val());
      var ended = new Date($('#dateEventEndEdit').val());
      //console.log();
      //return;
      if(ended<started){
        new PNotify({
          title: 'Error!',
          text: 'Date event end must be greater than  start date!',
          type: 'error',
          styling: 'bootstrap3'
        });
        return;
      }else{
        if(ended.getTime()===started.getTime()){
         
          var startHour = parseInt($('#timeEventStartEdit').val().split(':')[0]);
          var startMinute = parseInt($('#timeEventStartEdit').val().split(':')[1]);
          var endHour = parseInt($('#timeEventEndEdit').val().split(':')[0]);
          var endMinute = parseInt($('#timeEventEndEdit').val().split(':')[1]);
          if(endHour<startHour){
            new PNotify({
              title: 'Error!',
              text: 'End time must be greater than  start time!',
              type: 'error',
              styling: 'bootstrap3'
            });
            return;
          }else{
            if(endHour==startHour){
              if(endMinute<startMinute){
                new PNotify({
                  title: 'Error!',
                  text: 'End time must be greater than  start time!',
                  type: 'error',
                  styling: 'bootstrap3'
                });
                return;
              }
            }
          }
        }
      }
      prependPostAjaxErrorCallback('/event/checkconflict/update', mydata, function(check){
        if(check == 'true'){
          prependPostAjaxErrorCallback('/event/update', mydata, function(data) {
            $("#calendar").fullCalendar('removeEvents', 'event-'+eventid);
            $('#calendar').fullCalendar('renderEvent', data, true);
            new PNotify({
              title: 'Success!',
              text: 'Event Successfully Saved!',
              type: 'success',
              styling: 'bootstrap3'
            });
            $("#eventEditForm input[type=text], textarea").val("");
            $('#editEventModal').modal('hide');
          });
        }else{
          $('#checkModal').modal('show');
          $('#checkModal #checkMessage').html('<h2>This conflicts with another events! Save anyway?</h2>');
        }
      });
    });

    $('#studentTypeFilter').on('change', function(){
      if($('#studentTypeFilter').val() == 'all'){
          $('#courseGradeLevelFilter').val('all');
          $('#courseGradeLevelFilter').attr('disabled', true);
          tableLoader('5', 'tableOfStudents');
          $.get('/event/group/students/message/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/', function (data) {
            
            $('#tableOfStudents').html(data);
             
          });
        }else{
          $('#courseGradeLevelFilter').attr('disabled', false);
          switch ($('#studentTypeFilter').val()) {
            case 'college':
              $.get('/report/college/course', function(data){
                  $('#courseGradeLevelFilter').html("<option value='all'>all</option>");
                  $('#courseGradeLevelFilter').append(data);
                  tableLoader('5', 'tableOfStudents');
                  $.get('/event/group/students/message/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/', function (data) {
                        
                      $('#tableOfStudents').html(data);
                     
                  });
              });
            break;
            case 'seniorhigh':
              var html = "<option value ='all'>all</option>"+
                          "<option value ='12'>12</option>"+
                          "<option value ='11'>11</option>";
              $('#courseGradeLevelFilter').html(html);
              tableLoader('5', 'tableOfStudents');
              $.get('/event/group/students/message/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/', function (data) {
                  $('#tableOfStudents').html(data);
                  
              });
            break;
            case 'juniorhigh':
              var html = "<option value ='all'>all</option>"+
                          "<option value ='10'>10</option>"+
                          "<option value ='9'>9</option>"+
                          "<option value ='8'>8</option>"+
                          "<option value ='7'>7</option>";
              $('#courseGradeLevelFilter').html(html);
              tableLoader('5', 'tableOfStudents');
              $.get('/event/group/students/message/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/', function (data) {
                  $('#tableOfStudents').html(data);
                
              });
            break;
            case 'elementary':
              var html = "<option value ='all'>all</option>"+
                      "<option value ='6'>6</option>"+
                      "<option value ='5'>5</option>"+
                      "<option value ='4'>4</option>"+
                      "<option value ='3'>3</option>"+
                      "<option value ='2'>2</option>"+
                      "<option value ='1'>1</option>";
              $('#courseGradeLevelFilter').html(html);
              tableLoader('5', 'tableOfStudents');
              $.get('/event/group/students/message/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/', function (data) {
                  $('#tableOfStudents').html(data);
                
              });
            break;
            default:
              break;
          }
      }  
      
    });
    $('#schoolYearFilter').on('change', function(){
      if($('#schoolYearFilter').val() != "all"){
        $.get('/report/schoolyear/'+$('#schoolYearFilter').val(), function(data){
          $('#semesterFilter').html("<option value='all'>all</option>");
          $('#semesterFilter').append(data);
          tableLoader('5', 'tableOfStudents');
          $.get('/event/group/students/message/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/', function (data) {
              $('#tableOfStudents').html(data);
              
          });
        });
      }else{
          tableLoader('5', 'tableOfStudents');
          $.get('/event/group/students/message/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/', function (data) {
              $('#tableOfStudents').html(data);
              
          });
      }
    }); 
    $('#semesterFilter').on('change', function(){
      tableLoader('5', 'tableOfStudents');
      $.get('/event/group/students/message/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/', function (data) {
          $('#tableOfStudents').html(data);
          
      });
    });
    $('#courseGradeLevelFilter').on('change', function(){
      tableLoader('5', 'tableOfStudents');
      $.get('/event/group/students/message/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/', function (data) {
          $('#tableOfStudents').html(data);
      });
    });
    
    $('#eventsListTable').on('click', '.sendMessage', function(){
      eventid = $(this).prop("id").split("-")[1];
      $("#eventMessageModal").modal("show");
      tableLoader('5', 'messageOfEvents');
      $.get('/event/messages/'+eventid, function (data) {
        $('#messageOfEvents').html(data);
      });
    });
    $("#messageEventForm").on("submit", function(e){
      e.preventDefault();
      var i=0;
      var students = [];
      $('.checkboxStudent:checkbox:checked').each(function () {
        students[i] = $(this).val();
        i++; 
      });
      if(i==0){
        new PNotify({
          title: 'Error!',
          text: 'Please select a student to send this message to.',
          type: 'error',
          styling: 'bootstrap3'
        });
        return;
      }
      $('#statusMessageModal').modal('show');
      divLoader('messageLoader');
      new Promise((resolve, reject)=>{
        var x =0;
        for(var y = 0; y< i; y++){
          var mydata  =  document.getElementById("messageEventForm");
          var formdata  =  new FormData(mydata);
          formdata.append("studentID", students[y]);
          formdata.append("eventID", eventid);
          prependPostAjaxErrorCallback('/event/sendmessage', formdata, function(data){
            
            new PNotify({
              title: 'Success!',
              text: 'Message sent to '+data,
              type: 'success',
              styling: 'bootstrap3'
            });
          
            x++;
            if(x==i){
              resolve(true);
            }
          });
        }
      }).then((resolve)=>{
        var message  = '';
        if($("#messageEvent").val().length > 30){
          message = $("#messageEvent").val().substr(0,30)+"...";
        }else{
          message = $("#messageEvent").val();
        }
        var row = "<tr><td>"+message+"</td><td>"+$("#systemDate").val()+"</td></tr>";
        $("#messageOfEvents").children("h2").each(function(){
          $(this).remove();
        });
        $("#messageOfEvents").append(row);
        $('#messageStatus').html("<h2>All messages sent!</h2>");
        $('#messageLoader').html("");
        $('#messageEventForm textarea').val("");
      });
    });
    $('#statusMessageModal').on('hidden.bs.modal', function(){
      $('#messageStatus').html("<h2>Message is now sending. Please wait, while we send all the messages. This may take a few minutes.</h2>");
    }); 
    $("#checkAllStudents").on("click", function(){
      if($(this).prop("checked")){
        $(".checkboxStudent").attr("checked", true);
      }else{
        $(".checkboxStudent").attr("checked", false);
      }
    });
});
function formatMonth(month){
  if(parseInt(month)<10){
    return '0'+month; 
  }else{
    return month;
  }
}
function formatDay(day){
  if(parseInt(day)<10){
    return '0'+day; 
  }else{
    return day
  }
}
function initializePages(data){
  totalRow = data;
  if(totalRow<=5){
      totalPages = 1;
  }else{
      var remainder = totalRow%10;
      if(remainder >0){
          totalPages = ((totalRow-remainder)/10)+1;
      }else{
          totalPages = totalRow/10;
      }
      if(totalPages<=5){
          visiblePages =totalPages;
      }else{
          visiblePages = 5;
      }    
  }
}
tableLoader('6', 'eventsListTable');
$.get("/events/listtable/totalrow/"+$('#searchEvents').val(), function(data){
  initializePages(data);
    $('#pagination').twbsPagination({
        totalPages: totalPages,
        visiblePages: visiblePages,
        onPageClick: function(event, page){
            mypage = page;
            tableLoader('6', 'eventsListTable');
            getAjax('/events/listtable/'+page+'/'+$('#searchEvents').val(), 'eventsListTable');
        }
    });        
});
$.get('/event/group/students/message/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/', function (data) {
                        
  $('#tableOfStudents').html(data);
 
});
