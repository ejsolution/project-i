@extends('admin.adminTemplate')

@section('title', 'AdminDashboard')
@section('stylesheet')
  <link href="{{asset('css/admin/transactions.css')}}" rel="stylesheet">
  <link href="{{asset('css/admin/referral/referral.css')}}" rel="stylesheet">
@stop
@section('content')
  <div class='container'>
    @component('components.headerSearchFilter', ['user'=>$user,
                                                  'collegeOfUser'=>$collegeOfUser])
      @slot('comboLabel')
        View By Student Type
      @endslot
      @slot('schoolyears') 
        @foreach($schoolyears as $year)
          <option value ='{{$year->id}}'>{{explode('-', $year->schoolYearStart)[0]}} - {{explode('-', $year->schoolYearEnd)[0]}}</option>
        @endforeach
      @endslot
      @slot('searchID')
        searchReferral
      @endslot
      @slot('placeholder')
        Search By Student Or School Year
      @endslot
      @slot('button')
        <button class='btn btn-block btn-dark' id = 'addStudentButton'>Add</button>
      @endslot
    @endcomponent
    <div class='row'>
      <div class='col-md-12 well tableContainer'>
        <table class='table table-striped'>
          <thead>
            <tr> 
              <th>No</th>
              <th>Name</th>
              <th>Student Type</th>
              <th>Course/ Strand</th>
              <th>Year Level/ Grade Level</th>
              {{-- <th>School Year</th>
              <th>Semester</th> --}}
              <th>Date</th>
              <th>Message Parents</th>
              <th>View</th>
            </tr> 
          </thead>
          <tbody id ='referralsTable'>
          </tbody>
        </table>
      </div> 
    </div>
    <div class='genericDivTop'>
        <center><ul id="pagination" class="pagination-sm"></ul></center>
    </div>
    <div class='alignRight'> 
      <button class='btn btn-dark' id = 'printReferralStudents'>
        <i class='fa fa-print'>&nbsp; Print</i>
      </button>
    </div>
    @include('Partials.Modals.referral.referralForm')
    
    @component('components.modal')
      @slot('modalid')
        referralFormModalEdit
      @endslot
      @slot('modalsize')
        modal-lg 
      @endslot
      @slot('modaltitle')
        Edit Referral Form
      @endslot
      @slot('modalcontent')
        <div class='row'>
          <div class='col-md-12'>
            @component('components.x_panel')
              @slot('title')
                  Student Information
              @endslot
              @slot('content')
                <div class='row'>
                  <div class='col-md-6'>
                    <label for = 'studentNameReferralFormTextEdit'>Name</label>
                    <select id = 'studentNameReferralFormTextEdit' readonly
                    name = 'studentID' class='form-control'>
                      @foreach ($studentstable as $studentstables)
                        <option value ='{{$studentstables->id}}'>{{$studentstables->firstName}} {{$studentstables->lastName}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class='col-md-6'>
                    <div class='row'>
                      <div class='col-md-6'>
                        <label for = 'schoolYearReferralFormTextEdit'>School Year</label>
                        <input type = 'text' id = 'schoolYearReferralFormTextEdit'
                        name = 'schoolYearReferralFormText' disabled class='form-control' value ='{{$schoolyear}}'>
                      </div>
                      <div class='col-md-6'>
                        <label for = 'semesterReferralFormTextEdit'>Semester</label>
                        <input type = 'text' id = 'semesterReferralFormTextEdit'
                        name = 'semesterReferralFormText' disabled class='form-control' value ='{{$currentsem}}'>
                      </div>
                    </div>
                  </div>
                </div>
                <div class='row'>
                  <div class='col-md-12'>
                    <label for = 'homeAddressReferralFormTextEdit'>Home Address</label>
                    <input type = 'text' id = 'homeAddressReferralFormTextEdit'
                    name = 'homeAddressReferralFormText' class='form-control' disabled>
                  </div>
                </div>
                <div class='row'>
                  <div class='col-md-6'>
                    <label for = 'courseReferralFormTextEdit'>Course</label>
                    <input type = 'text' id = 'courseReferralFormTextEdit'
                    name = 'courseReferralFormText' class='form-control' disabled>
                  </div>
                  <div class='col-md-6'>
                    <label for = 'contactReferralFormTextEdit'>Tel/Contact No.</label>
                    <input type = 'text' id = 'contactReferralFormTextEdit'
                    name = 'contactReferralFormText' class='form-control' disabled>
                  </div>
                </div>
                <div class='row'>
                  <div class='col-md-6'>
                    <label for = 'yearLevelReferralFormTextEdit'>Year Level/Grade Level</label>
                    <input type = 'text' id = 'yearLevelReferralFormTextEdit'
                    name = 'yearLevelReferralFormText' class='form-control' disabled>
                  </div>
                  <div class='col-md-6'>
                    <label for = 'dateReferralFormTextEdit'>Referral Date</label>
                    <input type = 'date' id = 'dateReferralFormTextEdit'
                    name = 'referralDate' class='form-control' form ='referralEditForm' required readonly>
                  </div>
                </div>
                {!! Form::open([
                  'id'=>'referralEditForm']) !!}
                {!! Form::close() !!}
              @endslot
            @endcomponent
          </div>
        </div>
        <div class='row'>
          <div class='col-md-12 well'>
            <div class='genericDivBottom alignRight'>
              <button id = 'addBehaviorEdit' class='btn btn-dark'>Add</button>
            </div>
            <div class='row'>
              <div class='col-md-12 tableModalContainer'>
                <table class='table table-striped'>
                  <thead>
                    <tr>
                      <th>Observable Behaviours</th>
                      <th>Frequency</th>
                      <th>Remarks</th>
                      <th>Remove</th>
                    </tr>
                  </thead>
                  <tbody id ='behaviorReferralTableEdit'>
                  </tbody>
                </table>
              </div>
            </div>
            
          </div>
        </div>
        <div class='row'>
          <div class='col-md-6'>
            <div class='row'>
              <div class='col-md-4'>
                <label for = 'referredToReferralFormTextEdit'>Referred To:</label>
              </div>
              <div class='col-md-8'>
                <input type = 'text' id = 'referredToReferralFormTextEdit' name ='referredToReferralFormText'
                class='form-control' required form ='referralEditForm'>
              </div>
            </div>
          </div>
          <div class='col-md-6'>
            <div class='row'>
              <div class='col-md-4'>
                  <label for = 'referredByReferralFormTextEdit'>Referred By:</label>
                  <label for = 'refferedByUniversityCollegeEdit'>College:</label>
              </div>
              <div class='col-md-8'>
                <input type = 'text' id = 'referredByReferralFormTextEdit' name ='referredByReferralFormText'
                class='form-control' required form ='referralEditForm'>
                <select class='form-control' required form ='referralEditForm' 
                  id = 'refferedByUniversityCollegeEdit' name = 'unviersityCollege' style = 'margin-top:10px;'>

                </select>
              </div> 
            </div>
          </div>
        </div>
      @endslot
      @slot('modalbutton')
          <button class="btn btn-danger" form ='referralEditForm'><i class='fa fa-save'></i>&nbsp;Save</button>
      @endslot
    @endcomponent
    @component('components.modal')
      @slot('modalid')
        referralMessage
      @endslot
      @slot('modalsize')
        modal-lg 
      @endslot
      @slot('modaltitle')
        Message Guardian
      @endslot
      @slot('modalcontent')
        <div class='row'>
          <div class='col-md-12 tableModalContainer'>
            <h5>Family of students</h5>
            <table class='table table-striped genericDivTop'>
              <thead>
                <tr>
                  <th>Check</th>
                  <th>Name</th>
                  <th>Relationship</th>
                  <th>Contact #</th>
                </tr>
              </thead>
              <tbody id ='familyTableOfStudents'>
              </tbody>
            </table>
          </div>
        </div>
        <div class='row'>
          <div class='col-md-12'>
            {!! Form::open([
              'id'=>'sendMessageReferral']) !!}
              
              <label for='messageReferral'>Message</label>
              <textarea class='form-control' id = 'messageReferral' name ='message' rows='5' required></textarea>
            {!! Form::close() !!}
          </div>
        </div> 
        <div class='row'>
          <div class='col-md-12 tableModalContainer'>
            <strong>Sent Messages</strong>
            <table class='table table-striped'>
              <thead>
                <tr>
                  <th>Name Of Family</th>
                  <th>Contact No</th>
                  <th>Message</th>
                </tr> 
              </thead>
              <tbody id='referralSentMessagesTable'>
              </tbody>
            </table>
          </div> 
        </div>
      @endslot
      @slot('modalbutton')
          <button class="btn btn-warning" form ='sendMessageReferral'><i class='fa fa-save'></i>&nbsp;Send</button>
      @endslot
    @endcomponent
    @include('Partials.Modals.referral.addBehaviour')
    @include('Partials.Modals.referral.referredToList')
    @include('Partials.Modals.contract.studentBrowse')
    @include('Partials.Modals.statusMessage')
  </div>


@endSection
@section('scripts')
  <script src = '{{asset('js/parsley.min.js')}}'></script>
  <script src = '{{asset('js/jqueryPagination.js')}}'></script>
  <script src = '{{asset('js/referral/referral.js')}}'></script>
@stop
