<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\model\useraccount;
use App\model\schoolYear;
use App\model\systemDate;
use App\Http\Controllers\student\studentController;
use Carbon\Carbon;
use Auth;
use Session;
class myuserlogin extends Controller
{
  public function __construct(){
    $this->middleware('guest:myuser', ['except'=>['logoutUser']]);
    //$this->middleware('guest:admin', ['except'=>['logoutAdmin']]);
  }
  public function getLoginPage(){
     return view('authViews/login');
  }
  public function logoutUser(){
    //error_log("Logout");
    $user = useraccount::find(Auth::guard('myuser')->id());
    if($user->userType=='admin'){
      if($user->logged != null){
        $user->logged = null;
        $user->save();
      }
    }
    Auth::guard('myuser')->logout();
    return redirect(route('login'));
  }
  public function validateUser(Request $request){
    $this->validate($request, [
      'email'=>'required',
      'password'=>'required'
    ]);


    
    if(Auth::guard('myuser')->attempt(['email'=>$request->email, 'password'=>$request->password, 'verified'=>1, 'status'=>'1'], $request->remember_me)){
      $user = useraccount::find(Auth::guard('myuser')->id());
      if($user->userType=='admin'){
        if($user->logged == null){
          $user->logged = Carbon::now();
          $user->save();
        }else{
          $to_time = strtotime(Carbon::now());
          $from_time = strtotime($user->logged);
          $minutes= ($to_time - $from_time) / 60;
          error_log($minutes);
          if($minutes <60){
            Auth::guard('myuser')->logout();
            Session::flash('error', 'Your account is being used elsewhere!');
            return redirect()->back()->withInput($request->only('email', 'remember'));
          }
        }
      }
      
      session(['user'=> useraccount::select('userName', 'email', 'imageType','userType','designation', 'designationLevel', 'id')->find(Auth::guard('myuser')->id())]);
       
      //timetravel shit
        $systemdate = systemDate::all();
        $schoolyear = schoolYear::where('schoolYearStart', '<=', date('Y-m-d'))
                                ->where('schoolYearEnd', '>=', date('Y-m-d'))->get();
        if(count($systemdate)==0){
          $systemDate =  new systemDate;
          $systemDate->systemDate = date('Y-m-d');
          $systemDate->save();
          session(['systemDate'=>$systemDate->systemDate]);
        }else{
          session(['systemDate'=>$systemdate[0]->systemDate]);
        }
      $schoolyear = schoolYear::where('status', '=', '1')->get();
      if(count($schoolyear)==0){
        session(['schoolyear'=> array('year'=>'0000-0000',
                                      'sem'=>'0')]);
      }else{
        session(['schoolyear'=> array('year'=>explode('-', $schoolyear[0]->schoolYearStart)[0]." - ".explode('-', $schoolyear[0]->schoolYearEnd)[0],
                                      'sem'=>(new studentController)->getCurrentSem())]);
      }
      return redirect()->intended(route('admin.home'));
    }
    Session::flash('error', 'Authentication error!');
    return redirect()->back()->withInput($request->only('email', 'remember'));
  }
}
