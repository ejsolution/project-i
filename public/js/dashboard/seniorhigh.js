$(document).ready(function(){
    tableLoader('6', 'seniroHighListTable');
    $('#searchSeniorHighStudent').closest('div').parent().next().remove();
    $('#searchSeniorHighStudent').closest('div').parent().removeClass('col-md-3').addClass('col-md-4');
    $('#studentTypeFilter').html("<option value ='seniorhigh'>Senior High</option>").attr('readonly', true);
    $('#semesterFilter').closest('div').remove();
    $('#schoolYearFilter').children('option').each(function(){
        var temp = $(this).text().split(' ');
        console.log(temp);
        if(temp.length == 4){
           // alert("Hello");
            $('#schoolYearFilter').val($(this).val());
        }
    });
    getAjax('/dashboard/seniorhighstudents/'+$('#schoolYearFilter').val()+'/all/'+$('#courseGradeLevelFilter').val()+'/'+$('#searchSeniorHighStudent').val(), 'seniroHighListTable');
    var html = "<option value ='all'>all</option>"+
        "<option value ='12'>12</option>"+
        "<option value ='11'>11</option>";
    $('#courseGradeLevelFilter').html(html);
    $('#schoolYearFilter').on('change', function(){
        if($('#schoolYearFilter').val() != "all"){
          $.get('/report/schoolyear/'+$('#schoolYearFilter').val(), function(data){
            $('#semesterFilter').html("<option value='all'>all</option>");
            $('#semesterFilter').append(data);
            tableLoader('6', 'seniroHighListTable');
            getAjax('/dashboard/seniorhighstudents/'+$('#schoolYearFilter').val()+'/all/'+$('#courseGradeLevelFilter').val()+'/'+$('#searchSeniorHighStudent').val(), 'seniroHighListTable');
          });
        }else{
            tableLoader('6', 'seniroHighListTable');
            getAjax('/dashboard/seniorhighstudents/'+$('#schoolYearFilter').val()+'/all/'+$('#courseGradeLevelFilter').val()+'/'+$('#searchSeniorHighStudent').val(), 'seniroHighListTable');
        }
    });
    $('#semesterFilter').on('change', function(){
        tableLoader('6', 'seniroHighListTable');
        getAjax('/dashboard/seniorhighstudents/'+$('#schoolYearFilter').val()+'/all/'+$('#courseGradeLevelFilter').val()+'/'+$('#searchSeniorHighStudent').val(), 'seniroHighListTable');
    });
    $('#courseGradeLevelFilter').on('change', function(){
        tableLoader('6', 'seniroHighListTable');
        getAjax('/dashboard/seniorhighstudents/'+$('#schoolYearFilter').val()+'/all/'+$('#courseGradeLevelFilter').val()+'/'+$('#searchSeniorHighStudent').val(), 'seniroHighListTable');
    });
    $('#searchSeniorHighStudent').on('keydown', function(){
        tableLoader('6', 'seniroHighListTable');
        getAjax('/dashboard/seniorhighstudents/'+$('#schoolYearFilter').val()+'/all/'+$('#courseGradeLevelFilter').val()+'/'+$('#searchSeniorHighStudent').val(), 'seniroHighListTable');
    }); 
});