@extends('admin.adminTemplate')

@section('title', 'AdminDashboard')
@section('stylesheet')
  <link href="{{asset('css/admin/transactions.css')}}" rel="stylesheet">
  <link href="{{asset('css/admin/counseling/counseling.css')}}" rel="stylesheet">
@stop
@section('content')
  <div class='container'>
    <input type = 'hidden' id ='resched' value = {{$resched}}>
    <input type = 'hidden' id ='accept' value = {{$accept}}>
    <input type = 'hidden' id ='view' value = {{$view}}>
    <input type = 'hidden' id ='counseling' value = {{$counselingID}}>
    <input type = 'hidden' id ='studentCounseling' value = {{$studentID}}>
    <input type = 'hidden' id ='counselingTypeOf' value = {{$counselingType}}>
     
    <div class='row'>
      @component('components.headerSearchFilter', ['user'=>$user,
                                                  'collegeOfUser'=>$collegeOfUser])
        @slot('comboLabel')
          View By Student Type
        @endslot
        @slot('schoolyears')
          @foreach($schoolyears as $year)
            <option value ='{{$year->id}}'>{{explode('-', $year->schoolYearStart)[0]}} - {{explode('-', $year->schoolYearEnd)[0]}}</option>
          @endforeach
        @endslot
        @slot('searchID')
          searchCounseling
        @endslot
        @slot('placeholder')
          Search By Student Or School Year
        @endslot
        @slot('button') 
          <div class="btn-group">
            <button data-toggle="dropdown" class="btn btn-dark dropdown-toggle" type="button" aria-expanded="false">Add <span class="caret"></span>
            </button>
            <ul role="menu" class="dropdown-menu">
              <li id = 'individualCounselingButton'><a href="#">Individual Counseling</a>
              </li>
              <li id = 'groupCounselingButton'><a href="#">Group Counseling</a>
              </li>
            </ul>
          </div>
        @endslot
      @endcomponent
    </div>
    <div class='row'>
      <div class='col-md-12'>
        <ul class="nav nav-tabs">
          <li class="active"><a data-toggle="tab" href="#scheduledCounseling">Scheduled Counseling</a></li>
          <!--li><a data-toggle="tab" href="#counselingList">List Of Counseling</a></li-->
        </ul>
      </div>
    </div>
    <div class='row'>
      <div class='col-md-12'>
        <div class="tab-content">
          <div class='tab-pane fade in active' id = 'scheduledCounseling'>
            <div class='well tableContainer'>
              <table class='table table-striped'>
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th>Student Type</th>
                    <th>Course/ Grade Level</th>
                    {{-- <th>School Year</th>
                    <th>Semester</th> --}}
                    <th>Counseling Type</th>
                    <th>Date Recorded</th>
                    <th>Counseling Date</th>
                    <th>Counseling Status</th>
                    <th>View</th>
                  </tr>
                </thead> 
                <tbody id ='counselingTable'>
                </tbody>
              </table>
            </div>
            <div class='genericDivTop'>
              <center><ul id="pagination" class="pagination-sm"></ul></center>
            </div>
            <div class='alignRight'> 
              <button class='btn btn-dark' id = 'printCounselingStudents'>
                <i class='fa fa-print'>&nbsp; Print</i>
              </button>
            </div>
          </div>
        </div>  
      </div> 
    </div>
    @include('Partials.Modals.counseling.individualCounseling')
    @include('Partials.Modals.counseling.groupCounseling')
   
    @include('Partials.Modals.contract.studentBrowse')
    @component('components.modal')
      @slot('modalid')
        editIndividualCounselingModal
      @endslot
      @slot('modalsize')
        modal-lg
      @endslot
      @slot('modaltitle')
        View Counseling
      @endslot
      @slot('modalcontent')
      <div class='row'>
             
          <div class="x_panel">
            <div class="x_title">
              <h2>Student Information</h2>
              <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
              </ul>
              <div class="clearfix"></div>
            </div>
            <div class="x_content" style="display: block;">
              <br>
              <div id ='groupContainer'>
                  <div class='row'>
                      
                      <div class='col-md-12'>
                        <label>
                          Person Involved
                        </label>
                        <div class='tableModalContainer'>
                          <table class='table table-striped'>
                            <thead>
                              <tr>
                                <th>Check</th>
                                <th>Name</th>
                                <th>Contact No.</th>
                                <th>Address</th>
                                <th>Student No</th> 
                                <th>Student Type</th>
                              </tr>
                            </thead>
                            <tbody id ='studentTableForGroupCounselingEdit'>
                                
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
              </div>
              <div id ='individualContainer'>
                <div class='row'>
                  <div class='col-md-4'>
                    <label for = 'studentNameTextEdit'>Student Name</label>
                    <select id = 'studentNameTextEdit' name = 'studentID'
                    class='form-control' disabled>
                      @foreach ($student as $student)
                          <option value ='{{$student->id}}'>{{$student->firstName}} {{$student->lastName}}</option>
                      @endforeach
                    </select>
                  </div> 
                  <div class='col-md-4'>
                    <label for = 'courseNameText'>Course</label>
                    <input type = 'text' id = 'courseNameTextEdit' name = 'courseNameText'
                    class='form-control' disabled>
                  </div>
                  <div class='col-md-4'>
                      <label for = 'homeAddressText'>Home Address</label>
                      <input type = 'text' id = 'homeAddressTextEdit' name = 'homeAddressText'
                      class='form-control' disabled>
                    </div>
                </div>
                <div class='row'>
                  <div class='col-md-4'>
                    <label for = 'studentContactNoText'>Student Contact No.</label>
                    <input type = 'text' id = 'studentContactNoTextEdit' name = 'studentContactNoText'
                    class='form-control' disabled>
                  </div>
                  <div class='col-md-4'>
                    <label for = 'yearLevelGradeLevelText'>Year Level/ Grade Level</label>
                    <input type = 'text' id = 'yearLevelGradeLevelTextEdit' name = 'yearLevelGradeLevelText'
                    class='form-control' disabled>
                  </div>
                  <div class='col-md-4'>
                      <label for = 'studentAgeText'>Age</label>
                      <input type = 'text' id = 'studentAgeTextEdit' name = 'studentAgeText'
                      class='form-control' disabled>
                    </div>
                </div>
                <div class='row'>
                  <div class='col-md-4'>
                    <label for = 'studentTypeText'>Student Type</label>
                    <input type = 'text' id = 'studentTypeTextEdit' name = 'studentTypeText'
                    class='form-control' disabled>
                  </div>
                  <div class='col-md-4'>
                    
                    
                        <label for = 'studentSexText'>Sex</label>
                        <input type = 'text' id = 'studentSexTextEdit' name = 'studentSexText'
                        class='form-control' disabled>
                    
                      
                    
                  </div>
                  
                </div>
              </div>
            </div>
          </div>
          
        </div>
        <div class='row'>
          <div class="process">
           <div class="process-row nav nav-tabs">
            <div class="process-step">
             <button type="button" id = 'firstEdit' class="btn btn-info btn-circle" data-toggle="tab" href="#individualCounselingTabEdit"><i class="fa fa-info fa-3x"></i></button>
             <p><small>Individual Counseling</small></p>
            </div>
            <div class="process-step">
             <button type="button" id ='secondEdit' class="btn btn-default btn-circle" data-toggle="tab" href="#followUpTabEdit"><i class="fa fa-file-text-o fa-3x"></i></button>
             <p><small>Follow Up</small></p>
            </div>
            @if(session()->get('user')['userType']!='staff')
              <div class="process-step">
              <button type="button" id = 'thirdEdit' class="btn btn-default btn-circle" data-toggle="tab" href="#evaluationTabEdit"><i class="fa fa-image fa-3x"></i></button>
              <p><small>Evaluation</small></p>
              </div>
            @endif
           </div>
          </div>
          <div class="tab-content">
            <div class='tab-pane fade in active' id = 'individualCounselingTabEdit'>
                {!! Form::open([
                  'id'=>'counselingEditForm']) !!}
              <div class='well'>
                <div class='row'>
                  <div class='col-md-6'>
                    <label>School Year</label>
                    <input type = 'text' id = 'schoolYearTextEdit' name = 'schoolYearText'
                    class='form-control' value ='{{$schoolyear}}' disabled>
                  </div>
                  <div class='col-md-6'>
                      <label>Semester</label>
                      <input type = 'text' id = 'semesterTextEdit' name = 'schoolYearText' value ='{{$currentsem}}'
                      class='form-control' disabled>
                    </div>
                </div>
                <div class='row'>
                  
                  <div class='col-md-6'>
                    <label for = 'counselingDateTextEdit'>Counseling Date</label>
                    <input type = 'date' name = 'counselingDate' id = 'counselingDateEdit' class='form-control' required readonly>
                    <div class='genericDivTop'>
                      <button class='btn btn-warning' id = 'rescheduleButton' type ='button'>Reschedule</button>
                    </div>
                  </div>
                  <div class='col-md-6'>
                    <label for = 'dateScheduledTextEdit'>Scheduled Date</label>
                    <input type = 'date' name = 'dateScheduledText' id = 'dateScheduledTextEdit' class='form-control' required readonly>
                    
                  </div>
                  
                </div>
                <div class='row'>
                  
                  <div class='col-md-6'>
                    <div class='genericDivTop'>
                      <label for = 'walkinEdit'><input type=  'checkbox' id = 'walkinEdit' value = 'walkin' name ='walkin'>Walk-in</label>
                    </div>
                  </div>
                </div>
                <div class='row'>
                  <div class='col-md-12'>
                    <label for='statementOfTheProblemTextAreaEdit'>Statement Of The Problem</label>
                    <textarea class='form-control' id = 'statementOfTheProblemTextAreaEdit' name ='statementOfTheProblemTextArea'
                    rows='6' required></textarea>
                  </div>
                </div>
                <div class='row genericDivTop'>
                  <div class='col-md-6'>
                    <div class='col-md-4'>
                      <label for='assistedByTextEdit'>Assisted By:</label>
                    </div>
                    <div class='col-md-8'>
                      <input type = 'text' id = 'assistedByTextEdit' name = 'assistedByText' 
                      class='form-control' required readonly>
                    </div>
                  </div>
                  <div class='col-md-6'>
                      <div class='col-md-4'>
                        <label for = 'counseledByTextEdit'>Counseled By:</label>
                      </div>
                      <div class='col-md-8'>
                        {{--  <input type = 'text' id = 'counseledByEditText' name = 'counseledBy'
                        class='form-control' required>  --}}
                        <div class="input-group">
                          <input type="text" id = 'nameOfCounselorEdit' name='nameOfCounselor' readonly class="form-control">
                          <input type='hidden' name = 'counseledBy' id ='counseledByTextEdit'>
                          <span class="input-group-btn">
                            <button type="button" class="btn btn-primary" id ='chooseCounselorEdit'><i class='fa fa-plus'></i></button>
                          </span>
                        </div>
                      </div>
                    </div>
                </div>
                <div class='row alignRight genericDivTop'>
                  <ul class="list-unstyled list-inline pull-right">
                    <li><button type="button" class="btn btn-info next-step" id = 'showSecondTabEdit'>Next <i class="fa fa-chevron-right"></i></button></li>
                   </ul>
                   <button class='btn btn-dark' id ='saveEditCounselingForm'>Save</button>
                </div>
              </div>
              {!! Form::close() !!} 
            </div>
            <div class='tab-pane fade' id = 'followUpTabEdit'>
                <div class='well'>
                  {!! Form::open([
                    'id'=>'counselingEditFollowUpForm']) !!}
                    <div class='row'>
                      <label for= 'followUpTextArea'>Follow Up</label>
                      <textarea id = 'followUpTextAreaEdit' name = 'followUpTextArea' rows='3' class='form-control' required></textarea>
                    </div>
                    <div class='row'>
                      <label for ='assistedByFollowUpIndividualText'>Assisted By</label>
                      <input type ='text' id ='assistedByFollowUpIndividualText' name ='assistedBy' class='form-control' required value ='{{$nameofuser}}' disabled>
                    </div>
                    <div class='genericDivTop alignRight'>
                      <button id ='saveFollowUpFormIndividual' class='btn btn-dark'>Save</button>
                    </div>
                  {!! Form::close() !!}
                  <div class='row tableModalContainer'>
                    <table class='table table-striped'>
                      <thead>
                        <tr>
                          <th>Details</th>
                          <th>Assisted By</th>
                          <th>Date Recorded</th>
                          <th>Edit</th>
                          
                        </tr>
                      </thead>
                      <tbody id ='followUpTableIndividual'>
                      </tbody>
                    </table> 
                  </div>
                  <div class='row genericDivTop alignRight'>
                    <ul class="list-unstyled list-inline pull-right">
                      <li><button type="button" class="btn btn-default prev-step" id = 'showFirstTabEdit'><i class="fa fa-chevron-left"></i> Back</button></li>
                      @if(session()->get('user')['userType']!='staff')
                        <li><button type="button" class="btn btn-info next-step" id = 'showLastTabEdit'>Next <i class="fa fa-chevron-right"></i></button></li>
                      @endif
                    </ul>
                  </div>
              </div> 
            </div>
            @if(session()->get('user')['userType']!='staff')
            <div class='tab-pane fade' id = 'evaluationTabEdit'>
              <div class='well'>
                  {!! Form::open([
                    'id'=>'evaluationCounselingForm']) !!}
                    <div class='row'>
                      <label for= 'evaluationTextAreaEdit'>Evaluation</label>
                      <textarea id = 'evaluationTextArea' name = 'remarks' rows='3' class='form-control' required></textarea>
                    </div>
                    <div class='row'>
                      <label for ='evaluatedByText'>Evaluated By</label>
                      <input type ='text' id = 'evaluatedByText' name ='evaluatedBy' class='form-control'  value ="{{$nameofuser}}" required disabled>
                    </div>
                    <div class='genericDivTop alignRight'>
                        <button type ='button' class='btn btn-warning' id ='editEvaluationForm'>Edit</button>
                        <button class='btn btn-dark' id ='saveEvaluationForm'>Save</button>
                    </div>
                  {!! Form::close() !!}
                  <div class='row genericDivTop alignRight'>
                      <ul class="list-unstyled list-inline pull-right">
                       <li><button type="button" class="btn btn-default prev-step"  id = 'showSecondTab2Edit'><i class="fa fa-chevron-left"></i> Back</button></li>
  
                      </ul>
                  </div>
                </div>
                
              </div>
              @endif
            </div>
            
          </div>
        
        
      @endslot
      @slot('modalbutton')
      
      @endslot
    @endcomponent
    @component('components.modal')
      @slot('modalid')
        editFollowUpModal
      @endslot
      @slot('modalsize')
        modal-md
      @endslot
      @slot('modaltitle')
        Edit a followup
      @endslot
      @slot('modalcontent')
        {!! Form::open([
          'id'=>'followUpEditForm']) !!}
          <div class='row'>
              <label for= 'followUpTextArea2'>Details</label>
              <textarea id = 'followUpTextAreaEdit2' name = 'details' rows='3' class='form-control' required></textarea>
            </div>
            <div class='row'>
              <label for ='assistedByFollowUpIndividualText2'>Assisted By</label>
              <input type ='text' id ='assistedByFollowUpIndividualText2' name ='assistedBy' class='form-control' required>
            </div>
        {!! Form::close() !!}
      @endslot
      @slot('modalbutton')
        <button class='btn btn-dark' id ='followUpEditSaveButton' form ='followUpEditForm'>Save</button> 
      @endslot
    @endcomponent 
    @include('Partials.Modals.counseling.chooseCounselor')
    @include('Partials.Modals.statusMessage')
  </div>
@endSection 
@section('scripts')
  <script src = '{{asset('js/parsley.min.js')}}'></script>
  <script src = '{{asset('js/jqueryPagination.js')}}'></script>
  <script src = '{{asset('js/counseling/counseling.js')}}'></script>
@stop
 