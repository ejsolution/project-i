@component('components.modal')
    @slot('modalid')
    behaviorSettingModal
    @endslot
    @slot('modalsize')
    modal-md
    @endslot
    @slot('modaltitle')
    Behaviours
    @endslot
    @slot('modalcontent')
    {!! Form::open([
    'id'=>'behaviorAddForms']) !!}
    @component('components.x_panel')
        @slot('title')
        Add A Behaviour
        @endslot
        @slot('content')
            <div class='row'>
                <div class='col-md-12'>
                    <label for ='behaviourName'>Behaviour Name</label>
                    <input type ='text' id ='behaviourName' required maxlength ='100' class='form-control' name ='name'>
                </div>
            </div>
            <div class='row'>
                <div class='col-md-12'>
                    <label for ='behaviorDescription'>Description</label>
                    <textarea id ='behaviorDescription' required maxlength ='100' class='form-control' name ='description'></textarea>
                </div>
            </div>
            <div class='row'>
                <div class='col-md-12 genericDivTop alignRight'>
                    <button class='btn btn-dark'>Save</button>
                </div>
            </div>
        @endslot
    @endcomponent
    {!! Form::close() !!}
    {!! Form::open([
        'id'=>'behaviorEditForm']) !!}
    {!! Form::close() !!}
    <table class='table table-striped'>
        <thead>
            <tr>
                <th>Name</th>
                <th>Description</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </thead>
        <tbody id ='behaviourTable'>
        </tbody>
    </table>
    @endslot
    @slot('modalbutton')
   
    @endslot
@endcomponent