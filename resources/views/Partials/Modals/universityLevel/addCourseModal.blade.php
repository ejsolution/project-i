@component('components.modal')
  @slot('modalid')
    addCourseModal
  @endslot
  @slot('modalsize')
    modal-md
  @endslot
  @slot('modaltitle')
    course
  @endslot
  @slot('modalcontent')
    <div class='row'>
      @component('components.x_panel')
        @slot('title')
            Add A Course to Department
        @endslot
        @slot('content')
          {!! Form::open([
                  'data-parsley-validate'=>'',
                  'id'=>'courseAddForm']) !!}
          <div class='row'>
            <div class='col-md-6'>
              <label for ='courseNameText'>Course Name</label>
              <input type ='text' class='form-control' name = 'courseNameText' id = 'courseNameText' required maxlength="50">
            </div>
            <div class='col-md-6'>
              <div class='row'>
                <div class='col-md-6'>
                  <label for ='courseCodeText'>Course Code</label>
                  <input type ='text' class='form-control' name = 'courseCodeText' id = 'courseCodeText' required maxlength="5">
                </div>
                <div class='col-md-6'>
                  <label for ='courseCodeYear'>Total Year</label>
                  <input type ='number' class='form-control' name = 'courseCodeYear' id = 'courseCodeYear' required>
                </div>
              </div>
            </div>
          </div>
          <div class='row'>
            <div class='col-md-12 alignRight genericDivTop'>
              <button class='btn btn-dark'>Save Course</button>
            </div>
          </div>
          {!! Form::close() !!}
        @endslot
      @endcomponent
    </div>
    <div class='row'>
      {!! Form::open([
              'id'=>'courseEditForm']) !!}
              
      {!! Form::close() !!}
      <div class='col-md-12 tableModalContainer genericDivTop'>
        <table class='table table-striped'>
          <thead>
            <tr>
              <th>Course Code</th>
              <th>Course Name</th>
              <th>Course Year</th>
              <th>Edit</th>
              <th>Change Department</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody id='coursesTable'>
          </tbody>
        </table>
      </div>
    </div>
  @endslot
  @slot('modalbutton')
      <!--button type="button" class="btn btn-danger" data-dismiss="modal"><i class='fa fa-save'></i>&nbsp;Save</button-->
  @endslot
@endcomponent
