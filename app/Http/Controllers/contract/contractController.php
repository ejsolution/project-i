<?php

namespace App\Http\Controllers\contract;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\contract;
use App\model\contractGrade; 
use App\model\contractProof;
use App\model\student;
use App\model\collegeStudent;
use App\model\SHSStudent;
use App\model\JHStudent;
use App\model\elemStudent;
use App\model\schoolYear;
use App\model\course;
use App\model\systemDate;
use App\model\useraccount;
use App\model\contractSetting;
use App\model\contractRemarks;
use App\Http\Controllers\activityController;
use App\Http\Controllers\student\studentController;
use App\Http\Requests\contractRequest;
use DB;
use Image;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage; 
use Session;
use Auth;
use PDF;
use App\model\adminsetting;
use Carbon\Carbon;
use App\Rules\systemTimeRule;
use App\Rules\schoolYearRule;
use App\Http\Controllers\users\userController;
use App\Http\Controllers\notification\notificationController;
class contractController extends Controller
{
    private $request;
    private $studentid;
    private $html;
    private $studentsCounter;
    private $rowCounter;
    public function __construct(){
        $this->middleware('auth:myuser');
    }
    public function getContractPage(){
        $currentsem = (new studentController)->getCurrentSem();
        $schoolyearid = (new studentController)->getCurrentSchoolYear();
        $schoolyear = schoolYear::find($schoolyearid);
        $systemdate = systemDate::all();
        $schoolyears = schoolYear::all(); 
        $nameofuser = useraccount::find(Auth::guard('myuser')->id());
        $student = student::join('college_students', 'college_students.studentID', '=', 'students.id')
                            ->whereDate('students.created_at', '<=', $systemdate[0]->systemDate)
                            ->select('students.*')->get();
        $user =  (new userController)->getUserDesignation(Auth::guard('myuser')->id());
        $college ='';
        if($user[0]=='college'){
            $college = DB::select(DB::raw("SELECT colleges.collegeName, courses.courseName, 
            courses.id FROM colleges INNER JOIN (SELECT collegeID, userID, id FROM 
            assignment_colleges INNER JOIN (SELECT max(id) AS maxid FROM assignment_colleges 
            GROUP BY userID) ac ON ac.maxid = assignment_colleges.id) ac2 ON ac2.collegeID = 
            colleges.id INNER JOIN (SELECT collegeID, departmentID, id FROM 
            department_college_hists INNER JOIN (SELECT max(id) as maxid FROM 
            department_college_hists GROUP BY departmentID) dch ON dch.maxid = 
            department_college_hists.id) dch2 ON dch2.collegeID = colleges.id 
            INNER JOIN (SELECT courseID, departmentID, id FROM course_department_hists 
            INNER JOIN (SELECT max(id) as maxid FROM course_department_hists GROUP BY courseID) 
            cdh ON cdh.maxid = course_department_hists.id) cdh2 ON cdh2.departmentID = 
            dch2.departmentID INNER JOIN courses on courses.id  = cdh2.courseID WHERE 
            ac2.collegeID = (SELECT collegeID FROM assignment_colleges WHERE id = 
            (SELECT max(id) FROM assignment_colleges WHERE userID ='".Auth::guard('myuser')->id()."'))
            GROUP BY courses.id"));
        }
        $notif = (new notificationController)->getNotifs();
        return view('admin/contract/contract',['collegeStudents' => $student,
                                                'schoolyear' =>explode('-', $schoolyear->schoolYearStart)[0].'-'.explode('-', $schoolyear->schoolYearEnd)[0],
                                                'currentsem' =>$currentsem,
                                                'schoolyears'=>$schoolyears,
                                                'nameofuser' => $nameofuser->firstName." ".$nameofuser->lastName,
                                                'systemDate' =>  $systemdate[0]->systemDate,
                                                'user'=>$user,
                                                'collegeOfUser'=>$college,
                                                'notif'=>$notif,
                                                'viewContract'=>'false',
                                                'contractID'=>'']);
    }
    public function getTotalRow($schoolyear, $semester, $course, $search = ''){
        $systemdate = systemDate::all();
        $contract;
        if($schoolyear == 'all' && $semester == 'all' && $course == 'all'){
            $contract = contract::join('college_students', 'college_students.id', '=', 'contracts.collegStudentID')
                                ->join('students', 'students.id', '=', 'college_students.studentID')
                                ->join('school_years', 'school_years.id', '=', 'college_students.schoolYearID')
                                ->join('courses', 'courses.id', '=', 'college_students.courseID')
                                ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd', 
                                        'students.firstName', 'students.lastName', 'contracts.id', 
                                        'college_students.studentID', 'courses.courseName',
                                        'contracts.contractDate', 'contracts.semesterID','contracts.type')
                                ->where(function($query) use ($search){
                                    $query->where('students.firstName', 'LIKE', "%$search%");
                                    $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                    $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                    $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                })->whereDate('contracts.created_at', '<=', $systemdate[0]->systemDate)
                                ->count();
        }elseif ($schoolyear == 'all' && $semester == 'all' && $course != 'all') {
            $contract = contract::join('college_students', 'college_students.id', '=', 'contracts.collegStudentID')
                                ->join('students', 'students.id', '=', 'college_students.studentID')
                                ->join('school_years', 'school_years.id', '=', 'college_students.schoolYearID')
                                ->join('courses', 'courses.id', '=', 'college_students.courseID')
                                ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd', 
                                        'students.firstName', 'students.lastName', 'contracts.id', 
                                        'college_students.studentID', 'courses.courseName',
                                        'contracts.contractDate', 'contracts.semesterID','contracts.type')
                                ->where(function($query) use ($search){
                                    $query->where('students.firstName', 'LIKE', "%$search%");
                                    $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                    $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                    $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                })->whereDate('contracts.created_at', '<=', $systemdate[0]->systemDate)
                                ->where('courses.id', '=', $course)
                                ->count();
        }elseif ($schoolyear == 'all' && $semester != 'all' && $course == 'all') {
            $contract = contract::join('college_students', 'college_students.id', '=', 'contracts.collegStudentID')
                                ->join('students', 'students.id', '=', 'college_students.studentID')
                                ->join('school_years', 'school_years.id', '=', 'college_students.schoolYearID')
                                ->join('courses', 'courses.id', '=', 'college_students.courseID')
                                ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd', 
                                        'students.firstName', 'students.lastName', 'contracts.id', 
                                        'college_students.studentID', 'courses.courseName',
                                        'contracts.contractDate', 'contracts.semesterID','contracts.type')
                                ->where(function($query) use ($search){
                                    $query->where('students.firstName', 'LIKE', "%$search%");
                                    $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                    $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                    $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                })->whereDate('contracts.created_at', '<=', $systemdate[0]->systemDate)
                                ->where('contracts.semesterID', '=', $semester)
                                ->count();
        }elseif ($schoolyear == 'all' && $semester != 'all' && $course != 'all') {
            $contract = contract::join('college_students', 'college_students.id', '=', 'contracts.collegStudentID')
                            ->join('students', 'students.id', '=', 'college_students.studentID')
                            ->join('school_years', 'school_years.id', '=', 'college_students.schoolYearID')
                            ->join('courses', 'courses.id', '=', 'college_students.courseID')
                            ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd', 
                                    'students.firstName', 'students.lastName', 'contracts.id', 
                                    'college_students.studentID', 'courses.courseName',
                                    'contracts.contractDate', 'contracts.semesterID','contracts.type')
                            ->where(function($query) use ($search){
                                $query->where('students.firstName', 'LIKE', "%$search%");
                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                            })->whereDate('contracts.created_at', '<=', $systemdate[0]->systemDate)
                            ->where('contracts.semesterID', '=', $semester)
                            ->where('courses.id', '=', $course)
                            ->count();
        }elseif ($schoolyear != 'all' && $semester == 'all' && $course == 'all') {
            $contract = contract::join('college_students', 'college_students.id', '=', 'contracts.collegStudentID')
                                ->join('students', 'students.id', '=', 'college_students.studentID')
                                ->join('school_years', 'school_years.id', '=', 'college_students.schoolYearID')
                                ->join('courses', 'courses.id', '=', 'college_students.courseID')
                                ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd', 
                                        'students.firstName', 'students.lastName', 'contracts.id', 
                                        'college_students.studentID', 'courses.courseName',
                                        'contracts.contractDate', 'contracts.semesterID','contracts.type')
                                ->where(function($query) use ($search){
                                    $query->where('students.firstName', 'LIKE', "%$search%");
                                    $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                    $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                    $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                })->whereDate('contracts.created_at', '<=', $systemdate[0]->systemDate)
                                ->where('contracts.schoolYearID', '=', $schoolyear)
                                ->count();
        }elseif ($schoolyear != 'all' && $semester == 'all' && $course != 'all') {
            $contract = contract::join('college_students', 'college_students.id', '=', 'contracts.collegStudentID')
                                ->join('students', 'students.id', '=', 'college_students.studentID')
                                ->join('school_years', 'school_years.id', '=', 'college_students.schoolYearID')
                                ->join('courses', 'courses.id', '=', 'college_students.courseID')
                                ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd', 
                                        'students.firstName', 'students.lastName', 'contracts.id', 
                                        'college_students.studentID', 'courses.courseName',
                                        'contracts.contractDate', 'contracts.semesterID','contracts.type')
                                ->where(function($query) use ($search){
                                    $query->where('students.firstName', 'LIKE', "%$search%");
                                    $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                    $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                    $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                })->whereDate('contracts.created_at', '<=', $systemdate[0]->systemDate)
                                ->where('contracts.schoolYearID', '=', $schoolyear)
                                ->where('courses.id', '=', $course)
                                ->count();
        }elseif ($schoolyear != 'all' && $semester != 'all' && $course == 'all') {
            $contract = contract::join('college_students', 'college_students.id', '=', 'contracts.collegStudentID')
                            ->join('students', 'students.id', '=', 'college_students.studentID')
                            ->join('school_years', 'school_years.id', '=', 'college_students.schoolYearID')
                            ->join('courses', 'courses.id', '=', 'college_students.courseID')
                            ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd', 
                                    'students.firstName', 'students.lastName', 'contracts.id', 
                                    'college_students.studentID', 'courses.courseName',
                                    'contracts.contractDate', 'contracts.semesterID','contracts.type')
                            ->where(function($query) use ($search){
                                $query->where('students.firstName', 'LIKE', "%$search%");
                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                            })->whereDate('contracts.created_at', '<=', $systemdate[0]->systemDate)
                            ->where('contracts.schoolYearID', '=', $schoolyear)
                            ->where('contracts.semesterID', '=', $semester)
                            ->count();
        }elseif ($schoolyear != 'all' && $semester != 'all' && $course != 'all') {
            $contract = contract::join('college_students', 'college_students.id', '=', 'contracts.collegStudentID')
                            ->join('students', 'students.id', '=', 'college_students.studentID')
                            ->join('school_years', 'school_years.id', '=', 'college_students.schoolYearID')
                            ->join('courses', 'courses.id', '=', 'college_students.courseID')
                            ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd', 
                                    'students.firstName', 'students.lastName', 'contracts.id', 
                                    'college_students.studentID', 'courses.courseName',
                                    'contracts.contractDate', 'contracts.semesterID','contracts.type')
                            ->where(function($query) use ($search){
                                $query->where('students.firstName', 'LIKE', "%$search%");
                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                            })->whereDate('contracts.created_at', '<=', $systemdate[0]->systemDate)
                            ->where('contracts.schoolYearID', '=', $schoolyear)
                            ->where('contracts.semesterID', '=', $semester)
                            ->where('courses.id', '=', $course)
                            ->count();
        }else{
            $contract = contract::join('college_students', 'college_students.id', '=', 'contracts.collegStudentID')
                            ->join('students', 'students.id', '=', 'college_students.studentID')
                            ->join('school_years', 'school_years.id', '=', 'college_students.schoolYearID')
                            ->join('courses', 'courses.id', '=', 'college_students.courseID')
                            ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd', 
                                    'students.firstName', 'students.lastName', 'contracts.id', 
                                    'college_students.studentID', 'courses.courseName',
                                    'contracts.contractDate', 'contracts.semesterID', 'contracts.type')
                            ->where(function($query) use ($search){
                                $query->where('students.firstName', 'LIKE', "%$search%");
                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                            })->whereDate('contracts.created_at', '<=', $systemdate[0]->systemDate)
                            ->count();
        }        
        return $contract;
    }
    public function getContractList($page, $schoolyear, $semester, $course, $search=''){
        $skip = ($page-1)*10;
        $systemdate = systemDate::all();
        $contract;
        if($schoolyear == 'all' && $semester == 'all' && $course == 'all'){
            $contract = contract::join('college_students', 'college_students.id', '=', 'contracts.collegStudentID')
                                ->join('students', 'students.id', '=', 'college_students.studentID')
                                ->join('school_years', 'school_years.id', '=', 'college_students.schoolYearID')
                                ->join('courses', 'courses.id', '=', 'college_students.courseID')
                                ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd', 
                                        'students.firstName', 'students.lastName', 'contracts.id', 
                                        'college_students.studentID', 'courses.courseName',
                                        'contracts.contractDate', 'contracts.semesterID','contracts.type')
                                ->where(function($query) use ($search){
                                    $query->where('students.firstName', 'LIKE', "%$search%");
                                    $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                    $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                    $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                })->whereDate('contracts.created_at', '<=', $systemdate[0]->systemDate)
                                ->limit(10)->offset($skip)
                                ->get();
        }elseif ($schoolyear == 'all' && $semester == 'all' && $course != 'all') {
            $contract = contract::join('college_students', 'college_students.id', '=', 'contracts.collegStudentID')
                                ->join('students', 'students.id', '=', 'college_students.studentID')
                                ->join('school_years', 'school_years.id', '=', 'college_students.schoolYearID')
                                ->join('courses', 'courses.id', '=', 'college_students.courseID')
                                ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd', 
                                        'students.firstName', 'students.lastName', 'contracts.id', 
                                        'college_students.studentID', 'courses.courseName',
                                        'contracts.contractDate', 'contracts.semesterID','contracts.type')
                                ->where(function($query) use ($search){
                                    $query->where('students.firstName', 'LIKE', "%$search%");
                                    $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                    $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                    $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                })->whereDate('contracts.created_at', '<=', $systemdate[0]->systemDate)
                                ->where('courses.id', '=', $course)
                                ->limit(10)->offset($skip)
                                ->get();
        }elseif ($schoolyear == 'all' && $semester != 'all' && $course == 'all') {
            $contract = contract::join('college_students', 'college_students.id', '=', 'contracts.collegStudentID')
                                ->join('students', 'students.id', '=', 'college_students.studentID')
                                ->join('school_years', 'school_years.id', '=', 'college_students.schoolYearID')
                                ->join('courses', 'courses.id', '=', 'college_students.courseID')
                                ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd', 
                                        'students.firstName', 'students.lastName', 'contracts.id', 
                                        'college_students.studentID', 'courses.courseName',
                                        'contracts.contractDate', 'contracts.semesterID','contracts.type')
                                ->where(function($query) use ($search){
                                    $query->where('students.firstName', 'LIKE', "%$search%");
                                    $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                    $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                    $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                })->whereDate('contracts.created_at', '<=', $systemdate[0]->systemDate)
                                ->where('contracts.semesterID', '=', $semester)
                                ->limit(10)->offset($skip)
                                ->get();
        }elseif ($schoolyear == 'all' && $semester != 'all' && $course != 'all') {
            $contract = contract::join('college_students', 'college_students.id', '=', 'contracts.collegStudentID')
                            ->join('students', 'students.id', '=', 'college_students.studentID')
                            ->join('school_years', 'school_years.id', '=', 'college_students.schoolYearID')
                            ->join('courses', 'courses.id', '=', 'college_students.courseID')
                            ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd', 
                                    'students.firstName', 'students.lastName', 'contracts.id', 
                                    'college_students.studentID', 'courses.courseName',
                                    'contracts.contractDate', 'contracts.semesterID','contracts.type')
                            ->where(function($query) use ($search){
                                $query->where('students.firstName', 'LIKE', "%$search%");
                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                            })->whereDate('contracts.created_at', '<=', $systemdate[0]->systemDate)
                            ->where('contracts.semesterID', '=', $semester)
                            ->where('courses.id', '=', $course)
                            ->limit(10)->offset($skip)
                            ->get();
        }elseif ($schoolyear != 'all' && $semester == 'all' && $course == 'all') {
            $contract = contract::join('college_students', 'college_students.id', '=', 'contracts.collegStudentID')
                                ->join('students', 'students.id', '=', 'college_students.studentID')
                                ->join('school_years', 'school_years.id', '=', 'college_students.schoolYearID')
                                ->join('courses', 'courses.id', '=', 'college_students.courseID')
                                ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd', 
                                        'students.firstName', 'students.lastName', 'contracts.id', 
                                        'college_students.studentID', 'courses.courseName',
                                        'contracts.contractDate', 'contracts.semesterID','contracts.type')
                                ->where(function($query) use ($search){
                                    $query->where('students.firstName', 'LIKE', "%$search%");
                                    $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                    $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                    $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                })->whereDate('contracts.created_at', '<=', $systemdate[0]->systemDate)
                                ->where('contracts.schoolYearID', '=', $schoolyear)
                                ->limit(10)->offset($skip)
                                ->get();
        }elseif ($schoolyear != 'all' && $semester == 'all' && $course != 'all') {
            $contract = contract::join('college_students', 'college_students.id', '=', 'contracts.collegStudentID')
                                ->join('students', 'students.id', '=', 'college_students.studentID')
                                ->join('school_years', 'school_years.id', '=', 'college_students.schoolYearID')
                                ->join('courses', 'courses.id', '=', 'college_students.courseID')
                                ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd', 
                                        'students.firstName', 'students.lastName', 'contracts.id', 
                                        'college_students.studentID', 'courses.courseName',
                                        'contracts.contractDate', 'contracts.semesterID','contracts.type')
                                ->where(function($query) use ($search){
                                    $query->where('students.firstName', 'LIKE', "%$search%");
                                    $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                    $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                    $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                })->whereDate('contracts.created_at', '<=', $systemdate[0]->systemDate)
                                ->where('contracts.schoolYearID', '=', $schoolyear)
                                ->where('courses.id', '=', $course)
                                ->limit(10)->offset($skip)
                                ->get();
        }elseif ($schoolyear != 'all' && $semester != 'all' && $course == 'all') {
            $contract = contract::join('college_students', 'college_students.id', '=', 'contracts.collegStudentID')
                            ->join('students', 'students.id', '=', 'college_students.studentID')
                            ->join('school_years', 'school_years.id', '=', 'college_students.schoolYearID')
                            ->join('courses', 'courses.id', '=', 'college_students.courseID')
                            ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd', 
                                    'students.firstName', 'students.lastName', 'contracts.id', 
                                    'college_students.studentID', 'courses.courseName',
                                    'contracts.contractDate', 'contracts.semesterID','contracts.type')
                            ->where(function($query) use ($search){
                                $query->where('students.firstName', 'LIKE', "%$search%");
                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                            })->whereDate('contracts.created_at', '<=', $systemdate[0]->systemDate)
                            ->where('contracts.schoolYearID', '=', $schoolyear)
                            ->where('contracts.semesterID', '=', $semester)
                            ->limit(10)->offset($skip)
                            ->get();
        }elseif ($schoolyear != 'all' && $semester != 'all' && $course != 'all') {
            $contract = contract::join('college_students', 'college_students.id', '=', 'contracts.collegStudentID')
                            ->join('students', 'students.id', '=', 'college_students.studentID')
                            ->join('school_years', 'school_years.id', '=', 'college_students.schoolYearID')
                            ->join('courses', 'courses.id', '=', 'college_students.courseID')
                            ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd', 
                                    'students.firstName', 'students.lastName', 'contracts.id', 
                                    'college_students.studentID', 'courses.courseName',
                                    'contracts.contractDate', 'contracts.semesterID','contracts.type')
                            ->where(function($query) use ($search){
                                $query->where('students.firstName', 'LIKE', "%$search%");
                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                            })->whereDate('contracts.created_at', '<=', $systemdate[0]->systemDate)
                            ->where('contracts.schoolYearID', '=', $schoolyear)
                            ->where('contracts.semesterID', '=', $semester)
                            ->where('courses.id', '=', $course)
                            ->limit(10)->offset($skip)
                            ->get();
        }else{
            $contract = contract::join('college_students', 'college_students.id', '=', 'contracts.collegStudentID')
            ->join('students', 'students.id', '=', 'college_students.studentID')
            ->join('school_years', 'school_years.id', '=', 'college_students.schoolYearID')
            ->join('courses', 'courses.id', '=', 'college_students.courseID')
            ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd', 
                    'students.firstName', 'students.lastName', 'contracts.id', 
                    'college_students.studentID', 'courses.courseName',
                    'contracts.contractDate', 'contracts.semesterID', 'contracts.type','contracts.type')
            ->where(function($query) use ($search){
                $query->where('students.firstName', 'LIKE', "%$search%");
                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
            })->whereDate('contracts.created_at', '<=', $systemdate[0]->systemDate)
            ->limit(10)->offset($skip)
            ->get();
        }
        
        if(count($contract)==0){
            return "<h2>There are no contracts</h2>";
        }
        $html ='';
        $this->rowCounter = 0;
        foreach ($contract as $contracts) {
            $this->rowCounter++;
            $html .= $this->getContractRow($contracts);
        }
        return $html;
    }
    public function getContractFaster($page, $schoolyear, $semester, $course, $row,$search=''){
        $user = new userController();
        $systemdate = systemDate::all();
        
        $sql = "SELECT cs2.courseID AS courseGrade, c2.courseName, cs2.schoolYearID, cs2.studentNo, 
        cs2.yearLevel, students.firstName, students.lastName, 'college' AS studentTypes, cs2.semesterID,
        contracts.id, contracts.contractDate, contracts.semesterID, contracts.type, cs2.studentID,
        students.middleName,
        school_years.schoolYearStart,
        school_years.schoolYearEnd  FROM students 
        INNER JOIN (SELECT studentID, studentNo,
        id, courseID, schoolYearID, yearLevel, semesterID FROM college_students  INNER JOIN (SELECT max(id) as maxid 
        FROM college_students 
        GROUP BY studentID) cs ON cs.maxid = college_students.id) cs2 ON cs2.studentID = students.id 
        INNER JOIN (SELECT c.id AS courseID, cdh.departmentID, c.courseName FROM courses c INNER JOIN (SELECT
        id, courseID, departmentID FROM course_department_hists cd INNER JOIN (SELECT max(id) as maxid FROM 
        course_department_hists GROUP BY courseID) cd2 ON cd2.maxid = cd.id) cdh ON cdh.courseID = c.id) c2 ON c2.courseID = cs2.courseID 
        INNER JOIN (SELECT departmentID, id, collegeID FROM department_college_hists
        INNER JOIN (SELECT max(id) as maxid FROM department_college_hists GROUP BY departmentID) 
        depc ON depc.maxid = department_college_hists.id) depc2 ON depc2.departmentID=c2.departmentID 
        INNER JOIN (SELECT collegeID, userID, id FROM assignment_colleges INNER JOIN (SELECT max(id) 
        as maxid FROM assignment_colleges GROUP BY userID) assc ON assc.maxid = assignment_colleges.id) 
        assc2 ON assc2.collegeID = depc2.collegeID 
        INNER JOIN contracts ON contracts.collegStudentID = cs2.id
        INNER JOIN school_years ON school_years.id = contracts.schoolYearID
        WHERE  DATE(contracts.created_at) <= '".$systemdate[0]->systemDate."'
        AND (students.firstName LIKE '%$search%' OR students.lastName LIKE '%$search%'
        OR contracts.type LIKE '%$search%' OR contracts.contractDate LIKE '%$search%')";
        if(session()->get('user')['userType']=='admin'){
            $sql = "SELECT cs2.courseID AS courseGrade, c2.courseName, cs2.schoolYearID, cs2.studentNo, 
            cs2.yearLevel, students.firstName, students.lastName, 'college' AS studentTypes, cs2.semesterID,
            contracts.id, contracts.contractDate, contracts.semesterID, contracts.type, cs2.studentID,
            school_years.schoolYearStart,
            students.middleName,
            school_years.schoolYearEnd  FROM students 
            INNER JOIN (SELECT studentID, studentNo,
            id, courseID, schoolYearID, yearLevel, semesterID FROM college_students  INNER JOIN (SELECT max(id) as maxid 
            FROM college_students 
            GROUP BY studentID) cs ON cs.maxid = college_students.id) cs2 ON cs2.studentID = students.id 
            INNER JOIN (SELECT c.id AS courseID, cdh.departmentID, c.courseName FROM courses c INNER JOIN (SELECT
            id, courseID, departmentID FROM course_department_hists cd INNER JOIN (SELECT max(id) as maxid FROM 
            course_department_hists GROUP BY courseID) cd2 ON cd2.maxid = cd.id) cdh ON cdh.courseID = c.id) c2 ON c2.courseID = cs2.courseID 
            INNER JOIN (SELECT departmentID, id, collegeID FROM department_college_hists
            INNER JOIN (SELECT max(id) as maxid FROM department_college_hists GROUP BY departmentID) 
            depc ON depc.maxid = department_college_hists.id) depc2 ON depc2.departmentID=c2.departmentID 
             
            INNER JOIN contracts ON contracts.collegStudentID = cs2.id
            INNER JOIN school_years ON school_years.id = contracts.schoolYearID
            WHERE  DATE(contracts.created_at) <= '".$systemdate[0]->systemDate."'
            AND (students.firstName LIKE '%$search%' OR students.lastName LIKE '%$search%'
            OR contracts.type LIKE '%$search%' OR contracts.contractDate LIKE '%$search%')";
            if($schoolyear != 'all'){
                $sql.=" AND contracts.schoolYearID = '$schoolyear'";
            }
            if($semester != 'all'){
                $sql.=" AND contracts.semesterID = '$semester'";
            }
            if($course != 'all'){
                $sql.=" AND cs2.courseID = '$course'";
            }
        }else{
            $sql .= " AND assc2.userID ='".Auth::guard('myuser')->id()."'";
            if($schoolyear != 'all'){
                $sql.=" AND contracts.schoolYearID = '$schoolyear'";
            }
            if($semester != 'all'){
                $sql.=" AND contracts.semesterID = '$semester'";
            }
            if($course != 'all'){
                $sql.=" AND cs2.courseID = '$course'";
            }
        }
        $sql .= " GROUP BY contracts.id
        ORDER BY contracts.contractDate DESC, students.lastName ASC
        ";
        //error_log($sql);
        if($row == "true"){
            $contract = DB::select(DB::raw($sql));
            return count($contract);
        }else{
            $skip = ($page-1)*10;
            $sql .=" LIMIT $skip, 10";
            $contract = DB::select(DB::raw($sql));
            if(count($contract)==0){
                return "<h2>No contracts found.</h2>";
            }else{
                $html = '';
                $this->rowCounter=0;
                foreach ($contract as $contracts) {
                    $this->rowCounter++;
                    $html .= $this->getContractRow($contracts);
                }
                return $html;
            }
        }
    }
    public function getContractRow($contract){
        $contracttype = explode(" ", $contract->type);
        $type = '';
        for ($i=0; $i < count($contracttype); $i++) { 
            $type .= ucfirst($contracttype[$i])." ";
        }
        return "<tr id ='contractRow-$contract->id'>
                    <td>".$this->rowCounter."</td>
                    <td>".$this->getBeautifiedName($contract->firstName, $contract->middleName, $contract->lastName)."</td>
                    <td>$contract->contractDate</td>
                    <td>".explode('-', $contract->schoolYearStart)[0]."-".explode('-', $contract->schoolYearEnd)[0]."</td>
                    <td>$type</td>
                    <td>$contract->semesterID</td>
                    <td>$contract->courseName</td>
                    <td>
                        <button class='btn btn-dark viewContract' id= 'viewContract-$contract->id'>
                            <i class='fa fa-eye'></i>
                        </button>
                    </td>
                </tr>";
    }
    public function getBeautifiedName($firstname, $middlename, $lastname){
        $middlename2 = '';
        if($middlename != ''){
            $middlename2 = ucfirst(substr($middlename, 0, 1)).".";
        }
        return $lastname.", ".$firstname." $middlename2";
    }
    public function getCollegeStudentData($studentid){
        $student = collegeStudent::join('courses', 'courses.id', '=', 'college_students.courseID')
                                    ->select('courses.courseName', 'college_students.yearLevel')
                                    ->where('college_students.studentID', '=', $studentid)
                                    ->where('college_students.id', '=', DB::raw("(SELECT MAX(college_students.id) FROM college_students WHERE studentID = ".$studentid.")"))
                                    ->get();
        return $student;
    }
    public function insertContract(contractRequest $request){
        $this->request = $request;
        
         $student = collegeStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM college_students WHERE studentID = ".$this->request->studentID.")"))->get();
        if(count($student)==0){
            return "nostudent";
        }
        $this->studentid = $student[0]->id;
        DB::transaction(function() {
            $contract = new contract;
            $contract->collegStudentID =  $this->studentid;
            $contract->type = $this->request->typeOfContract;
            $contract->remarks = $this->request->remarksContract;
            $contract->notedBy = $this->request->notedByContract;
            $contract->contractDate = $this->request->contractDate;
            $schoolyearActive = schoolYear::where('status', '=', '1')->get();
            $contract->schoolYearID =$schoolyearActive[0]->id; 
            $contract->semesterID = (new studentController)->getCurrentSem();
            $contract->others = $this->request->others;
            $contract->save();
            (new notificationController)->checkContractNotif($contract->id);
            $myitems=[];
            $arr = json_decode($this->request->subjectgrades);
            for($i=0; $i<count($arr); $i++){
                $myitems[$i]['units']= $arr[$i]->units;
                $myitems[$i]['subjectCode']= $arr[$i]->subjectcode;
                $myitems[$i]['grade']= $arr[$i]->grades;
                $myitems[$i]['contractID']= $contract->id;
            }
            DB::table("contract_grades")->insert($myitems);
            foreach (Input::file('uploadPhotosContract') as $photo) {
            //foreach ($this->request->uploadPhotosContract as $photo) {
                $contractphoto =  new contractProof;
                $contractphoto->contractID = $contract->id;
                $contractphoto->proofPhotoType = $photo->getClientOriginalExtension();
                $contractphoto->save();
                $filename = $contractphoto->id.'.'.$photo->getClientOriginalExtension();
                $location = public_path("images-database/contractproof/". $filename);
                Image::make($photo)->resize(200,200)->save($location);
            }
            $student = student::find($this->request->studentID);
            $contract->firstName = $student->firstName;
            $contract->lastName= $student->lastName;
            $myschoolyear = collegeStudent::where('studentID', '=', $this->request->studentID)->get();
            $myschoolyear2 = schoolYear::find($myschoolyear[0]->schoolYearID);
            $contract->schoolYearStart = $myschoolyear2->schoolYearStart;
            $contract->schoolYearEnd = $myschoolyear2->schoolYearEnd;
            $course = course::find($myschoolyear[0]->courseID);
            $contract->courseName = $course->courseName;
            $this->rowCounter = ++$this->request->totalRows;
            $this->html = $this->getContractRow($contract);
        });
        activityController::insertActivity('Inserted a contract for student with student id:'.$student[0]->studentID);
        return $this->html;
    }
    public function getContractData($contractid){
        $contractdata = contract::find($contractid);
        $collegedata = collegeStudent::find($contractdata->collegStudentID);
        $studentdata = student::find($collegedata->studentID);
        $coursedata = course::find($collegedata->courseID);
        $schoolyear = schoolYear::find($collegedata->schoolYearID);
        $contractdata->courseName =$coursedata->courseName;
        $contractdata->yearLevel = $collegedata->yearLevel;
        $contractdata->studentID = $collegedata->studentID;
        $contractdata->schoolYear = explode('-', $schoolyear->schoolYearStart)[0].'-'.explode('-', $schoolyear->schoolYearEnd)[0];
        $contractdata->firstName =  $studentdata->firstName;
        $contractdata->lastName =  $studentdata->lastName;
        $contractsetting = contractSetting::all();
        if(count($contractsetting)>0){
            $contractdata->consecutive =$contractsetting[0]->consecutive; 
        }else{
            $contractdata->consecutive = 3;
        }
        $contractgrades = contractGrade::where('contractID', '=', $contractid)->get();
        $html = '';
        foreach ($contractgrades as $contractgrade) {
            $html .="<tr>
                        <td>$contractgrade->subjectCode</td>
                        <td>$contractgrade->grade</td>
                        <td>$contractgrade->units</td>
                        <td><button class='remove btn btn-dark'><i class='fa fa-eye'></i></button></td>
                    </tr>
                    ";
        }
        $contractdata->gradeHtml = $html;
        //thisImage
        $contractproof = contractProof::where('contractID', '=', $contractid)->get();
        $html ='';
        foreach ($contractproof as $contractproofs) {
            $html .="<div class='imageWrapper' id ='imageDelete-$contractproofs->id'>
                        <img src='".asset("images-database/contractproof/".$contractproofs->id.'.'.$contractproofs->proofPhotoType)."' class='thisImage'/>
                        <span class='close' id ='close-$contractproofs->id'></span>
                    </div>";
        }
        $contractdata->proofHtml = $html;
        return $contractdata;
    }
    public function deleteProof($proofid){
        $proofphoto = contractProof::find($proofid);
        //Storage::delete(public_path("images-database/contractproof/".$proofphoto->id.'.'.$proofphoto->proofPhotoType));
        unlink(public_path("images-database\contractproof\\".$proofphoto->id.'.'.$proofphoto->proofPhotoType));
        $proofphoto->forceDelete();
        return "deleted";
    }
    public function updateContract(contractRequest $request){
        $this->request = $request;
        //$student = collegeStudent::where('studentID', '=', $this->request->studentID)->get();
        $student = collegeStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM college_students WHERE studentID = ".$this->request->studentID.")"))->get();
        if(count($student)==0){
            return "nostudent";
        }
        $this->studentid = $student[0]->id;
        DB::transaction(function() {
            $contract = contract::find($this->request->id);
            $contract->collegStudentID =  $this->studentid;
            $contract->type = $this->request->typeOfContract;
            $contract->remarks = $this->request->remarksContract;
            $contract->notedBy = $this->request->notedByContract;
            $contract->contractDate = $this->request->contractDate;
            $contract->others = $this->request->others;
            //$contract->semesterID = (new studentController)->getCurrentSem();
            $contract->save();
            $grades = contractGrade::where('contractID', '=', $this->request->id)->get();
            foreach ($grades as $grade) {
                $grade->forceDelete();
            }
            $myitems=[];
            $arr = json_decode($this->request->subjectgrades);
            for($i=0; $i<count($arr); $i++){
                $myitems[$i]['units']= $arr[$i]->units;
                $myitems[$i]['subjectCode']= $arr[$i]->subjectcode;
                $myitems[$i]['grade']= $arr[$i]->grades;
                $myitems[$i]['contractID']= $contract->id;
            }
            DB::table("contract_grades")->insert($myitems);
            if(Input::hasFile('uploadPhotosContract')){
                foreach (Input::file('uploadPhotosContract') as $photo) {
                //foreach ($this->request->uploadPhotosContract as $photo) {
                    $contractphoto =  new contractProof;
                    $contractphoto->contractID = $contract->id;
                    $contractphoto->proofPhotoType = $photo->getClientOriginalExtension();
                    $contractphoto->save();
                    $filename = $contractphoto->id.'.'.$photo->getClientOriginalExtension();
                    $location = public_path("images-database/contractproof/". $filename);
                    Image::make($photo)->resize(200,200)->save($location);
                }
            }
            $student = student::find($this->request->studentID);
            $contract->firstName = $student->firstName;
            $contract->lastName= $student->lastName;
            $myschoolyear = collegeStudent::where('studentID', '=', $this->request->studentID)->get();
            $myschoolyear2 = schoolYear::find($myschoolyear[0]->schoolYearID);
            $contract->schoolYearStart = $myschoolyear2->schoolYearStart;
            $contract->schoolYearEnd = $myschoolyear2->schoolYearEnd;
            $course = course::find($myschoolyear[0]->courseID);
            $contract->courseName = $course->courseName;
            $this->rowCounter = $this->request->rowCounter;
            $this->html = $this->getContractRow($contract);
        });
        activityController::insertActivity('Updated a contract of student with student id:'.$student[0]->studentID);
        return $this->html;
    }
    public function browseStudents($studenttype, $courseGradeLevel, $schoolYear, $semester, $search = ''){
        $mystudent =  new studentController();
        $students;
        switch ($studenttype) {
            case 'all':
                if($courseGradeLevel=='all'){
                    if($schoolYear=='all'){
                        if($semester == 'all'){
                             $students = student::where('students.firstName', 'LIKE', "%$search%")
                                                 ->orWhere('students.lastName', 'LIKE', "%$search%")
                                                 ->get();
                             //$students = student::where('created_at', '<=', $systemdate[0]->systemDate)->get();
                        }else{
                            $students = student::where('students.firstName', 'LIKE', "%$search%")
                                                ->orWhere('students.lastName', 'LIKE', "%$search%")
                                                ->get();
                             //$students = student::where('created_at', '<=', $systemdate[0]->systemDate)->get();
                            foreach ($students as $key => $value) {
                                switch($mystudent->getStudentType($students[$key]->id)){
                                    case 'college':
                                        $temp = collegeStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM college_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->semesterID != $semester){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                    case 'seniorhigh':
                                        $temp = SHSStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM s_h_s_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->semesterID != $semester){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                    case 'juniorhigh':
                                        $temp = JHStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM j_h_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->semesterID != $semester){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                    case 'elementary':
                                        $temp = elemStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM elem_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->semesterID != $semester){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                }
                            }
                        }
                    }else{
                        //return "Hello";
                        if($semester == 'all'){
                            $students = student::where('students.firstName', 'LIKE', "%$search%")
                                        ->orWhere('students.lastName', 'LIKE', "%$search%")
                                        ->get();
                            //$students = student::where('created_at', '<=', $systemdate[0]->systemDate)->get();
                            foreach ($students as $key => $value) {
                                switch($mystudent->getStudentType($students[$key]->id)){
                                    case 'college':
                                        $temp = collegeStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM college_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->schoolYearID != $schoolYear){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                    case 'seniorhigh':
                                        $temp = SHSStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM s_h_s_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->schoolYearID != $schoolYear){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                    case 'juniorhigh':
                                        $temp = JHStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM j_h_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->schoolYearID != $schoolYear){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                    case 'elementary':
                                        $temp = elemStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM elem_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->schoolYearID != $schoolYear){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                }
                            }
                        }else{
                            $students = student::where('students.firstName', 'LIKE', "%$search%")
                                        ->orWhere('students.lastName', 'LIKE', "%$search%")
                                        ->get();
                            foreach ($students as $key => $value) {
                                switch($mystudent->getStudentType($students[$key]->id)){
                                    case 'college':
                                        $temp = collegeStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM college_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->schoolYearID != $schoolYear || $temp[0]->semesterID != $semester){
                                                $students->forget($key);
                                            }
                                            //return $temp[0]->schoolYearID.' '.$schoolYear;
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                    case 'seniorhigh':
                                        $temp = SHSStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM s_h_s_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->schoolYearID != $schoolYear || $temp[0]->semesterID != $semester){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                    case 'juniorhigh':
                                        $temp = JHStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM j_h_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->schoolYearID != $schoolYear || $temp[0]->semesterID != $semester){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                    case 'elementary':
                                        $temp = elemStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM elem_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->schoolYearID != $schoolYear || $temp[0]->semesterID != $semester){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                }
                            }
                        }
                    }
                }else{
                    if($schoolYear=='all'){
                        if($semester == 'all'){
                            $students = student::where('students.firstName', 'LIKE', "%$search%")
                                        ->orWhere('students.lastName', 'LIKE', "%$search%")
                                        ->get();
                             //$students = student::where('created_at', '<=', $systemdate[0]->systemDate)->get();
                            foreach ($students as $key => $value) {
                                switch($mystudent->getStudentType($students[$key]->id)){
                                    case 'college':
                                        $temp = collegeStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM college_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->courseID != $courseGradeLevel){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                    case 'seniorhigh':
                                        $temp = SHSStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM s_h_s_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->gradeLevel != $courseGradeLevel){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                    case 'juniorhigh':
                                        $temp = JHStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM j_h_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->gradeLevel != $courseGradeLevel){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                    case 'elementary':
                                        $temp = elemStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM elem_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->gradeLevel != $courseGradeLevel){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                }
                            }
                        }else {
                            $students = student::where('students.firstName', 'LIKE', "%$search%")
                                                ->orWhere('students.lastName', 'LIKE', "%$search%")
                                                ->get();
                            foreach ($students as $key => $value) {
                                switch($mystudent->getStudentType($students[$key]->id)){
                                    case 'college':
                                        $temp = collegeStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM college_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->courseID != $courseGradeLevel || $temp[0]->semesterID != $semester){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                    case 'seniorhigh':
                                        $temp = SHSStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM s_h_s_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->gradeLevel != $courseGradeLevel || $temp[0]->semesterID != $semester){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                    case 'juniorhigh':
                                        $temp = JHStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM j_h_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->gradeLevel != $courseGradeLevel || $temp[0]->semesterID != $semester){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                    case 'elementary':
                                        $temp = elemStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM elem_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->gradeLevel != $courseGradeLevel || $temp[0]->semesterID != $semester){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                }
                            }
                        }
                    }else{
                        if($semester == 'all'){
                            $students = student::where('students.firstName', 'LIKE', "%$search%")
                                                ->orWhere('students.lastName', 'LIKE', "%$search%")
                                                ->get();
                            foreach ($students as $key => $value) {
                                switch($mystudent->getStudentType($students[$key]->id)){
                                    case 'college':
                                        $temp = collegeStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM college_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->courseID != $courseGradeLevel || $temp[0]->schoolYearID != $schoolYear){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                    case 'seniorhigh':
                                        $temp = SHSStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM s_h_s_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->gradeLevel != $courseGradeLevel || $temp[0]->schoolYearID != $schoolYear){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                    case 'juniorhigh':
                                        $temp = JHStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM j_h_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->gradeLevel != $courseGradeLevel || $temp[0]->schoolYearID != $schoolYear){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                    case 'elementary':
                                        $temp = elemStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM elem_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->gradeLevel != $courseGradeLevel || $temp[0]->schoolYearID != $schoolYear){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                }
                            }
                        }else {
                            $students = student::where('students.firstName', 'LIKE', "%$search%")
                                                ->orWhere('students.lastName', 'LIKE', "%$search%")
                                                ->get();
                            foreach ($students as $key => $value) {
                                switch($mystudent->getStudentType($students[$key]->id)){
                                    case 'college':
                                        $temp = collegeStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM college_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->courseID != $courseGradeLevel || $temp[0]->schoolYearID != $schoolYear || $temp[0]->semesterID != $semester){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                    case 'seniorhigh':
                                        $temp = SHSStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM s_h_s_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->gradeLevel != $courseGradeLevel || $temp[0]->schoolYearID != $schoolYear || $temp[0]->semesterID != $semester){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                    case 'juniorhigh':
                                        $temp = JHStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM j_h_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->gradeLevel != $courseGradeLevel || $temp[0]->schoolYearID != $schoolYear || $temp[0]->semesterID != $semester){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                    case 'elementary':
                                        $temp = elemStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM elem_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->gradeLevel != $courseGradeLevel || $temp[0]->schoolYearID != $schoolYear || $temp[0]->semesterID != $semester){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                }
                            }
                        }
                    }
                } 
            break;
            case 'college':
                if($courseGradeLevel=='all'){
                    if($schoolYear=='all'){
                        if($semester == 'all'){
                            $students = student::where('students.firstName', 'LIKE', "%$search%")
                                                ->orWhere('students.lastName', 'LIKE', "%$search%")
                                                ->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='college'){
                                    $students->forget($key);
                                }
                            }
                        }else{
                            $students = student::where('students.firstName', 'LIKE', "%$search%")
                                                ->orWhere('students.lastName', 'LIKE', "%$search%")
                                                ->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='college'){
                                    $students->forget($key);
                                }else{
                                    $temp = collegeStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM college_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->semesterID != $semester){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }
                    }else{
                        if($semester == 'all'){
                            $students = student::where('students.firstName', 'LIKE', "%$search%")
                                                ->orWhere('students.lastName', 'LIKE', "%$search%")
                                                ->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='college'){
                                    $students->forget($key);
                                }else{
                                    $temp = collegeStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM college_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->schoolYearID != $schoolYear){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }else {
                            $students = student::where('students.firstName', 'LIKE', "%$search%")
                                                ->orWhere('students.lastName', 'LIKE', "%$search%")
                                                ->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='college'){
                                    $students->forget($key);
                                }else{
                                    $temp = collegeStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM college_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->schoolYearID != $schoolYear || $temp[0]->semesterID != $semester){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }
                    }
                }else{
                    if($schoolYear=='all'){
                        if($semester == 'all'){
                            $students = student::where('students.firstName', 'LIKE', "%$search%")
                                                ->orWhere('students.lastName', 'LIKE', "%$search%")
                                                ->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='college'){
                                    $students->forget($key);
                                }else{
                                    $temp = collegeStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM college_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->courseID != $courseGradeLevel){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }else {
                            $students = student::where('students.firstName', 'LIKE', "%$search%")
                                                ->orWhere('students.lastName', 'LIKE', "%$search%")
                                                ->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='college'){
                                    $students->forget($key);
                                }else{
                                    $temp = collegeStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM college_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->courseID != $courseGradeLevel || $temp[0]->semesterID != $semester){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }
                    }else{
                        if($semester == 'all'){
                            $students = student::where('students.firstName', 'LIKE', "%$search%")
                                                ->orWhere('students.lastName', 'LIKE', "%$search%")
                                                ->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='college'){
                                    $students->forget($key);
                                }else{
                                    $temp = collegeStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM college_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->courseID != $courseGradeLevel || $temp[0]->schoolYearID != $schoolYear){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }else {
                            $students = student::where('students.firstName', 'LIKE', "%$search%")
                                                ->orWhere('students.lastName', 'LIKE', "%$search%")
                                                ->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='college'){
                                    $students->forget($key);
                                }else{
                                    $temp = collegeStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM college_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->courseID != $courseGradeLevel || $temp[0]->schoolYearID != $schoolYear || $temp[0]->semesterID != $semester){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }
                    }
                } 
            break;
            case 'seniorhigh':
                if($courseGradeLevel=='all'){
                    if($schoolYear=='all'){
                        if($semester == 'all'){
                            $students = student::where('students.firstName', 'LIKE', "%$search%")
                                                ->orWhere('students.lastName', 'LIKE', "%$search%")
                                                ->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='seniorhigh'){
                                    $students->forget($key);
                                }
                            }
                        }else {
                            $students = student::where('students.firstName', 'LIKE', "%$search%")
                                        ->orWhere('students.lastName', 'LIKE', "%$search%")
                                        ->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='seniorhigh'){
                                    $students->forget($key);
                                }else{
                                    $temp = SHSStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM s_h_s_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->semesterID != $semester){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }
                    }else{
                        if($semester == 'all'){
                            $students = student::where('students.firstName', 'LIKE', "%$search%")
                                                ->orWhere('students.lastName', 'LIKE', "%$search%")
                                                ->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='seniorhigh'){
                                    $students->forget($key);
                                }else{
                                    $temp = SHSStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM s_h_s_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->schoolYearID != $schoolYear){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }else {
                            $students = student::where('students.firstName', 'LIKE', "%$search%")
                                                ->orWhere('students.lastName', 'LIKE', "%$search%")
                                                ->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='seniorhigh'){
                                    $students->forget($key);
                                }else{
                                    $temp = SHSStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM s_h_s_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->schoolYearID != $schoolYear || $temp[0]->semesterID != $semester){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }
                    }
                }else{
                    if($schoolYear=='all'){
                        if($semester == 'all'){
                            $students = student::where('students.firstName', 'LIKE', "%$search%")
                                                ->orWhere('students.lastName', 'LIKE', "%$search%")
                                                ->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='seniorhigh'){
                                    $students->forget($key);
                                }else{
                                    $temp = SHSStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM s_h_s_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->gradeLevel != $courseGradeLevel){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }else {
                            $students = student::where('students.firstName', 'LIKE', "%$search%")
                                                ->orWhere('students.lastName', 'LIKE', "%$search%")
                                                ->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='seniorhigh'){
                                    $students->forget($key);
                                }else{
                                    $temp = SHSStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM s_h_s_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->gradeLevel != $courseGradeLevel || $temp[0]->semesterID != $semester){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }
                    }else{
                        if($semester == 'all'){
                            $students = student::where('students.firstName', 'LIKE', "%$search%")
                                                ->orWhere('students.lastName', 'LIKE', "%$search%")
                                                ->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='seniorhigh'){
                                    $students->forget($key);
                                }else{
                                    $temp = SHSStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM s_h_s_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->gradeLevel != $courseGradeLevel || $temp[0]->schoolYearID != $schoolYear){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }else {
                            $students = student::where('students.firstName', 'LIKE', "%$search%")
                                                ->orWhere('students.lastName', 'LIKE', "%$search%")
                                                ->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='seniorhigh'){
                                    $students->forget($key);
                                }else{
                                    $temp = SHSStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM s_h_s_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->gradeLevel != $courseGradeLevel || $temp[0]->schoolYearID != $schoolYear || $temp[0]->semesterID != $semester){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }
                    }
                } 
            break;
            case 'juniorhigh':
                if($courseGradeLevel=='all'){
                    if($schoolYear=='all'){
                        if($semester == 'all'){
                            $students = student::where('students.firstName', 'LIKE', "%$search%")
                                                ->orWhere('students.lastName', 'LIKE', "%$search%")
                                                ->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='juniorhigh'){
                                    $students->forget($key);
                                }
                            }
                        }else {
                            $students = student::where('students.firstName', 'LIKE', "%$search%")
                                                ->orWhere('students.lastName', 'LIKE', "%$search%")
                                                ->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='juniorhigh'){
                                    $students->forget($key);
                                }else{
                                    $temp = JHStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM j_h_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->semesterID != $semester){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }
                    }else{
                        if($semester == 'all'){
                            $students = student::where('students.firstName', 'LIKE', "%$search%")
                                                ->orWhere('students.lastName', 'LIKE', "%$search%")
                                                ->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='juniorhigh'){
                                    $students->forget($key);
                                }else{
                                    $temp = JHStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM j_h_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->schoolYearID != $schoolYear){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }else {
                            $students = student::where('students.firstName', 'LIKE', "%$search%")
                                                ->orWhere('students.lastName', 'LIKE', "%$search%")
                                                ->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='juniorhigh'){
                                    $students->forget($key);
                                }else{
                                    $temp = JHStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM j_h_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->schoolYearID != $schoolYear || $temp[0]->semesterID != $semester){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }
                    }
                }else{
                    if($schoolYear=='all'){
                        if($semester == 'all'){
                            $students = student::where('students.firstName', 'LIKE', "%$search%")
                                                ->orWhere('students.lastName', 'LIKE', "%$search%")
                                                ->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='juniorhigh'){
                                    $students->forget($key);
                                }else{
                                    $temp = JHStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM j_h_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->gradeLevel != $courseGradeLevel){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }else {
                            $students = student::where('students.firstName', 'LIKE', "%$search%")
                                                ->orWhere('students.lastName', 'LIKE', "%$search%")
                                                ->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='juniorhigh'){
                                    $students->forget($key);
                                }else{
                                    $temp = JHStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM j_h_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->gradeLevel != $courseGradeLevel || $temp[0]->semesterID != $semester){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }
                    }else{
                        if($semester == 'all'){
                            $students = student::where('students.firstName', 'LIKE', "%$search%")
                                                ->orWhere('students.lastName', 'LIKE', "%$search%")
                                                ->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='juniorhigh'){
                                    $students->forget($key);
                                }else{
                                    $temp = JHStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM j_h_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->gradeLevel != $courseGradeLevel || $temp[0]->schoolYearID != $schoolYear){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }else {
                            $students = student::where('students.firstName', 'LIKE', "%$search%")
                                                ->orWhere('students.lastName', 'LIKE', "%$search%")
                                                ->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='juniorhigh'){
                                    $students->forget($key);
                                }else{
                                    $temp = JHStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM j_h_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->gradeLevel != $courseGradeLevel || $temp[0]->schoolYearID != $schoolYear || $temp[0]->semesterID != $semester){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }
                    }
                }   
            break;
            case 'elementary':
                if($courseGradeLevel=='all'){
                    if($schoolYear=='all'){
                        if($semester == 'all'){
                            $students = student::where('students.firstName', 'LIKE', "%$search%")
                                                ->orWhere('students.lastName', 'LIKE', "%$search%")
                                                ->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='elementary'){
                                    $students->forget($key);
                                }
                            }
                        }else {
                            $students = student::where('students.firstName', 'LIKE', "%$search%")
                                                ->orWhere('students.lastName', 'LIKE', "%$search%")
                                                ->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='elementary'){
                                    $students->forget($key);
                                }else{
                                    $temp = elemStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM elem_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->semesterID != $semester){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }
                    }else{
                        if($semester == 'all'){
                            $students = student::where('students.firstName', 'LIKE', "%$search%")
                                                ->orWhere('students.lastName', 'LIKE', "%$search%")
                                                ->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='elementary'){
                                    $students->forget($key);
                                }else{
                                    $temp = elemStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM elem_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->schoolYearID != $schoolYear){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }else {
                            $students = student::where('students.firstName', 'LIKE', "%$search%")
                                                ->orWhere('students.lastName', 'LIKE', "%$search%")
                                                ->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='elementary'){
                                    $students->forget($key);
                                }else{
                                    $temp = elemStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM elem_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->schoolYearID != $schoolYear || $temp[0]->semesterID != $semester){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }
                    }
                }else{
                    if($schoolYear=='all'){
                        if($semester == 'all'){
                            $students = student::where('students.firstName', 'LIKE', "%$search%")
                                                ->orWhere('students.lastName', 'LIKE', "%$search%")
                                                ->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='elementary'){
                                    $students->forget($key);
                                }else{
                                    $temp = elemStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM elem_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->gradeLevel != $courseGradeLevel){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }else {
                            $students = student::where('students.firstName', 'LIKE', "%$search%")
                                                ->orWhere('students.lastName', 'LIKE', "%$search%")
                                                ->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='elementary'){
                                    $students->forget($key);
                                }else{
                                    $temp = elemStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM elem_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->gradeLevel != $courseGradeLevel || $temp[0]->semesterID != $semester){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }
                    }else{
                        if($semester == 'all'){
                            $students = student::where('students.firstName', 'LIKE', "%$search%")
                                                ->orWhere('students.lastName', 'LIKE', "%$search%")
                                                ->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='elementary'){
                                    $students->forget($key);
                                }else{
                                    $temp = elemStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM elem_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->gradeLevel != $courseGradeLevel || $temp[0]->schoolYearID != $schoolYear){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }else {
                            $students = student::where('students.firstName', 'LIKE', "%$search%")
                                                ->orWhere('students.lastName', 'LIKE', "%$search%")
                                                ->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='elementary'){
                                    $students->forget($key);
                                }else{
                                    $temp = elemStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM elem_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->gradeLevel != $courseGradeLevel || $temp[0]->schoolYearID != $schoolYear || $temp[0]->semesterID != $semester){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }
                    }
                }
            break;
            default:
                
            break;
        }
        $html = '';
        if(count($students)==0){
            return "<h2>There are no students found!</h2>";
        }
        $this->studentsCounter = 0;
        foreach ($students as $student) {
            $html .= $this->getStudentRow($student);
        }
        return $html;
    }
    public function getStudentRow($student){
        $this->studentsCounter = $this->studentsCounter+1;
        $mystudent =  new studentController();
        $studenttype;
        $course='';
        $yearlevel='';
        $studentno='';
        $studenttype = $mystudent->getStudentType($student->id);
        if($studenttype=='college'){
            $tempCollege = collegeStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM college_students WHERE studentID = ".$student->id.")"))->get();
            if(count($tempCollege)>0){
                $course = course::find($tempCollege[0]->courseID)->courseName;
                $yearlevel = $tempCollege[0]->yearLevel;
                $studentno = $tempCollege[0]->studentNo;
            }
        }else{
            switch ($studenttype) {
                case 'seniorhigh':
                    $temp = SHSStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM s_h_s_students WHERE studentID = ".$student->id.")"))->get();
                    if(count($temp)>0){
                        $yearlevel=$temp[0]->gradeLevel;
                        $studentno = $temp[0]->studNo;
                    }
                break;
                case 'juniorhigh':
                    $temp = JHStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM j_h_students WHERE studentID = ".$student->id.")"))->get();
                    if(count($temp)>0){
                        $yearlevel=$temp[0]->gradeLevel;
                        $studentno = $temp[0]->studNo;
                    }
                break;
                case 'elementary':
                    $temp = elemStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM elem_students WHERE studentID = ".$student->id.")"))->get();
                    if(count($temp)>0){
                        $yearlevel=$temp[0]->gradeLevel;
                        $studentno = $temp[0]->studNo;
                    }
                break;
                default:
                    # code...
                    break;
            }
        }
        return "<tr id ='students-$student->id' class= 'rowClickable'>
                    <td>$this->studentsCounter</td>
                    <td>$student->firstName $student->lastName</td>
                    <td>$studentno</td>
                    <td>$studenttype</td>
                    <td>$course</td>
                    <td> $yearlevel</td>
                    <td><button class='selectStudent btn btn-dark' id ='selectStudent-$student->id'><i class='fa fa-check'></i></button></td>
                </tr>";
    }
    public function getStudentRowFaster($student){
        $gradelevel ='';
        if($student->courseName == ''){
            $gradelevel = $student->courseGrade;
        }else{
            $gradelevel = $student->yearLevel;
        }
        $studenttype = '';
        switch ($student->studentType) {
            case 'college':
                $studenttype = 'College';
            break;
            case 'seniorhigh':
                $studenttype = 'Senior High';
            break;
            case 'juniorhigh':
                $studenttype = 'Junior High';
            break;
            case 'elementary':
                $studenttype = 'Elementary';
            break;
            default:
                # code...
                break;
        }
       
        return "<tr id ='students-$student->id' class= 'rowClickable'>
                    <td>$this->studentsCounter</td>
                    <td>$student->firstName $student->lastName</td>
                    <td>$student->studentNo</td>
                    <td>$studenttype</td>
                    <td>$student->strandCourse</td>
                    <td> $gradelevel</td>
                    <td><button class='selectStudent btn btn-dark' id ='selectStudent-$student->id'><i class='fa fa-check'></i></button></td>
                </tr>";
    }
    public function browseStudentsFaster($viewby, $coursegrade, $schoolyear, $semester, $search = ''){
        $user = new userController();
        $systemdate = systemDate::all();
        $sql=''; 
        if(session()->get('user')['userType']=='admin'){
            $sql = "SELECT COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) 
                    semesterID, COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) 
                    schoolYearID, COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) studentType, 
                    COALESCE(col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) courseGrade, 
                    COALESCE(col.studentNo, shs.studNo, jhs.studNo, elem.studNo) studentNo, students.*,
                    COALESCE(col.courseName, shs.strandName) strandCourse,
                    col.courseName, col.yearLevel
                    FROM students LEFT JOIN (SELECT college_students.studentID, college_students.yearLevel, college_students.semesterID, college_students.schoolYearID, college_students.id, 'college' AS studentType, 
                    college_students.courseID, courses.courseName, college_students.studentNo FROM college_students INNER JOIN (SELECT max(id) as maxid FROM college_students GROUP 
                    BY studentID) cs ON cs.maxid = college_students.id
                    INNER JOIN courses ON courses.id = college_students.courseID) col ON col.studentID = students.id LEFT 
                    JOIN (SELECT s_h_s_students.studentID, s_h_s_students.semesterID, s_h_s_students.schoolYearID, s_h_s_students.id, 'seniorhigh' 
                    AS studentType, s_h_s_students.gradeLevel, s_h_s_students.studNo,  senior_high_strands.strandName, s_h_s_students.seniorHighStrandID
                    FROM s_h_s_students INNER JOIN (SELECT max(id) as maxid FROM s_h_s_students GROUP BY studentID) 
                    shs ON shs.maxid = s_h_s_students.id INNER JOIN senior_high_strands ON senior_high_strands.id = 
                    s_h_s_students.seniorHighStrandID) shs ON shs.studentID = students.id
                    LEFT JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'juniorhigh' AS 
                    studentType, gradeLevel, studNo  FROM j_h_students INNER JOIN (SELECT max(id) as maxid FROM j_h_students 
                    GROUP BY studentID) jhs ON jhs.maxid = j_h_students.id) jhs ON jhs.studentID = students.id LEFT 
                    JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'elementary' AS studentType, 
                    gradeLevel, studNo FROM elem_students INNER JOIN (SELECT max(id) as maxid FROM elem_students GROUP 
                    BY studentID) elem ON elem.maxid = elem_students.id) elem ON elem.studentID = students.id
            WHERE (students.firstName LIKE '%$search%' OR students.lastName LIKE '%$search%') 
            AND DATE(students.created_at) <= '".$systemdate[0]->systemDate."'";
            switch ($viewby) {
                case 'college':
                    $sql .= " AND COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) = 'college'";
                break;
                case 'seniorhigh':
                    $sql .= " AND COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) = 'seniorhigh'";
                break;
                case 'juniorhigh':
                    $sql .= " AND COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) = 'juniorhigh'";
                break;
                case 'elementary':
                    $sql .= " AND COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) = 'elementary'";
                break;
                default:
                   
                break;
            }
            if($schoolyear!="all"){
                $sql .= " AND COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) = '$schoolyear'";
            }
            if($semester!="all"){
                $sql .= " AND COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) = '$semester'";
            }
            if($coursegrade!="all"){
                $sql .= " AND COALESCE(col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) = '$coursegrade'";
            }
        }else{
            $designation = $user->getUserDesignation(Auth::guard('myuser')->id());
            $sql = "SELECT COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) 
                    semesterID, COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) 
                    schoolYearID, COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) 					studentType, 
                    COALESCE(col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) courseGrade, 
                    COALESCE(col.studentNo, shs.studNo, jhs.studNo, elem.studNo) studentNo, students.*,
                    COALESCE(col.courseName, shs.strandName) strandCourse,
                    col.courseName, col.yearLevel
                    FROM students LEFT JOIN (SELECT college_students.studentID, college_students.yearLevel, college_students.semesterID, college_students.schoolYearID, college_students.id, 'college' AS studentType, 
                    college_students.courseID, courses.courseName, college_students.studentNo FROM college_students INNER JOIN (SELECT max(id) as maxid FROM college_students GROUP 
                    BY studentID) cs ON cs.maxid = college_students.id
                    INNER JOIN courses ON courses.id = college_students.courseID) col ON col.studentID = students.id LEFT 
                    JOIN (SELECT s_h_s_students.studentID, s_h_s_students.semesterID, s_h_s_students.schoolYearID, s_h_s_students.id, 'seniorhigh' 
                    AS studentType, s_h_s_students.gradeLevel, s_h_s_students.studNo,  senior_high_strands.strandName, s_h_s_students.seniorHighStrandID
                    FROM s_h_s_students INNER JOIN (SELECT max(id) as maxid FROM s_h_s_students GROUP BY studentID) 
                    shs ON shs.maxid = s_h_s_students.id INNER JOIN senior_high_strands ON senior_high_strands.id = 
                    s_h_s_students.seniorHighStrandID) shs ON shs.studentID = students.id
                    LEFT JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'juniorhigh' AS 
                    studentType, gradeLevel, studNo  FROM j_h_students INNER JOIN (SELECT max(id) as maxid FROM j_h_students 
                    GROUP BY studentID) jhs ON jhs.maxid = j_h_students.id) jhs ON jhs.studentID = students.id LEFT 
                    JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'elementary' AS studentType, 
                    gradeLevel, studNo FROM elem_students INNER JOIN (SELECT max(id) as maxid FROM elem_students GROUP 
                    BY studentID) elem ON elem.maxid = elem_students.id) elem ON elem.studentID = students.id
                    WHERE (students.firstName LIKE '%$search%' OR students.lastName LIKE '%$search%') 
                    AND DATE(students.created_at) <= '".$systemdate[0]->systemDate."'";
            switch($designation[0]){
                case 'college':
                    $sql= "SELECT cs2.courseID AS courseGrade, c2.courseName, cs2.schoolYearID, cs2.studentNo, 
                    cs2.yearLevel, students.*, 'college' AS studentType, cs2.semesterID FROM students 
                    INNER JOIN (SELECT studentID, studentNo,
                    id, courseID, schoolYearID, yearLevel, semesterID FROM college_students  INNER JOIN (SELECT max(id) as maxid 
                    FROM college_students 
                    GROUP BY studentID) cs ON cs.maxid = college_students.id) cs2 ON cs2.studentID = students.id 
                    INNER JOIN (SELECT c.id AS courseID, dep.departmentID, c.courseName FROM courses c INNER JOIN (SELECT
                    id, courseID FROM course_department_hists cd INNER JOIN (SELECT max(id) as maxid FROM 
                    course_department_hists GROUP BY courseID) cd2 ON cd2.maxid = cd.id) cdh ON cdh.courseID = c.id
                    INNER JOIN (SELECT departmentID, id FROM course_department_hists INNER JOIN (SELECT
                    max(id) AS maxid FROM course_department_hists GROUP BY courseID) cdh1 ON cdh1.maxid = 
                    course_department_hists.id) dep ON dep.id =cdh.id) c2 ON c2.courseID = cs2.courseID 
                    INNER JOIN (SELECT departmentID, id, collegeID FROM department_college_hists
                    INNER JOIN (SELECT max(id) as maxid FROM department_college_hists GROUP BY departmentID) 
                    depc ON depc.maxid = department_college_hists.id) depc2 ON depc2.departmentID=c2.departmentID 
                    INNER JOIN (SELECT collegeID, userID, id FROM assignment_colleges INNER JOIN (SELECT max(id) 
                    as maxid FROM assignment_colleges GROUP BY userID) assc ON assc.maxid = assignment_colleges.id) 
                    assc2 ON assc2.collegeID = depc2.collegeID WHERE assc2.userID ='".Auth::guard('myuser')->id()."'
                    AND DATE(students.created_at) <= '".$systemdate[0]->systemDate."'
                    AND (students.firstName LIKE '%$search%' OR students.lastName LIKE '%$search%') ";
                    if($schoolyear != "all"){
                        $sql .= " AND cs2.schoolYearID = '$schoolyear'";
                    }
                    if($semester != "all"){
                        $sql .= " AND cs2.semesterID = '$semester'";
                    }
                    if($coursegrade != "all"){
                        $sql .= " AND cs2.courseID = '$coursegrade'";
                    }
                    
                break;
                case 'seniorhigh':
                    $sql .= " AND COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) = 'seniorhigh'
                              AND COALESCE(col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) = '".$designation[1]."'";
                     if($schoolyear != "all"){
                        $sql .= " AND COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) = '$schoolyear'";
                    }
                    if($semester != "all"){
                        $sql .= " AND COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) = '$semester'";
                    }
                break;
                case 'juniorhigh':
                    $sql .= " AND COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) = 'juniorhigh'
                                AND COALESCE(col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) = '".$designation[1]."'";
                    if($schoolyear != "all"){
                        $sql .= " AND COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) = '$schoolyear'";
                    }
                    if($semester != "all"){
                        $sql .= " AND COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) = '$semester'";
                    }
                break;
                case 'elementary':
                    $sql .= " AND COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) = 'elementary'
                                AND COALESCE(col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) = '".$designation[1]."'";
                    if($schoolyear != "all"){
                        $sql .= " AND COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) = '$schoolyear'";
                    }
                    if($semester != "all"){
                        $sql .= " AND COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) = '$semester'";
                    }
                break;
            }
        }
        $sql .= "ORDER BY students.lastName ASC";
        $students = DB::select(DB::raw($sql));
        if(count($students)==0){
            return "<h2>There are no students found.</h2>";
        }else{
            $html = '';
            $this->studentsCounter=0;
            foreach ($students as $student) {
                $this->studentsCounter++;
                $html .= $this->getStudentRowFaster($student);
            }
            return $html;
        }    
    }
    public function contractRemarksInsert(Request $request){
        $this->validate($request, array(
            'remarksStudentContract'=>'required'
        ));
        $contractremarks = new contractRemarks;
        $contractremarks->remarks = $request->remarksStudentContract;
        $contractremarks->contractID = $request->contractID;
        $contractsetting  = contractSetting::all();
        if(count($contractsetting)>0){
            $contractremarks->maxContract = $contractsetting[0]->consecutive;
        }else{
            $contractremarks->maxContract = 3;
        }
        $contractremarks->save();
        return "success";
    }
    public function getConsecutiveRemarks(){
        $systemdate = systemDate::all();
        $sql = "";
        $sql = "SELECT cs2.courseID AS courseGrade, c2.courseName, cs2.schoolYearID, cs2.studentNo, 
        cs2.yearLevel, students.firstName, students.lastName, contracts.counts, contracts2.maxid,
        contract_remarks.remarks, contract_remarks.maxContract  FROM students 
        INNER JOIN (SELECT studentID, studentNo,
        id, courseID, schoolYearID, yearLevel, semesterID FROM college_students  INNER JOIN (SELECT max(id) as maxid 
        FROM college_students 
        GROUP BY studentID) cs ON cs.maxid = college_students.id) cs2 ON cs2.studentID = students.id 
        INNER JOIN (SELECT c.id AS courseID, cdh.departmentID, c.courseName FROM courses c INNER JOIN (SELECT
        id, courseID, departmentID FROM course_department_hists cd INNER JOIN (SELECT max(id) as maxid FROM 
        course_department_hists GROUP BY courseID) cd2 ON cd2.maxid = cd.id) cdh ON cdh.courseID = c.id) c2 ON c2.courseID = cs2.courseID 
        INNER JOIN (SELECT departmentID, id, collegeID FROM department_college_hists
        INNER JOIN (SELECT max(id) as maxid FROM department_college_hists GROUP BY departmentID) 
        depc ON depc.maxid = department_college_hists.id) depc2 ON depc2.departmentID=c2.departmentID 
        INNER JOIN (SELECT collegeID, userID, id FROM assignment_colleges INNER JOIN (SELECT max(id) 
        as maxid FROM assignment_colleges GROUP BY userID) assc ON assc.maxid = assignment_colleges.id) 
        assc2 ON assc2.collegeID = depc2.collegeID 
        INNER JOIN (SELECT contracts.*, count(contracts.id) as counts FROM contracts GROUP BY contracts.collegStudentID) contracts
        ON contracts.collegStudentID = cs2.id
        LEFT JOIN (SELECT max(contracts.id) as maxid, collegStudentID FROM contracts GROUP BY contracts.collegStudentID) 
        contracts2 ON contracts2.collegStudentID = contracts.collegStudentID
         INNER JOIN contract_remarks ON contract_remarks.contractID = contracts2.maxid
        INNER JOIN school_years ON school_years.id = contracts.schoolYearID
        WHERE  DATE(contracts.created_at) <= '".$systemdate[0]->systemDate."'";
        if(session()->get('user')['userType']=='admin'){
            $sql = "SELECT cs2.courseID AS courseGrade, c2.courseName, cs2.schoolYearID, cs2.studentNo, 
            cs2.yearLevel, students.firstName, students.lastName, contracts.counts, contracts2.maxid,
            contract_remarks.remarks, contract_remarks.maxContract   FROM students 
            INNER JOIN (SELECT studentID, studentNo,
            id, courseID, schoolYearID, yearLevel, semesterID FROM college_students  INNER JOIN (SELECT max(id) as maxid 
            FROM college_students 
            GROUP BY studentID) cs ON cs.maxid = college_students.id) cs2 ON cs2.studentID = students.id 
            INNER JOIN (SELECT c.id AS courseID, cdh.departmentID, c.courseName FROM courses c INNER JOIN (SELECT
            id, courseID, departmentID FROM course_department_hists cd INNER JOIN (SELECT max(id) as maxid FROM 
            course_department_hists GROUP BY courseID) cd2 ON cd2.maxid = cd.id) cdh ON cdh.courseID = c.id) c2 ON c2.courseID = cs2.courseID 
            INNER JOIN (SELECT departmentID, id, collegeID FROM department_college_hists
            INNER JOIN (SELECT max(id) as maxid FROM department_college_hists GROUP BY departmentID) 
            depc ON depc.maxid = department_college_hists.id) depc2 ON depc2.departmentID=c2.departmentID 
            INNER JOIN (SELECT contracts.*, count(contracts.id) as counts FROM contracts GROUP BY contracts.collegStudentID) contracts
             ON contracts.collegStudentID = cs2.id
             LEFT JOIN (SELECT max(contracts.id) as maxid, collegStudentID FROM contracts GROUP BY contracts.collegStudentID) contracts2 ON contracts2.collegStudentID = contracts.collegStudentID
            INNER JOIN contract_remarks ON contract_remarks.contractID = contracts2.maxid
            INNER JOIN school_years ON school_years.id = contracts.schoolYearID
            WHERE  DATE(contracts.created_at) <= '".$systemdate[0]->systemDate."'";
        }else{
            $sql .= " AND assc2.userID ='".Auth::guard('myuser')->id()."'";
        }
        /**TODO: put validation on joining contracts with contract remarks to see if what max contract is in setting
         * 
         */
        $contractremarks = DB::select(DB::raw($sql));
        $consecutive = 3;
        $contractsetting = contractSetting::all();
        if(count($contractsetting)>0){
            $consecutive = $contractsetting[0]->consecutive;
        }
        if(count($contractremarks)>0){
            $html = '';
            
            $counter = 0;
            foreach ($contractremarks as $contractremark) {
                $counter++;
                $html .= "<tr>
                            <td>$counter</td>
                            <td>$contractremark->firstName $contractremark->lastName</td> 
                            <td>$contractremark->remarks</td> 
                            <td>$contractremark->maxContract</td>
                          </tr>";
            }
            return $html;
        }else{
            return "<h2>No students with $consecutive consecutive contracts.</h2>";
        }
    }
    public function contractCollection($studentid, &$student, $key, $studenttype, $courseGradeLevel, $schoolYear, $semester){
        $mystudent =  new studentController();
        $studenttypetemp = $mystudent->getStudentType($studentid);
        if($studenttype != "all"){
           if($studenttypetemp!=$studenttype){
                $student->forget($key);
                return true;
            }
        }
        if($courseGradeLevel != "all"){
            switch($studenttypetemp){
                case 'college':
                    $temp = collegeStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM college_students WHERE studentID = ".$studentid.")"))->get();
                    if(count($temp)>0){
                        if($temp[0]->courseID != $courseGradeLevel){
                            $student->forget($key);
                            return true;
                        }
                    }else{
                        $student->forget($key);
                        return true;
                    }
                break;
                case 'seniorhigh':
                    $temp = SHSStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM s_h_s_students WHERE studentID = ".$studentid.")"))->get();
                    if(count($temp)>0){
                        if($temp[0]->gradeLevel != $courseGradeLevel){
                            $student->forget($key);
                            return true;
                        }
                    }else{
                        $student->forget($key);
                        return true;
                    }
                break;
                case 'juniorhigh':
                    $temp = JHStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM j_h_students WHERE studentID = ".$studentid.")"))->get();
                    if(count($temp)>0){
                        if($temp[0]->gradeLevel != $courseGradeLevel){
                            $student->forget($key);
                            return true;
                        }
                    }else{
                        $student->forget($key);
                        return true;
                    }
                break;
                case 'elementary':
                    $temp = elemStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM elem_students WHERE studentID = ".$studentid.")"))->get();
                    if(count($temp)>0){
                        if($temp[0]->gradeLevel != $courseGradeLevel){
                            $student->forget($key);
                            return true;
                        }
                    }else{
                        $student->forget($key);
                        return true;
                    }
                break;
            }
        }
        
        return false;
    }
    public function contractReportPrint($studenttype, $courseGradeLevel, $schoolYear, $semester, $search = ''){
        $contract;
        $thissemester = 'all Semester';
        $thisschoolyear = 'all School year';
        if($semester!="all" && $schoolYear!= 'all'){
            switch ($semester) {
                case '1':
                    $thissemester = '1st Semester';
                break;
                case '2':
                    $thissemester = '2nd Semester';
                break;
                case '3':
                    $thissemester = '3rd Semester';
                break;
                default:
                    # code...
                    break;
            }
            $thisschoolyear = $this->getYearOfActiveSchoolYear()." School Year";
            $contract = contract::join("college_students", 'college_students.id', '=' ,'contracts.collegStudentID')
                            ->join("students", 'students.id', '=', 'college_students.studentID')
                            ->join("school_years", 'school_years.id', '=', 'college_students.schoolYearID')
                            ->select('contracts.*', 'school_years.schoolYearStart', 'school_years.schoolYearEnd',
                            'students.firstName', 'students.lastName', 'college_students.studentID')
                            ->where('contracts.semesterID', '=', $semester)
                            ->where('contracts.schoolYearID', '=', $schoolYear)
                            ->orWhere(function($query) use($search){
                                $query->where('students.firstName', 'LIKE', "%$search%");
                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                $query->orWhere('contracts.type', 'LIKE', "%$search%");
                                $query->orWhere('contracts.contractDate', 'LIKE', "%$search%");
                            })
                            ->get();
        }elseif($semester=="all" && $schoolYear== 'all'){
            $contract = contract::join("college_students", 'college_students.id', '=' ,'contracts.collegStudentID')
                            ->join("students", 'students.id', '=', 'college_students.studentID')
                            ->join("school_years", 'school_years.id', '=', 'college_students.schoolYearID')
                            ->select('contracts.*', 'school_years.schoolYearStart', 'school_years.schoolYearEnd',
                            'students.firstName', 'students.lastName', 'college_students.studentID')
                            ->where(function($query) use($search){
                                $query->where('students.firstName', 'LIKE', "%$search%");
                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                $query->orWhere('contracts.type', 'LIKE', "%$search%");
                                $query->orWhere('contracts.contractDate', 'LIKE', "%$search%");
                            })
                            ->get();
        }elseif ($semester!="all" && $schoolYear== 'all') {
            switch ($semester) {
                case '1':
                    $thissemester = '1st semester';
                break;
                case '2':
                    $thissemester = '2nd semester';
                break;
                case '3':
                    $thissemester = '3rd semester';
                break;
                default:
                    # code...
                    break;
            }
            $contract = contract::join("college_students", 'college_students.id', '=' ,'contracts.collegStudentID')
                            ->join("students", 'students.id', '=', 'college_students.studentID')
                            ->join("school_years", 'school_years.id', '=', 'college_students.schoolYearID')
                            ->select('contracts.*', 'school_years.schoolYearStart', 'school_years.schoolYearEnd',
                            'students.firstName', 'students.lastName', 'college_students.studentID')
                            ->where('contracts.semesterID', '=', $semester)
                            ->orWhere(function($query) use($search){
                                $query->where('students.firstName', 'LIKE', "%$search%");
                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                $query->orWhere('contracts.type', 'LIKE', "%$search%");
                                $query->orWhere('contracts.contractDate', 'LIKE', "%$search%");
                            })
                            ->get();
        }else{
            $thisschoolyear = $this->getYearOfActiveSchoolYear()." School Year";
            $contract = contract::join("college_students", 'college_students.id', '=' ,'contracts.collegStudentID')
                            ->join("students", 'students.id', '=', 'college_students.studentID')
                            ->join("school_years", 'school_years.id', '=', 'college_students.schoolYearID')
                            ->select('contracts.*', 'school_years.schoolYearStart', 'school_years.schoolYearEnd',
                            'students.firstName', 'students.lastName', 'college_students.studentID')
                            ->where('contracts.schoolYearID', '=', $schoolYear)
                            ->orWhere(function($query) use($search){
                                $query->where('students.firstName', 'LIKE', "%$search%");
                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                $query->orWhere('contracts.type', 'LIKE', "%$search%");
                                $query->orWhere('contracts.contractDate', 'LIKE', "%$search%");
                            })
                            ->get();
        }
        

        $html ='';
        if(count($contract)==0){
            $html = "<tr><h2>There are no contracts found</h2></tr>";
        }else{
            $html ='';
            $counter = 0;
            foreach ($contract as $key => $value) {
                error_log($contract[$key]->studentID);
                if($this->contractCollection($contract[$key]->studentID, $contract, $key, $studenttype, $courseGradeLevel, $schoolYear, $semester)){
                    continue;
                }
                $type = '';
                $contracttype = explode(" ", $contract[$key]->type);
                for ($i=0; $i < count($contracttype); $i++) { 
                    $type .= ucfirst($contracttype[$i])." ";
                }
                $html .= "<tr>
                            <td>".(++$counter)."</td>
                            <td>".$contract[$key]->firstName." ".$contract[$key]->lastName."</td>
                            <td>".date("F j, Y", strtotime($contract[$key]->contractDate))."</td>
                            <td>".$type."</td>
                            <td>".$contract[$key]->remarks."</td>
                            <td>".$contract[$key]->notedBy."</td>
                        </tr>";
            }
            if($html ==""){
                $html = "<tr><h2>There are no contracts found</h2></tr>";
            }
        
        }
        $title = "List of Students with Contracts for ".$thissemester." and ".$thisschoolyear;
        $myhtml = "
        <html>
            <head>
                <style>
                    table {
                        font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif;
                        border-collapse: collapse;
                        width: 100%;
                    }
                    .well{
                        min-height: 20px;
                        padding: 19px;
                        margin-bottom: 20px;
                        background-color: #f5f5f5;
                        border: 1px solid #e3e3e3;
                        border-radius: 4px;
                        -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
                        box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
                    }
                    table td, table th {
                        border: 1px solid #ddd;
                        padding: 8px;
                    }
                    
                    table tr:nth-child(even){background-color: #f2f2f2;}
                    
                    table tr:hover {background-color: #ddd;}
                    
                    table th {
                        padding-top: 12px;
                        padding-bottom: 12px;
                        text-align: left;
                        background-color: #4CAF50;
                        color: white;
                    }
                    .filterContainer{
                        display:inline-block;
                        width:23%;
                        
                    }
                    @page{
                        margin-left:20px;
                        margin-right:20px;
                        margin-top:160px;
                      }
                    .imageWrapper {
                        float:left;
                    
                    }
                    .thisImage{
                    height:200px;
                    width:150px;
                    }
                    
                </style>
            </head>
            <body>
                ".$this->getAdminSetting()."
                <div style = 'margin-bottom:20px; margin-top:20px;'>
                    <center><h2 style = 'text-decoration:underline;'>$title</h2></center>
                </div>
                <table>
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Student</th>
                            <th>Contract Date</th>
                            <th>Type</th>
                            <th>Remarks</th>
                            <th>Noted By</th>
                        </tr>
                    </thead>
                    <tbody>

                 
        ";
        $myhtml .= $html;
        $html.="
                        </tbody>
                    </table>
                </body>
            </html>";
        
        PDF::loadHTML($myhtml)->save(public_path("temp/".'report.pdf'));
        return "true";
    }
    public function getAdminSetting(){
        $temp = new \stdClass();
        $count = adminsetting::all();
        if(count($count)>0){
            $adminsetting = adminsetting::find($count[0]->id);
            $html = "<header style = 'position:fixed; top:-100px; left:0px; right:0px; height: 60px;'>
                        <div style = 'width:19%; display:inline-block;  padding: 0px 0px 0px 50px;'>
                            <img style = 'height:100px; width:100px; border-radius:50px;' src = 'images-database/adminsetting/schoolimage/$adminsetting->id.$adminsetting->schoolSealImageType'>
                        </div>
                        <div style = 'width:50%; height:120px; display:inline-block;  margin:0px;'>
                            <center>
                                <h4 class='header' style='color:black; margin:0px;'>$adminsetting->schoolName</h4>
                                <h4 class='header' style='color:black; margin:0px;'>$adminsetting->guidanceName</h4>
                                <h4 class='header' style='color:black; margin:0px;'>$adminsetting->streetAddress, $adminsetting->barangay</h4>
                                <h4 class='header' style='color:black; margin:0px;'>$adminsetting->city</h4>
                                <h4 class='header' style='color:black; margin:0px;'>Philippines, 7000</h4>
                            </center>    
                        </div>
                        <div style = 'width:15%; display:inline-block; padding: 0px 50px 0px 0px;  text-align: right;'>
                            <img style = 'height:100px; width:100px; border-radius:50px;' src = 'images-database/adminsetting/guidanceimage/$adminsetting->id.$adminsetting->guidanceSealImageType'>
                        </div>
                    </header>";
            return $html;
        }else{
            return "";
        }
    }
}
