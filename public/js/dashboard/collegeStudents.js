$(document).ready(function(){
    tableLoader('6', 'collegeStudentsTable');
    $('#searchCollegeStudent').closest('div').parent().next().remove();
    $('#searchCollegeStudent').closest('div').parent().removeClass('col-md-3').addClass('col-md-4');
    $('#studentTypeFilter').val('college').attr('disabled', true);
    $('#semesterFilter').closest('div').removeClass('col-md-1').addClass('col-md-2');
    $('#schoolYearFilter').children('option').each(function(){
        var temp = $(this).text().split(' ');
        console.log(temp);
        if(temp.length == 4){
           // alert("Hello");
            $('#schoolYearFilter').val($(this).val());
        }
    });
    getAjax('/dashboard/collegestudents/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/'+$('#searchCollegeStudent').val(), 'collegeStudentsTable');
    $.get('/report/college/course', function(data){
        $('#courseGradeLevelFilter').html("<option value='all'>all</option>");
        $('#courseGradeLevelFilter').append(data);
       
    });
    $('#schoolYearFilter').on('change', function(){
        if($('#schoolYearFilter').val() != "all"){
          $.get('/report/schoolyear/'+$('#schoolYearFilter').val(), function(data){
            $('#semesterFilter').html("<option value='all'>all</option>");
            $('#semesterFilter').append(data);
            tableLoader('6', 'collegeStudentsTable');
            getAjax('/dashboard/collegestudents/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/'+$('#searchCollegeStudent').val(), 'collegeStudentsTable');
          });
        }else{
            tableLoader('6', 'collegeStudentsTable');
            getAjax('/dashboard/collegestudents/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/'+$('#searchCollegeStudent').val(), 'collegeStudentsTable');
        }
    });
    $('#semesterFilter').on('change', function(){
        tableLoader('6', 'collegeStudentsTable');
        getAjax('/dashboard/collegestudents/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/'+$('#searchCollegeStudent').val(), 'collegeStudentsTable');
    });
    $('#courseGradeLevelFilter').on('change', function(){
        tableLoader('6', 'collegeStudentsTable');
        getAjax('/dashboard/collegestudents/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/'+$('#searchCollegeStudent').val(), 'collegeStudentsTable');
    });
    $('#searchCollegeStudent').on('keydown', function(){
        tableLoader('6', 'collegeStudentsTable');
        getAjax('/dashboard/collegestudents/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/'+$('#searchCollegeStudent').val(), 'collegeStudentsTable');
    }); 
});