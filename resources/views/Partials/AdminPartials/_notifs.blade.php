<li role="presentation" class="dropdown">
  <a href="javascript:;"  class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
    <i class="fa fa-bell-o"></i>
    @if($notif['ignored']>0)
      <span class="badge bg-green">{{$notif['ignored']}}</span>
    @endif
  </a>
  <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
    @foreach($notif['notif'] as $notifs)
      @if($notifs->type =='counseling')
        @if($notifs->accepted == '0')
        <li>
          <a>
            <span>
              <span><b>Counseling</b></span>
            </span>
            <span class="message">
              {{$notifs->message}}
            </span>
            <span class="message">
              <div class='row'>
                <div class='col-md-6'>
                  <a class='btn btn-danger' href="{{route('notification.resched.counseling', $notifs->counselingID)}}">Reschedule</a>
                </div>
                <div  class='col-md-6'>
                <a  class='btn btn-warning' href = "{{route('notification.accept.counseling', ['counselingid'=>$notifs->counselingID,
                                                                                                'notifid'=>$notifs->id])}}">Accept</a>
                </div>
              </div>
            </span>
          </a>
        </li>
        @else
        <li>
          <a>
            <span>
              <span><b>Counseling</b></span>
            </span>
            <span class="message">
              A counseling is accepted
            </span>
            <span class="message">
              <a class='btn btn-warning' href ="{{route('notification.view.counseling', $notifs->counselingID)}}">View</a>
            </span>
          </a>
        </li>
        @endif
      @endif
    @endforeach
    @foreach($notif['notifContract'] as $notifs)
      @if($notifs->type =='contract')
        @if($notifs->accepted == '0')
          <li>
            <a>
              <span>
                <span><b>Contract</b></span>
              </span>
              <span class="message">
                {{$notifs->message}}
              </span>
              <span class="message">
                <a  class='btn btn-warning' href = "{{route('notification.view.contract', $notifs->contractID)}}">View</a>
              </span>
            </a>
          </li>
        @endif
      @endif
    @endforeach
    
  </ul>
</li>
<li role="presentation" class="dropdown">
  <a href="{{route('messages')}}">
    <i class="fa fa-envelope-o"></i>
    <!--span class="badge bg-green">6</span-->
  </a>
</li>
