var totalRow;
var totalPages;
var visiblePages;
var mypage; 
var actionContract;
$(document).ready(function(){
  var contractid;
  $.get('/report/college/course', function(data){
    $('#courseGradeLevelFilter').html("<option value='all'>all</option>");
    $('#courseGradeLevelFilter').append(data);
  });
  $.get('/contract/remarks/consecutive', function(data){
    $("#consecutiveContractsTable").html(data);
  });
  if($("#viewContract").val()=="true"){
    contractid = $("#contractIDView").val();
    //$('#editContractModal').modal('show');
    $("#remarksContractModal").modal("show");
    $.get('/contract/data/'+contractid, function(data){
      $("#studentNameRemarks").text(data.firstName+" "+data.lastName+"'s remarks for his/her "+data.consecutive+" consecutive contracts.");
      // $('#gradesTableEdit').html(data.gradeHtml);
      // var totalgrades=0;
      // $('#gradesTableEdit').children('tr').each(function(){
      //   i=0;
      //   $(this).children('td').each(function(){
      //     if(i==1){
      //       totalgrades = parseFloat(totalgrades) + parseFloat($(this).html()); 
      //     }
      //     i++;
      //   });
      // });
      // $('#totalGradesEdit').val(totalgrades);
      // $('#photosCurrentlyUploaded').html(data.proofHtml);
      // $('#studentsComboBoxEdit').val(data.studentID);
      // $('#courseContractEdit').val(data.courseName);
      // $('#courseYearContractEdit').val(data.yearLevel);
      // $('#contractDateEdit').val(data.contractDate);
      // $('#schoolYearContractEdit').val(data.schoolYear);
      // $('#semesterContractEdit').val(data.semesterID);
      // // $('#typeOfContractEdit').val(data.type);
      // if(data.type!="inc" && data.type != "pregnant"){
      //   $('#typeOfContractEdit').val('etc');
      // }else{
      //   $('#typeOfContractEdit').val(data.type);
      // }
      // $('#remarksContractEdit').val(data.remarks);
      // $('#notedByContractEdit').val(data.notedBy);
    });
   
  }
  $("#addStudentButton").on('click', function(){
    $('#addContractModal').modal('show');
  }); 
  $("#courseGradeLevelFilterLabel").text("Course");
  $("#viewByStudentType").val("college");
  $("#viewByStudentType").attr("disabled", true);
  $('#studentTypeFilter').attr('disabled', true);
  $('#studentTypeFilter').val('college');
  $('#searchContract').on('keydown', function(){
    tableLoader('8', 'contractTable');
    $('#pagination').twbsPagination('destroy');
    $.get('/contract/pages/1/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/true/'+$('#searchContract').val(), function(data){
      initializePages(data);
      $('#pagination').twbsPagination({
          totalPages: totalPages,
          visiblePages: visiblePages,
          onPageClick: function(event, page){
              mypage = page;
              tableLoader('8', 'contractTable');
              getAjax('/contract/pages/'+page+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/false/'+$('#searchContract').val(), 'contractTable');
          }
      });
    });
  });
  $('#schoolYearFilter').on('change', function(){
    if($('#schoolYearFilter').val() != "all"){
      $.get('/report/schoolyear/'+$('#schoolYearFilter').val(), function(data){
        $('#semesterFilter').html("<option value='all'>all</option>");
        $('#semesterFilter').append(data);
        tableLoader('8', 'contractTable');
        $('#pagination').twbsPagination('destroy');
        $.get('/contract/pages/1/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/true/'+$('#searchContract').val(), function(data){
          initializePages(data);
          $('#pagination').twbsPagination({
              totalPages: totalPages,
              visiblePages: visiblePages,
              onPageClick: function(event, page){
                  mypage = page;
                  tableLoader('8', 'contractTable');
                  getAjax('/contract/pages/'+page+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/false/'+$('#searchContract').val(), 'contractTable');
              }
          });
        });
      });
    }else{
      tableLoader('8', 'contractTable');
      $('#pagination').twbsPagination('destroy');
      $.get('/contract/pages/1/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/true/'+$('#searchContract').val(), function(data){
        initializePages(data);
        $('#pagination').twbsPagination({
            totalPages: totalPages,
            visiblePages: visiblePages,
            onPageClick: function(event, page){
                mypage = page;
                tableLoader('8', 'contractTable');
                getAjax('/contract/pages/'+page+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/false/'+$('#searchContract').val(), 'contractTable');
            }
        });
      });
    }
  });
  $('#semesterFilter').on('change', function(){
    tableLoader('8', 'contractTable');
    $('#pagination').twbsPagination('destroy');
    $.get('/contract/pages/1/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/true/'+$('#searchContract').val(), function(data){
      initializePages(data);
      $('#pagination').twbsPagination({
          totalPages: totalPages,
          visiblePages: visiblePages,
          onPageClick: function(event, page){
              mypage = page;
              tableLoader('8', 'contractTable');
              getAjax('/contract/pages/'+page+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/false/'+$('#searchContract').val(), 'contractTable');
          }
      });
    });
  });
  $('#courseGradeLevelFilter').on('change', function(){
    tableLoader('8', 'contractTable');
    $('#pagination').twbsPagination('destroy');
    $.get('/contract/pages/1/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/true/'+$('#searchContract').val(), function(data){
      initializePages(data);
      $('#pagination').twbsPagination({
          totalPages: totalPages,
          visiblePages: visiblePages,
          onPageClick: function(event, page){
              mypage = page;
              tableLoader('8', 'contractTable');
              getAjax('/contract/pages/'+page+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/false/'+$('#searchContract').val(), 'contractTable');
          }
      });
    });
  });
  $('#studentsComboBox').on('change', function(){
    $.get('/contracts/collegestudent/'+$('#studentsComboBox').val(), function(data){
      $('#courseContract').val(data[0].courseName);
      $('#courseYearContract').val(data[0].yearLevel);
      //console.log(data);  
    });
  });
  $('#addGradeForm').on('submit', function(e){ 
    e.preventDefault();
    var temp = [];
    var y = 0;
    var z =0;
    var valid = true;
    $('#gradesTable').children('tr').each(function(){
      y = 0;
      $(this).children('td').each(function(){
        temp[y] = $(this).html(); 
        y++;
      });
      if(temp[0] == $('#subjectCodeText').val()){
       valid =false;
        return;
      }
    });
    if(valid==false){
      new PNotify({
        title: 'Error!',
        text: 'There should be no duplicate subject code!',
        type: 'error',
        styling: 'bootstrap3'
      });  
      return;
    }
    var html = "<tr>"+
                "<td>"+$('#subjectCodeText').val()+"</td>"+
                "<td>"+$('#gradeContract').val()+"</td>"+
                "<td>"+$('#unitContract').val()+"</td>"+
                "<td><button class='remove btn btn-dark'><i class='fa fa-eye'></i></button></td>"+
              "</tr>";
    $('#gradesTable').append(html);
    var totalgrades=0;
    $('#gradesTable').children('tr').each(function(){
      i=0;
      $(this).children('td').each(function(){
        if(i==1){
          totalgrades = parseFloat(totalgrades) + parseFloat($(this).html()); 
        }
        i++;
      });
    });

    $('#totalGrades').val(totalgrades);
  });
  $('#gradesTable').on('click', '.remove', function(){
    $(this).closest('tr').remove();
  });
  $('#uploadPhotosContract').on('change', function(){
    var imagefile;
    var url;
    for (var i = 0; i < $(this).get(0).files.length; ++i) {
      imageFile = this.files[i];
      url = window.URL.createObjectURL(imageFile);
      $('#displayImagesToBeUploaded').append("<img class='thisImage' src='"+url+"'>");
    }
  });
  $('#chooseStudent').on('click', function(){
    actionContract ='add';
    $('#browseStudentModal').modal('show');
    tableLoader('7','studentTableReport');
    $.get('/contract/studentsbrowse/college/all/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val()+'/'+$('#searchBrowseStudents').val(), function(data){
      $('#studentTableReport').html(data);
    });
    $.get('/report/college/course', function(data){
      $('#Course_GradeLevel').html("<option value='all'>all</option>");
      $('#Course_GradeLevel').append(data);
    });
  });
  $('#chooseStudentEdit').on('click', function(){
    actionContract ='edit';
    $('#browseStudentModal').modal('show');
    tableLoader('7','studentTableReport');
    $.get('/contract/studentsbrowse/college/all/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val()+'/'+$('#searchBrowseStudents').val(), function(data){
      $('#studentTableReport').html(data);
    });
    $.get('/report/college/course', function(data){
      $('#Course_GradeLevel').html("<option value='all'>all</option>");
      $('#Course_GradeLevel').append(data);
    });
  });
  $('#contractAddForm').on('submit', function(e){
    e.preventDefault();
    var subjectGrades =[];
    var i =0;
    var y =0;
    var grades;
    var units;
    var subjectcode;
    if($('#studentHiddenID').val()==''){
      new PNotify({
        title: 'Error!',
        text: 'Please select a student!',
        type: 'error',
        styling: 'bootstrap3'
      });  
      return;
    }
    $('#gradesTable').children('tr').each(function(){
      $(this).children('td').each(function(){
        switch(y){
          case 0:
            subjectcode =$(this).html();
          break;
          case 1:
            grades=$(this).html();
          break;
          case 2:
            units=$(this).html();
          break;
        }
        y++;
      });
      subjectGrades.push({
        subjectcode:subjectcode,
        grades:grades,
        units:units
      });
      y =0;
      i++;
    });
    if($('#typeOfContract').val()=='failing grades'){
      if(i==0){
        new PNotify({
          title: 'Error!',
          text: 'Please add a contract grade!',
          type: 'error',
          styling: 'bootstrap3'
        });  
        return;
      }
    }
    var totalRows=0;
    $('#contractTable').children('tr').each(function(){
      totalRows++;
    });
    var json_arr = JSON.stringify(subjectGrades);
    var form = document.getElementById('contractAddForm');
    var myform = new FormData(form);
    myform.append('subjectgrades', json_arr);
    myform.append('totalRows', totalRows);
    prependPostAjaxErrorCallback('/contract/insert', myform, function(data){
      if(data=="nostudent"){
        new PNotify({ 
          title: 'Error!',
          text: 'Something went wrong, please try again later!',
          type: 'error',
          styling: 'bootstrap3'
        });  
      } 
      new PNotify({
        title: 'Success!',
        text: 'Contract Successfully saved!',
        type: 'success',
        styling: 'bootstrap3'
      });
      $('#contractTable').children('h2').each(function(){
        $(this).remove();
      });
      $('#contractTable').append(data);
      $('#displayImagesToBeUploaded').html('');
      $('#uploadPhotosContract').val('');
      $('#addContractModal').modal('hide');
      var notedby = $("#notedByContract").val();
      $('#contractAddForm').find("input[type='text'], textarea").val("");
      $("#notedByContract").val(notedby);
      $('#gradesTable').html("");
      $('#addGradeForm').find("input").val("");
      
      $('#nameOfStudents').val("");
      $("#studentHiddenID").val("");
    });
  });
  $('#contractTable').on('click', '.viewContract', function(){
    contractid = $(this).prop('id').split('-')[1];
    $('#editContractModal').modal('show');
    $.get('/contract/data/'+contractid, function(data){
      //console.log(data);
      $('#gradesTableEdit').html(data.gradeHtml);
      var totalgrades=0;
      $('#gradesTableEdit').children('tr').each(function(){
        i=0;
        $(this).children('td').each(function(){
          if(i==1){
            totalgrades = parseFloat(totalgrades) + parseFloat($(this).html()); 
          }
          i++;
        });
      });
      $('#totalGradesEdit').val(totalgrades);
      $('#photosCurrentlyUploaded').html(data.proofHtml);
      $('#studentsComboBoxEdit').val(data.studentID);
      $('#courseContractEdit').val(data.courseName);
      $('#courseYearContractEdit').val(data.yearLevel);
      $('#contractDateEdit').val(data.contractDate);
      $('#schoolYearContractEdit').val(data.schoolYear);
      $('#semesterContractEdit').val(data.semesterID);
      // $('#typeOfContractEdit').val(data.type);
      
      $('#typeOfContractEdit').val(data.type);
      $("#totalGradesEdit").hide();
      $('#remarksContractEdit').val(data.remarks);
      $('#notedByContractEdit').val(data.notedBy);
      if(data.others === null){
        $("#othersTextEdit").attr("readonly", true);
        $("#othersTextEdit").attr("required", false);
      }else{
        $("#othersTextEdit").attr("readonly", false);
        $("#othersTextEdit").attr("required", true);
      }
      $("#othersTextEdit").val(data.others);
    });
  });
  $('#editGradeForm').on('submit', function(e){
    e.preventDefault();
    var temp = [];
    var y = 0;
    var z =0;
    var valid = true;
    $('#gradesTable').children('tr').each(function(){
      y = 0;
      $(this).children('td').each(function(){
        temp[y] = $(this).html(); 
        y++;
      });
      if(temp[0] == $('#subjectCodeText').val()){
       
        valid = false;
        return;
      }
    });
    if(valid==false){
      new PNotify({
        title: 'Error!',
        text: 'There should be no duplicate subject code!',
        type: 'error',
        styling: 'bootstrap3'
      });  
      return;
    }
    var html = "<tr>"+
                "<td>"+$('#subjectCodeTextEdit').val()+"</td>"+
                "<td>"+$('#gradeContractEdit').val()+"</td>"+
                "<td>"+$('#unitContractEdit').val()+"</td>"+
                "<td><button class='remove btn btn-dark'><i class='fa fa-eye'></i></button></td>"+
              "</tr>";
    $('#gradesTableEdit').append(html);
    
  });
  $('#uploadPhotosContractEdit').on('change', function(){
    var imagefile;
    var url;
    for (var i = 0; i < $(this).get(0).files.length; ++i) {
      imageFile = this.files[i];
      url = window.URL.createObjectURL(imageFile);
      $('#displayImagesToBeUploadedEdit').append("<img class='thisImage' src='"+url+"'>");
    }
  });
  $('#photosCurrentlyUploaded').on('click', '.close', function(){
    globalcontractproofid = $(this).prop('id').split('-')[1];
    $("#deleteModal").modal('show');
    $("#deleteMessage").html("<h3>Are you sure you want to delete this proof?</h3>");
    mydelete ="deleteContractProof";
  });
  $('#contractEditForm').on('submit', function(e){
    e.preventDefault();
    var subjectGrades =[];
    var i =0;
    var y =0;
    var grades;
    var units;
    var subjectcode;
    $('#gradesTableEdit').children('tr').each(function(){
      $(this).children('td').each(function(){
        switch(y){
          case 0:
            subjectcode =$(this).html();
          break;
          case 1:
            grades=$(this).html();
          break;
          case 2:
            units=$(this).html();
          break;
        }
        y++;
      });
      subjectGrades.push({
        subjectcode:subjectcode,
        grades:grades,
        units:units
      });
      y =0;
      i++;
    });
    if($('#typeOfContractEdit').val()=='failing grades'){
      if(i==0){
        new PNotify({
          title: 'Error!',
          text: 'Please add a contract grade!',
          type: 'error',
          styling: 'bootstrap3'
        });  
        return;
      }
    }
    var tempContract = [];
    var counter = 0;
    $('#contractRow-'+contractid).children('td').each(function(){
      tempContract[counter] = $(this).html();
      counter++; 
    });
    var json_arr = JSON.stringify(subjectGrades);
    var form = document.getElementById('contractEditForm');
    var myform = new FormData(form);
    myform.append('subjectgrades', json_arr);
    myform.append('id', contractid);
    myform.append('rowCounter', tempContract[0]);
    
    
    prependPostAjaxErrorCallback('/contract/update', myform, function(data){
      if(data=="nostudent"){
        new PNotify({
          title: 'Error!',
          text: 'Something went wrong, please try again later!',
          type: 'error',
          styling: 'bootstrap3'
        });  
      } 
      new PNotify({
        title: 'Success!',
        text: 'Contract Successfully saved!',
        type: 'success',
        styling: 'bootstrap3'
      });
      data = data.replace("<tr id ='contractRow-"+contractid+"'>", "");
      data = data.replace("</tr>", "");
      $('#contractRow-'+contractid).html(data);
      $('#displayImagesToBeUploadedEdit').html('');
      $('#uploadPhotosContractEdit').val('');
      $('#editContractModal').modal('hide');
    });
  });
  $('#viewByStudentType').on('change', function(){
    $.get('/report/college/course', function(data){
      $('#Course_GradeLevel').html("<option value='all'>all</option>");
      $('#Course_GradeLevel').append(data);
      $.get('/contract/studentsbrowse/college/'+$('#Course_GradeLevel').val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val()+'/'+$('#searchBrowseStudents').val(), function(data){
        $('#studentTableReport').html(data);
      });
    });
  });
  $('#Course_GradeLevel').on('change', function(){
    tableLoader('7','studentTableReport');
    $.get('/contract/studentsbrowse/'+$('#viewByStudentType').val()+'/'+$(this).val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val()+'/'+$('#searchBrowseStudents').val(), function(data){
      $('#studentTableReport').html(data);
    });
  });
  $('#schoolYearStudent').on('change', function(){
    tableLoader('7','studentTableReport');
    if($('#schoolYearStudent').val() != "all"){
      $.get('/report/schoolyear/'+$('#schoolYearStudent').val(), function(data){
        $('#semesterStudent').html("<option value='all'>all</option>");
        $('#semesterStudent').append(data);
        $.get('/contract/studentsbrowse/'+$('#viewByStudentType').val()+'/'+$('#Course_GradeLevel').val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val()+'/'+$('#searchBrowseStudents').val(), function(data){
          $('#studentTableReport').html(data);
        });
      });
    }else{
      $.get('/contract/studentsbrowse/'+$('#viewByStudentType').val()+'/'+$('#Course_GradeLevel').val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val()+'/'+$('#searchBrowseStudents').val(), function(data){
        $('#studentTableReport').html(data);
      });
    }
  }); 
  $('#semesterStudent').on('change', function(){
    tableLoader('5','studentTableReport');
    $.get('/contract/studentsbrowse/'+$('#viewByStudentType').val()+'/'+$('#Course_GradeLevel').val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val()+'/'+$('#searchBrowseStudents').val(), function(data){
      $('#studentTableReport').html(data);
    });
  });
  $('#searchBrowseStudents').on('keydown', function(){
    tableLoader('7','studentTableReport');
    $.get('/contract/studentsbrowse/college/'+$('#Course_GradeLevel').val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val()+'/'+$('#searchBrowseStudents').val(), function(data){
      $('#studentTableReport').html(data);
    });
  });
  $('#studentTableReport').on('click', '.selectStudent', function(){
    $id = $(this).prop("id").split('-')[1];
    $('#studentHiddenID').val($id);
    var temp =[];
    var i =0;
    $(this).closest('tr').children("td").each(function(){
      temp[i] = $(this).html();
      i++;
    });
    if(actionContract == "add"){
      $('#courseContract').val(temp[4]);
      $('#courseYearContract').val(temp[5]);
      $('#nameOfStudents').val(temp[1]);
    }else{
      $('#courseContractEdit').val(temp[4]);
      $('#courseYearContractEdit').val(temp[5]);
      $('#nameOfStudentsEdit').val(temp[1]);
    }
    $('#browseStudentModal').modal('hide');
  });
  $('#typeOfContract').on('change', function(){
    if($('#typeOfContract').val()!= "failing grades"){
      $('#gradesTable').html('');
      $('#gradeContract').attr('disabled', true);
      $('#unitContract').attr('disabled', true);
      $('#subjectCodeText').attr('disabled', true);
    }else{
      $('#gradeContract').attr('disabled', false);
      $('#unitContract').attr('disabled', false);
      $('#subjectCodeText').attr('disabled', false);
    }
    if($(this).val()=="others"){
      $("#othersTextAdd").val("");
      $("#othersTextAdd").attr("readonly", false);
      $("#othersTextAdd").attr("required", true);
    }else{
      $("#othersTextAdd").attr("readonly", true);
      $("#othersTextAdd").attr("required", false);
    }
  }); 
  $('#typeOfContractEdit').on('change', function(){
    if($('#typeOfContractEdit').val()!= "failing grades"){
      $('#gradesTableEdit').html('');
      $('#gradeContractEdit').attr('disabled', true);
      $('#unitContractEdit').attr('disabled', true);
      $('#subjectCodeTextEdit').attr('disabled', true);
    }else{
      $('#gradeContractEdit').attr('disabled', false);
      $('#unitContractEdit').attr('disabled', false);
      $('#subjectCodeTextEdit').attr('disabled', false);
    }
    if($(this).val()=="others"){
      $("#othersTextEdit").val("");
      $("#othersTextEdit").attr("readonly", false);
      $("#othersTextEdit").attr("required", true);
    }else{
      $("#othersTextEdit").attr("readonly", true);
      $("#othersTextEdit").attr("required", false);
    }
    
  }); 
  $("#contractRemarksForm").on('submit', function(e){
    e.preventDefault();
    var mydata  = document.getElementById("contractRemarksForm");
    var formdata = new FormData(mydata);
    formdata.append("contractID", contractid);
    prependPostAjaxErrorCallback('/contract/remarks/insert', formdata, function(data){
      new PNotify({
        title: 'Success!',
        text: 'Remarks saved!',
        type: 'success',
        styling: 'bootstrap3'
      });
      $("#remarksContractModal").modal("hide");
      $("#remarksContractModal textarea").val("");
      window.location.href="/admin/contract";
    });
  });
  $("#printContractStudents").on('click', function(){
     //alert("Hello");
    $.get('/contractreport/print/'+$('#studentTypeFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#searchContract').val(), function(data){
      if(data=="true"){
        window.location.replace("/temp/report.pdf");
      }
    });
  });
});
tableLoader('8', 'contractTable');
$.get('/contract/pages/1/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/true/'+$('#searchContract').val(), function(data){
  
  initializePages(data);
  $('#pagination').twbsPagination({
      totalPages: totalPages,
      visiblePages: visiblePages,
      onPageClick: function(event, page){
          mypage = page;
          tableLoader('8', 'contractTable');
          getAjax('/contract/pages/'+page+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/false/'+$('#searchContract').val(), 'contractTable');
      }
  });
});
function initializePages(data){
  totalRow = data;
  if(totalRow<=5){
      totalPages = 1;
  }else{
      var remainder = totalRow%10;
      if(remainder >0){
          totalPages = ((totalRow-remainder)/10)+1;
      }else{
          totalPages = totalRow/10;
      }
      if(totalPages<=5){
          visiblePages =totalPages;
      }else{
          visiblePages = 5;
      }    
  }
}
