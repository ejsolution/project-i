@component('components.modal')
    @slot('modalid')
    personalitySettingsModal
    @endslot
    @slot('modalsize')
    modal-md
    @endslot
    @slot('modaltitle')
    Personalities
    @endslot
    @slot('modalcontent')
    {!! Form::open([
    'id'=>'personalityAddForm']) !!}
    @component('components.x_panel')
        @slot('title')
        Add A Personality
        @endslot
        @slot('content')
            <div class='row'>
                <div class='col-md-12'>
                    <label for ='personalityName'>Personality Name</label>
                    <input type ='text' id ='personalityName' required maxlength ='100' class='form-control' name ='name'>
                </div>
            </div>
            <div class='row'>
                <div class='col-md-12'>
                    <label for ='personalityDescription'>Descrition</label>
                    <textarea id ='personalityDescription' required maxlength ='100' class='form-control' name ='description'></textarea>
                </div>
            </div>
            <div class='row'>
                <div class='col-md-12 genericDivTop alignRight'>
                    <button class='btn btn-dark'>Save</button>
                </div>
            </div>
        @endslot
    @endcomponent
    {!! Form::close() !!}
    {!! Form::open([
        'id'=>'personalityEditForm']) !!}
    {!! Form::close() !!}
    <table class='table table-striped'>
        <thead>
            <tr>
                <th>Name</th>
                <th>Description</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </thead>
        <tbody id ='personalityTable'>
        </tbody>
    </table>
    @endslot
    @slot('modalbutton')
   
    @endslot
@endcomponent