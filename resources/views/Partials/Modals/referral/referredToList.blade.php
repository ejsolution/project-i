@component('components.modal')
  @slot('modalid')
    selectCoordinatorModal
  @endslot
  @slot('modalsize')
    modal-md
  @endslot
  @slot('modaltitle') 
    Select A Coordinator
  @endslot
  @slot('modalcontent')
    <div class='row'>
        <div class='col-md-12 well tableModalBigContainer'>
            <table class='table'>
                <thead>
                    <tr>
                        <th>Name</th>
                        
                        <th>Designation</th>
                        <th>Select</th>
                    </tr>
                </thead>
                <tbody id ='selectCoordinatorsTable'>
                </tbody>
            </table>
        </div>
    </div>
  @endslot
  @slot('modalbutton')
     
  @endslot
@endcomponent
