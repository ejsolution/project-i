<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepartmentCollegeHistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('department_college_hists', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('departmentID')->unsigned();
            $table->foreign('departmentID')->references('id')->on('departments');
            $table->integer('collegeID')->unsigned();
            $table->foreign('collegeID')->references('id')->on('colleges');
            $table->date('dateAssigned');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('department_college_hists');
    }
}
