@if(Session::has('success'))
	<div class='alert alert-success' role='alert'>
		<strong>Success:</strong>{{Session::get('success')}}
	</div>
@endif
@if(Session::has('error'))
	<div class='alert alert-danger' role='alert'>
		<strong>Error:</strong>{{Session::get('error')}}
	</div>
@endif
@if(Session::has('warning'))
	<div class='alert alert-danger' role='alert'>
		<strong>Warning: </strong>{{Session::get('warning')}}
	</div>
@endif
@if(Session::has('warnings'))
	<div class='alert alert-danger' role='alert'>
		<strong>Warnings:</strong>
		<ul>
			@foreach(Session::get('warnings') as $warning)
				<li>{{ $warning }}</li>
			@endforeach
		</ul>
	</div>
@endif

@if(count($errors)>0)
	<div class='alert alert-danger' role='alert'>
		<strong>Errors:</strong>
		<ul>
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
@endif
