<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeNullStudentLeisure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('student_leisure_colleges', function (Blueprint $table) {
            
            /*$table->integer('collegStudentID')->unsigned();
            $table->foreign('collegStudentID')->references('id')->on('college_students');
            $table->integer('average');
            $table->integer('rankClass');
            $table->string('major');*/
            $table->string('presentEducation')->nullable()->change();
            $table->string('courseChoice')->nullable()->change();
            $table->string('otherCourse')->nullable()->change();
            $table->string('schoolChoice')->nullable()->change();
            $table->string('courseInfo')->nullable()->change();
            $table->mediumText('howDidYouMakeThisChoice')->nullable();
            $table->mediumText('howDidYouComeToThisSchool')->nullable();
            $table->mediumText('whereDidYouGetThisInfo')->nullable();
            $table->mediumText('scholarship')->nullable();
            /*$table->string('getInfo')->nullable();
            $table->string('financialSupport');
            $table->mediumText('selfEval1');
            $table->mediumText('selfEval2');
            $table->mediumText('selfEval3');
            $table->mediumText('selfEval4');
            $table->mediumText('selfEval5');
            $table->mediumText('selfEval6');
            $table->mediumText('otherRemarks');*/
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
