<?php

namespace App\Http\Controllers\settings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\adminsetting;
use App\model\contractSetting;
use Image;
class settingsController extends Controller
{
    public function insertSettings(Request $request){

        $count = adminsetting::all();
        if(count($count)>0){
            $this->validate($request, array(
                'guidanceNameTextManageLayout'=>'required|max:100|unique:useraccounts,userName',
                'streetTextManageLayout'=>'required|max:100',
                'barangayTextManageLayout'=>'required|max:100',
                'cityTextManageLayout'=>'required|max:100',
                'schoolSeal' => 'image|mimes:jpeg,png,jpg|max:2048',
                'guidanceSeal'=> 'image|mimes:jpeg,png,jpg|max:2048',
                'schoolNameTextManageLayout'=> 'required|max:100',
                'consecutiveContract' =>'required'
            ));
            $adminsetting = adminsetting::find($count[0]->id);
            $adminsetting->schoolName = $request->schoolNameTextManageLayout;
            $adminsetting->guidanceName = $request->guidanceNameTextManageLayout;
            $adminsetting->streetAddress = $request->streetTextManageLayout;
            $adminsetting->barangay = $request->barangayTextManageLayout;
            $adminsetting->city = $request->cityTextManageLayout;
            $adminsetting->status = "1";
            $adminsetting->description = "no description";
            $contractSetting = contractSetting::all();
            if(count($contractSetting)>0){
                $contractSetting[0]->consecutive = $request->consecutiveContract;
                $contractSetting[0]->save();
            }else{
                $contractSetting = new contractSetting;
                $contractSetting->consecutive = $request->consecutiveContract;
                $contractSetting->save();
            }
            if($request->hasFile('guidanceSeal') || $request->hasFile('schoolSeal')){
                if($request->hasFile('guidanceSeal')){
                    $imageGuidance = $request->file('guidanceSeal');
                    $imageGuidanceType = $imageGuidance->getClientOriginalExtension();
                    $adminsetting->guidanceSealImageType = $imageGuidanceType;
                    $filenameGuidance = $adminsetting->id.'.'.$imageGuidance->getClientOriginalExtension();
                    $locationGuidance = public_path("images-database/adminsetting/guidanceimage/". $filenameGuidance);
                    Image::make($imageGuidance)->resize(200,200)->save($locationGuidance);
                }
                if($request->hasFile('schoolSeal')){
                    $imageSchool =  $request->file('schoolSeal');
                    $imageSchoolType = $imageSchool->getClientOriginalExtension();
                    $adminsetting->schoolSealImageType = $imageSchoolType;
                    $filenameSchool = $adminsetting->id.'.'.$imageSchool->getClientOriginalExtension();
                    $locationSchool = public_path("images-database/adminsetting/schoolimage/". $filenameSchool);
                    Image::make($imageSchool)->resize(200,200)->save($locationSchool);
                }
                $adminsetting->save();
            }else{
                $adminsetting->save();
            }
        }else{
            $this->validate($request, array(
                'guidanceNameTextManageLayout'=>'required|max:100|unique:useraccounts,userName',
                'streetTextManageLayout'=>'required|max:100',
                'barangayTextManageLayout'=>'required|max:100',
                'cityTextManageLayout'=>'required|max:100',
                'schoolSeal' => 'required|image|mimes:jpeg,png,jpg|max:2048',
                'guidanceSeal'=> 'required|image|mimes:jpeg,png,jpg|max:2048',
                'schoolNameTextManageLayout'=> 'required|max:100',
                'consecutiveContract' =>'required'
            ));
            $adminsetting = new adminsetting;
            $adminsetting->schoolName = $request->schoolNameTextManageLayout;
            $adminsetting->guidanceName = $request->guidanceNameTextManageLayout;
            $adminsetting->streetAddress = $request->streetTextManageLayout;
            $adminsetting->barangay = $request->barangayTextManageLayout;
            $adminsetting->city = $request->cityTextManageLayout;
            $adminsetting->status = "1";
            $adminsetting->description = "no description";
            $imageGuidance = $request->file('guidanceSeal');
            $imageGuidanceType = $imageGuidance->getClientOriginalExtension();
            $adminsetting->guidanceSealImageType = $imageGuidanceType;
            $imageSchool =  $request->file('schoolSeal');
            $imageSchoolType = $imageSchool->getClientOriginalExtension();
            $adminsetting->schoolSealImageType = $imageSchoolType;
            $adminsetting->save();
            $contractSetting = new contractSetting;
            $contractSetting->consecutive = $request->consecutiveContract;
            $contractSetting->save();
            $filenameGuidance = $adminsetting->id.'.'.$imageGuidance->getClientOriginalExtension();
            $locationGuidance = public_path("images-database/adminsetting/guidanceimage/". $filenameGuidance);
            Image::make($imageGuidance)->resize(200,200)->save($locationGuidance);
            $filenameSchool = $adminsetting->id.'.'.$imageSchool->getClientOriginalExtension();
            $locationSchool = public_path("images-database/adminsetting/schoolimage/". $filenameSchool);
            Image::make($imageSchool)->resize(200,200)->save($locationSchool);
        }
        return "saved!";
    }
    public function getSetting(){
        $temp = new \stdClass();
        $count = adminsetting::all();
        $contract = contractSetting::all();
        
        if(count($count)>0){
            $adminsetting = adminsetting::find($count[0]->id);
            $adminsetting->check = "";
            $adminsetting->contract = 0;
            if(count($contract)>0){
                $adminsetting->contract = $contract[0]->consecutive;
            }
            return $adminsetting;
        }else{
            $temp->check="null";
            //$json_data = json_encode((array) $temp);
            return  response()->json($temp);
        } 
    }
}
