<div id="newMessageModal" class="compose col-md-6" style = 'display:none; z-index:100;' >
  <div class='compose-header'>
    New Message
    <button class='close' id = 'closeNewMessage'>
      <span>x</span>
    </button>

  </div>
  <div class='compose-body'>
    <div class='container-fluid'>
      <div class='row'>
        <div class='col-md-12'>
          <label for='receiverNewMessageModalText'>To:</label>
          <input type = 'text' id = 'receiverNewMessageModalText' class='form-control' disabled>
        </div>
        <div class='col-md-12 alignRight genericDivTop'>
          <button class='btn btn-default' id = 'addRecipientNewMessageModalButton'>Add Recipient</button>
        </div>
        {!! Form::open([
          'id'=>'sendMessageForm']) !!}
          <div class='col-md-12'>
            <label for = 'messageNewMessageModalText'>Message:</label>
            <textarea class='form-control' id = 'messageNewMessageModalText' name ='messageNewMessageModalText' rows = '8' required></textarea>
          </div>
        {!! Form::close() !!}
      </div>
      
      <div class='row'>
        <div class='col-md-1'>
          From:
        </div>
        <div class='col-md-11'>
          <div>
            <strong> {{session()->get('user')['userName']}}</strong>
          </div>
          <!--div>
            <strong>Position</strong>
          </div>
          <div>
            <strong>College</strong>
          </div-->
        </div>
      </div>
    </div>
  </div>
  <div class='compose-footer'>
    <button class="btn btn-warning" form ='sendMessageForm'><i class='fa fa-reply'></i>&nbsp;Send</button>
    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
  </div>
</div>

