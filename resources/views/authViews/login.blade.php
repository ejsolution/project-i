@extends('authViews.authViewTemplate')
@section('css')
    <link href="{{asset('css/authStyles/login.css')}}" rel="stylesheet">
@stop
@section('content')
  <div class = 'flexContainerMiddle' >

  <div class ='regAdminLoginForm'>
    @include('Partials._message')
    <div><center><strong>Login</strong></center></div>


    {!! Form::open(['route' => 'login.check',
            'data-parsley-validate'=>'']) !!}
      {{Form:: label('email', 'Email:')}}
      {{Form:: text('email', null, array(
                        'placeholder'=>'Email',
                        'class'=>'form-control',
                        'required'=>'',
                        'maxlength'=>'100'))}}
      {{Form:: label('password', 'Password:')}}
      {{Form:: password('password', array(
                        'placeholder'=>'Password',
                        'class'=>'form-control',
                        'required'=>'',
                        'maxlength'=>'100'))}}
      <div class='genericDivTop'>

      {{--  {{Form::checkbox('remember_me')}}
      {{Form:: label('remember_me', 'Remember me')}}  --}}
      </div>
      {{Form::submit('Login', array('class'=>'btn btn-success btn-block'))}}
    {!! Form::close() !!}
    <div>
      <div class='row'>
        <div class='col-6'>
          {{Html::link('#', 'Forgot Password', array('class'=>'btn btn-block btn-info genericDivTop'
                                                    ))}}
        </div>
        <div class='col-6'>
          <a href ='{{route('register')}}' class='btn btn-block btn-primary genericDivTop'>Register</a>
        </div>
      </div>
    </div>
  </div>
</div>
<div class='loginFooter' style = 'color:white;'>
  <center>
    <img src= '{{asset('images/companylogo.png')}}' id = 'myimage'><strong>Powered By:</strong> EchoWeave Tech Solutions
  </center>
</div>
@stop
