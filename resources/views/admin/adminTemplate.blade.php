<!DOCTYPE html>
<html lang="en">
  <head>
    @include('Partials._metaHead')
    <title>@yield('title')</title>
    @include('Partials.AdminPartials._adminStylesheet')
    @yield('stylesheet')
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
            <a href="index.html" class="site_title"><i class="fa fa-user"></i> <span>Hello {{ucfirst(session()->get('user')['userType'])}}!</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <!-- profile image here-->
                <img src="{{asset('images-database/admin/'.Auth::guard('myuser')->id().'.'.session()->get('user')['imageType'])}}" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2>{{session()->get('user')['userName']}}</h2>
              </div>

            </div>
            <div class='semInfo'>
              <div>
                <center><strong>Semester:
                  @if(session()->get('schoolyear')['sem'] == '1')
                    {{session()->get('schoolyear')['sem']}}st
                  @elseif(session()->get('schoolyear')['sem'] == '2')
                    {{session()->get('schoolyear')['sem']}}nd
                  @else
                    {{session()->get('schoolyear')['sem']}}rd
                  @endif
                </strong></center>
              </div>
              <div>
                <center>
                  <h5>{{session()->get('schoolyear')['year']}}</h5>
                </center>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            @include('Partials.AdminPartials._sideBarAdmin')
           
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>
             
              <ul class="nav navbar-nav navbar-right"> 
                {!! Form::open(['id'=>'systemDateForm',
                'class'=>'nav navbar-form navbar-left']) !!}
                  <div class="form-group">
                    <label>System Date:</label>
                  </div>
                  <div class="form-group">
                    <input type="date" class="form-control" id ='systemDate' name ='systemDate' value ="{{session()->get('systemDate')}}" required>
                  </div>
                  
                    <button type="submit" class="btn btn-default" id ='changeSystemDateButton' style ='margin-top:3px;' disabled>Change</button>
                  
                {!! Form::close() !!}
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="{{asset('images-database/admin/'.Auth::guard('myuser')->id().'.'.session()->get('user')['imageType'])}}" alt="">{{session()->get('user')['userName']}}
                    <!-- admin image here -->
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li id = 'adminProfile'><a href="{{route('userprofile')}}">Profile</a></li>
                    {{--  <li><a href="#">Help</a></li>  --}}
                    <li><a href="{{route('logout')}}"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                    <!--Logout link here-->
                  </ul>
                </li>

                @include('Partials.AdminPartials._notifs')

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main" style = 'overflow-x:scroll;'>
          @yield('content')
          @include('Partials.Modals.managelayouts.managelayouts')
          @include('Partials.Modals.universityLevel.collegeModal')
          @include('Partials.Modals.universityLevel.seniorHighModal')
          @include('Partials.Modals.universityLevel.juniorHighModal')
          @include('Partials.Modals.universityLevel.elementarySettingModal')
          @include('Partials.Modals.universityLevel.addCourseModal')
          @include('Partials.Modals.universityLevel.selectCollegeModal')
          @include('Partials.Modals.universityLevel.selectDepartmentModal')
          @include('Partials.Modals.universityLevel.subjectsModal')
          @include('Partials.Modals.personality.personalityModal')
          @include('Partials.Modals.behaviour.behaviourModal')
          @include('Partials.Modals.counselor.counselorModal')
          @include('Partials.Modals.deleteModal')
        </div>
        @include('Partials._footer')
        </div>
    </div>
      <!-- jQuery -->
    @include('Partials.AdminPartials._adminScripts')
    @yield('scripts')
  </body>
</html>
