<?php

namespace App\Http\Controllers\universityschoolyear;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\schoolYear;

use App\model\referral;
use App\model\collegeStudent;
use App\model\elemStudent;
use App\model\contract;
use App\model\counseling;
use App\model\favSubjectList;
use App\model\makeUpAssistance;
use App\model\JHStudent;
use App\model\SHSStudent;
use App\Http\Controllers\activityController;
use App\Http\Controllers\student\studentController;
use App\Http\Controllers\notification\notificationController;
use Session;
class schoolyearController extends Controller
{
    private $request;
    public function __construct(){
        $this->middleware('auth:myuser');
    }
    public function getUniversitySchoolYear(){
        $notif = (new notificationController)->getNotifs();
        return view('admin/universityschoolyear/universityschoolyear', ['notif'=>$notif]);
    }
    public function schoolYearLoad(){
        $schoolyear  =  schoolYear::all();
        if(count($schoolyear)==0){
            return "<h2>There are no available schoolyear yet.</h2>";
        }
        $html = '';
        foreach($schoolyear as $key){
            $html .= $this->schoolYearRow($key);
        }
        return $html;
    }
    public function schoolYearRow($schoolyear){
        $status = 'inactive';
        if($schoolyear->status==1){
            $status ='active';
        }
        $schoolyearstart = explode('-', $schoolyear->schoolYearStart);
        $schoolyearend = explode('-', $schoolyear->schoolYearEnd);
        $html="<tr id ='schoolYear-$schoolyear->id'>
                    <td>$schoolyearstart[0] - $schoolyearend[0]</td>
                    <td>$schoolyear->firstSemesterStart to $schoolyear->firstSemesterEnd</td>
                    <td>$schoolyear->secondSemesterStart to $schoolyear->secondSemesterEnd</td>
                    <td>$schoolyear->thirdSemesterStart to $schoolyear->thirdSemesterEnd</td>
                    <td>$status</td>";
                    
        if($schoolyear->status ==0){
            $html .="<td><button class='btn btn-dark setActive' id = 'setActive-$schoolyear->id'>
                    <i class='fa fa-edit'></i></button></td>
                    <td><button class='btn btn-info editSchoolYear' id = 'editSchoolYear-$schoolyear->id'>
                    <i class='fa fa-edit'></i></button></td>
                </tr>";
        }else{
            $html .="<td><button class='btn btn-dark setActive' id = 'setActive-$schoolyear->id' disabled>
                        <i class='fa fa-edit'></i></button></td>
                        <td><button class='btn btn-info editSchoolYear' id = 'editSchoolYear-$schoolyear->id'>
                        <i class='fa fa-edit'></i></button></td>
                    </tr>";
        }
         return $html;
    }
    
    public function schoolYearInsert(Request $request){
        $this->validate($request, array(
            'fromDateSchoolYear'=>'required',
            'toDateSchoolYear'=>'required',
            'fromFirstSemester'=>'required',
            'toFirstSemester'=>'required',
            'fromSecondSemester'=>'required',
            'toSecondSemester'=>'required'
        ));
        $semestertype = '3';
        if( $request->toThirdSemester == ''){
            $semestertype = '2';
        }
        $this->request = $request;
        $conflict = schoolYear::where(function($query){
            $query->where('schoolYearStart', '<=', $this->request->fromDateSchoolYear)
            ->where('schoolYearEnd', '>=', $this->request->fromDateSchoolYear);
        })->orWhere(function($query){
            $query->where('schoolYearStart', '<=', $this->request->toDateSchoolYear)
            ->where('schoolYearEnd', '>=', $this->request->toDateSchoolYear);
        })->get();
        if(count($conflict)>0){
            return "conflict";
        }
        $schoolyear =  new schoolYear;
        $schoolyear->schoolYearStart = $request->fromDateSchoolYear;
        $schoolyear->schoolYearEnd = $request->toDateSchoolYear;
        $schoolyear->firstSemesterStart = $request->fromFirstSemester;
        $schoolyear->firstSemesterEnd = $request->toFirstSemester;
        $schoolyear->secondSemesterStart = $request->fromSecondSemester;
        $schoolyear->secondSemesterEnd = $request->toSecondSemester;
        $schoolyear->thirdSemesterStart = $request->fromThirdSemester;
        $schoolyear->thirdSemesterEnd = $request->toThirdSemester;
        $schoolyear->status = 0;
        $schoolyear->semesterType = $semestertype;
        $schoolyear->save();
        activityController::insertActivity('Inserted a schoolyear :'.$schoolyear->schoolYearStart.' to '.$schoolyear->schoolYearEnd);
        return $this->schoolYearRow($schoolyear);
    }
    public function getYears($schoolyearid){
        $schoolyear = schoolYear::find($schoolyearid);
        return ['schoolYearStart'=>$schoolyear->schoolYearStart,
                'schoolYearEnd'=>$schoolyear->schoolYearEnd];
    }
    public function schoolYearUpdate(Request $request){
        $this->validate($request, array(
            'fromDateSchoolYearEdit'=>'required',
            'toDateSchoolYearEdit'=>'required',
            'fromFirstSemesterEdit'=>'required',
            'toFirstSemesterEdit'=>'required',
            'fromSecondSemesterEdit'=>'required',
            'toSecondSemesterEdit'=>'required'
        ));
        $semestertype = '3';
        if( $request->toThirdSemesterEdit == ''){
            $semestertype = '2';
        }
        $this->request = $request;
        $conflict = schoolYear::where(function($query){
            $query->where(function($query){
                $query->where('schoolYearStart', '<=', $this->request->fromDateSchoolYearEdit)
                ->where('schoolYearEnd', '>=', $this->request->fromDateSchoolYearEdit);
            })->orWhere(function($query){
                $query->where('schoolYearStart', '<=', $this->request->toDateSchoolYearEdit)
                ->where('schoolYearEnd', '>=', $this->request->toDateSchoolYearEdit);
            });
        })->where('id', '<>', $request->id)->get();
        
        if(count($conflict)>0){
            return "conflict";
        }
        $count = [];

        $count = referral::where('schoolYearID', '=', $request->id)->get();
        if(count($count)>0){
            return "cannotupdate";
        }
        $count = collegeStudent::where('schoolYearID', '=', $request->id)->get();
        if(count($count)>0){
            return "cannotupdate";
        }
        $count = elemStudent::where('schoolYearID', '=', $request->id)->get();
        if(count($count)>0){
            return "cannotupdate";
        }
        $count = counseling::where('schoolYearID', '=', $request->id)->get();
        if(count($count)>0){
            return "cannotupdate";
        }
       
        $count = makeUpAssistance::where('schoolYearID', '=', $request->id)->get();
        if(count($count)>0){
            return "cannotupdate";
        }
        $count = JHStudent::where('schoolYearID', '=', $request->id)->get();
        if(count($count)>0){
            return "cannotupdate";
        }
        $count = SHSStudent::where('schoolYearID', '=', $request->id)->get();
        if(count($count)>0){
            return "cannotupdate";
        }
        
        $schoolyear =  schoolYear::find($request->id);
        $schoolyear->schoolYearStart = $request->fromDateSchoolYearEdit;
        $schoolyear->schoolYearEnd = $request->toDateSchoolYearEdit;
        $schoolyear->firstSemesterStart = $request->fromFirstSemesterEdit;
        $schoolyear->firstSemesterEnd = $request->toFirstSemesterEdit;
        $schoolyear->secondSemesterStart = $request->fromSecondSemesterEdit;
        $schoolyear->secondSemesterEnd = $request->toSecondSemesterEdit;
        $schoolyear->thirdSemesterStart = $request->fromThirdSemesterEdit;
        $schoolyear->thirdSemesterEnd = $request->toThirdSemesterEdit;
        //$schoolyear->status = 0;
        $schoolyear->semesterType = $semestertype;
        $schoolyear->save();
        activityController::insertActivity('Updated a schoolyear :'.$schoolyear->schoolYearStart.' to '.$schoolyear->schoolYearEnd);
        return "success";
        //return $this->schoolYearRow($schoolyear);
    } 
    public function setActive($schoolyearid){
        $prevschoolyear = schoolYear::where('status', '=', 1)->get();
        if(count($prevschoolyear)>0){
            $prevschoolyear[0]->status = '0';
            $prevschoolyear[0]->save();
        }
        $schoolyear = schoolYear::find($schoolyearid);
        $schoolyear->status = 1;
        $schoolyear->save();

        
        session(['schoolyear'=> array('year'=>explode('-', $schoolyear->schoolYearStart)[0]." - ".explode('-', $schoolyear->schoolYearEnd)[0],
                                    'sem'=>(new studentController)->getCurrentSem())]);
        
        activityController::insertActivity('Set a schoolyear to active :'.$schoolyear->schoolYearStart.' to '.$schoolyear->schoolYearEnd);
        return "success";
    }
}
