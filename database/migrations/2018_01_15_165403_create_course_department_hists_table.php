<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseDepartmentHistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_department_hists', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('courseID')->unsigned();
            $table->foreign('courseID')->references('id')->on('courses');
            $table->integer('departmentID')->unsigned();
            $table->foreign('departmentID')->references('id')->on('departments');
            $table->date('dateAssigned');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_department_hists');
    }
}
