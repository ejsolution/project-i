<div class='row'>
    <div class='col-md-3'>
        <label for ='studentTypeFilter'>Student Type</label>
        
         
            @if(session()->get('user')['userType']=='admin')
            <select id ='studentTypeFilter' class='form-control'>
                <option value ='all'>All</option>
                <option value ='college'>College</option>
                <option value ='seniorhigh'>Senior High</option>
                <option value ='juniorhigh'>Junior High</option>
                <option value ='elementary'>Elementary</option>
            @else
                @switch($user[0])
                    @case('college')
                        <select id ='studentTypeFilter' class='form-control' readonly>
                            <option value ='college'>College</option>
                    @break
                    @case('seniorhigh')
                        <select id ='studentTypeFilter' class='form-control' readonly>
                            <option value ='seniorhigh'>Senior High</option>
                    @break
                    @case('juniorhigh')
                        <select id ='studentTypeFilter' class='form-control' readonly>
                            <option value ='juniorhigh'>Junior High</option>
                    @break
                    @case('elementary')
                        <select id ='studentTypeFilter' class='form-control' readonly>
                            <option value ='elementary'>Elementary</option>
                    @break
                    @default
                        Default case...
                @endswitch
            @endif
        </select>
        
    </div>
    <div class='col-md-2'>
        <label for ='schoolYearFilter'>School Year</label>
        <select id ='schoolYearFilter' class='form-control'>
            <option value ='all'>all</option>
            {{$schoolyears}}
            
        </select>
    </div>
    <div class='col-md-2'>
        <label for ='semesterFilter'>Semester</label>
        <select id ='semesterFilter' class='form-control'>
            <option value ='all'>all</option>
        </select>
    </div> 
    <div class='col-md-3'>
        <label for ='courseGradeLevelFilter'>Course/Strand</label>
        @if(session()->get('user')['userType']=='admin')
                    <select id ='courseGradeLevelFilter' class='form-control'>
                            <option value ='all'>all</option>
        @else
            @switch($user[0])
                @case('college')
                <select id ='courseGradeLevelFilter' class='form-control'>
                    <option value ='all'>all</option>
                    
                    @foreach($collegeOfUser as $college)
                        <option value = "{{$college->id}}">{{$college->courseName}}</option>
                    @endforeach
                @break
                @case('seniorhigh')
                    <select id ='courseGradeLevelFilter' class='form-control' readonly>
                        @foreach($strand as $strands)
                            <option value = "{{$strands->id}}">{{$strands->strandName}}</option>
                        @endforeach
                    </select>
                {{--  <select id ='courseGradeLevelFilter' class='form-control' readonly>
                    <option value ="{{$user[1]}}">{{$user[1]}}</option>  --}}
                @break
                @case('juniorhigh')
                    <select id ='courseGradeLevelFilter' class='form-control' readonly>
                        <option value ="all">all</option>
                @break
                @case('elementary')
                    <select id ='courseGradeLevelFilter' class='form-control' readonly>
                        <option value ="all">all</option>
                @break
                @default
                    Default case...
            @endswitch
        @endif
        </select>
    </div>
    <div class='col-md-2'>
        <label for ='gradeLevelFilter'>Year Level/Grade Level</label>
        @if(session()->get('user')['userType']=='admin')
                    <select id ='gradeLevelFilter' class='form-control'>
                            <option value ='all'>all</option>
        @else
            @switch($user[0])
                @case('college')
                <select id ='gradeLevelFilter' class='form-control'>
                    <option value ='all'>all</option>
                    @for($i=1; $i<$collegeOfUser->courseYear; $i++)
                        <option value ="{{$i}}">{{$i}}</option>
                    @endfor
                @break
                @case('seniorhigh')
                    <select id ='gradeLevelFilter' class='form-control' readonly>
                        <option value ="{{$user[1]}}">{{$user[1]}}</option>
                @break
                @case('juniorhigh')
                    <select id ='gradeLevelFilter' class='form-control' readonly>
                        <option value ="{{$user[1]}}">{{$user[1]}}</option>
                @break
                @case('elementary')
                    <select id ='gradeLevelFilter' class='form-control' readonly>
                        <option value ="{{$user[1]}}">{{$user[1]}}</option>
                @break
                @default
                    Default case...
            @endswitch
        @endif
        </select>
    </div>
    <div class='col-md-10 genericDivPadTop'>
        <div class="form-group has-feedback">
            <input type="text" class="form-control"  placeholder="{{$placeholder}}" id ='{{$searchID}}'/>
            <i class="fa fa-search form-control-feedback"></i>
        </div>
    </div>
    <div class='col-md-2 genericDivPadTop'>
        {{$button}}
      
    </div>
</div>
      