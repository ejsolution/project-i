@component('components.modal')
  @slot('modalid')
    manageLayoutModal
  @endslot
  @slot('modalsize')
    modal-md
  @endslot
  @slot('modaltitle') 
    Manage Layout
  @endslot
  @slot('modalcontent') 
  {!! Form::open([
    'id'=>'manageLayoutForm']) !!}
      <div class='row'>
        <div class='col-md-7'>
          <label for ='schoolNameTextManageLayout'>School Name:</label>
          <input type = 'text' id = 'schoolNameTextManageLayout' name = 'schoolNameTextManageLayout' required
          class='form-control'>
          <label for ='guidanceNameTextManageLayout'>Guidance Name:</label>
          <input type = 'text' id = 'guidanceNameTextManageLayout' name = 'guidanceNameTextManageLayout' required
          class='form-control'>
          @component('components.x_panel')
            @slot('title')
                School Address
            @endslot
            @slot('content')
              <label for ='streetTextManageLayout'>Street:</label>
              <input type = 'text' id = 'streetTextManageLayout' name = 'streetTextManageLayout' required
              class='form-control'>
              <label for ='barangayTextManageLayout'>Barangay:</label>
              <input type = 'text' id = 'barangayTextManageLayout' name = 'barangayTextManageLayout' required
              class='form-control'>
              <label for ='cityTextManageLayout'>City:</label>
              <input type = 'text' id = 'cityTextManageLayout' name = 'cityTextManageLayout' required
              class='form-control'>
            @endslot
          @endcomponent
          <label for = 'consecutiveContract'>Consecutive Contract</label>
          <input type = 'number' id = 'consecutiveContract' class='form-control' required name = 'consecutiveContract'>
        </div>
        <div class='col-md-5'>
          <div class='row genericDivBottom'>
            <label>School Seal</label>
            <img src = '{{asset('images/defaultimage.png')}}'
            class='img-circle profile_img' id ="schoolSealImage">
            <div class='row'>
              <div class='col-md-12'>
                  <input type = 'file' class='form-control' required name = "schoolSeal" id = "schoolSeal">
              </div>
              <div class='col-md-12'>
                <button type = "button" class='btn btn-block btn-danger' id = "removeSchoolSealImage">Remove</button>
              </div>
            </div>
          </div>
          <div class='row genericDivBottom'>
            <label>Guidance Seal</label>
            <img src = '{{asset('images/defaultimage.png')}}'
            class='img-circle profile_img' id = "guidanceSealImage">
            <div class='row'>
              <div class='col-md-12'>
                  <input type = 'file' class='form-control' required name = "guidanceSeal" id = "guidanceSeal">
              </div>
              <div class='col-md-12'>
                <button  type = "button" class='btn btn-block btn-danger' id = "removeGuidanceSealImage">Remove</button>
              </div>
            </div>
          </div>
        </div> 
      </div>
    {!! Form::close() !!}
  @endslot
  @slot('modalbutton')
      <button form = "manageLayoutForm" class="btn btn-warning"><i class='fa fa-save'></i>&nbsp;Save</button>
  @endslot
@endcomponent
