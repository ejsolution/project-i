$(document).ready(function(){
    tableLoader('5', 'elementaryStudentsTable');
    $('#searchElementaryStudent').closest('div').parent().next().remove();
    $('#searchElementaryStudent').closest('div').parent().removeClass('col-md-3').addClass('col-md-4');
    $('#studentTypeFilter').html("<option value ='elementary'>Elementary</option>").attr('readonly', true);
    $('#semesterFilter').closest('div').remove();
    $('#schoolYearFilter').children('option').each(function(){
        var temp = $(this).text().split(' ');
        console.log(temp);
        if(temp.length == 4){
           // alert("Hello");
            $('#schoolYearFilter').val($(this).val());
        }
    });
    getAjax('/dashboard/elementarystudents/'+$('#schoolYearFilter').val()+'/all/'+$('#courseGradeLevelFilter').val()+'/'+$('#searchElementaryStudent').val(), 'elementaryStudentsTable');
    var html = "<option value ='all'>all</option>"+
                    "<option value ='6'>6</option>"+
                    "<option value ='5'>5</option>"+
                    "<option value ='4'>4</option>"+
                    "<option value ='3'>3</option>"+
                    "<option value ='2'>2</option>"+
                    "<option value ='1'>1</option>";
            $('#courseGradeLevelFilter').html(html);
    $('#schoolYearFilter').on('change', function(){
        if($('#schoolYearFilter').val() != "all"){
          $.get('/report/schoolyear/'+$('#schoolYearFilter').val(), function(data){
            $('#semesterFilter').html("<option value='all'>all</option>");
            $('#semesterFilter').append(data);
            tableLoader('5', 'elementaryStudentsTable');
            getAjax('/dashboard/elementarystudents/'+$('#schoolYearFilter').val()+'/all/'+$('#courseGradeLevelFilter').val()+'/'+$('#searchElementaryStudent').val(), 'elementaryStudentsTable');
          });
        }else{
            tableLoader('5', 'elementaryStudentsTable');
            getAjax('/dashboard/elementarystudents/'+$('#schoolYearFilter').val()+'/all/'+$('#courseGradeLevelFilter').val()+'/'+$('#searchElementaryStudent').val(), 'elementaryStudentsTable');
        }
    });
    $('#semesterFilter').on('change', function(){
        tableLoader('5', 'elementaryStudentsTable');
        getAjax('/dashboard/elementarystudents/'+$('#schoolYearFilter').val()+'/all/'+$('#courseGradeLevelFilter').val()+'/'+$('#searchElementaryStudent').val(), 'elementaryStudentsTable');
    });
    $('#courseGradeLevelFilter').on('change', function(){
        tableLoader('5', 'elementaryStudentsTable');
        getAjax('/dashboard/elementarystudents/'+$('#schoolYearFilter').val()+'/all/'+$('#courseGradeLevelFilter').val()+'/'+$('#searchElementaryStudent').val(), 'elementaryStudentsTable');
    });
    $('#searchElementaryStudent').on('keydown', function(){
        tableLoader('5', 'elementaryStudentsTable');
        getAjax('/dashboard/elementarystudents/'+$('#schoolYearFilter').val()+'/all/'+$('#courseGradeLevelFilter').val()+'/'+$('#searchElementaryStudent').val(), 'elementaryStudentsTable');
    }); 
});