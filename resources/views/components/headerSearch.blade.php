<div class='row'>
  <div class = 'col-md-3'>
    <label id ='viewByStudentLabel'>{{$comboLabel}}</label>
    <select class='form-control' id = '{{$comboID}}'>{{$comboContent}}</select>
  </div>
  <div class='col-md-3'>
  </div>
  <div class='col-md-4 genericDivPadTop'>
    <div class="form-group has-feedback">
        <input type="text" class="form-control"  placeholder="{{$placeholder}}" id ='{{$searchID}}'/>
        <i class="fa fa-search form-control-feedback"></i>
    </div>
  </div>
  <div class='col-md-2 genericDivPadTop'>
    <button class='btn btn-block btn-dark' id = '{{$buttonID}}'>Add</button>
  </div>
</div>
