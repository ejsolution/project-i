<?php

namespace App\Http\Controllers\personality;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\personality;
use App\model\makeUpAssistance;
use App\model\systemDate;
class personalityController extends Controller
{
    public function getPersonality(){
        $systemdate = systemDate::all();
        //$personality = personality::all();
        $personality = personality::where('created_at', '<=', $systemdate[0]->systemDate)->get();
        if(count($personality)==0){
            return "<h2>There are no personalities yet</h2>";
        }else{
            $html ='';
            foreach ($personality as $personality) {
                $html .=$this->getPersonalityRow($personality);
            }
            return $html;
        }
    }
    public function getPersonalityRow($personality){
        return "<tr id ='personalityrow-$personality->id'>
                    <td>$personality->name</td>
                    <td>$personality->description</td>
                    <td>
                        <button class='btn btn-dark editPersonalityRow' id ='editPersonalityRow-$personality->id'>
                            <i class='fa fa-edit'></i>
                        </button>
                    </td>
                    <td>
                        <button class='btn btn-warning deletePersonalityRow' id ='deletePersonalityRow-$personality->id'>
                            <i class='fa fa-trash-o'></i>
                        </button>
                    </td>
                </tr>";
    }
    public function personalityInsert(Request $request){
        $this->validate($request, array(
            'name'=>'required|max:100',
            'description'=>'required|max:100'
        ));
        $personality = new personality;
        $personality->name =$request->name;
        $personality->description = $request->description;
        $personality->save();
        $personality->html =$this->getPersonalityRow($personality);
        return $personality;
    }
    public function personalityUpdate(Request $request){
        $this->validate($request, array(
            'name'=>'required|max:100',
            'description'=>'required|max:100'
        ));
        $personality = personality::find($request->id);
        $personality->name =$request->name;
        $personality->description = $request->description;
        $personality->save();
        //$personality->html =$this->getPersonalityRow($personality);
        return "saved";
    }
    public function deletePersonality($personalityid){
        $check =  makeUpAssistance::where('personalityID', '=',$personalityid)->count();
        if($check>0){
            return "cannotdelete";
        }
        $personality = personality::find($personalityid);
        $personality->forceDelete();
        return "deleted";
    }
    public function getPersonalityCheckbox(){
        $personality = personality::all();
        if(count($personality)==0){
            return "<h2>Please add a personality on the settings.</h2>";
        }
        $html ='';
        $index = 0;
        foreach ($personality as $personality) {
            // $html .="<tr id ='personalityCheckRow-$personality->id'>
            //             <td><input type ='radio' class='radioPersonality' id ='radioPersonality-$personality->id'
            //                     name ='radioPersonality' value ='$personality->id' required></td>
            //             <td>$personality->name</td>
            //             <td>$personality->description</td>
            //         </tr>
            //         ";
            if($index == 0){
                $html .="<tr id ='personalityCheckRow-$personality->id'>
                            <td><input type ='checkbox' class='radioPersonality' id ='radioPersonality-$personality->id'
                                    name ='radioPersonality-$personality->id' value ='$personality->id' >$personality->name</td>
                        ";
            }else{
                if($index%3 ==0){
                    $html .="
                            <td><input type ='checkbox' class='radioPersonality' id ='radioPersonality-$personality->id'
                                    name ='radioPersonality-$personality->id' value ='$personality->id' >$personality->name</td>
                        </tr>";
                    $index = 0;
                }else{
                    $html .="
                            <td><input type ='checkbox' class='radioPersonality' id ='radioPersonality-$personality->id'
                                    name ='radioPersonality-$personality->id' value ='$personality->id' >$personality->name</td>
                        ";
                }
            }
            $index++;
        }
        return $html;
    }
}
