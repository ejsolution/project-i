$(document).ready(function(){
  var arraySelectedStudents = [];
  var arraySelectedFamily = [];
  tableLoader('3', 'sentMessagesTable');
  tableLoader('3', 'selectStudentsForRecipent');
  tableLoader('5', 'tableForGuardianSelectRecipent');
  $.get('/messages/students/guardian/recipient/all/'+$('#searchGuardianRecipient').val(), function(data){
    $('#tableForGuardianSelectRecipent').html(data);
  });
  $.get('/messages/students/recipient/all/'+$('#searchStudentsRecipient').val(), function(data){
    $('#selectStudentsForRecipent').html(data);
  });
  $.get('/messages/list/all/'+$('#searchSentText').val()+'/', function(data){
    $('#sentMessagesTable').html(data);
  });
  $("#newMessageButton").on('click', function(){
    //$("#newMessageModal").modal("show");
    $("#newMessageModal").slideToggle();
  });
  $("#closeNewMessage").on('click', function(){
    $("#newMessageModal").slideToggle(); 
  });
  $("#newGroupMessageButton").on('click', function(){
    $("#newGroupMessageModal").slideToggle();
  });
  $("#closeNewGroupMessage").on('click', function(){
    $("#newGroupMessageModal").slideToggle();
  });
  $("#addRecipientNewMessageModalButton").on('click', function(){
    $("#contactNoModal").modal("show");
  });
  $("#addRecipientNewGroupMessageModalButton").on('click', function(){
    $("#selectManyRecipientsModal").modal("show");
  });
  $('#sentSchoolYearCombo').on('change', function(){
    tableLoader('3', 'sentMessagesTable');
    if($(this).val()!='all'){
      $.get('/messages/schoolyear/type/'+$(this).val(), function(data){
        switch (data) {
          case '2':
            var html ="<option value ='1'>1</option>"
                      +"<option value ='2'>2</option>";
            $('#sentSemesterCombo').html(html);
          break;
          case '1':
            var html ="<option value ='1'>1</option>"
                      +"<option value ='2'>2</option>"
                      +"<option value ='3'>3</option>";
            $('#sentSemesterCombo').html(html);
          break;
          default:
            break;
        }
        if($('#searchSentText').val() == ''){
          $.get('/messages/list2/'+$('#sentSchoolYearCombo').val()+'/'+$('#sentSemesterCombo').val(), function(data){
            $('#sentMessagesTable').html(data);
          });
        }else{
          $.get('/messages/list/'+$('#sentSchoolYearCombo').val()+'/'+$('#searchSentText').val()+'/'+$('#sentSemesterCombo').val(), function(data){
            $('#sentMessagesTable').html(data);
          });
        }
      });
    }else{
      $.get('/messages/list/all/'+$('#searchSentText').val()+'/', function(data){
        $('#sentMessagesTable').html(data);
      });  
    }
  });
  $('#sentSemesterCombo').on('change', function(){
    tableLoader('3', 'sentMessagesTable');
    if($('#sentSchoolYearCombo').val()!='all'){
      if($('#searchSentText').val()!=''){
        $.get('/messages/list/'+$('#sentSchoolYearCombo').val()+'/'+$('#searchSentText').val()+'/'+$('#sentSemesterCombo').val(), function(data){
          $('#sentMessagesTable').html(data);
        });
      }else{
        $.get('/messages/list2/'+$('#sentSchoolYearCombo').val()+'/'+$('#sentSemesterCombo').val(), function(data){
          $('#sentMessagesTable').html(data);
        });
      }
    }
  });
  $('#searchSentText').on('keydown', function(){
    tableLoader('3', 'sentMessagesTable');
    if($('#sentSchoolYearCombo').val()=='all'){
      //if($('#searchSentText').val()){
        $.get('/messages/list/all/'+$('#searchSentText').val()+'/', function(data){
          $('#sentMessagesTable').html(data);
        });
    }else{
      if($('#searchSentText').val()==''){
        $.get('/messages/list2/'+$('#sentSchoolYearCombo').val()+'/'+$('#sentSemesterCombo').val(), function(data){
          $('#sentMessagesTable').html(data);
        });
      }else{
        $.get('/messages/list/'+$('#sentSchoolYearCombo').val()+'/'+$('#searchSentText').val()+'/'+$('#sentSemesterCombo').val(), function(data){
          $('#sentMessagesTable').html(data);
        });
      }
    }
  });
  $('#selectByStudentTypeRecipent').on('change', function(){
    $.get('/messages/students/recipient/'+$('#selectByStudentTypeRecipent').val()+'/'+$('#searchStudentsRecipient').val(), function(data){
      $('#selectStudentsForRecipent').html(data);
      $('#tableForStudentRecipent').children('tr').each(function(){
        $('#selectStudent-'+$(this).prop('id').split('-')[1]).attr('disabled', true);
      });
    });
  });
  $('#searchStudentsRecipient').on('keydown', function(){
    $.get('/messages/students/recipient/all/'+$('#searchStudentsRecipient').val(), function(data){
      $('#selectStudentsForRecipent').html(data);
      $('#tableForStudentRecipent').children('tr').each(function(){
        $('#selectStudent-'+$(this).prop('id').split('-')[1]).attr('disabled', true);
      });
    });
  });
  $('#selectStudentsForRecipent').on('click', '.selectStudent', function(){
    var id = $(this).prop('id').split('-')[1];
    $(this).attr('disabled', true);
    var temp = [];
    var i=0;
    $(this).closest('tr').children('td').each(function () {
      temp[i]= $(this).html();
      i++;
    })
    var html = "<tr id ='studentRecipient-"+id+"'>"+
                  "<td>"+temp[0]+"</td>"+
                  "<td>"+temp[1]+"</td>"+
                  "<td><button class='btn btn-warning recipientStudent' id ='recipientStudent-"+id+"'><i class='fa fa-trash-o'></i></button></td>"+
                "</tr>";
    $('#tableForStudentRecipent').prepend(html);
  });
  $('#tableForStudentRecipent').on('click', '.recipientStudent', function(){
    var id  = $(this).prop('id').split('-')[1];
    $(this).closest('tr').remove();
    $('#selectStudent-'+id).attr('disabled', false);
  });
  $('#viewBtStudentGuardianType').on('change', function(){
    tableLoader('5', 'tableForGuardianSelectRecipent');
    $.get('/messages/students/guardian/recipient/'+$(this).val()+'/'+$('#searchGuardianRecipient').val(), function(data){
      $('#tableForGuardianSelectRecipent').html(data);
      $('#tableForGuardianRecipent').children('tr').each(function(){
        $('#selectGuardian-'+$(this).prop('id').split('-')[1]).attr('disabled', true);
      });
    });
  });
  $('#searchGuardianRecipient').on('keydown', function(){
    tableLoader('5', 'tableForGuardianSelectRecipent');
    $.get('/messages/students/guardian/recipient/'+$('#viewBtStudentGuardianType').val()+'/'+$('#searchGuardianRecipient').val(), function(data){
      $('#tableForGuardianSelectRecipent').html(data);
      $('#tableForGuardianRecipent').children('tr').each(function(){
        $('#selectGuardian-'+$(this).prop('id').split('-')[1]).attr('disabled', true);
      });
    });
  });
  $('#tableForGuardianSelectRecipent').on('click', '.selectGuardian', function(){
    var id = $(this).prop('id').split('-')[1];
    $(this).attr('disabled', true);
    var html ='';
    var temp = [];
    var i=0;
    $(this).closest('tr').children('td').each(function(){
      temp[i] = $(this).html();
      i++;
    });

    html = "<tr id ='selectedRecipientGuardian-"+id+"'>"+
              "<td>"+temp[0]+"</td>"+
              "<td>"+temp[1]+"</td>"+
              "<td>"+temp[2]+"</td>"+
              "<td>"+temp[3]+"</td>"+
              "<td><button class='btn btn-warning recipientGuardianSelected' id ='recipientGuardianSelected-"+id+"'><i class='fa fa-trash-o'></i></button></td>"+
            "</tr>";
    $('#tableForGuardianRecipent').prepend(html);
  });
  $('#tableForGuardianRecipent').on('click', '.recipientGuardianSelected', function(){
    var id = $(this).prop('id').split('-')[1];
    $(this).closest('tr').remove();
    $('#selectGuardian-'+id).attr('disabled', false);
  });
  $('#addAllRecipients').on('click', function(){
    var i = 0;
    var y =0;
    
    var z=0;
    arraySelectedStudents = [];
    arraySelectedFamily = [];
    $('#receiverNewMessageModalText').val('');
    var recipients = $('#receiverNewMessageModalText').val();
    $('#tableForGuardianRecipent').children('tr').each(function(){
      arraySelectedFamily[y] = $(this).prop('id').split('-')[1];
      y++;
      z=0;
      $(this).children('td').each(function(){
        if(z==1){
          recipients += $(this).html()+', ';
          //return false;
        }
        z++;
      });
    });
    $('#tableForStudentRecipent').children('tr').each(function(){
      arraySelectedStudents[i] = $(this).prop('id').split('-')[1];
      i++;
      $(this).children('td').each(function(){
          recipients += $(this).html()+', ';
          return false;
      });
    });
    $('#receiverNewMessageModalText').val(recipients);
    $('#contactNoModal').modal('hide');
  });
  $('#sendMessageForm').on('submit', function(e){
    e.preventDefault(); 
    if(arraySelectedStudents.length==0 && arraySelectedFamily.length == 0){
      new PNotify({
        title: 'Error!',
        text: 'Please select a recipients to send this message to!',
        type: 'error',
        styling: 'bootstrap3'
      });
      return;
    }
    $('#statusMessageModal').modal('show');
    divLoader('messageLoader');
   
    var form = document.getElementById('sendMessageForm');
    var myform = new FormData(form);
    //myform.append('students', arraySelectedStudents);
    //myform.append('family', arraySelectedFamily);
    myform.append('recipient', '0');
    myform.append('sendTo', 'student');
    for (var index =0; index < arraySelectedStudents.length; index++) {
      myform.set('recipient', arraySelectedStudents[index]);
      myform.set('sendTo', 'student');
      prependPostAjaxErrorCallback('/message/sendindividual', myform, function(data){
        if(data.data != data.data.substring(0, 8)!='Something'){
          if(index == arraySelectedStudents.length-1){
            if(arraySelectedFamily.length==0){
              new PNotify({
                title: 'Success!',
                text: 'All messages have been sent!',
                type: 'success',
                styling: 'bootstrap3'
              });
              $('#statusMessageModal').modal('hide');
              $('#closeNewMessage').trigger('click');
            }else{
              new PNotify({
                title: 'Success!',
                text: data.data,
                type: 'success',
                styling: 'bootstrap3'
              });
            }
          }else{
            new PNotify({
              title: 'Success!',
              text: data.data,
              type: 'success',
              styling: 'bootstrap3'
            });
          }
          $('#sentMessagesTable').children('h2').each(function(){
            $(this).remove();
          });
          $('#sentMessagesTable').prepend(data.html);
        }else{
          new PNotify({
            title: 'Error!',
            text: data.data,
            type: 'error',
            styling: 'bootstrap3'
          });
        }
      });   
    }
    for (var index =0; index < arraySelectedFamily.length; index++) {
      myform.set('recipient', arraySelectedFamily[index]);
      myform.set('sendTo', 'family');
      prependPostAjaxErrorCallback('/message/sendindividual', myform, function(data){
        if(data.data != data.data.substring(0, 8)!='Something'){
          if(index == arraySelectedFamily.length-1){
            new PNotify({
              title: 'Success!',
              text: 'All messages have been sent!',
              type: 'success',
              styling: 'bootstrap3'
            });
            $('#statusMessageModal').modal('hide');
            $('#closeNewMessage').trigger('click');
          }else{
            new PNotify({
              title: 'Success!',
              text: data.data,
              type: 'success',
              styling: 'bootstrap3'
            });
          }
          $('#sentMessagesTable').children('h2').each(function(){
            $(this).remove();
          });
          $('#sentMessagesTable').prepend(data.html);
        }else{
          new PNotify({
            title: 'Error!',
            text: data.data,
            type: 'error',
            styling: 'bootstrap3'
          });
        }
      });   
    }
    //$('#newMessageModal').modal('hide');
  });
  $('#sentMessagesTable').on('click', 'tr', function(){
    var temp =[];
    var i=0;
    $(this).children('td').each(function(){
      temp[i]= $(this).html();
      i++;
    });
    $('#messageOfSenderInbox').val(temp[2]);
  });
  $('#buttonForwardMessage').on('click', function(){
    $('#newMessageButton').trigger('click');
    $('#messageNewMessageModalText').val($('#messageOfSenderInbox').val());
    $('#receiverNewMessageModalText').val('');
  });
  $('#selectGroupsToSendButton').on('click', function(){
    var check = $('#checkBoxGroupContainer').find('input[type=checkbox]:checked').length;
    if(check == 0){
      new PNotify({
        title: 'Error!',
        text: 'Please check atleast one of the student type!',
        type: 'error',
        styling: 'bootstrap3'
      });
      return;
    }else{
      var names='';
      if($('#collageCheckbox').is(':checked')){
        names +='college, ';
      }
      if($('#seniorHighCheckbox').is(':checked')){
        names +='seniorhigh, ';
      }
      if($('#juniorHighCheckbox').is(':checked')){
        names +='juniorhigh, ';
      }
      if($('#elementaryCheckbox').is(':checked')){
        names +='elementary ';
      }
      $('#receiverNewGroupMessageModalText').val(names);
      $('#selectManyRecipientsModal').modal('hide');
    }
  });
  $('#sendGroupMessageForm').on('submit', function(e){
    e.preventDefault();
    var form = document.getElementById('sendGroupMessageForm');
    var myform = new FormData(form);
    //$('#newGroupMessageModal').modal('hide');
    $('#statusMessageModal').modal('show');
    divLoader('messageLoader');
    prependPostAjaxErrorCallback('/message/sendgroup', myform, function(data){
      $('#sentMessagesTable').prepend(data);
      new PNotify({
        title: 'Success!',
        text: 'Messages has been sent!',
        type: 'success',
        styling: 'bootstrap3'
      });
      $('#closeNewGroupMessage').trigger('click');
      $('#statusMessageModal').modal('hide');
    });
    
  });
});
