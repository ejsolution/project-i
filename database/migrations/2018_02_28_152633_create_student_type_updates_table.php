<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentTypeUpdatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_type_updates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('studentID')->unsigned()->nullable();
            $table->string("studentType")->nullable();
            $table->string("strand")->nullable();
            $table->string("gradeLevel")->nullable();
            $table->string("courseName")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_type_updates');
    }
}
