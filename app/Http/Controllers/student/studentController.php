<?php

namespace App\Http\Controllers\student;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\model\schoolYear;
use App\model\college;
use App\model\course;
use App\Http\Controllers\activityController;
use App\Http\Controllers\notification\notificationController;
use App\model\student;
use App\model\healthRecord;
use App\model\collegeStudent;
use App\model\SHSStudent;
use App\model\JHStudent;
use App\model\elemStudent;
use App\model\personality;
use App\model\studentLeisureCollege;
use App\model\studentLeisureElem;
use App\model\studentLeisureJH;
use App\model\studentLeisureSH;
use App\model\contract;
use App\model\seniorHighStrand;
use App\model\organization;
use App\model\familyInfo;
use App\model\family;
use App\model\schoolBG;
use App\model\subject;
use App\model\favSubjectList;
use App\model\dislikedSubject;
use App\model\makeUpAssistance;
use App\model\studentUpdates;
use App\model\systemDate;
use App\model\assignmentCollege;
use App\model\studentTypeUpdate;
use App\model\adminsetting;
use Image;
use DB;
use Session;
use Auth;
use PDF;
use App\Rules\systemTimeRule;
use App\Rules\studentNoRule;
use App\Http\Controllers\users\userController;

class studentController extends Controller
{
    private $request;
    private $student;
    private $html; 
    private $rowCounter;
    public function __construct(){
        $this->middleware('auth:myuser');
    }
    public function getStudentPersonalData(){
        $colleges = college::all();
        $schoolyear = schoolYear::where('status', '=', 1)->get();
        $thisyear = '';
        $schoolyears = schoolYear::all(); 
        if(count($schoolyear)>0){
            $temp = explode('-', $schoolyear[0]->schoolYearStart);
            $thisyear = $temp[0];
            $temp = explode('-', $schoolyear[0]->schoolYearEnd);
            $thisyear .=' - '.$temp[0];
        }
        $user =  (new userController)->getUserDesignation(Auth::guard('myuser')->id());
        $college ='';
        if($user[0]=='college'){
            $college = DB::select(DB::raw("SELECT colleges.collegeName, courses.courseName, 
            courses.id, courses.courseYear FROM colleges INNER JOIN (SELECT collegeID, userID, id FROM 
            assignment_colleges INNER JOIN (SELECT max(id) AS maxid FROM assignment_colleges 
            GROUP BY userID) ac ON ac.maxid = assignment_colleges.id) ac2 ON ac2.collegeID = 
            colleges.id INNER JOIN (SELECT collegeID, departmentID, id FROM 
            department_college_hists INNER JOIN (SELECT max(id) as maxid FROM 
            department_college_hists GROUP BY departmentID) dch ON dch.maxid = 
            department_college_hists.id) dch2 ON dch2.collegeID = colleges.id 
            INNER JOIN (SELECT courseID, departmentID, id FROM course_department_hists 
            INNER JOIN (SELECT max(id) as maxid FROM course_department_hists GROUP BY courseID) 
            cdh ON cdh.maxid = course_department_hists.id) cdh2 ON cdh2.departmentID = 
            dch2.departmentID INNER JOIN courses on courses.id  = cdh2.courseID WHERE 
            ac2.collegeID = (SELECT collegeID FROM assignment_colleges WHERE id = 
            (SELECT max(id) FROM assignment_colleges WHERE userID ='".Auth::guard('myuser')->id()."'))
            GROUP BY courses.id"));
        }
        $notif = (new notificationController)->getNotifs();
        $collegeOfNotAdmin = DB::select(DB::raw("SELECT colleges.collegeName, colleges.id FROM colleges 
        INNER JOIN (SELECT collegeID, id, userID FROM assignment_colleges INNER 
        JOIN (SELECT MAX(id) as maxid FROM assignment_colleges GROUP BY userID) 
        ac ON ac.maxid = assignment_colleges.id) ac2 ON ac2.collegeID = colleges.id 
        WHERE ac2.userID = '".Auth::guard('myuser')->id()."'"));
        $collegeid = '';
        if(count($collegeOfNotAdmin)>0){
            $collegeid = $collegeOfNotAdmin[0]->id;
        } 
        $seniorhighstrand = seniorHighStrand::all();
        return view('admin/studentPersonalData/studentPersonalData', ['currentsem' => $this->getCurrentSem(),
                                                                      'college' =>$colleges,
                                                                      'schoolyear'=>$thisyear,
                                                                      'language' => $this->getStudentData('language'),
                                                                      'religion' =>$this->getStudentData('religion'),
                                                                      'ethnicity' =>$this->getStudentData('ethnicity'),
                                                                      'programs'=>$this->getHealthRecords('programs'),
                                                                      'ailments'=>$this->getHealthRecords('ailments'), 
                                                                      'personality' => $this->getPersonalities(),
                                                                      'howdidyoumakethischoice'=> $this->getPersonalities('college', 'howdidyoumakethischoice'),
                                                                      'howdidyoucometothisschool' => $this->getPersonalities('college', 'howdidyoucometothisschool'),
                                                                      'financialsupport'=> $this->getPersonalities('college', 'financialsupport'), 
                                                                      'strands' => $this->getSeniorHighStrands(),
                                                                      'schoolyears'=>$schoolyears,
                                                                       'user'=>$user,
                                                                       'collegeOfUser'=>$college,
                                                                       'notif'=>$notif,
                                                                       'strand' => $seniorhighstrand,
                                                                       'collegeOfNotAdmin'=>$collegeid]);
    }
    public function getStrands(){
        $strand  = seniorHighStrand::all();
        $html = '';
        foreach ($strand as $strands) {
            $html .= "<option value ='$strands->id'>$strands->strandName</option>";
        }
        return $html;
    }
    public function getCurrentSem(){
        //$todaydate = date('Y-m-d');
        $todaydate;
        $systemdate = systemDate::all();
        if(count($systemdate)>0){
            $todaydate =  $systemdate[0]->systemDate;
        }else{
            $todaydate = date('Y-m-d');
        }
        $currentschoolyear = schoolYear::where('status', '=', 1)->get();
        if(count($currentschoolyear)>0){
            if($currentschoolyear[0]->semesterType=='3'){
                if(strtotime($todaydate)>strtotime($currentschoolyear[0]->thirdSemesterStart)){
                    return '3';    
                }else{
                    if(strtotime($todaydate)>strtotime($currentschoolyear[0]->secondSemesterStart)){
                        return '2';    
                    }else{
                        return '1';
                    }
                }
            }else{
                if(strtotime($todaydate)>strtotime($currentschoolyear[0]->secondSemesterStart)){
                    return '2';    
                }else{
                    return '1';
                }
            }
        }else{
            return '0';
        }
    }
    public function getCollegeOfCourse($courseid){
        $course = DB::select(DB::raw("SELECT col.id FROM courses c
       INNER JOIN (SELECT id, courseID, departmentID FROM
       course_department_hists INNER JOIN
       (SELECT max(id) AS maxid FROM course_department_hists
       group by courseID) cp ON cp.maxid =
       course_department_hists.id) t ON c.id = t.courseID
       INNER JOIN (SELECT id, departmentID, collegeID FROM
       department_college_hists INNER JOIN
       (SELECT max(id) AS maxid FROM department_college_hists
       group by departmentID) cp ON cp.maxid =
       department_college_hists.id) d ON d.departmentID = t.departmentID
       INNER JOIN colleges col ON col.id = d.collegeID WHERE c.id = '$courseid'"));
        if(count($course)>0){
            return $course[0]->id;
        }else{
            return 0;
        }
    }
    public function getBeautifiedName($firstname, $middlename, $lastname){
        $middlename2 = '';
        if($middlename != ''){
            $middlename2 = ucfirst(substr($middlename, 0, 1)).".";
        }
        return $lastname.", ".$firstname." $middlename2";
    }
    public function getCourses($collegeid){
        //$systemdate = systemDate::all();
        $course = DB::select(DB::raw("SELECT c.courseCode, c.id, c.courseName, c.courseYear FROM courses c
        INNER JOIN (SELECT id, courseID, departmentID FROM
        course_department_hists INNER JOIN
        (SELECT max(id) AS maxid FROM course_department_hists
        group by courseID) cp ON cp.maxid =
        course_department_hists.id) t ON c.id = t.courseID
        INNER JOIN (SELECT id, departmentID, collegeID FROM
        department_college_hists INNER JOIN
        (SELECT max(id) AS maxid FROM department_college_hists
        group by departmentID) cp ON cp.maxid =
        department_college_hists.id) d ON d.departmentID = t.departmentID
        INNER JOIN colleges col ON col.id = d.collegeID WHERE col.id = '$collegeid'
        "));
        if(count($course)==0){
            return "nocourses";
        }else{
            $html = "";
            foreach ($course as $course) {
                $html .="<option value = '$course->id'>$course->courseName</option>";
        }
            return $html;
       }
    }
    public function getCourseYear($courseid){
        $course = course::find($courseid);
        $html = "";
        for ($i=1; $i<= $course->courseYear ; $i++) { 
            $html .= "<option value ='$i'>$i</option>";
        }
        return $html;
    }
    public function getStudentData($column){
        switch ($column) {
            case 'language':
                $student = student::select('language')->groupBy('language');
                return $student;
                break;
            case 'religion':
                $student = student::select('religion')->groupBy('religion');
                //error_log($student);
                return $student;
            break;
            case 'ethnicity':
                $student = student::select('ethnicity')->groupBy('ethnicity');
                return $student;
            break;
            default:
                # code...
                break;
        }
        
    }
    public function getHealthRecords($column){
        
        switch ($column) {
            case 'programs':
                $health = healthRecord::all()->groupBy('programsParticipated'); 
                return $health;
                break;
            case 'ailments':
                $health = healthRecord::all()->groupBy('physicalAilment');
                return $health;
            break;
            default:
                # code...
                break;
        }
    }
    public function getStudentCollection($coursegrade, &$student, $semester, $schoolyear, $studenttypes){
        foreach ($student as $key => $value) {
            $studenttype = $this->getStudentType($student[$key]->id);
            switch ($studenttype) {
                case 'college':
                    if($coursegrade != "all"){
                        $temp = collegeStudent::where('id', '=', DB::select(DB::raw("(SELECT MAX(id) 
                                                FROM college_students WHERE studentID = '".$student[$key]->id."')"))[0]->id)
                                                ->get();
                        if(count($temp)>0){
                            if($temp[0]->courseID != $coursegrade){
                                $student->forget($key);
                            }
                        }
                    }
                    if($semester != "all"){
                        $temp = collegeStudent::where('id', '=', DB::select(DB::raw("(SELECT MAX(id) 
                                                FROM college_students WHERE studentID = '".$student[$key]->id."')"))[0]->id)
                                                ->get();
                        if(count($temp)>0){
                            if($temp[0]->semesterID != $semester){
                                $student->forget($key);
                            }
                        }
                    }
                    if($schoolyear != "all"){
                        $temp = collegeStudent::where('id', '=', DB::select(DB::raw("(SELECT MAX(id) AS id
                                                FROM college_students WHERE studentID = '".$student[$key]->id."')"))[0]->id)
                                                ->get();
                        if(count($temp)>0){
                            if($temp[0]->schoolYearID != $schoolyear){
                                $student->forget($key);
                            }
                        }
                    }
                    if($studenttypes!= 'all'){
                        if($studenttype!= $studenttypes){
                            $student->forget($key);
                        }
                    }
                break;
                case 'seniorhigh':
                    if($coursegrade != "all"){
                        $temp = SHSStudent::where('id', '=', DB::select(DB::raw("(SELECT MAX(id) AS id
                                            FROM s_h_s_students WHERE studentID = '".$student[$key]->id."')"))[0]->id)
                                            ->get();
                        if(count($temp)>0){
                            if($temp[0]->gradeLevel != $coursegrade){
                                $student->forget($key);
                            }
                        }
                    }
                    if($schoolyear != "all"){
                        $temp = SHSStudent::where('id', '=', DB::select(DB::raw("(SELECT MAX(id) AS id
                                            FROM s_h_s_students WHERE studentID = '".$student[$key]->id."')"))[0]->id)
                                            ->get();
                        if(count($temp)>0){
                            if($temp[0]->schoolYearID != $schoolyear){
                                $student->forget($key);
                            }
                        }
                    }
                    if($studenttypes!= 'all'){
                        if($studenttype!= $studenttypes){
                            $student->forget($key);
                        }
                    }
                break;
                case 'juniorhigh':
                    if($coursegrade != "all"){
                        $temp = JHStudent::where('id', '=', DB::select(DB::raw("(SELECT MAX(id) AS id
                                            FROM j_h_students WHERE studentID = '".$student[$key]->id."')"))[0]->id)
                                            ->get();
                        if(count($temp)>0){
                            if($temp[0]->gradeLevel != $coursegrade){
                                $student->forget($key);
                            }
                        }
                    }
                    if($schoolyear != "all"){
                        $temp = JHStudent::where('id', '=', DB::select(DB::raw("(SELECT MAX(id) AS id
                                            FROM j_h_students WHERE studentID = '".$student[$key]->id."')"))[0]->id)
                                            ->get();
                        if(count($temp)>0){
                            if($temp[0]->schoolYearID != $schoolyear){
                                $student->forget($key);
                            }
                        }
                    }
                    if($studenttypes!= 'all'){
                        if($studenttype!= $studenttypes){
                            $student->forget($key);
                        }
                    }
                break;
                case 'elementary':
                    if($coursegrade != "all"){
                        $temp = elemStudent::where('id', '=', DB::select(DB::raw("(SELECT MAX(id) AS id
                                            FROM elem_students WHERE studentID = '".$student[$key]->id."')"))[0]->id)
                                            ->get();
                        if(count($temp)>0){
                            if($temp[0]->gradeLevel != $coursegrade){
                                $student->forget($key);
                            }
                        }
                    }
                    if($schoolyear != "all"){
                        $temp = elemStudent::where('id', '=', DB::select(DB::raw("(SELECT MAX(id) AS id
                                            FROM elem_students WHERE studentID = '".$student[$key]->id."')"))[0]->id)
                                            ->get();
                        if(count($temp)>0){
                            if($temp[0]->schoolYearID != $schoolyear){
                                $student->forget($key);
                            }
                        }
                    }
                    if($studenttypes!= 'all'){
                        if($studenttype!= $studenttypes){
                            $student->forget($key);
                        }
                    }
                break;
                default:
                    # code...
                    break;
            }
        }
    } 
    public function getStudents($viewby, $schoolyear, $semester, $coursegrade, $search=''){
        //$student = student::all();
        $systemdate = systemDate::all();
        $student;
        switch($viewby){
            case 'all':
                $student = student::whereDate("created_at", '<=', $systemdate[0]->systemDate)->get();
                $this->getStudentCollection($coursegrade, $student, $semester, $schoolyear, $viewby);
            break;
            case 'college':
                if($schoolyear == 'all' && $semester == 'all' && $coursegrade == 'all'){
                    $student = student::join('college_students', 'college_students.studentID', '=',
                                            'students.id')
                                        ->select('students.*')
                                        ->whereDate("students.created_at", '<=', $systemdate[0]->systemDate)->get();
                }elseif ($schoolyear == 'all' && $semester == 'all' && $coursegrade != 'all') {
                    $student = student::join('college_students', 'college_students.studentID', '=',
                                            'students.id')
                                        ->select('students.*')
                                        ->whereDate("students.created_at", '<=', $systemdate[0]->systemDate)
                                        ->where("college_students.courseID", '=', $coursegrade)
                                        ->get();
                }elseif ($schoolyear == 'all' && $semester != 'all' && $coursegrade == 'all') {
                    $student = student::join('college_students', 'college_students.studentID', '=',
                                            'students.id')
                                        ->select('students.*')
                                        ->whereDate("students.created_at", '<=', $systemdate[0]->systemDate)
                                        ->where("college_students.semesterID", '=', $semester)
                                        ->get();
                }elseif ($schoolyear == 'all' && $semester != 'all' && $coursegrade != 'all') {
                    $student = student::join('college_students', 'college_students.studentID', '=',
                                            'students.id')
                                        ->select('students.*')
                                        ->whereDate("students.created_at", '<=', $systemdate[0]->systemDate)
                                        ->where("college_students.semesterID", '=', $semester)
                                        ->where("college_students.courseID", '=', $coursegrade)
                                        ->get();
                }elseif ($schoolyear != 'all' && $semester == 'all' && $coursegrade == 'all') {
                    $student = student::join('college_students', 'college_students.studentID', '=',
                                            'students.id')
                                        ->select('students.*')
                                        ->whereDate("students.created_at", '<=', $systemdate[0]->systemDate)
                                        ->where("college_students.schoolYearID", '=', $schoolyear)
                                        ->get();
                }elseif ($schoolyear != 'all' && $semester == 'all' && $coursegrade != 'all') {
                    $student = student::join('college_students', 'college_students.studentID', '=',
                                    'students.id')
                                    ->select('students.*')
                                    ->whereDate("students.created_at", '<=', $systemdate[0]->systemDate)
                                    ->where("college_students.schoolYearID", '=', $schoolyear)
                                    ->where("college_students.courseID", '=', $coursegrade)
                                    ->get();
                }elseif ($schoolyear != 'all' && $semester != 'all' && $coursegrade == 'all') {
                    $student = student::join('college_students', 'college_students.studentID', '=',
                                        'students.id')
                                        ->select('students.*')
                                        ->whereDate("students.created_at", '<=', $systemdate[0]->systemDate)
                                        ->where("college_students.schoolYearID", '=', $schoolyear)
                                        ->where("college_students.semesterID", '=', $semester)
                                        ->get();
                }elseif ($schoolyear != 'all' && $semester != 'all' && $coursegrade != 'all') {
                    $student = student::join('college_students', 'college_students.studentID', '=',
                                        'students.id')
                                        ->select('students.*')
                                        ->whereDate("students.created_at", '<=', $systemdate[0]->systemDate)
                                        ->where("college_students.schoolYearID", '=', $schoolyear)
                                        ->where("college_students.semesterID", '=', $semester)
                                        ->where("college_students.courseID", '=', $coursegrade)
                                        ->get();               
                }else{
                    $student = student::whereDate("created_at", '<=', $systemdate[0]->systemDate)->get();
                }  
            break;
            case 'seniorhigh':
                if($schoolyear == 'all' && $coursegrade == 'all'){
                    $student = student::join('s_h_s_students', 's_h_s_students.studentID', '=',
                                            'students.id')
                                        ->select('students.*')
                                        ->whereDate("students.created_at", '<=', $systemdate[0]->systemDate)->get();
                }elseif ($schoolyear == 'all' && $coursegrade != 'all') {
                    $student = student::join('s_h_s_students', 's_h_s_students.studentID', '=',
                                        'students.id')
                                    ->select('students.*')
                                    ->whereDate("students.created_at", '<=', $systemdate[0]->systemDate)
                                    ->where('s_h_s_students.gradeLevel', '=', $coursegrade)
                                    ->get();
                }elseif ($schoolyear != 'all' && $coursegrade == 'all') {
                    $student = student::join('s_h_s_students', 's_h_s_students.studentID', '=',
                                        'students.id')
                                    ->select('students.*')
                                    ->whereDate("students.created_at", '<=', $systemdate[0]->systemDate)
                                    ->where('s_h_s_students.schoolYearID', '=', $schoolyear)
                                    ->get();
                }elseif ($schoolyear != 'all' && $coursegrade != 'all') {
                    $student = student::join('s_h_s_students', 's_h_s_students.studentID', '=',
                                        'students.id')
                                    ->select('students.*')
                                    ->whereDate("students.created_at", '<=', $systemdate[0]->systemDate)
                                    ->where('s_h_s_students.schoolYearID', '=', $schoolyear)
                                    ->where('s_h_s_students.gradeLevel', '=', $coursegrade)
                                    ->get();
                }else{
                    $student = student::join('s_h_s_students', 's_h_s_students.studentID', '=',
                                            'students.id')
                                        ->select('students.*')
                                        ->whereDate("students.created_at", '<=', $systemdate[0]->systemDate)->get();
                }
            
            break;
            case 'juniorhigh':
                if($schoolyear == 'all' && $coursegrade == 'all'){
                    $student = student::join('j_h_students', 'j_h_students.studentID', '=',
                                            'students.id')
                                        ->select('students.*')
                                        ->whereDate("students.created_at", '<=', $systemdate[0]->systemDate)->get();
                }elseif ($schoolyear == 'all' && $coursegrade != 'all') {
                    $student = student::join('j_h_students', 'j_h_students.studentID', '=',
                                        'students.id')
                                    ->select('students.*')
                                    ->whereDate("students.created_at", '<=', $systemdate[0]->systemDate)
                                    ->where('j_h_students.gradeLevel', '=', $coursegrade)
                                    ->get();
                }elseif ($schoolyear != 'all' && $coursegrade == 'all') {
                    $student = student::join('j_h_students', 'j_h_students.studentID', '=',
                                        'students.id')
                                    ->select('students.*')
                                    ->whereDate("students.created_at", '<=', $systemdate[0]->systemDate)
                                    ->where('j_h_students.schoolYearID', '=', $schoolyear)
                                    ->get();
                }elseif ($schoolyear != 'all' && $coursegrade != 'all') {
                    $student = student::join('j_h_students', 'j_h_students.studentID', '=',
                                        'students.id')
                                    ->select('students.*')
                                    ->whereDate("students.created_at", '<=', $systemdate[0]->systemDate)
                                    ->where('j_h_students.schoolYearID', '=', $schoolyear)
                                    ->where('j_h_students.gradeLevel', '=', $coursegrade)
                                    ->get();
                }else{
                    $student = student::join('j_h_students', 'j_h_students.studentID', '=',
                                            'students.id')
                                        ->select('students.*')
                                        ->whereDate("students.created_at", '<=', $systemdate[0]->systemDate)->get();
                }
        
            break;
            case 'elementary':
                if($schoolyear == 'all' && $coursegrade == 'all'){
                    $student = student::join('elem_students', 'elem_students.studentID', '=',
                                            'students.id')
                                        ->select('students.*')
                                        ->whereDate("students.created_at", '<=', $systemdate[0]->systemDate)->get();
                }elseif ($schoolyear == 'all' && $coursegrade != 'all') {
                    $student = student::join('elem_students', 'elem_students.studentID', '=',
                                        'students.id')
                                    ->select('students.*')
                                    ->whereDate("students.created_at", '<=', $systemdate[0]->systemDate)
                                    ->where('elem_students.gradeLevel', '=', $coursegrade)
                                    ->get();
                }elseif ($schoolyear != 'all' && $coursegrade == 'all') {
                    $student = student::join('elem_students', 'elem_students.studentID', '=',
                                        'students.id')
                                    ->select('students.*')
                                    ->whereDate("students.created_at", '<=', $systemdate[0]->systemDate)
                                    ->where('elem_students.schoolYearID', '=', $schoolyear)
                                    ->get();
                }elseif ($schoolyear != 'all' && $coursegrade != 'all') {
                    $student = student::join('elem_students', 'elem_students.studentID', '=',
                                        'students.id')
                                    ->select('students.*')
                                    ->whereDate("students.created_at", '<=', $systemdate[0]->systemDate)
                                    ->where('elem_students.schoolYearID', '=', $schoolyear)
                                    ->where('elem_students.gradeLevel', '=', $coursegrade)
                                    ->get();
                }else{
                    $student = student::join('elem_students', 'elem_students.studentID', '=',
                                            'students.id')
                                        ->select('students.*')
                                        ->whereDate("students.created_at", '<=', $systemdate[0]->systemDate)->get();
                }
    
            break;
        }
       
        if(count($student)==0){
            return "<h2>There are no students found</h2>";
        }
        $html = '';
        $this->rowCounter =0;
        foreach ($student as $students) {
            $this->rowCounter++;
            $html .=$this->getStudentRow($students);
        }
        return $html;
    }
    public function getStudentsFaster($viewby, $schoolyear, $semester, $coursegrade, $gradelevel, $upgrade, $search=''){
        $user = new userController();
        $systemdate = systemDate::all();
        $sql=''; 
        if(session()->get('user')['userType']=='admin'){
            $sql = "SELECT COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) 
                    semesterID, COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) 
                    schoolYearID, COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) studentType, 
                    COALESCE(col.yearLevel, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) courseGrade, 
                    COALESCE(col.studentNo, shs.studNo, jhs.studNo, elem.studNo) studentNo,
                    COALESCE(col.courseID, shs.seniorHighStrandID) strandCourse,
                    students.*,
                    col.courseName, col.yearLevel,
                    shs.strandName
                    FROM students LEFT JOIN (SELECT college_students.studentID, college_students.yearLevel, college_students.semesterID, college_students.schoolYearID, college_students.id, 'college' AS studentType, 
                    college_students.courseID, courses.courseName, college_students.studentNo FROM college_students INNER JOIN (SELECT max(id) as maxid FROM college_students GROUP 
                    BY studentID) cs ON cs.maxid = college_students.id
                    INNER JOIN courses ON courses.id = college_students.courseID) col ON col.studentID = students.id LEFT 
                    JOIN (SELECT s_h_s_students.studentID, s_h_s_students.semesterID, s_h_s_students.schoolYearID, s_h_s_students.id, 'seniorhigh' AS studentType, s_h_s_students.gradeLevel, s_h_s_students.studNo,  senior_high_strands.strandName, s_h_s_students.seniorHighStrandID
                    FROM s_h_s_students INNER JOIN (SELECT max(id) as maxid FROM s_h_s_students GROUP BY studentID) 
                    shs ON shs.maxid = s_h_s_students.id INNER JOIN senior_high_strands ON senior_high_strands.id = s_h_s_students.seniorHighStrandID) shs ON shs.studentID = students.id
                    LEFT JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'juniorhigh' AS 
                    studentType, gradeLevel, studNo  FROM j_h_students INNER JOIN (SELECT max(id) as maxid FROM j_h_students 
                    GROUP BY studentID) jhs ON jhs.maxid = j_h_students.id) jhs ON jhs.studentID = students.id LEFT 
                    JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'elementary' AS studentType, 
                    gradeLevel, studNo FROM elem_students INNER JOIN (SELECT max(id) as maxid FROM elem_students GROUP 
                    BY studentID) elem ON elem.maxid = elem_students.id) elem ON elem.studentID = students.id
                    
                    LEFT JOIN school_b_gs ON school_b_gs.studentID = students.id
                    LEFT JOIN 
                    ( SELECT fav_subject_lists.studentID, fav_subject_lists.subjectID, subj.subjectName 
                    FROM fav_subject_lists INNER JOIN subjects subj ON subj.id = fav_subject_lists.subjectID) 
                    favsubject ON favsubject.studentID =students.id
                    LEFT JOIN 
                    (SELECT disliked_subjects.studentID, disliked_subjects.subjectID, subj1.subjectName 
                    FROM disliked_subjects INNER JOIN subjects subj1 ON subj1.id = disliked_subjects.subjectID)
                    dislikedsubject ON dislikedsubject.studentID =students.id
                    LEFT JOIN  
                    (SELECT make_up_assistances.studentID, make_up_assistances.personalityID, personalities.name 
                    FROM make_up_assistances INNER JOIN personalities ON  make_up_assistances.personalityID = personalities.id) 
                    makeup ON makeup.studentID = students.id
            WHERE (students.firstName LIKE '%$search%' OR students.lastName LIKE '%$search%' OR 
                    school_b_gs.schoolName LIKE '%$search%' OR favsubject.subjectName LIKE '%$search%' 
                    OR dislikedsubject.subjectName LIKE '%$search%'
                          OR makeup.name LIKE '%$search%'
                          OR students.gender = '$search'
                          OR shs.strandName LIKE '%$search%') 
            AND DATE(students.created_at) <= '".$systemdate[0]->systemDate."'";
            switch ($viewby) {
                case 'college':
                    $sql .= " AND COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) = 'college'";
                break;
                case 'seniorhigh':
                    $sql .= " AND COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) = 'seniorhigh'";
                break;
                case 'juniorhigh':
                    $sql .= " AND COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) = 'juniorhigh'";
                break;
                case 'elementary':
                    $sql .= " AND COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) = 'elementary'";
                break;
                default:
                   
                break;
            }
            if($schoolyear!="all"){
                $sql .= " AND COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) = '$schoolyear'";
            }
            if($semester!="all"){
                $sql .= " AND COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) = '$semester'";
            }
            if($gradelevel!="all"){
                $sql .= " AND COALESCE(col.yearLevel, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) = '$gradelevel'";
            }
            if($coursegrade != 'all'){
                $sql .= " AND COALESCE(col.courseID, shs.seniorHighStrandID) = '$coursegrade'";
            }
        }else{
            $designation = $user->getUserDesignation(Auth::guard('myuser')->id());
            $sql = "SELECT COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) 
                    semesterID, COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) 
                    schoolYearID, COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) studentType, 
                    COALESCE(col.yearLevel, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) courseGrade, 
                    COALESCE(col.studentNo, shs.studNo, jhs.studNo, elem.studNo) studentNo,
                    COALESCE(col.courseID, shs.seniorHighStrandID) strandCourse,
                    students.*,
                    col.courseName, col.yearLevel,
                    shs.strandName
                    FROM students LEFT JOIN (SELECT college_students.studentID, college_students.yearLevel, college_students.semesterID, college_students.schoolYearID, college_students.id, 'college' AS studentType, 
                    college_students.courseID, courses.courseName, college_students.studentNo FROM college_students INNER JOIN (SELECT max(id) as maxid FROM college_students GROUP 
                    BY studentID) cs ON cs.maxid = college_students.id
                    INNER JOIN courses ON courses.id = college_students.courseID) col ON col.studentID = students.id LEFT 
                    JOIN (SELECT s_h_s_students.studentID, s_h_s_students.semesterID, s_h_s_students.schoolYearID, s_h_s_students.id, 'seniorhigh' AS studentType, s_h_s_students.gradeLevel, s_h_s_students.studNo,  senior_high_strands.strandName, s_h_s_students.seniorHighStrandID
                    FROM s_h_s_students INNER JOIN (SELECT max(id) as maxid FROM s_h_s_students GROUP BY studentID) 
                    shs ON shs.maxid = s_h_s_students.id INNER JOIN senior_high_strands ON senior_high_strands.id = s_h_s_students.seniorHighStrandID) shs ON shs.studentID = students.id
                    LEFT JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'juniorhigh' AS 
                    studentType, gradeLevel, studNo  FROM j_h_students INNER JOIN (SELECT max(id) as maxid FROM j_h_students 
                    GROUP BY studentID) jhs ON jhs.maxid = j_h_students.id) jhs ON jhs.studentID = students.id LEFT 
                    JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'elementary' AS studentType, 
                    gradeLevel, studNo FROM elem_students INNER JOIN (SELECT max(id) as maxid FROM elem_students GROUP 
                    BY studentID) elem ON elem.maxid = elem_students.id) elem ON elem.studentID = students.id
                    
                    LEFT JOIN school_b_gs ON school_b_gs.studentID = students.id
                    LEFT JOIN 
                    ( SELECT fav_subject_lists.studentID, fav_subject_lists.subjectID, subj.subjectName 
                    FROM fav_subject_lists INNER JOIN subjects subj ON subj.id = fav_subject_lists.subjectID) 
                    favsubject ON favsubject.studentID =students.id
                    LEFT JOIN 
                    (SELECT disliked_subjects.studentID, disliked_subjects.subjectID, subj1.subjectName 
                    FROM disliked_subjects INNER JOIN subjects subj1 ON subj1.id = disliked_subjects.subjectID)
                    dislikedsubject ON dislikedsubject.studentID =students.id
                    LEFT JOIN  
                    (SELECT make_up_assistances.studentID, make_up_assistances.personalityID, personalities.name 
                    FROM make_up_assistances INNER JOIN personalities ON  make_up_assistances.personalityID = personalities.id) 
                    makeup ON makeup.studentID = students.id
                    WHERE (students.firstName LIKE '%$search%' OR students.lastName LIKE '%$search%' OR 
                    school_b_gs.schoolName LIKE '%$search%' OR favsubject.subjectName LIKE '%$search%' 
                    OR dislikedsubject.subjectName LIKE '%$search%'
                          OR makeup.name LIKE '%$search%'
                          OR students.gender = '$search') 
                    AND DATE(students.created_at) <= '".$systemdate[0]->systemDate."'";
            switch($designation[0]){
                case 'college':
                    $sql= "SELECT cs2.courseID AS courseGrade, c2.courseName, cs2.schoolYearID, cs2.studentNo, 
                    cs2.yearLevel, students.*, 'college' AS studentType, cs2.semesterID FROM students 
                    INNER JOIN (SELECT studentID, studentNo,
                    id, courseID, schoolYearID, yearLevel, semesterID FROM college_students  INNER JOIN (SELECT max(id) as maxid 
                    FROM college_students 
                    GROUP BY studentID) cs ON cs.maxid = college_students.id) cs2 ON cs2.studentID = students.id 
                    INNER JOIN (SELECT c.id AS courseID, dep.departmentID, c.courseName FROM courses c INNER JOIN (SELECT
                    id, courseID FROM course_department_hists cd INNER JOIN (SELECT max(id) as maxid FROM 
                    course_department_hists GROUP BY courseID) cd2 ON cd2.maxid = cd.id) cdh ON cdh.courseID = c.id
                    INNER JOIN (SELECT departmentID, id FROM course_department_hists INNER JOIN (SELECT
                    max(id) AS maxid FROM course_department_hists GROUP BY courseID) cdh1 ON cdh1.maxid = 
                    course_department_hists.id) dep ON dep.id =cdh.id) c2 ON c2.courseID = cs2.courseID 
                    INNER JOIN (SELECT departmentID, id, collegeID FROM department_college_hists
                    INNER JOIN (SELECT max(id) as maxid FROM department_college_hists GROUP BY departmentID) 
                    depc ON depc.maxid = department_college_hists.id) depc2 ON depc2.departmentID=c2.departmentID 
                    INNER JOIN (SELECT collegeID, userID, id FROM assignment_colleges INNER JOIN (SELECT max(id) 
                    as maxid FROM assignment_colleges GROUP BY userID) assc ON assc.maxid = assignment_colleges.id) 
                    assc2 ON assc2.collegeID = depc2.collegeID 

                    LEFT JOIN school_b_gs ON school_b_gs.studentID = students.id
                    LEFT JOIN 
                    ( SELECT fav_subject_lists.studentID, fav_subject_lists.subjectID, subj.subjectName 
                    FROM fav_subject_lists INNER JOIN subjects subj ON subj.id = fav_subject_lists.subjectID) 
                    favsubject ON favsubject.studentID =students.id
                    LEFT JOIN 
                    (SELECT disliked_subjects.studentID, disliked_subjects.subjectID, subj1.subjectName 
                    FROM disliked_subjects INNER JOIN subjects subj1 ON subj1.id = disliked_subjects.subjectID)
                     dislikedsubject ON dislikedsubject.studentID =students.id
                    LEFT JOIN  
                    (SELECT make_up_assistances.studentID, make_up_assistances.personalityID, personalities.name 
                    FROM make_up_assistances INNER JOIN personalities ON  make_up_assistances.personalityID = personalities.id) 
                    makeup ON makeup.studentID = students.id

                    WHERE assc2.userID ='".Auth::guard('myuser')->id()."'
                    AND DATE(students.created_at) <= '".$systemdate[0]->systemDate."'
                    AND (students.firstName LIKE '%$search%' OR students.lastName LIKE '%$search%' OR 
                    school_b_gs.schoolName LIKE '%$search%' OR favsubject.subjectName LIKE '%$search%' 
                    OR dislikedsubject.subjectName LIKE '%$search%'
                          OR makeup.name LIKE '%$search%'
                          OR students.gender = '$search'
                          OR shs.strandName LIKE '%$search%') ";
                    if($schoolyear != "all"){
                        $sql .= " AND cs2.schoolYearID = '$schoolyear'";
                    }
                    if($semester != "all"){
                        $sql .= " AND cs2.semesterID = '$semester'";
                    }
                    if($coursegrade != "all"){
                        $sql .= " AND cs2.courseID = '$coursegrade'";
                    }
                    if($gradelevel != 'all'){
                        $sql .= " AND  cs2.yearLevel = '$gradelevel'";
                    }
                break;
                case 'seniorhigh':
                    $sql .= " AND COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) = 'seniorhigh'
                              AND COALESCE(col.yearLevel, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) = '".$designation[1]."'";
                     if($schoolyear != "all"){
                        $sql .= " AND COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) = '$schoolyear'";
                    }
                    if($semester != "all"){
                        $sql .= " AND COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) = '$semester'";
                    }
                    
                break;
                case 'juniorhigh':
                    $sql .= " AND COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) = 'juniorhigh'
                                AND COALESCE(col.yearLevel, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) = '".$designation[1]."'";
                    if($schoolyear != "all"){
                        $sql .= " AND COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) = '$schoolyear'";
                    }
                    if($semester != "all"){
                        $sql .= " AND COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) = '$semester'";
                    }
                    if($gradelevel != 'all'){
                        
                    }
                break;
                case 'elementary':
                    $sql .= " AND COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) = 'elementary'
                                AND COALESCE(col.yearLevel, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) = '".$designation[1]."'";
                    if($schoolyear != "all"){
                        $sql .= " AND COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) = '$schoolyear'";
                    }
                    if($semester != "all"){
                        $sql .= " AND COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) = '$semester'";
                    }
                   
                break;
            } 
        }
        $sql .= "GROUP BY students.id ORDER BY students.lastName ASC";
        $students = DB::select(DB::raw($sql));
        if(count($students)==0){
            return "<h2>There are no students found.</h2>";
        }else{
            $html = '';
            $this->rowCounter=0;
            foreach ($students as $student) {
                $this->rowCounter++;
                if($upgrade!='true'){
                    $html .= $this->getStudentRowFaster($student);
                }else{
                   
                    $html .= $this->getStudentUpgradeRowFaster($student);
                }                
            }
            return $html;
        }    
    }
    public function getStudentRowFaster($student){
        $course = ''; 
        if($student->courseName !=''){
            $course= $student->courseName;
        }else{
            $course= $student->strandName;
        }
        $grade = '';
        if($student->yearLevel != ''){
            
            $grade =$student->yearLevel;
        }else{
            $grade = $student->courseGrade;
        }
        $middlename = '';
        if($student->middleName != ''){
            $middlename = ucfirst(substr($student->middleName, 0, 1)).".";
        }
        return "<tr id  = 'student-$student->id'>
                    <td>$this->rowCounter</td>
                    <td>$student->lastName, $student->firstName $middlename</td>
                    <td>$student->studentNo</td>
                    <td>".$this->getBeautifiedStudentType($student->studentType)."</td>
                    <td>$course</td>
                    <td>$grade</td>
                    <td>
                        <button class='btn btn-warning viewProfile' id = 'viewProfile-$student->id'>
                            
                            <a href ='".route('student.profile', ['profileid'=>$student->id])."' >
                                <i class='fa fa-edit'>
                                </i>
                            <a/>
                            
                        </button>
                    </td>
        
                </tr>";
    }
    public function getStudentUpgradeRowFaster($student){
        $course = ''; 
        if($student->courseName !=''){
            $course= $student->courseName;
        }else{
            $course= $student->strandName;
        }
        $grade = '';
        if($student->yearLevel != ''){
            
            $grade =$student->yearLevel;
        }else{
            $grade = $student->courseGrade;
        }
        $middlename = '';
        if($student->middleName != ''){
            $middlename = ucfirst(substr($student->middleName, 0, 1)).".";
        }
        return "<tr id  = 'student-$student->id'>
                    <td><input type= 'checkbox' class='upgradeStudentCheckbox' id = 'checkStudentUpgrade-$student->id' value ='$student->id'></td>
                    <td>$this->rowCounter</td>
                    <td>$student->lastName, $student->firstName $middlename</td>
                    <td>$student->studentNo</td>
                    <td>".$this->getBeautifiedStudentType($student->studentType)."</td>
                    <td>$course</td>
                    <td>$grade</td>
                </tr>";
    }
    public function getStudentRow($student){
        
        $studenttype=$this->getStudentType($student->id);
        $studentno = '';
        switch($studenttype){
            case 'college':
                $studentno =$this->getStudentNo($student->id, 'college');
            break;
            case 'seniorhigh':
                $studentno =$this->getStudentNo($student->id, 'seniorhigh');
            break;
            case 'juniorhigh':
                $studentno =$this->getStudentNo($student->id, 'juniorhigh');
            break;
            case 'elementary':
                $studentno =$this->getStudentNo($student->id, 'elementary');
            break;
        }
        $course = '';
        $yearlevel = '';
        if($studenttype == 'college'){
            $temp = DB::select(DB::raw("SELECT c.courseName, cs.yearLevel FROM courses c INNER JOIN 
            college_students cs ON cs.courseID = c.id
            WHERE cs.id = (SELECT max(college_students.id) 
            FROM college_students WHERE college_students.studentID = '$student->id' )"));
            $course = $temp[0]->courseName;
            $yearlevel = $temp[0]->yearLevel;
        }else{
            switch($studenttype){
                case 'seniorhigh':
                    $temp = DB::select(DB::raw("SELECT s_h_s_students.gradeLevel, senior_high_strands.strandName FROM s_h_s_students
                     INNER JOIN senior_high_strands ON senior_high_strands.id = s_h_s_students.seniorHighStrandID
                     WHERE s_h_s_students.id 
                     = (SELECT max(id) FROM s_h_s_students WHERE studentID = '$student->id')"));
                    $yearlevel = $temp[0]->gradeLevel;
                    $course = $temp[0]->strandName;
                break;
                case 'juniorhigh':
                    $temp = DB::select(DB::raw("SELECT gradeLevel FROM j_h_students WHERE id 
                    = (SELECT max(id) FROM j_h_students WHERE studentID = '$student->id')"));
                    $yearlevel = $temp[0]->gradeLevel;
                break;
                case 'elementary':
                    $temp = DB::select(DB::raw("SELECT gradeLevel FROM elem_students WHERE id 
                    = (SELECT max(id) FROM elem_students WHERE studentID = '$student->id')"));
                    $yearlevel = $temp[0]->gradeLevel;
                break;
            }
        }
        $middlename = '';
        if($student->middleName != ''){
            $middlename = ucfirst(substr($student->middleName, 0, 1)).".";
        }
        return "<tr id  = 'student-$student->id'>
                    <td>$this->rowCounter</td>
                    <td>$student->lastName, $student->firstName $middlename</td>
                    <td>$studentno</td>
                    <td>".$this->getBeautifiedStudentType($studenttype)."</td>
                    <td>$course</td>
                    <td>$yearlevel</td>
                    <td>
                        <button class='btn btn-warning viewProfile' id = 'viewProfile-$student->id'>
                            
                            <a href ='".route('student.profile', ['profileid'=>$student->id])."' >
                                <i class='fa fa-edit'>
                                </i>
                            <a/>
                            
                        </button>
                    </td>
                    
                </tr>";
    }
    public function getBeautifiedStudentType($studenttype){
        switch ($studenttype) {
            case 'college':
                return "College";
            break;
            case 'seniorhigh':
                return "Senior High";
            break;
            case 'juniorhigh':
                return "Junior High";
            break;
            case 'elementary':
                return "Elementary";
            break;
            default:
                # code...
                break;
        }
    }
    public function getStudentType($studentid){
        $student = collegeStudent::where('studentID', '=', $studentid)->get();
        if(count($student)){
            return 'college';
        }
        $student = SHSStudent::where('studentID', '=', $studentid)->get();
        if(count($student)){
            return 'seniorhigh';
        }
        $student = JHStudent::where('studentID', '=', $studentid)->get();
        if(count($student)){
            return 'juniorhigh';
        }
        $student = elemStudent::where('studentID', '=', $studentid)->get();
        if(count($student)){
            return 'elementary';
        }
    }
    public function getStudentNo($studentid, $studenttype){
        switch($studenttype){
            case 'college':
                $student = collegeStudent::where('studentID', '=', $studentid)->get();
                return $student[0]->studentNo;
            break;
            case 'seniorhigh':
                $student = SHSStudent::where('studentID', '=', $studentid)->get();
                //error_log($student);
                return $student[0]->studNo;
            break;
            case 'juniorhigh':
                $student = JHStudent::where('studentID', '=', $studentid)->get();
                return $student[0]->studNo;
            break;
            case 'elementary':
                $student = elemStudent::where('studentID', '=', $studentid)->get();
                return $student[0]->studNo;
            break;
        }
    }
    public function getStudentNo2($studentid, $studenttype){
        switch($studenttype){
            case 'college':
                $temp = DB::select(DB::raw("SELECT studentNo FROM college_students WHERE id 
                                = (SELECT max(id) FROM college_students WHERE studentID = '$studentid')"));
                
                return $temp[0]->studentNo;
            break; 
            case 'seniorhigh':
                $temp = DB::select(DB::raw("SELECT studNo FROM s_h_s_students WHERE id 
                     = (SELECT max(id) FROM s_h_s_students WHERE studentID = '$studentid')"));
                return $temp[0]->studNo;
            break;
            case 'juniorhigh':
                $temp = DB::select(DB::raw("SELECT studNo FROM j_h_students WHERE id 
                        = (SELECT max(id) FROM j_h_students WHERE studentID = '$studentid')"));
                return $temp[0]->studNo;
            break;
            case 'elementary':
                $temp = DB::select(DB::raw("SELECT studNo FROM elem_students WHERE id 
                        = (SELECT max(id) FROM elem_students WHERE studentID = '$studentid')"));
                return $temp[0]->studNo;
            break;
        }
    }
    public function getNoOrganizationID(){
        $org = organization::where('organizationName', '=', 'no organization')->get();
        if(count($org)>0){
            return $org[0]->id;
        }else{
            return 0;
        }
    }
    public function studentInsert(Request $request){
        
        $this->validate($request, array(
            'studentTypeComboModal'=>'required|max:100',
            'yearLevelComboModal'=>'required',
            'dobTextModal'=>'required',
            'nationalityTextModal'=>'required|max:100',
            'lastNameTextModal'=>'required|max:100',
            'firstNameTextModal'=>'required|max:100',
            'studentImage' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'genderRadioButton' => 'required',
            'studentNoTextModal' => ['required', new studentNoRule],
            'dateEnrolledText' =>['required', new SystemTimeRule]
        ));
        if($this->getCurrentSchoolYear()==0){
            return "There is no activated school year yet!";
        }
        
        //validate studentid
        $this->request = $request;
        
        DB::transaction(function(){
           
            $student =  new student;
            $student->firstName = $this->request->firstNameTextModal;
            $student->middleName = $this->request->middleNameTextModal;
            $student->lastName = $this->request->lastNameTextModal;
            $student->dateOfBirth = $this->request->dobTextModal;
            $student->gender = $this->request->genderRadioButton;
            $student->status = $this->request->maritalStatusComboModal;
            $student->nationality = $this->request->nationalityTextModal;
            $student->religion = $this->request->religionTextModal;
            $student->ethnicity = $this->request->ethnicityTextModal;
            $student->language = $this->request->languageSpokenTextModal;
            $student->contactNo = $this->request->contactNoTextModal;
            $student->cityAddress = $this->request->cityAddressTextModal;
            $student->provincialAddress = $this->request->provincialAddressTextModal;
            
            if($this->request->studentTypeComboModal == 'college'){
                $course = $this->getCourses($this->request->collegeComboModal);
                if($course =='nocourses'){
                    $this->html= "There is no assigned courses in this college!";
                    return;
                }
                $collegestudent  = collegeStudent::where('studentNo', '=', $this->request->studentNoTextModal)->get();
                if(count($collegestudent)>0){
                    $this->html= "student number already exists!";
                    return;
                }
                $image = $this->request->file('studentImage');
                $student->imageType = $image->getClientOriginalExtension();
                $student->save();
                activityController::insertActivity('Inserted new student with student id:'. $student->id);
                $filename = $student->id.'.'.$image->getClientOriginalExtension();
                $location = public_path("images-database/students/". $filename);
                Image::make($image)->resize(200,200)->save($location);
                $collegesave = new collegeStudent;
                $collegesave->studentID = $student->id;
                $collegesave->studentNo = $this->request->studentNoTextModal;
                $collegesave->yearLevel = $this->request->yearLevelComboModal;
                $collegesave->dateEnrolled = $this->request->dateEnrolledText;
                $collegesave->semesterID = $this->getCurrentSem();
                $collegesave->schoolYearID = $this->getCurrentSchoolYear();
                $collegesave->organizationID = $this->getNoOrganizationID();
                $collegesave->courseID = $this->request->courseComboModal;
                $collegesave->save();
                
            }elseif ($this->request->studentTypeComboModal == 'seniorhigh') {
                if($this->request->seniorHighStrand==''){
                    $this->html= "Senior high strand is required for a senior high student!";
                    return;
                }
                $studno  = SHSStudent::where('studNo', '=', $this->request->studentNoTextModal)->get();
                if(count($studno)>0){
                    $this->html= "student number already exists!";
                    return;
                }
                $image = $this->request->file('studentImage');
                $student->imageType = $image->getClientOriginalExtension();
                $student->save();
                activityController::insertActivity('Inserted new student with student id:'. $student->id);
                $filename = $student->id.'.'.$image->getClientOriginalExtension();
                $location = public_path("images-database/students/". $filename);
                Image::make($image)->resize(200,200)->save($location);
                $seniorstudent = new SHSStudent;
                $seniorstudent->studentID = $student->id;
                $seniorstudent->studNo = $this->request->studentNoTextModal;
                $seniorstudent->schoolYearID =$this->getCurrentSchoolYear();
                $seniorstudent->semesterID =$this->getCurrentSem();
                $seniorstudent->seniorHighStrandID =$this->request->seniorHighStrand;
                $seniorstudent->gradeLevel = $this->request->yearLevelComboModal;
                $seniorstudent->dateEnrolled = $this->request->dateEnrolledText;
                $seniorstudent->save();
            }elseif ($this->request->studentTypeComboModal == 'juniorhigh') {
                
                $juniorhigh  = JHStudent::where('studNo', '=', $this->request->studentNoTextModal)->get();
                if(count($juniorhigh)>0){
                    $this->html= "student number already exists!";
                    return;
                }
                $image = $this->request->file('studentImage');
                $student->imageType = $image->getClientOriginalExtension();
                $student->save();
                activityController::insertActivity('Inserted new student with student id:'. $student->id);
                $filename = $student->id.'.'.$image->getClientOriginalExtension();
                $location = public_path("images-database/students/". $filename);
                Image::make($image)->resize(200,200)->save($location);
                $juniorhighstudent  =  new JHStudent;
                $juniorhighstudent->studentID =$student->id;
                $juniorhighstudent->studNO =$this->request->studentNoTextModal;
                $juniorhighstudent->schoolYearID =$this->getCurrentSchoolYear();
                $juniorhighstudent->gradeLevel =  $this->request->yearLevelComboModal;
                $juniorhighstudent->dateEnrolled =$this->request->dateEnrolledText;
                $juniorhighstudent->save();
            }elseif ($this->request->studentTypeComboModal == 'elementary') {
                $elementary  = elemStudent::where('studNo', '=', $this->request->studentNoTextModal)->get();
                if(count($elementary)>0){
                    $this->html= "student number already exists!";
                    return;
                }
                $image = $this->request->file('studentImage');
                $student->imageType = $image->getClientOriginalExtension();
                $student->save();
                activityController::insertActivity('Inserted new student with student id:'. $student->id);
                $filename = $student->id.'.'.$image->getClientOriginalExtension();
                $location = public_path("images-database/students/". $filename);
                Image::make($image)->resize(200,200)->save($location);
                $elementarystudent = new elemStudent;
                $elementarystudent->studentID =$student->id;
                $elementarystudent->studNO =$this->request->studentNoTextModal;
                $elementarystudent->schoolYearID =$this->getCurrentSchoolYear();
                $elementarystudent->dateEnrolled =$this->request->dateEnrolledText;
                $elementarystudent->gradeLevel =$this->request->yearLevelComboModal;
                $elementarystudent->save();
            }
            $this->student = $student;
            $this->rowCounter = $this->request->rowCounter;
            $this->html = $this->getStudentRow($student);
             
        });
        
        return $this->html;
    }
    public function getPersonalities(){
        $personality = personality::all()->groupBy('name');
        return $personality;
    } 
    public function getCurrentSchoolYear(){
        $schoolyear = schoolYear::where('status', '=', 1)->get();
        if(count($schoolyear)>0){
            return $schoolyear[0]->id;
        }else{
            return 0;
        }
    }
    public function getActiveSchoolYearSemester(){
        return $this->getCurrentSchoolYear().".".$this->getCurrentSem();
    }
    public function getStudentLeisure($studentType, $question){
        switch ($studentType) {
            case 'college':
                switch ($variable) {
                    case 'howdidyoumakethischoice':
                        $studentleisure = studentLeisureCollege::all()->groupBy('howDidYouMakeThisChoice');
                        return $studentleisure;
                    break;
                    case 'ifchoicewasnotyourown':
                        $studentleisure = studentLeisureCollege::all()->groupBy('courseChoice');
                        return $studentleisure;
                    break;
                    case 'howdidyoucometothisschool':
                        $studentleisure = studentLeisureCollege::all()->groupBy('howDidYouComeToThisSchool');
                        return $studentleisure;
                    break;
                    case 'howmuchinforequirements':
                        $studentleisure = studentLeisureCollege::all()->groupBy('getInfo');
                        return $studentleisure;
                    break;
                    case 'wheredidyougetthisinfo':
                        $studentleisure = studentLeisureCollege::all()->groupBy('whereDidYouGetThisInfo');
                        return $studentleisure;
                    break;
                    case 'financialsupport':
                        $studentleisure = studentLeisureCollege::all()->groupBy('financialSupport');
                        return $studentleisure;
                    break;
                    case 'scholarship':
                        $studentleisure = studentLeisureCollege::all()->groupBy('scholarship');
                        return $studentleisure;
                    break;
                    default:
                        # code...
                    break;
                }    
            break;
            case 'seniorhigh':
                switch ($variable) {
                    case 'howdidyoumakethischoice':
                        $studentleisure = studentLeisureSH::all()->groupBy('selfEval1');
                        return $studentleisure;
                    break;
                    case 'ifchoicewasnotyourown':
                        $studentleisure = studentLeisureSH::all()->groupBy('selfEval2');
                        return $studentleisure;
                    break;
                    case 'howdidyoucometothisschool':
                        $studentleisure = studentLeisureSH::all()->groupBy('selfEval3');
                        return $studentleisure;
                    break;
                    case 'howmuchinforequirements':
                        $studentleisure = studentLeisureSH::all()->groupBy('selfEval4');
                        return $studentleisure;
                    break;
                    case 'wheredidyougetthisinfo':
                        $studentleisure = studentLeisureSH::all()->groupBy('selfEval5');
                        return $studentleisure;
                    break;
                    case 'scholasticstanding':
                        $studentleisure = studentLeisureSH::all()->groupBy('selfEval6');
                        return $studentleisure;
                    break;
                    default:
                        # code...
                    break;
                }    
            break;
            case 'junorhigh':
                switch ($variable) {
                    case 'howdidyoumakethischoice':
                        $studentleisure = studentLeisureJH::all()->groupBy('selfEval1');
                        return $studentleisure;
                    break;
                    case 'ifchoicewasnotyourown':
                        $studentleisure = studentLeisureJH::all()->groupBy('selfEval2');
                        return $studentleisure;
                    break;
                    case 'howdidyoucometothisschool':
                        $studentleisure = studentLeisureJH::all()->groupBy('selfEval3');
                        return $studentleisure;
                    break;
                    case 'howmuchinforequirements':
                        $studentleisure = studentLeisureJH::all()->groupBy('selfEval4');
                        return $studentleisure;
                    break;
                    case 'wheredidyougetthisinfo':
                        $studentleisure = studentLeisureJH::all()->groupBy('selfEval5');
                        return $studentleisure;
                    break;
                    case 'scholasticstanding':
                        $studentleisure = studentLeisureJH::all()->groupBy('selfEval6');
                        return $studentleisure;
                    break;
                    default:
                        # code...
                    break;
                }    
            break;
            case 'elementary':
                switch ($variable) {
                    case 'howdidyoumakethischoice':
                        $studentleisure = studentLeisureElem::all()->groupBy('selfEval1');
                        return $studentleisure;
                    break;
                    case 'ifchoicewasnotyourown':
                        $studentleisure = studentLeisureElem::all()->groupBy('selfEval2');
                        return $studentleisure;
                    break;
                    case 'howdidyoucometothisschool':
                        $studentleisure = studentLeisureElem::all()->groupBy('selfEval3');
                        return $studentleisure;
                    break;
                    case 'howmuchinforequirements':
                        $studentleisure = studentLeisureElem::all()->groupBy('selfEval4');
                        return $studentleisure;
                    break;
                    case 'wheredidyougetthisinfo':
                        $studentleisure = studentLeisureElem::all()->groupBy('selfEval5');
                        return $studentleisure;
                    break;
                    case 'scholasticstanding':
                        $studentleisure = studentLeisureElem::all()->groupBy('selfEval6');
                        return $studentleisure;
                    break;
                    default:
                        # code...
                    break;
                }    
            break;
            default:
                # code...
                break;
        }
    }
    public function getSeniorHighStrands(){
        $strand = seniorHighStrand::all();
        /*if(count($strand)>0){
            $html = '';
            foreach ($strand as $strands) {
                $html .="<option value ='$strands->id'>$strand->strandName</option>";
            }
            return $html;
        }else{
            return '';
        }*/
        return $strand;
    }
    public function getStudentsFiltered($viewby){
        $systemdate = systemDate::all();
        switch ($viewby) {
            case 'college':
                $students = student::select('students.*')
                                    ->join('college_students', 'students.id', 
                                    '=', 'college_students.studentID')
                                    ->whereDate('students.created_at', '<=', $systemdate[0]->systemDate)
                                    ->get();
                if(count($students)==0){
                    return "<h2>There are no college students yet</h2>";
                }else{
                    $html = '';
                    foreach ($students as $students) {
                        $html .=$this->getStudentRow($students);
                    }
                    return $html;
                }
            break;
            case 'seniorhigh':
                $students = student::select('students.*')
                            ->join('s_h_s_students', 'students.id',
                             '=', 's_h_s_students.studentID')
                             ->whereDate('students.created_at', '<=', $systemdate[0]->systemDate)
                             ->get();
                if(count($students)==0){
                    return "<h2>There are no senior high students yet</h2>";
                }else{
                    $html = '';
                    foreach ($students as $students) {
                        $html .=$this->getStudentRow($students);
                    }
                    return $html;
                }
            break;
            case 'juniorhigh':
                $students = student::select('students.*')
                                    ->join('j_h_students', 
                                    'students.id', '=', 
                                    'j_h_students.studentID')
                                    ->whereDate('students.created_at', '<=', $systemdate[0]->systemDate)
                                    ->get();
                if(count($students)==0){
                    return "<h2>There are no junior high students yet</h2>";
                }else{
                    $html = '';
                    foreach ($students as $students) {
                        $html .=$this->getStudentRow($students);
                    }
                    return $html;
                }
            break;
            case 'elementary':
                $students = student::select('students.*')
                                    ->join('elem_students', 'students.id', 
                                    '=', 'elem_students.studentID')
                                    ->whereDate('students.created_at', '<=', $systemdate[0]->systemDate)
                                    ->get();
                if(count($students)==0){
                    return "<h2>There are no elementary students yet</h2>";
                }else{
                    $html = '';
                    foreach ($students as $students) {
                        $html .=$this->getStudentRow($students);
                    }
                    return $html;
                }
            break;
            default:
                # code...
                break;
        }
    }
    public function getStudentsSearch($viewby, $search =''){
        switch ($viewby) {
            case 'all':
                $students = student::select('students.*')
                ->where('students.firstName', 'LIKE', "%$search%")
                ->orWhere('students.lastName', 'LIKE', "%$search%")
                ->orWhere('students.lastName', 'LIKE', "%$search%")
                ->get();
                if(count($students)==0){
                return "<h2>There are no students found</h2>";
                }else{
                $html = '';
                foreach ($students as $students) {
                $html .=$this->getStudentRow($students);
                }
                return $html;
                }
            break;
            case 'college':
                $students = student::select('students.*')->join('college_students', 'students.id', '=', 'college_students.studentID')
                                    ->where('students.firstName', 'LIKE', "%$search%")
                                    ->orWhere('students.lastName', 'LIKE', "%$search%")
                                    ->orWhere('students.lastName', 'LIKE', "%$search%")
                                    ->orWhere('college_students.studentNO', 'LIKE', "%$search%")
                                    ->get();
                if(count($students)==0){
                    return "<h2>There are no college students found</h2>";
                }else{
                    $html = '';
                    foreach ($students as $students) {
                        $html .=$this->getStudentRow($students);
                    }
                    return $html;
                }
            break;
            case 'seniorhigh':
                $students = student::select('students.*')->join('s_h_s_students', 'students.id', '=', 's_h_s_students.studentID')
                ->where('students.firstName', 'LIKE', "%$search%")
                ->orWhere('students.lastName', 'LIKE', "%$search%")
                ->orWhere('students.lastName', 'LIKE', "%$search%")
                ->orWhere('s_h_s_students.studNO', 'LIKE', "%$search%")
                ->get();
                if(count($students)==0){
                    return "<h2>There are no senior high students found</h2>";
                }else{
                    $html = '';
                    foreach ($students as $students) {
                        $html .=$this->getStudentRow($students);
                    }
                    return $html;
                }
            break;
            case 'juniorhigh':
                $students = student::select('students.*')->join('j_h_students', 'students.id', '=', 'j_h_students.studentID')
                ->where('students.firstName', 'LIKE', "%$search%")
                ->orWhere('students.lastName', 'LIKE', "%$search%")
                ->orWhere('students.lastName', 'LIKE', "%$search%")
                ->orWhere('j_h_students.studNO', 'LIKE', "%$search%")
                ->get();
                if(count($students)==0){
                    return "<h2>There are no junior high students found</h2>";
                }else{
                    $html = '';
                    foreach ($students as $students) {
                        $html .=$this->getStudentRow($students);
                    }
                    return $html;
                }
            break;
            case 'elementary':
                $students = student::select('students.*')->join('elem_students', 'students.id', '=', 'elem_students.studentID')
                ->where('students.firstName', 'LIKE', "%$search%")
                ->orWhere('students.lastName', 'LIKE', "%$search%")
                ->orWhere('students.lastName', 'LIKE', "%$search%")
                ->orWhere('elem_students.studNO', 'LIKE', "%$search%")
                ->get();
                if(count($students)==0){
                    return "<h2>There are no elementary students found</h2>";
                }else{
                    $html = '';
                    foreach ($students as $students) {
                        $html .=$this->getStudentRow($students);
                    }
                    return $html;
                }
            break;
            default:
                # code...
                break;
        }
    }
    public function getStudentProfile($studentid){
        $colleges = college::all();
        $schoolyear = schoolYear::where('status', '=', 1)->get();
        $thisyear = '';
        //error_log($schoolyear);
        if(count($schoolyear)>0){
            $temp = explode('-', $schoolyear[0]->schoolYearStart);
            $thisyear = $temp[0];
            $temp = explode('-', $schoolyear[0]->schoolYearEnd);
            $thisyear .=' - '.$temp[0];
        }
        $student =  student::find($studentid);
        $subjects = subject::all();
        $user =  (new userController)->getUserDesignation(Auth::guard('myuser')->id());
        $college ='';
        if($user[0]=='college'){
            $college = DB::select(DB::raw("SELECT colleges.collegeName, courses.courseName, 
            courses.id FROM colleges INNER JOIN (SELECT collegeID, userID, id FROM 
            assignment_colleges INNER JOIN (SELECT max(id) AS maxid FROM assignment_colleges 
            GROUP BY userID) ac ON ac.maxid = assignment_colleges.id) ac2 ON ac2.collegeID = 
            colleges.id INNER JOIN (SELECT collegeID, departmentID, id FROM 
            department_college_hists INNER JOIN (SELECT max(id) as maxid FROM 
            department_college_hists GROUP BY departmentID) dch ON dch.maxid = 
            department_college_hists.id) dch2 ON dch2.collegeID = colleges.id 
            INNER JOIN (SELECT courseID, departmentID, id FROM course_department_hists 
            INNER JOIN (SELECT max(id) as maxid FROM course_department_hists GROUP BY courseID) 
            cdh ON cdh.maxid = course_department_hists.id) cdh2 ON cdh2.departmentID = 
            dch2.departmentID INNER JOIN courses on courses.id  = cdh2.courseID WHERE 
            ac2.collegeID = (SELECT collegeID FROM assignment_colleges WHERE id = 
            (SELECT max(id) FROM assignment_colleges WHERE userID ='".Auth::guard('myuser')->id()."'))
            GROUP BY courses.id"));
        }
        $notif = (new notificationController)->getNotifs();
        $collegeOfNotAdmin = DB::select(DB::raw("SELECT colleges.collegeName, colleges.id FROM colleges 
        INNER JOIN (SELECT collegeID, id, userID FROM assignment_colleges INNER 
        JOIN (SELECT MAX(id) as maxid FROM assignment_colleges GROUP BY userID) 
        ac ON ac.maxid = assignment_colleges.id) ac2 ON ac2.collegeID = colleges.id 
        WHERE ac2.userID = '".Auth::guard('myuser')->id()."'"));
         $collegeid = '';
         if(count($collegeOfNotAdmin)>0){
             $collegeid = $collegeOfNotAdmin[0]->id; 
         }
        return view('admin/studentPersonalData/studentProfile', ['currentsem' => $this->getCurrentSem(),
                                                                'college' =>$colleges,
                                                                'schoolyear'=>$thisyear,
                                                                'language' => $this->getStudentData('language'),
                                                                'religion' =>$this->getStudentData('religion'),
                                                                'ethnicity' =>$this->getStudentData('ethnicity'),
                                                                'programs'=>$this->getHealthRecords('programs'),
                                                                'ailments'=>$this->getHealthRecords('ailments'), 
                                                                'personality' => $this->getPersonalities(),
                                                                'howdidyoumakethischoice'=> $this->getPersonalities('college', 'howdidyoumakethischoice'),
                                                                'howdidyoucometothisschool' => $this->getPersonalities('college', 'howdidyoucometothisschool'),
                                                                'financialsupport'=> $this->getPersonalities('college', 'financialsupport'), 
                                                                'strands' => $this->getSeniorHighStrands(),
                                                                'studentid'=>$studentid,
                                                                'imageType' => $student->imageType,
                                                                'studentname' =>$student->firstName.' '.$student->lastName,
                                                                'cityaddress'=>$student->cityAddress,
                                                                'studenttype' =>$this->getBeautifiedStudentType($this->getStudentType($studentid)),
                                                                'lastinserted'=>date("F j, Y", strtotime($student->created_at)),
                                                                'lastupdated'=>date("F j, Y", strtotime($student->updated_at)),
                                                                'birthdayStudent'=>date("F j, Y", strtotime($student->dateOfBirth)),
                                                                'faminfo'=>$this->getStudentFamilyInfo($studentid),
                                                                'subjects'=>$subjects,
                                                                'user'=>$user,
                                                                'collegeOfUser'=>$college,
                                                                'notif'=>$notif,
                                                                'collegeOfNotAdmin'=>$collegeid]);
    }
    public function getStudentProfileArray($studentid){
        $student =  student::find($studentid);
        $student->type =$this->getStudentType($studentid);
        if($student->type =='college'){
            $course = collegeStudent::select('courseID','yearLevel', 'dateEnrolled', 'studentNo')->where('studentID', '=', $studentid)
                                    ->where('id', '=', DB::raw("(SELECT MAX(id) FROM college_students WHERE studentID = '$studentid')"))
                                    ->get();
            if(count($course)>0){
                $student->course=$course[0]->courseID;
                $student->gradeLevel = $course[0]->yearLevel;
                $student->college = $this->getCollegeOfCourse($course[0]->courseID);
                $student->dateEnrolled = $course[0]->dateEnrolled;
                $student->studNo = $course[0]->studentNo;
            }
        }elseif ($student->type =='seniorhigh') {
            $strand = SHSStudent::select('seniorHighStrandID', 'gradeLevel', 'dateEnrolled', 'studNo')
                                ->where('studentID', '=', $studentid)
                                ->where('id', '=', DB::raw("(SELECT MAX(id) FROM s_h_s_students WHERE studentID = '$studentid')"))
                                ->get();
            if(count($strand)>0){
                $student->strand = $strand[0]->seniorHighStrandID; 
                $student->gradeLevel = $strand[0]->gradeLevel;
                $student->dateEnrolled = $strand[0]->dateEnrolled;
                $student->studNo = $strand[0]->studNo;
            }
        }elseif ($student->type =='juniorhigh') {
            $gradelevel = JHStudent::select('gradeLevel', 'dateEnrolled', 'studNo')
                                    ->where('studentID', '=', $studentid)
                                    ->where('id', '=', DB::raw("(SELECT MAX(id) FROM j_h_students WHERE studentID = '$studentid')"))
                                    ->get();
            if(count($gradelevel)>0){
                $student->gradeLevel = $gradelevel[0]->gradeLevel;
                $student->dateEnrolled = $gradelevel[0]->dateEnrolled;
                $student->studNo = $gradelevel[0]->studNo;
            }
        }elseif($student->type =='elementary'){
            $gradelevel = elemStudent::select('gradeLevel', 'dateEnrolled', 'studNo')
                                        ->where('studentID', '=', $studentid)
                                        ->where('id', '=', DB::raw("(SELECT MAX(id) FROM elem_students WHERE studentID = '$studentid')"))
                                        ->get();
            if(count($gradelevel)>0){
                $student->gradeLevel = $gradelevel[0]->gradeLevel;
                $student->dateEnrolled = $gradelevel[0]->dateEnrolled;
                $student->studNo = $gradelevel[0]->studNo;
            }
        }
        return $student;
    }
    public function studentUpdate(Request $request){
        $this->validate($request, array(
            'studentTypeComboModal'=>'required|max:100',
            'yearLevelComboModal'=>'required',
            'dobTextModal'=>'required',
            'nationalityTextModal'=>'required|max:100',
            'lastNameTextModal'=>'required|max:100',
            'firstNameTextModal'=>'required|max:100',
            'genderRadioButton' => 'required',
            'studentNoTextModal' => 'required',
            'dateEnrolledText' =>'required'
        ));
        if($this->getCurrentSchoolYear()==0){
            return "There is no activated school year yet!";
        }
        
        
        $this->request = $request;
        
        DB::transaction(function(){
           
            $student =  student::find($this->request->id);
            $this->insertStudentUpdate($student);
            $student->firstName = $this->request->firstNameTextModal;
            $student->middleName = $this->request->middleNameTextModal;
            $student->lastName = $this->request->lastNameTextModal;
            $student->dateOfBirth = $this->request->dobTextModal;
            $student->gender = $this->request->genderRadioButton;
            $student->status = $this->request->maritalStatusComboModal;
            $student->nationality = $this->request->nationalityTextModal;
            $student->religion = $this->request->religionTextModal;
            $student->ethnicity = $this->request->ethnicityTextModal;
            $student->language = $this->request->languageSpokenTextModal;
            $student->contactNo = $this->request->contactNoTextModal;
            $student->cityAddress = $this->request->cityAddressTextModal;
            $student->provincialAddress = $this->request->provincialAddressTextModal;
            
            if($this->request->studentTypeComboModal == 'college'){
                $course = $this->getCourses($this->request->collegeComboModal);
                if($course =='nocourses'){
                    $this->html= "There is no assigned courses in this college!";
                    return;
                }
                $studenttype = $this->getStudentType($this->request->id);
                
                $studNo = collegeStudent::select('studentNo')->where('studentID', '=', $this->request->id)->get();
                
                if(count($studNo)>0){
                    if($studNo[0]->studentNo!=$this->request->studentNoTextModal){
                        $this->validate($this->request, array(
                            'studentNoTextModal' => ['required', new studentNoRule],
                        ));
                    }
                }
                
                $image ='';
                if( $this->request->hasFile('studentImage')){
                    $image = $this->request->file('studentImage');
                    $student->imageType = $image->getClientOriginalExtension();
                }

                $student->save();
                //$this->insertStudentUpdate($student);
                activityController::insertActivity('Updated new student with student id:'.$student->id);
                if( $this->request->hasFile('studentImage')){
                    $filename = $student->id.'.'.$image->getClientOriginalExtension();
                    $location = public_path("images-database/students/". $filename);
                    Image::make($image)->resize(200,200)->save($location);
                }
                
                if($studenttype != $this->request->studentTypeComboModal){
                    $count = collegeStudent::where('studentID', '=', $this->request->id)->count();
                    if($count >0){
                        $this->html= "This student cannot change to college because he has existing college records!";
                        return;
                    }
                    switch ($studenttype) {
                        
                        case 'seniorhigh':
                            $seniorstudent = SHSStudent::where('studentID', '=', $this->request->id)->get();
                            if(count($seniorstudent)>0){
                                $deletelei = studentLeisureSH::where('SHSStudentID', '=', $seniorstudent[0]->id)->count();
                                if($deletelei>0){
                                    $this->html= "cannotchange";
                                    return;
                                }
                                $seniorstudent[0]->forceDelete();
                            }
                        break;
                        case 'juniorhigh':
                            $juniorstudent = JHStudent::where('studentID', '=', $this->request->id)->get();
                            if(count($juniorstudent)>0){
                                $deletelei = studentLeisureJH::where('JHStudentID', '=', $juniorstudent[0]->id)->get();
                                if($deletelei>0){
                                    $this->html= "cannotchange";
                                    return;
                                }
                                $juniorstudent[0]->forceDelete();
                            }
                        break;
                        case 'elementary':
                            $elemstudent = elemStudent::where('studentID', '=', $this->request->id)->get();
                            if(count($elemstudent)>0){
                                $deleteLei = studentLeisureElem::where('elemStudentID', '=',  $elemstudent[0]->id)->count();
                                if(count($deleteLei)>0){
                                    $this->html= "cannotchange";
                                    return;
                                }
                                $elemstudent[0]->forceDelete();
                            }
                        break;
                        default:
                            # code...
                            break;
                    }
                    $collegesave = new collegeStudent;
                    $collegesave->studentID = $student->id;
                    $collegesave->studentNo = $this->request->studentNoTextModal;
                    $collegesave->yearLevel = $this->request->yearLevelComboModal;
                    $collegesave->dateEnrolled = $this->request->dateEnrolledText;
                    $collegesave->semesterID = $this->getCurrentSem();
                    $collegesave->schoolYearID = $this->getCurrentSchoolYear();
                    $collegesave->organizationID = $this->getNoOrganizationID();
                    $collegesave->courseID = $this->request->courseComboModal;
                    $this->insertStudentTypeUpdate($student->id, "college", null, $this->request->yearLevelComboModal, $this->request->courseComboModal);
                    $collegesave->save();
                    
                }else{
                    $collegesave = collegeStudent::where('studentID', '=', $student->id)
                                                    ->where('id', '=', DB::raw("(SELECT max(id) AS maxid 
                                                                                FROM college_students
                                                                                WHERE studentID = '$student->id')"))
                                                    ->get();
                    if(count($collegesave)>0){
                        $collegesave[0]->studentNo = $this->request->studentNoTextModal;
                        $collegesave[0]->yearLevel = $this->request->yearLevelComboModal;
                        $collegesave[0]->dateEnrolled = $this->request->dateEnrolledText;
                        $collegesave[0]->courseID = $this->request->courseComboModal;
                        $this->insertStudentTypeUpdate($student->id, "", null, $this->request->yearLevelComboModal, $this->request->courseComboModal);
                        $collegesave[0]->save();
                        
                    }
                }
            }elseif ($this->request->studentTypeComboModal == 'seniorhigh') {
                if($this->request->seniorHighStrand==''){
                    $this->html= "Senior high strand is required for a senior high student!";
                    return;
                }
                $studenttype = $this->getStudentType($this->request->id);

                $studNo = SHSStudent::select('studNo')->where('studentID', '=', $this->request->id)->get();
                if(count($studNo)>0){
                    if($studNo[0]->studNo!=$this->request->studentNoTextModal){
                        $this->validate($this->request, array(
                            'studentNoTextModal' => ['required', new studentNoRule],
                        ));
                    }
                }
                
                $image ='';
                if( $this->request->hasFile('studentImage')){
                    $image = $this->request->file('studentImage');
                    $student->imageType = $image->getClientOriginalExtension();
                }

                $student->save();
                // $this->insertStudentUpdate($student);
                activityController::insertActivity('Updated new student with student id:'.$student->id);
                if( $this->request->hasFile('studentImage')){
                    $filename = $student->id.'.'.$image->getClientOriginalExtension();
                    $location = public_path("images-database/students/". $filename);
                    Image::make($image)->resize(200,200)->save($location);
                }
                if($studenttype != $this->request->studentTypeComboModal){
                    $count = SHSStudent::where('studentID', '=', $this->request->id)->count();
                    if($count >0){
                        $this->html= "This student cannot change to seniorhigh because he has existing seniorhigh records!";
                        return;
                    }
                    switch ($studenttype) {
                        
                        case 'college':
                            $collegestudent = collegeStudent::where('studentID', '=', $this->request->id)->get();
                            if(count($collegestudent)>0){
                                $deleteContract = contract::where('collegStudentID', '=', $collegestudent[0]->id)->count();
                                if($deleteContract>0){
                                    $this->html= "cannotchange";
                                    return; 
                                }
                                $deleteStudLei = studentLeisureCollege::where('collegStudentID', '=', $collegestudent[0]->id)->count();
                                if($deleteStudLei>0){
                                    $this->html= "cannotchange";
                                    return; 
                                }
                                $collegestudent[0]->forceDelete();
                            }
                        break;
                        case 'juniorhigh':
                            $juniorstudent = JHStudent::where('studentID', '=', $this->request->id)->get();
                            if(count($juniorstudent)>0){
                                $deletelei = studentLeisureJH::where('JHStudentID', '=', $juniorstudent[0]->id)->count();
                                if($deletelei>0){
                                    $this->html= "cannotchange";
                                    return;
                                }
                                $juniorstudent[0]->forceDelete();
                            }
                        break;
                        case 'elementary':
                            $elemstudent = elemStudent::where('studentID', '=', $this->request->id)->get();
                            if(count($elemstudent)>0){
                                $deleteLei = studentLeisureElem::where('elemStudentID', '=',  $elemstudent[0]->id)->count();
                                if(count($deleteLei)>0){
                                    $this->html= "cannotchange";
                                    return;
                                }
                                $elemstudent[0]->forceDelete();
                            }
                        break;
                        default:
                            # code...
                            break;
                    }
                    $seniorstudent = new SHSStudent;
                    $seniorstudent->studentID = $student->id;
                    $seniorstudent->studNo = $this->request->studentNoTextModal;
                    $seniorstudent->schoolYearID =$this->getCurrentSchoolYear();
                    $seniorstudent->semesterID =$this->getCurrentSem();
                    $seniorstudent->seniorHighStrandID =$this->request->seniorHighStrand;
                    $seniorstudent->gradeLevel = $this->request->yearLevelComboModal;
                    $seniorstudent->dateEnrolled = $this->request->dateEnrolledText;
                    $this->insertStudentTypeUpdate($student->id, "seniorhigh", $this->request->seniorHighStrand, $this->request->yearLevelComboModal, null);
                    $seniorstudent->save();
                    
                }else{
                    $collegesave = SHSStudent::where('studentID', '=', $student->id)
                                                    ->where('id', '=', DB::raw("(SELECT max(id) AS maxid 
                                                    FROM s_h_s_students
                                                    WHERE studentID = '$student->id')"))
                                                    ->get();
                    if(count($collegesave)>0){
                        $collegesave[0]->studNo = $this->request->studentNoTextModal;
                        $collegesave[0]->seniorHighStrandID=$this->request->seniorHighStrand;
                        $collegesave[0]->gradeLevel = $this->request->yearLevelComboModal;
                        $collegesave[0]->dateEnrolled = $this->request->dateEnrolledText;
                        $this->insertStudentTypeUpdate($student->id, "", $this->request->seniorHighStrand, $this->request->yearLevelComboModal, null);
                        $collegesave[0]->save();
                        
                    }
                }
                
            }elseif ($this->request->studentTypeComboModal == 'juniorhigh') {
                $studenttype = $this->getStudentType($this->request->id);

                $studNo = JHStudent::select('studNo')->where('studentID', '=', $this->request->id)->get();
                //error_log($studNo);
                if(count($studNo)>0){
                    if($studNo[0]->studNo!=$this->request->studentNoTextModal){
                        $this->validate($this->request, array(
                            'studentNoTextModal' => ['required', new studentNoRule],
                        ));
                    }
                }
               
                $image ='';
                if( $this->request->hasFile('studentImage')){
                    $image = $this->request->file('studentImage');
                    $student->imageType = $image->getClientOriginalExtension();
                }
                $student->save();
                // $this->insertStudentUpdate($student);
                activityController::insertActivity('Updated new student with student id:'.$student->id);
                if( $this->request->hasFile('studentImage')){
                    $filename = $student->id.'.'.$image->getClientOriginalExtension();
                    $location = public_path("images-database/students/". $filename);
                    Image::make($image)->resize(200,200)->save($location);
                }
                if($studenttype != $this->request->studentTypeComboModal){
                    $count = JHStudent::where('studentID', '=', $this->request->id)->count();
                    if($count >0){
                        $this->html= "This student cannot change to juniorhigh because he has existing juniorhigh records!";
                        return;
                    }
                    switch ($studenttype) {
                        
                        case 'college':
                            $collegestudent = collegeStudent::where('studentID', '=', $this->request->id)->get();
                            if(count($collegestudent)>0){
                                $deleteContract = contract::where('collegStudentID', '=', $collegestudent[0]->id)->count();
                                if($deleteContract>0){
                                    $this->html= "cannotchange";
                                    return; 
                                }
                                $deleteStudLei = studentLeisureCollege::where('collegStudentID', '=', $collegestudent[0]->id)->count();
                                if($deleteStudLei>0){
                                    $this->html= "cannotchange";
                                    return; 
                                }
                                $collegestudent[0]->forceDelete();
                            }
                        break;
                        case 'seniorhigh':
                            $juniorstudent = SHSStudent::where('studentID', '=', $this->request->id)->get();
                            if(count($juniorstudent)>0){
                                $deletelei = studentLeisureSH::where('SHSStudentID', '=', $juniorstudent[0]->id)->count();
                                if($deletelei>0){
                                    $this->html= "cannotchange";
                                    return;
                                }
                                $juniorstudent[0]->forceDelete();
                            }
                        break;
                        case 'elementary':
                            $elemstudent = elemStudent::where('studentID', '=', $this->request->id)->get();
                            if(count($elemstudent)>0){
                                $deleteLei = studentLeisureElem::where('elemStudentID', '=',  $elemstudent[0]->id)->count();
                                if(count($deleteLei)>0){
                                    $this->html= "cannotchange";
                                    return;
                                }
                                $elemstudent[0]->forceDelete();
                            }
                        break;
                        default:
                            # code...
                            break;
                    }
                    $juniorhighstudent  =  new JHStudent;
                    $juniorhighstudent->studentID =$student->id;
                    $juniorhighstudent->studNO =$this->request->studentNoTextModal;
                    $juniorhighstudent->schoolYearID =$this->getCurrentSchoolYear();
                    $juniorhighstudent->gradeLevel =  $this->request->yearLevelComboModal;
                    $juniorhighstudent->dateEnrolled =$this->request->dateEnrolledText;
                    $this->insertStudentTypeUpdate($student->id, "juniorhigh", null, $this->request->yearLevelComboModal, null);
                    $juniorhighstudent->save();
                   
                }else{
                    $collegesave = JHStudent::where('studentID', '=', $student->id)
                                                    ->where('id', '=', DB::raw("(SELECT max(id) AS maxid 
                                                    FROM j_h_students
                                                    WHERE studentID = '$student->id')"))
                                                    ->get();
                    if(count($collegesave)>0){
                        $collegesave[0]->studNo = $this->request->studentNoTextModal;
                        $collegesave[0]->gradeLevel =  $this->request->yearLevelComboModal;
                        $collegesave[0]->dateEnrolled =$this->request->dateEnrolledText;
                        $this->insertStudentTypeUpdate($student->id, "", null, $this->request->yearLevelComboModal, null);
                        $collegesave[0]->save();
                        
                    }
                }
               
            }elseif ($this->request->studentTypeComboModal == 'elementary') {
                $studenttype = $this->getStudentType($this->request->id);

                $studNo = elemStudent::select('studNo')->where('studentID', '=', $this->request->id)->get();
                if(count($studNo)>0){
                    if($studNo[0]->studNo!=$this->request->studentNoTextModal){
                        $this->validate($this->request, array(
                            'studentNoTextModal' => ['required', new studentNoRule],
                        ));
                    }
                }
                
                $image ='';
                if( $this->request->hasFile('studentImage')){
                    $image = $this->request->file('studentImage');
                    $student->imageType = $image->getClientOriginalExtension();
                }
                $student->save();
                // $this->insertStudentUpdate($student);
                activityController::insertActivity('Updated new student with student id:'.$student->id);
                if( $this->request->hasFile('studentImage')){
                    $filename = $student->id.'.'.$image->getClientOriginalExtension();
                    $location = public_path("images-database/students/". $filename);
                    Image::make($image)->resize(200,200)->save($location);
                }
                if($studenttype != $this->request->studentTypeComboModal){
                    $count = elemStudent::where('studentID', '=', $this->request->id)->count();
                    if($count >0){
                        $this->html= "This student cannot change to elementary because he has existing elementary records!";
                        return;
                    }
                    switch ($studenttype) {
                        
                        case 'college':
                            $collegestudent = collegeStudent::where('studentID', '=', $this->request->id)->get();
                            if(count($collegestudent)>0){
                                $deleteContract = contract::where('collegStudentID', '=', $collegestudent[0]->id)->count();
                                if($deleteContract>0){
                                    $this->html= "cannotchange";
                                    return; 
                                }
                                $deleteStudLei = studentLeisureCollege::where('collegStudentID', '=', $collegestudent[0]->id)->count();
                                if($deleteStudLei>0){
                                    $this->html= "cannotchange";
                                    return; 
                                }
                                $collegestudent[0]->forceDelete();
                            }
                        break;
                        case 'seniorhigh':
                            $juniorstudent = SHSStudent::where('studentID', '=', $this->request->id)->get();
                            if(count($juniorstudent)>0){
                                $deletelei = studentLeisureSH::where('SHSStudentID', '=', $juniorstudent[0]->id)->count();
                                if($deletelei>0){
                                    $this->html= "cannotchange";
                                    return;
                                }
                                $juniorstudent[0]->forceDelete();
                            }
                        break;
                        case 'juniorhigh':
                            $elemstudent = JHStudent::where('studentID', '=', $this->request->id)->get();
                            if(count($elemstudent)>0){
                                $deletelei = studentLeisureJH::where('JHStudentID', '=', $elemstudent[0]->id)->get();
                                if($deletelei>0){
                                    $this->html= "cannotchange";
                                    return;
                                }
                                $elemstudent[0]->forceDelete();
                            }
                        break;
                        default:
                            # code...
                            break;
                    }
                    $elementarystudent = new elemStudent;
                    $elementarystudent->studentID =$student->id;
                    $elementarystudent->studNO =$this->request->studentNoTextModal;
                    $elementarystudent->schoolYearID =$this->getCurrentSchoolYear();
                    $elementarystudent->dateEnrolled =$this->request->dateEnrolledText;
                    $elementarystudent->gradeLevel =$this->request->yearLevelComboModal;
                    $this->insertStudentTypeUpdate($student->id, "elementary", null, $this->request->yearLevelComboModal, null);
                    $elementarystudent->save();
                    
                }else{
                    $collegesave = elemStudent::where('studentID', '=', $student->id)
                                                ->where('id', '=', DB::raw("(SELECT max(id) AS maxid 
                                                FROM elem_students
                                                WHERE studentID = '$student->id')"))
                                                ->get();
                    if(count($collegesave)>0){
                        $collegesave[0]->studNo = $this->request->studentNoTextModal;
                        $collegesave[0]->gradeLevel =  $this->request->yearLevelComboModal;
                        $collegesave[0]->dateEnrolled =$this->request->dateEnrolledText;
                        $this->insertStudentTypeUpdate($student->id, "", null, $this->request->yearLevelComboModal, null);
                        $collegesave[0]->save();
                       
                    }
                }
               
            }
           
            $this->student = $student;
            
        });
        
    
        return $this->html;
    }
    public function insertStudentTypeUpdate($studentid, $studenttype, $strand, $gradeLevel, $courseName){
        $studUpdate =  new studentTypeUpdate;
        if($studenttype!=""){
            $studUpdate->studentID = $studentid;
            $studUpdate->studentType =  $studenttype;
            $studUpdate->strand = $strand;
            $studUpdate->gradeLevel = $gradeLevel;
            $studUpdate->courseName = $courseName;
            $studUpdate->updatedBy = session()->get('user')['userName'];
            $studUpdate->save();
        }else{
            $mystudenttype = $this->getStudentType($studentid);
            switch ($mystudenttype) {
                case 'college':
                    $temp = collegeStudent::where('studentID', '=', $studentid)
                    ->where('id', '=', DB::raw("(SELECT max(id) AS maxid 
                    FROM college_students
                    WHERE studentID = '$studentid')"))
                    ->get();
                    if(count($temp)>0){
                        if($temp[0]->yearLevel != $gradeLevel){
                            $studUpdate->gradeLevel = $gradeLevel;
                        }
                        error_log($temp[0]->courseID." ".$courseName);
                        if($temp[0]->courseID != $courseName){
                            $studUpdate->courseName = $courseName;
                        }
                    }
                break;
                case 'seniorhigh':
                    $temp = SHSStudent::where('studentID', '=', $studentid)
                    ->where('id', '=', DB::raw("(SELECT max(id) AS maxid 
                    FROM s_h_s_students
                    WHERE studentID = '$studentid')"))
                    ->get();
                    if(count($temp)>0){
                        if($temp[0]->gradeLevel != $gradeLevel){
                            $studUpdate->gradeLevel = $gradeLevel;
                        }
                        if($temp[0]->strandID != $strand){
                            $studUpdate->strand = $strand;
                        }
                    }
                break;
                case 'juniorhigh':
                    $temp = JHStudent::where('studentID', '=', $studentid)
                    ->where('id', '=', DB::raw("(SELECT max(id) AS maxid 
                    FROM j_h_students
                    WHERE studentID = '$studentid')"))
                    ->get();
                    if(count($temp)>0){
                        if($temp[0]->gradeLevel != $gradeLevel){
                            $studUpdate->gradeLevel = $gradeLevel;
                        }
                    }
                break;
                case 'elementary':
                    $temp = elemStudent::where('studentID', '=', $studentid)
                    ->where('id', '=', DB::raw("(SELECT max(id) AS maxid 
                    FROM elem_students
                    WHERE studentID = '$studentid')"))
                    ->get();
                    if(count($temp)>0){
                        if($temp[0]->gradeLevel != $gradeLevel){
                            $studUpdate->gradeLevel = $gradeLevel;
                        }
                    }
                break;
                default:
                    # code...
                    break;
            }
            $studUpdate->studentID = $studentid;
            $studUpdate->updatedBy = session()->get('user')['userName'];
            $studUpdate->save();
        }
    }
    public function insertStudentUpdate($student){
        $mystudent = new studentUpdates;
        $mystudent->studentID =$student->id;
        //$student =  student::find($this->request->id);
        $mystudent->firstName = $student->firstName;
        $mystudent->middleName = $student->middleName; 
        $mystudent->lastName = $student->lastName;
        $mystudent->dateOfBirth = $student->dateOfBirth;
        $mystudent->gender = $student->gender;
        $mystudent->status = $student->status;
        $mystudent->nationality = $student->nationality; 
        $mystudent->religion = $student->religion;
        $mystudent->ethnicity = $student->ethnicity; 
        $mystudent->language = $student->language;
        $mystudent->contactNo = $student->contactNo;
        $mystudent->cityAddress = $student->cityAddress;
        $mystudent->provincialAddress = $student->provincialAddress;
        $mystudent->updatedBy =  session()->get('user')['userName'];
        $mystudent->save();
    }
    public function getStudentFamilyInfo($studentid){
        $faminfo = familyInfo::where('studentID', '=', $studentid)
        ->where('id', '=', familyInfo::where('studentID', '=', $studentid)->max('id'))->get();
        error_log($faminfo);
        return $faminfo;
    }
    public function studentFamilyInfoInsert(Request $request){
        $this->validate($request, array(
            'maritalStatus'=>'required',
            'noOfRelatives'=>'required',
            'noOfHelpers'=>'required'
        ));
        $faminfo = new familyInfo;
        if($request->maritalStatus != 'MarriedCivilly'){
            $faminfo->marriedCivil = 0;
            switch ($request->maritalStatus) {
                case 'MarriedInChurch':
                    $faminfo->marriedChurch = 1;
                break;
                case 'ParentsLivingTogether':
                    $faminfo->livingTogether = 1;
                break;
                case 'ParentsSeperated':
                    $faminfo->seperated = 1;
                break;  
                case 'FatherRemarried':
                    $faminfo->fatherRemarried = 1; 
                break;   
                case 'MotherRemarried':
                    $faminfo->motherRemarried = 1; 
                break;             
                default:
                    # code...
                    break;
            }
        }
        $faminfo->relatives = $request->noOfRelatives;
        $faminfo->helpers = $request->noOfHelpers;
        $faminfo->studentID = $request->id;
        activityController::insertActivity('Updated a family of student:'.$request->id);
        $faminfo->save();
        return $faminfo;
    }
    public function getFamilyRow($family){
        return "<tr id ='familyStudent-$family->id'>
                    <td>".ucfirst($family->relationship)."</td>
                    <td>$family->name</td>
                    <td>$family->address</td>
                    <td>$family->dateOfBirth</td>
                    <td>$family->contactNo</td>
                    <td>$family->religion</td>
                    <td>$family->nationality</td>
                    <td>$family->occupation</td>
                    <td>$family->employer</td>
                    <td>$family->language</td>
                    <td><button class='btn btn-dark editFamilyStudent' id ='editFamilyStudent-$family->id'>
                    <i class='fa fa-edit'></i></button></td>
                    <td><button class='btn btn-warning deleteFamilyStudent' id ='deleteFamilyStudent-$family->id'>
                    <i class='fa fa-trash-o'></i></button></td>
                </tr>";
    }
    public function getFamilyOfStudent($studentid){
        $family = family::where('studentID', '=', $studentid)->get();
        if(count($family)==0){
            return "<tr class='noStudentFamily'><td colspan = '12'><h2>These student have no families in database</h2></td></tr>";
        }
        $html = '';
        foreach ($family as $family) {
            $html .=$this->getFamilyRow($family);
        }
        return $html;
    }
    public function familyInsertUpdate(Request $request){
        $this->validate($request, array(
            'nameOfFamilyMember'=>'required|max:100',
            'addressFamily'=>'required|max:100',
            'dateOfBirthFamily'=>'required',
            'contactNoFamily'=>'required|max:100',
            'religionFamily'=>'required|max:100',
            'nationalityFamily'=>'required|max:100',
            'languageFamily' => 'required|max:100',
            'relationshipFamily' => 'required|max:100'
        ));
        if($request->action =='add'){
            $family = new family;
            $family->type ='-';
            $family->studentID =$request->id;
            $family->name = $request->nameOfFamilyMember;
            $family->address = $request->addressFamily;
            $family->dateOfBirth = $request->dateOfBirthFamily;
            $family->contactNo = $request->contactNoFamily;
            $family->religion = $request->religionFamily;
            $family->nationality = $request->nationalityFamily;
            $family->language=$request->languageFamily;
            $family->relationship = $request->relationshipFamily;
            $family->employer = $request->employerFamily;
            $family->occupation = $request->occupationFamily;
            $family->save();
            activityController::insertActivity('Inserted a family of student:'.$request->id);
            return $this->getFamilyRow($family);
        }else{
            $family = family::find($request->familyid);
            $family->type ='-';
            $family->name = $request->nameOfFamilyMember;
            $family->address = $request->addressFamily;
            $family->dateOfBirth = $request->dateOfBirthFamily;
            $family->contactNo = $request->contactNoFamily;
            $family->religion = $request->religionFamily;
            $family->nationality = $request->nationalityFamily;
            $family->language=$request->languageFamily;
            $family->relationship = $request->relationshipFamily;
            $family->employer = $request->employerFamily;
            $family->occupation = $request->occupationFamily;
            activityController::insertActivity('Updated a family of student:'.$request->id);
            $family->save();
            return $this->getFamilyRow($family);
        }
        
    }
    public function deleteFamily($familyid){
        $family = family::find($familyid);
        $family->forceDelete();
        activityController::insertActivity('Deleted a family of student:'.$familyid);
        return "deleted";
    }
    public function getEducationalBackgroundList($studentid){
        $student  = schoolBG::where('studentID', '=', $studentid)->get();
        if(count($student)==0){
            return "<h2>This student doesn't have any educational background yet</h2>";
        }
        $html = '';
        foreach ($student as $student) {
            $html .=$this->getEducationalBackgroundRow($student);
        }
        return $html;
    }
    public function getEducationalBackgroundRow($student){        
        
        return "<tr id ='educationalBackground-$student->id'>
                    <td>$student->schoolName</td>
                    <td>$student->Honors</td>
                    <td>$student->yearLevel</td>
                    <td>$student->yearAttended</td>
                    <td>
                        <button class='btn btn-dark editEducationalBackground' id ='editEducationalBackground-$student->id'>
                            <i class='fa fa-edit'></i>
                        </button>
                    </td>
                    <td>
                        <button class='btn btn-warning deleteEducationalBackground' id ='deleteEducationalBackground-$student->id'>
                            <i class='fa fa-trash-o'></i>
                        </button>
                    </td>
                </tr>";
    }
    public function educationalBackgroundInsert(Request $request){
        $this->validate($request, array(
            'schoolName'=>'required',
            'yearAttended'=>'required',
            'yearLevel'=>'required',
            'Honors'=>'required'
        ));
       $schoolbackground = new schoolBG;
       $schoolbackground->schoolName = $request->schoolName;
       $schoolbackground->yearAttended = $request->yearAttended;
       $schoolbackground->yearLevel = $request->yearLevel;
       $schoolbackground->Honors = $request->Honors;
       $schoolbackground->studentID = $request->id;
       $schoolbackground->save();
       return $this->getEducationalBackgroundRow($schoolbackground);
    }
    public function educationalBackgroundUpdate(Request $request){
        $this->validate($request, array(
            'schoolName'=>'required',
            'yearAttended'=>'required',
            'yearLevel'=>'required',
            'Honors'=>'required'
        ));
       $schoolbackground = schoolBG::find($request->id);
       $schoolbackground->schoolName = $request->schoolName;
       $schoolbackground->yearAttended = $request->yearAttended;
       $schoolbackground->yearLevel = $request->yearLevel;
       $schoolbackground->Honors = $request->Honors;
       $schoolbackground->studentID = $request->studentID;
       $schoolbackground->save();
       return $this->getEducationalBackgroundRow($schoolbackground);
    }
    public function educationalBackgroundDelete($backgroundid){
        $school = schoolBG::find($backgroundid);
        $school->forceDelete();
        return "deleted";
    }
    public function getFavSubject($studentid){
        $subjects = favSubjectList::join('subjects', 'subjects.id', '=', 'fav_subject_lists.subjectID')
        ->select('fav_subject_lists.*', 'subjects.subjectName', 'subjects.id AS subjectkey')->
        where('fav_subject_lists.studentID', '=', $studentid)->get();
        if(count($subjects)==0){
            return "<h2>This student doesnt have a favorite subject yet</h2>";
        }
        $html = '';
        foreach ($subjects as $subjects) {
            $html .= $this->getFavSubjectRow($subjects);
        }
        return $html;

    }
    public function getFavSubjectRow($subject){
        return "<tr id ='favsubject-$subject->id'>
                    <td data-value ='$subject->subjectkey'>$subject->subjectName</td>
                    <td>$subject->grade</td>
                    <td>
                        <button class='btn btn-dark editFavSubject' id ='editFavSubject-$subject->id'>
                            <i class='fa fa-edit'></i>
                        </button>
                    </td>
                    <td>
                        <button class='btn btn-warning deleteFavSubject' id ='deleteFavSubject-$subject->id'>
                            <i class='fa fa-trash-o'></i>
                        </button>
                    </td>
                </tr>";
    }
    public function favSubjectInsert(Request $request){
        $this->validate($request, array(
            'grade'=>'required'
        ));
        $favsubject = favSubjectList::where('subjectID', '=', $request->subjectID)
                                    ->where('studentID', '=',$request->studentID)->count();
        if($favsubject>0){
            return "cannotinsertfav";
        }
        $disliked = dislikedSubject::where('subjectID', '=', $request->subjectID)
        ->where('studentID', '=',$request->studentID)->count();
        if($disliked>0){
            return "cannotinsert";
        }
        $subject = new favSubjectList;
        $subject->subjectID = $request->subjectID;
        $subject->studentID = $request->studentID;
        $subject->grade = $request->grade;
        $subject->dateAssigned = date('Y-m-d');
        $subject->save();
        $thissubject = favSubjectList::join('subjects', 'subjects.id', '=', 'fav_subject_lists.subjectID')
                                        ->select('fav_subject_lists.*', 'subjects.subjectName', 'subjects.id AS subjectkey')
                                        ->where('fav_subject_lists.id', '=', $subject->id)->get();
        foreach ($thissubject as $thissubject) {
            $thissubject->html = $this->getFavSubjectRow($thissubject);
        }
        
        $thissubject->status = "success";
        activityController::insertActivity('Inserted new favorite subject of student with student id:'. $request->studentID);
        return $thissubject;
    }
    public function favSubjectUpdate(Request $request){
        $this->validate($request, array(
            'grade'=>'required'
        ));
        $tempfavsubject = favSubjectList::find($request->favsubjid);
        if($tempfavsubject->subjectID!=$request->subjectID){
            $favsubject = favSubjectList::where('subjectID', '=', $request->subjectID)
                                    ->where('studentID', '=',$request->studentID)->count();
            if($favsubject>0){
                return "cannotinsert";
            }
            $disliked = dislikedSubject::where('subjectID', '=', $request->subjectID)
            ->where('studentID', '=',$request->studentID)->count();
            if($disliked>0){
                return "cannotinsert";
            }
            $tempfavsubject->subjectID = $request->subjectID;
            $tempfavsubject->grade = $request->grade;
            $tempfavsubject->save();
            activityController::insertActivity('Update a favorite subject of student with student id:'. $request->studentID);
        }else{
            $tempfavsubject->grade = $request->grade;
            $tempfavsubject->save();
            activityController::insertActivity('Update a favorite subject of student with student id:'. $request->studentID);
        }
        return "success";
    }
    public function deleteFavSubject($subjid){
        $subject = favSubjectList::find($subjid);
        $subject->forceDelete();
        return "deleted";
    }
    public function getSubjectsCombo(){
        $subject = subject::all();
        $html = '';
        foreach ($subject as $subject) {
            $html .="<option value ='$subject->id'>$subject->subjectName</option>";
        }
        return $html;
    }
    public function getDislikedSubjectRow($subject){
        return "<tr id ='dislikedsubject-$subject->id'>
                    <td data-value ='$subject->subjectkey'>$subject->subjectName</td>
                    <td>$subject->grade</td>
                    <td>
                        <button class='btn btn-dark editDislikedSubject' id ='editDislikedSubject-$subject->id'>
                            <i class='fa fa-edit'></i>
                        </button>
                    </td>
                    <td>
                        <button class='btn btn-warning deleteDislikedSubject' id ='deleteDislikedSubject-$subject->id'>
                            <i class='fa fa-trash-o'></i>
                        </button>
                    </td>
                </tr>";
    }
    public function getDislikedSubjects($studentid){
        $subjects = dislikedSubject::join('subjects', 'subjects.id', '=', 'disliked_subjects.subjectID')
        ->select('disliked_subjects.*', 'subjects.subjectName', 'subjects.id AS subjectkey')->
        where('disliked_subjects.studentID', '=', $studentid)->get();
        if(count($subjects)==0){
            return "<h2>This student doesnt have a disliked subject yet</h2>";
        }
        $html = '';
        foreach ($subjects as $subjects) {
            $html .= $this->getDislikedSubjectRow($subjects);
        }
        return $html;
    }
    public function dislikedSubjectInsert(Request $request){
        $this->validate($request, array(
            'grade'=>'required'
        ));
        $favsubject = favSubjectList::where('subjectID', '=', $request->subjectID)
                                    ->where('studentID', '=',$request->studentID)->count();
        if($favsubject>0){
            return "cannotinsertfav";
        }
        $disliked = dislikedSubject::where('subjectID', '=', $request->subjectID)
        ->where('studentID', '=',$request->studentID)->count();
        if($disliked>0){
            return "cannotinsert";
        }
        $subject = new dislikedSubject;
        $subject->subjectID = $request->subjectID;
        $subject->studentID = $request->studentID;
        $subject->grade = $request->grade;
        $subject->dateAssigned = date('Y-m-d');
        $subject->save();
        $thissubject = dislikedSubject::join('subjects', 'subjects.id', '=', 'disliked_subjects.subjectID')
                                        ->select('disliked_subjects.*', 'subjects.subjectName', 'subjects.id AS subjectkey')
                                        ->where('disliked_subjects.id', '=', $subject->id)->get();
        foreach ($thissubject as $thissubject) {
            $thissubject->html = $this->getDislikedSubjectRow($thissubject);
        }
        
        $thissubject->status = "success";
        activityController::insertActivity('Inserted new disliked subject of student with student id:'. $request->studentID);
        return $thissubject;
    }
    public function dislikedSubjectUpdate(Request $request){
        $this->validate($request, array(
            'grade'=>'required'
        ));
        $tempfavsubject = dislikedSubject::find($request->dissubjid);
        if($tempfavsubject->subjectID!=$request->subjectID){
            $favsubject = favSubjectList::where('subjectID', '=', $request->subjectID)
                                    ->where('studentID', '=',$request->studentID)->count();
            if($favsubject>0){
                return "cannotinsert";
            }
            $disliked = dislikedSubject::where('subjectID', '=', $request->subjectID)
            ->where('studentID', '=',$request->studentID)->count();
            if($disliked>0){
                return "cannotinsert";
            }
            $tempfavsubject->subjectID = $request->subjectID;
            $tempfavsubject->grade = $request->grade;
            $tempfavsubject->save();
            activityController::insertActivity('Update a disliked subject of student with student id:'. $request->studentID);
        }else{
            $tempfavsubject->grade = $request->grade;
            $tempfavsubject->save();
            activityController::insertActivity('Update a disliked subject of student with student id:'. $request->studentID);
        }
        return "success";
    }
    public function deleteDislikedSubject($subjectid){
        $subject =dislikedSubject::find($subjectid);
        $subject->forceDelete();
        return "deleted";
    }
    public function insertStudentLeisure(Request $request){
        $studenttype = $this->getStudentType($request->studentID);
        $leisure ='';
        $this->validate($request, array(
            'schoolChoice'=>'required|max:100',
            'howDidYouComeToThisSchool'=>'required|max:100',
            'getInfo'=>'required|max:100',
            'whereDidYouGetThisInfo'=>'required|max:100',
            'financialSupport'=>'required|max:100',
            'scholarship'=>'required|max:100',
            'rankClass' => 'required',
            'average'=>'required'
        ));
        switch ($studenttype) {
            case 'college':
                $this->validate($request, array(
                    'major'=>'required|max:100',
                    'presentEducation'=>'required|max:100',
                    'courseChoice'=>'required|max:100',
                    'howDidYouMakeThisChoice'=>'required|max:100'
                ));
                $leisure = new studentLeisureCollege;
                $leisure->major = $request->major;
                $leisure->presentEducation = $request->presentEducation;
                $leisure->courseChoice = $request->courseChoice;
                $leisure->howDidYouMakeThisChoice = $request->howDidYouMakeThisChoice;
                $leisure->schoolChoice = $request->schoolChoice;
                $leisure->howDidYouComeToThisSchool = $request->howDidYouComeToThisSchool;
                $leisure->getInfo = $request->getInfo;
                $leisure->whereDidYouGetThisInfo = $request->whereDidYouGetThisInfo;
                $leisure->financialSupport = $request->financialSupport;
                $leisure->rankClass = $request->rankClass;
                $leisure->average = $request->average;
                $leisure->scholarship = $request->scholarship;
                $leisure->otherRemarks = $request->otherRemarks;
                $collegeid = collegeStudent::where('studentID', '=',$request->studentID)->get();
                $leisure->collegStudentID = $collegeid[0]->id;
                if(isset($request->barelyPassedSubjectsCheck)){
                    $leisure->selfEval1 = '1';
                }else{
                    $leisure->selfEval1 = '0';
                }
                if(isset($request->failedSubjectsCheck)){
                    $leisure->selfEval2 = '1';
                }else{
                    $leisure->selfEval2 = '0';
                }
                if(isset($request->fearSubjectsCheck)){
                    $leisure->selfEval3 = '1';
                }else{
                    $leisure->selfEval3 = '0';
                }
                if(isset($request->hardSubjectsCheck)){
                    $leisure->selfEval4 = '1';
                }else{
                    $leisure->selfEval4 = '0';
                }
                if(isset($request->dificultySubjectsCheck)){
                    $leisure->selfEval5 = '1';
                }else{
                    $leisure->selfEval5 = '0';
                }
                if(isset($request->confidentCheck)){
                    $leisure->selfEval6 = '1';
                }else{
                    $leisure->selfEval6 = '0';
                }
                $leisure->save();
                activityController::insertActivity('Inserted new leisure of student with student id:'. $request->studentID);
                return $this->getLeisureRow($leisure);
                /*TODO: do insert for other leisure tables */
            break;
            case 'seniorhigh':
                $this->validate($request, array(
                    'major'=>'required|max:100'
                ));
                $leisure = new studentLeisureSH;
                $leisure->major = $request->major;
                $leisure->schoolChoice = $request->schoolChoice;
                $leisure->howDidYouComeToThisSchool = $request->howDidYouComeToThisSchool;
                $leisure->getInfo = $request->getInfo;
                $leisure->whereDidYouGetThisInfo = $request->whereDidYouGetThisInfo;
                $leisure->financialSupport = $request->financialSupport;
                $leisure->rankClass = $request->rankClass;
                $leisure->average = $request->average;
                $leisure->scholarship = $request->scholarship;
                $leisure->otherRemarks = $request->otherRemarks;
                $seniorhighid = SHSStudent::where('studentID', '=',$request->studentID)->get();
                $leisure->SHSStudentID = $seniorhighid[0]->id;
                if(isset($request->barelyPassedSubjectsCheck)){
                    $leisure->selfEval1 = '1';
                }else{
                    $leisure->selfEval1 = '0';
                }
                if(isset($request->failedSubjectsCheck)){
                    $leisure->selfEval2 = '1';
                }else{
                    $leisure->selfEval2 = '0';
                }
                if(isset($request->barelyPassedSubjectsCheck)){
                    $leisure->selfEval3 = '1';
                }else{
                    $leisure->selfEval3 = '0';
                }
                if(isset($request->failedSubjectsCheck)){
                    $leisure->selfEval4 = '1';
                }else{
                    $leisure->selfEval4 = '0';
                }
                if(isset($request->dificultySubjectsCheck)){
                    $leisure->selfEval5 = '1';
                }else{
                    $leisure->selfEval5 = '0';
                }
                if(isset($request->confidentCheck)){
                    $leisure->selfEval6 = '1';
                }else{
                    $leisure->selfEval6 = '0';
                }
                $leisure->save();
                activityController::insertActivity('Inserted new leisure of student with student id:'. $request->studentID);
                return $this->getLeisureRow($leisure);
            break;
            case 'juniorhigh':
                $leisure = new studentLeisureJH;
                $leisure->schoolChoice = $request->schoolChoice;
                $leisure->howDidYouComeToThisSchool = $request->howDidYouComeToThisSchool;
                $leisure->getInfo = $request->getInfo;
                $leisure->whereDidYouGetThisInfo = $request->whereDidYouGetThisInfo;
                $leisure->financialSupport = $request->financialSupport;
                $leisure->rankClass = $request->rankClass;
                $leisure->average = $request->average;
                $leisure->scholarship = $request->scholarship;
                $leisure->otherRemarks = $request->otherRemarks;
                $juniorhighid = JHStudent::where('studentID', '=',$request->studentID)->get();
                $leisure->JHStudentID = $juniorhighid[0]->id;
                if(isset($request->barelyPassedSubjectsCheck)){
                    $leisure->selfEval1 = '1';
                }else{
                    $leisure->selfEval1 = '0';
                }
                if(isset($request->failedSubjectsCheck)){
                    $leisure->selfEval2 = '1';
                }else{
                    $leisure->selfEval2 = '0';
                }
                if(isset($request->barelyPassedSubjectsCheck)){
                    $leisure->selfEval3 = '1';
                }else{
                    $leisure->selfEval3 = '0';
                }
                if(isset($request->failedSubjectsCheck)){
                    $leisure->selfEval4 = '1';
                }else{
                    $leisure->selfEval4 = '0';
                }
                if(isset($request->dificultySubjectsCheck)){
                    $leisure->selfEval5 = '1';
                }else{
                    $leisure->selfEval5 = '0';
                }
                if(isset($request->confidentCheck)){
                    $leisure->selfEval6 = '1';
                }else{
                    $leisure->selfEval6 = '0';
                }
                $leisure->save();
                activityController::insertActivity('Inserted new leisure of student with student id:'. $request->studentID);
                return $this->getLeisureRow($leisure);
            break;
            case 'elementary':
                $leisure = new studentLeisureElem;
                $leisure->schoolChoice = $request->schoolChoice;
                $leisure->howDidYouComeToThisSchool = $request->howDidYouComeToThisSchool;
                $leisure->getInfo = $request->getInfo;
                $leisure->whereDidYouGetThisInfo = $request->whereDidYouGetThisInfo;
                $leisure->financialSupport = $request->financialSupport;
                $leisure->rankClass = $request->rankClass;
                $leisure->average = $request->average;
                $leisure->scholarship = $request->scholarship;
                $leisure->otherRemarks = $request->otherRemarks;
                $elemstudentid = elemStudent::where('studentID', '=',$request->studentID)->get();
                $leisure->elemStudentID = $elemstudentid[0]->id;
                if(isset($request->barelyPassedSubjectsCheck)){
                    $leisure->selfEval1 = '1';
                }else{
                    $leisure->selfEval1 = '0';
                }
                if(isset($request->failedSubjectsCheck)){
                    $leisure->selfEval2 = '1';
                }else{
                    $leisure->selfEval2 = '0';
                }
                if(isset($request->barelyPassedSubjectsCheck)){
                    $leisure->selfEval3 = '1';
                }else{
                    $leisure->selfEval3 = '0';
                }
                if(isset($request->failedSubjectsCheck)){
                    $leisure->selfEval4 = '1';
                }else{
                    $leisure->selfEval4 = '0';
                }
                if(isset($request->dificultySubjectsCheck)){
                    $leisure->selfEval5 = '1';
                }else{
                    $leisure->selfEval5 = '0';
                }
                if(isset($request->confidentCheck)){
                    $leisure->selfEval6 = '1';
                }else{
                    $leisure->selfEval6 = '0';
                }
                $leisure->save();
                activityController::insertActivity('Inserted new leisure of student with student id:'. $request->studentID);
                return $this->getLeisureRow($leisure);
            break;
            default:
                # code...
                break;
        }
    }
    public function studentLeisureList($studentid){
        $studenttype = $this->getStudentType($studentid);
        $html ='';
        $leisure;
        switch ($studenttype) {
            case 'college':
                $collegeid = collegeStudent::where('studentID', '=',$studentid)->get();
                $leisure =studentLeisureCollege::where('collegStudentID', '=', $collegeid[0]->id)->get();
            break;
            case 'seniorhigh':
                $seniorhighid = SHSStudent::where('studentID', '=',$studentid)->get();
                $leisure =studentLeisureSH::where('SHSStudentID', '=', $seniorhighid[0]->id)->get();
            break;
            case 'juniorhigh':
                $juniorhighid = JHStudent::where('studentID', '=',$studentid)->get();
                $leisure =studentLeisureJH::where('JHStudentID', '=', $juniorhighid[0]->id)->get();
            break;
            case 'elementary':
                $elementaryid= elemStudent::where('studentID', '=',$studentid)->get();
                $leisure =studentLeisureElem::where('elemStudentID', '=', $elementaryid[0]->id)->get();
            break;
            default:
                # code...
                break;
        }
        if(count($leisure)==0){
            return "<h2>This student doesn't have any leisure yet</h2>";
        }else{
            foreach ($leisure as $leisure) {
                $html .=$this->getLeisureRow($leisure);
            }
            return $html;
        }
    }
    public function getLeisureRow($leisure){
        return "<tr id ='leisurerow-$leisure->id'>
                    <td>$leisure->schoolChoice</td>
                    <td>$leisure->financialSupport</td>
                    <td>$leisure->rankClass</td>
                    <td>$leisure->average</td>
                    <td>
                        <button class='btn btn-dark leisureedit' id ='leisureedit-$leisure->id'>
                            <i class='fa fa-edit'></i>
                        </button>
                    </td>
                    <td>
                        <button class='btn btn-warning leisuredelete' id ='leisuredelete-$leisure->id'>
                            <i class='fa fa-trash-o'></i>
                        </button>
                    </td>
                </tr>";
    }
    public function studentLeisureData($leisureid, $studentid){
        $leisure;
        $studenttype = $this->getStudentType($studentid);
        switch ($studenttype) {
            case 'college':
                $leisure  = studentLeisureCollege::find($leisureid);
            break;
            case 'seniorhigh':
            $leisure  = studentLeisureSH::find($leisureid);
            break;
            case 'juniorhigh':
            $leisure  = studentLeisureJH::find($leisureid);
            break;
            case 'elementary':
            $leisure  = studentLeisureElem::find($leisureid);
            break;
            default:
                
                break;
        }
        return $leisure;
    }
    public function updateStudentLeisure(Request $request){
        $studenttype = $this->getStudentType($request->studentID);
        $leisure ='';
        $this->validate($request, array(
            'schoolChoice'=>'required|max:100',
            'howDidYouComeToThisSchool'=>'required|max:100',
            'getInfo'=>'required|max:100',
            'whereDidYouGetThisInfo'=>'required|max:100',
            'financialSupport'=>'required|max:100',
            'scholarship'=>'required|max:100',
            'rankClass' => 'required',
            'average'=>'required'
        ));
        switch ($studenttype) {
            case 'college':
                $this->validate($request, array(
                    'major'=>'required|max:100',
                    'presentEducation'=>'required|max:100',
                    'courseChoice'=>'required|max:100',
                    'howDidYouMakeThisChoice'=>'required|max:100'
                ));
                $leisure =  studentLeisureCollege::find($request->id);
                $leisure->major = $request->major;
                $leisure->presentEducation = $request->presentEducation;
                $leisure->courseChoice = $request->courseChoice;
                $leisure->howDidYouMakeThisChoice = $request->howDidYouMakeThisChoice;
                $leisure->schoolChoice = $request->schoolChoice;
                $leisure->howDidYouComeToThisSchool = $request->howDidYouComeToThisSchool;
                $leisure->getInfo = $request->getInfo;
                $leisure->whereDidYouGetThisInfo = $request->whereDidYouGetThisInfo;
                $leisure->financialSupport = $request->financialSupport;
                $leisure->rankClass = $request->rankClass;
                $leisure->average = $request->average;
                $leisure->scholarship = $request->scholarship;
                $leisure->otherRemarks = $request->otherRemarks;
                /*$collegeid = collegeStudent::where('studentID', '=',$request->studentID)->get();
                $leisure->collegStudentID = $collegeid[0]->id;*/
                if(isset($request->barelyPassedSubjectsCheck)){
                    $leisure->selfEval1 = '1';
                }else{
                    $leisure->selfEval1 = '0';
                }
                if(isset($request->failedSubjectsCheck)){
                    $leisure->selfEval2 = '1';
                }else{
                    $leisure->selfEval2 = '0';
                }
                if(isset($request->fearSubjectsCheck)){
                    $leisure->selfEval3 = '1';
                }else{
                    $leisure->selfEval3 = '0';
                }
                if(isset($request->hardSubjectsCheck)){
                    $leisure->selfEval4 = '1';
                }else{
                    $leisure->selfEval4 = '0';
                }
                if(isset($request->dificultySubjectsCheck)){
                    $leisure->selfEval5 = '1';
                }else{
                    $leisure->selfEval5 = '0';
                }
                if(isset($request->confidentCheck)){
                    $leisure->selfEval6 = '1';
                }else{
                    $leisure->selfEval6 = '0';
                }
                $leisure->save();
                activityController::insertActivity('Updated a leisure of student with student id:'. $request->studentID);
                return $this->getLeisureRow($leisure);
                /*TODO: do insert for other leisure tables */
            break;
            case 'seniorhigh':
                $this->validate($request, array(
                    'major'=>'required|max:100'
                ));
                $leisure = studentLeisureSH::find($request->id);
                $leisure->major = $request->major;
                $leisure->schoolChoice = $request->schoolChoice;
                $leisure->howDidYouComeToThisSchool = $request->howDidYouComeToThisSchool;
                $leisure->getInfo = $request->getInfo;
                $leisure->whereDidYouGetThisInfo = $request->whereDidYouGetThisInfo;
                $leisure->financialSupport = $request->financialSupport;
                $leisure->rankClass = $request->rankClass;
                $leisure->average = $request->average;
                $leisure->scholarship = $request->scholarship;
                $leisure->otherRemarks = $request->otherRemarks;
                /*$seniorhighid = SHSStudent::where('studentID', '=',$request->studentID)->get();
                $leisure->SHSStudentID = $seniorhighid[0]->id;*/
                if(isset($request->barelyPassedSubjectsCheck)){
                    $leisure->selfEval1 = '1';
                }else{
                    $leisure->selfEval1 = '0';
                }
                if(isset($request->failedSubjectsCheck)){
                    $leisure->selfEval2 = '1';
                }else{
                    $leisure->selfEval2 = '0';
                }
                if(isset($request->barelyPassedSubjectsCheck)){
                    $leisure->selfEval3 = '1';
                }else{
                    $leisure->selfEval3 = '0';
                }
                if(isset($request->failedSubjectsCheck)){
                    $leisure->selfEval4 = '1';
                }else{
                    $leisure->selfEval4 = '0';
                }
                if(isset($request->dificultySubjectsCheck)){
                    $leisure->selfEval5 = '1';
                }else{
                    $leisure->selfEval5 = '0';
                }
                if(isset($request->confidentCheck)){
                    $leisure->selfEval6 = '1';
                }else{
                    $leisure->selfEval6 = '0';
                }
                $leisure->save();
                activityController::insertActivity('Updated a leisure of student with student id:'. $request->studentID);
                return $this->getLeisureRow($leisure);
            break;
            case 'juniorhigh':
                $leisure = studentLeisureJH::find($request->id);;
                $leisure->schoolChoice = $request->schoolChoice;
                $leisure->howDidYouComeToThisSchool = $request->howDidYouComeToThisSchool;
                $leisure->getInfo = $request->getInfo;
                $leisure->whereDidYouGetThisInfo = $request->whereDidYouGetThisInfo;
                $leisure->financialSupport = $request->financialSupport;
                $leisure->rankClass = $request->rankClass;
                $leisure->average = $request->average;
                $leisure->scholarship = $request->scholarship;
                $leisure->otherRemarks = $request->otherRemarks;
                /*$juniorhighid = JHStudent::where('studentID', '=',$request->studentID)->get();
                $leisure->JHStudentID = $juniorhighid[0]->id;*/
                if(isset($request->barelyPassedSubjectsCheck)){
                    $leisure->selfEval1 = '1';
                }else{
                    $leisure->selfEval1 = '0';
                }
                if(isset($request->failedSubjectsCheck)){
                    $leisure->selfEval2 = '1';
                }else{
                    $leisure->selfEval2 = '0';
                }
                if(isset($request->barelyPassedSubjectsCheck)){
                    $leisure->selfEval3 = '1';
                }else{
                    $leisure->selfEval3 = '0';
                }
                if(isset($request->failedSubjectsCheck)){
                    $leisure->selfEval4 = '1';
                }else{
                    $leisure->selfEval4 = '0';
                }
                if(isset($request->dificultySubjectsCheck)){
                    $leisure->selfEval5 = '1';
                }else{
                    $leisure->selfEval5 = '0';
                }
                if(isset($request->confidentCheck)){
                    $leisure->selfEval6 = '1';
                }else{
                    $leisure->selfEval6 = '0';
                }
                $leisure->save();
                activityController::insertActivity('Updated a leisure of student with student id:'. $request->studentID);
                return $this->getLeisureRow($leisure);
            break;
            case 'elementary':
                $leisure = studentLeisureElem::find($request->id);
                $leisure->schoolChoice = $request->schoolChoice;
                $leisure->howDidYouComeToThisSchool = $request->howDidYouComeToThisSchool;
                $leisure->getInfo = $request->getInfo;
                $leisure->whereDidYouGetThisInfo = $request->whereDidYouGetThisInfo;
                $leisure->financialSupport = $request->financialSupport;
                $leisure->rankClass = $request->rankClass;
                $leisure->average = $request->average;
                $leisure->scholarship = $request->scholarship;
                $leisure->otherRemarks = $request->otherRemarks;
                /*$elemstudentid = elemStudent::where('studentID', '=',$request->studentID)->get();
                $leisure->elemStudentID = $elemstudentid[0]->id;*/
                if(isset($request->barelyPassedSubjectsCheck)){
                    $leisure->selfEval1 = '1';
                }else{
                    $leisure->selfEval1 = '0';
                }
                if(isset($request->failedSubjectsCheck)){
                    $leisure->selfEval2 = '1';
                }else{
                    $leisure->selfEval2 = '0';
                }
                if(isset($request->barelyPassedSubjectsCheck)){
                    $leisure->selfEval3 = '1';
                }else{
                    $leisure->selfEval3 = '0';
                }
                if(isset($request->failedSubjectsCheck)){
                    $leisure->selfEval4 = '1';
                }else{
                    $leisure->selfEval4 = '0';
                }
                if(isset($request->dificultySubjectsCheck)){
                    $leisure->selfEval5 = '1';
                }else{
                    $leisure->selfEval5 = '0';
                }
                if(isset($request->confidentCheck)){
                    $leisure->selfEval6 = '1';
                }else{
                    $leisure->selfEval6 = '0';
                }
                $leisure->save();
                activityController::insertActivity('Updated a leisure of student with student id:'. $request->studentID);
                return $this->getLeisureRow($leisure);
            break;
            default:
                # code...
                break;
        }
    }
    public function studentLeisureDelete($leisureid, $studentid){
        $leisure;
        $studenttype = $this->getStudentType($studentid);
        //error_log("hello");
        //error_log($studenttype);
        switch ($studenttype) {
            case 'college':
                $leisure  = studentLeisureCollege::find($leisureid);
                $leisure->forceDelete();
                activityController::insertActivity('Deleted a leisure of student with student id:'. $studentid);
                return "deleted";
            break;
            case 'seniorhigh':
                $leisure  = studentLeisureSH::find($leisureid);
                $leisure->forceDelete();
                activityController::insertActivity('Deleted a leisure of student with student id:'. $studentid);
                return "deleted";
            break;
            case 'juniorhigh':
                $leisure  = studentLeisureJH::find($leisureid);
                $leisure->forceDelete();
                activityController::insertActivity('Deleted a leisure of student with student id:'. $studentid);
                return "deleted";
            break;
            case 'elementary':
                $leisure  = studentLeisureElem::find($leisureid);
                $leisure->forceDelete();
                activityController::insertActivity('Deleted a leisure of student with student id:'. $studentid);
                return "deleted";
            break;
            default:
                
                break;
        }
       
    }
    public function studentHealthRecordList($studentid){
        $healthrecord = healthRecord::where('studentID', '=', $studentid)->get();
        if(count($healthrecord)==0){
            return "<tr class='noHealthRecordStudent'><td colspan='12'><h2>This student doesnt have a health record yet</h2></td></tr>";
        }
        $html ='';
        foreach ($healthrecord as $healthrecord) {
            $html .=$this->healthRecordRow($healthrecord);
        }
        return $html;
    }
    public function healthRecordRow($healthrecord){
        
        $wearglasses;
        if($healthrecord->wearGlasess =='1'){
            $wearglasses ='Yes';
        }else{
            $wearglasses ='No';
        }
        $createdat =  explode(' ', $healthrecord->created_at)[0];
        return "<tr id ='healthrecord-$healthrecord->id'>
                    <td>$healthrecord->height</td>
                    <td>$healthrecord->weight</td>
                    <td>$healthrecord->complexion</td>
                    <td>$wearglasses</td>
                    <td>$healthrecord->programsParticipated</td>
                    <td>$healthrecord->physicalAilment</td>
                    <td>$healthrecord->live</td>
                    <td>$healthrecord->numPeopleHouse</td>
                    <td>$healthrecord->numPeopleRoom</td>
                    <td>$createdat</td>
                    <td>
                        <button class='btn btn-dark healthRecordEdit' id ='healthRecordEdit-$healthrecord->id'>
                            <i class='fa fa-edit'></i>
                        </button>
                    </td>
                    <td>
                        <button class='btn btn-warning healthRecordDelete' id ='healthRecordDelete-$healthrecord->id'>
                            <i class='fa fa-trash-o'></i>
                        </button>
                    </td>
                </tr>";
    }
    public function insertHealthRecord(Request $request){
        $this->validate($request, array(
            'height'=>'required',
            'weight'=>'required',
            'complexion'=>'required',
            'wearGlasses'=>'required',
            'programsParticipated'=>'required|max:100',
            'physicalAilment'=>'required|max:100',
            'live' => 'required',
            'numPeopleHouse'=>'required',
            'numPeopleRoom'=>'required'
        ));
        $healthrecord =  new healthRecord;
        $healthrecord->height = $request->height;
        $healthrecord->weight = $request->weight;
        $healthrecord->complexion = $request->complexion;
        $wearglasses;
        if($request->wearGlasses =='yes'){
            $wearglasses =1;
        }else{
            $wearglasses =0;
        }
        $healthrecord->wearGlasess = $wearglasses;
        $healthrecord->programsParticipated = $request->programsParticipated;
        $healthrecord->physicalAilment = $request->physicalAilment;
        $healthrecord->live = $request->live;
        $healthrecord->numPeopleHouse = $request->numPeopleHouse;
        $healthrecord->numPeopleRoom = $request->numPeopleRoom;
        $healthrecord->studentID =$request->studentID;
        $healthrecord->save();
        activityController::insertActivity('Inserted new healthrecord of student with student id:'. $request->studentID);
        return $this->healthRecordRow($healthrecord);
    }
    public function updateHealthRecord(Request $request){
        $this->validate($request, array(
            'height'=>'required',
            'weight'=>'required',
            'complexion'=>'required',
            'wearGlasses'=>'required',
            'programsParticipated'=>'required|max:100',
            'physicalAilment'=>'required|max:100',
            'live' => 'required',
            'numPeopleHouse'=>'required',
            'numPeopleRoom'=>'required'
        ));
        $healthrecord =  healthRecord::find($request->id);
        $healthrecord->height = $request->height;
        $healthrecord->weight = $request->weight;
        $healthrecord->complexion = $request->complexion;
        $wearglasses;
        if($request->wearGlasses =='yes'){
            $wearglasses =1;
        }else{
            $wearglasses =0;
        }
        $healthrecord->wearGlasess = $wearglasses;
        $healthrecord->programsParticipated = $request->programsParticipated;
        $healthrecord->physicalAilment = $request->physicalAilment;
        $healthrecord->live = $request->live;
        $healthrecord->numPeopleHouse = $request->numPeopleHouse;
        $healthrecord->numPeopleRoom = $request->numPeopleRoom;
        
        $healthrecord->save();
        activityController::insertActivity('Updated a healthrecord of student with student id:'. $request->studentID);
        return $this->healthRecordRow($healthrecord);
    }
    public function deleteHealthRecord($healthrecordid){
        $healthrecord =healthRecord::find($healthrecordid);
        $healthrecord->forceDelete();
        activityController::insertActivity('Deleted a healthrecord of student with healthrecord id:'. $healthrecordid);
        return "deleted";
    }
    public function getMakeUpAssistance($studentid){
        $makeup = makeUpAssistance::where('studentID', '=',$studentid)->get();
        if(count($makeup)==0){
            return "<h2>This student still doesnt have a makeup assistance</h2>";
        }
        $html = '';
        foreach ($makeup as $makeup) {
            $html .= $this->getMakeUpAssistanceRow($makeup);
        }
        return $html;
    }
    public function getMakeUpAssistanceRow($makeup){

        $personality = personality::find($makeup->personalityID);
        $createdat = explode(" ",$makeup->created_at)[0];
        return "<tr id ='makeUpAssistanceRow-$makeup->id'>
                    <td>$personality->name</td>
                    <td>$createdat</td>
                    <td>
                        <button class='btn btn-dark editMakeUpAssistance' 
                        id ='editMakeUpAssistance-$makeup->id'>
                            <i class='fa fa-edit'></i>
                        </button>
                    </td>
                    <td>
                        <button class='btn btn-warning deleteMakeUpAssistance' 
                        id ='deleteMakeUpAssistance-$makeup->id'>
                            <i class='fa fa-trash-o'></i>
                        </button>
                    </td>
                </tr>";
    }
    public function insertMakeUpAssistance(Request $request){
        // $this->validate($request, array(
           
        //     'significantEvents'=>'required|max:500',
        //     'helpNeeded'=>'required|max:500'
        // ));
        $personality = explode(",", $request->personalities);
        $row = '';
        //error_log($personality[0]);
        foreach ($personality as $person) {
            $makeup = new makeUpAssistance;
            $makeup->schoolYearID = $this->getCurrentSchoolYear();
            if($makeup->schoolYearID == 0){
                return "There is no active schoolyear yet. Please activate one in settings";
            }
            $makeup->studentID = $request->studentID;
            $makeup->personalityID = $person;
            $makeup->significantEvents = $request->significantEvents;
            $makeup->helpNeeded = $request->helpNeeded;
            $makeup->save();
            $row .= $this->getMakeUpAssistanceRow($makeup);
        }
            
        activityController::insertActivity('Inserted a makeupassistance of student with student id:'. $request->studentID);
        return $row;
    }
    public function getMakeUpData($makeupid){
        $makeup = makeUpAssistance::find($makeupid);
        return $makeup;
    }
    public function updateMakeUpAssistance(Request $request){
        $this->validate($request, array(
            'radioPersonality'=>'required'
        ));
        $makeup =  makeUpAssistance::find($request->id);
        $makeup->schoolYearID = $this->getCurrentSchoolYear();
        if($makeup->schoolYearID == 0){
            return "There is no active schoolyear yet. Please activate one in settings";
        }
       
        $makeup->personalityID = $request->radioPersonality;
        $makeup->significantEvents = $request->significantEvents;
        $makeup->helpNeeded = $request->helpNeeded;
        $makeup->save();
        activityController::insertActivity('Updated a makeupassistance of student with student id:'. $request->studentID);
        return $this->getMakeUpAssistanceRow($makeup);
    }
    public function deleteMakeUp($makeupid){
        $makeup = makeUpAssistance::find($makeupid);
        activityController::insertActivity('Deleted a makeupassistance of student with makeupassistance id:'. $makeupid);
        $makeup->forceDelete();
    }
    public function getStudentTypeToUpgrade($studentid){
        $studenttype = $this->getStudentType($studentid);
        switch ($studenttype) {
            case 'college':
                $temp = collegeStudent::where('studentID', '=', $studentid)
                                        ->where("id", '=', DB::raw("(SELECT
                                        max(id) AS maxid FROM college_students
                                        WHERE studentID = '$studentid')"))
                                        ->get();
                $course = course::find($temp[0]->courseID);
                if($course->courseYear == $temp[0]->yearLevel){
                    return "college-".$temp[0]->yearLevel."-graduating";
                }else{
                    return "college-".$temp[0]->yearLevel."-0";
                }
            break;
            case 'seniorhigh':
                $temp = SHSStudent::where('studentID', '=', $studentid)
                ->where("id", '=', DB::raw("(SELECT
                max(id) AS maxid FROM s_h_s_students
                WHERE studentID = '$studentid')"))
                ->get();
                return "seniorhigh-".$temp[0]->gradeLevel."-0";
            break;
            case 'juniorhigh':
                $temp = JHStudent::where('studentID', '=', $studentid)
                ->where("id", '=', DB::raw("(SELECT
                max(id) AS maxid FROM j_h_students
                WHERE studentID = '$studentid')"))
                ->get();
                return "juniorhigh-".$temp[0]->gradeLevel."-0";
            break;
            case 'elementary':
                $temp = elemStudent::where('studentID', '=', $studentid)
                ->where("id", '=', DB::raw("(SELECT
                max(id) AS maxid FROM elem_students
                WHERE studentID = '$studentid')"))
                ->get();
                return "elementary-".$temp[0]->gradeLevel."-0";
            break;
            default:
                
                break;
        }
    }
    public function upgradeStudent($studentid){
        $studentupgrade = $this->getStudentTypeToUpgrade($studentid);
        switch (explode("-", $studentupgrade)[0]) {
            case 'college':
                if(explode("-", $studentupgrade)[2]!='graduating'){
                    $temp = collegeStudent::where('studentID', '=', $studentid)
                                        ->where("id", '=', DB::raw("(SELECT
                                        max(id) AS maxid FROM college_students
                                        WHERE studentID = '$studentid')"))
                                        ->get();
                    $college = new collegeStudent;
                   
                    $college->studentID =$studentid;
                    $college->studentNo = $temp[0]->studentNo;
                    $college->yearLevel =  $temp[0]->yearLevel + 1;
                    $college->dateEnrolled = $temp[0]->dateEnrolled;
                    $college->courseID =$temp[0]->courseID;
                    $college->semesterID = $this->getCurrentSem();
                    $college->schoolYearID= $this->getCurrentSchoolYear();
                    $college->organizationID=$temp[0]->organizationID;
                    $college->save();
                }else{
                    $temp = collegeStudent::where('studentID', '=', $studentid)
                    ->where("id", '=', DB::raw("(SELECT
                    max(id) AS maxid FROM college_students
                    WHERE studentID = '$studentid')"))
                    ->get();
                    $college = new collegeStudent;

                    $college->studentID =$studentid;
                    $college->studentNo = $temp[0]->studentNo;
                    $college->yearLevel =  $temp[0]->yearLevel;
                    $college->dateEnrolled = $temp[0]->dateEnrolled;
                    $college->courseID =$temp[0]->courseID;
                    $college->semesterID = $this->getCurrentSem();
                    $college->schoolYearID= $this->getCurrentSchoolYear();
                    $college->organizationID=$temp[0]->organizationID;
                    $college->save();
                }
            break;
            case 'seniorhigh':
                $temp = SHSStudent::where('studentID', '=', $studentid)
                            ->where("id", '=', DB::raw("(SELECT
                            max(id) AS maxid FROM s_h_s_students
                            WHERE studentID = '$studentid')"))
                            ->get();
                $college = new SHSStudent;
                $college->studentID =$studentid;
                $college->studNo = $temp[0]->studNo;
                $college->schoolYearID= $this->getCurrentSchoolYear();
                $college->semesterID= $this->getCurrentSem();
                $college->seniorHighStrandID = $temp[0]->seniorHighStrandID;
                $college->gradeLevel = $temp[0]->gradeLevel + 1;
                $college->save();
            break;
            case 'juniorhigh':
                $temp = JHStudent::where('studentID', '=', $studentid)
                ->where("id", '=', DB::raw("(SELECT
                max(id) AS maxid FROM j_h_students
                WHERE studentID = '$studentid')"))
                ->get();
                $college = new JHStudent;
                $college->studentID = $studentid;
                $college->studNo = $temp[0]->studNo;
                $college->schoolYearID= $this->getCurrentSchoolYear();
                $college->gradeLevel= $temp[0]->gradeLevel + 1;
                $college->save();
            break;
            case 'elementary':
                $temp = elemStudent::where('studentID', '=', $studentid)
                ->where("id", '=', DB::raw("(SELECT
                max(id) AS maxid FROM elem_students
                WHERE studentID = '$studentid')"))
                ->get();
                if($temp[0]->gradeLevel!='6'){
                    $college = new elemStudent;
                    $college->studentID = $studentid;
                    $college->studNo = $temp[0]->studNo;
                    $college->schoolYearID= $this->getCurrentSchoolYear();
                    $college->gradeLevel= $temp[0]->gradeLevel + 1;
                    $college->save();
                }else{
                    $college = new JHStudent;
                    $college->studentID = $studentid;
                    $college->studNo = $temp[0]->studNo;
                    $college->schoolYearID= $this->getCurrentSchoolYear();
                    $college->gradeLevel= 7;
                    $college->save();
                }
            break;
            
            default:
                # code...
                break;
        }
        return redirect()->route('student.profile', $studentid);
    }
    public function upgradeStudentGroup($studentid){
        $studenttype = $this->getStudentType($studentid);
        $student = student::find($studentid);
        $studentname = $student->lastName.", ".$student->firstName;
        $temp = new \stdClass;
        switch ($studenttype) {
            case 'college':
                $collegetemp = collegeStudent::join('courses', 'courses.id', '=', 'college_students.courseID')
                                        ->select('courses.courseYear', 'college_students.*')
                                        ->where('college_students.studentID', '=', $studentid)
                                        ->where('college_students.id', '=', DB::raw("(SELECT 
                                        MAX(id) AS maxid FROM college_students WHERE studentID = '$studentid')"))
                                        ->get();
                if(count($collegetemp)>0){
                    if($collegetemp[0]->courseYear==$collegetemp[0]->yearLevel){
                        // $temp->status = 'failedWithRemarks';
                        // $temp->remarks = "$studentname is already graduating college and cannot be upgraded.";
                        // return json_encode($temp);
                        $collegeStudent =  new collegeStudent;
                        $collegeStudent->studentID =$studentid;
                        $collegeStudent->studentNo = $collegetemp[0]->studentNo;
                        $collegeStudent->yearLevel =  $collegetemp[0]->yearLevel;
                        $collegeStudent->dateEnrolled = $collegetemp[0]->dateEnrolled;
                        $collegeStudent->courseID =$collegetemp[0]->courseID;
                        $collegeStudent->semesterID = $this->getCurrentSem();
                        $collegeStudent->schoolYearID= $this->getCurrentSchoolYear();
                        $collegeStudent->organizationID=$collegetemp[0]->organizationID;
                        $collegeStudent->save();
                        $temp->status = 'success';
                        $temp->remarks = "$studentname is enrolled to current school year and sem with Year Level: ".($collegetemp[0]->yearLevel);
                        return json_encode($temp);
                    }else{
                        $collegeStudent =  new collegeStudent;
                        $collegeStudent->studentID =$studentid;
                        $collegeStudent->studentNo = $collegetemp[0]->studentNo;
                        $collegeStudent->yearLevel =  $collegetemp[0]->yearLevel + 1;
                        $collegeStudent->dateEnrolled = $collegetemp[0]->dateEnrolled;
                        $collegeStudent->courseID =$collegetemp[0]->courseID;
                        $collegeStudent->semesterID = $this->getCurrentSem();
                        $collegeStudent->schoolYearID= $this->getCurrentSchoolYear();
                        $collegeStudent->organizationID=$collegetemp[0]->organizationID;
                        $collegeStudent->save();
                        $temp->status = 'success';
                        $temp->remarks = "$studentname is upgraded to Year Level: ".($collegetemp[0]->yearLevel + 1);
                        return json_encode($temp);
                    }
                }else{
                    //error_log($studentid);
                    $temp->status= 'failed';
                    return json_encode($temp);
                }
            break;
            case 'seniorhigh':
                $seniorHighTemp  = SHSStudent::where('studentID', '=', $studentid)
                                    ->where('id', '=', DB::raw("(SELECT
                                    MAX(id) as maxid FROM s_h_s_students WHERE
                                    studentID = '$studentid')"))
                                    ->get();
                if($seniorHighTemp[0]->gradeLevel == '12'){
                    $temp->status = "failedWithRemarks";
                    $temp->remarks = "$studentname is graduating in Senior High and needs to be upgraded individually to select a course for college.";
                    return json_encode($temp);
                }else{
                    $student = new SHSStudent;
                    $student->studentID =$studentid;
                    $student->studNo = $seniorHighTemp[0]->studNo;
                    $student->schoolYearID= $this->getCurrentSchoolYear();
                    $student->semesterID= $this->getCurrentSem();
                    $student->seniorHighStrandID = $seniorHighTemp[0]->seniorHighStrandID;
                    $student->gradeLevel = $seniorHighTemp[0]->gradeLevel + 1;
                    $student->save();
                    $temp->status = "success";
                    $temp->remarks = "$studentname is upgraded to Senior High, grade: 12";
                    return json_encode($temp);
                }
            break;
            case 'juniorhigh':
                $juniorhightemp = JHStudent::where('studentID', '=',  $studentid)
                                            ->where('id', '=', DB::raw("(SELECT
                                            max(id) AS maxid FROM j_h_students
                                            WHERE studentID = '$studentid')"))
                                            ->get();
                if($juniorhightemp[0]->gradeLevel == '10'){
                    $temp->status = "failedWithRemarks";
                    $temp->remarks = "$studentname is graduating in Junior High and needs to be upgraded individually to select a strand for Senior High.";
                    return json_encode($temp);
                }else{
                    $student = new JHStudent;
                    $student->studentID = $studentid;
                    $student->studNo = $juniorhightemp[0]->studNo;
                    $student->schoolYearID= $this->getCurrentSchoolYear();
                    $student->gradeLevel= $juniorhightemp[0]->gradeLevel + 1;
                    $student->save();
                    $temp->status = "success";
                    $temp->remarks = "$studentname is upgraded to Junior High, grade: ".($juniorhightemp[0]->gradeLevel + 1);
                    return json_encode($temp);
                }
            break;
            case 'elementary':
                $elemantaryTemp = elemStudent::where('studentID', '=', $studentid)
                                            ->where('id', '=', DB::raw("(SELECT
                                            MAX(id) AS maxid FROM elemStudents
                                            WHERE studentID = '$studentid')"))
                                            ->get();
                if($elementaryTemp[0]->gradeLevel == '6'){
                    $student = new JHStudent;
                    $student->studentID = $studentid;
                    $student->studNo = $elemantaryTemp[0]->studNo;
                    $student->schoolYearID= $this->getCurrentSchoolYear();
                    $student->gradeLevel= $elemantaryTemp[0]->gradeLevel + 1;
                    $student->save();
                    $temp->status = "success";
                    $temp->remarks = "$studentname is upgraded to Junior High, grade: 7";
                    return json_encode($temp);
                }else{
                    $college = new elemStudent;
                    $college->studentID = $studentid;
                    $college->studNo = $elementaryTemp[0]->studNo;
                    $college->schoolYearID= $this->getCurrentSchoolYear();
                    $college->gradeLevel= $elementaryTemp[0]->gradeLevel + 1;
                    $college->save();
                    $temp->status = "success";
                    $temp->remarks = "$studentname is upgraded to Junior High, grade: ".($elementaryTemp[0]->gradeLevel + 1);
                    return json_encode($temp);
                }
            break;
            default:
                
            break;
        }
    }
    public function upgradeStudentSeniorHigh($studentid, $strandid){
        $temp =  JHStudent::where('studentID', '=', $studentid)
        ->where("id", '=', DB::raw("(SELECT
        max(id) AS maxid FROM j_h_students
        WHERE studentID = '$studentid')"))->get();;
        $college = new SHSStudent;
        $college->studentID =$studentid;
        $college->studNo = $temp[0]->studNo;
        $college->schoolYearID= $this->getCurrentSchoolYear();
        $college->semesterID= $this->getCurrentSem();
        $college->seniorHighStrandID = $strandid;
        $college->gradeLevel = 11;
        $college->save();
        return redirect()->route('student.profile', $studentid);
    }
    public function upgradeStudentCollege($studentid, $courseid){
        $temp = SHSStudent::where('studentID', '=', $studentid)
        ->where("id", '=', DB::raw("(SELECT
        max(id) AS maxid FROM s_h_s_students
        WHERE studentID = '$studentid')"))
        ->get();
        $college = new collegeStudent;

        $college->studentID =$studentid;
        $college->studentNo = $temp[0]->studNo;
        $college->yearLevel =  1;
        $college->dateEnrolled = $temp[0]->dateEnrolled;
        $college->courseID =$courseid;
        $college->semesterID = $this->getCurrentSem();
        $college->schoolYearID= $this->getCurrentSchoolYear();
        $college->organizationID=$this->getNoOrganizationID();
        $college->save();
        return redirect()->route('student.profile', $studentid);
    }
    public function printStudents($viewby, $coursegrade, $gradelevel, $schoolyear, $semester, $search){
        $user = new userController();
        $systemdate = systemDate::all();
        $sql='';
        if(session()->get('user')['userType']=='admin'){
            $sql = "SELECT COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) 
                    semesterID, COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) 
                    schoolYearID, COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) studentType, 
                    COALESCE(col.yearLevel, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) courseGrade, 
                    COALESCE(col.studentNo, shs.studNo, jhs.studNo, elem.studNo) studentNo,
                    COALESCE(col.courseID, shs.seniorHighStrandID) strandCourse,
                    students.*,
                    col.courseName, col.yearLevel,
                    shs.strandName
                    FROM students LEFT JOIN (SELECT college_students.studentID, college_students.yearLevel, college_students.semesterID, college_students.schoolYearID, college_students.id, 'college' AS studentType, 
                    college_students.courseID, courses.courseName, college_students.studentNo FROM college_students INNER JOIN (SELECT max(id) as maxid FROM college_students GROUP 
                    BY studentID) cs ON cs.maxid = college_students.id
                    INNER JOIN courses ON courses.id = college_students.courseID) col ON col.studentID = students.id LEFT 
                    JOIN (SELECT s_h_s_students.studentID, s_h_s_students.semesterID, s_h_s_students.schoolYearID, s_h_s_students.id, 'seniorhigh' AS studentType, s_h_s_students.gradeLevel, s_h_s_students.studNo,  senior_high_strands.strandName, s_h_s_students.seniorHighStrandID
                    FROM s_h_s_students INNER JOIN (SELECT max(id) as maxid FROM s_h_s_students GROUP BY studentID) 
                    shs ON shs.maxid = s_h_s_students.id INNER JOIN senior_high_strands ON senior_high_strands.id = s_h_s_students.seniorHighStrandID) shs ON shs.studentID = students.id
                    LEFT JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'juniorhigh' AS 
                    studentType, gradeLevel, studNo  FROM j_h_students INNER JOIN (SELECT max(id) as maxid FROM j_h_students 
                    GROUP BY studentID) jhs ON jhs.maxid = j_h_students.id) jhs ON jhs.studentID = students.id LEFT 
                    JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'elementary' AS studentType, 
                    gradeLevel, studNo FROM elem_students INNER JOIN (SELECT max(id) as maxid FROM elem_students GROUP 
                    BY studentID) elem ON elem.maxid = elem_students.id) elem ON elem.studentID = students.id
                    
                    LEFT JOIN school_b_gs ON school_b_gs.studentID = students.id
                    LEFT JOIN 
                    ( SELECT fav_subject_lists.studentID, fav_subject_lists.subjectID, subj.subjectName 
                    FROM fav_subject_lists INNER JOIN subjects subj ON subj.id = fav_subject_lists.subjectID) 
                    favsubject ON favsubject.studentID =students.id
                    LEFT JOIN 
                    (SELECT disliked_subjects.studentID, disliked_subjects.subjectID, subj1.subjectName 
                    FROM disliked_subjects INNER JOIN subjects subj1 ON subj1.id = disliked_subjects.subjectID)
                    dislikedsubject ON dislikedsubject.studentID =students.id
                    LEFT JOIN  
                    (SELECT make_up_assistances.studentID, make_up_assistances.personalityID, personalities.name 
                    FROM make_up_assistances INNER JOIN personalities ON  make_up_assistances.personalityID = personalities.id) 
                    makeup ON makeup.studentID = students.id
            WHERE (students.firstName LIKE '%$search%' OR students.lastName LIKE '%$search%' OR 
                    school_b_gs.schoolName LIKE '%$search%' OR favsubject.subjectName LIKE '%$search%' 
                    OR dislikedsubject.subjectName LIKE '%$search%'
                          OR makeup.name LIKE '%$search%'
                          OR students.gender = '$search'
                          OR shs.strandName LIKE '%$search%') 
            AND DATE(students.created_at) <= '".$systemdate[0]->systemDate."'";
            switch ($viewby) {
                case 'college':
                    $sql .= " AND COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) = 'college'";
                break;
                case 'seniorhigh':
                    $sql .= " AND COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) = 'seniorhigh'";
                break;
                case 'juniorhigh':
                    $sql .= " AND COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) = 'juniorhigh'";
                break;
                case 'elementary':
                    $sql .= " AND COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) = 'elementary'";
                break;
                default:
                   
                break;
            }
            if($schoolyear!="all"){
                $sql .= " AND COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) = '$schoolyear'";
            }
            if($semester!="all"){
                $sql .= " AND COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) = '$semester'";
            }
            if($gradelevel!="all"){
                $sql .= " AND COALESCE(col.yearLevel, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) = '$gradelevel'";
            }
            if($coursegrade != 'all'){
                $sql .= " AND COALESCE(col.courseID, shs.seniorHighStrandID) = '$coursegrade'";
            }
        }else{
            $designation = $user->getUserDesignation(Auth::guard('myuser')->id());
            $sql = "SELECT COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) 
                    semesterID, COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) 
                    schoolYearID, COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) studentType, 
                    COALESCE(col.yearLevel, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) courseGrade, 
                    COALESCE(col.studentNo, shs.studNo, jhs.studNo, elem.studNo) studentNo,
                    COALESCE(col.courseID, shs.seniorHighStrandID) strandCourse,
                    students.*,
                    col.courseName, col.yearLevel,
                    shs.strandName
                    FROM students LEFT JOIN (SELECT college_students.studentID, college_students.yearLevel, college_students.semesterID, college_students.schoolYearID, college_students.id, 'college' AS studentType, 
                    college_students.courseID, courses.courseName, college_students.studentNo FROM college_students INNER JOIN (SELECT max(id) as maxid FROM college_students GROUP 
                    BY studentID) cs ON cs.maxid = college_students.id
                    INNER JOIN courses ON courses.id = college_students.courseID) col ON col.studentID = students.id LEFT 
                    JOIN (SELECT s_h_s_students.studentID, s_h_s_students.semesterID, s_h_s_students.schoolYearID, s_h_s_students.id, 'seniorhigh' AS studentType, s_h_s_students.gradeLevel, s_h_s_students.studNo,  senior_high_strands.strandName, s_h_s_students.seniorHighStrandID
                    FROM s_h_s_students INNER JOIN (SELECT max(id) as maxid FROM s_h_s_students GROUP BY studentID) 
                    shs ON shs.maxid = s_h_s_students.id INNER JOIN senior_high_strands ON senior_high_strands.id = s_h_s_students.seniorHighStrandID) shs ON shs.studentID = students.id
                    LEFT JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'juniorhigh' AS 
                    studentType, gradeLevel, studNo  FROM j_h_students INNER JOIN (SELECT max(id) as maxid FROM j_h_students 
                    GROUP BY studentID) jhs ON jhs.maxid = j_h_students.id) jhs ON jhs.studentID = students.id LEFT 
                    JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'elementary' AS studentType, 
                    gradeLevel, studNo FROM elem_students INNER JOIN (SELECT max(id) as maxid FROM elem_students GROUP 
                    BY studentID) elem ON elem.maxid = elem_students.id) elem ON elem.studentID = students.id
                    
                    LEFT JOIN school_b_gs ON school_b_gs.studentID = students.id
                    LEFT JOIN 
                    ( SELECT fav_subject_lists.studentID, fav_subject_lists.subjectID, subj.subjectName 
                    FROM fav_subject_lists INNER JOIN subjects subj ON subj.id = fav_subject_lists.subjectID) 
                    favsubject ON favsubject.studentID =students.id
                    LEFT JOIN 
                    (SELECT disliked_subjects.studentID, disliked_subjects.subjectID, subj1.subjectName 
                    FROM disliked_subjects INNER JOIN subjects subj1 ON subj1.id = disliked_subjects.subjectID)
                    dislikedsubject ON dislikedsubject.studentID =students.id
                    LEFT JOIN  
                    (SELECT make_up_assistances.studentID, make_up_assistances.personalityID, personalities.name 
                    FROM make_up_assistances INNER JOIN personalities ON  make_up_assistances.personalityID = personalities.id) 
                    makeup ON makeup.studentID = students.id
                    WHERE (students.firstName LIKE '%$search%' OR students.lastName LIKE '%$search%' OR 
                    school_b_gs.schoolName LIKE '%$search%' OR favsubject.subjectName LIKE '%$search%' 
                    OR dislikedsubject.subjectName LIKE '%$search%'
                          OR makeup.name LIKE '%$search%'
                          OR students.gender = '$search') 
                    AND DATE(students.created_at) <= '".$systemdate[0]->systemDate."'";
            switch($designation[0]){
                case 'college':
                    $sql= "SELECT cs2.courseID AS courseGrade, c2.courseName, cs2.schoolYearID, cs2.studentNo, 
                    cs2.yearLevel, students.*, 'college' AS studentType, cs2.semesterID FROM students 
                    INNER JOIN (SELECT studentID, studentNo,
                    id, courseID, schoolYearID, yearLevel, semesterID FROM college_students  INNER JOIN (SELECT max(id) as maxid 
                    FROM college_students 
                    GROUP BY studentID) cs ON cs.maxid = college_students.id) cs2 ON cs2.studentID = students.id 
                    INNER JOIN (SELECT c.id AS courseID, dep.departmentID, c.courseName FROM courses c INNER JOIN (SELECT
                    id, courseID FROM course_department_hists cd INNER JOIN (SELECT max(id) as maxid FROM 
                    course_department_hists GROUP BY courseID) cd2 ON cd2.maxid = cd.id) cdh ON cdh.courseID = c.id
                    INNER JOIN (SELECT departmentID, id FROM course_department_hists INNER JOIN (SELECT
                    max(id) AS maxid FROM course_department_hists GROUP BY courseID) cdh1 ON cdh1.maxid = 
                    course_department_hists.id) dep ON dep.id =cdh.id) c2 ON c2.courseID = cs2.courseID 
                    INNER JOIN (SELECT departmentID, id, collegeID FROM department_college_hists
                    INNER JOIN (SELECT max(id) as maxid FROM department_college_hists GROUP BY departmentID) 
                    depc ON depc.maxid = department_college_hists.id) depc2 ON depc2.departmentID=c2.departmentID 
                    INNER JOIN (SELECT collegeID, userID, id FROM assignment_colleges INNER JOIN (SELECT max(id) 
                    as maxid FROM assignment_colleges GROUP BY userID) assc ON assc.maxid = assignment_colleges.id) 
                    assc2 ON assc2.collegeID = depc2.collegeID 

                    LEFT JOIN school_b_gs ON school_b_gs.studentID = students.id
                    LEFT JOIN 
                    ( SELECT fav_subject_lists.studentID, fav_subject_lists.subjectID, subj.subjectName 
                    FROM fav_subject_lists INNER JOIN subjects subj ON subj.id = fav_subject_lists.subjectID) 
                    favsubject ON favsubject.studentID =students.id
                    LEFT JOIN 
                    (SELECT disliked_subjects.studentID, disliked_subjects.subjectID, subj1.subjectName 
                    FROM disliked_subjects INNER JOIN subjects subj1 ON subj1.id = disliked_subjects.subjectID)
                     dislikedsubject ON dislikedsubject.studentID =students.id
                    LEFT JOIN  
                    (SELECT make_up_assistances.studentID, make_up_assistances.personalityID, personalities.name 
                    FROM make_up_assistances INNER JOIN personalities ON  make_up_assistances.personalityID = personalities.id) 
                    makeup ON makeup.studentID = students.id

                    WHERE assc2.userID ='".Auth::guard('myuser')->id()."'
                    AND DATE(students.created_at) <= '".$systemdate[0]->systemDate."'
                    AND (students.firstName LIKE '%$search%' OR students.lastName LIKE '%$search%' OR 
                    school_b_gs.schoolName LIKE '%$search%' OR favsubject.subjectName LIKE '%$search%' 
                    OR dislikedsubject.subjectName LIKE '%$search%'
                          OR makeup.name LIKE '%$search%'
                          OR students.gender = '$search'
                          OR shs.strandName LIKE '%$search%') ";
                    if($schoolyear != "all"){
                        $sql .= " AND cs2.schoolYearID = '$schoolyear'";
                    }
                    if($semester != "all"){
                        $sql .= " AND cs2.semesterID = '$semester'";
                    }
                    if($coursegrade != "all"){
                        $sql .= " AND cs2.courseID = '$coursegrade'";
                    }
                    if($gradelevel != 'all'){
                        $sql .= " AND  cs2.yearLevel = '$gradelevel'";
                    }
                break;
                case 'seniorhigh':
                    $sql .= " AND COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) = 'seniorhigh'
                              AND COALESCE(col.yearLevel, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) = '".$designation[1]."'";
                     if($schoolyear != "all"){
                        $sql .= " AND COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) = '$schoolyear'";
                    }
                    if($semester != "all"){
                        $sql .= " AND COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) = '$semester'";
                    }
                    
                break;
                case 'juniorhigh':
                    $sql .= " AND COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) = 'juniorhigh'
                                AND COALESCE(col.yearLevel, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) = '".$designation[1]."'";
                    if($schoolyear != "all"){
                        $sql .= " AND COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) = '$schoolyear'";
                    }
                    if($semester != "all"){
                        $sql .= " AND COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) = '$semester'";
                    }
                    if($gradelevel != 'all'){
                        
                    }
                break;
                case 'elementary':
                    $sql .= " AND COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) = 'elementary'
                                AND COALESCE(col.yearLevel, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) = '".$designation[1]."'";
                    if($schoolyear != "all"){
                        $sql .= " AND COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) = '$schoolyear'";
                    }
                    if($semester != "all"){
                        $sql .= " AND COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) = '$semester'";
                    }
                   
                break;
            } 
        }
        $sql .= " ORDER BY students.lastName ASC";
        $students = DB::select(DB::raw($sql));
        if(count($students)==0){
            return "<tr><h2>There are no students found.</h2></tr>";
        }else{
            $html = '';
            $this->rowCounter=0;
            foreach ($students as $student) {
                $this->rowCounter++;
                $gradeLevel = '';
                if($student->courseName == ''){
                    $gradeLevel = $student->courseGrade;
                }else{
                    $gradeLevel = $student->yearLevel;
                }
                $thiscourse = '';
                if($student->courseName == ''){
                    $thiscourse = $student->strandName;
                }else{
                    $thiscourse =$student->courseName;
                }
                $html .= "<tr id ='students-$student->id' class= 'rowClickable'>
                            <td>$this->rowCounter</td>
                            <td>".$this->getBeautifiedName($student->firstName, $student->middleName, $student->lastName)."</td>
                            <td>$student->studentNo</td>
                            <td>".$this->getBeautifiedStudentType($student->studentType)."</td>
                            <td>$thiscourse</td>
                            <td>$gradeLevel</td>
                        </tr>";
            }
            return $html;
        }    
    }
    public function getBeautifiedGradeLevel($yearlevel){
        $level = '';
        switch ($yearlevel) {
            case '1':
                $level .= $yearlevel."st";
            break;
            case '2':
                $level .= $yearlevel."nd";
            break;
            case '3':
                $level .= $yearlevel."rd";
            break;
            case '4':
                $level .= $yearlevel."th";
            break;
            case '5':
                $level .= $yearlevel."th";
            break;
            case '6':
                $level .= $yearlevel."th";
            break;
            case '7':
                $level .= $yearlevel."th";
            break;
            case '8':
                $level .= $yearlevel."th";
            break;
            case '9':
                $level .= $yearlevel."th";
            break;
            default:
                $level .= $yearlevel."th";
                break;
        }
        return $level;
    }
    public function printStudents2($studenttype, $courseGradeLevel, $gradelevel, $schoolYear, $semester, $search = ''){
        $coursename='all';
        $myschoolyhear= 'all';
        $mygradelevel = 'all';
        if($schoolYear!='all'){
            $myschoolyhear = schoolYear::find($schoolYear);
            $myschoolyhear = explode('-', $myschoolyhear->schoolYearStart)[0] ." - ". explode('-', $myschoolyhear->schoolYearEnd)[0];
        }
      
        //error_log($schoolYear);
        if($studenttype=='college' && $courseGradeLevel != 'all'){
            $coursename = course::find($courseGradeLevel)->courseName;
            if($gradelevel!='all'){
                $mygradelevel = 'Year: '.$this->getBeautifiedGradeLevel($gradelevel);
            }
        }else{
            if($studenttype=='seniorhigh' && $courseGradeLevel !='all'){
                $coursename = seniorHighStrand::find($courseGradeLevel)->strandName;
                if($gradelevel!='all'){
                    $mygradelevel = 'Grade: '.$this->getBeautifiedGradeLevel($gradelevel);
                }
            }elseif($studenttype != 'all' && $studenttype != 'seniorhigh' && $gradelevel != 'all'){
                $coursename='Grade: '.$courseGradeLevel;
            }
        } 
        $thissemester = '';
        switch ($semester) {
            case '1':
                $thissemester = '1st';
            break;
            case '2':
                $thissemester = '2nd';
            break;
            case '3':
                $thissemester = '3rd';
            break;
            default:
                # code...
                break;
        }
        $html = "
            <html>
                <head>
                    <style>
                        table {
                            font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif;
                            border-collapse: collapse;
                            width: 100%;
                        }
                        
                        table td, table th {
                            border: 1px solid #ddd;
                            padding: 8px;
                        }
                        
                        table tr:nth-child(even){background-color: #f2f2f2;}
                        
                        table tr:hover {background-color: #ddd;}
                        
                        table th {
                            padding-top: 12px;
                            padding-bottom: 12px;
                            text-align: left;
                            background-color: #4CAF50;
                            color: white;
                        }
                        .filterContainer{
                            display:inline-block;
                            vertical-align:top;
                            width:19%;
                        }
                        @page{
                            margin-left:20px;
                            margin-right:20px;
                            margin-top:160px;
                        }
                    </style>
                </head>
                <body>
                    ".$this->getAdminSetting()."
                    <div style = 'margin-bottom:20px; margin-top:20px;'>
                        <center><h2 style ='text-decoration: underline;'>List Of Students</h2></center>
                    </div>
                    <div>
                        
                            <div style = 'width:500px; margin: 0 auto;'>
                                <div class='filterContainer'>
                                    <label>Student Type</label>
                                    <h3>".ucfirst($studenttype)."</h3>
                                </div>
                                <div class='filterContainer'>
                                    <label>Course/<br>Strand</label>
                                    <h3>".ucfirst($coursename)."</h3>
                                </div>
                                <div class='filterContainer'>
                                    <label>Grade Level</label>
                                    <h3>".ucfirst($mygradelevel)."</h3>
                                </div>
                                <div class='filterContainer'>
                                    <label>School Year</label>
                                    <h3>".ucfirst($myschoolyhear)."</h3>
                                </div>
                                <div class='filterContainer'>
                                    <label>Semester</label>
                                    <h3>".ucfirst($semester)."</h3>
                                </div>
                            </div>
                        
                    </div>
                    <table>
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Name</th>
                                <th>Student No.</th>
                                <th>Student Type</th>
                                <th>Course/Strand</th>
                                <th>Year Level</th>
                            </tr>
                        </thead>
                        <tbody> 
        ";
        $html .= $this->printStudents($studenttype, $courseGradeLevel, $gradelevel, $schoolYear, $semester, $search);
        $html.="</tbody>
            </table>
            </body>
            </html>";
        PDF::loadHTML($html)->save(public_path("temp/".'report.pdf'));
        //return redirect('temp/companylogo.png');
        return "true";
    }
    public function getAdminSetting(){
        $temp = new \stdClass();
        $count = adminsetting::all();
        if(count($count)>0){
            $adminsetting = adminsetting::find($count[0]->id);
            $html = "<header style = 'position:fixed; top:-100px; left:0px; right:0px; height: 60px;'>
                        <div style = 'width:19%; display:inline-block;  padding: 0px 0px 0px 50px;'>
                            <img style = 'height:100px; width:100px; border-radius:50px;' src = 'images-database/adminsetting/schoolimage/$adminsetting->id.$adminsetting->schoolSealImageType'>
                        </div>
                        <div style = 'width:50%; height:120px; display:inline-block;  margin:0px;'>
                            <center>
                                <h4 class='header' style='color:black; margin:0px;'>$adminsetting->schoolName</h4>
                                <h4 class='header' style='color:black; margin:0px;'>$adminsetting->guidanceName</h4>
                                <h4 class='header' style='color:black; margin:0px;'>$adminsetting->streetAddress, $adminsetting->barangay</h4>
                                <h4 class='header' style='color:black; margin:0px;'>$adminsetting->city</h4>
                                <h4 class='header' style='color:black; margin:0px;'>Philippines, 7000</h4>
                            </center>    
                        </div>
                        <div style = 'width:15%; display:inline-block; padding: 0px 50px 0px 0px;  text-align: right;'>
                            <img style = 'height:100px; width:100px; border-radius:50px;' src = 'images-database/adminsetting/guidanceimage/$adminsetting->id.$adminsetting->guidanceSealImageType'>
                        </div>
                    </header>";
            return $html;
        }else{
            return "";
        }
    }
    public function numberStudentRecord($studentid){
        $count =0;
        $student = collegeStudent::where('studentID', '=', $studentid)->get();
        $count += count($student);
        $student = SHSStudent::where('studentID', '=', $studentid)->get();
        $count += count($student);
        $student = JHStudent::where('studentID', '=', $studentid)->get();
        $count += count($student);
        $student = elemStudent::where('studentID', '=', $studentid)->get();
        $count += count($student);
        if($count == 1){
            return "false";
        }else{
            return "true";
        }
    }
    public function deleteStudentRecord($studentid){
        $studenttype = $this->getStudentType($studentid);
        $student;
        switch ($studenttype) {
            case 'college':
                $student = collegeStudent::where('studentID', '=', $studentid)
                                            ->where('id', '=', 
                                            DB::raw("(SELECT MAX(id) FROM college_students WHERE studentID = '".$studentid."')"))
                                            ->get();
            break;
            case 'seniorhigh':
                $student = SHSStudent::where('studentID', '=', $studentid)  
                                        ->where('id', '=', 
                                        DB::raw("(SELECT MAX(id) FROM s_h_s_students WHERE studentID = '".$studentid."')"))
                                        ->get();
            break;
            case 'juniorhigh':
                $student = JHStudent::where('studentID', '=', $studentid)
                                        ->where('id', '=', 
                                        DB::raw("(SELECT MAX(id) FROM j_h_students WHERE studentID = '".$studentid."')"))
                                        ->get();
            break;
            case 'elementary':
                $student = elemStudent::where('studentID', '=', $studentid)  
                                        ->where('id', '=', 
                                        DB::raw("(SELECT MAX(id) FROM elem_students WHERE studentID = '".$studentid."')"))
                                        ->get();
            break;
            default:
                # code...
                break;
        }
        //error_log($studenttype);
       
        try {
            $student[0]->forceDelete();
            
        }
        catch (\Exception $e) {
            return redirect()->route('student.profile', ['profileid' =>  $studentid]);
        }
        return redirect()->route('student.profile', ['profileid' =>  $studentid]);
    }
    public function getFamilyData($familyid){
        $family = family::find($familyid);
        return json_encode($family);
    }
    public function saveAllStudent(Request $request){
        //save family
            $faminfo = new familyInfo;
            if($request->family_maritalStatus != 'MarriedCivilly'){
                $faminfo->marriedCivil = 0;
                switch ($request->maritalStatus) {
                    case 'MarriedInChurch':
                        $faminfo->marriedChurch = 1;
                    break;
                    case 'ParentsLivingTogether':
                        $faminfo->livingTogether = 1;
                    break;
                    case 'ParentsSeperated':
                        $faminfo->seperated = 1;
                    break;  
                    case 'FatherRemarried':
                        $faminfo->fatherRemarried = 1; 
                    break;   
                    case 'MotherRemarried':
                        $faminfo->motherRemarried = 1; 
                    break;             
                    default:
                        # code...
                        break;
                }
            }
            $faminfo->relatives = $request->family_noOfRelatives;
            $faminfo->helpers = $request->family_noOfHelpers;
            $faminfo->studentID = $request->id;
           
            $faminfo->save();
            $temp = new \stdClass;
        //save schoolbackground
            $schoolbackground = new schoolBG;
            $schoolbackground->schoolName = $request->schoolBackground_schoolBackgroundNameText;
            $schoolbackground->yearAttended = $request->schoolBackground_schoolBackgroundYearAttended;
            $schoolbackground->yearLevel = $request->schoolBackground_schoolBackgroundYearLevel;
            $schoolbackground->Honors = $request->schoolBackground_honorsText;
            $schoolbackground->studentID = $request->id;
            $schoolbackground->save();

            $temp->schoolBackground = $this->getEducationalBackgroundRow($schoolbackground);
        //save fav subject
            $subject = new favSubjectList;
            $subject->subjectID = $request->favSubject_likedSubjectsCombo;
            $subject->studentID = $request->id;
            $subject->grade = $request->favSubject_gradeLikedSubjects;
            $subject->dateAssigned = date('Y-m-d');
            $subject->save();
            $thissubject = favSubjectList::join('subjects', 'subjects.id', '=', 'fav_subject_lists.subjectID')
                                            ->select('fav_subject_lists.*', 'subjects.subjectName', 'subjects.id AS subjectkey')
                                            ->where('fav_subject_lists.id', '=', $subject->id)->get();
            foreach ($thissubject as $thissubject) {
                $thissubject->html = $this->getFavSubjectRow($thissubject);
            }

            $temp->favSubject = $thissubject->html;
        //save disliked subject
            $subject = new dislikedSubject;
            $subject->subjectID = $request->dislikedSubject_dislikedSubjectsCombo;
            $subject->studentID = $request->id;
            $subject->grade = $request->dislikedSubject_gradeDislikedSubjects;
            $subject->dateAssigned = date('Y-m-d');
            $subject->save();
            $thissubject = dislikedSubject::join('subjects', 'subjects.id', '=', 'disliked_subjects.subjectID')
                                            ->select('disliked_subjects.*', 'subjects.subjectName', 'subjects.id AS subjectkey')
                                            ->where('disliked_subjects.id', '=', $subject->id)->get();
            foreach ($thissubject as $thissubject) {
                $thissubject->html = $this->getDislikedSubjectRow($thissubject);
            }
            $temp->dislikedSubject = $thissubject->html;
        //save leisure
            $studenttype = $this->getStudentType($request->id);
            $leisure ='';
            
            switch ($studenttype) {
                case 'college':
                    
                    $leisure = new studentLeisureCollege;
                    $leisure->major = $request->leisure_leisureMajorText;
                    $leisure->presentEducation = $request->leisure_leisurePresentEducationText;
                    $leisure->courseChoice = $request->leisure_leisureCourseChoice;
                    $leisure->howDidYouMakeThisChoice = $request->leisure_leisureHowDidYouMakeThisChoiceText;
                    $leisure->schoolChoice = $request->leisure_leisureSchoolChoiceText;
                    $leisure->howDidYouComeToThisSchool = $request->leisure_leisureHowDidYouComeToThisSchoolText;
                    $leisure->getInfo = $request->leisure_getInfo;
                    $leisure->whereDidYouGetThisInfo = $request->leisure_leisureWhereDidYouGetThisInfo;
                    $leisure->financialSupport = $request->leisure_leisureFinancialSupportText;
                    $leisure->rankClass = $request->leisure_leisureRankClass;
                    $leisure->average = $request->leisure_leisureAverageText;
                    $leisure->scholarship = $request->leisure_leisureScholarshipText;
                    $leisure->otherRemarks = $request->leisure_leisureRemarksTextModal;
                    $collegeid = collegeStudent::where('studentID', '=',$request->id)->get();
                    $leisure->collegStudentID = $collegeid[0]->id;
                    if($request->leisure_barelyPassedSubjectsCheck == "true"){
                        $leisure->selfEval1 = '1';
                    }else{
                        $leisure->selfEval1 = '0';
                    }
                    if($request->leisure_failedSubjectsCheck == "true"){
                        $leisure->selfEval2 = '1';
                    }else{
                        $leisure->selfEval2 = '0';
                    }
                    if($request->leisure_fearSubjectsCheck == "true"){
                        $leisure->selfEval3 = '1';
                    }else{
                        $leisure->selfEval3 = '0';
                    }
                    if($request->leisure_hardSubjectsCheck == "true"){
                        $leisure->selfEval4 = '1';
                    }else{
                        $leisure->selfEval4 = '0';
                    }
                    if($request->leisure_dificultySubjectsCheck == "true"){
                        $leisure->selfEval5 = '1';
                    }else{
                        $leisure->selfEval5 = '0';
                    }
                    if($request->leisure_confidentCheck == "true"){
                        $leisure->selfEval6 = '1';
                    }else{
                        $leisure->selfEval6 = '0';
                    }
                    $leisure->save();
                   
                    $temp->leisure =  $this->getLeisureRow($leisure);
                    /*TODO: do insert for other leisure tables */
                break;
                case 'seniorhigh':
                    
                    $leisure = new studentLeisureSH;
                    $leisure->major = $request->leisure_leisureMajorText;
                    $leisure->schoolChoice = $request->leisure_leisureSchoolChoiceText;
                    $leisure->howDidYouComeToThisSchool = $request->leisure_leisureHowDidYouComeToThisSchoolText;
                    $leisure->getInfo = $request->leisure_getInfo;
                    $leisure->whereDidYouGetThisInfo = $request->leisure_leisureWhereDidYouGetThisInfo;
                    $leisure->financialSupport = $request->leisure_leisureFinancialSupportText;
                    $leisure->rankClass = $request->leisure_leisureRankClass;
                    $leisure->average = $request->leisure_leisureAverageText;
                    $leisure->scholarship = $request->leisure_leisureScholarshipText;
                    $leisure->otherRemarks = $request->leisure_leisureRemarksTextModal;
                    $seniorhighid = SHSStudent::where('studentID', '=',$request->id)->get();
                    $leisure->SHSStudentID = $seniorhighid[0]->id;
                    if($request->leisure_barelyPassedSubjectsCheck == "true"){
                        $leisure->selfEval1 = '1';
                    }else{
                        $leisure->selfEval1 = '0';
                    }
                    if($request->leisure_failedSubjectsCheck == "true"){
                        $leisure->selfEval2 = '1';
                    }else{
                        $leisure->selfEval2 = '0';
                    }
                    if($request->leisure_fearSubjectsCheck == "true"){
                        $leisure->selfEval3 = '1';
                    }else{
                        $leisure->selfEval3 = '0';
                    }
                    if($request->leisure_hardSubjectsCheck == "true"){
                        $leisure->selfEval4 = '1';
                    }else{
                        $leisure->selfEval4 = '0';
                    }
                    if($request->leisure_dificultySubjectsCheck == "true"){
                        $leisure->selfEval5 = '1';
                    }else{
                        $leisure->selfEval5 = '0';
                    }
                    if($request->leisure_confidentCheck == "true"){
                        $leisure->selfEval6 = '1';
                    }else{
                        $leisure->selfEval6 = '0';
                    }
                    $leisure->save();
                    $temp->leisure =  $this->getLeisureRow($leisure);
                break;
                case 'juniorhigh':
                    $leisure = new studentLeisureJH;
                    $leisure->schoolChoice = $request->leisure_leisureSchoolChoiceText;
                    $leisure->howDidYouComeToThisSchool = $request->leisure_leisureHowDidYouComeToThisSchoolText;
                    $leisure->getInfo = $request->leisure_getInfo;
                    $leisure->whereDidYouGetThisInfo = $request->leisure_leisureWhereDidYouGetThisInfo;
                    $leisure->financialSupport = $request->leisure_leisureFinancialSupportText;
                    $leisure->rankClass = $request->leisure_leisureRankClass;
                    $leisure->average = $request->leisure_leisureAverageText;
                    $leisure->scholarship = $request->leisure_leisureScholarshipText;
                    $leisure->otherRemarks = $request->leisure_leisureRemarksTextModal;
                    $juniorhighid = JHStudent::where('studentID', '=',$request->id)->get();
                    $leisure->JHStudentID = $juniorhighid[0]->id;
                    if($request->leisure_barelyPassedSubjectsCheck == "true"){
                        $leisure->selfEval1 = '1';
                    }else{
                        $leisure->selfEval1 = '0';
                    }
                    if($request->leisure_failedSubjectsCheck == "true"){
                        $leisure->selfEval2 = '1';
                    }else{
                        $leisure->selfEval2 = '0';
                    }
                    if($request->leisure_fearSubjectsCheck == "true"){
                        $leisure->selfEval3 = '1';
                    }else{
                        $leisure->selfEval3 = '0';
                    }
                    if($request->leisure_hardSubjectsCheck == "true"){
                        $leisure->selfEval4 = '1';
                    }else{
                        $leisure->selfEval4 = '0';
                    }
                    if($request->leisure_dificultySubjectsCheck == "true"){
                        $leisure->selfEval5 = '1';
                    }else{
                        $leisure->selfEval5 = '0';
                    }
                    if($request->leisure_confidentCheck == "true"){
                        $leisure->selfEval6 = '1';
                    }else{
                        $leisure->selfEval6 = '0';
                    }
                    $leisure->save();
                    $temp->leisure =  $this->getLeisureRow($leisure);
                break;
                case 'elementary':
                    $leisure = new studentLeisureElem;
                    $leisure->schoolChoice = $request->leisure_leisureSchoolChoiceText;
                    $leisure->howDidYouComeToThisSchool = $request->leisure_leisureHowDidYouComeToThisSchoolText;
                    $leisure->getInfo = $request->leisure_getInfo;
                    $leisure->whereDidYouGetThisInfo = $request->leisure_leisureWhereDidYouGetThisInfo;
                    $leisure->financialSupport = $request->leisure_leisureFinancialSupportText;
                    $leisure->rankClass = $request->leisure_leisureRankClass;
                    $leisure->average = $request->leisure_leisureAverageText;
                    $leisure->scholarship = $request->leisure_leisureScholarshipText;
                    $leisure->otherRemarks = $request->leisure_leisureRemarksTextModal;
                    $elemstudentid = elemStudent::where('studentID', '=',$request->id)->get();
                    $leisure->elemStudentID = $elemstudentid[0]->id;
                    if($request->leisure_barelyPassedSubjectsCheck == "true"){
                        $leisure->selfEval1 = '1';
                    }else{
                        $leisure->selfEval1 = '0';
                    }
                    if($request->leisure_failedSubjectsCheck == "true"){
                        $leisure->selfEval2 = '1';
                    }else{
                        $leisure->selfEval2 = '0';
                    }
                    if($request->leisure_fearSubjectsCheck == "true"){
                        $leisure->selfEval3 = '1';
                    }else{
                        $leisure->selfEval3 = '0';
                    }
                    if($request->leisure_hardSubjectsCheck == "true"){
                        $leisure->selfEval4 = '1';
                    }else{
                        $leisure->selfEval4 = '0';
                    }
                    if($request->leisure_dificultySubjectsCheck == "true"){
                        $leisure->selfEval5 = '1';
                    }else{
                        $leisure->selfEval5 = '0';
                    }
                    if($request->leisure_confidentCheck == "true"){
                        $leisure->selfEval6 = '1';
                    }else{
                        $leisure->selfEval6 = '0';
                    }
                    $leisure->save();
                    $temp->leisure =  $this->getLeisureRow($leisure);
                break;
                default:
                    # code...
                    break;
            }
        //health record
            $healthrecord =  new healthRecord;
            $healthrecord->height = $request->healthRecord_healthRecordHeight;
            $healthrecord->weight = $request->healthRecord_healthRecordWeight;
            $healthrecord->complexion = $request->healthRecord_healthRecordComplexion;
            $wearglasses;
            if($request->healthRecord_wearGlasses =='yes'){
                $wearglasses =1;
            }else{
                $wearglasses =0;
            }
            $healthrecord->wearGlasess = $wearglasses;
            $healthrecord->programsParticipated = $request->healthRecord_healthRecordProgramsParticipated;
            $healthrecord->physicalAilment = $request->healthRecord_healthRecordPhysicalAilment;
            $healthrecord->live = $request->healthRecord_healthRecordLive;
            $healthrecord->numPeopleHouse = $request->healthRecord_healthRecordNumPeopleHouse;
            $healthrecord->numPeopleRoom = $request->healthRecord_healthRecordNumPeopleRoom;
            $healthrecord->studentID =$request->id;
            $healthrecord->save();
            $temp->leisure =  $this->getLeisureRow($leisure);
        return json_encode($temp);
    }
}
