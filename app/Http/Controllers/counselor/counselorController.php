<?php

namespace App\Http\Controllers\counselor;

use Illuminate\Http\Request; 
use App\Http\Controllers\Controller;
use App\model\counselor;
use App\model\counseling;
use App\model\college;
use App\Rules\counselorRule;
class counselorController extends Controller
{
    public function getCounselors(){
        $counselor = counselor::leftJoin('colleges', 'colleges.id', '=', 'counselors.designation')
                                ->select('counselors.*', 'colleges.collegeName')
                                ->get();
        if(count($counselor)== 0){
            return "<h2>No counselors found</h2>";
        }
        $html = '';
        foreach ($counselor as $counselors) {
            $html .= $this->getCounselorRow($counselors);
        }
        return  $html;
    }
    public function getCounselorRow($counselors){
        $counselortype = '';
        $designation = '';
        if($counselors->counselorType=='counselor'){
            $designation = $counselors->designation;
        }
        switch($counselors->counselorType){
            case 'universityCounselor':
                $counselortype = "University Counselor";
            break;
            case 'counselor':
                $counselortype = "Counselor";
            break;
        }
        switch ($designation) {
            case 'seniorhigh':
                $designation = "Senior High";
            break;
            case 'juniorhigh':
                $designation = "Junior High";
            break;
            case 'elementary':
                $designation = "Elementary";
            break;
            default:
                $designation = $counselors->collegeName;
                break;
        }
        $counselorStatusButton='';
        
        if($counselors->status == "active"){
            $counselorStatusButton="<button class='btn btn-danger deleteCounselor' id = 'deleteCounselor-$counselors->id'>
                                        <i class='fa fa-trash-o'></i>
                                    </button>";
        }else{
            $counselorStatusButton= "<button class='btn btn-warning restoreCounselor' id = 'deleteCounselor-$counselors->id'>
                                        <i class='fa fa-undo'></i>
                                    </button>";
        }
        return  "<tr id = 'counselorRow-$counselors->id'>
                    <td>$counselors->lastName, $counselors->firstName</td>
                    <td>$counselortype</td>
                    <td data-value = '$counselors->designation' data-college = '$counselors->collegeName'>$designation</td>
                    <td data-value = '$counselors->designation'>
                        <button class='btn btn-warning editCounselor' id = 'editCounselor-$counselors->id'>
                            <i class='fa fa-edit'></i>
                        </button>
                    </td>
                    <td>
                        $counselorStatusButton
                    </td>
                </tr>";
    }
    public function insertCounselor(Request $request){
        $this->validate($request, array(
            'firstName'=>'required|max:100',
            'lastName'=>'required|max:100',
            'counselorType' => ['required', new counselorRule],
            'designation' => 'required|unique:counselors,designation'
        ));
        $counselor = new counselor;
        $counselor->firstName = $request->firstName;
        $counselor->lastName = $request->lastName;
        $counselor->counselorType = $request->counselorType;
        $counselor->designation = $request->designation;
        $counselor->status = "active";
        $counselor->save();
        $counselor->collegeName = '';
        if($counselor->designation != "seniorhigh" && 
            $counselor->designation != "juniorhigh" &&
            $counselor->designation != "elementary" ){
            $temp = college::where('id', '=', $counselor->designation)->get();
            if(count($temp)>0){
                $counselor->collegeName = $temp[0]->collegeName;
            }
             
        }
        
      
        return $this->getCounselorRow($counselor);
    }
    public function editCounselor(Request $request){
        $this->validate($request, array(
            'firstName'=>'required|max:100',
            'lastName'=>'required|max:100',
            'counselorType' => 'required',
            'designation' => 'required'
        ));
        $counselor = counselor::find($request->id);
        if($request->counselorType == 'universityCounselor'){
            if($request->counselorType != $counselor->counselorType){
                $this->validate($request, array(
                    'firstName'=>'required|max:100',
                    'lastName'=>'required|max:100',
                    'counselorType' => ['required', new counselorRule],
                    'designation' => 'required'
                ));
            }
        }
        $counselor->firstName = $request->firstName;
        $counselor->lastName = $request->lastName;
        $counselor->counselorType = $request->counselorType;
        $counselor->designation = $request->designation;
        $counselor->save();
        return "true";
    }
    public function deleteCounselor($counselorID){
        // $counseling = counseling::where('counselorID', '=', $counselorID)->get();
        // if(count($counseling)>0){
        //     return "failed";
        // }else{
            $counselor = counselor::find($counselorID);
            $counselor->status = "inactive";
            $counselor->save();
            return "true";
        //}
    }
    public function restoreCounselor($counselorid){
        $counselor = counselor::find($counselorid);
        $counselor->status = "active";
        $counselor->save();
        return "true";
    }
    public function getCounselorsForCounseling(){
        $counselor = counselor::leftJoin('colleges', 'colleges.id', '=', 'counselors.designation')
        ->select('counselors.*', 'colleges.collegeName')
        ->where('counselors.status', '=', 'active')
        ->get();
        if(count($counselor)== 0){
        return "<h2>No counselors found</h2>";
        }
        $html = '';
        foreach ($counselor as $counselors) {
            $counselortype = '';
            $designation = '';
            if($counselors->counselorType=='counselor'){
                $designation = $counselors->designation;
            }
            switch($counselors->counselorType){
                case 'universityCounselor':
                    $counselortype = "University Counselor";
                break;
                case 'counselor':
                    $counselortype = "Counselor";
                break;
            }
            switch ($designation) {
                case 'seniorhigh':
                    $designation = "Senior High";
                break;
                case 'juniorhigh':
                    $designation = "Junior High";
                break;
                case 'elementary':
                    $designation = "Elementary";
                break;
                default:
                    $designation = $counselors->collegeName;
                    break;
            }
            $html .=  "<tr id = 'counselorCounselingRow-$counselors->id'>
                        <td>$counselors->lastName, $counselors->firstName</td>
                        <td>$counselortype</td>
                        <td data-value = '$counselors->designation' data-college = '$counselors->collegeName'>$designation</td>
                        <td data-value = '$counselors->designation'>
                            <button class='btn btn-dark selectCounselor' id = 'selectCounselor-$counselors->id'>
                                <i class='fa fa-check'></i>
                            </button>
                        </td>
                       
                    </tr>";
        
        }
        return  $html;
    }
}
