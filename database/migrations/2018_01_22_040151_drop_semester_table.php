<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropSemesterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::table('college_students', function (Blueprint $table) {
            $table->dropForeign(['semesterID']);
        });
        Schema::table('contracts', function (Blueprint $table) {
            $table->dropForeign(['semesterID']);
        });
        Schema::table('s_h_s_students', function (Blueprint $table) {
            $table->dropForeign(['semesterID']);
        });
        Schema::table('counselings', function (Blueprint $table) {
            $table->dropForeign(['semesterID']);
        });
        Schema::table('referrals', function (Blueprint $table) {
            $table->dropForeign(['semesterID']);
        });
        //s_h_s_students
        Schema::dropIfExists('semesters');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
