<div id="individualCounselingModal" class="modal fade" role="dialog" >
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content" style = 'z-index:200;'>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Individual Counseling</h4>
      </div>
      <div class="modal-body">
        <div class='container-fluid'> 
         
          <div class='row'> 
             
            <div class="x_panel">
              <div class="x_title">
                <h2>Student Information</h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content" style="display: block;">
                <br>
                <div class='row'>
                  <div class='col-md-4'>
                    <label for = 'studentNameText'>Student Name</label>
                    <div class="input-group">
                      <input type="text" id = 'nameOfStudents' name='nameOfStudents' disabled class="form-control">
                      <input type='hidden' name = 'studentID' id ='studentNameText'>
                      <span class="input-group-btn">
                        <button type="button" class="btn btn-primary" id ='chooseStudent'><i class='fa fa-plus'></i></button>
                      </span>
                    </div>
                    {{--  <select id = 'studentNameText' name = 'studentID'
                    class='form-control' required>
                      @foreach ($student as $student)
                          <option value ='{{$student->id}}'>{{$student->firstName}} {{$student->lastName}}</option>
                      @endforeach
                    </select>  --}}
                  </div>
                  <div class='col-md-4'>
                    <label for = 'courseNameText'>Course</label>
                    <input type = 'text' id = 'courseNameText' name = 'courseNameText'
                    class='form-control' disabled>
                  </div> 
                  <!--div class='col-md-4'>
                    <label for = 'guardianNameText'>Guardian Name</label>
                    <input type = 'text' id = 'guardianNameText' name = 'guardianNameText'
                    class='form-control' disabled>
                  </div-->
                  <div class='col-md-4'>
                      <label for = 'homeAddressText'>Home Address</label>
                      <input type = 'text' id = 'homeAddressText' name = 'homeAddressText'
                      class='form-control' disabled>
                    </div>
                </div>
                <div class='row'>
                  <div class='col-md-4'>
                    <label for = 'studentContactNoText'>Student Contact No.</label>
                    <input type = 'text' id = 'studentContactNoText' name = 'studentContactNoText'
                    class='form-control' disabled>
                  </div>
                  <div class='col-md-4'>
                    <label for = 'yearLevelGradeLevelText'>Year Level/ Grade Level</label>
                    <input type = 'text' id = 'yearLevelGradeLevelText' name = 'yearLevelGradeLevelText'
                    class='form-control' disabled>
                  </div> 
                  <div class='col-md-4'>
                      <label for = 'studentAgeText'>Age</label>
                      <input type = 'text' id = 'studentAgeText' name = 'studentAgeText'
                      class='form-control' disabled>
                    </div>
                </div>
                <div class='row'>
                  <div class='col-md-4'>
                    <label for = 'studentTypeText'>Student Type</label>
                    <input type = 'text' id = 'studentTypeText' name = 'studentTypeText'
                    class='form-control' disabled>
                  </div>
                  <div class='col-md-4'>
                    <label for = 'studentSexText'>Sex</label>
                    <input type = 'text' id = 'studentSexText' name = 'studentSexText'
                    class='form-control' disabled>
                  </div>
                  
                </div>
              </div>
            </div>
            
          </div>
          <div class='row'>
            <div class="tab-content">
              <div class='tab-pane fade in active' id = 'individualCounselingTab'>
                  {!! Form::open([
                    'id'=>'counselingAddForm']) !!}
                <div class='well'>
                  <div class='row'>
                    <div class='col-md-6'>
                      <label>School Year</label>
                      <input type = 'text' id = 'schoolYearText' name = 'schoolYearText'
                      class='form-control' value ='{{$schoolyear}}' disabled>
                    </div>
                    <div class='col-md-6'>
                      <label>Semester</label>
                      @if ($currentsem == 2)
                        <input type = 'text' id = 'semesterText' name = 'schoolYearText' value ='2nd'
                        class='form-control' disabled>
                      @elseif ($currentsem == 1)
                        <input type = 'text' id = 'semesterText' name = 'schoolYearText' value ='1st'
                        class='form-control' disabled>
                      @else
                        <input type = 'text' id = 'semesterText' name = 'schoolYearText' value ='3rd'
                        class='form-control' disabled>
                      @endif
                      
                    </div>
                  </div>
                  <div class='row'>
                    
                    <div class='col-md-6'>
                      <label for = 'counselingDateText'>Counseling Date</label>
                      <input type = 'date' name = 'counselingDate' id = 'counselingDate' value ='{{$systemdate}}' class='form-control' required>
                    </div>
                    <div class='col-md-6'>
                        <label for = 'dateScheduledText'>Scheduled Date</label>
                    <input type = 'date' name = 'dateScheduledText' value ='{{$systemdate}}' id = 'dateScheduledText' class='form-control' required readonly>
                    </div>
                  </div>
                  <div class='row'>
                    
                    <div class='col-md-6'>
                      <div class='genericDivTop'>
                        <label for = 'walkin'><input type=  'checkbox' id = 'walkin' value = 'walkin' name ='walkin'>Walk-in</label>
                      </div>
                    </div>
                  </div>
                  <div class='row'>
                    <div class='col-md-12'>
                      <label for='statementOfTheProblemTextArea'>Statement Of The Problem</label>
                      <textarea class='form-control' id = 'statementOfTheProblemTextArea' name ='statementOfTheProblemTextArea'
                      rows='6' required></textarea>
                    </div>
                  </div>
                  <div class='row genericDivTop'>
                    <div class='col-md-6'>
                      <div class='col-md-4'>
                        <label for='assistedByText'>Assisted By:</label>
                      </div>
                      <div class='col-md-8'>
                        <input type = 'text' id = 'assistedByText' name = 'assistedByText'
                        class='form-control' required value ='{{$nameofuser}}' disabled>
                      </div> 
                    </div>
                    <div class='col-md-6'>
                      <div class='col-md-4'>
                        <label for = 'nameOfCounselor'>Counseled By:</label>
                      </div>
                      <div class="input-group">
                        <input type="text" id = 'nameOfCounselor' name='nameOfCounselor' readonly class="form-control">
                        <input type='hidden' name = 'counseledBy' id ='counseledByText'>
                        <span class="input-group-btn">
                          <button type="button" class="btn btn-primary" id ='chooseCounselor'><i class='fa fa-plus'></i></button>
                        </span>
                      </div>
                    </div>
                  </div>
                  <!--div class='row alignRight genericDivTop'>
                    <ul class="list-unstyled list-inline pull-right">
                      <li><button type="button" class="btn btn-info next-step" id = 'showSecondTab'>Next <i class="fa fa-chevron-right"></i></button></li>
                     </ul>
                     <button class='btn btn-dark'>Save</button>
                  </div-->
                </div>
                {!! Form::close() !!}
              </div>
              
            </div>
          </div>
          
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-warning" form ='counselingAddForm'><i class='fa fa-save'></i>&nbsp;Save</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>

  </div>
</div>
