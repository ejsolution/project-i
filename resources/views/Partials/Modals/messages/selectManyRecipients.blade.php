<div id="selectManyRecipientsModal" class="modal fade" role="dialog" >
  <div class="modal-dialog modal-md">

    <div class="modal-content" style = 'z-index:200'>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Select Recipients</h4>
      </div>
      <div class="modal-body">
        <div class='container-fluid'>
          <div class='row'>
            <h2>
              Select recipients that you would want to receive your message
            </h2>
          </div>
          <div class='row'>
            <div class='col-md-12 well' id ='checkBoxGroupContainer'>
              <div>
                <input type = 'checkbox' id = 'collageCheckbox' form ='sendGroupMessageForm' name ='college'>
                <label for = 'collageCheckbox' >College</label>
              </div>
              <div class='genericDivTop'>
                <input type = 'checkbox' id = 'seniorHighCheckbox' form ='sendGroupMessageForm' name ='seniorhigh'>
                <label for = 'seniorHighCheckbox'>Senior High</label>
              </div>
              <div class='genericDivTop'>
                <input type = 'checkbox' id = 'juniorHighCheckbox' form ='sendGroupMessageForm' name ='juniorhigh'>
                <label for = 'juniorHighCheckbox'>Junior High</label>
              </div>
              <div class='genericDivTop'>
                <input type = 'checkbox' id = 'elementaryCheckbox' form ='sendGroupMessageForm' name ='elementary'>
                <label for = 'elementaryCheckbox'>Elementary</label>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-danger" id ='selectGroupsToSendButton'><i class='fa fa-reply'></i>&nbsp;Send</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>

  </div>
</div>
