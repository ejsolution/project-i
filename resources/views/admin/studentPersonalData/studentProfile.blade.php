@extends('admin.adminTemplate')

@section('title', 'AdminDashboard')
@section('stylesheet')
  <link href="{{asset('css/admin/transactions.css')}}" rel="stylesheet">
  <link href="{{asset('css/admin/studentPersonalData/studentPersonalData.css')}}" rel="stylesheet">
@stop
@section('content')
  <div class='container'>  
    
    <div class='row'>
        <h1>Student Profile</h1>
    </div>
    <div class='row well'> 
                  <input type = 'hidden' value ='{{$studentid}}' id ='studentid'>
                  <input type = 'hidden' value ='{{$studenttype}}' id ='studenttype'>
                  
                  <div class="clearfix"></div>
      
                  <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                      <div class="x_panel">
                        <div class="x_title">
                          <h2>Student</h2>
                          <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                          </ul>
                          <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                          <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
                            <div class="profile_img">
                              <div id="crop-avatar">
                                <!-- Current avatar -->
                                <img class="img-responsive avatar-view" src="{{asset('images-database/students/'.$studentid.'.'.$imageType)}}" alt="Avatar" title="Change the avatar">
                              </div>
                            </div>
                            
                            <h3>{{$studentname}}</h3>
      
                            <ul class="list-unstyled user_data">
                              <li><i class="fa fa-map-marker user-profile-icon"></i> {{$cityaddress}}
                              </li>
                              <li>
                                <i class="fa fa-briefcase user-profile-icon"></i> {{$studenttype}}
                              </li>
                              <li>
                                <i class="fa fa-birthday-cake user-profile-icon"></i> {{$birthdayStudent}}
                              </li>
                            </ul>
      
                            <a class="btn btn-success btn-block" id = 'editProfile'><i class="fa fa-edit m-right-xs"></i>Edit Profile</a>
                            <br />
                            <a class='btn btn-warning btn-block' id = 'upgradeStudent'>Upgrade</a>
                            <a class = 'btn btn-danger btn-block'  id = 'deleteLatestRecord'>Remove latest record</a>
                          </div>
                          <div class="col-md-9 col-sm-9 col-xs-12">
      
                            <div class="profile_title">
                              <div class="col-md-5">
                                <h2>Student Profile Report</h2>
                              </div>
                              <div class="col-md-7">
                                <!--div class='row'>
                                    <div class="col-md-6">
                                        Last Inserted: {{$lastinserted}}    
                                    </div>
                                    <div class="col-md-6">
                                        Last Updated: {{$lastupdated}}
                                    </div>
                                </div-->
                              </div>
                            </div>
                            
                            <div class='row'>
                              <div class='col-md-12'>
                                @component('components.x_panel')
                                  @slot('title')
                                    Family Report
                                  @endslot
                                  @slot('content')
                                    <div class='row'>
                                      <div class='col-md-12 well'>
                                        {!! Form::open([
                                          'id'=>'familyInfoForm']) !!}
                                          <div class='row'>
                                            <div class='col-md-4 well'>
                                              <div>
                                                @if(count($faminfo)>0)
                                                  @if($faminfo[0]->marriedChurch == '1')
                                                    <input type = 'radio' name = 'maritalStatus' value = 'MarriedInChurch' required checked id ='marriedChurchRadio'>Parents Maried in Church
                                                  @else
                                                    <input type = 'radio' name = 'maritalStatus' value = 'MarriedInChurch' required id ='marriedChurchRadio'>Parents Maried in Church
                                                  @endif
                                                @else
                                                  <input type = 'radio' name = 'maritalStatus' value = 'MarriedInChurch' required id ='marriedChurchRadio'>Parents Maried in Church
                                                @endif
                                              </div>
                                              <div class='genericDivTop'>
                                                @if(count($faminfo)>0)
                                                  @if($faminfo[0]->marriedCivil == '1')
                                                    <input type = 'radio' name = 'maritalStatus' value = 'MarriedCivilly' required checked id ='marriedCivillyRadio'>Parents Maried Civilly
                                                  @else
                                                    <input type = 'radio' name = 'maritalStatus' value = 'MarriedCivilly' required id ='marriedCivillyRadio'>Parents Maried Civilly
                                                  @endif
                                                @else
                                                  <input type = 'radio' name = 'maritalStatus' value = 'MarriedCivilly' required id ='marriedCivillyRadio'>Parents Maried Civilly
                                                @endif
                                              </div>
                                              <div class='genericDivTop'>
                                                @if(count($faminfo)>0)
                                                  @if($faminfo[0]->livingTogether == '1')
                                                    <input type = 'radio' name = 'maritalStatus' value = 'ParentsLivingTogether' required checked id ='parentsLivingTogetherRadio'>Parents Living Together
                                                  @else
                                                    <input type = 'radio' name = 'maritalStatus' value = 'ParentsLivingTogether' required id ='parentsLivingTogetherRadio'>Parents Living Together
                                                  @endif
                                                @else
                                                  <input type = 'radio' name = 'maritalStatus' value = 'ParentsLivingTogether' required id ='parentsLivingTogetherRadio'>Parents Living Together
                                                @endif
                            
                                              </div>
                                              <div class='genericDivTop'>
                                                @if(count($faminfo)>0)
                                                  @if($faminfo[0]->seperated == '1')
                                                    <input type = 'radio' name = 'maritalStatus' value = 'ParentsSeperated' required checked id ='parentsSeperatedRadio'>Parents are Seperated
                                                  @else
                                                    <input type = 'radio' name = 'maritalStatus' value = 'ParentsSeperated' required id ='parentsSeperatedRadio'>Parents are Seperated
                                                  @endif
                                                @else
                                                  <input type = 'radio' name = 'maritalStatus' value = 'ParentsSeperated' required id ='parentsSeperatedRadio'>Parents are Seperated
                                                @endif
                                              </div>
                                              <div class='genericDivTop'>
                                                @if(count($faminfo)>0)
                                                  @if($faminfo[0]->fatherRemarried == '1')
                                                    <input type = 'radio' name = 'maritalStatus' value = 'FatherRemarried' required checked id ='fatherRemarriedRadio'>Father Remarried
                                                  @else
                                                    <input type = 'radio' name = 'maritalStatus' value = 'FatherRemarried' required id ='fatherRemarriedRadio'>Father Remarried
                                                  @endif
                                                @else
                                                  <input type = 'radio' name = 'maritalStatus' value = 'FatherRemarried' required id ='fatherRemarriedRadio'>Father Remarried
                                                @endif
                                              </div>
                                              <div class='genericDivTop'>
                                                @if(count($faminfo)>0)
                                                  @if($faminfo[0]->motherRemarried == '1')
                                                    <input type = 'radio' name = 'maritalStatus' value = 'MotherRemarried' required checked id ='motherRemarriedRadio'>Mother Remarried
                                                  @else
                                                    <input type = 'radio' name = 'maritalStatus' value = 'MotherRemarried' required id ='motherRemarriedRadio'>Mother Remarried
                                                  @endif
                                                @else
                                                  <input type = 'radio' name = 'maritalStatus' value = 'MotherRemarried' required id ='motherRemarriedRadio'>Mother Remarried
                                                @endif
                                              </div>
                                            </div>
                                            <div class='col-md-8'>
                                              <div>
                                                <label for ='noOfRelatives'>No. of Relatives</label>
                                                @if(count($faminfo)>0)
                                                  <input type = 'number' id ='noOfRelatives' name = 'noOfRelatives' required class='form-control' value ='{{$faminfo[0]->relatives}}'>
                                                @else
                                                  <input type = 'number' id ='noOfRelatives' name = 'noOfRelatives' required class='form-control' value =''>
                                                @endif
                                              </div>
                                              <div>
                                                <label for ='noOfHelpers'>No. of Helpers</label>
                                                @if(count($faminfo)>0)
                                                  <input type = 'number' id ='noOfHelpers' name = 'noOfHelpers' required class='form-control' value ='{{$faminfo[0]->helpers}}'>
                                                @else
                                                  <input type = 'number' id ='noOfHelpers' name = 'noOfHelpers' required class='form-control' value =''>
                                                @endif
                                              </div>
                                              <div class='genericDivTop alignRight'>
                                                <button class='btn btn-dark' id= 'submitFamilyInfo'>Save</button>
                                              </div>
                                            </div>
                                          </div>
                                        {!! Form::close() !!}
                                      </div>
                                    </div>
                                    <div class='row genericDivTop genericDivBottom alignRight'>
                                      <button class='btn btn-warning' id ='addNewFamily'>Add New Family</button>
                                    </div>
                                    <div class='row'>
                                      <div class='col-md-12 tableModalContainer'>
                                        <table class='table table-striped'>
                                          <thead>
                                            <tr>
                                              <th>Relationship</th>
                                              <th>Name</th>
                                              <th>Address</th>
                                              <th>Date Of Birth</th>
                                              <th>Contact No</th>
                                              <th>Religion</th>
                                              <th>Nationality</th>
                                              <th>Occupation</th>
                                              <th>Employer</th>
                                              <th>Language</th>
                                              <th>Edit</th>
                                              <th>Delete</th>
                                            </tr>
                                          </thead>
                                          <tbody id = 'tableOfFamilies'>
                                            
                                          </tbody>
                                        </table>
                                      </div>
                                    </div>
                                  @endslot
                                @endcomponent
                              </div>
                        
                            </div>
                          </div>
                                @component('components.x_panel')
                                  @slot('title')
                                    Educational Background
                                  @endslot
                                  @slot('content')
                                    <div class='row'>
                                      <div class='col-md-12 well'>
                                        <h2>School Background</h2>
                                        {!! Form::open([
                                          'id'=>'studentEducationalBackgroundForm']) !!}
                                          <div class='well'>
                                            <div class='row'>
                                              <div class='col-md-6'>
                                                <label for='schoolBackgroundNameText'>School Name</label>
                                                <input type = 'text' id = 'schoolBackgroundNameText' name ='schoolName' class='form-control' required maxlength ='100'>
                                              </div>
                                              <div class='col-md-6'>
                                                <label for='schoolBackgroundHonorsText'>Honors</label>
                                                <input type = 'text' id = 'honorsText' name ='Honors' class='form-control' required maxlength ='100'>
                                              </div>
                                            </div>
                                            <div class='row'>
                                              <div class='col-md-6'>
                                                <label for='schoolBackgroundYearLevel'>Year Level</label>
                                                <input type = 'number' id = 'schoolBackgroundYearLevel' name ='yearLevel' class='form-control' required min="1" max="12">
                                              </div>
                                              <div class='col-md-6'>
                                                <label for='schoolBackgroundYearAttended'>Year Attended</label>
                                                <input type = 'number' id = 'schoolBackgroundYearAttended' name ='yearAttended'  class='form-control' required min ='2000' max='3000'>
                                              </div>
                                            </div>
                                            <div class='row'>
                                              <div class='col-md-12 alignRight genericDivTop'>
                                                <button class='btn btn-dark'>Save</button>
                                              </div>
                                            </div>
                                          </div>
                                        {!! Form::close() !!}
                                        <div class='row'>
                                            <div class='col-md-12'>
                                              <table class='table table-striped'>
                                                <thead>
                                                  <tr>
                                                    <th>School Name</th>
                                                    <th>Honors</th>
                                                    <th>Year Level</th>
                                                    <th>Year Attended</th>
                                                    <th>Edit</th>
                                                    <th>Delete</th>
                                                  </tr>
                                                </thead>
                                                <tbody id ='educationalBackgroundTable'>
                                                </tbody>
                                              </table>
                                            </div>
                                          </div>
                                      </div>
                                    </div>
                                    <div class='row'>
                                      <div class='col-md-6 well'>
                                        <h2>Favorite Subjects</h2>
                                        {!! Form::open([
                                          'id'=>'likedSubjectsForm']) !!}
                                          <div>
                                            <label for='likedSubjectsCombo'>Favorite Subject</label>
                                            <select id ='likedSubjectsCombo' name ='subjectID' required class='form-control'>
                                              @foreach ($subjects as $subject)
                                                  <option value ='{{$subject->id}}'>{{$subject->subjectName}}</option>
                                              @endforeach
                                            </select>
                                          </div>
                                          <div>
                                            <label for='gradeLikedSubjects'>Grade</label>
                                            <input type ='number' required id ='gradeLikedSubjects' name ='grade' required class='form-control'>
                                          </div>
                                          <div class='genericDivTop alignRight'>
                                            <button class='btn btn-dark'>Save</button>
                                          </div>
                                        {!! Form::close() !!}
                                        <div class='tableModalSmallContainer'>
                                          <table class='table table-striped'>
                                            <thead>
                                              <tr>
                                                <th>Subject Name</th>
                                                <th>Grade</th>
                                                <th>Edit</th>
                                                <th>Delete</th>
                                              </tr>
                                            </thead>
                                            <tbody id ='favSubjectTable'>
                                              
                                            </tbody>
                                          </table>
                                        </div>
                                      </div>
                                      <div class='col-md-6 well'>
                                          <h2>Disliked Subjects</h2>
                                          {!! Form::open([
                                            'id'=>'dislikedSubjectsForm']) !!}
                                            <div>
                                              <label for='dislikedSubjectsCombo'>Disliked Subject</label>
                                              <select id ='dislikedSubjectsCombo' name ='subjectID' required class='form-control'>
                                                @foreach ($subjects as $subject)
                                                  <option value ='{{$subject->id}}'>{{$subject->subjectName}}</option>
                                                @endforeach
                                              </select>
                                              
                                            </div>
                                            <div>
                                              <label for='gradeDislikedSubjects'>Grade</label>
                                              <input type ='number' required id ='gradeDislikedSubjects' name ='grade' required class='form-control'>
                                            </div>
                                            <div class='genericDivTop alignRight'>
                                              <button class='btn btn-dark'>Save</button>
                                            </div>
                                          {!! Form::close() !!}
                                          <div class='tableModalSmallContainer'>
                                            <table class='table table-striped'>
                                              <thead>
                                                <tr>
                                                  <th>Subject Name</th>
                                                  <th>Grade</th>
                                                  <th>Edit</th>
                                                  <th>Delete</th>
                                                </tr>
                                              </thead>
                                              <tbody id ='dislikedSubjectTable'>
                                                
                                              </tbody>
                                            </table>
                                          </div>
                                      </div>
                                    </div>
                                  @endslot
                                @endcomponent
                                @component('components.x_panel')
                                @slot('title')
                                  Student Leisure
                                @endslot
                                @slot('content')  
                                  {!! Form::open([
                                    'id'=>'studentLeisureForm']) !!}
                                  <div class='well'>
                                    <!--College/SH  Info-->
                                    <div class='row'>
                                      <div class='col-md-6'>
                                        <label for='leisureMajorText'>Major</label>
                                        <input type ='text' id ='leisureMajorText' name ='major' class='form-control'>
                                      </div>
                                      <div class='col-md-6'>
                                        <label for='leisurePresentEducationText'>Present Education</label>
                                        <input type ='text' id ='leisurePresentEducationText' name ='presentEducation' class='form-control'>
                                      </div>
                                    </div>
                                    <div class='row'>
                                      <div class='col-md-6'>
                                        <label for ='leisureCourseChoiceText'>Course Choice</label>
                                        <input type ='text' id ='leisureCourseChoiceText' name ='courseChoice' class='form-control'>
                                      </div>
                                      <div class='col-md-6'>
                                        <label for ='leisureHowDidYouMakeThisChoiceText'>How Did You Make This Choice</label>
                                        <input type ='text' id ='leisureHowDidYouMakeThisChoiceText' name ='howDidYouMakeThisChoice' class='form-control'>
                                      </div>
                                    </div>
                                  </div>
                                  <div class='row'>
                                    <div class='col-md-6'>
                                      <label for ='leisureSchoolChoiceText'>School Choice</label>
                                      <input type ='text' id ='leisureSchoolChoiceText' name ='schoolChoice' class='form-control' required>
                                    </div>
                                    <div class='col-md-6'>
                                      <label for ='leisureHowDidYouComeToThisSchoolText'>How did you come to this school?</label>
                                      <input type ='text' id ='leisureHowDidYouComeToThisSchoolText' name ='howDidYouComeToThisSchool' class='form-control' required>
                                    </div>
                                  </div>
                                  <div class='row'>
                                    <div class='col-md-6'>
                                        <label>How much information do you have about the requirements you're taking up?</label>
                                      
                                        <div class='col-md-3 black'>
                                          <input type = 'radio' name = 'getInfo' value = 'verymuch' required>Very Much
                                        </div>
                                        <div class='col-md-2 black'>
                                          <input type = 'radio' name = 'getInfo' value = 'much' required>Much
                                        </div>
                                        <div class='col-md-2 black'>
                                          <input type = 'radio' name = 'getInfo' value = 'enough' required>Enough
                                        </div>
                                        <div class='col-md-2 black'>
                                          <input type = 'radio' name = 'getInfo' value = 'verylittle' required>Very Little
                                        </div>
                                        <div class='col-md-2 black'>
                                          <input type = 'radio' name = 'getInfo' value = 'none' required>None
                                        </div>
                                      </div>
                                      <div class='col-md-6 black'>
                                        <label for ='leisureWhereDidYouGetThisInfo'>Where did you get this info?</label>
                                        <input type = 'text' id ='leisureWhereDidYouGetThisInfo' name ='whereDidYouGetThisInfo' class='form-control' required>
                                      </div>
                                  </div>
                                  <div class='row'>
                                    <div class='col-md-6'>
                                      <label for ='leisureFinancialSupportText'>Financial Support</label>
                                      <input type ='text' id ='leisureFinancialSupportText' name ='financialSupport' class='form-control' required>
                                    </div>
                                    <div class='col-md-6'>
                                      <label for ='leisureScholarshipText'>Scholarship</label>
                                      <input type ='text' id ='leisureScholarshipText' name ='scholarship' class='form-control' required>
                                    </div>
                                  </div>
                                  <div class='row'>
                                      <div class='col-md-12'>
                                        <label>Self-evaluation regarding scholastic standing. Check the following which apply to you</label>
                                      </div>
                                    </div>
                                    <div class='row'>
                                      <div class='col-md-4'>
                                        <label for = 'barelyPassedSubjectsCheck'>
                                          <input type = 'checkbox' id = 'barelyPassedSubjectsCheck' name = 'barelyPassedSubjectsCheck'
                                          value = 'true' >
                                          I barely pass all my subjects
                                        </label>
                                        <label for = 'failedSubjectsCheck'>
                                          <input type = 'checkbox' id = 'failedSubjectsCheck' name = 'failedSubjectsCheck'
                                          value = 'true' >
                                          I failed most of my subjects
                                        </label>
                                      </div>
                                      <div class='col-md-4'>
                                        <label for = 'fearSubjectsCheck'>
                                          <input type = 'checkbox' id = 'fearSubjectsCheck' name = 'barelyPassedSubjectsCheck'
                                          value = 'true' >
                                          I fear im going to fail this semester
                                        </label>
                                        <label for = 'hardSubjectsCheck'>
                                          <input type = 'checkbox' id = 'hardSubjectsCheck' name = 'hardSubjectsCheck'
                                          value = 'true' >
                                          I am having a hard time passing my subjects
                                        </label>
            
                                      </div>
                                      <div class='col-md-4'>
                                        <label for = 'dificultySubjectsCheck'>
                                          <input type = 'checkbox' id = 'dificultySubjectsCheck' name = 'dificultySubjectsCheck'
                                          value = 'true' >
                                          I have difficulty with some of my subjects
                                        </label>
                                        <label for = 'confidentCheck'>
                                          <input type = 'checkbox' id = 'confidentCheck' name = 'confidentCheck'
                                          value = 'true' >
                                          I am confident i can finish my course
                                        </label>
                                      </div>
                                    </div>
                                    <div class='row'>
                                        <div class='col-md-12'>
                                          <label for = 'leisureRemarksTextModal'>Remarks:</label>
                                          <input type = 'text' id = 'leisureRemarksTextModal' name = 'otherRemarks'
                                          class='form-control'>
                                        </div>
                                    </div>
                                    <div class='row'>
                                      <div class='col-md-6'>
                                        <label for ='leisureRankClass'>Rank in Class</label>
                                        <input type ='number' class='form-control' id ='leisureRankClass' name ='rankClass' required>
                                      </div>
                                      <div class='col-md-6'>
                                        <label for ='leisureAverageText'>Average Grade</label>
                                        <input type ='number' id ='leisureAverageText' class='form-control' name ='average' required>
                                      </div>
                                    </div>
                                    <div class='row genericDivTop alignRight'>
                                        <button class='btn btn-dark'>Save</button>
                                      </div>
                                    
                                  {!! Form::close() !!}
                                  <div class='row'>
                                    <div class='col-md-12 tableContainer'>
                                      <table class='table table-striped'>
                                        <thead>
                                          <tr>
                                            <th>School Choice</th>
                                            <th>Financial Support</th>
                                            <th>Rank in Class</th>
                                            <th>Average</th>
                                            <th>Edit</th>
                                            <th>Delete</th>
                                          </tr>
                                        </thead>
                                        <tbody id ='studentLeisureTable'>
                                        </tbody>
                                      </table>
                                    </div>
                                  </div>
                                @endslot
                              @endcomponent
                              @component('components.x_panel')
                                @slot('title')
                                  Health Records
                                @endslot
                                @slot('content')
                                  <div>
                                    {!! Form::open([
                                      'id'=>'studentHealthRecordForm']) !!}
                                      <div class='row'>
                                        <div class='col-md-6'>
                                          <label for ='healthRecordHeight'>Height</label>
                                          <input type ='number' class='form-control' required id ='healthRecordHeight' name ='height'>
                                        </div>
                                        <div class='col-md-6'>
                                          <label for ='healthRecordWeight'>Weight</label>
                                          <input type ='number' class='form-control' required id ='healthRecordWeight' name ='weight'>
                                        </div>
                                      </div>  
                                      <div class='row'>
                                        <div class='col-md-6'>
                                          <label for ='healthRecordComplexion'>Complexion</label>
                                          <select class='form-control' id ='healthRecordComplexion' name ='complexion' required>
                                            <option value ='lightskin'>Light skin</option>
                                            <option value ='fairskin'>Fair Skin</option>
                                            <option value ='mediumskin'>Medium Skin</option>
                                            <option value ='tanbrown'>Tan Brown</option>
                                            <option value ='blackbrown'>Black Brown</option>
                                          </select>
                                        </div>
                                        <div class='col-md-6'>
                                          
                                          <div class='row'>
                                            <div class='col-md-12'>
                                                <label>Wearing Glasses?</label>
                                            </div>
                                            
                                            <div class='col-md-6'>
                                              <label for = 'yesWearGlassesRadio'>
                                                <input type = 'radio' id ='yesWearGlassesRadio' name = 'wearGlasses' value = 'yes' required>
                                                Yes
                                              </label>
                                            </div>
                                            <div class='col-md-6'>
                                              <label for = 'noWearGlassesRadio'>
                                                <input type = 'radio' id ='noWearGlassesRadio' name = 'wearGlasses' value = 'no' required>
                                                No
                                              </label>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class='row'>
                                        <div class='col-md-6'>
                                          <label for ='healthRecordProgramsParticipated'>Programs Participated</label>
                                          <input type ='text' id ='healthRecordProgramsParticipated' class='form-control' name ='programsParticipated' required maxlength ='100'>
                                        </div>
                                        <div class='col-md-6'>
                                          <label for ='healthRecordPhysicalAilment'>Physical Ailment</label>
                                          <input type ='text' id ='healthRecordPhysicalAilment' class='form-control' name ='physicalAilment'  required maxlength ='100'>
                                        </div>
                                      </div>
                                      
                                      <div class='row'>
                                        <div class='col-md-6'>
                                          <label for ='healthRecordNumPeopleHouse'>Number Of People At Your House</label>
                                          <input type ='number' id ='healthRecordNumPeopleHouse' name ='numPeopleHouse' class='form-control' required>
                                        </div>
                                        <div class='col-md-6'>
                                          <label for ='healthRecordNumPeopleRoom'>Number Of People At Your Room</label>
                                          <input type ='number' id ='healthRecordNumPeopleRoom' name ='numPeopleRoom' class='form-control' required>
                                        </div>
                                      </div>
                                      <div class='row'>
                                        <div class='col-md-12'>
                                          <label for ='healthRecordLive'>Describe Where You Live</label>
                                          <input type ='text' id ='healthRecordLive' name ='live' required class='form-control'>
                                        </div>
                                      </div>
                                      <div class='genericDivTop alignRight'>
                                        <button class='btn btn-dark'>Save</button>
                                      </div>
                                    {!! Form::close() !!}
                                  </div> 
                                  <div class='row'>
                                    <div class='col-md-12 tableModalBigContainer'>
                                        <table class='table table-striped'>
                                          <thead>
                                            <tr>
                                              <th>Height</th>
                                              <th>Weight</th>
                                              <th>Complexion</th>
                                              <th>Wear Glasses</th>
                                              <th>Programs Participated</th>
                                              <th>Physical Ailment</th>
                                              <th>Live</th>
                                              <th>People In House</th>
                                              <th>People In Room</th>
                                              <th>Date Added</th>
                                              <th>Edit</th>
                                              <th>Delete</th>
                                            </tr>
                                          </thead>
                                          <tbody id ='tableHealthRecordBody'>
                                          </tbody>
                                        </table>
                                    </div>
                                  </div>
                                @endslot
                              @endcomponent
                              @component('components.x_panel')
                                @slot('title')
                                  Make Up Assistance
                                @endslot
                                @slot('content')
                                  <div>
                                    {!! Form::open([
                                      'id'=>'makeUpAssistanceForm']) !!}
                                      <div class='row'>
                                        <div class='col-md-12 tableModalContainer'>
                                          <table class='table table-striped'>
                                            <thead>
                                              <tr>
                                                <th>Check</th>
                                                {{-- <th>Personality</th>
                                                <th>Description</th> --}}
                                              </tr>
                                            </thead>
                                            <tbody id ='personalityTableCheck'>

                                            </tbody>
                                          </table>
                                        </div>
                                      </div>
                                      <div class='row'>
                                        <div class='col-md-12'>
                                          <label for ='makeUpAssistanceSignificantEventsText'>Siginificant Events in Your Life. Explain Briefly</label>
                                          <textarea class='form-control' name ='significantEvents' id ='makeUpAssistanceSignificantEventsText'
                                          ></textarea>
                                        </div>
                                      </div>
                                      <div class='row'>
                                        <div class='col-md-12'>
                                          <label for ='makeUpAssistanceHelpNeededText'>What Help Do You Want to Obtain From The Guidance Center</label>
                                          <textarea class='form-control' name ='helpNeeded' id ='makeUpAssistanceHelpNeededText'
                                          ></textarea>
                                        </div>
                                      </div>
                                      <div class='row genericDivTop alignRight'>
                                        <button class='btn btn-dark'> Save</button>
                                      </div>

                                    {!!Form::close()!!}
                                  </div>
                                  <div class='tableModalContainer'>
                                    <table class='table table-striped'>
                                      <thead>
                                        <tr>
                                          <th>Personality</th>
                                          <th>Date Added</th>
                                          <th>Edit</th>
                                          <th>Deleted</th>
                                        </tr>
                                      </thead>
                                      <tbody id ='makeUpAssistanceTable'>
                                      </tbody>
                                    </table>
                                  </div>
                                @endslot
                              @endcomponent
                              <div class='genericDivTop alignRight'>
                                <button class='btn btn-primary' id = 'saveAllButton'>Save All</button>
                              </div>
                             <!-- -->
                      </div>
                    </div>
                  </div>
                </div>
    </div>
    @component('components.modal')
      @slot('modalid')
        editMakeUpAssistanceModal
      @endslot
      @slot('modalsize')
        modal-md
      @endslot
      @slot('modaltitle')
        Edit Make Up Assistance
      @endslot
      @slot('modalcontent')
        {!! Form::open([
        'id'=>'editMakeUpAssistanceForm']) !!}
        <div class='row'>
            <div class='col-md-12 tableModalContainer'>
              <table class='table table-striped'>
                <thead>
                  <tr>
                    <th>Check</th>
                    <th>Personality</th>
                    <th>Description</th>
                  </tr>
                </thead>
                <tbody id ='personalityTableCheckEdit'>

                </tbody>
              </table>
            </div>
          </div>
          <div class='row'>
            <div class='col-md-12'>
              <label for ='makeUpAssistanceSignificantEventsTextEdit'>Siginificant Events in Your Life. Explain Briefly</label>
              <textarea class='form-control' name ='significantEvents' id ='makeUpAssistanceSignificantEventsTextEdit'
              ></textarea>
            </div>
          </div>
          <div class='row'>
            <div class='col-md-12'>
              <label for ='makeUpAssistanceHelpNeededText'>What Help Do You Want to Obtain From The Guidance Center</label>
              <textarea class='form-control' name ='helpNeeded' id ='makeUpAssistanceHelpNeededTextEdit'
              ></textarea>
            </div>
          </div>
        {!! Form::close() !!}
      @endslot
      @slot('modalbutton')
      <button class="btn btn-warning" form = 'editMakeUpAssistanceForm'><i class='fa fa-save'></i>&nbsp;Save</button>
      @endslot
    @endcomponent
    @component('components.modal')
      @slot('modalid')
        editHealthRecordModal
      @endslot
      @slot('modalsize')
        modal-md
      @endslot
      @slot('modaltitle')
        Edit Health Record
      @endslot
      @slot('modalcontent')
        {!! Form::open([
        'id'=>'editHealthRecordForm']) !!}
          <div class='row'>
            <div class='col-md-6'>
              <label for ='healthRecordHeightEdit'>Height</label>
              <input type ='number' class='form-control' required id ='healthRecordHeightEdit' name ='height'>
            </div>
            <div class='col-md-6'>
              <label for ='healthRecordWeightEdit'>Weight</label>
              <input type ='number' class='form-control' required id ='healthRecordWeightEdit' name ='weight'>
            </div>
          </div>  
          <div class='row'>
            <div class='col-md-6'>
              <label for ='healthRecordComplexionEdit'>Complexion</label>
              <select class='form-control' id ='healthRecordComplexionEdit' name ='complexion' required>
                <option value ='lightskin'>Light skin</option>
                <option value ='fairskin'>Fair Skin</option>
                <option value ='mediumskin'>Medium Skin</option>
                <option value ='tanbrown'>Tan Brown</option>
                <option value ='blackbrown'>Black Brown</option>
              </select>
            </div>
            <div class='col-md-6'>
              
              <div class='row'>
                <div class='col-md-12'>
                    <label>Wearing Glasses?</label>
                </div>
                
                <div class='col-md-6'>
                  <label for = 'yesWearGlassesRadioEdit'>
                    <input type = 'radio' id ='yesWearGlassesRadioEdit' name = 'wearGlasses' value = 'yes' required>
                    Yes
                  </label>
                </div>
                <div class='col-md-6'>
                  <label for = 'noWearGlassesRadioEdit'>
                    <input type = 'radio' id ='noWearGlassesRadioEdit' name = 'wearGlasses' value = 'no' required>
                    No
                  </label>
                </div>
              </div>
            </div>
          </div>
          <div class='row'>
            <div class='col-md-6'>
              <label for ='healthRecordProgramsParticipatedEdit'>Programs Participated</label>
              <input type ='text' id ='healthRecordProgramsParticipatedEdit' class='form-control' name ='programsParticipated' required maxlength ='100'>
            </div>
            <div class='col-md-6'>
              <label for ='healthRecordPhysicalAilmentEdit'>Physical Ailment</label>
              <input type ='text' id ='healthRecordPhysicalAilmentEdit' class='form-control' name ='physicalAilment'  required maxlength ='100'>
            </div>
          </div>
          
          <div class='row'>
            <div class='col-md-6'>
              <label for ='healthRecordNumPeopleHouseEdit'>Number Of People At Your House</label>
              <input type ='number' id ='healthRecordNumPeopleHouseEdit' name ='numPeopleHouse' class='form-control' required>
            </div>
            <div class='col-md-6'>
              <label for ='healthRecordNumPeopleRoomEdit'>Number Of People At Your Room</label>
              <input type ='number' id ='healthRecordNumPeopleRoomEdit' name ='numPeopleRoom' class='form-control' required>
            </div>
          </div>
          <div class='row'>
            <div class='col-md-12'>
              <label for ='healthRecordLiveEdit'>Describe Where You Live</label>
              <input type ='text' id ='healthRecordLiveEdit' name ='live' required class='form-control'>
            </div>
          </div>
        
        {!! Form::close() !!}
      @endslot
      @slot('modalbutton')
      <button class="btn btn-warning" form = 'editHealthRecordForm'><i class='fa fa-save'></i>&nbsp;Save</button>
      @endslot
    @endcomponent
    @component('components.modal')
      @slot('modalid')
        editStudentLeisureModal
      @endslot
      @slot('modalsize')
        modal-lg
      @endslot
      @slot('modaltitle')
        Edit Student's Leisure
      @endslot
      @slot('modalcontent')
        {!! Form::open([
        'id'=>'editStudentLeisureForm']) !!}
        <div class='well'>
          <!--College/SH  Info-->
          <div class='row'>
            <div class='col-md-6'>
              <label for='leisureMajorTextEdit'>Major</label>
              <input type ='text' id ='leisureMajorTextEdit' name ='major' class='form-control'>
            </div>
            <div class='col-md-6'>
              <label for='leisurePresentEducationTextEdit'>Present Education</label>
              <input type ='text' id ='leisurePresentEducationTextEdit' name ='presentEducation' class='form-control'>
            </div>
          </div>
          <div class='row'>
            <div class='col-md-6'>
              <label for ='leisureCourseChoiceTextEdit'>Course Choice</label>
              <input type ='text' id ='leisureCourseChoiceTextEdit' name ='courseChoice' class='form-control'>
            </div>
            <div class='col-md-6'>
              <label for ='leisureHowDidYouMakeThisChoiceTextEdit'>How Did You Make This Choice</label>
              <input type ='text' id ='leisureHowDidYouMakeThisChoiceTextEdit' name ='howDidYouMakeThisChoice' class='form-control'>
            </div>
          </div>
        </div>
        <div class='row'>
          <div class='col-md-6'>
            <label for ='leisureSchoolChoiceTextEdit'>School Choice</label>
            <input type ='text' id ='leisureSchoolChoiceTextEdit' name ='schoolChoice' class='form-control' required>
          </div>
          <div class='col-md-6'>
            <label for ='leisureHowDidYouComeToThisSchoolTextEdit'>How did you come to this school?</label>
            <input type ='text' id ='leisureHowDidYouComeToThisSchoolTextEdit' name ='howDidYouComeToThisSchool' class='form-control' required>
          </div>
        </div>
        <div class='row'>
          <div class='col-md-6'>
              <label>How much information do you have about the requirements you're taking up?</label>
            
              <div class='col-md-3'>
                <input type = 'radio' name = 'getInfo' value = 'verymuch' required>Very Much
              </div>
              <div class='col-md-2'>
                <input type = 'radio' name = 'getInfo' value = 'much' required>Much
              </div>
              <div class='col-md-2'>
                <input type = 'radio' name = 'getInfo' value = 'enough' required>Enough
              </div>
              <div class='col-md-2'>
                <input type = 'radio' name = 'getInfo' value = 'verylittle' required>Very Little
              </div>
              <div class='col-md-2'>
                <input type = 'radio' name = 'getInfo' value = 'none' required>None
              </div>
            </div>
            <div class='col-md-6'>
              <label for ='leisureWhereDidYouGetThisInfoEdit'>Where did you get this info?</label>
              <input type = 'text' id ='leisureWhereDidYouGetThisInfoEdit' name ='whereDidYouGetThisInfo' class='form-control' required>
            </div>
        </div>
        <div class='row'>
          <div class='col-md-6'>
            <label for ='leisureFinancialSupportTextEdit'>Financial Support</label>
            <input type ='text' id ='leisureFinancialSupportTextEdit' name ='financialSupport' class='form-control' required>
          </div>
          <div class='col-md-6'>
            <label for ='leisureScholarshipTextEdit'>Scholarship</label>
            <input type ='text' id ='leisureScholarshipTextEdit' name ='scholarship' class='form-control' required>
          </div>
        </div>
        <div class='row'>
            <div class='col-md-12'>
              <label>Self-evaluation regarding scholastic standing. Check the following which apply to you</label>
            </div>
          </div>
          <div class='row'>
            <div class='col-md-4'>
              <label for = 'barelyPassedSubjectsCheckEdit'>
                <input type = 'checkbox' id = 'barelyPassedSubjectsCheckEdit' name = 'barelyPassedSubjectsCheck'
                value = 'true' >
                I barely pass all my subjects
              </label>
              <label for = 'failedSubjectsCheckEdit'>
                <input type = 'checkbox' id = 'failedSubjectsCheckEdit' name = 'failedSubjectsCheck'
                value = 'true' >
                I failed most of my subjects
              </label>
            </div>
            <div class='col-md-4'>
              <label for = 'fearSubjectsCheckEdit'>
                <input type = 'checkbox' id = 'fearSubjectsCheckEdit' name = 'fearSubjectsCheckEdit'
                value = 'true' >
                I fear im going to fail this semester
              </label>
              <label for = 'hardSubjectsCheckEdit'>
                <input type = 'checkbox' id = 'hardSubjectsCheckEdit' name = 'hardSubjectsCheckEdit'
                value = 'true' >
                I am having a hard time passing my subjects
              </label>

            </div>
            <div class='col-md-4'>
              <label for = 'dificultySubjectsCheckEdit'>
                <input type = 'checkbox' id = 'dificultySubjectsCheckEdit' name = 'dificultySubjectsCheck'
                value = 'true' >
                I have difficulty with some of my subjects
              </label>
              <label for = 'confidentCheckEdit'>
                <input type = 'checkbox' id = 'confidentCheckEdit' name = 'confidentCheck'
                value = 'true' >
                I am confident i can finish my course
              </label>
            </div>
          </div>
          <div class='row'>
              <div class='col-md-12'>
                <label for = 'leisureRemarksTextModalEdit'>Remarks:</label>
                <input type = 'text' id = 'leisureRemarksTextModalEdit' name = 'otherRemarks'
                class='form-control'>
              </div>
          </div>
          <div class='row'>
            <div class='col-md-6'>
              <label for ='leisureRankClassEdit'>Rank in Class</label>
              <input type ='number' class='form-control' id ='leisureRankClassEdit' name ='rankClass' required>
            </div>
            <div class='col-md-6'>
              <label for ='leisureAverageTextEdit'>Average Grade</label>
              <input type ='number' id ='leisureAverageTextEdit' class='form-control' name ='average' required>
            </div>
          </div>
          
          
        {!! Form::close() !!}
      @endslot
      @slot('modalbutton')
      <button class="btn btn-warning" form = 'editStudentLeisureForm'><i class='fa fa-save'></i>&nbsp;Save</button>
      @endslot
    @endcomponent
          
    @component('components.modal')
      @slot('modalid')
      addFamilyStudentModal
      @endslot
      @slot('modalsize')
        modal-md
      @endslot
      @slot('modaltitle')
        Add A Student Family
      @endslot
      @slot('modalcontent')
        {!! Form::open([
        'id'=>'studentFamilyForm']) !!}
            <div class='row'>
              <div class='col-md-6'>
                <label for = 'nameOfFamilyMember'>Name:</label>
                <input type ='text' required maxlength ='100' id  ='nameOfFamilyMember' name ='nameOfFamilyMember' class='form-control'>
              </div>
              <div class='col-md-6'>
                <label for = 'addressFamily'>Address:</label>
                <input type ='text' required maxlength ='100' id  ='addressFamily' name ='addressFamily' class='form-control'>
              </div>
            </div>
            <div class='row'>
              <div class='col-md-6'>
                <label for ='dateOfBirthFamily'>Date Of Birth:</label>
                <input type ='date' required id = 'dateOfBirthFamily' name= 'dateOfBirthFamily' class='form-control'>
              </div>
              <div class='col-md-6'>
                <label for ='contactNoFamily'>Contact No:</label>
                <input type ='text' id ='contactNoFamily' name = 'contactNoFamily' required maxlength ='12' class='form-control'>
              </div>
            </div>
            <div class='row'>
              <div class='col-md-6'>
                <label for ='religionFamily'>Religion:</label>
                <input type ='text' id = 'religionFamily' name ='religionFamily' required maxlength ='100' class='form-control'>
              </div>
              <div class='col-md-6'>
                <label for ='nationalityFamily'>Nationality:</label>
                <input type ='text' id ='nationalityFamily' name ='nationalityFamily' required maxlength ='100' class='form-control'>
              </div>
            </div>
            <div class='row'>
              <div class='col-md-6'>
                <label for ='employerFamily'>Employer:</label>
                <input type ='text' id ='employerFamily' name ='employerFamily' maxlength='100' class='form-control'>
              </div>
              <div class='col-md-6'>
                <label for ='languageFamily'>Language:</label>
                <input type ='text' id ='languageFamily' name ='languageFamily' maxlength='100' required class='form-control'>
              </div>
            </div>
            <div class='row'>
              <div class='col-md-6'>
                <label for ='relationshipFamily'>Relationship:</label>
                <select id ='relationshipFamily' name ='relationshipFamily' required class='form-control'>
                  <option value ='cousin'>Cousin</option>
                  <option value ='mother'>Mother</option>
                  <option value ='father'>Father</option>
                  <option value ='brother'>Brother</option>
                  <option value ='sister'>Sister</option>
                  <option value ='grandparent'>Grandparent</option>
                </select>
              </div>
              <div class='col-md-6'>
                <label for ='occupationFamily'>Occupation:</label>
                <input type ='text' id ='occupationFamily' name ='occupationFamily' maxlength='100' class='form-control'>
              </div>
            </div>
        {!! Form::close() !!}
      @endslot
      @slot('modalbutton')
      <button class="btn btn-warning" form = 'studentFamilyForm'><i class='fa fa-save'></i>&nbsp;Save</button>
      @endslot
    @endcomponent
    @component('components.modal')
      @slot('modalid') 
        editStudentBackgroundForm
      @endslot
      @slot('modalsize')
        modal-md
      @endslot
      @slot('modaltitle')
        Edit A Student School Background
      @endslot
      @slot('modalcontent')
        {!! Form::open([
        'id'=>'schoolBackgroundEditForm']) !!}
          <div class='row'>
            <div class='col-md-6'>
              <label for='schoolBackgroundNameEditText'>School Name</label>
              <input type = 'text' id = 'schoolBackgroundNameEditText' name ='schoolName' class='form-control' required maxlength ='100'>
            </div>
            <div class='col-md-6'>
              <label for='schoolBackgroundHonorsEditText'>Honors</label>
              <input type = 'text' id = 'schoolBackgroundHonorsEditText' name ='Honors' class='form-control' required maxlength ='100'>
            </div>
          </div>
          <div class='row'>
            <div class='col-md-6'>
              <label for='schoolBackgroundYearLevelEdit'>Year Level</label>
              <input type = 'number' id = 'schoolBackgroundYearLevelEdit' name ='yearLevel' class='form-control' required min="1" max="12">
            </div>
            <div class='col-md-6'>
              <label for='schoolBackgroundYearAttendedEdit'>Year Attended</label>
              <input type = 'number' id = 'schoolBackgroundYearAttendedEdit' name ='yearAttended'  class='form-control' required min ='2000' max='3000'>
            </div>
          </div>
        {!! Form::close() !!}
      @endslot
      @slot('modalbutton')
      <button class= "btn btn-warning" form = 'schoolBackgroundEditForm'><i class='fa fa-save'></i>&nbsp;Save</button>
      @endslot
    @endcomponent
    @component('components.modal')
      @slot('modalid')
        editFavSubjectModal
      @endslot
      @slot('modalsize')
        modal-sm
      @endslot
      @slot('modaltitle')
        Edit Favorite Subject
      @endslot
      @slot('modalcontent')
        {!! Form::open([
        'id'=>'editFavSubjectForm']) !!}
          <div class='row'>
            <div class='col-md-12'>
              <label for ='favSubjectsEdit'>Subjects:</label>
              <select id ='favSubjectsEdit' name ='subjectID' class='form-control'>
                
              </select>
            </div>
          </div>
          <div class='row'>
            <div class='col-md-12'>
              <label for ='favSubjectGradeEdit'>Grade</label>
              <input type ='number' id ='favSubjectGradeEdit' name ='grade' required class='form-control'>
            </div>
          </div>
        {!! Form::close() !!}
      @endslot
      @slot('modalbutton')
      <button class="btn btn-warning" form = 'editFavSubjectForm'><i class='fa fa-save'></i>&nbsp;Save</button>
      @endslot
    @endcomponent
    @component('components.modal')
      @slot('modalid')
        editDislikedSubjectModal
      @endslot
      @slot('modalsize')
        modal-sm
      @endslot
      @slot('modaltitle')
        Edit Favorite Subject
      @endslot
      @slot('modalcontent')
        {!! Form::open([
        'id'=>'editDislikedSubjectForm']) !!}
          <div class='row'>
            <div class='col-md-12'>
              <label for ='dislikedSubjectsEdit'>Subjects:</label>
              <select id ='dislikedSubjectsEdit' name ='subjectID' class='form-control'>
                
              </select>
            </div>
          </div>
          <div class='row'>
            <div class='col-md-12'>
              <label for ='dislikedSubjectGradeEdit'>Grade</label>
              <input type ='number' id ='dislikedSubjectGradeEdit' name ='grade' required class='form-control'>
            </div>
          </div>
        {!! Form::close() !!}
      @endslot
      @slot('modalbutton')
      <button class="btn btn-warning" form = 'editDislikedSubjectForm'><i class='fa fa-save'></i>&nbsp;Save</button>
      @endslot
    @endcomponent
    @component('components.modal')
      @slot('modalid')
        upgradeStudentModal
      @endslot
      @slot('modalsize')
        modal-sm
      @endslot
      @slot('modaltitle')
        Upgrade Student
      @endslot
      @slot('modalcontent')
        <div id='UpgradeStudentMessage'>
        </div>
        <div id='collegeCourseStudentUpgrade'>
          <label for = 'upgradeCollegeCombo'>College</label>
          <select id = 'upgradeCollegeCombo'  class='form-control'>
              @foreach($college as $colleges)
                <option value = '{{$colleges->id}}'>{{$colleges->collegeName}}</option>
              @endforeach
          </select>
          <label for = 'upgradeCourseCombo'>Course</label>
          <select id = 'upgradeCourseCombo' class='form-control'>
              
          </select>
        </div>
        <div id='strandContainerUpgrade'>
            
            <label for = 'upgradeStrandCombo'>Strand</label>
            <select id = 'upgradeStrandCombo' class='form-control'>
              @foreach($strands as $strand)
                <option value = '{{$strand->id}}'>{{$strand->strandName}}</option>
              @endforeach
            </select>
          </div>
      @endslot
      @slot('modalbutton')
          <button type="button" class="btn btn-danger" disabled id = 'upgradeStudentConfirm'><i class='fa fa-trash-o'></i>&nbsp;Confirm</button>
      @endslot
    @endcomponent 
    {!! Form::open([
      'id'=>'saveAllForm']) !!}
    {!! Form::close() !!}

  </div>
  @include('Partials.Modals.studentPersonalData.studentPersonalDataModal2')
@endSection
@section('scripts')
  <script src = '{{asset('js/studentPersonalData/studentProfile.js')}}'></script>
@stop
