<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\systemTimeRule;
use App\Rules\schoolYearRule;
class contractRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules= [
            'typeOfContract'=>'required|max:50',
            'remarksContract'=>'required|max:50',
            'notedByContract'=>['required', 'max:50',new SystemTimeRule, new schoolYearRule],
            'uploadPhotosContract.*' => 'required|image|mimes:jpeg,bmp,png|max:2000'
        ];
        
 
        return $rules;
    }
}
