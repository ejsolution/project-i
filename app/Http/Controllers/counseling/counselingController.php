<?php

namespace App\Http\Controllers\counseling;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\student\studentController;
use App\Http\Controllers\activityController;
use App\Http\Controllers\users\userController;
use App\Http\Controllers\notification\notificationController;
use App\model\counseling;
use App\model\collegeStudent;
use App\model\SHSStudent;
use App\model\JHStudent;
use App\model\elemStudent;
use App\model\student;
use App\model\course;
use App\model\schoolYear;
use App\model\evaluation;
use App\model\followUp;
use App\model\groupCounseling;
use App\model\systemDate;
use App\model\useraccount;
use App\model\notification;
use App\model\message;
use App\model\adminsetting;
use DB;
use Auth; 
use PDF;
use App\Rules\systemTimeRule;
class counselingController extends Controller
{
    private $request; 
    private $html;
    private $counseling;
    private $rowCounter;
    public function __construct(){
        $this->middleware('auth:myuser');
        (new notificationController)->checkCounselingsForNotif();
    } 
    public function getCounseling(){
        $student = student::all();
        $schoolyearid = (new studentController)->getCurrentSchoolYear();
        $schoolyear = schoolYear::find($schoolyearid);
        $schoolyears = schoolYear::all();
      
        $systemdate = systemDate::all(); 
        
        
        $nameofuser = useraccount::find(Auth::guard('myuser')->id());
        

        $user =  (new userController)->getUserDesignation(Auth::guard('myuser')->id());
        $college ='';
        if($user[0]=='college'){
            $college = DB::select(DB::raw("SELECT colleges.collegeName, courses.courseName, 
            courses.id FROM colleges INNER JOIN (SELECT collegeID, userID, id FROM 
            assignment_colleges INNER JOIN (SELECT max(id) AS maxid FROM assignment_colleges 
            GROUP BY userID) ac ON ac.maxid = assignment_colleges.id) ac2 ON ac2.collegeID = 
            colleges.id INNER JOIN (SELECT collegeID, departmentID, id FROM 
            department_college_hists INNER JOIN (SELECT max(id) as maxid FROM 
            department_college_hists GROUP BY departmentID) dch ON dch.maxid = 
            department_college_hists.id) dch2 ON dch2.collegeID = colleges.id 
            INNER JOIN (SELECT courseID, departmentID, id FROM course_department_hists 
            INNER JOIN (SELECT max(id) as maxid FROM course_department_hists GROUP BY courseID) 
            cdh ON cdh.maxid = course_department_hists.id) cdh2 ON cdh2.departmentID = 
            dch2.departmentID INNER JOIN courses on courses.id  = cdh2.courseID WHERE 
            ac2.collegeID = (SELECT collegeID FROM assignment_colleges WHERE id = 
            (SELECT max(id) FROM assignment_colleges WHERE userID ='".Auth::guard('myuser')->id()."'))
            GROUP BY courses.id"));
        }
        $notif = (new notificationController)->getNotifs();
        $currentsem = (new studentController)->getCurrentSem();
        return view('admin/counseling/counseling', ['student' =>$student,
                                                    'schoolyear' =>explode('-', $schoolyear->schoolYearStart)[0].'-'.explode('-', $schoolyear->schoolYearEnd)[0],
                                                    'currentsem' =>$currentsem,
                                                    'schoolyears'=>$schoolyears,
                                                    'systemdate'=>$systemdate[0]->systemDate,
                                                    'nameofuser' => $nameofuser->firstName." ".$nameofuser->lastName,
                                                    'user'=>$user,
                                                    'collegeOfUser'=>$college,
                                                    'notif'=>$notif,
                                                    'resched'=>'false',
                                                    'accept'=>'false',
                                                    'view'=>'false',
                                                    'counselingID'=>'',
                                                    'counselingType'=>'',
                                                    'studentID'=>'']);
    }
    public function getGradeLevel($gradelevel, &$students){
        $mystudent  = new studentController;
        $studenttype;
        foreach ($students as $key => $value) {
            $studenttype = $mystudent->getStudentType($students[$key]->studentID);
            switch ($studenttype) {
                case 'college':
                    $temp = collegeStudent::where('studentID', '=', $students[$key]->studentID)->get();
                    if(count($temp)>0){
                        if($gradelevel!= $temp[0]->courseID){
                            $students->forget($key);
                            
                        }
                    }        
                break;
                case 'seniorhigh':
                    $temp = SHSStudent::where('studentID', '=', $students[$key]->studentID)->get();
                    if(count($temp)>0){
                        if($gradelevel!= $temp[0]->gradeLevel){
                            $students->forget($key);
                           
                        }
                    }     
                break;
                case 'juniorhigh':
                    $temp = JHStudent::where('studentID', '=', $students[$key]->studentID)->get();
                    if(count($temp)>0){
                        if($gradelevel!= $temp[0]->gradeLevel){
                            $students->forget($key);
                         
                        }
                    }     
                break;
                case 'elementary':
                    $temp = elemStudent::where('studentID', '=', $students[$key]->studentID)->get();
                    if(count($temp)>0){
                        if($gradelevel!= $temp[0]->gradeLevel){
                            $students->forget($key);
                            
                        }
                    }     
                break;
                default:
                # code...
                break;
            }
            
        }
    }
    public function getCounselingTotalRow($viewby, $schoolyear, $semester, $courseGradelevel, $search = ''){
        $systemdate = systemDate::all();
        $counseling;
        switch($viewby){
            case 'all':
                if($schoolyear == 'all' && $semester == 'all' && $courseGradelevel == 'all'){
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                            ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                            ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                    'school_years.schoolYearEnd')
                                            ->where(function($query) use($search){
                                                $query->where('students.firstName', 'LIKE', "%$search%");
                                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                                $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                            })
                                            ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                            ->count();
                }elseif ($schoolyear == 'all' && $semester == 'all' && $courseGradelevel != 'all') {
                    $temp = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                            ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                            ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                    'school_years.schoolYearEnd')
                                            ->where(function($query) use($search){
                                                $query->where('students.firstName', 'LIKE', "%$search%");
                                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                                $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                            })
                                            ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                            ->get();
                    $this->getGradeLevel($courseGradelevel, $temp);
                    $counseling = count($temp);
                }elseif ($schoolyear == 'all' && $semester != 'all' && $courseGradelevel == 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                    ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                    ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                            'school_years.schoolYearEnd')
                                    ->where(function($query) use($search){
                                        $query->where('students.firstName', 'LIKE', "%$search%");
                                        $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                        $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                        $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                        $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                    })
                                    ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                    ->where('counselings.semesterID', '=', $semester)
                                    ->count();
                    
                }elseif ($schoolyear == 'all' && $semester != 'all' && $courseGradelevel != 'all') {
                    $temp = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                            ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                            ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                    'school_years.schoolYearEnd')
                                            ->where(function($query) use($search){
                                                $query->where('students.firstName', 'LIKE', "%$search%");
                                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                                $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                            })
                                            ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                            ->where('counselings.semesterID', '=', $semester)
                                            ->get();
                    $this->getGradeLevel($courseGradelevel, $temp);
                    $counseling = count($temp);
                }elseif ($schoolyear != 'all' && $semester == 'all' && $courseGradelevel == 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                            ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                            ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                    'school_years.schoolYearEnd')
                                            ->where(function($query) use($search){
                                                $query->where('students.firstName', 'LIKE', "%$search%");
                                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                                $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                            })
                                            ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                            ->where('counselings.schoolYearID', '=', $schoolyear)
                                            ->count();
                }elseif ($schoolyear != 'all' && $semester == 'all' && $courseGradelevel != 'all') {
                    $temp = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                            ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                            ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                    'school_years.schoolYearEnd')
                                            ->where(function($query) use($search){
                                                $query->where('students.firstName', 'LIKE', "%$search%");
                                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                                $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                            })
                                            ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                            ->where('counselings.schoolYearID', '=', $schoolyear)
                                            ->get();
                    $this->getGradeLevel($courseGradelevel, $temp);
                    $counseling = count($temp);
                }elseif ($schoolyear != 'all' && $semester != 'all' && $courseGradelevel == 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                            ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                            ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                    'school_years.schoolYearEnd')
                                            ->where(function($query) use($search){
                                                $query->where('students.firstName', 'LIKE', "%$search%");
                                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                                $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                            })
                                            ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                            ->where('counselings.schoolYearID', '=', $schoolyear)
                                            ->where('counselings.semesterID', '=', $semester)
                                            ->count();
                }elseif ($schoolyear != 'all' && $semester != 'all' && $courseGradelevel != 'all') {
                    $temp = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                            ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                            ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                    'school_years.schoolYearEnd')
                                            ->where(function($query) use($search){
                                                $query->where('students.firstName', 'LIKE', "%$search%");
                                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                                $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                            })
                                            ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                            ->where('counselings.schoolYearID', '=', $schoolyear)
                                            ->where('counselings.semesterID', '=', $semester)
                                            ->get();
                    $this->getGradeLevel($courseGradelevel, $temp);
                    $counseling = count($temp);
                }else{
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                    ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                    ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                            'school_years.schoolYearEnd')
                    ->where(function($query) use($search){
                        $query->where('students.firstName', 'LIKE', "%$search%");
                        $query->orWhere('students.lastName', 'LIKE', "%$search%");
                        $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                        $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                        $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                    })
                    ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                    ->count();
                }       
                
            break;
            case 'college':
                if($schoolyear == 'all' && $semester == 'all' && $courseGradelevel == 'all'){
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                        ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                        ->join('college_students', 'college_students.studentID', '=', 'students.id')
                                        ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                'school_years.schoolYearEnd')
                                        ->where(function($query) use($search){
                                            $query->where('students.firstName', 'LIKE', "%$search%");
                                            $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                            $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                            $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                            $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                        })
                                        ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                        ->count();
                }elseif ($schoolyear == 'all' && $semester == 'all' && $courseGradelevel != 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                        ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                        ->join('college_students', 'college_students.studentID', '=', 'students.id')
                                        ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                'school_years.schoolYearEnd')
                                        ->where(function($query) use($search){
                                            $query->where('students.firstName', 'LIKE', "%$search%");
                                            $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                            $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                            $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                            $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                        })
                                        ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                        ->where('college_students.courseID', '=', $courseGradelevel)
                                        ->count();
                }elseif ($schoolyear == 'all' && $semester != 'all' && $courseGradelevel == 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                        ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                        ->join('college_students', 'college_students.studentID', '=', 'students.id')
                                        ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                'school_years.schoolYearEnd')
                                        ->where(function($query) use($search){
                                            $query->where('students.firstName', 'LIKE', "%$search%");
                                            $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                            $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                            $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                            $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                        })
                                        ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                        ->where('counselings.semesterID', '=', $semester)
                                        ->count();
                }elseif ($schoolyear == 'all' && $semester != 'all' && $courseGradelevel != 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                        ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                        ->join('college_students', 'college_students.studentID', '=', 'students.id')
                                        ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                'school_years.schoolYearEnd')
                                        ->where(function($query) use($search){
                                            $query->where('students.firstName', 'LIKE', "%$search%");
                                            $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                            $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                            $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                            $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                        })
                                        ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                        ->where('counselings.semesterID', '=', $semester)
                                        ->where('college_students.courseID', '=', $courseGradelevel)
                                        ->count();
                }elseif ($schoolyear != 'all' && $semester == 'all' && $courseGradelevel == 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                            ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                            ->join('college_students', 'college_students.studentID', '=', 'students.id')
                                            ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                    'school_years.schoolYearEnd')
                                            ->where(function($query) use($search){
                                                $query->where('students.firstName', 'LIKE', "%$search%");
                                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                                $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                            })
                                            ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                            ->where('counselings.schoolYearID', '=', $schoolyear)
                                            ->count();
                }elseif ($schoolyear != 'all' && $semester == 'all' && $courseGradelevel != 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                        ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                        ->join('college_students', 'college_students.studentID', '=', 'students.id')
                                        ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                'school_years.schoolYearEnd')
                                        ->where(function($query) use($search){
                                            $query->where('students.firstName', 'LIKE', "%$search%");
                                            $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                            $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                            $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                            $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                        })
                                        ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                        ->where('counselings.schoolYearID', '=', $schoolyear)
                                        ->where('college_students.courseID', '=', $courseGradelevel)
                                        ->count();
                }elseif ($schoolyear != 'all' && $semester != 'all' && $courseGradelevel == 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                        ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                        ->join('college_students', 'college_students.studentID', '=', 'students.id')
                                        ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                'school_years.schoolYearEnd')
                                        ->where(function($query) use($search){
                                            $query->where('students.firstName', 'LIKE', "%$search%");
                                            $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                            $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                            $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                            $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                        })
                                        ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                        ->where('counselings.schoolYearID', '=', $schoolyear)
                                        ->where('counselings.semesterID', '=', $semester)
                                        ->count();
                }elseif ($schoolyear != 'all' && $semester != 'all' && $courseGradelevel != 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                            ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                            ->join('college_students', 'college_students.studentID', '=', 'students.id')
                                            ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                    'school_years.schoolYearEnd')
                                            ->where(function($query) use($search){
                                                $query->where('students.firstName', 'LIKE', "%$search%");
                                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                                $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                            })
                                            ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                            ->where('counselings.schoolYearID', '=', $schoolyear)
                                            ->where('counselings.semesterID', '=', $semester)
                                            ->where('college_students.courseID', '=', $courseGradelevel)
                                            ->count();
                }else{
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                            ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                            ->join('college_students', 'college_students.studentID', '=', 'students.id')
                                            ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                    'school_years.schoolYearEnd')
                                            ->where(function($query) use($search){
                                                $query->where('students.firstName', 'LIKE', "%$search%");
                                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                                $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                            })
                                            ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                            ->count();
                }       
            
                
            break;
            case 'seniorhigh':
                if($schoolyear == 'all' && $semester == 'all' && $courseGradelevel == 'all'){
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                        ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                        ->join('s_h_s_students', 's_h_s_students.studentID', '=', 'students.id')
                                        ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                'school_years.schoolYearEnd')
                                        ->where(function($query) use($search){
                                            $query->where('students.firstName', 'LIKE', "%$search%");
                                            $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                            $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                            $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                            $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                        })
                                        ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                        ->count();
                }elseif ($schoolyear == 'all' && $semester == 'all' && $courseGradelevel != 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                        ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                        ->join('s_h_s_students', 's_h_s_students.studentID', '=', 'students.id')
                                        ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                'school_years.schoolYearEnd')
                                        ->where(function($query) use($search){
                                            $query->where('students.firstName', 'LIKE', "%$search%");
                                            $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                            $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                            $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                            $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                        })
                                        ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                        ->where('college_students.yearLevel', '=', $courseGradelevel)
                                        ->count();
                }elseif ($schoolyear == 'all' && $semester != 'all' && $courseGradelevel == 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                        ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                        ->join('s_h_s_students', 's_h_s_students.studentID', '=', 'students.id')
                                        ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                'school_years.schoolYearEnd')
                                        ->where(function($query) use($search){
                                            $query->where('students.firstName', 'LIKE', "%$search%");
                                            $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                            $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                            $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                            $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                        })
                                        ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                        ->where('counselings.semesterID', '=', $semester)
                                        ->count();
                }elseif ($schoolyear == 'all' && $semester != 'all' && $courseGradelevel != 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                        ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                        ->join('s_h_s_students', 's_h_s_students.studentID', '=', 'students.id')
                                        ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                'school_years.schoolYearEnd')
                                        ->where(function($query) use($search){
                                            $query->where('students.firstName', 'LIKE', "%$search%");
                                            $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                            $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                            $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                            $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                        })
                                        ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                        ->where('counselings.semesterID', '=', $semester)
                                        ->where('college_students.yearLevel', '=', $courseGradelevel)
                                        ->count();
                }elseif ($schoolyear != 'all' && $semester == 'all' && $courseGradelevel == 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                            ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                            ->join('s_h_s_students', 's_h_s_students.studentID', '=', 'students.id')
                                            ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                    'school_years.schoolYearEnd')
                                            ->where(function($query) use($search){
                                                $query->where('students.firstName', 'LIKE', "%$search%");
                                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                                $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                            })
                                            ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                            ->where('counselings.schoolYearID', '=', $schoolyear)
                                            ->count();
                }elseif ($schoolyear != 'all' && $semester == 'all' && $courseGradelevel != 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                        ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                        ->join('s_h_s_students', 's_h_s_students.studentID', '=', 'students.id')
                                        ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                'school_years.schoolYearEnd')
                                        ->where(function($query) use($search){
                                            $query->where('students.firstName', 'LIKE', "%$search%");
                                            $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                            $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                            $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                            $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                        })
                                        ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                        ->where('counselings.schoolYearID', '=', $schoolyear)
                                        ->where('college_students.yearLevel', '=', $courseGradelevel)
                                        ->count();
                }elseif ($schoolyear != 'all' && $semester != 'all' && $courseGradelevel == 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                        ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                        ->join('s_h_s_students', 's_h_s_students.studentID', '=', 'students.id')
                                        ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                'school_years.schoolYearEnd')
                                        ->where(function($query) use($search){
                                            $query->where('students.firstName', 'LIKE', "%$search%");
                                            $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                            $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                            $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                            $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                        })
                                        ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                        ->where('counselings.schoolYearID', '=', $schoolyear)
                                        ->where('counselings.semesterID', '=', $semester)
                                        ->count();
                }elseif ($schoolyear != 'all' && $semester != 'all' && $courseGradelevel != 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                            ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                            ->join('s_h_s_students', 's_h_s_students.studentID', '=', 'students.id')
                                            ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                    'school_years.schoolYearEnd')
                                            ->where(function($query) use($search){
                                                $query->where('students.firstName', 'LIKE', "%$search%");
                                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                                $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                            })
                                            ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                            ->where('counselings.schoolYearID', '=', $schoolyear)
                                            ->where('counselings.semesterID', '=', $semester)
                                            ->where('college_students.yearLevel', '=', $courseGradelevel)
                                            ->count();
                }else{
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                            ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                            ->join('s_h_s_students', 's_h_s_students.studentID', '=', 'students.id')
                                            ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                    'school_years.schoolYearEnd')
                                            ->where(function($query) use($search){
                                                $query->where('students.firstName', 'LIKE', "%$search%");
                                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                                $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                            })
                                            ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                            ->count();
                }       
            break;
            case 'juniorhigh':
                if($schoolyear == 'all' && $semester == 'all' && $courseGradelevel == 'all'){
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                        ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                        ->join('j_h_students', 'j_h_students.studentID', '=', 'students.id')
                                        ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                'school_years.schoolYearEnd')
                                        ->where(function($query) use($search){
                                            $query->where('students.firstName', 'LIKE', "%$search%");
                                            $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                            $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                            $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                            $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                        })
                                        ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                        ->count();
                }elseif ($schoolyear == 'all' && $semester == 'all' && $courseGradelevel != 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                        ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                        ->join('j_h_students', 'j_h_students.studentID', '=', 'students.id')
                                        ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                'school_years.schoolYearEnd')
                                        ->where(function($query) use($search){
                                            $query->where('students.firstName', 'LIKE', "%$search%");
                                            $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                            $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                            $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                            $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                        })
                                        ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                        ->where('college_students.yearLevel', '=', $courseGradelevel)
                                        ->count();
                }elseif ($schoolyear == 'all' && $semester != 'all' && $courseGradelevel == 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                        ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                        ->join('j_h_students', 'j_h_students.studentID', '=', 'students.id')
                                        ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                'school_years.schoolYearEnd')
                                        ->where(function($query) use($search){
                                            $query->where('students.firstName', 'LIKE', "%$search%");
                                            $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                            $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                            $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                            $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                        })
                                        ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                        ->where('counselings.semesterID', '=', $semester)
                                        ->count();
                }elseif ($schoolyear == 'all' && $semester != 'all' && $courseGradelevel != 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                        ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                        ->join('j_h_students', 'j_h_students.studentID', '=', 'students.id')
                                        ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                'school_years.schoolYearEnd')
                                        ->where(function($query) use($search){
                                            $query->where('students.firstName', 'LIKE', "%$search%");
                                            $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                            $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                            $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                            $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                        })
                                        ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                        ->where('counselings.semesterID', '=', $semester)
                                        ->where('college_students.yearLevel', '=', $courseGradelevel)
                                        ->count();
                }elseif ($schoolyear != 'all' && $semester == 'all' && $courseGradelevel == 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                            ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                            ->join('j_h_students', 'j_h_students.studentID', '=', 'students.id')
                                            ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                    'school_years.schoolYearEnd')
                                            ->where(function($query) use($search){
                                                $query->where('students.firstName', 'LIKE', "%$search%");
                                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                                $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                            })
                                            ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                            ->where('counselings.schoolYearID', '=', $schoolyear)
                                            ->count();
                }elseif ($schoolyear != 'all' && $semester == 'all' && $courseGradelevel != 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                        ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                        ->join('j_h_students', 'j_h_students.studentID', '=', 'students.id')
                                        ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                'school_years.schoolYearEnd')
                                        ->where(function($query) use($search){
                                            $query->where('students.firstName', 'LIKE', "%$search%");
                                            $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                            $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                            $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                            $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                        })
                                        ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                        ->where('counselings.schoolYearID', '=', $schoolyear)
                                        ->where('college_students.yearLevel', '=', $courseGradelevel)
                                        ->count();
                }elseif ($schoolyear != 'all' && $semester != 'all' && $courseGradelevel == 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                        ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                        ->join('j_h_students', 'j_h_students.studentID', '=', 'students.id')
                                        ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                'school_years.schoolYearEnd')
                                        ->where(function($query) use($search){
                                            $query->where('students.firstName', 'LIKE', "%$search%");
                                            $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                            $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                            $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                            $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                        })
                                        ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                        ->where('counselings.schoolYearID', '=', $schoolyear)
                                        ->where('counselings.semesterID', '=', $semester)
                                        ->count();
                }elseif ($schoolyear != 'all' && $semester != 'all' && $courseGradelevel != 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                            ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                            ->join('j_h_students', 'j_h_students.studentID', '=', 'students.id')
                                            ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                    'school_years.schoolYearEnd')
                                            ->where(function($query) use($search){
                                                $query->where('students.firstName', 'LIKE', "%$search%");
                                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                                $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                            })
                                            ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                            ->where('counselings.schoolYearID', '=', $schoolyear)
                                            ->where('counselings.semesterID', '=', $semester)
                                            ->where('college_students.yearLevel', '=', $courseGradelevel)
                                            ->count();
                }else{
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                            ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                            ->join('j_h_students', 'j_h_students.studentID', '=', 'students.id')
                                            ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                    'school_years.schoolYearEnd')
                                            ->where(function($query) use($search){
                                                $query->where('students.firstName', 'LIKE', "%$search%");
                                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                                $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                            })
                                            ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                            ->count();
                } 
                
            break;
            case 'elementary':
                if($schoolyear == 'all' && $semester == 'all' && $courseGradelevel == 'all'){
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                        ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                        ->join('elem_students', 'elem_students.studentID', '=', 'students.id')
                                        ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                'school_years.schoolYearEnd')
                                        ->where(function($query) use($search){
                                            $query->where('students.firstName', 'LIKE', "%$search%");
                                            $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                            $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                            $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                            $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                        })
                                        ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                        ->count();
                }elseif ($schoolyear == 'all' && $semester == 'all' && $courseGradelevel != 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                        ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                        ->join('elem_students', 'elem_students.studentID', '=', 'students.id')
                                        ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                'school_years.schoolYearEnd')
                                        ->where(function($query) use($search){
                                            $query->where('students.firstName', 'LIKE', "%$search%");
                                            $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                            $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                            $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                            $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                        })
                                        ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                        ->where('college_students.yearLevel', '=', $courseGradelevel)
                                        ->count();
                }elseif ($schoolyear == 'all' && $semester != 'all' && $courseGradelevel == 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                        ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                        ->join('elem_students', 'elem_students.studentID', '=', 'students.id')
                                        ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                'school_years.schoolYearEnd')
                                        ->where(function($query) use($search){
                                            $query->where('students.firstName', 'LIKE', "%$search%");
                                            $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                            $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                            $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                            $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                        })
                                        ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                        ->where('counselings.semesterID', '=', $semester)
                                        ->count();
                }elseif ($schoolyear == 'all' && $semester != 'all' && $courseGradelevel != 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                        ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                        ->join('elem_students', 'elem_students.studentID', '=', 'students.id')
                                        ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                'school_years.schoolYearEnd')
                                        ->where(function($query) use($search){
                                            $query->where('students.firstName', 'LIKE', "%$search%");
                                            $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                            $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                            $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                            $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                        })
                                        ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                        ->where('counselings.semesterID', '=', $semester)
                                        ->where('college_students.yearLevel', '=', $courseGradelevel)
                                        ->count();
                }elseif ($schoolyear != 'all' && $semester == 'all' && $courseGradelevel == 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                            ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                            ->join('elem_students', 'elem_students.studentID', '=', 'students.id')
                                            ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                    'school_years.schoolYearEnd')
                                            ->where(function($query) use($search){
                                                $query->where('students.firstName', 'LIKE', "%$search%");
                                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                                $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                            })
                                            ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                            ->where('counselings.schoolYearID', '=', $schoolyear)
                                            ->count();
                }elseif ($schoolyear != 'all' && $semester == 'all' && $courseGradelevel != 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                        ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                        ->join('elem_students', 'elem_students.studentID', '=', 'students.id')
                                        ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                'school_years.schoolYearEnd')
                                        ->where(function($query) use($search){
                                            $query->where('students.firstName', 'LIKE', "%$search%");
                                            $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                            $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                            $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                            $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                        })
                                        ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                        ->where('counselings.schoolYearID', '=', $schoolyear)
                                        ->where('college_students.yearLevel', '=', $courseGradelevel)
                                        ->count();
                }elseif ($schoolyear != 'all' && $semester != 'all' && $courseGradelevel == 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                        ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                        ->join('elem_students', 'elem_students.studentID', '=', 'students.id')
                                        ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                'school_years.schoolYearEnd')
                                        ->where(function($query) use($search){
                                            $query->where('students.firstName', 'LIKE', "%$search%");
                                            $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                            $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                            $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                            $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                        })
                                        ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                        ->where('counselings.schoolYearID', '=', $schoolyear)
                                        ->where('counselings.semesterID', '=', $semester)
                                        ->count();
                }elseif ($schoolyear != 'all' && $semester != 'all' && $courseGradelevel != 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                            ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                            ->join('elem_students', 'elem_students.studentID', '=', 'students.id')
                                            ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                    'school_years.schoolYearEnd')
                                            ->where(function($query) use($search){
                                                $query->where('students.firstName', 'LIKE', "%$search%");
                                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                                $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                            })
                                            ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                            ->where('counselings.schoolYearID', '=', $schoolyear)
                                            ->where('counselings.semesterID', '=', $semester)
                                            ->where('college_students.yearLevel', '=', $courseGradelevel)
                                            ->count();
                }else{
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                            ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                            ->join('elem_students', 'elem_students.studentID', '=', 'students.id')
                                            ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                    'school_years.schoolYearEnd')
                                            ->where(function($query) use($search){
                                                $query->where('students.firstName', 'LIKE', "%$search%");
                                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                                $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                            })
                                            ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                            ->count();
                } 
            break;
        }
        return $counseling;
    }
    public function getCounselingPage($page,$viewby, $schoolyear, $semester, $courseGradelevel, $search = ''){
        $systemdate = systemDate::all();
        $skip = ($page-1)*10;
        $counseling;
        switch($viewby){
            case 'all':
                if($schoolyear == 'all' && $semester == 'all' && $courseGradelevel == 'all'){
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                            ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                            ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                    'school_years.schoolYearEnd')
                                            ->where(function($query) use($search){
                                                $query->where('students.firstName', 'LIKE', "%$search%");
                                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                                $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                            })
                                            ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                            ->limit(10)->offset($skip)
                                            ->get();
                }elseif ($schoolyear == 'all' && $semester == 'all' && $courseGradelevel != 'all') {
                    $temp = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                            ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                            ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                    'school_years.schoolYearEnd')
                                            ->where(function($query) use($search){
                                                $query->where('students.firstName', 'LIKE', "%$search%");
                                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                                $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                            })
                                            ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                            ->limit(10)->offset($skip)
                                            ->get();
                    $this->getGradeLevel($courseGradelevel, $temp);
                    $counseling = $temp;
                }elseif ($schoolyear == 'all' && $semester != 'all' && $courseGradelevel == 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                    ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                    ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                            'school_years.schoolYearEnd')
                                    ->where(function($query) use($search){
                                        $query->where('students.firstName', 'LIKE', "%$search%");
                                        $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                        $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                        $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                        $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                    })
                                    ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                    ->where('counselings.semesterID', '=', $semester)
                                    ->limit(10)->offset($skip)
                                    ->get();
                    
                }elseif ($schoolyear == 'all' && $semester != 'all' && $courseGradelevel != 'all') {
                    $temp = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                            ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                            ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                    'school_years.schoolYearEnd')
                                            ->where(function($query) use($search){
                                                $query->where('students.firstName', 'LIKE', "%$search%");
                                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                                $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                            })
                                            ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                            ->where('counselings.semesterID', '=', $semester)
                                            ->limit(10)->offset($skip)
                                            ->get();
                    $this->getGradeLevel($courseGradelevel, $temp);
                    $counseling = $temp;
                }elseif ($schoolyear != 'all' && $semester == 'all' && $courseGradelevel == 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                            ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                            ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                    'school_years.schoolYearEnd')
                                            ->where(function($query) use($search){
                                                $query->where('students.firstName', 'LIKE', "%$search%");
                                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                                $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                            })
                                            ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                            ->where('counselings.schoolYearID', '=', $schoolyear)
                                            ->limit(10)->offset($skip)
                                            ->get();
                }elseif ($schoolyear != 'all' && $semester == 'all' && $courseGradelevel != 'all') {
                    $temp = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                            ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                            ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                    'school_years.schoolYearEnd')
                                            ->where(function($query) use($search){
                                                $query->where('students.firstName', 'LIKE', "%$search%");
                                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                                $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                            })
                                            ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                            ->where('counselings.schoolYearID', '=', $schoolyear)
                                            ->limit(10)->offset($skip)
                                            ->get();
                    $this->getGradeLevel($courseGradelevel, $temp);
                    $counseling = $temp;
                }elseif ($schoolyear != 'all' && $semester != 'all' && $courseGradelevel == 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                            ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                            ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                    'school_years.schoolYearEnd')
                                            ->where(function($query) use($search){
                                                $query->where('students.firstName', 'LIKE', "%$search%");
                                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                                $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                            })
                                            ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                            ->where('counselings.schoolYearID', '=', $schoolyear)
                                            ->where('counselings.semesterID', '=', $semester)
                                            ->limit(10)->offset($skip)
                                            ->get();
                }elseif ($schoolyear != 'all' && $semester != 'all' && $courseGradelevel != 'all') {
                    $temp = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                            ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                            ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                    'school_years.schoolYearEnd')
                                            ->where(function($query) use($search){
                                                $query->where('students.firstName', 'LIKE', "%$search%");
                                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                                $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                            })
                                            ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                            ->where('counselings.schoolYearID', '=', $schoolyear)
                                            ->where('counselings.semesterID', '=', $semester)
                                            ->limit(10)->offset($skip)
                                            ->get();
                    $this->getGradeLevel($courseGradelevel, $temp);
                    $counseling = $temp;
                }else{
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                            ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                            ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                    'school_years.schoolYearEnd')
                                            ->where(function($query) use($search){
                                                $query->where('students.firstName', 'LIKE', "%$search%");
                                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                                $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                            })
                                            ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                            ->limit(10)->offset($skip)
                                            ->get();
                }       
                
            break;
            case 'college':
                if($schoolyear == 'all' && $semester == 'all' && $courseGradelevel == 'all'){
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                        ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                        ->join('college_students', 'college_students.studentID', '=', 'students.id')
                                        ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                'school_years.schoolYearEnd')
                                        ->where(function($query) use($search){
                                            $query->where('students.firstName', 'LIKE', "%$search%");
                                            $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                            $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                            $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                            $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                        })
                                        ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                        ->limit(10)->offset($skip)
                                        ->get();
                }elseif ($schoolyear == 'all' && $semester == 'all' && $courseGradelevel != 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                        ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                        ->join('college_students', 'college_students.studentID', '=', 'students.id')
                                        ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                'school_years.schoolYearEnd')
                                        ->where(function($query) use($search){
                                            $query->where('students.firstName', 'LIKE', "%$search%");
                                            $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                            $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                            $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                            $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                        })
                                        ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                        ->where('college_students.courseID', '=', $courseGradelevel)
                                        ->limit(10)->offset($skip)
                                        ->get();
                }elseif ($schoolyear == 'all' && $semester != 'all' && $courseGradelevel == 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                        ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                        ->join('college_students', 'college_students.studentID', '=', 'students.id')
                                        ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                'school_years.schoolYearEnd')
                                        ->where(function($query) use($search){
                                            $query->where('students.firstName', 'LIKE', "%$search%");
                                            $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                            $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                            $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                            $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                        })
                                        ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                        ->where('counselings.semesterID', '=', $semester)
                                        ->limit(10)->offset($skip)
                                        ->get();
                }elseif ($schoolyear == 'all' && $semester != 'all' && $courseGradelevel != 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                        ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                        ->join('college_students', 'college_students.studentID', '=', 'students.id')
                                        ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                'school_years.schoolYearEnd')
                                        ->where(function($query) use($search){
                                            $query->where('students.firstName', 'LIKE', "%$search%");
                                            $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                            $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                            $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                            $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                        })
                                        ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                        ->where('counselings.semesterID', '=', $semester)
                                        ->where('college_students.courseID', '=', $courseGradelevel)
                                        ->limit(10)->offset($skip)
                                        ->get();
                }elseif ($schoolyear != 'all' && $semester == 'all' && $courseGradelevel == 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                            ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                            ->join('college_students', 'college_students.studentID', '=', 'students.id')
                                            ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                    'school_years.schoolYearEnd')
                                            ->where(function($query) use($search){
                                                $query->where('students.firstName', 'LIKE', "%$search%");
                                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                                $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                            })
                                            ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                            ->where('counselings.schoolYearID', '=', $schoolyear)
                                            ->limit(10)->offset($skip)
                                            ->get();
                }elseif ($schoolyear != 'all' && $semester == 'all' && $courseGradelevel != 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                        ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                        ->join('college_students', 'college_students.studentID', '=', 'students.id')
                                        ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                'school_years.schoolYearEnd')
                                        ->where(function($query) use($search){
                                            $query->where('students.firstName', 'LIKE', "%$search%");
                                            $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                            $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                            $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                            $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                        })
                                        ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                        ->where('counselings.schoolYearID', '=', $schoolyear)
                                        ->where('college_students.courseID', '=', $courseGradelevel)
                                        ->limit(10)->offset($skip)
                                        ->get();
                }elseif ($schoolyear != 'all' && $semester != 'all' && $courseGradelevel == 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                        ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                        ->join('college_students', 'college_students.studentID', '=', 'students.id')
                                        ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                'school_years.schoolYearEnd')
                                        ->where(function($query) use($search){
                                            $query->where('students.firstName', 'LIKE', "%$search%");
                                            $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                            $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                            $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                            $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                        })
                                        ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                        ->where('counselings.schoolYearID', '=', $schoolyear)
                                        ->where('counselings.semesterID', '=', $semester)
                                        ->limit(10)->offset($skip)
                                        ->get();
                }elseif ($schoolyear != 'all' && $semester != 'all' && $courseGradelevel != 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                            ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                            ->join('college_students', 'college_students.studentID', '=', 'students.id')
                                            ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                    'school_years.schoolYearEnd')
                                            ->where(function($query) use($search){
                                                $query->where('students.firstName', 'LIKE', "%$search%");
                                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                                $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                            })
                                            ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                            ->where('counselings.schoolYearID', '=', $schoolyear)
                                            ->where('counselings.semesterID', '=', $semester)
                                            ->where('college_students.courseID', '=', $courseGradelevel)
                                            ->limit(10)->offset($skip)
                                            ->get();
                }else{
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                            ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                            ->join('college_students', 'college_students.studentID', '=', 'students.id')
                                            ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                    'school_years.schoolYearEnd')
                                            ->where(function($query) use($search){
                                                $query->where('students.firstName', 'LIKE', "%$search%");
                                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                                $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                            })
                                            ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                            ->limit(10)->offset($skip)
                                            ->get();
                }       
            
                
            break;
            case 'seniorhigh':
                if($schoolyear == 'all' && $semester == 'all' && $courseGradelevel == 'all'){
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                        ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                        ->join('s_h_s_students', 's_h_s_students.studentID', '=', 'students.id')
                                        ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                'school_years.schoolYearEnd')
                                        ->where(function($query) use($search){
                                            $query->where('students.firstName', 'LIKE', "%$search%");
                                            $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                            $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                            $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                            $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                        })
                                        ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                        ->limit(10)->offset($skip)
                                        ->get();
                }elseif ($schoolyear == 'all' && $semester == 'all' && $courseGradelevel != 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                        ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                        ->join('s_h_s_students', 's_h_s_students.studentID', '=', 'students.id')
                                        ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                'school_years.schoolYearEnd')
                                        ->where(function($query) use($search){
                                            $query->where('students.firstName', 'LIKE', "%$search%");
                                            $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                            $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                            $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                            $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                        })
                                        ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                        ->where('college_students.yearLevel', '=', $courseGradelevel)
                                        ->limit(10)->offset($skip)
                                        ->get();
                }elseif ($schoolyear == 'all' && $semester != 'all' && $courseGradelevel == 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                        ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                        ->join('s_h_s_students', 's_h_s_students.studentID', '=', 'students.id')
                                        ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                'school_years.schoolYearEnd')
                                        ->where(function($query) use($search){
                                            $query->where('students.firstName', 'LIKE', "%$search%");
                                            $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                            $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                            $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                            $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                        })
                                        ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                        ->where('counselings.semesterID', '=', $semester)
                                        ->limit(10)->offset($skip)
                                        ->get();
                }elseif ($schoolyear == 'all' && $semester != 'all' && $courseGradelevel != 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                        ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                        ->join('s_h_s_students', 's_h_s_students.studentID', '=', 'students.id')
                                        ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                'school_years.schoolYearEnd')
                                        ->where(function($query) use($search){
                                            $query->where('students.firstName', 'LIKE', "%$search%");
                                            $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                            $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                            $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                            $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                        })
                                        ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                        ->where('counselings.semesterID', '=', $semester)
                                        ->where('college_students.yearLevel', '=', $courseGradelevel)
                                        ->limit(10)->offset($skip)
                                        ->get();
                }elseif ($schoolyear != 'all' && $semester == 'all' && $courseGradelevel == 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                            ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                            ->join('s_h_s_students', 's_h_s_students.studentID', '=', 'students.id')
                                            ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                    'school_years.schoolYearEnd')
                                            ->where(function($query) use($search){
                                                $query->where('students.firstName', 'LIKE', "%$search%");
                                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                                $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                            })
                                            ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                            ->where('counselings.schoolYearID', '=', $schoolyear)
                                            ->limit(10)->offset($skip)
                                            ->get();
                }elseif ($schoolyear != 'all' && $semester == 'all' && $courseGradelevel != 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                        ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                        ->join('s_h_s_students', 's_h_s_students.studentID', '=', 'students.id')
                                        ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                'school_years.schoolYearEnd')
                                        ->where(function($query) use($search){
                                            $query->where('students.firstName', 'LIKE', "%$search%");
                                            $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                            $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                            $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                            $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                        })
                                        ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                        ->where('counselings.schoolYearID', '=', $schoolyear)
                                        ->where('college_students.yearLevel', '=', $courseGradelevel)
                                        ->limit(10)->offset($skip)
                                        ->get();
                }elseif ($schoolyear != 'all' && $semester != 'all' && $courseGradelevel == 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                        ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                        ->join('s_h_s_students', 's_h_s_students.studentID', '=', 'students.id')
                                        ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                'school_years.schoolYearEnd')
                                        ->where(function($query) use($search){
                                            $query->where('students.firstName', 'LIKE', "%$search%");
                                            $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                            $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                            $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                            $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                        })
                                        ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                        ->where('counselings.schoolYearID', '=', $schoolyear)
                                        ->where('counselings.semesterID', '=', $semester)
                                        ->limit(10)->offset($skip)
                                        ->get();
                }elseif ($schoolyear != 'all' && $semester != 'all' && $courseGradelevel != 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                            ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                            ->join('s_h_s_students', 's_h_s_students.studentID', '=', 'students.id')
                                            ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                    'school_years.schoolYearEnd')
                                            ->where(function($query) use($search){
                                                $query->where('students.firstName', 'LIKE', "%$search%");
                                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                                $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                            })
                                            ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                            ->where('counselings.schoolYearID', '=', $schoolyear)
                                            ->where('counselings.semesterID', '=', $semester)
                                            ->where('college_students.yearLevel', '=', $courseGradelevel)
                                            ->limit(10)->offset($skip)
                                            ->get();
                }else{
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                            ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                            ->join('s_h_s_students', 's_h_s_students.studentID', '=', 'students.id')
                                            ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                    'school_years.schoolYearEnd')
                                            ->where(function($query) use($search){
                                                $query->where('students.firstName', 'LIKE', "%$search%");
                                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                                $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                            })
                                            ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                            ->limit(10)->offset($skip)
                                            ->get();
                }       
            break;
            case 'juniorhigh':
                if($schoolyear == 'all' && $semester == 'all' && $courseGradelevel == 'all'){
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                        ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                        ->join('j_h_students', 'j_h_students.studentID', '=', 'students.id')
                                        ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                'school_years.schoolYearEnd')
                                        ->where(function($query) use($search){
                                            $query->where('students.firstName', 'LIKE', "%$search%");
                                            $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                            $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                            $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                            $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                        })
                                        ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                        ->limit(10)->offset($skip)
                                        ->get();
                }elseif ($schoolyear == 'all' && $semester == 'all' && $courseGradelevel != 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                        ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                        ->join('j_h_students', 'j_h_students.studentID', '=', 'students.id')
                                        ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                'school_years.schoolYearEnd')
                                        ->where(function($query) use($search){
                                            $query->where('students.firstName', 'LIKE', "%$search%");
                                            $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                            $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                            $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                            $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                        })
                                        ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                        ->where('college_students.yearLevel', '=', $courseGradelevel)
                                        ->limit(10)->offset($skip)
                                        ->get();
                }elseif ($schoolyear == 'all' && $semester != 'all' && $courseGradelevel == 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                        ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                        ->join('j_h_students', 'j_h_students.studentID', '=', 'students.id')
                                        ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                'school_years.schoolYearEnd')
                                        ->where(function($query) use($search){
                                            $query->where('students.firstName', 'LIKE', "%$search%");
                                            $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                            $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                            $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                            $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                        })
                                        ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                        ->where('counselings.semesterID', '=', $semester)
                                        ->limit(10)->offset($skip)
                                        ->get();
                }elseif ($schoolyear == 'all' && $semester != 'all' && $courseGradelevel != 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                        ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                        ->join('j_h_students', 'j_h_students.studentID', '=', 'students.id')
                                        ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                'school_years.schoolYearEnd')
                                        ->where(function($query) use($search){
                                            $query->where('students.firstName', 'LIKE', "%$search%");
                                            $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                            $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                            $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                            $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                        })
                                        ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                        ->where('counselings.semesterID', '=', $semester)
                                        ->where('college_students.yearLevel', '=', $courseGradelevel)
                                        ->limit(10)->offset($skip)
                                        ->get();
                }elseif ($schoolyear != 'all' && $semester == 'all' && $courseGradelevel == 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                            ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                            ->join('j_h_students', 'j_h_students.studentID', '=', 'students.id')
                                            ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                    'school_years.schoolYearEnd')
                                            ->where(function($query) use($search){
                                                $query->where('students.firstName', 'LIKE', "%$search%");
                                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                                $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                            })
                                            ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                            ->where('counselings.schoolYearID', '=', $schoolyear)
                                            ->limit(10)->offset($skip)
                                            ->get();
                }elseif ($schoolyear != 'all' && $semester == 'all' && $courseGradelevel != 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                        ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                        ->join('j_h_students', 'j_h_students.studentID', '=', 'students.id')
                                        ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                'school_years.schoolYearEnd')
                                        ->where(function($query) use($search){
                                            $query->where('students.firstName', 'LIKE', "%$search%");
                                            $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                            $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                            $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                            $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                        })
                                        ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                        ->where('counselings.schoolYearID', '=', $schoolyear)
                                        ->where('college_students.yearLevel', '=', $courseGradelevel)
                                        ->limit(10)->offset($skip)
                                        ->get();
                }elseif ($schoolyear != 'all' && $semester != 'all' && $courseGradelevel == 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                        ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                        ->join('j_h_students', 'j_h_students.studentID', '=', 'students.id')
                                        ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                'school_years.schoolYearEnd')
                                        ->where(function($query) use($search){
                                            $query->where('students.firstName', 'LIKE', "%$search%");
                                            $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                            $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                            $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                            $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                        })
                                        ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                        ->where('counselings.schoolYearID', '=', $schoolyear)
                                        ->where('counselings.semesterID', '=', $semester)
                                        ->limit(10)->offset($skip)
                                        ->get();
                }elseif ($schoolyear != 'all' && $semester != 'all' && $courseGradelevel != 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                            ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                            ->join('j_h_students', 'j_h_students.studentID', '=', 'students.id')
                                            ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                    'school_years.schoolYearEnd')
                                            ->where(function($query) use($search){
                                                $query->where('students.firstName', 'LIKE', "%$search%");
                                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                                $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                            })
                                            ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                            ->where('counselings.schoolYearID', '=', $schoolyear)
                                            ->where('counselings.semesterID', '=', $semester)
                                            ->where('college_students.yearLevel', '=', $courseGradelevel)
                                            ->limit(10)->offset($skip)
                                            ->get();
                }else{
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                            ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                            ->join('j_h_students', 'j_h_students.studentID', '=', 'students.id')
                                            ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                    'school_years.schoolYearEnd')
                                            ->where(function($query) use($search){
                                                $query->where('students.firstName', 'LIKE', "%$search%");
                                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                                $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                            })
                                            ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                            ->limit(10)->offset($skip)
                                            ->get();
                } 
                
            break;
            case 'elementary':
                if($schoolyear == 'all' && $semester == 'all' && $courseGradelevel == 'all'){
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                        ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                        ->join('elem_students', 'elem_students.studentID', '=', 'students.id')
                                        ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                'school_years.schoolYearEnd')
                                        ->where(function($query) use($search){
                                            $query->where('students.firstName', 'LIKE', "%$search%");
                                            $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                            $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                            $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                            $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                        })
                                        ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                        ->limit(10)->offset($skip)
                                        ->get();
                }elseif ($schoolyear == 'all' && $semester == 'all' && $courseGradelevel != 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                        ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                        ->join('elem_students', 'elem_students.studentID', '=', 'students.id')
                                        ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                'school_years.schoolYearEnd')
                                        ->where(function($query) use($search){
                                            $query->where('students.firstName', 'LIKE', "%$search%");
                                            $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                            $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                            $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                            $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                        })
                                        ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                        ->where('college_students.yearLevel', '=', $courseGradelevel)
                                        ->limit(10)->offset($skip)
                                        ->get();
                }elseif ($schoolyear == 'all' && $semester != 'all' && $courseGradelevel == 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                        ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                        ->join('elem_students', 'elem_students.studentID', '=', 'students.id')
                                        ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                'school_years.schoolYearEnd')
                                        ->where(function($query) use($search){
                                            $query->where('students.firstName', 'LIKE', "%$search%");
                                            $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                            $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                            $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                            $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                        })
                                        ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                        ->where('counselings.semesterID', '=', $semester)
                                        ->limit(10)->offset($skip)
                                        ->get();
                }elseif ($schoolyear == 'all' && $semester != 'all' && $courseGradelevel != 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                        ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                        ->join('elem_students', 'elem_students.studentID', '=', 'students.id')
                                        ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                'school_years.schoolYearEnd')
                                        ->where(function($query) use($search){
                                            $query->where('students.firstName', 'LIKE', "%$search%");
                                            $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                            $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                            $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                            $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                        })
                                        ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                        ->where('counselings.semesterID', '=', $semester)
                                        ->where('college_students.yearLevel', '=', $courseGradelevel)
                                        ->limit(10)->offset($skip)
                                        ->get();
                }elseif ($schoolyear != 'all' && $semester == 'all' && $courseGradelevel == 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                            ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                            ->join('elem_students', 'elem_students.studentID', '=', 'students.id')
                                            ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                    'school_years.schoolYearEnd')
                                            ->where(function($query) use($search){
                                                $query->where('students.firstName', 'LIKE', "%$search%");
                                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                                $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                            })
                                            ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                            ->where('counselings.schoolYearID', '=', $schoolyear)
                                            ->limit(10)->offset($skip)
                                            ->get();
                }elseif ($schoolyear != 'all' && $semester == 'all' && $courseGradelevel != 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                        ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                        ->join('elem_students', 'elem_students.studentID', '=', 'students.id')
                                        ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                'school_years.schoolYearEnd')
                                        ->where(function($query) use($search){
                                            $query->where('students.firstName', 'LIKE', "%$search%");
                                            $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                            $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                            $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                            $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                        })
                                        ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                        ->where('counselings.schoolYearID', '=', $schoolyear)
                                        ->where('college_students.yearLevel', '=', $courseGradelevel)
                                        ->limit(10)->offset($skip)
                                        ->get();
                }elseif ($schoolyear != 'all' && $semester != 'all' && $courseGradelevel == 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                        ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                        ->join('elem_students', 'elem_students.studentID', '=', 'students.id')
                                        ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                'school_years.schoolYearEnd')
                                        ->where(function($query) use($search){
                                            $query->where('students.firstName', 'LIKE', "%$search%");
                                            $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                            $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                            $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                            $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                        })
                                        ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                        ->where('counselings.schoolYearID', '=', $schoolyear)
                                        ->where('counselings.semesterID', '=', $semester)
                                        ->limit(10)->offset($skip)
                                        ->get();
                }elseif ($schoolyear != 'all' && $semester != 'all' && $courseGradelevel != 'all') {
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                            ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                            ->join('elem_students', 'elem_students.studentID', '=', 'students.id')
                                            ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                    'school_years.schoolYearEnd')
                                            ->where(function($query) use($search){
                                                $query->where('students.firstName', 'LIKE', "%$search%");
                                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                                $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                            })
                                            ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                            ->where('counselings.schoolYearID', '=', $schoolyear)
                                            ->where('counselings.semesterID', '=', $semester)
                                            ->where('college_students.yearLevel', '=', $courseGradelevel)
                                            ->limit(10)->offset($skip)
                                            ->get();
                }else{
                    $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
                                            ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
                                            ->join('elem_students', 'elem_students.studentID', '=', 'students.id')
                                            ->select('counselings.*', 'students.firstName','students.lastName','school_years.schoolYearStart',
                                                    'school_years.schoolYearEnd')
                                            ->where(function($query) use($search){
                                                $query->where('students.firstName', 'LIKE', "%$search%");
                                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                                $query->orWhere('counselings.counselingType', 'LIKE', "$search%");
                                            })
                                            ->whereDate('counselings.created_at', '<=',  $systemdate[0]->systemDate)
                                            ->limit(10)->offset($skip)
                                            ->get();
                } 
            break;
        }
        if(count($counseling)==0){
            return "<h2>There are no  counselings found!";
        }else{
            $html = '';
            $this->rowCounter = 0;
            foreach ($counseling as $counseling) {
                $this->rowCounter++;
                $html .= $this->getCounselingRow($counseling);
            }
            return $html;
        }
    }
    public function getCounselingFaster($page,$viewby, $schoolyear, $semester, $coursegrade, $totalRow, $search = ''){
        $user = new userController();
        $systemdate = systemDate::all();
        $sql='';
        $skip = ($page-1)*10;
        if(session()->get('user')['userType']=='admin'){
            $sql = "SELECT COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) 
                    semesterID, COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) 
                    schoolYearID, COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) studentTypes,
                    col.studentType, shs.studentType, jhs.studentType, elem.studentType,
                    COALESCE(col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) courseGrade, 
                    col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel,
                    COALESCE(col.studentNo, shs.studNo, jhs.studNo, elem.studNo) studentNo,  students.firstName, students.lastName,
                    students.middleName,
                    col.courseName, col.yearLevel,
                    school_years.schoolYearStart,
                    school_years.schoolYearEnd,
                    counselings.*
                    FROM students LEFT JOIN (SELECT college_students.studentID, 
                    college_students.yearLevel, college_students.semesterID, college_students.schoolYearID,
                    college_students.id, 'college' AS studentType, 
                    college_students.courseID, courses.courseName, college_students.studentNo FROM
                    college_students INNER JOIN (SELECT max(id) as maxid FROM college_students GROUP 
                    BY studentID) cs ON cs.maxid = college_students.id
                    INNER JOIN courses ON courses.id = college_students.courseID) col ON 
                    col.studentID = students.id LEFT 
                    JOIN (SELECT studentID, semesterID, schoolYearID, id, 'seniorhigh' AS studentType, 
                    gradeLevel, studNo 
                    FROM s_h_s_students INNER JOIN (SELECT max(id) as maxid FROM s_h_s_students GROUP BY studentID) 
                    shs ON shs.maxid = s_h_s_students.id) shs ON shs.studentID = students.id
                    LEFT JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'juniorhigh' AS 
                    studentType, gradeLevel, studNo  FROM j_h_students INNER JOIN (SELECT max(id) as maxid FROM j_h_students 
                    GROUP BY studentID) jhs ON jhs.maxid = j_h_students.id) jhs ON jhs.studentID = students.id LEFT 
                    JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'elementary' AS studentType, 
                    gradeLevel, studNo FROM elem_students INNER JOIN (SELECT max(id) as maxid FROM elem_students GROUP 
                    BY studentID) elem ON elem.maxid = elem_students.id) elem ON elem.studentID = students.id
                    INNER JOIN counselings ON counselings.studentID =  students.id
                    INNER JOIN school_years ON school_years.id = counselings.schoolYearID
                    LEFT JOIN follow_ups ON follow_ups.counselingID = counselings.id
                    LEFT JOIN evaluations ON evaluations.counselingID = counselings.id
            WHERE (students.firstName LIKE '%$search%' OR students.lastName LIKE '%$search%'
                    OR evaluations.remarks LIKE '%$search%' OR follow_ups.details LIKE '%$search%'
                    OR counselings.statementOfTheProblem LIKE '%$search%') 
            AND DATE(students.created_at) <= '".$systemdate[0]->systemDate."'";
            switch ($viewby) {
                case 'college':
                    $sql .= " AND col.studentType = 'college'";
                break;
                case 'seniorhigh':
                    $sql .= " AND shs.studentType = 'seniorhigh'";
                break;
                case 'juniorhigh':
                    $sql .= " AND jhs.studentType = 'juniorhigh'";
                break;
                case 'elementary':
                    $sql .= " AND elem.studentType = 'elementary'";
                break;
                default:
                   
                break;
            }
            if($schoolyear!="all"){
                $sql .= " AND counselings.schoolYearID = '$schoolyear'";
            }
            if($semester!="all"){
                $sql .= " AND counselings.semesterID = '$semester'";
            }
            if($coursegrade!="all"){
                switch ($viewby) {
                    case 'college':
                        $sql .= " AND col.courseID = '$coursegrade'";
                    break;
                    case 'seniorhigh':
                        $sql .= "  AND shs.gradeLevel = '$coursegrade'";
                    break;
                    case 'juniorhigh':
                        $sql .= "  AND jhs.gradeLevel = '$coursegrade'";
                    break;
                    case 'elementary':
                        $sql .= "  AND elem.gradeLevel = '$coursegrade'";
                    break;
                    default:
                        $sql .= " AND COALESCE(col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) = '$coursegrade'";
                    break;
                }
            }
        }else{
            $designation = $user->getUserDesignation(Auth::guard('myuser')->id());
            $sql = "SELECT COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) 
                    semesterID, COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) 
                    schoolYearID, COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) studentTypes,
                    col.studentType, shs.studentType, jhs.studentType, elem.studentType, 
                    COALESCE(col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) courseGrade, 
                    col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel,
                    COALESCE(col.studentNo, shs.studNo, jhs.studNo, elem.studNo) studentNo, students.firstName, students.lastName,
                    students.middleName,
                    school_years.schoolYearStart,
                    school_years.schoolYearEnd,
                    counselings.*,
                    col.courseName, col.yearLevel
                    FROM students LEFT JOIN (SELECT college_students.studentID, 
                    college_students.yearLevel, college_students.semesterID, college_students.schoolYearID,
                    college_students.id, 'college' AS studentType, 
                    college_students.courseID, courses.courseName, college_students.studentNo FROM
                    college_students INNER JOIN (SELECT max(id) as maxid FROM college_students GROUP 
                    BY studentID) cs ON cs.maxid = college_students.id
                    INNER JOIN courses ON courses.id = college_students.courseID) col ON 
                    col.studentID = students.id LEFT 
                    JOIN (SELECT studentID, semesterID, schoolYearID, id, 'seniorhigh' AS studentType, 
                    gradeLevel, studNo 
                    FROM s_h_s_students INNER JOIN (SELECT max(id) as maxid FROM s_h_s_students GROUP BY studentID) 
                    shs ON shs.maxid = s_h_s_students.id) shs ON shs.studentID = students.id
                    LEFT JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'juniorhigh' AS 
                    studentType, gradeLevel, studNo  FROM j_h_students INNER JOIN (SELECT max(id) as maxid FROM j_h_students 
                    GROUP BY studentID) jhs ON jhs.maxid = j_h_students.id) jhs ON jhs.studentID = students.id LEFT 
                    JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'elementary' AS studentType, 
                    gradeLevel, studNo FROM elem_students INNER JOIN (SELECT max(id) as maxid FROM elem_students GROUP 
                    BY studentID) elem ON elem.maxid = elem_students.id) elem ON elem.studentID = students.id
                    INNER JOIN counselings ON counselings.studentID =  students.id
                    INNER JOIN school_years ON school_years.id = counselings.schoolYearID
                    WHERE (students.firstName LIKE '%$search%' OR students.lastName LIKE '%$search%') 
                    AND DATE(students.created_at) <= '".$systemdate[0]->systemDate."'";
            switch($designation[0]){
                case 'college':
                    $sql= "SELECT cs2.courseID AS courseGrade, c2.courseName, cs2.schoolYearID, cs2.studentNo, 
                    cs2.yearLevel, students.firstName, students.lastName, 'college' AS studentTypes, cs2.semesterID,
                    counselings.*,
                    students.middleName,
                    school_years.schoolYearStart,
                    school_years.schoolYearEnd  FROM students 
                    INNER JOIN (SELECT studentID, studentNo,
                    id, courseID, schoolYearID, yearLevel, semesterID FROM college_students  INNER JOIN (SELECT max(id) as maxid 
                    FROM college_students 
                    GROUP BY studentID) cs ON cs.maxid = college_students.id) cs2 ON cs2.studentID = students.id 
                    INNER JOIN (SELECT c.id AS courseID, dep.departmentID, c.courseName FROM courses c INNER JOIN (SELECT
                    id, courseID FROM course_department_hists cd INNER JOIN (SELECT max(id) as maxid FROM 
                    course_department_hists GROUP BY courseID) cd2 ON cd2.maxid = cd.id) cdh ON cdh.courseID = c.id
                    INNER JOIN (SELECT departmentID, id FROM course_department_hists INNER JOIN (SELECT
                    max(id) AS maxid FROM course_department_hists GROUP BY courseID) cdh1 ON cdh1.maxid = 
                    course_department_hists.id) dep ON dep.id =cdh.id) c2 ON c2.courseID = cs2.courseID 
                    INNER JOIN (SELECT departmentID, id, collegeID FROM department_college_hists
                    INNER JOIN (SELECT max(id) as maxid FROM department_college_hists GROUP BY departmentID) 
                    depc ON depc.maxid = department_college_hists.id) depc2 ON depc2.departmentID=c2.departmentID 
                    INNER JOIN (SELECT collegeID, userID, id FROM assignment_colleges INNER JOIN (SELECT max(id) 
                    as maxid FROM assignment_colleges GROUP BY userID) assc ON assc.maxid = assignment_colleges.id) 
                    assc2 ON assc2.collegeID = depc2.collegeID 
                    INNER JOIN counselings ON counselings.studentID = students.id
                    INNER JOIN school_years ON school_years.id = counselings.schoolYearID
                    WHERE assc2.userID ='".Auth::guard('myuser')->id()."'
                    AND DATE(students.created_at) <= '".$systemdate[0]->systemDate."'
                    AND (students.firstName LIKE '%$search%' OR students.lastName LIKE '%$search%') ";
                    if($schoolyear != "all"){
                        $sql .= " AND counselings.schoolYearID = '$schoolyear'";
                    }
                    if($semester != "all"){
                        $sql .= " AND counselings.semesterID = '$semester'";
                    }
                    if($coursegrade != "all"){
                        $sql .= " AND cs2.courseID = '$coursegrade'";
                    }
                    
                break;
                case 'seniorhigh':
                    $sql .= " AND  shs.studentType = 'seniorhigh' 
                              AND  shs.gradeLevel = '".$designation[1]."'";
                     if($schoolyear != "all"){
                        $sql .= " AND counselings.schoolYearID = '$schoolyear'";
                    }
                    if($semester != "all"){
                        $sql .= " AND counselings.semesterID = '$semester'";
                    }
                break;
                case 'juniorhigh':
                    $sql .= " AND  jhs.studentType = 'juniorhigh' AND jhs.gradeLevel = '".$designation[1]."'";
                     if($schoolyear != "all"){
                        $sql .= " AND counselings.schoolYearID = '$schoolyear'";
                    }
                    if($semester != "all"){
                        $sql .= " AND counselings.semesterID = '$semester'";
                    }
                break;
                case 'elementary':
                    $sql .= " AND elem.studentType = 'elementary' AND  elem.gradeLevel = '".$designation[1]."'";
                    if($schoolyear != "all"){
                        $sql .= " AND counselings.schoolYearID = '$schoolyear'";
                    }
                    if($semester != "all"){
                        $sql .= " AND counselings.semesterID = '$semester'";
                    }
                break;
            }
        }
        
        if($totalRow=="true"){
            $counseling = DB::select(DB::raw($sql));
            return count($counseling);
        }else{
            $sql .=" LIMIT $skip, 10";
            $counseling = DB::select(DB::raw($sql));
            if(count($counseling)==0){
                return "<h2>There are no counselings found.</h2>";
            }else{
                $html = '';
                $this->rowCounter = 0;
                foreach ($counseling as $counselings) {
                    $this->rowCounter++;
                    $html .= $this->getCounselingRowFaster($counselings, $viewby);
                }
                return $html;
            }
        }
    }
    public function getCounselingRowFaster($counseling, $viewby){
        $studenttype = '';
        if($viewby == 'all'){
            $studenttype = $counseling->studentTypes;
        }else{
            $studenttype =$viewby;
        }
        $gradelevel = '';
        if($counseling->yearLevel ==''){
            $gradelevel = $counseling->courseGrade;
        }else{
            $gradelevel = $counseling->courseName;
        }
        $status;
        
        switch ($counseling->counselingStatus) {
            case '0':
                $status = 'Pending';
            break;
            case '1':
                $status = 'Followed up';
            break;
            case '2':
                $status = 'Evaluated';
            break;
            default:
                # code...
                break;
        }
        // $schoolyearstart = explode('-', $counseling->schoolYearStart)[0];
        // $schoolyearend = explode('-', $counseling->schoolYearEnd)[0];
        $middlename = '';
        if($counseling->middleName != ''){
            $middlename = ucfirst(substr($counseling->middleName, 0, 1)).".";
        }
        return "<tr id ='counselingRow-$counseling->id'>
                    <td>$this->rowCounter</td>
                    <td>$counseling->lastName, $counseling->firstName  $middlename</td>
                    <td>".$this->getBeautifiedStudentType($studenttype)."</td>
                    <td>$gradelevel</td>
                   
                    <td>".ucfirst($counseling->counselingType)."</td>
                    <td>$counseling->dateRecorded</td>
                    <td>$counseling->dateScheduled</td>
                    <td>$status</td>
                    
                    <td>
                        <button class='btn btn-dark viewCounseling' id ='viewCounseling-$counseling->id' data-value ='$counseling->studentID'>
                            <i class='fa fa-eye'></i>
                        </button>
                    </td>
                </tr>";
    }
    public function getCounselingRow($counseling){ 
        $student = new studentController;
        $schoolyearstart = explode('-', $counseling->schoolYearStart)[0];
        $schoolyearend = explode('-', $counseling->schoolYearEnd)[0];
        $studenttype = $student->getStudentType($counseling->studentID);
        $gradelevel;
        
        switch ($studenttype) {
            case 'college':
                $temp = collegeStudent::join('courses', 'courses.id', '=', 'college_students.courseID')
                            ->select('college_students.*', 'courses.courseName')
                            ->where('studentID', '=', $counseling->studentID)->get();
                if(count($temp)>0){
                    $gradelevel = $temp[0]->courseName;
                }else{
                    $gradelevel = 0;
                }
            break;
            case 'seniorhigh':
                $temp = SHSStudent::where('studentID', '=', $counseling->studentID)->get();
                if(count($temp)>0){
                    $gradelevel = $temp[0]->gradeLevel;
                }else{
                    $gradelevel = 0;
                }
            break;
            case 'juniorhigh':
                $temp = JHStudent::where('studentID', '=', $counseling->studentID)->get();
                if(count($temp)>0){
                    $gradelevel = $temp[0]->gradeLevel;
                }else{
                    $gradelevel = 0;
                }
            break;
            case 'elementary':
                $temp = elemStudent::where('studentID', '=', $counseling->studentID)->get();
                if(count($temp)>0){
                    $gradelevel = $temp[0]->gradeLevel;
                }else{
                    $gradelevel = 0;
                }
            break;
            
            default:
                
                break;
        }
        $status;
        
        switch ($counseling->counselingStatus) {
            case '0':
                $status = 'Pending';
            break;
            case '1':
                $status = 'Followed up';
            break;
            case '2':
                $status = 'Evaluated';
            break;
            default:
                # code...
                break;
        }
        $middlename = '';
        if($counseling->middleName != ''){
            $middlename = ucfirst(substr($counseling->middleName, 0, 1)).".";
        }
        return "<tr id ='counselingRow-$counseling->id'>
                    <td>$this->rowCounter</td>
                    <td>$counseling->lastName, $counseling->firstName $middlename</td>
                    <td>".$this->getBeautifiedStudentType($studenttype)."</td>
                    <td>$gradelevel</td>
                    <td>".ucfirst($counseling->counselingType)."</td>
                    <td>$counseling->dateRecorded</td>
                    <td>$counseling->dateScheduled</td>
                    <td>$status</td>
                    
                    <td>
                        <button class='btn btn-dark viewCounseling' id ='viewCounseling-$counseling->id' data-value ='$counseling->studentID'>
                            <i class='fa fa-eye'></i>
                        </button>
                    </td>
                </tr>";
    }
    public function getStudentData($studentid){
        $student = student::find($studentid);
        $gradelevel;
        $studenttype  = (new studentController)->getStudentType($studentid); 
        $student->studentType = $studenttype;
        switch ($studenttype) {
            case 'college':
                $temp = collegeStudent::where('studentID', '=', $studentid)->get();
                if(count($temp)>0){
                    $gradelevel = $temp[0]->yearLevel;
                    $student->course =course::find($temp[0]->courseID)->courseName;
                }else{
                    $gradelevel = 0;
                }
                $student->gradeLevel =  $gradelevel;
                
            break;
            case 'seniorhigh':
                $temp = SHSStudent::where('studentID', '=', $studentid)->get();
                if(count($temp)>0){
                    $gradelevel = $temp[0]->gradeLevel;
                }else{
                    $gradelevel = 0;
                }
                $student->gradeLevel =  $gradelevel;
            break;
            case 'juniorhigh':
                $temp = JHStudent::where('studentID', '=', $studentid)->get();
                if(count($temp)>0){
                    $gradelevel = $temp[0]->gradeLevel;
                }else{
                    $gradelevel = 0;
                }
                $student->gradeLevel =  $gradelevel;
            break;
            case 'elementary':
                $temp = elemStudent::where('studentID', '=', $studentid)->get();
                if(count($temp)>0){
                    $gradelevel = $temp[0]->gradeLevel;
                }else{
                    $gradelevel = 0;
                }
                $student->gradeLevel =  $gradelevel;
            break;
            
            default:
                
                break;
        }
        return $student;
    }
    public function counselingInsert(Request $request){
        //error_log($request->counselingDate);
        $this->validate($request, array(
            'counselingDate'=>'required',
            'dateScheduledText'=> ['required', new systemTimeRule],
            'statementOfTheProblemTextArea'=>'required|max:500',
            'assistedByText'=>'required|max:100',
            'counseledBy' => 'required'
        ));
      
        $counseling = new counseling; 
        $counseling->studentID = $request->studentID;
        $counseling->counselingType ='individual';
        $counseling->semesterID =  (new studentController)->getCurrentSem();
        $counseling->schoolYearID = (new studentController)->getCurrentSchoolYear();
        $counseling->assistedBy = $request->assistedByText;
        $counseling->dateRecorded = $request->dateScheduledText;
        $counseling->dateScheduled = $request->counselingDate;
        $counseling->counselingStatus = '0';
        $counseling->counseledBy = $request->nameOfCounselor;
        $counseling->counselorID = $request->counseledBy;
        if(isset($request->walkin)){
            
            $counseling->walkin = 1;
        }else{
            $counseling->walkin = 0;
        }
        
        $counseling->statementOfTheProblem = $request->statementOfTheProblemTextArea;
        $counseling->save();
        (new notificationController)->insertCounselingNotif($counseling->id, $counseling->dateScheduled);
        $counseling->schoolYearStart = schoolYear::find($counseling->schoolYearID)->schoolYearStart;
        $counseling->schoolYearEnd = schoolYear::find($counseling->schoolYearID)->schoolYearEnd;
        $counseling->firstName = student::find($counseling->studentID)->firstName;
        $counseling->lastName  = student::find($counseling->studentID)->lastName;
        $counseling->middleName  = student::find($counseling->studentID)->middleName;
        activityController::insertActivity('Inserted a counseling of student with id :'.$counseling->studentID);
        $this->rowCounter = $request->rowCounter;
        return $this->getCounselingRow($counseling);
    } 
    public function counselingGroupInsert(Request $request){
        $this->validate($request, array(
            'counselingDateGroupCounseling'=>'required',
            'dateScheduledGroupCounseling'=>'required',
            'statementOfTheProblemGroupCounselingTextArea'=>'required|max:500',
            'assistedByGroupCounselingText'=>'required|max:100',
            'counseledBy' => 'required'
            
        ));
        $this->request = $request;
        /*error_log("Hello");
        error_log($this->request->students[1]);*/
        DB::transaction(function() {
            $counseling = new counseling;
            $students = explode(',', $this->request->students);
            $counseling->studentID =$students[0];
            //$counseling->counseledBy = $this->request->counseledBy;
            $counseling->counseledBy =  $this->request->nameOfCounselor;
            $counseling->counselorID =  $this->request->counseledBy;
            $counseling->counselingType ='group';
            $counseling->semesterID =  (new studentController)->getCurrentSem();
            $counseling->schoolYearID = (new studentController)->getCurrentSchoolYear();
            $counseling->assistedBy = $this->request->assistedByGroupCounselingText;
            $counseling->dateRecorded = $this->request->counselingDateGroupCounseling;
            $counseling->dateScheduled = $this->request->dateScheduledGroupCounseling;
            $counseling->counselingStatus = '0';
            if(isset($this->request->walkinGroupCounceling)){
                $counseling->walkin = 1;
            }else{
                $counseling->walkin = 0;
            }
            $counseling->statementOfTheProblem = $this->request->statementOfTheProblemGroupCounselingTextArea;
            $counseling->save();
            (new notificationController)->insertCounselingNotif($counseling->id, $counseling->dateScheduled);
            $myitems;
            //$i =0; 
            for ($i=0; $i < count($students); $i++) { 
                if($i==0){
                    continue;
                }
                $myitems[$i]['studentID']= $students[$i];
                $myitems[$i]['counselingID']= $counseling->id;
            }
            /*foreach ($this->request->students as $student) {
                if(i==0){
                    continue;
                }
                $myitems[$i]['studentID']= $student;
                $i++;
            }*/
            DB::table("group_counselings")->insert($myitems);
            $counseling->schoolYearStart = schoolYear::find($counseling->schoolYearID)->schoolYearStart;
            $counseling->schoolYearEnd = schoolYear::find($counseling->schoolYearID)->schoolYearEnd;
            $counseling->firstName = student::find($counseling->studentID)->firstName;
            $counseling->lastName  = student::find($counseling->studentID)->lastName;
            $counseling->middleName  = student::find($counseling->studentID)->middleName;
            $this->rowCounter = $this->request->rowCounter;
            $this->html = $this->getCounselingRow($counseling);
        });
        activityController::insertActivity('Inserted a group Counseling.');
        return $this->html;
    }
    public function sendMessageCounseling(Request $request){
        if(isset($request->resched)){ 
            $student = student::find($request->studentID);
            $ch = curl_init();
            $itexmo = array('1' => $student->contactNo, '2' => 'Your counseling is rescheduled to '.$request->counselingDate, '3' => 'TR-EPHRA940314_T1W6X');
            curl_setopt($ch, CURLOPT_URL,"https://www.itexmo.com/php_api/api.php");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, 
                    http_build_query($itexmo));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec ($ch);
            error_log($response);
            curl_close ($ch);
            return "sent";
        }else{
            $student = student::find($request->studentID);
            $ch = curl_init();
            $itexmo = array('1' => $student->contactNo, '2' => 'You have a counseling in guidance on '.$request->counselingDate, '3' => 'TR-EPHRA940314_T1W6X');
            curl_setopt($ch, CURLOPT_URL,"https://www.itexmo.com/php_api/api.php");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, 
                    http_build_query($itexmo));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec ($ch);
            error_log($response);
            curl_close ($ch);
            return "sent";
        }
    }
    
    public function getBeautifiedStudentType($studenttype){
        switch ($studenttype) {
            case 'college':
                return "College";
            break;
            case 'seniorhigh':
                return "Senior High";
            break;
            case 'juniorhigh':
                return "Junior High";
            break;
            case 'elementary':
                return "Elementary";
            break;
            default:
                # code...
                break;
        }
    }
    public function getCounselingData($counselingid){
        return counseling::find($counselingid);
    }
    public function counselingIndividualUpdate(Request $request){
        $this->validate($request, array(
            'counselingDate'=>'required',
            'dateScheduledText'=>'required',
            'statementOfTheProblemTextArea'=>'required|max:500',
            'assistedByText'=>'required|max:100',
            'counseledBy' => 'required'
        ));
        $counseling =  counseling::find($request->id);
        $counseling->studentID = $request->studentID;
       
        
        $counseling->assistedBy = $request->assistedByText;
        $counseling->dateRecorded = $request->dateScheduledText;
        $counseling->dateScheduled = $request->counselingDate;
        $counseling->counseledBy = $request->nameOfCounselor;
        $counseling->counselorID = $request->counseledBy;
        //$counseling->counselingStatus = '0';
       
        if(isset($request->walkin)){
            
            $counseling->walkin = 1;
        }else{
            $counseling->walkin = 0;
        }
        $counseling->statementOfTheProblem = $request->statementOfTheProblemTextArea;
        $counseling->save();
        $counseling->schoolYearStart = schoolYear::find($counseling->schoolYearID)->schoolYearStart;
        $counseling->schoolYearEnd = schoolYear::find($counseling->schoolYearID)->schoolYearEnd;
        $counseling->firstName = student::find($counseling->studentID)->firstName;
        $counseling->lastName  = student::find($counseling->studentID)->lastName;
        activityController::insertActivity('Updated a counseling of student with id :'.$counseling->studentID);
        $this->rowCounter = $request->rowCounter;
        return $this->getCounselingRow($counseling);
    } 
    public function checkIfFollowedUpOrEvaluated($counselingid){
        //error_log($counselingid);
        if(count(evaluation::where('counselingID', '=', $counselingid)->get())>0){
            return "evaluated";
        }else{
            if(count(followUp::where('counselingID', '=', $counselingid)->get())>0){
                return "followedup";
            }else{
                return "false";
            }
        }
        
    }
    public function getFollowUpData($counselingid){
        $followup = followUp::where('counselingID', '=', $counselingid)->get();
        if(count($followup)==0){
            return "<h2>This counseling doesnt have any followup yet</h2>";
        }else{
            $html = '';
            foreach ($followup as $followups) {
                $html .= $this->getFollowUpRow($followups);
            }
            return $html;
        }
    }
    public function getFollowUpRow($followup){
        $details='';
        if(strlen($followup->details)>20){
            $details = substr($followup->details, 0,19);
            $details .='...';
        }else{
            $details = $followup->details;
        }
        return "
            <tr id ='followUpIndividualRow-$followup->id'>
                <td>$details</td>
                <td>$followup->assistedBy</td>
                <td>".explode(" ",$followup->created_at)[0]."</td>
                <td>
                    <button class='btn btn-dark editFollowUpIndividual' id='editFollowUpIndividual-$followup->id' >
                        <i class='fa fa-edit'></i>
                    </button>
                </td>

            </tr>
        ";
    }
    public function followupInsert(Request $request){
        $this->validate($request, array(
            'assistedBy'=>'required|max:100',
            'followUpTextArea'=>'required|max:1000'
        ));
        $followup = new followUp;
        $followup->counselingID = $request->counselingID;
        $followup->assistedBy = $request->assistedBy;
        $followup->details = $request->followUpTextArea;
        $followup->save();
        $counseling = counseling::find($request->counselingID);
        $counseling->counselingStatus = '1';
        $counseling->save();
        activityController::insertActivity('Inserted a followup for counseling.');
        return $this->getFollowUpRow($followup);
    }
    public function followupUpdate(Request $request){
        $this->validate($request, array(
            'assistedBy'=>'required|max:100',
            'details'=>'required|max:1000'
        ));
        $followup =  followUp::find($request->id);
    
        $followup->assistedBy = $request->assistedBy;
        $followup->details = $request->details;
        $followup->save();
        activityController::insertActivity('Editted a followup for counseling.');
        return $this->getFollowUpRow($followup);
    }
    public function getFollowUpDetails($followupid){
        return followUp::find($followupid)->details;
    }
    public function evaluationForm(Request $request){
        $this->validate($request, array(
            'remarks'=>'required|max:500',
            'evaluatedBy'=>'required|max:1000'
        ));
        $check1 = followUp::where('counselingID', '=', $request->counselingID)->count();
        if($check1==0){
            return "This counseling must be followed up first!";
        }
        $check = evaluation::where('counselingID', '=', $request->counselingID)->count();
        if($check > 0){
            $curreval = evaluation::where('counselingID', '=', $request->counselingID)->get();
            $evaluation = evaluation::find($curreval[0]->id);
            $evaluation->remarks = $request->remarks;
            $evaluation->evaluatedBy = $request->evaluatedBy;
            $evaluation->save();
           
            activityController::insertActivity('Updated an evaluation of counseling with counseling id:'.$request->counselingID);
            return "saved!";
        }else{
            $systemdate = systemDate::all(); 
            $evaluation = new evaluation;
            $evaluation->evaluationDate = $systemdate[0]->systemDate;
            $evaluation->remarks = $request->remarks;
            $evaluation->evaluatedBy = $request->evaluatedBy;
            $evaluation->counselingID = $request->counselingID;
            $evaluation->save();
            $counseling = counseling::find($request->counselingID);
            $counseling->counselingStatus = '2';
            $counseling->save();
            activityController::insertActivity('Evaluated a counseling with counseling id:'.$request->counselingID);
            return "saved!";
        }
    }
    public function getEvaluationData($counselingid){
        $counseling = evaluation::where('counselingID', '=', $counselingid)->get();
        if(count($counseling)>0){
            return $counseling[0];
        }else{
            return "0";
        }
        
    }
    public function getCounselingGroupData($counselingid){
        $groupdata = groupCounseling::join('students', 'students.id', '=', 'group_counselings.studentID')
                                ->select('group_counselings.studentID')
                                ->where('group_counselings.counselingID', '=', $counselingid)->get();
        $seconddata = counseling::select('studentID')
                                  ->where('id', '=', $counselingid)
                                  ->get();
        $groupdata->push($seconddata[0]);
       
        $studentstable = student::all();
        $studentobj = new studentController;
        foreach ($studentstable as $key => $students) {
            $studentstable[$key]->studentType = $studentobj->getStudentType($students->id); 
        }
        foreach ($studentstable as $key => $students) {
            if($groupdata->contains('studentID', $students->id)){
                $studentstable[$key]->checked = true;
            }else{
                $studentstable[$key]->checked = false;
            }
        }
        $counseling = counseling::find($counselingid);
        $html = '';
        $studenttype = "";
        
        foreach ($studentstable as $students) {
            switch ($students->studentType) {
                case 'college':
                    $studenttype = "College";
                break;
                case 'seniorhigh':
                    $studenttype = "Senior High";
                break;
                case 'juniorhigh':
                    $studenttype = "Junior High";
                break;
                case 'elementary':
                    $studenttype = "Elementary";
                break;
                default:
                    # code...
                    break;
            }
            $html .="<tr>";
                if($students->checked){
                    $html .="<td><input type ='checkbox' class='checkboxGroupEdit' value ='$students->id' checked></td>";
                }else{
                    $html .="<td><input type ='checkbox' class='checkboxGroupEdit' value ='$students->id'></td>";
                }
                $html .="<td>$students->firstName $students->lastName</td>
                        <td>$students->contactNo</td>
                        <td>$students->cityAddress</td>
                        <td>".$studentobj->getStudentNo2($students->id, $students->studentType)."</td>
                        <td>$studenttype</td>";
            $html .= "</tr>";
        }
        $counseling->html =$html;
        return $counseling;
    }
    public function groupCounselingUpdate(Request $request){
        $this->validate($request, array(
            'counselingDate'=>'required',
            'dateScheduledText'=>'required',
            'statementOfTheProblemTextArea'=>'required|max:500',
            'assistedByText'=>'required|max:100',
            'counseledBy' => 'required'
        ));
        $this->request= $request;
        DB::transaction(function() {
            $students = explode(',', $this->request->student);
            $counseling =  counseling::find($this->request->id);
            $counseling->studentID = $students[0];
            $counseling->assistedBy = $this->request->assistedByText;
            $counseling->dateRecorded =$this->request->dateScheduledText;
            $counseling->dateScheduled = $this->request->counselingDate;
            //$counseling->counseledBy = $this->request->counseledBy;
            $counseling->counseledBy = $this->request->nameOfCounselor;
            $counseling->counselorID = $this->request->counseledBy;
            //$counseling->counselingStatus = '0';
        
            if(isset($this->request->walkin)){
                
                $counseling->walkin = 1;
            }else{
                $counseling->walkin = 0;
            }
            $counseling->statementOfTheProblem = $this->request->statementOfTheProblemTextArea;
            $counseling->save();
            $myitems;
            for($i=1; $i<count($students); $i++){
                $myitems[$i]['studentID']= $students[$i];
                $myitems[$i]['counselingID']= $this->request->id;
            }
            $group = groupCounseling::where('counselingID', '=' ,$this->request->id)->get();
            foreach ($group as $groups) {
                $groups->forceDelete();
            }
            DB::table("group_counselings")->insert($myitems);
            
            $counseling->schoolYearStart = schoolYear::find($counseling->schoolYearID)->schoolYearStart;
            $counseling->schoolYearEnd = schoolYear::find($counseling->schoolYearID)->schoolYearEnd;
            $counseling->firstName = student::find($counseling->studentID)->firstName;
            $counseling->lastName  = student::find($counseling->studentID)->lastName;
            $this->counseling = $counseling;
        });
        activityController::insertActivity('Updated a group counseling');
        $this->rowCounter = $request->rowCounter;
        return $this->getCounselingRow($this->counseling);
    }
    public function getStudentsForGroupCounseling($viewby, $schoolyear, $semester, $coursegrade, $search=''){
        $user = new userController();
        $systemdate = systemDate::all();
        $sql='';
        if(session()->get('user')['userType']=='admin'){
            $sql = "SELECT COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) 
                    semesterID, COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) 
                    schoolYearID, COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) 					studentType, 
                    COALESCE(col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) courseGrade, 
                    COALESCE(col.studentNo, shs.studNo, jhs.studNo, elem.studNo) studentNo, students.*,
                    col.courseName, col.yearLevel
                    FROM students LEFT JOIN (SELECT college_students.studentID, college_students.yearLevel, college_students.semesterID, college_students.schoolYearID, college_students.id, 'college' AS studentType, 
                    college_students.courseID, courses.courseName, college_students.studentNo FROM college_students INNER JOIN (SELECT max(id) as maxid FROM college_students GROUP 
                    BY studentID) cs ON cs.maxid = college_students.id
                    INNER JOIN courses ON courses.id = college_students.courseID) col ON col.studentID = students.id LEFT 
                    JOIN (SELECT studentID, semesterID, schoolYearID, id, 'seniorhigh' AS studentType, gradeLevel, studNo 
                    FROM s_h_s_students INNER JOIN (SELECT max(id) as maxid FROM s_h_s_students GROUP BY studentID) 
                    shs ON shs.maxid = s_h_s_students.id) shs ON shs.studentID = students.id
                    LEFT JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'juniorhigh' AS 
                    studentType, gradeLevel, studNo  FROM j_h_students INNER JOIN (SELECT max(id) as maxid FROM j_h_students 
                    GROUP BY studentID) jhs ON jhs.maxid = j_h_students.id) jhs ON jhs.studentID = students.id LEFT 
                    JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'elementary' AS studentType, 
                    gradeLevel, studNo FROM elem_students INNER JOIN (SELECT max(id) as maxid FROM elem_students GROUP 
                    BY studentID) elem ON elem.maxid = elem_students.id) elem ON elem.studentID = students.id
            WHERE (students.firstName LIKE '%$search%' OR students.lastName LIKE '%$search%') 
            AND DATE(students.created_at) <= '".$systemdate[0]->systemDate."'";
            switch ($viewby) {
                case 'college':
                    $sql .= " AND COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) = 'college'";
                break;
                case 'seniorhigh':
                    $sql .= " AND COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) = 'seniorhigh'";
                break;
                case 'juniorhigh':
                    $sql .= " AND COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) = 'juniorhigh'";
                break;
                case 'elementary':
                    $sql .= " AND COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) = 'elementary'";
                break;
                default:
                   
                break;
            }
            if($schoolyear!="all"){
                $sql .= " AND COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) = '$schoolyear'";
            }
            if($semester!="all"){
                $sql .= " AND COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) = '$semester'";
            }
            if($coursegrade!="all"){
                $sql .= " AND COALESCE(col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) = '$coursegrade'";
            }
        }else{
            $designation = $user->getUserDesignation(Auth::guard('myuser')->id());
            $sql = "SELECT COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) 
                    semesterID, COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) 
                    schoolYearID, COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) 					studentType, 
                    COALESCE(col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) courseGrade, 
                    COALESCE(col.studentNo, shs.studNo, jhs.studNo, elem.studNo) studentNo, students.*,
                    col.courseName, col.yearLevel
                    FROM students LEFT JOIN (SELECT college_students.studentID, college_students.yearLevel, college_students.semesterID, college_students.schoolYearID, college_students.id, 'college' AS studentType, 
                    college_students.courseID, courses.courseName, college_students.studentNo FROM college_students INNER JOIN (SELECT max(id) as maxid FROM college_students GROUP 
                    BY studentID) cs ON cs.maxid = college_students.id
                    INNER JOIN courses ON courses.id = college_students.courseID) col ON col.studentID = students.id LEFT 
                    JOIN (SELECT studentID, semesterID, schoolYearID, id, 'seniorhigh' AS studentType, gradeLevel, studNo 
                    FROM s_h_s_students INNER JOIN (SELECT max(id) as maxid FROM s_h_s_students GROUP BY studentID) 
                    shs ON shs.maxid = s_h_s_students.id) shs ON shs.studentID = students.id
                    LEFT JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'juniorhigh' AS 
                    studentType, gradeLevel, studNo  FROM j_h_students INNER JOIN (SELECT max(id) as maxid FROM j_h_students 
                    GROUP BY studentID) jhs ON jhs.maxid = j_h_students.id) jhs ON jhs.studentID = students.id LEFT 
                    JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'elementary' AS studentType, 
                    gradeLevel, studNo FROM elem_students INNER JOIN (SELECT max(id) as maxid FROM elem_students GROUP 
                    BY studentID) elem ON elem.maxid = elem_students.id) elem ON elem.studentID = students.id
                    WHERE (students.firstName LIKE '%$search%' OR students.lastName LIKE '%$search%') 
                    AND DATE(students.created_at) <= '".$systemdate[0]->systemDate."'";
            switch($designation[0]){
                case 'college':
                    $sql= "SELECT cs2.courseID AS courseGrade, c2.courseName, cs2.schoolYearID, cs2.studentNo, 
                    cs2.yearLevel, students.*, 'college' AS studentType, cs2.semesterID FROM students 
                    INNER JOIN (SELECT studentID, studentNo,
                    id, courseID, schoolYearID, yearLevel, semesterID FROM college_students  INNER JOIN (SELECT max(id) as maxid 
                    FROM college_students 
                    GROUP BY studentID) cs ON cs.maxid = college_students.id) cs2 ON cs2.studentID = students.id 
                    INNER JOIN (SELECT c.id AS courseID, dep.departmentID, c.courseName FROM courses c INNER JOIN (SELECT
                    id, courseID FROM course_department_hists cd INNER JOIN (SELECT max(id) as maxid FROM 
                    course_department_hists GROUP BY courseID) cd2 ON cd2.maxid = cd.id) cdh ON cdh.courseID = c.id
                    INNER JOIN (SELECT departmentID, id FROM course_department_hists INNER JOIN (SELECT
                    max(id) AS maxid FROM course_department_hists GROUP BY courseID) cdh1 ON cdh1.maxid = 
                    course_department_hists.id) dep ON dep.id =cdh.id) c2 ON c2.courseID = cs2.courseID 
                    INNER JOIN (SELECT departmentID, id, collegeID FROM department_college_hists
                    INNER JOIN (SELECT max(id) as maxid FROM department_college_hists GROUP BY departmentID) 
                    depc ON depc.maxid = department_college_hists.id) depc2 ON depc2.departmentID=c2.departmentID 
                    INNER JOIN (SELECT collegeID, userID, id FROM assignment_colleges INNER JOIN (SELECT max(id) 
                    as maxid FROM assignment_colleges GROUP BY userID) assc ON assc.maxid = assignment_colleges.id) 
                    assc2 ON assc2.collegeID = depc2.collegeID WHERE assc2.userID ='".Auth::guard('myuser')->id()."'
                    AND DATE(students.created_at) <= '".$systemdate[0]->systemDate."'
                    AND (students.firstName LIKE '%$search%' OR students.lastName LIKE '%$search%') ";
                    if($schoolyear != "all"){
                        $sql .= " AND cs2.schoolYearID = '$schoolyear'";
                    }
                    if($semester != "all"){
                        $sql .= " AND cs2.semesterID = '$semester'";
                    }
                    if($coursegrade != "all"){
                        $sql .= " AND cs2.courseID = '$coursegrade'";
                    }
                    
                break;
                case 'seniorhigh':
                    $sql .= " AND COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) = 'seniorhigh'
                              AND COALESCE(col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) = '".$designation[1]."'";
                     if($schoolyear != "all"){
                        $sql .= " AND COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) = '$schoolyear'";
                    }
                    if($semester != "all"){
                        $sql .= " AND COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) = '$semester'";
                    }
                break;
                case 'juniorhigh':
                    $sql .= " AND COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) = 'juniorhigh'
                                AND COALESCE(col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) = '".$designation[1]."'";
                    if($schoolyear != "all"){
                        $sql .= " AND COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) = '$schoolyear'";
                    }
                    if($semester != "all"){
                        $sql .= " AND COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) = '$semester'";
                    }
                break;
                case 'elementary':
                    $sql .= " AND COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) = 'elementary'
                                AND COALESCE(col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) = '".$designation[1]."'";
                    if($schoolyear != "all"){
                        $sql .= " AND COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) = '$schoolyear'";
                    }
                    if($semester != "all"){
                        $sql .= " AND COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) = '$semester'";
                    }
                break;
            }
        }
        $students = DB::select(DB::raw($sql));
        if(count($students)==0){
            return "<h2>There are no students found.</h2>";
        }else{
            $html = '';
            $this->rowCounter=0;
            foreach ($students as $student) {
                $this->rowCounter++;
                $html .= $this->getStudentRowFaster($student);
            }
            return $html;
        }    
    }
    public function getStudentRowFaster($student){
        return "<tr id  = 'student-$student->id'>
                    <td><input type ='checkbox' class ='checkboxStudent' 
                    id ='studentcounseling-$student->id' value ='$student->id'></td>
                    <td>$student->lastName, $student->firstName</td>
                    <td>$student->studentNo</td>
                    <td>$student->studentType</td>
                </tr>";
    }
    public function getYearOfActiveSchoolYear(){
        $schoolyear = schoolYear::where('status','=', '1')->get();
        if(count($schoolyear)>0){
            return explode('-', $schoolyear[0]->schoolYearStart)[0]." - ".explode('-', $schoolyear[0]->schoolYearEnd)[0];
        }else{
            return "";
        }
    }
    public function printCounselingReport($viewby, $coursegrade, $schoolyear, $semester, $search=''){
        $user = new userController();
        $systemdate = systemDate::all();
        $counseling;
        $thissemester = 'all Semester';
        $thisschoolyear = 'all School year';
        if(session()->get('user')['userType']=='admin'){
            $sql = "SELECT COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) 
                    semesterID, COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) 
                    schoolYearID, COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) studentTypes,
                    col.studentType, shs.studentType, jhs.studentType, elem.studentType,
                    COALESCE(col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) courseGrade, 
                    col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel,
                    COALESCE(col.studentNo, shs.studNo, jhs.studNo, elem.studNo) studentNo,  students.firstName, students.lastName,
                    students.middleName,
                    col.courseName, col.yearLevel,
                    school_years.schoolYearStart,
                    school_years.schoolYearEnd,
                    counselings.*
                    FROM students LEFT JOIN (SELECT college_students.studentID, 
                    college_students.yearLevel, college_students.semesterID, college_students.schoolYearID,
                    college_students.id, 'college' AS studentType, 
                    college_students.courseID, courses.courseName, college_students.studentNo FROM
                    college_students INNER JOIN (SELECT max(id) as maxid FROM college_students GROUP 
                    BY studentID) cs ON cs.maxid = college_students.id
                    INNER JOIN courses ON courses.id = college_students.courseID) col ON 
                    col.studentID = students.id LEFT 
                    JOIN (SELECT studentID, semesterID, schoolYearID, id, 'seniorhigh' AS studentType, 
                    gradeLevel, studNo 
                    FROM s_h_s_students INNER JOIN (SELECT max(id) as maxid FROM s_h_s_students GROUP BY studentID) 
                    shs ON shs.maxid = s_h_s_students.id) shs ON shs.studentID = students.id
                    LEFT JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'juniorhigh' AS 
                    studentType, gradeLevel, studNo  FROM j_h_students INNER JOIN (SELECT max(id) as maxid FROM j_h_students 
                    GROUP BY studentID) jhs ON jhs.maxid = j_h_students.id) jhs ON jhs.studentID = students.id LEFT 
                    JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'elementary' AS studentType, 
                    gradeLevel, studNo FROM elem_students INNER JOIN (SELECT max(id) as maxid FROM elem_students GROUP 
                    BY studentID) elem ON elem.maxid = elem_students.id) elem ON elem.studentID = students.id
                    INNER JOIN counselings ON counselings.studentID =  students.id
                    INNER JOIN school_years ON school_years.id = counselings.schoolYearID
                    LEFT JOIN follow_ups ON follow_ups.counselingID = counselings.id
                    LEFT JOIN evaluations ON evaluations.counselingID = counselings.id
            WHERE (students.firstName LIKE '%$search%' OR students.lastName LIKE '%$search%'
                    OR evaluations.remarks LIKE '%$search%' OR follow_ups.details LIKE '%$search%'
                    OR counselings.statementOfTheProblem LIKE '%$search%') 
            AND DATE(students.created_at) <= '".$systemdate[0]->systemDate."'";
            switch ($viewby) {
                case 'college':
                    $sql .= " AND col.studentType = 'college'";
                break;
                case 'seniorhigh':
                    $sql .= " AND shs.studentType = 'seniorhigh'";
                break;
                case 'juniorhigh':
                    $sql .= " AND jhs.studentType = 'juniorhigh'";
                break;
                case 'elementary':
                    $sql .= " AND elem.studentType = 'elementary'";
                break;
                default:
                   
                break;
            }
            if($schoolyear!="all"){
                
                $thisschoolyear = $this->getYearOfActiveSchoolYear()." School Year";
                $sql .= " AND counselings.schoolYearID = '$schoolyear'";
            }
            if($semester!="all"){
                switch ($semester) {
                    case '1':
                        $thissemester = '1st Semester';
                    break;
                    case '2':
                        $thissemester = '2nd Semester';
                    break;
                    case '3':
                        $thissemester = '3rd Semester';
                    break;
                    default:
                        # code...
                        break;
                }
                $sql .= " AND counselings.semesterID = '$semester'";
            }
            if($coursegrade!="all"){
                switch ($viewby) {
                    case 'college':
                        $sql .= " AND col.courseID = '$coursegrade'";
                    break;
                    case 'seniorhigh':
                        $sql .= "  AND shs.gradeLevel = '$coursegrade'";
                    break;
                    case 'juniorhigh':
                        $sql .= "  AND jhs.gradeLevel = '$coursegrade'";
                    break;
                    case 'elementary':
                        $sql .= "  AND elem.gradeLevel = '$coursegrade'";
                    break;
                    default:
                        $sql .= " AND COALESCE(col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) = '$coursegrade'";
                    break;
                }
            }
        }else{
            $designation = $user->getUserDesignation(Auth::guard('myuser')->id());
            $sql = "SELECT COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) 
                    semesterID, COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) 
                    schoolYearID, COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) studentTypes,
                    col.studentType, shs.studentType, jhs.studentType, elem.studentType, 
                    COALESCE(col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) courseGrade, 
                    col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel,
                    COALESCE(col.studentNo, shs.studNo, jhs.studNo, elem.studNo) studentNo, students.firstName, students.lastName,
                    students.middleName,
                    school_years.schoolYearStart,
                    school_years.schoolYearEnd,
                    counselings.*,
                    col.courseName, col.yearLevel
                    FROM students LEFT JOIN (SELECT college_students.studentID, 
                    college_students.yearLevel, college_students.semesterID, college_students.schoolYearID,
                    college_students.id, 'college' AS studentType, 
                    college_students.courseID, courses.courseName, college_students.studentNo FROM
                    college_students INNER JOIN (SELECT max(id) as maxid FROM college_students GROUP 
                    BY studentID) cs ON cs.maxid = college_students.id
                    INNER JOIN courses ON courses.id = college_students.courseID) col ON 
                    col.studentID = students.id LEFT 
                    JOIN (SELECT studentID, semesterID, schoolYearID, id, 'seniorhigh' AS studentType, 
                    gradeLevel, studNo 
                    FROM s_h_s_students INNER JOIN (SELECT max(id) as maxid FROM s_h_s_students GROUP BY studentID) 
                    shs ON shs.maxid = s_h_s_students.id) shs ON shs.studentID = students.id
                    LEFT JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'juniorhigh' AS 
                    studentType, gradeLevel, studNo  FROM j_h_students INNER JOIN (SELECT max(id) as maxid FROM j_h_students 
                    GROUP BY studentID) jhs ON jhs.maxid = j_h_students.id) jhs ON jhs.studentID = students.id LEFT 
                    JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'elementary' AS studentType, 
                    gradeLevel, studNo FROM elem_students INNER JOIN (SELECT max(id) as maxid FROM elem_students GROUP 
                    BY studentID) elem ON elem.maxid = elem_students.id) elem ON elem.studentID = students.id
                    INNER JOIN counselings ON counselings.studentID =  students.id
                    INNER JOIN school_years ON school_years.id = counselings.schoolYearID
                    WHERE (students.firstName LIKE '%$search%' OR students.lastName LIKE '%$search%') 
                    AND DATE(students.created_at) <= '".$systemdate[0]->systemDate."'";
            switch($designation[0]){
                case 'college':
                    $sql= "SELECT cs2.courseID AS courseGrade, c2.courseName, cs2.schoolYearID, cs2.studentNo, 
                    cs2.yearLevel, students.firstName, students.lastName, 'college' AS studentTypes, cs2.semesterID,
                    counselings.*,
                    students.middleName,
                    school_years.schoolYearStart,
                    school_years.schoolYearEnd  FROM students 
                    INNER JOIN (SELECT studentID, studentNo,
                    id, courseID, schoolYearID, yearLevel, semesterID FROM college_students  INNER JOIN (SELECT max(id) as maxid 
                    FROM college_students 
                    GROUP BY studentID) cs ON cs.maxid = college_students.id) cs2 ON cs2.studentID = students.id 
                    INNER JOIN (SELECT c.id AS courseID, dep.departmentID, c.courseName FROM courses c INNER JOIN (SELECT
                    id, courseID FROM course_department_hists cd INNER JOIN (SELECT max(id) as maxid FROM 
                    course_department_hists GROUP BY courseID) cd2 ON cd2.maxid = cd.id) cdh ON cdh.courseID = c.id
                    INNER JOIN (SELECT departmentID, id FROM course_department_hists INNER JOIN (SELECT
                    max(id) AS maxid FROM course_department_hists GROUP BY courseID) cdh1 ON cdh1.maxid = 
                    course_department_hists.id) dep ON dep.id =cdh.id) c2 ON c2.courseID = cs2.courseID 
                    INNER JOIN (SELECT departmentID, id, collegeID FROM department_college_hists
                    INNER JOIN (SELECT max(id) as maxid FROM department_college_hists GROUP BY departmentID) 
                    depc ON depc.maxid = department_college_hists.id) depc2 ON depc2.departmentID=c2.departmentID 
                    INNER JOIN (SELECT collegeID, userID, id FROM assignment_colleges INNER JOIN (SELECT max(id) 
                    as maxid FROM assignment_colleges GROUP BY userID) assc ON assc.maxid = assignment_colleges.id) 
                    assc2 ON assc2.collegeID = depc2.collegeID 
                    INNER JOIN counselings ON counselings.studentID = students.id
                    INNER JOIN school_years ON school_years.id = counselings.schoolYearID
                    WHERE assc2.userID ='".Auth::guard('myuser')->id()."'
                    AND DATE(students.created_at) <= '".$systemdate[0]->systemDate."'
                    AND (students.firstName LIKE '%$search%' OR students.lastName LIKE '%$search%') ";
                    if($schoolyear != "all"){
                        
                        $thisschoolyear = $this->getYearOfActiveSchoolYear()." School Year";
                        $sql .= " AND counselings.schoolYearID = '$schoolyear'";
                    }
                    if($semester != "all"){
                        switch ($semester) {
                            case '1':
                                $thissemester = '1st Semester';
                            break;
                            case '2':
                                $thissemester = '2nd Semester';
                            break;
                            case '3':
                                $thissemester = '3rd Semester';
                            break;
                            default:
                                # code...
                                break;
                        }
                        $sql .= " AND counselings.semesterID = '$semester'";
                    }
                    if($coursegrade != "all"){
                        $sql .= " AND cs2.courseID = '$coursegrade'";
                    }
                    
                break;
                case 'seniorhigh':
                    $sql .= " AND  shs.studentType = 'seniorhigh' 
                              AND  shs.gradeLevel = '".$designation[1]."'";
                     if($schoolyear != "all"){
                        $thisschoolyear = $this->getYearOfActiveSchoolYear()." School Year";
                        $sql .= " AND counselings.schoolYearID = '$schoolyear'";
                    }
                    if($semester != "all"){
                        switch ($semester) {
                            case '1':
                                $thissemester = '1st Semester';
                            break;
                            case '2':
                                $thissemester = '2nd Semester';
                            break;
                            case '3':
                                $thissemester = '3rd Semester';
                            break;
                            default:
                                # code...
                                break;
                        }
                        $sql .= " AND counselings.semesterID = '$semester'";
                    }
                break;
                case 'juniorhigh':
                    $sql .= " AND  jhs.studentType = 'juniorhigh' AND jhs.gradeLevel = '".$designation[1]."'";
                     if($schoolyear != "all"){
                        $thisschoolyear = $this->getYearOfActiveSchoolYear()." School Year";
                        $sql .= " AND counselings.schoolYearID = '$schoolyear'";
                    }
                    if($semester != "all"){
                        switch ($semester) {
                            case '1':
                                $thissemester = '1st Semester';
                            break;
                            case '2':
                                $thissemester = '2nd Semester';
                            break;
                            case '3':
                                $thissemester = '3rd Semester';
                            break;
                            default:
                                # code...
                                break;
                        }
                        $sql .= " AND counselings.semesterID = '$semester'";
                    }
                break;
                case 'elementary':
                    $sql .= " AND elem.studentType = 'elementary' AND  elem.gradeLevel = '".$designation[1]."'";
                    if($schoolyear != "all"){
                        $thisschoolyear = $this->getYearOfActiveSchoolYear()." School Year";
                        $sql .= " AND counselings.schoolYearID = '$schoolyear'";
                    }
                    if($semester != "all"){
                        switch ($semester) {
                            case '1':
                                $thissemester = '1st Semester';
                            break;
                            case '2':
                                $thissemester = '2nd Semester';
                            break;
                            case '3':
                                $thissemester = '3rd Semester';
                            break;
                            default:
                                # code...
                                break;
                        }
                        $sql .= " AND counselings.semesterID = '$semester'";
                    }
                break;
            }
        }
        
        $counseling = DB::select(DB::raw($sql));
        $title = "List of Students with Counseling for ".$thissemester." and ".$thisschoolyear."";
        $html ='';
        if(count($counseling)==0){
            $html = "<tr><td colspan='7'><b>There are no counselings found</b></td></tr>";
        }else{
            $counter = 0;
            foreach ($counseling as $key => $value) {
                $status = '';
               
                switch ($counseling[$key]->counselingStatus) {
                    case '0':
                        $status = 'pending';
                    break;
                    case '1':
                        $status = 'followed up';
                    break;
                    case '2':
                        $status = 'evaluated';
                    break;
                    default:
                        # code...
                        break;
                }
                if($counseling[$key]->counselingType == 'group'){
                    $groupcounseling = groupCounseling::join('students', 'students.id', '=', 'group_counselings.studentID')
                                                        ->select('students.firstName', 'students.lastName', 'group_counselings.studentID')
                                                        ->where('counselingID', '=', $counseling[$key]->id)->get();
                    $html .="<tr>
                                <td>".(++$counter)."</td>
                                <td>".$counseling[$key]->firstName." ".$counseling[$key]->lastName."</td>
                                
                                <td>".date("F j, Y", strtotime($counseling[$key]->dateScheduled))."</td>
                                <td>".date("F j, Y", strtotime($counseling[$key]->dateRecorded))."</td>
                                <td>".ucfirst($counseling[$key]->counselingType)."</td>
                                <td>".ucfirst($status)."</td>
                                <td>".$counseling[$key]->assistedBy."</td>
                            </tr>";
                    
                    foreach ($groupcounseling as $skey => $value) {
                        
                        $html .="<tr>
                                    <td>".(++$counter)."</td>
                                    <td>".$groupcounseling[$skey]->firstName." ".$groupcounseling[$skey]->lastName."</td>
                                    
                                    <td>".date("F j, Y", strtotime($counseling[$key]->dateScheduled))."</td>
                                    <td>".date("F j, Y", strtotime($counseling[$key]->dateRecorded))."</td>
                                    <td>".ucfirst($counseling[$key]->counselingType)."</td>
                                    <td>".ucfirst($status)."</td>
                                    <td>".$counseling[$key]->assistedBy."</td>
                            </tr>";
                    }
                }else{
                    $html .="<tr>
                                <td>".(++$counter)."</td>
                                <td>".$counseling[$key]->firstName." ".$counseling[$key]->lastName."</td>
                                
                                <td>".date("F j, Y", strtotime($counseling[$key]->dateScheduled))."</td>
                                <td>".date("F j, Y", strtotime($counseling[$key]->dateRecorded))."</td>
                                <td>".ucfirst($counseling[$key]->counselingType)."</td>
                                <td>".ucfirst($status)."</td>
                                <td>".$counseling[$key]->assistedBy."</td>
                            </tr>";
                }
            }
            if($html == ""){
                $html= "<tr><td colspan='7'><b>There are no counselings found</b></td></tr>";
            }
           
        }
        $myhtml = "
        <html>
            <head>
                <style>
                    table {
                        font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif;
                        border-collapse: collapse;
                        width: 100%;
                    }
                    .well{
                        min-height: 20px;
                        padding: 19px;
                        margin-bottom: 20px;
                        background-color: #f5f5f5;
                        border: 1px solid #e3e3e3;
                        border-radius: 4px;
                        -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
                        box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
                    }
                    table td, table th {
                        border: 1px solid #ddd;
                        padding: 8px;
                    }
                    
                    table tr:nth-child(even){background-color: #f2f2f2;}
                    
                    table tr:hover {background-color: #ddd;}
                    
                    table th {
                        padding-top: 12px;
                        padding-bottom: 12px;
                        text-align: left;
                        background-color: #4CAF50;
                        color: white;
                    }
                    .filterContainer{
                        display:inline-block;
                        width:23%;
                        
                    }
                    @page{
                        margin-left:20px;
                        margin-right:20px;
                        margin-top:160px;
                    }
                    .imageWrapper {
                        float:left;
                    
                    }
                    .thisImage{
                    height:200px;
                    width:150px;
                    }
                    
                </style>
            </head>
            <body>
                ".$this->getAdminSetting()."
                <div style = 'margin-bottom:20px; margin-top:20px;'>
                    <center><h2 style = 'text-decoration:underline;'>$title</h2></center>
                </div>
                <table>
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Student</th>
                           
                            <th>Date Scheduled</th>
                            <th>Date Recorded</th>
                            <th>Counseling Type</th>
                            <th>Status</th>
                            <th>Assisted By</th>
                        </tr>
                    </thead>
                    <tbody>

                    
        ";
        $myhtml .= $html;
        $html.="
                        </tbody>
                    </table>
                </body>
            </html>";
        
        PDF::loadHTML($myhtml)->save(public_path("temp/".'report.pdf'));
        return "true";
    }
    public function counselingCollection($studentid, &$student, $key, $studenttype, $courseGradeLevel, $schoolYear, $semester){
        $mystudent =  new studentController();
        $studenttypetemp = $mystudent->getStudentType($studentid);
        if($studenttype != "all"){
           if($studenttypetemp!=$studenttype){
                $student->forget($key);
                return true;
            }
        }
        if($courseGradeLevel != "all"){
            switch($studenttypetemp){
                case 'college':
                    $temp = collegeStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM college_students WHERE studentID = ".$studentid.")"))->get();
                    if(count($temp)>0){
                        if($temp[0]->courseID != $courseGradeLevel){
                            $student->forget($key);
                            return true;
                        }
                    }else{
                        $student->forget($key);
                        return true;
                    }
                break;
                case 'seniorhigh':
                    $temp = SHSStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM s_h_s_students WHERE studentID = ".$studentid.")"))->get();
                    if(count($temp)>0){
                        if($temp[0]->gradeLevel != $courseGradeLevel){
                            $student->forget($key);
                            return true;
                        }
                    }else{
                        $student->forget($key);
                        return true;
                    }
                break;
                case 'juniorhigh':
                    $temp = JHStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM j_h_students WHERE studentID = ".$studentid.")"))->get();
                    if(count($temp)>0){
                        if($temp[0]->gradeLevel != $courseGradeLevel){
                            $student->forget($key);
                            return true;
                        }
                    }else{
                        $student->forget($key);
                        return true;
                    }
                break;
                case 'elementary':
                    $temp = elemStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM elem_students WHERE studentID = ".$studentid.")"))->get();
                    if(count($temp)>0){
                        if($temp[0]->gradeLevel != $courseGradeLevel){
                            $student->forget($key);
                            return true;
                        }
                    }else{
                        $student->forget($key);
                        return true;
                    }
                break;
            }
        }
        return false;
    }
    public function getAdminSetting(){
        $temp = new \stdClass();
        $count = adminsetting::all();
        if(count($count)>0){
            $adminsetting = adminsetting::find($count[0]->id);
            $html = "<header style = 'position:fixed; top:-100px; left:0px; right:0px; height: 60px;'>
                        <div style = 'width:19%; display:inline-block;  padding: 0px 0px 0px 50px;'>
                            <img style = 'height:100px; width:100px; border-radius:50px;' src = 'images-database/adminsetting/schoolimage/$adminsetting->id.$adminsetting->schoolSealImageType'>
                        </div>
                        <div style = 'width:50%; height:120px; display:inline-block;  margin:0px;'>
                            <center>
                                <h4 class='header' style='color:black; margin:0px;'>$adminsetting->schoolName</h4>
                                <h4 class='header' style='color:black; margin:0px;'>$adminsetting->guidanceName</h4>
                                <h4 class='header' style='color:black; margin:0px;'>$adminsetting->streetAddress, $adminsetting->barangay</h4>
                                <h4 class='header' style='color:black; margin:0px;'>$adminsetting->city</h4>
                                <h4 class='header' style='color:black; margin:0px;'>Philippines, 7000</h4>
                            </center>    
                        </div>
                        <div style = 'width:15%; display:inline-block; padding: 0px 50px 0px 0px;  text-align: right;'>
                            <img style = 'height:100px; width:100px; border-radius:50px;' src = 'images-database/adminsetting/guidanceimage/$adminsetting->id.$adminsetting->guidanceSealImageType'>
                        </div>
                    </header>";
            return $html;
        }else{
            return "";
        }
    }
}
