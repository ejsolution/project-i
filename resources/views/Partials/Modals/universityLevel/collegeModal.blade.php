@component('components.modal')
  @slot('modalid')
    collegeSettingModal
  @endslot
  @slot('modalsize')
    modal-lg
  @endslot
  @slot('modaltitle')
    College Settings
  @endslot
  @slot('modalcontent')
    <div class='row'>
      <div class='accordion' role = 'tablist' aria-multiselectable = 'true' id = 'collegeAccordion'>

      @component('components.accordionTab')
        @slot('headerid')
          addCollegeTitle
        @endslot
        @slot('accordionid')
          collegeAccordion
        @endslot
        @slot('title')
          Add Academics Or Baccalaurette Programs
        @endslot
        @slot('bodyid')
          addCollegeBody
        @endslot
        @slot('content')
          {!! Form::open([
                  'data-parsley-validate'=>'',
                  'id'=>'collegeAddForm']) !!}
                    @include('Partials._message')
            <div class='row'>
              <div class='col-md-6'>
                <label for = 'collegeNameText'>College Name</label>
                <input type= 'text' class='form-control' id ='collegeNameText' name = 'collegeNameText' required maxlength="50">
              </div>
              <div class='col-md-6'>
                <label for = 'collegeCodeText'>College Code</label>
                <input type= 'text' class='form-control' id ='collegeCodeText' name = 'collegeCodeText' required maxlength="5">
              </div>
            </div>
            <div class='row'>
              <div class='col-md-12'>
                <label for ='collegeDescription'>Description</label>
                <textarea id ='collegeDescription' name= 'collegeDescription' class='form-control' rows='3' required maxlength="250"></textarea>
              </div>
            </div>
            <div class='row'>
              <div class='col-md-12 alignRight genericDivTop'>
                <input type='submit' id = 'submitNewCollegeButton' text = 'Save' class='btn btn-dark'>
              </div>
            </div>
          {!! Form::close() !!}
        @endslot
      @endcomponent
      @component('components.accordionTab')
          @slot('headerid')
            AddDepartmentTitle
          @endslot
          @slot('accordionid')
            collegeAccordion
          @endslot
          @slot('title')
            College Departments
          @endslot
          @slot('bodyid')
            addDepartmentBody
          @endslot
          @slot('content')

              @component('components.x_panel')
                @slot('title')
                  Add A Department
                @endslot
                @slot('content')
                  {!! Form::open([
                          'data-parsley-validate'=>'',
                          'id'=>'collegeDepartmentForm']) !!}
                    <div class='row'>
                      <div class='col-md-6'>
                        <label for ='collegeComboBox'>College Of Deparment</label>
                        <select class='form-control' id = 'collegeComboBox' name = 'collegeComboBox' required>
                        </select>
                      </div>
                      <div class='col-md-3'>
                        <label for ='departmentNameText'>Department Name</label>
                        <input type = 'text' maxlength="50" class='form-control' id = 'departmentNameText' name = 'departmentNameText' required>
                      </div>
                      <div class='col-md-3'>
                        <label for ='departmentCodeText'>Department Code</label>
                        <input type = 'text' maxlength="5" class='form-control' id = 'departmentCodeText' name = 'departmentCodeText' required>
                      </div>
                    </div>
                    <div class='row'>
                      <div class='col-md-12 alignRight genericDivTop'>
                        <button id = 'saveDepartment' class='btn btn-dark'>Save Department</button>
                      </div>
                    </div>
                  {!! Form::close() !!}
                  {!! Form::open([
                          'data-parsley-validate'=>'',
                          'id'=>'collegeEditDepartmentForm']) !!}
                  {!! Form::close() !!}
                  <div class='row' class='tableModalContainer'>
                    <table class='table table-striped'>
                      <thead>
                        <tr>
                          <th>Dept. Code</th>
                          <th>Dept. Name</th>
                          <th>Assign Courses</th>
                          <th>Edit</th>
                          <th>Delete</th>
                          <th>Change College</th>
                        </tr>
                      </thead>
                      <tbody id ='departmentsTable'>
                      </tbody>
                    </table>
                  </div>
                @endslot
              @endcomponent
          @endslot
      @endcomponent
      </div>
    </div>
    <div class='row'>
      <div class="form-group has-feedback">
          <input type="text" class="form-control"
          placeholder="Search College Code Or College Name" id = 'searchCollegeModal'/>
          <i class="fa fa-search form-control-feedback"></i>
      </div>
    </div>
    <div class='row'>

      {!! Form::open([
              'data-parsley-validate'=>'',
              'id'=>'collegeEditForm']) !!}
      {!! Form::close() !!}
      <div class='col-md-12 tableModalBigContainer' id = 'collegeTable'>
        <table class='table table-striped'>
          <thead>
            <tr>
              <th>College Code</th>
              <th>College Name</th>
              <th>Description</th>
              <th>View Departments</th>
              <th>Edit</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody id='collegeTableBody'>

          </tbody>
        </table>
      </div>
    </div>
  @endslot
  @slot('modalbutton')
      <!--button type="button" class="btn btn-danger" data-toggle="modal" data-target="#elementarySettingsModal"><i class='fa fa-save'></i>&nbsp;Add Academics Or Baccalaurette Programs</button-->
  @endslot
@endcomponent
