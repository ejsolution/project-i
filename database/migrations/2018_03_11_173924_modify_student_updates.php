<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyStudentUpdates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('student_updates', function (Blueprint $table) {
           
        
            $table->string('firstName')->nullable()->change();
            
            $table->string('lastName')->nullable()->change();
            $table->date('dateOfBirth')->nullable()->change();
            $table->string('gender')->nullable()->change();
            $table->string('status')->nullable()->change();
            $table->string('nationality')->nullable()->change();
            $table->string('religion')->nullable()->change();
            $table->string('ethnicity')->nullable()->change();
            $table->string('language')->nullable()->change();
            $table->string('contactNo')->nullable()->change();
            $table->mediumText('cityAddress')->nullable()->change();
            $table->mediumText('provincialAddress')->nullable()->change();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
