@extends('authViews.authViewTemplate')

@section('title', 'Register')

@section('css')
  <link href="{{asset('css/authStyles/adminRegister.css')}}" rel="stylesheet">
@stop

@section('content')
  <div class = 'flexContainerMiddle' >

    <div class ='regAdminLoginForm'>
      <strong>To complete registration please click the link we sent to your email</strong>
    </div>
  </div>
@stop
