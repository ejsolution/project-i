@extends('admin.adminTemplate')

@section('title', 'AdminDashboard')

@section('stylesheet')
  <link href="{{asset('css/admin/settings.css')}}" rel="stylesheet">
  <link href="{{asset('css/admin/useraccounts/useraccounts.css')}}" rel="stylesheet">
@stop
@section('content')
  <div class='container'>
    @component('components.headerSearch')
      @slot('comboLabel')
        View By Inactive or Active
      @endslot
      @slot('comboID')
        viewByActive
      @endslot
      @slot('comboContent')
        <option value = 'active'>Active</option>
        <option value = 'inactive'>Inactive</option>
      @endslot
      @slot('placeholder')
          search by username or email or name
      @endslot
      @slot('searchID')
        searchUserText
      @endslot
      @slot('buttonID')
       addUserButton
      @endslot
    @endcomponent
    <div class='row'>
      <div class='col-md-12 tableContainer'>
        <table class='table table-striped'>
          <thead>
            <tr> 
              <th>No</th>
              <th>Name</th>
              <th>Username</th>
              <th>Email</th>
              <th>Usertype</th>
              <th>Designation</th>
              <th>Edit</th>
              <th id = 'activeHeader'>Set to inactive</th>
            </tr>
          </thead>
          <tbody id = 'usersAccountsTable'>
          </tbody>
        </table>
      </div>
    </div>
    @include('Partials.Modals.useraccounts.newuseraccount')
  </div>
@endSection

@section('scripts')

  <script src = '{{asset('js/useraccounts/useraccounts.js')}}'></script>
@stop
