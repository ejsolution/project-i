<?php

namespace App\Http\Controllers\dashboard;
use DB;
use Auth;
use App\model\collegeStudent;
use App\model\SHSStudent;
use App\model\JHStudent;
use App\model\elemStudent;
use App\model\systemDate;
use App\model\schoolYear;
use App\model\event;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\activityController;
use App\Http\Controllers\users\userController;
use App\Http\Controllers\notification\notificationController;
class dashboardController extends Controller
{
  public function __construct(){
    $this->middleware('auth:myuser');
  }
  public function getAdminHome(){
    $notif = (new notificationController)->getNotifs();
    return view('admin/adminHome', ['totalCollege'=>$this->getTotalStudents('college'),
                                        'totalSeniorHigh'=>$this->getTotalStudents('seniorhigh'),
                                        'totalJuniorHigh'=>$this->getTotalStudents('juniorhigh'),
                                        'totalElementary'=>$this->getTotalStudents('elementary'),
                                        'notif'=>$notif]);
  }
  public function getTotalStudents($table){
    $row= '';
    switch($table){
      case 'college':
        $row = DB::select(DB::raw($this->sqlTotalRowStudentFaster('college_students')));
      break;
      case 'seniorhigh':
        $row = DB::select(DB::raw($this->sqlTotalRowStudentFaster('s_h_s_students')));
      break;
      case 'juniorhigh':
        $row = DB::select(DB::raw($this->sqlTotalRowStudentFaster('j_h_students')));
      break;
      case 'elementary':
        $row = DB::select(DB::raw($this->sqlTotalRowStudentFaster('elem_students')));
      break;
    }
    return count($row);
  }
  public function sqlTotalRowStudent($table){
    $systemdate = systemDate::all();
    return "SELECT e.firstName, t.id FROM students e
        INNER JOIN (SELECT id, studentID, schoolYearID FROM
        ".$table." INNER JOIN
        (SELECT max(id) AS maxid FROM ".$table."
        group by studentID) cp ON cp.maxid =
        ".$table.".id) t ON e.id = t.studentID
        INNER JOIN school_years ON t.schoolYearID = school_years.id
        WHERE school_years.status ='1'";
  }
  public function sqlTotalRowStudentFaster($table){

    $systemdate = systemDate::all();
    $sql = "SELECT COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) 
    semesterID, COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) 
    schoolYearID, COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) studentType, 
    COALESCE(col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) courseGrade, 
    COALESCE(col.studentNo, shs.studNo, jhs.studNo, elem.studNo) studentNo, students.*,
    col.courseName, col.yearLevel
    FROM students LEFT JOIN (SELECT college_students.studentID, college_students.yearLevel, 
    college_students.semesterID, college_students.schoolYearID, college_students.id, 'college' AS studentType, 
    college_students.courseID, courses.courseName, college_students.studentNo 
    FROM college_students INNER JOIN (SELECT max(id) as maxid FROM college_students GROUP 
    BY studentID) cs ON cs.maxid = college_students.id
    INNER JOIN courses ON courses.id = college_students.courseID) col ON col.studentID = students.id LEFT 
    JOIN (SELECT studentID, semesterID, schoolYearID, id, 'seniorhigh' AS studentType, gradeLevel, studNo 
    FROM s_h_s_students INNER JOIN (SELECT max(id) as maxid FROM s_h_s_students GROUP BY studentID) 
    shs ON shs.maxid = s_h_s_students.id) shs ON shs.studentID = students.id
    LEFT JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'juniorhigh' AS 
    studentType, gradeLevel, studNo  FROM j_h_students INNER JOIN (SELECT max(id) as maxid FROM j_h_students 
    GROUP BY studentID) jhs ON jhs.maxid = j_h_students.id) jhs ON jhs.studentID = students.id LEFT 
    JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'elementary' AS studentType, 
    gradeLevel, studNo FROM elem_students INNER JOIN (SELECT max(id) as maxid FROM elem_students GROUP 
    BY studentID) elem ON elem.maxid = elem_students.id) elem ON elem.studentID = students.id
    INNER JOIN school_years ON COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID)
     = school_years.id
    WHERE  DATE(students.created_at) <= '".$systemdate[0]->systemDate."'
    AND school_years.status ='1'";
    switch ($table) {
      case 'college_students':
        $sql .= " AND COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) ='college'";
      break;
      case 's_h_s_students':
        $sql .= " AND COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) ='seniorhigh'";
      break;
      case 'j_h_students':
        $sql .= " AND COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) ='juniorhigh'";
      break;
      case 'elem_students':
        $sql .= " AND COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) ='elementary'";
      break;
      default:
        # code...
        break;
    }
    return $sql;
  }
  public function getCollegeStudentsPage(){
    $user =  (new userController)->getUserDesignation(Auth::guard('myuser')->id());
    $college ='';
    if($user[0]=='college'){
        $college = DB::select(DB::raw("SELECT colleges.collegeName, courses.courseName, 
        courses.id FROM colleges INNER JOIN (SELECT collegeID, userID, id FROM 
        assignment_colleges INNER JOIN (SELECT max(id) AS maxid FROM assignment_colleges 
        GROUP BY userID) ac ON ac.maxid = assignment_colleges.id) ac2 ON ac2.collegeID = 
        colleges.id INNER JOIN (SELECT collegeID, departmentID, id FROM 
        department_college_hists INNER JOIN (SELECT max(id) as maxid FROM 
        department_college_hists GROUP BY departmentID) dch ON dch.maxid = 
        department_college_hists.id) dch2 ON dch2.collegeID = colleges.id 
        INNER JOIN (SELECT courseID, departmentID, id FROM course_department_hists 
        INNER JOIN (SELECT max(id) as maxid FROM course_department_hists GROUP BY courseID) 
        cdh ON cdh.maxid = course_department_hists.id) cdh2 ON cdh2.departmentID = 
        dch2.departmentID INNER JOIN courses on courses.id  = cdh2.courseID WHERE 
        ac2.collegeID = (SELECT collegeID FROM assignment_colleges WHERE id = 
        (SELECT max(id) FROM assignment_colleges WHERE userID ='".Auth::guard('myuser')->id()."'))
        GROUP BY courses.id"));
    }
    $schoolyears = schoolYear::all(); 
    $notif = (new notificationController)->getNotifs();
    return view('admin/dashboard/collegeStudents', ['schoolyears'=>$schoolyears,
                                                    'user'=>$user,
                                                    'collegeOfUser'=>$college,
                                                    'notif'=>$notif]);
  }
  public function getCollegeStudentsList($schoolyear, $semester, $course, $search=''){
    $sql = "SELECT e.firstName, e.lastName, e.middleName, t.id, t.studentNo, courses.courseName,
            school_years.schoolYearStart, school_years.schoolYearEnd, t.semesterID, t.yearLevel FROM students e
            INNER JOIN (SELECT id, studentID, schoolYearID, semesterID, courseID, studentNo, yearLevel FROM
            college_students INNER JOIN
            (SELECT max(id) AS maxid FROM college_students
            group by studentID) cp ON cp.maxid =
            college_students.id) t ON e.id = t.studentID
            INNER JOIN school_years ON t.schoolYearID = school_years.id
            INNER JOIN courses ON courses.id = t.courseID
            WHERE (school_years.status = '1' AND (e.firstName LIKE '%$search%' OR e.lastName LIKE '%$search%'))";
    if($schoolyear != "all"){
      $sql .= " AND schoolYearID = '$schoolyear'";
    }
    if($semester != "all"){
      $sql .= " AND semesterID = '$semester'";
    }
    if($course != "all"){
      $sql .= " AND courseID = '$course'";
    }
    $sql .= " ORDER BY e.lastName ASC";
    $row = DB::select(DB::raw($sql));
    $html = '';
    if(count($row)==0){
      return "<h2>No college students found!</h2>";
    }else{
      $counter=0;
      foreach ($row as $college) {
        $counter++;
        $html .= $this->getCollegeStudentRow($college, $counter);
      }
      return $html;
    }
  }
  public function getCollegeStudentRow($student, $counter){
    $middlename = '';
    if($student->middleName != ''){
        $middlename = ucfirst(substr($student->middleName, 0, 1)).".";
    }
    return "<tr>
              <td>$counter</td>
              <td>$student->studentNo</td>
              <td>$student->lastName, $student->firstName $middlename</td>
              <td>$student->courseName</td>
              <td>$student->yearLevel</td>
              <td>".explode('-', $student->schoolYearStart)[0]." - ".explode('-', $student->schoolYearEnd)[0]."</td>
            </tr>";
  }
  public function getSeniorStudentsPage(){
    $user =  (new userController)->getUserDesignation(Auth::guard('myuser')->id());
    $college ='';
    $notif = (new notificationController)->getNotifs();
    if($user[0]=='college'){
        $college = DB::select(DB::raw("SELECT colleges.collegeName, courses.courseName, 
        courses.id FROM colleges INNER JOIN (SELECT collegeID, userID, id FROM 
        assignment_colleges INNER JOIN (SELECT max(id) AS maxid FROM assignment_colleges 
        GROUP BY userID) ac ON ac.maxid = assignment_colleges.id) ac2 ON ac2.collegeID = 
        colleges.id INNER JOIN (SELECT collegeID, departmentID, id FROM 
        department_college_hists INNER JOIN (SELECT max(id) as maxid FROM 
        department_college_hists GROUP BY departmentID) dch ON dch.maxid = 
        department_college_hists.id) dch2 ON dch2.collegeID = colleges.id 
        INNER JOIN (SELECT courseID, departmentID, id FROM course_department_hists 
        INNER JOIN (SELECT max(id) as maxid FROM course_department_hists GROUP BY courseID) 
        cdh ON cdh.maxid = course_department_hists.id) cdh2 ON cdh2.departmentID = 
        dch2.departmentID INNER JOIN courses on courses.id  = cdh2.courseID WHERE 
        ac2.collegeID = (SELECT collegeID FROM assignment_colleges WHERE id = 
        (SELECT max(id) FROM assignment_colleges WHERE userID ='".Auth::guard('myuser')->id()."'))
        GROUP BY courses.id"));
    }
    $schoolyears = schoolYear::all(); 
    return view('admin/dashboard/seniorHighStudents', ['schoolyears'=>$schoolyears,
                                                        'user'=>$user,
                                                        'collegeOfUser'=>$college,
                                                        'notif'=>$notif]);
  }
  public function getJuniorStudentsPage(){
    $user =  (new userController)->getUserDesignation(Auth::guard('myuser')->id());
    $college ='';
    if($user[0]=='college'){
        $college = DB::select(DB::raw("SELECT colleges.collegeName, courses.courseName, 
        courses.id FROM colleges INNER JOIN (SELECT collegeID, userID, id FROM 
        assignment_colleges INNER JOIN (SELECT max(id) AS maxid FROM assignment_colleges 
        GROUP BY userID) ac ON ac.maxid = assignment_colleges.id) ac2 ON ac2.collegeID = 
        colleges.id INNER JOIN (SELECT collegeID, departmentID, id FROM 
        department_college_hists INNER JOIN (SELECT max(id) as maxid FROM 
        department_college_hists GROUP BY departmentID) dch ON dch.maxid = 
        department_college_hists.id) dch2 ON dch2.collegeID = colleges.id 
        INNER JOIN (SELECT courseID, departmentID, id FROM course_department_hists 
        INNER JOIN (SELECT max(id) as maxid FROM course_department_hists GROUP BY courseID) 
        cdh ON cdh.maxid = course_department_hists.id) cdh2 ON cdh2.departmentID = 
        dch2.departmentID INNER JOIN courses on courses.id  = cdh2.courseID WHERE 
        ac2.collegeID = (SELECT collegeID FROM assignment_colleges WHERE id = 
        (SELECT max(id) FROM assignment_colleges WHERE userID ='".Auth::guard('myuser')->id()."'))
        GROUP BY courses.id"));
    }
    $notif = (new notificationController)->getNotifs();
    $schoolyears = schoolYear::all(); 
    return view('admin/dashboard/juniorHighStudents', ['schoolyears'=>$schoolyears,
                                                        'user'=>$user,
                                                        'collegeOfUser'=>$college,
                                                        'notif'=>$notif]);
  }
  public function getElementaryStudentsPage(){
    $user =  (new userController)->getUserDesignation(Auth::guard('myuser')->id());
    $college ='';
    if($user[0]=='college'){
        $college = DB::select(DB::raw("SELECT colleges.collegeName, courses.courseName, 
        courses.id FROM colleges INNER JOIN (SELECT collegeID, userID, id FROM 
        assignment_colleges INNER JOIN (SELECT max(id) AS maxid FROM assignment_colleges 
        GROUP BY userID) ac ON ac.maxid = assignment_colleges.id) ac2 ON ac2.collegeID = 
        colleges.id INNER JOIN (SELECT collegeID, departmentID, id FROM 
        department_college_hists INNER JOIN (SELECT max(id) as maxid FROM 
        department_college_hists GROUP BY departmentID) dch ON dch.maxid = 
        department_college_hists.id) dch2 ON dch2.collegeID = colleges.id 
        INNER JOIN (SELECT courseID, departmentID, id FROM course_department_hists 
        INNER JOIN (SELECT max(id) as maxid FROM course_department_hists GROUP BY courseID) 
        cdh ON cdh.maxid = course_department_hists.id) cdh2 ON cdh2.departmentID = 
        dch2.departmentID INNER JOIN courses on courses.id  = cdh2.courseID WHERE 
        ac2.collegeID = (SELECT collegeID FROM assignment_colleges WHERE id = 
        (SELECT max(id) FROM assignment_colleges WHERE userID ='".Auth::guard('myuser')->id()."'))
        GROUP BY courses.id"));
    }
    $schoolyears = schoolYear::all(); 
    $notif = (new notificationController)->getNotifs();
    return view('admin/dashboard/elementary', ['schoolyears'=>$schoolyears,
                                                'user'=>$user,
                                                'collegeOfUser'=>$college,
                                                'notif'=>$notif]);
  }
  public function getSeniorHighStudentsList($schoolyear, $semester, $course, $search=''){
    $systemdate = systemDate::all();
    
    $sql = "SELECT COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) 
    semesterID, COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) 
    schoolYearID, COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) studentType, 
    COALESCE(col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) gradeLevel, 
    COALESCE(col.studentNo, shs.studNo, jhs.studNo, elem.studNo) studNo, students.*,
    school_years.schoolYearStart, school_years.schoolYearEnd,
    col.courseName, col.yearLevel,
    shs.strandName
    FROM students LEFT JOIN (SELECT college_students.studentID, college_students.yearLevel, 
    college_students.semesterID, college_students.schoolYearID, college_students.id, 'college' AS studentType, 
    college_students.courseID, courses.courseName, college_students.studentNo
    FROM college_students INNER JOIN (SELECT max(id) as maxid FROM college_students GROUP 
    BY studentID) cs ON cs.maxid = college_students.id
    INNER JOIN courses ON courses.id = college_students.courseID) col ON col.studentID = students.id LEFT 
    JOIN (SELECT s_h_s_students.studentID, s_h_s_students.semesterID, s_h_s_students.schoolYearID, s_h_s_students.id, 'seniorhigh' AS studentType, s_h_s_students.gradeLevel, s_h_s_students.studNo, senior_high_strands.strandName 
    FROM s_h_s_students INNER JOIN (SELECT max(id) as maxid FROM s_h_s_students GROUP BY studentID) 
    shs ON shs.maxid = s_h_s_students.id
    INNER JOIN senior_high_strands ON senior_high_strands.id = s_h_s_students.seniorHighStrandID) shs ON shs.studentID = students.id
    LEFT JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'juniorhigh' AS 
    studentType, gradeLevel, studNo  FROM j_h_students INNER JOIN (SELECT max(id) as maxid FROM j_h_students 
    GROUP BY studentID) jhs ON jhs.maxid = j_h_students.id) jhs ON jhs.studentID = students.id LEFT 
    JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'elementary' AS studentType, 
    gradeLevel, studNo FROM elem_students INNER JOIN (SELECT max(id) as maxid FROM elem_students GROUP 
    BY studentID) elem ON elem.maxid = elem_students.id) elem ON elem.studentID = students.id
    INNER JOIN school_years ON COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID)
    = school_years.id
    WHERE COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) = 'seniorhigh'
    AND DATE(students.created_at) <= '".$systemdate[0]->systemDate."'
    AND (students.firstName LIKE '%$search%' OR students.lastName LIKE '%$search%')";
    if($schoolyear != "all"){
      $sql .= " AND COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) = '$schoolyear'";
    }
    if($course != "all"){
      $sql .= " AND COALESCE(col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) = '$course'";
    }
    $sql .= " ORDER BY students.lastName ASC";
    $row = DB::select(DB::raw($sql));
    $html = '';
    if(count($row)==0){
      return "<h2>No seniorhigh students found!</h2>";
    }else{
      $counter=0;
      foreach ($row as $college) {
        $counter++;
        $html .= $this->getSeniorStudentRow($college, $counter);
      }
      return $html;
    }
  }
  public function getSeniorStudentRow($student, $counter){
    $middlename = '';
    if($student->middleName != ''){
        $middlename = ucfirst(substr($student->middleName, 0, 1)).".";
    }
    return "<tr>
              <td>$counter</td>
              <td>$student->studNo</td>
              <td>$student->lastName, $student->firstName $middlename</td>
              <td>$student->strandName</td>
              <td>$student->gradeLevel</td>
              <td>".explode('-', $student->schoolYearStart)[0]." - ".explode('-', $student->schoolYearEnd)[0]."</td>
            </tr>";
  }
  public function getJuniorHighList($schoolyear, $semester, $course, $search=''){
    
    $systemdate = systemDate::all();
    
    $sql = "SELECT COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) 
    semesterID, COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) 
    schoolYearID, COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) studentType, 
    COALESCE(col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) gradeLevel, 
    COALESCE(col.studentNo, shs.studNo, jhs.studNo, elem.studNo) studNo, students.*,
    school_years.schoolYearStart, school_years.schoolYearEnd,
    col.courseName, col.yearLevel,
    shs.strandName
    FROM students LEFT JOIN (SELECT college_students.studentID, college_students.yearLevel, 
    college_students.semesterID, college_students.schoolYearID, college_students.id, 'college' AS studentType, 
    college_students.courseID, courses.courseName, college_students.studentNo
    FROM college_students INNER JOIN (SELECT max(id) as maxid FROM college_students GROUP 
    BY studentID) cs ON cs.maxid = college_students.id
    INNER JOIN courses ON courses.id = college_students.courseID) col ON col.studentID = students.id LEFT 
    JOIN (SELECT s_h_s_students.studentID, s_h_s_students.semesterID, s_h_s_students.schoolYearID, s_h_s_students.id, 'seniorhigh' AS studentType, s_h_s_students.gradeLevel, s_h_s_students.studNo, senior_high_strands.strandName 
    FROM s_h_s_students INNER JOIN (SELECT max(id) as maxid FROM s_h_s_students GROUP BY studentID) 
    shs ON shs.maxid = s_h_s_students.id
    INNER JOIN senior_high_strands ON senior_high_strands.id = s_h_s_students.seniorHighStrandID) shs ON shs.studentID = students.id
    LEFT JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'juniorhigh' AS 
    studentType, gradeLevel, studNo  FROM j_h_students INNER JOIN (SELECT max(id) as maxid FROM j_h_students 
    GROUP BY studentID) jhs ON jhs.maxid = j_h_students.id) jhs ON jhs.studentID = students.id LEFT 
    JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'elementary' AS studentType, 
    gradeLevel, studNo FROM elem_students INNER JOIN (SELECT max(id) as maxid FROM elem_students GROUP 
    BY studentID) elem ON elem.maxid = elem_students.id) elem ON elem.studentID = students.id
    INNER JOIN school_years ON COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID)
    = school_years.id
    WHERE COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) = 'juniorhigh'
    AND DATE(students.created_at) <= '".$systemdate[0]->systemDate."'
    AND (students.firstName LIKE '%$search%' OR students.lastName LIKE '%$search%')";
    if($schoolyear != "all"){
      $sql .= " AND COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) = '$schoolyear'";
    }
    if($course != "all"){
      $sql .= " AND  COALESCE(col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) = '$course'";
    }
    $sql .= " ORDER BY students.lastName ASC";
    $row = DB::select(DB::raw($sql));
    $html = '';
    if(count($row)==0){
      return "<h2>No juniorhigh students found!</h2>";
    }else{
      $counter=0;
      foreach ($row as $college) {
        $counter++;
        $html .= $this->getJuniorStudentRow($college, $counter);
      }
      return $html;
    }
  }
  public function getJuniorStudentRow($student, $counter){
    $middlename = '';
    if($student->middleName != ''){
        $middlename = ucfirst(substr($student->middleName, 0, 1)).".";
    }
    return "<tr>
              <td>$counter</td>
              <td>$student->studNo</td>
              <td>$student->lastName, $student->firstName $middlename</td>
              <td>$student->gradeLevel</td>
              <td>".explode('-', $student->schoolYearStart)[0]." - ".explode('-', $student->schoolYearEnd)[0]."</td>
            </tr>";
  }
  public function getElementaryList($schoolyear, $semester, $course, $search=''){
    $systemdate = systemDate::all();
    
    $sql = "SELECT COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) 
    semesterID, COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) 
    schoolYearID, COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) studentType, 
    COALESCE(col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) gradeLevel, 
    COALESCE(col.studentNo, shs.studNo, jhs.studNo, elem.studNo) studNo, students.*,
    school_years.schoolYearStart, school_years.schoolYearEnd,
    col.courseName, col.yearLevel,
    shs.strandName
    FROM students LEFT JOIN (SELECT college_students.studentID, college_students.yearLevel, 
    college_students.semesterID, college_students.schoolYearID, college_students.id, 'college' AS studentType, 
    college_students.courseID, courses.courseName, college_students.studentNo
    FROM college_students INNER JOIN (SELECT max(id) as maxid FROM college_students GROUP 
    BY studentID) cs ON cs.maxid = college_students.id
    INNER JOIN courses ON courses.id = college_students.courseID) col ON col.studentID = students.id LEFT 
    JOIN (SELECT s_h_s_students.studentID, s_h_s_students.semesterID, s_h_s_students.schoolYearID, s_h_s_students.id, 'seniorhigh' AS studentType, s_h_s_students.gradeLevel, s_h_s_students.studNo, senior_high_strands.strandName 
    FROM s_h_s_students INNER JOIN (SELECT max(id) as maxid FROM s_h_s_students GROUP BY studentID) 
    shs ON shs.maxid = s_h_s_students.id
    INNER JOIN senior_high_strands ON senior_high_strands.id = s_h_s_students.seniorHighStrandID) shs ON shs.studentID = students.id
    LEFT JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'juniorhigh' AS 
    studentType, gradeLevel, studNo  FROM j_h_students INNER JOIN (SELECT max(id) as maxid FROM j_h_students 
    GROUP BY studentID) jhs ON jhs.maxid = j_h_students.id) jhs ON jhs.studentID = students.id LEFT 
    JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'elementary' AS studentType, 
    gradeLevel, studNo FROM elem_students INNER JOIN (SELECT max(id) as maxid FROM elem_students GROUP 
    BY studentID) elem ON elem.maxid = elem_students.id) elem ON elem.studentID = students.id
    INNER JOIN school_years ON COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID)
    = school_years.id
    WHERE COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) = 'elementary'
    AND DATE(students.created_at) <= '".$systemdate[0]->systemDate."'
    AND (students.firstName LIKE '%$search%' OR students.lastName LIKE '%$search%')";
    if($schoolyear != "all"){
      $sql .= " AND COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) = '$schoolyear'";
    }
    if($course != "all"){
      $sql .= " AND  COALESCE(col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) = '$course'";
    }
    $sql .= " ORDER BY students.lastName ASC";
    $row = DB::select(DB::raw($sql));
    $html = '';
    if(count($row)==0){
      return "<h2>No elementary students found!</h2>";
    }else{
    $counter=0;
    foreach ($row as $college) {
      $counter++;
      $html .= $this->getElementaryRow($college, $counter);
    }
      return $html;
    }
  }
  public function getElementaryRow($student, $counter){
    $middlename = '';
    if($student->middleName != ''){
        $middlename = ucfirst(substr($student->middleName, 0, 1)).".";
    }
    return "<tr>
              <td>$counter</td>
              <td>$student->studNo</td>
              <td>$student->lastName, $student->firstName $middlename</td>
              <td>$student->gradeLevel</td>
              <td>".explode('-', $student->schoolYearStart)[0]." - ".explode('-', $student->schoolYearEnd)[0]."</td>
            </tr>";
  }
  public function getStatisticsContract(){
    $sql = "SELECT contracts.collegStudentID, contracts.id, 
    colleges.collegeName FROM contracts INNER JOIN 
    college_students ON college_students.id = contracts.collegStudentID 
    INNER JOIN (SELECT courseID, id, departmentID FROM course_department_hists 
    INNER JOIN (SELECT MAX(id) as maxid FROM course_department_hists GROUP BY 
    courseID) cdh ON cdh.maxid = course_department_hists.id) cdh2 ON cdh2.courseID = 
    college_students.courseID INNER JOIN (SELECT departmentID, collegeID, id FROM 
    department_college_hists INNER JOIN (SELECT MAX(id) as maxid FROM department_college_hists 
    GROUP BY departmentID) dch ON dch.maxid = department_college_hists.id) dch2 ON 
    dch2.departmentID = cdh2.departmentID INNER JOIN colleges ON colleges.id = dch2.collegeID
    INNER JOIN school_years ON school_years.id = contracts.schoolYearID
    WHERE school_years.status = '1'";
    $colleges = DB::select(DB::raw($sql));
    $temp = new \stdClass();
    if(count($colleges)>0){
      $colArr= [];
      
      foreach($colleges as $college){
        if(array_key_exists($college->collegeName,$colArr)){
          $colArr[$college->collegeName] +=1;
        }else{
          $colArr[$college->collegeName]=1;
        }
      }
      //getTop1
      arsort($colArr);
      $i=0;
      
      foreach ($colArr as $key => $value) {
        switch($i){
          case 0:
            $temp->firstC = $key;
            $temp->firstCount  = $colArr[$key];
          break;
          case 1:
            $temp->secondC = $key;
            $temp->secondCount  = $colArr[$key];
          break;
          case 2:
            $temp->thirdC = $key;
            $temp->thirdCount  = $colArr[$key];
          break;
          case 3:
            $temp->fourthC = $key;
            $temp->fourthCount  = $colArr[$key];
          break;
          case 4:
            $temp->fifthC = $key;
            $temp->fifthCount  = $colArr[$key];
          break; 
        }
        if($i>4){
          return $temp;
        }
        $i++;
      }
      $temp->num = $i--;
      return response()->json($temp);
    }else{
      return "0";
    }
  }
  public function getStatisticsCounseling(){
    $temp = new \stdClass();
    $temp->january = $this->getTotalCounselingMonth("01");
    $temp->february = $this->getTotalCounselingMonth("02");
    $temp->march = $this->getTotalCounselingMonth("03");
    $temp->april = $this->getTotalCounselingMonth("04");
    $temp->may = $this->getTotalCounselingMonth("05");
    $temp->june = $this->getTotalCounselingMonth("06");
    $temp->july = $this->getTotalCounselingMonth("07");
    $temp->august = $this->getTotalCounselingMonth("08");
    $temp->september = $this->getTotalCounselingMonth("09");
    $temp->october = $this->getTotalCounselingMonth("10");
    $temp->november = $this->getTotalCounselingMonth("11");
    $temp->december = $this->getTotalCounselingMonth("12");
    return response()->json($temp);
  }
  public function getTotalCounselingMonth($month){
    $sql = "SELECT    COUNT(*) AS total
    FROM      counselings 
     INNER JOIN school_years ON school_years.id  = counselings.schoolYearID
     WHERE     MONTH(counselings.dateRecorded) = '$month' AND school_years.status = '1'";
     $total = DB::select(DB::raw($sql));
     if(count($total)>0){
       return $total[0]->total;
     }else{
       return 0;
     }
  }
  public function getStatisticsReferral(){
    $temp = new \stdClass();
    $temp->january = $this->getTotalReferralMonth("01");
    $temp->february = $this->getTotalReferralMonth("02");
    $temp->march = $this->getTotalReferralMonth("03");
    $temp->april = $this->getTotalReferralMonth("04");
    $temp->may = $this->getTotalReferralMonth("05");
    $temp->june = $this->getTotalReferralMonth("06");
    $temp->july = $this->getTotalReferralMonth("07");
    $temp->august = $this->getTotalReferralMonth("08");
    $temp->september = $this->getTotalReferralMonth("09");
    $temp->october = $this->getTotalReferralMonth("10");
    $temp->november = $this->getTotalReferralMonth("11");
    $temp->december = $this->getTotalReferralMonth("12");
    return response()->json($temp);
  }
  public function getTotalReferralMonth($month){
    $sql = "SELECT    COUNT(*) AS total
    FROM      referrals 
     INNER JOIN school_years ON school_years.id  = referrals.schoolYearID
     WHERE     MONTH(referrals.created_at) = '$month' AND school_years.status = '1'";
     $total = DB::select(DB::raw($sql));
     if(count($total)>0){
       return $total[0]->total;
     }else{
       return 0;
     }
  }
  public function getUpcomingEvents(){
    $systemdate = systemDate::all();
    $event  = DB::select(DB::raw("SELECT events.*,  datediff(events.dateStart, '".$systemdate[0]->systemDate."') AS days
                                FROM events WHERE DATE(events.created_at) <= '".$systemdate[0]->systemDate."'
                                AND datediff(events.dateStart, '".$systemdate[0]->systemDate."')
                                <= '30'
                                AND  datediff(events.dateStart, '".$systemdate[0]->systemDate."') >= '0'"));
    if(count($event)>0){
      $html = '';
      foreach ($event as $events) {
        $html .= "<div class='eventContainer'>
                      <div>
                        <h3>$events->eventsTitle</h3>
                      </div>
                      <div>
                        <label style = 'color:white;'>$events->dateStart to $events->dateEnd</label>
                        <p>$events->days days to go.</p>
                      </div>
                      <div class='eventsFooter'>
                        <a href = '".route("event.view", $events->id)."'>View</a>
                      </div>
                  </div>";
      }
      return $html;
    }else{
      return "<h3>No upcoming events that are less than one month of today's date</h3>";
    }
  }
  public function viewEvent($eventid){
    $schoolyears = schoolYear::all();
    $notif = (new notificationController)->getNotifs();

    $user =  (new userController)->getUserDesignation(Auth::guard('myuser')->id());
    $college ='';
    if($user[0]=='college'){
        $college = DB::select(DB::raw("SELECT colleges.collegeName, courses.courseName, 
        courses.id FROM colleges INNER JOIN (SELECT collegeID, userID, id FROM 
        assignment_colleges INNER JOIN (SELECT max(id) AS maxid FROM assignment_colleges 
        GROUP BY userID) ac ON ac.maxid = assignment_colleges.id) ac2 ON ac2.collegeID = 
        colleges.id INNER JOIN (SELECT collegeID, departmentID, id FROM 
        department_college_hists INNER JOIN (SELECT max(id) as maxid FROM 
        department_college_hists GROUP BY departmentID) dch ON dch.maxid = 
        department_college_hists.id) dch2 ON dch2.collegeID = colleges.id 
        INNER JOIN (SELECT courseID, departmentID, id FROM course_department_hists 
        INNER JOIN (SELECT max(id) as maxid FROM course_department_hists GROUP BY courseID) 
        cdh ON cdh.maxid = course_department_hists.id) cdh2 ON cdh2.departmentID = 
        dch2.departmentID INNER JOIN courses on courses.id  = cdh2.courseID WHERE 
        ac2.collegeID = (SELECT collegeID FROM assignment_colleges WHERE id = 
        (SELECT max(id) FROM assignment_colleges WHERE userID ='".Auth::guard('myuser')->id()."'))
        GROUP BY courses.id"));
    }
    
    return view('admin/events/events', ['notif'=>$notif,
                                        'user'=>$user,
                                        'collegeOfUser'=>$college,
                                        'schoolyears'=>$schoolyears,
                                        'eventid' => $eventid]);
  }
}
