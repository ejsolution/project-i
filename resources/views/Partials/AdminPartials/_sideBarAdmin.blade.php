            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li id = 'homePageAdmin'><a><i class="fa fa-home"></i> Main <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li id = 'dashBoardAdmin'><a href="{{route('admin.home')}}"><i class="fa fa-home"></i>Home</a></li>
                      <li id = 'reportsAdmin'><a href="{{route('reports')}}"><i class="fa fa-bar-chart"></i>Reports</a></a></li>
                      <!--li id = 'archiveAdmin'><a href="{{route('archive')}}"><i class="fa fa-archive"></i>Archive</a></a></li-->
                    </ul>
                  </li>
                  <li  id = 'transactionPageAdmin'><a href = '#'><i class="fa fa-exchange"></i> Transactions <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li id = 'studentPersonalDataAdmin'><a href="{{route('studentpersonaldata')}}"><i class="fa fa-clipboard"></i>Student Personal Data</a></li>
                      <li id ='counselingAdmin'><a href="{{route('counceling')}}"><i class="fa fa-heart"></i>Counseling</a></li>
                      <li id = 'referralAdmin'><a href="{{route('referral')}}"><i class="fa fa-user-plus"></i>Referral</a></li>
                      @if(session()->get('user')['designation']=='' || session()->get('user')['userType']=='admin')
                        <li id = 'contractAdmin'><a href="{{route('contract')}}"><i class="fa fa-book"></i>Contract</a></li>
                      @endif
                      @if(session()->get('user')['userType']=='admin')
                        <li id = 'eventAdmin'><a href="{{route('events')}}"><i class="fa fa-calendar"></i>Events</a></li>
                      @endif
                    </ul>
                  </li>
                  <h3>Context</h3>
                  <li id = 'settingsAdmin'><a><i class="fa fa-cog"></i> Settings <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      @if(session()->get('user')['userType']=='admin')
                        <li id = 'userAccounts'><a href="{{route('useraccounts')}}"><i class="fa fa-users"></i>Manage Users</a></li>
                      @endif
                      <li id = 'activityLogs'><a href="{{route('activitylogs')}}"><i class="fa fa-clipboard"></i>Activity Logs</a></li>
                      @if(session()->get('user')['userType']=='admin')
                        <li data-toggle="modal" data-target="#manageLayoutModal"><a href="#"><i class="fa fa-pencil-square-o"></i>Manage Layouts</a></li>
                      @endif
                      <li id = 'universitySchoolYear'><a href="{{route('universityschoolyear')}}"><i class="fa fa-pencil-square-o"></i>University School Year</a></li>
                      <li data-toggle="modal" data-target="#subjectsSettingsModal"><a href="#"><i class="fa fa-pencil-square-o"></i>Manage Subjects</a></li>
                      <li data-toggle="modal" data-target="#personalitySettingsModal"><a href="#"><i class="fa fa-pencil-square-o"></i>Manage Personalities</a></li>
                      <li data-toggle="modal" data-target="#behaviorSettingModal"><a href="#"><i class="fa fa-pencil-square-o"></i>Manage Behaviours</a></li>
                      <li data-toggle="modal" data-target="#counselorSettingModal"><a href="#"><i class="fa fa-pencil-square-o"></i>Manage Counselors</a></li>
                    </ul>
                  </li>
                  @if(session()->get('user')['userType']=='admin')
                    <li><a><i class="fa fa-university"></i> University Level<span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                        <li  data-toggle="modal" data-target="#collegeSettingModal"><a href="#"><i class="fa fa-pencil-square-o"></i>College</a></li>
                        <li data-toggle="modal" data-target="#seniorHighSettingModal"><a href="#"><i class="fa fa-pencil-square-o"></i>Senior High</a></li>
                      </ul>
                    </li>
                  @endif
                    </ul>
                  </li>
                </ul>
              </div>
              
            </div>
