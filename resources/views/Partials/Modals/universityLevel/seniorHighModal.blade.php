@component('components.modal')
  @slot('modalid')
    seniorHighSettingModal
  @endslot
  @slot('modalsize')
    modal-md
  @endslot
  @slot('modaltitle')
    Senior High Settings
  @endslot
  @slot('modalcontent')
    <div clas='row'>
      @component('components.x_panel')
        @slot('title')
          Add a Senior High Strand
        @endslot
        @slot('content')
          {!! Form::open([
                  'id'=>'seniorHighStrandAddForm']) !!}
            <div class='row'>
              <div class='col-md-6'>
                <div class='row'>
                  <label for ='seniorHighStrandCodeText'>Strand Code</label>
                  <input type = 'text' class='form-control' id ='seniorHighStrandCodeText' name = 'seniorHighStrandCodeText' required maxlength="5">
                </div>
                <div class='row'>
                  <label for ='seniorHighStrandNameText'>Strand Name</label>
                  <input type = 'text' class='form-control' id ='seniorHighStrandNameText' name = 'seniorHighStrandNameText' required maxlength="50">
                </div>
              </div>
              <div class='col-md-6'>
                <label for = 'strandDescriptionText'>Description</label>
                <textarea id = 'strandDescriptionText' name = 'strandDescriptionText' class='form-control' rows='4' required maxlength="250"></textarea>
              </div>
            </div>
            <div class='row'>
              <div class='col-md-12 genericDivTop alignRight'>
                <button class='btn btn-dark'>Save</button>
              </div>
            </div>
          {!! Form::close() !!}
        @endslot
      @endcomponent
    </div>
    <div class='row'>
      <div class="form-group has-feedback">
          <input type="text" class="form-control"
          placeholder="Search strand code or name" id = 'searchSeniorHighStrand'/>
          <i class="fa fa-search form-control-feedback"></i>
      </div>
    </div>
    <div class='row'>
      {!! Form::open([
              'id'=>'seniorHighEditForm']) !!}
      {!! Form::close() !!}
      <div class='col-md-12 tableModalContainer'>
        <table class='table table-striped'>
          <thead>
            <tr>
              <th>Strand Code</th>
              <th>Strand Name</th>
              <th>Strand Description</th>
              <th>Edit</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody id = 'seniorHighTable'>
          </tbody>
        </table>
      </div>
    </div>
  @endslot
  @slot('modalbutton')

  @endslot
@endcomponent
