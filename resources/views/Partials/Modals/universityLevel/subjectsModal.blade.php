@component('components.modal')
    @slot('modalid')
    subjectsSettingsModal
    @endslot
    @slot('modalsize')
    modal-md
    @endslot
    @slot('modaltitle')
    Subjects
    @endslot
    @slot('modalcontent')
    {!! Form::open([
    'id'=>'subjectAddForm']) !!}
    @component('components.x_panel')
        @slot('title')
        Add A Subject
        @endslot
        @slot('content')
            <div class='row'>
                <div class='col-md-6'>
                    <label for ='subjectNameAdd'>Subject Name:</label>
                    <input type ='text' name ='subjectNameAdd' id ='subjectNameAdd' class='form-control' required maxlength ='100'>
                </div>
                <div class='col-md-6'>
                    <label for ='subjectCodeAdd'>Subject Code:</label>
                    <input type ='text' name ='subjectCode' id ='subjectCodeAdd' class='form-control' required>
                </div>
            </div>
            <div class='row'>
                <div class='col-md-12'>
                    <label for ='subjectDescriptionAdd'>Description</label>
                    <textarea rows='3' id ='subjectDescriptionAdd' name ='subjectDescriptionAdd' class='form-control'></textarea>
                </div>        
            </div>
            <div class='row'>
                <div class='col-md-12 genericDivTop alignRight'>
                    <button class='btn btn-dark' id ='saveButton'>Save</button>
                </div>
            </div>
        @endslot
    @endcomponent
    {!! Form::close() !!}
    {!! Form::open([
        'id'=>'subjectEditForm']) !!}
            
    {!! Form::close() !!}
    <table class='table table-striped'>
        <thead>
            <tr>
                <th>Name</th>
                <th>Subject Code</th>
                <th>Description</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </thead>
        <tbody id ='subjectTable'>
        </tbody>
    </table>
    @endslot
    @slot('modalbutton')
   
    @endslot
@endcomponent