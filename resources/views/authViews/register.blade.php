@extends('authViews.authViewTemplate')

@section('title', 'Register')

@section('css')
  <link href="{{asset('css/authStyles/adminRegister.css')}}" rel="stylesheet">
@stop

@section('content')
  <div class = 'flexContainerMiddle' >

    <div class ='regAdminLoginForm'>
      <div><center><strong>Admin-Register</strong></center></div>
      @include('Partials._message')
      {!! Form::open(['route' => 'post.admin',
              'data-parsley-validate'=>'',
              'files'=>true]) !!}
        <div>
          <center>
            <img src="{{asset('images/defaultimage.png')}}" id = 'profileImage' class="genericImageCircle" alt="Cinque Terre">
          </center>
        </div>
        <div>
        {{Form::label('uploadProfileImage', 'Upload Profile Picture:')}}
        {{Form::file('uploadProfileImage', array('accept'=>
          '.png,.jpg'
        ))}}
      </div>
        <div class='row'>
          <div class='col-lg-6'>
            {{Form:: label('firstname', 'First Name:')}}
            {{Form:: text('firstname', null, array(
                              'placeholder'=>'First Name',
                              'class'=>'form-control',
                              'required'=>'',
                              'maxlength'=>'100'
                              ))}}
          </div>
          <div class='col-lg-6'>
            {{Form:: label('lastname', 'Last Name:')}}
            {{Form:: text('lastname', null,array(
                              'placeholder'=>'Last Name',
                              'class'=>'form-control',
                              'required'=>'',
                              'maxlength'=>'100'))}}
          </div>
        </div>
        {{Form:: label('userName', 'Username:')}}
        {{Form:: text('userName', null, array(
                          'placeholder'=>'Username',
                          'class'=>'form-control',
                          'required'=>'',
                          'maxlength'=>'100'))}}


        <div class='row'>
          <div class='col-lg-6'>
            {{Form:: label('password', 'Password:')}}
            {{Form:: password('password', array(
                              'placeholder'=>'Password',
                              'class'=>'form-control',
                              'required'=>'',
                              'maxlength'=>'100',
                              'data-parsley-equalto'=>"#password_confirmation"))}}
          </div>
          <div class='col-lg-6'>
            {{Form:: label('password_confirmation', 'Confirm Password:')}}
            {{Form:: password('password_confirmation', array(
                              'placeholder'=>'Confirm Password',
                              'class'=>'form-control',
                              'required'=>'',
                              'maxlength'=>'100',
                              'data-parsley-equalto'=>"#password"))}}
          </div>
        </div>

        {{Form:: label('adminKey', 'Admin Key:')}}
        {{Form:: password('adminKey', array(
                          'placeholder'=>'Confirm Password',
                          'class'=>'form-control',
                          'required'=>'',
                          'maxlength'=>'100'))}}
        {{Form:: label('email', 'Email:')}}
        {{Form:: email('email', null, array(
                          'placeholder'=>'Email',
                          'class'=>'form-control',
                          'required'=>'',
                          'maxlength'=>'100',
                          'data-parsley-type='=>"email"))}}

        {{Form::submit('Register Admin', array('class'=>'btn btn-success btn-block',
                            'style'=>'margin-top:20px;'))}}
      {!! Form::close() !!}
    </div>
  </div>
@stop

@section('script')
  <script src= '{{asset('js/authJS/adminRegister.js')}}'></script>
@stop
