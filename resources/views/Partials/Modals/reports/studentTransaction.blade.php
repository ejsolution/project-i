<div id="StudentTransactions" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Student Transaction</h4>
      </div>
      <div class="modal-body">
        <div class='container-fluid'>
          <div class='row'>
            <div class='col-md-12'>
              <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#studentListsModal">Student Personal Data Updates</a></li>
                <li><a data-toggle="tab" href="#referralListModal">Referral Transaction</a></li>
                <li><a data-toggle="tab" href="#contractListModal">Contract Transaction</a></li>
                <li><a data-toggle="tab" href="#counselingListModal">Counseling Transaction</a></li>
              </ul>
            </div>
          </div>
          <div class='row'>
            <div class='col-md-12 well'>
              <div>
                <label>
                  Student Information
                </label>
              </div>
              <div class='row'>
                <div class='col-md-4'>
                  <div><label for = 'name'>Name</label></div>
                  <div><input type = 'text' id = 'nameTextModal' class='form-control' disabled></div>
                </div>
                <div class='col-md-4'>
                  <div><label for = 'studentTypeModal'>Student Type</label></div>
                  <div>
                    <input type = 'text' id = 'studentTypeTextModal' class='form-control' disabled>
                  </div>
                </div>
               
                <div class='col-md-4'>
                  <div><label for = 'sexTextModal'>Sex</label></div>
                  <div>
                    <input type = 'text' id = 'sexTextModal' class='form-control' disabled>
                  </div>
                </div>
                <div class='col-md-6'>
                  <div><label for = 'courseTextModal'>Course/Grade Level</label></div>
                  <div>
                    <input type = 'text' id = 'courseTextModal' class='form-control' disabled>
                  </div>
                </div>
                <div class='col-md-6'>
                  <div><label for = 'homeAddressTextModal'>Home Address</label></div>
                  <div>
                    <input type = 'text' id = 'homeAddressTextModal' class='form-control' disabled>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class='row'>
            <div class='col-md-12'>
              <div class="tab-content">
                <div class='tab-pane fade in active' id ='studentListsModal'>
                  <div class='container-fluid'>
                    <div class='row'>
                      <div class='col-md-7'>
                        <!--div class='row'>
                          <div class="form-group has-feedback" style = 'padding-top:0px;'>
                              <input type="text" class="form-control" id = 'searchStudentListModal' placeholder="name" disabled/>
                              <i class="fa fa-search form-control-feedback"></i>
                          </div>
                        </div-->
                        <div class='row'>
                          <div class='well maxHeightContainer'>
                            <table class='table table-condensed'>
                              <thead>
                                <tr>
                                  <!--th>No.</th-->
                                  <th>Date</th>
                                  <th>Updated Student Data</th>
                                  <!--th>Information Details</th-->
                                </tr>
                              </thead>
                              <tbody id ='tableForStudentUpdates'>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                      <div class='col-md-5'>
                        <div class='row'>
                          <button class='btn btn-warning btn-block' id ='printStudentDataUpdates'><i class='fa fa-print'></i> &nbsp; Print Student Personal Data Updates</button>
                        </div>
                        <div class='row well'>
                          <label for='dateStudentUpdatedDataTextModal'>Date</label>
                          <input type = 'text' id = 'dateStudentUpdatedDataTextModal' class='form-control' disabled>

                          <label for='updateStudentDataTextModal'>Updated Student Data</label>
                          <input type = 'text' id = 'updateStudentDataTextModal' class='form-control' disabled>

                          <!--label for='informationDetailsTextModal'>Information Details</label>
                          <textarea id = 'informationDetailsTextModal' class='form-control' disabled></textarea-->
                          <label for='updateByTextModal'>Updated By</label>
                          <input type = 'text' id = 'updateByTextModal' class='form-control' disabled>
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
                <div class='tab-pane fade' id ='counselingListModal'>
                  <div class='container-fluid'>
                    <div class='row'>
                      {{--  <div class='col-md-8'>
                        <div class="form-group has-feedback" style = 'padding-top:0px;'>
                            <input type="text" class="form-control" id = 'searchCounselingTextModal' placeholder="Search by counseling type, date or schoolyear"/>
                            <i class="fa fa-search form-control-feedback"></i>
                        </div>
                      </div>  --}}
                      <div class='col-md-4'>
                        <button class='btn btn-warning btn-block' id ='printCounselingOfStudent'><i class='fa fa-print'></i>&nbsp; Print Counseling And Evaluation</button>
                      </div>
                    </div>
                    <div class='row'>
                      <div class='col-md-12 well'>
                        <table class='table table-condensed'>
                          <thead>
                            <tr>
                              <th>Counseling Date</th>
			                        <th>Date Scheduled</th>
                              <th>School Year</th>
                              <th>Semester</th>
                              <th>Counseling Type</th>
                              <th>Assisted By</th>
                              <th>Status</th>
                              <th>Statement of the problem</th>
                              <th>Counselor</th>
                            </tr>
                          </thead>
                          <tbody id ='counselingStudentTable'>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div class='row'>
                      <div class='col-md-12 well'>
                          <h3>Follow Up of counseling</h3>
                          <table class='table table-condensed'>
                            <thead>
                              <tr>
                                <th>Details</th>
                                <th>Assisted By</th>
                                <th>Follow Up Date</th>
                              </tr>
                            </thead>
                            <tbody id ='followUpOfCounseling'>
                            </tbody>
                          </table>
                        </div>
                    </div>
                    <div class='row'>
                      <div class='col-md-4 well'>
                        <div>
                          <label>Counseling Date</label>
                        </div>
                        <div>
                          <input type = 'date' id = 'counselingDateTextModal' class='form-control' disabled>
                        </div>
                        <div>
                          <label>School Year</label>
                        </div>
                        <div>
                          <input type = 'text' id = 'schoolYearCounselingTextModal' class='form-control' disabled>
                        </div>
                        <div>
                          <label>Semester</label>
                        </div>
                        <div>
                          <input type = 'text' id = 'semesterCounselingTextModal' class='form-control' disabled>
                        </div>
                        <div>
                          <label>Counseling Type</label>
                        </div>
                        <div>
                          <input type = 'text' id = 'councelingTypeTextModal' class='form-control' disabled>
                        </div>
                      </div>
                      <div class='col-md-4 well'>
                        <div>
                          <label>Statement Of The Problem</label>
                        </div>
                        <div>
                          <textarea class='form-control' id = 'reasonCounselingTextModal' rows="7" disabled></textarea>
                        </div>
                        <div>
                          <label>Assisted By:</label>
                        </div>
                        <div>
                          <input type = 'text' id = 'assistedByTextModal' class = 'form-control' disabled>
                        </div>
                      </div>
                      <div class='col-md-4 well'>
                        <div class='row'>

                            <div>
                              <label>Evaluation Date:</label>
                            </div>
                            <div>
                              <input type ='date' class='form-control' id = 'evaluationDateCounselingModal' disabled>
                            </div>
                          </div>
                          <div class='row'>

                              <div>
                                <label>Recommendation</label>
                              </div>
                              <div>
                                <textarea class='form-control' id = 'recommendationCounselingTextModal' rows="3" disabled>
                                </textarea>
                              </div>

                          </div>
                        <div class='row'>
                          <div>
                            <label>Evaluated By:</label>
                          </div>
                          <div>
                            <input type = 'text' id = 'evaluatedByTextModal' class = 'form-control' disabled>
                          </div>
                        </div>
                      </div>
                      {{--  <div class='col-md-3 well'>
                        <div>
                          <label>Follow Up:</label>
                        </div>
                        <div>
                          <textarea class='form-control' id = 'followUpTextModal' rows="7">
                          </textarea>
                        </div>
                        <div>
                          <label>Follow Up By:</label>
                        </div>
                        <div>
                          <input type = 'text' id = 'followupByTextModal' class = 'form-control'>
                        </div>
                      </div>  --}}
                    </div>
                  </div>
                </div>
                <div class='tab-pane fade' id ='referralListModal'>
                  <div class='container-fluid'>
                    <div class='row'>
                      {{--  <div class='col-md-8'>
                        <div class="form-group has-feedback" style = 'padding-top:0px;'>
                            <input type="text" class="form-control" id = 'seachReferralTextModal' placeholder="Search by Date Referral Or School Year"/>
                            <i class="fa fa-search form-control-feedback"></i>
                        </div>
                      </div>  --}}
                      <div class='col-md-4'>
                        <button class='btn btn-warning btn-block' id ='printReferralOfStudent'><i class='fa fa-print'></i>&nbsp; Print Referral</button>
                      </div>
                    </div>
                    <div class='row'>
                      <div class='col-md-12'>
                        <table class='table table-condensed'>
                          <thead>
                            <tr>
                              <th>Date Referral</th>
                              
                              <th>Referred By</th>
                              <th>School Year</th>
                              <th>Semester</th>
                            </tr>
                          </thead>
                          <tbody id ='tableReferralOfStudent'>

                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div class='row'>
                      {{--  <div class='col-md-3'>
                        <div>
                          <label>
                            Date Referal
                          </label>
                          <input type = 'text' id = 'dateReferralTextModal' class='form-control'>
                        </div>
                        <div>
                          <label>
                            School Year
                          </label>
                          <input type = 'text' id = 'schoolYearReferralTextModal' class='form-control'>
                        </div>
                        <div>
                          <label>
                            School Year
                          </label>
                          <input type = 'text' id = 'semesterReferralTextModal' class='form-control'>
                        </div>
                      </div>  --}}
                      <div class='col-md-12'>
                        <table class='table table-condensed'>
                          <thead>
                            <tr>
                              <th>Observable Behaviours</th>
                              <th>Frequency</th>
                              <th>Remarks</th>
                            </tr>
                          </thead>
                          <tbody id ='behaviourOfReferralStudent'>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div class='row'>
                      <div class='col-md-3'>
                      </div>
                      <div class='col-md-9'>
                        <div class='row'>
                          <div class='col-md-2'>
                            <label>Referred To:</label>
                          </div>
                          <div class='col-md-4'>
                            <input type = 'text' id = 'referredToTextModal' class='form-control' disabled>
                          </div>
                          <div class='col-md-2'>
                            <label>Referred By:</label>
                          </div>
                          <div class='col-md-4'>
                            <input type = 'text' id = 'referredByTextModal' class='form-control' disabled>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class='tab-pane fade' id ='contractListModal'>
                  <div class='container-fluid'>
                    <div class='row'>
                      {{--  <div class='col-md-8'>
                        <div class="form-group has-feedback" style = 'padding-top:0px;'>
                            <input type="text" class="form-control" id = 'searchContractTextModal' placeholder="Search by Contract Date, Type Of Contract Or School Year"/>
                            <i class="fa fa-search form-control-feedback"></i>
                        </div>
                      </div>  --}}
                      <div class='col-md-4'>
                        <button class='btn btn-warning btn-block' id ='printContracStudent'><i class='fa fa-print'></i>&nbsp; Print Contract</button>
                      </div>
                    </div>
                    <div class='row'>
                      <div class='col-md-12'>
                        <table class='table table-condensed'>
                          <thead>
                            <tr>
                              <th>Contract Date</th>
                              <th>Contract Type</th>
                              <th>Remarks</th>
                              <th>Noted by</th>
                            </tr>
                          </thead>
                          <tbody id ='contractTableForStudents'>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div class='row'>
                      <div class='col-md-8 well'>
                        <div class='row'>
                          <div class='col-md-4'>
                            <div class='row'>
                              <label>Contract Date</label>
                              <input type = 'text' id ='contractDateTextModal' class='form-control' disabled>
                            </div>
                            {{--  <div class='row'>
                              <label>School Year</label>
                              <input type = 'text' id ='schoolYearContractTextModal' class='form-control'>
                            </div>
                            <div class='row'>
                              <label>Semester</label>
                              <input type = 'text' id ='semesterContractTextModal' class='form-control'>
                            </div>  --}}
                            <div class='row'>
                              <label>Type Of Contract</label>
                              <input type = 'text' id ='contractTypeTextModal' class='form-control' disabled>
                            </div>
                            <div class='row'>
                              <label>Remarks</label>
                              <textarea id ='remarksContractTextModal' class='form-control' rows='5' disabled>
                              </textarea>
                            </div>
                          </div>
                          <div class='col-md-8'>
                            <div style = 'height:185px; overflow-y:scroll;'>
                              <table class='table table-condensed' >
                                <thead>
                                  <tr>
                                    <th>Subject</th>
                                    <th>Grade</th>
                                    <th>Unit</th>
                                  </tr>
                                </thead>
                                <tbody id ='subjectGradesContract'>
                                </tbody>
                              </table>
                            </div>
                            <div>
                              <label>Average Grade</label>
                              <input type ='text' readonly class='form-control' id ='averageGrade'>
                            </div>
                            {{--  <label>Average</label>
                            <input type = 'text' id ='averageContractTextModal' class='form-control'>  --}}
                          </div>
                         
                        </div>
                      </div>
                      <div class='col-md-4'>
                        <div style = 'height:175px; overflow-y:scroll;' id ='proofsContract'>
                          
                        </div>
                        <label>Noted By:</label>
                        <input type = 'text' id ='notedByContractTextModal' class='form-control' disabled>
                      </div>
                    </div>

                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        {{--  <button type="button" class="btn btn-danger" data-dismiss="modal"><i class='fa fa-print'></i>&nbsp;Print</button>  --}}
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
