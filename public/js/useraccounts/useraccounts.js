$(document).ready(function(){
  tableLoader('8', 'usersAccountsTable');
  getAjax('/useraccounts/list', 'usersAccountsTable');
  var action='';
  var userid;
  $("#addUserButton").on('click', function(){
    $('#addUserAccountModal').modal('show');
    action ='add';
    $('#passwordUserAccount').attr('disabled', false);
    $('#editPasswordButton').attr('disabled', true);
    $('#imageUserAccount').attr('required', '');
  }); 
  $('#designationUserAccount').on('change', function(){
    if($('#designationUserAccount').val() == 'college'){
      $('#designationLevel').attr('disabled', true);
      $('#collegesDesignation').attr('disabled', false);
    }else{ 
      $('#collegesDesignation').attr('disabled', true);
      $('#designationLevel').attr('disabled', false);
      var html = '';
      switch ($('#designationUserAccount').val()) {
        case 'seniorhigh':
           html ="<option value = 'grade-11'>grade 11</option>"+
                    "<option value = 'grade-12'>grade 12</option>";
        break;
        case 'juniorhigh':
           html ="<option value = 'grade-10'>grade 10</option>"+
                    "<option value = 'grade-9'>grade 9</option>"+
                    "<option value = 'grade-8'>grade 8</option>"+
                    "<option value = 'grade-7'>grade 7</option>";
        break;
        case 'elementary':
           html ="<option value = 'grade-6'>grade 6</option>"+
                    "<option value = 'grade-5'>grade 5</option>"+
                    "<option value = 'grade-4'>grade 4</option>"+
                    "<option value = 'grade-3'>grade 3</option>"+
                    "<option value = 'grade-2'>grade 2</option>"+
                    "<option value = 'grade-1'>grade 1</option>";
        break;
        default:
          break;
      }
      $('#designationLevel').html(html);
    }
  });
  $('#removeImageButton').on('click', function(){
    $('#imageUserAccount').val('');
    $('#viewProfileImage').attr('src', '/images/defaultimage.png');
  });
  $('#addEditUserForm').on('submit', function(e){
    e.preventDefault();
    var form = document.getElementById('addEditUserForm');
    var myform = new FormData(form);
    myform.append('action', action);
    if (action!='add'){
      myform.append('id', userid);
      var tempCounter = [];
      var counter = 0;
      $('#useraccount-'+userid).children('td').each(function(){
        tempCounter[counter] = $(this).html();
        counter++;
      });
      myform.append('rowCounter', tempCounter[0]);
      if($('#viewByActive').val()=='active'){
        myform.append('active', 'status');
      }else{
        myform.append('inactive', 'status');
      }
       prependPostAjaxErrorCallback('/useraccount/insert', myform, function(data){
        if(data!='errorCoordinator'){
          new PNotify({
            title: 'Success!',
            text: 'User successfully saved!',
            type: 'success',
            styling: 'bootstrap3'
          });
          $('#useraccount-'+userid).html(data);
          clearInputs();
      }else{
        new PNotify({
          title: 'Error!',
          text: 'A coordinator is already assigned in this designation!',
          type: 'error',
          styling: 'bootstrap3'
        });
        $('#passwordUserAccount').val('');
      }
      });
    }else{
      var totalrowcounter = 0;
      $('#usersAccountsTable').children('tr').each(function(){
        totalrowcounter++;
      });
      totalrowcounter++;
      myform.append('rowCounter', totalrowcounter);
      prependPostAjaxErrorCallback('/useraccount/insert', myform, function(data){
        //console.log(data);
        if(data!='errorCoordinator'){
            new PNotify({
              title: 'Success!',
              text: 'User successfully saved!',
              type: 'success',
              styling: 'bootstrap3'
            });
            if($('#viewByActive').val()=='active'){
              $('#usersAccountsTable').children("h2").each(function(){
                $(this).remove();
              });
              $('#usersAccountsTable').append(data);
            }
            clearInputs();
        }else{
          new PNotify({
            title: 'Error!',
            text: 'A coordinator is already assigned in this designation!',
            type: 'error',
            styling: 'bootstrap3'
          });
          $('#passwordUserAccount').val('');
        }
        
      });
    }
  }); 
  $('#usersAccountsTable').on('click', '.editUseraccount', function(){
    //alert("Hello");
    $('#passwordUserAccount').attr('disabled', true);
    $('#editPasswordButton').attr('disabled', false);
    $('#imageUserAccount').removeAttr('required')
    var id = $(this).prop('id');
    userid = id.split('-')[1];
    //alert(userid);
    action = 'edit';
    $.get('/useraccount/get/'+userid, function(data){
        //console.log(data);
        $('#usernameUserAccount').val(data.userName);
        $('#firstNameUserAccount').val(data.firstName);
        $('#lastNameUserAccount').val(data.lastName);
        $('#emailUserAccount').val(data.email);
        $('#viewProfileImage').attr('src', data.image);
        $('#usertype').val(data.userType);
        if(data.designation2=='college'){
          $('#designationLevel').attr('disabled', true);
          $('#collegesDesignation').attr('disabled', false);
          $('#designationUserAccount').val('college');
          //changeCombo();
          $('#collegesDesignation').val(data.designation);
          
        }else{
          $('#collegesDesignation').attr('disabled', true);
          $('#designationLevel').attr('disabled', false);
          $('#designationUserAccount').val(data.designation);
          changeCombo();
          $('#designationLevel').val(data.designationLevel);
          
        }

      $('#addUserAccountModal').modal('show');
    });
  });
  $('#editPasswordButton').on('click', function(){
    if($('#passwordUserAccount').prop('disabled')){
      $('#passwordUserAccount').attr('disabled', false);
    }else{
      $('#passwordUserAccount').attr('disabled', true);
    }
  });
  $('#viewByActive').on('change', function(){ 
    if($(this).val()=='active'){
      $('#activeHeader').text('Set to inactive')
      tableLoader('8', 'usersAccountsTable')
      getAjax('/useraccounts/list', 'usersAccountsTable');
    }else{
      $('#activeHeader').text('Set to active');
      tableLoader('8', 'usersAccountsTable')
      getAjax('/useraccounts/list/inactive', 'usersAccountsTable');
    }
  });
  
  $('#usersAccountsTable').on('click', '.setToInactive',function(){
    $("#deleteModal").modal('show');
    $('#deleteButton').text("Confirm");
    globalUserId = $(this).prop('id').split('-')[1];
    $("#deleteMessage").html("<h3>Are you sure you want to set this user to inactive?</h3>");
    mydelete ="setInactiveUser";
  });
  $('#usersAccountsTable').on('click', '.setToActive',function(){
    $("#deleteModal").modal('show');
    $('#deleteButton').text("Confirm");
    globalUserId = $(this).prop('id').split('-')[1];
    $("#deleteMessage").html("<h3>Are you sure you want to set this user to active?</h3>");
    mydelete ="setActiveUser";
  });
  $('#searchUserText').on('keydown', function(){
    
    tableLoader('8', 'usersAccountsTable');
    getAjax('/useraccount/search/'+$('#viewByActive').val()+'/'+$(this).val(), 'usersAccountsTable');

  });
});

document.getElementById('imageUserAccount').onchange = function(e) {
  // Get the first file in the FileList object
  var imageFile = this.files[0];
  // get a local URL representation of the image blob
  var url = window.URL.createObjectURL(imageFile);
  // Now use your newly created URL!
  var someImageTag = document.getElementById('viewProfileImage');
  someImageTag.src = url;
}

function changeCombo(){
  var html="";
  switch ($('#designationUserAccount').val()) {
    case 'seniorhigh':
       html ="<option value = 'grade-11'>grade 11</option>"+
                "<option value = 'grade-12'>grade 12</option>";
    break;
    case 'juniorhigh':
       html ="<option value = 'grade-10'>grade 10</option>"+
                "<option value = 'grade-9'>grade 9</option>"+
                "<option value = 'grade-8'>grade 8</option>"+
                "<option value = 'grade-7'>grade 7</option>";
    break;
    case 'elementary':
       html ="<option value = 'grade-6'>grade 6</option>"+
                "<option value = 'grade-5'>grade 5</option>"+
                "<option value = 'grade-4'>grade 4</option>"+
                "<option value = 'grade-3'>grade 3</option>"+
                "<option value = 'grade-2'>grade 2</option>"+
                "<option value = 'grade-1'>grade 1</option>";
    case 'college':
    break;
    break;
    default:
      break;
  }
  $('#designationLevel').html(html);
}
function clearInputs(){
  $('#lastNameUserAccount').val('');
  $('#firstNameUserAccount').val('');
  $('#emailUserAccount').val('');
  $('#usernameUserAccount').val('');
  $('#imageUserAccount').val('');
  $('#passwordUserAccount').val('');
}