<div id="groupCounselingModal" class="modal fade" role="dialog" >
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content" style = 'z-index:200;'>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Group Counseling</h4> 
      </div>
      <div class="modal-body">
        <div class='container-fluid'>
          {!! Form::open([ 
            'id'=>'counselingAddGroupForm']) !!}
          <div class='row'>
            <div class='col-md-6'>
             
              <div class='genericDivTop'>
                <label for = 'dateScheduledGroupCounseling'>Date Scheduled</label>
                <input type = 'date' id = 'dateScheduledGroupCounseling' name = 'dateScheduledGroupCounseling'
              class='form-control' required readonly  value ='{{$systemdate}}'>
              </div>
              
              <div class='genericDivTop'>
                <label for = 'counselingDateGroupCounseling'>Counseling Date</label>
                <input type = 'date' id = 'counselingDateGroupCounseling' name = 'counselingDateGroupCounseling'
                class='form-control'  value ='{{$systemdate}}'>
              </div>
              <div class='genericDivTop'>
                <label for = 'walkinGroupCounceling'><input type=  'checkbox' id = 'walkinGroupCounceling' value = 'walkin' name ='walkinGroupCounceling'>Walk-in</label>
              </div>
            </div>
            <div class='col-md-6'>
              <label>
                Person Involved
              </label>
              <div class='row'>
                <div class='col-md-6'>
                    <label for ='studentTypeFilterGroup'>Student Type</label>
                    
                    
                        @if(session()->get('user')['userType']=='admin')
                        <select id ='studentTypeFilterGroup' class='form-control'>
                            <option value ='all'>all</option>
                            <option value ='college'>College</option>
                            <option value ='seniorhigh'>Senior High</option>
                            <option value ='juniorhigh'>Junior High</option>
                            <option value ='elementary'>Elementary</option>
                        @else
                            @switch($user[0])
                                @case('college')
                                <select id ='studentTypeFilterGroup' class='form-control' readonly>
                                    <option value ='college'>College</option>
                                @break
                                @case('seniorhigh')
                                <select id ='studentTypeFilterGroup' class='form-control' readonly>
                                    <option value ='seniorhigh'>Senior High</option>
                                @break
                                @case('juniorhigh')
                                <select id ='studentTypeFilterGroup' class='form-control' readonly>
                                    <option value ='juniorhigh'>Junior High</option>
                                @break
                                @case('elementary')
                                <select id ='studentTypeFilterGroup' class='form-control' readonly>
                                    <option value ='elementary'>Elementary</option>
                                @break
                                @default
                                    Default case...
                            @endswitch
                        @endif
                    </select>
                    
                </div>
                <div class='col-md-6'>
                    <label for ='schoolYearFilterGroup'>School Year</label>
                    <select id ='schoolYearFilterGroup' class='form-control'>
                        <option value ='all'>all</option>
                       
                        @foreach($schoolyears as $year)
                          <option value ='{{$year->id}}'>{{explode('-', $year->schoolYearStart)[0]}} - {{explode('-', $year->schoolYearEnd)[0]}}</option>
                        @endforeach
                    </select>
                </div>
                <div class='col-md-6'>
                    <label for ='semesterFilterGroup'>Semester</label>
                    <select id ='semesterFilterGroup' class='form-control'>
                        <option value ='all'>all</option>
                    </select>
                </div>
                <div class='col-md-6'>
                    <label for ='courseGradeLevelFilterGroup'>Course/Grade Level</label>
                    @if(session()->get('user')['userType']=='admin')
                                <select id ='courseGradeLevelFilterGroup' class='form-control'>
                                        <option value ='all'>all</option>
                    @else
                        @switch($user[0])
                            @case('college')
                            <select id ='courseGradeLevelFilterGroup' class='form-control'>
                                <option value ='all'>all</option>
                                
                                @foreach($collegeOfUser as $college)
                                    <option value = "{{$college->id}}">{{$college->courseName}}</option>
                                @endforeach
                            @break
                            @case('seniorhigh')
                            <select id ='courseGradeLevelFilterGroup' class='form-control' readonly>
                                <option value ="{{$user[1]}}">{{$user[1]}}</option>
                            @break
                            @case('juniorhigh')
                            <select id ='courseGradeLevelFilterGroup' class='form-control' readonly>
                                <option value ="{{$user[1]}}">{{$user[1]}}</option>
                            @break
                            @case('elementary')
                            <select id ='courseGradeLevelFilterGroup' class='form-control' readonly>
                                <option value ="{{$user[1]}}">{{$user[1]}}</option>
                            @break
                            @default
                                Default case...
                        @endswitch
            
                    @endif
                    </select>
                </div>
                
              </div>
              
            </div>
          </div>
          <div class='row'>
            <div class='col-md-12'>
              <div class='genericDivPadTop'>
                  <div class="form-group has-feedback">
                      <input type="text" class="form-control"  placeholder="Search for name" id ='searchStudentsGroupCounseling'/>
                      <i class="fa fa-search form-control-feedback"></i>
                  </div> 
              </div>
              <div class='tableModalContainer'>
                <table class='table table-striped'>
                  <thead>
                    <tr>
                      <th>Check</th>
                      <th>Name</th>
                      <th>Student No</th>
                      <th>Student Type</th>
                    </tr>
                  </thead>
                  <tbody id ='studentTableForGroupCounseling'>
                      
                      
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class='row'>
            <div class='col-md-12'>
              <label for = 'statementOfTheProblemGroupCounselingTextArea'>Statement of the problem</label>
              <textarea id = 'statementOfTheProblemGroupCounselingTextArea' name = 'statementOfTheProblemGroupCounselingTextArea'
              class='form-control' rows = '5'></textarea>
            </div>
            <div class='col-md-6'> 
              <label for = 'assistedByGroupCounselingText'>Assisted By:</label>
              <input type = 'text' id = 'assistedByGroupCounselingText' name = 'assistedByGroupCounselingText'
              class='form-control' required value ='{{$nameofuser}}' readonly>
            </div>
            <div class='col-md-6'>
              <label for = 'counseledByTextGroup'>Counseled By:</label>
              {{--  <input type = 'text' id = 'counseledByGroupText' name = 'counseledBy'
              class='form-control' required>  --}}
              <div class="input-group">
                  <input type="text" id = 'nameOfCounselorGroup' name='nameOfCounselor' readonly class="form-control">
                  <input type='hidden' name = 'counseledBy' id ='counseledByTextGroup'>
                  <span class="input-group-btn">
                    <button type="button" class="btn btn-primary" id ='chooseCounselorGroup'><i class='fa fa-plus'></i></button>
                  </span>
                </div>
            </div>
          </div>
          {!! Form::close() !!}
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-warning" form ='counselingAddGroupForm'><i class='fa fa-save'></i>&nbsp;Save</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>

  </div>
</div>
