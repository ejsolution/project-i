@component('components.modal')
  @slot('modalid')
    deleteModal
  @endslot
  @slot('modalsize')
    modal-sm
  @endslot
  @slot('modaltitle')
    Delete
  @endslot
  @slot('modalcontent')
    <div id='deleteMessage'>
    </div>
  @endslot
  @slot('modalbutton')
      <button type="button" class="btn btn-danger" data-dismiss="modal" id = 'deleteButton'><i class='fa fa-trash-o'></i>&nbsp;Delete</button>
  @endslot
@endcomponent
