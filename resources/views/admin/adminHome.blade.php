@extends('admin.adminTemplate')

@section('title', 'AdminDashboard')
@section('stylesheet')
	<link href="{{asset('css/admin/dashboard/adminHome.css')}}" rel="stylesheet">
	<link href="{{asset('css/admin/dashboard/dashboard.css')}}" rel="stylesheet">
@stop
@section('content')
	<div class='container-fluid dashboard'>
		<div class='row'>
			<h1>Home</h1>
		</div>
		<label>Total Students this School Year</label>
		<div class='row'>
			
			<div class='col-md-3'>
				<div id = 'collegeCard' class='genericCard'>
					<div class='cardTitle' id = 'cardTitleCollege'>
						Total College Students
					</div>
					<div class='cardBody' id = 'cardBodyCollege'>
						<center>
							<h1 style = 'color:white;'>{{$totalCollege}}</h1>
						</center>
					</div>
					<div class='cardFooter' id = 'cardFooterCollege'>
						<a href="{{route('collegeStudents')}}">More Info &nbsp;<i class="fa fa-arrow-right"></i><a>
					</div>
				</div>
			</div>
			<div class='col-md-3'>
				<div class='genericCard'>
					<div class='cardTitle' id = 'cardTitleSenior'>
						Total Senior High Students
					</div>
					<div class='cardBody' id = 'cardBodySenior'>
						<center>
							<h1 style = 'color:white;'>{{$totalSeniorHigh}}</h1>
						</center>
					</div>
					<div class='cardFooter' id = 'cardFooterSenior'>
						<a href="{{route('seniorHighStudents')}}">More Info &nbsp;<i class="fa fa-arrow-right"></i></a>
					</div>
				</div>
			</div>
			<div class='col-md-3'>
				<div class='genericCard'>
					<div class='cardTitle' id = 'cardTitleJunior'>
						Total Junior High Students
					</div>
					<div class='cardBody' id = 'cardBodyJunior'>
						<center>
							<h1 style = 'color:white;'>{{$totalJuniorHigh}}</h1>
						</center>
					</div>
					<div class='cardFooter' id = 'cardFooterJunior'>
						<a href="{{route('juniorHighStudents')}}">More Info &nbsp;<i class="fa fa-arrow-right"></i></a>
					</div>
				</div>
			</div>
			<div class='col-md-3'>
				<div class='genericCard'>
					<div class='cardTitle' id = 'cardTitleElementary'>
						Total Elementary Students
					</div>
					<div class='cardBody' id = 'cardBodyElementary'>
						<center>
							<h1 style = 'color:white;'>{{$totalElementary}}</h1>
						</center>
					</div>
					<div class='cardFooter' id = 'cardFooterElementary'>
						<a href="{{route('elementary')}}">More Info &nbsp;<i class="fa fa-arrow-right"></i></a>
					</div>
				</div>
			</div>
		</div>
		<div class='row genericDivTop'>
			<div class='col-md-7'>
				<div class='graphCard'>
					<div class='graphTitle'>
						Counseling This SchoolYear
					</div>
					<div class='counselingGraphBody overFlowY overFlowX'>
						<canvas id="counselingBarChart" height="500" width="600" ></canvas>
					</div>
				</div> 
			</div>
			<div class='col-md-5'>
				<div class='graphCard'>
					<div class='graphTitle'>
						Top 5 Contract Courses This SchoolYear
					</div>
					<div class='counselingGraphBody overFlowY'>
						<div class='row'>
							<div class='col-md-5'>
								<canvas class="contractDoughnut" height="140" width="140" style="margin: 15px 10px 10px 0"></canvas>
							</div>
							<div class='col-md-7' id ='contractContent'>
								<table class='table table-striped'>
									<thead>
										<tr>
											<th>College Name</th> 
											<th>Total Contract</th>
										</tr>
									</thead>
									<tbody id = 'counselingTopTable'> 
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class='row genericDivTop'>
			<div class='col-md-8'>
				<div class='graphCard'>
					<div class='graphTitle'>
						Referral This SchoolYear
					</div>
					<div class='counselingGraphBody overFlowY overFlowX'>
						<canvas id="referralGraphBody" height="500" width="600" ></canvas>
					</div>
				</div>
			</div>
			<div class='col-md-4'>
				<div>
					<div class='eventsTitle'>
						<i class="fa fa-bell"></i>Upcoming Events
					</div>
					<div class='eventsBody'>
					</div>
					{{--  <div class='eventsFooter'>
						<a href="#">Edit</a>
					</div>  --}}
				</div>
			</div>
		</div>
	</div> 

@endSection
@section('scripts')

  <script src = '{{asset('js/dashboard/dashboard.js')}}'></script>
@stop