<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Putquestionsonleisure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('student_leisure_s_hs', function (Blueprint $table) {
            $table->mediumText('howDidYouComeToThisSchool')->nullable();
            $table->mediumText('whereDidYouGetThisInfo')->nullable();
            $table->mediumText('scholarship')->nullable();
        });
        Schema::table('student_leisure_j_hs', function (Blueprint $table) {
            $table->mediumText('howDidYouComeToThisSchool')->nullable();
            $table->mediumText('whereDidYouGetThisInfo')->nullable();
            $table->mediumText('scholarship')->nullable();
        });
        Schema::table('student_leisure_elems', function (Blueprint $table) {
            $table->mediumText('howDidYouComeToThisSchool')->nullable();
            $table->mediumText('whereDidYouGetThisInfo')->nullable();
            $table->mediumText('scholarship')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
