var totalRow;
var totalPages;
var visiblePages;
var mypage;
var studentGroups=[];
$(document).ready(function(){ 
    var counselingid;
    var followupid;
    var mycounselingdate;
    var counselingAction;
  $("#individualCounselingButton").on('click', function(){
    $("#individualCounselingModal").modal("show");
  }); 
  $("#courseTH").text("Course/Strand");
  $("#yearLevelTH").text("Year Level/Grade Level");
  if($('#resched').val()=="true"){
    $('#rescheduleButton').attr('disabled', false);
    counselingid = $('#counseling').val();
    var studentid = $('#studentCounseling').val();
    var dateScheduled;
    $('#secondEdit').attr('disabled', true);
    $('#thirdEdit').attr('disabled', true);
    $('#studentNameTextEdit').val(studentid);
    $.get('/counseling/studentdata/'+$('#studentNameTextEdit').val(), function(data){
        //console.log(data);
        if(data.studentType == 'college'){
          $('#courseNameTextEdit').val(data.course);
        }
        $('#yearLevelGradeLevelTextEdit').val(data.gradeLevel);
        $('#homeAddressTextEdit').val(data.cityAddress);      
        $('#studentContactNoTextEdit').val(data.contactNo);
        var yearToday =  (new Date()).getFullYear();
        var yearBirth  = data.dateOfBirth.split('-')[0];
        //console.log(yearToday+' '+yearBirth);
        var age = yearToday - yearBirth;
        $('#studentAgeTextEdit').val(age);
        $('#studentTypeTextEdit').val(data.studentType);
        $('#studentSexTextEdit').val(data.gender);    
    });
    new Promise((resolve, error)=>{
        $.get('/counseling/data/'+counselingid, function(data){
            $('#assistedByTextEdit').val(data.assistedBy);
            $('#statementOfTheProblemTextAreaEdit').val(data.statementOfTheProblem);
            if(data.walkin ==1){
                $('#walkinEdit').attr('checked', true);
            }
            $('#dateScheduledTextEdit').val(data.dateRecorded);
            $('#counselingDateEdit').val(data.dateScheduled);
            dateScheduled = data.dateScheduled; 
            resolve(true);
        });
    }).then((res)=>{
        $('#saveEditCounselingForm').attr('disabled', true);
        $.get('/counseling/checkiffolloweduporevaluated/'+counselingid, function(data){
            tableLoader('4', 'followUpTableIndividual');
            $.get('/counseling/followupdata/'+counselingid, function(mydata){
                $('#followUpTableIndividual').html(mydata);
                if(data =='evaluated'){
                    $('#saveFollowUpFormIndividual').attr('disabled', true);
                    $('.editFollowUpIndividual').attr('disabled', true);
                }else{
                    $('#saveFollowUpFormIndividual').attr('disabled', false);
                    $('.editFollowUpIndividual').attr('disabled', false);
                }
            });
            //console.log(data);
            if(data=='followedup' || data=='evaluated'){
                $('#rescheduleButton').attr('disabled', true);
                $('#saveEditCounselingForm').attr('disabled', true);
                $('#studentNameTextEdit').attr('disabled', true);
                if(data =='followedup'){
                    $('#secondEdit').attr('disabled', false);
                    $('#thirdEdit').attr('disabled', false);

                    $('#saveFollowUpFormIndividual').attr('disabled', false);
                    $("#firstEdit").removeClass('btn-info').addClass('btn-default');
                    $("#thirdEdit").removeClass('btn-info').addClass('btn-default');
                    $("#secondEdit").addClass('btn-info').removeClass('btn-default');
                    $('[href="#followUpTabEdit"]').tab('show');
                    
                    $('#editEvaluationForm').attr('disabled', true);
                    $('#evaluationTextArea').attr('disabled', false);
                    $('#evaluatedByText').attr('disabled', true);
                    $('#saveEvaluationForm').attr('disabled', false);
                }else{
                    $('#secondEdit').attr('disabled', false);
                    $('#thirdEdit').attr('disabled', false);

                    $('#evaluationTextArea').attr('disabled', true);
                    $('#evaluatedByText').attr('disabled', true);
                    $('#saveEvaluationForm').attr('disabled', true);
                    $('#editEvaluationForm').attr('disabled', false);
                    $('#saveFollowUpFormIndividual').attr('disabled', true);
                    $("#secondEdit").removeClass('btn-info').addClass('btn-default');
                    $("#firstEdit").removeClass('btn-info').addClass('btn-default');
                    $("#thirdEdit").addClass('btn-info').removeClass('btn-default');
                    $('[href="#evaluationTabEdit"]').tab('show');
                    $.get('/counseling/evaluation/data/'+counselingid, function(data){
                        if(data!="0"){
                            $('#evaluationTextArea').val(data.remarks);
                            $('#evaluatedByText').val(data.evaluatedBy);
                        }
                        
                    });
                }
            }else{
                if(dateScheduled==$('#systemDate').val()){
                    $('#secondEdit').attr('disabled', false);
                }else{
                    
                    $('#secondEdit').attr('disabled', true);
                }
                $("#secondEdit").removeClass('btn-info').addClass('btn-default');
                $("#thirdEdit").removeClass('btn-info').addClass('btn-default');
                $("#firstEdit").addClass('btn-info').removeClass('btn-default');
                $('[href="#individualCounselingTabEdit"]').tab('show');
                
                $('#studentNameTextEdit').attr('disabled', true);
                $('#saveEditCounselingForm').attr('disabled', false);

                $('#editEvaluationForm').attr('disabled', true);
                $('#evaluationTextArea').attr('disabled', false);
                $('#evaluatedByText').attr('disabled', true);
                $('#evaluationTextArea').val('');
                //$('#evaluatedByText').val('');
                $('#saveEvaluationForm').attr('disabled', false);
            }
        });
    });
     
    var temp = [];
    var i =0;
    $('#counselingRow-'+counselingid).children('td').each(function(){
        temp[i] = $(this).html();
        i++;
    });
    if($('#counselingTypeOf').val()=='individual'){
        $('#editIndividualCounselingModal').modal('show');
        $('#individualContainer').show();
        $('#groupContainer').hide();
    }else{
        $('#editIndividualCounselingModal').modal('show');
        $('#individualContainer').hide();
        $('#groupContainer').show();
        tableLoader('6', 'studentTableForGroupCounselingEdit');
        $.get('/counseling/group/data/'+counselingid, function(data){
            $('#studentTableForGroupCounselingEdit').html(data.html);
            /*walkinGroupCouncelingEdit
            dateScheduledGroupCounselingEdit
            counselingDateGroupCounselingEdit*/
            $('#dateScheduledGroupCounselingEdit').val(data.dateScheduled);
            $('#counselingDateGroupCounselingEdit').val(data.dateRecorded);
            if(data.walkin == 1){
                $('#walkinGroupCouncelingEdit').attr('checked', true);
            }else{
                $('#walkinGroupCouncelingEdit').attr('checked', false);
            }
            console.log(data);
        });
    }
  }
  if($('#accept').val()=="true"){
        $('#rescheduleButton').attr('disabled', false);
        counselingid = $('#counseling').val();
        var studentid = $('#studentCounseling').val();
        var dateScheduled;
        $('#secondEdit').attr('disabled', true);
        $('#thirdEdit').attr('disabled', true);
        $('#studentNameTextEdit').val(studentid);
        $.get('/counseling/studentdata/'+$('#studentNameTextEdit').val(), function(data){
            //console.log(data);
            if(data.studentType == 'college'){
                $('#courseNameTextEdit').val(data.course);
            }
            $('#yearLevelGradeLevelTextEdit').val(data.gradeLevel);
            $('#homeAddressTextEdit').val(data.cityAddress);      
            $('#studentContactNoTextEdit').val(data.contactNo);
            var yearToday =  (new Date()).getFullYear();
            var yearBirth  = data.dateOfBirth.split('-')[0];
            //console.log(yearToday+' '+yearBirth);
            var age = yearToday - yearBirth;
            $('#studentAgeTextEdit').val(age);
            $('#studentTypeTextEdit').val(data.studentType);
            $('#studentSexTextEdit').val(data.gender);    
        });
        new Promise((resolve, error)=>{
            $.get('/counseling/data/'+counselingid, function(data){
                $('#assistedByTextEdit').val(data.assistedBy);
                $('#statementOfTheProblemTextAreaEdit').val(data.statementOfTheProblem);
                if(data.walkin ==1){
                    $('#walkinEdit').attr('checked', true);
                }
                $('#dateScheduledTextEdit').val(data.dateRecorded);
                $('#counselingDateEdit').val(data.dateScheduled);
                dateScheduled = data.dateScheduled; 
                resolve(true);
            });
        }).then((res)=>{
            $('#saveEditCounselingForm').attr('disabled', true);
            $.get('/counseling/checkiffolloweduporevaluated/'+counselingid, function(data){
                tableLoader('4', 'followUpTableIndividual');
                $.get('/counseling/followupdata/'+counselingid, function(mydata){
                    $('#followUpTableIndividual').html(mydata);
                    if(data =='evaluated'){
                        $('#saveFollowUpFormIndividual').attr('disabled', true);
                        $('.editFollowUpIndividual').attr('disabled', true);
                    }else{
                        $('#saveFollowUpFormIndividual').attr('disabled', false);
                        $('.editFollowUpIndividual').attr('disabled', false);
                    }
                });
                //console.log(data);
                if(data=='followedup' || data=='evaluated'){
                    $('#rescheduleButton').attr('disabled', true);
                    $('#saveEditCounselingForm').attr('disabled', true);
                    $('#studentNameTextEdit').attr('disabled', true);
                    if(data =='followedup'){
                        $('#secondEdit').attr('disabled', false);
                        $('#thirdEdit').attr('disabled', false);
    
                        $('#saveFollowUpFormIndividual').attr('disabled', false);
                        $("#firstEdit").removeClass('btn-info').addClass('btn-default');
                        $("#thirdEdit").removeClass('btn-info').addClass('btn-default');
                        $("#secondEdit").addClass('btn-info').removeClass('btn-default');
                        $('[href="#followUpTabEdit"]').tab('show');
                        
                        $('#editEvaluationForm').attr('disabled', true);
                        $('#evaluationTextArea').attr('disabled', false);
                        $('#evaluatedByText').attr('disabled', true);
                        $('#saveEvaluationForm').attr('disabled', false);
                    }else{
                        $('#secondEdit').attr('disabled', false);
                        $('#thirdEdit').attr('disabled', false);
     
                        $('#evaluationTextArea').attr('disabled', true);
                        $('#evaluatedByText').attr('disabled', true);
                        $('#saveEvaluationForm').attr('disabled', true);
                        $('#editEvaluationForm').attr('disabled', false);
                        $('#saveFollowUpFormIndividual').attr('disabled', true);
                        $("#secondEdit").removeClass('btn-info').addClass('btn-default');
                        $("#firstEdit").removeClass('btn-info').addClass('btn-default');
                        $("#thirdEdit").addClass('btn-info').removeClass('btn-default');
                        $('[href="#evaluationTabEdit"]').tab('show');
                        $.get('/counseling/evaluation/data/'+counselingid, function(data){
                            if(data!="0"){
                                $('#evaluationTextArea').val(data.remarks);
                                $('#evaluatedByText').val(data.evaluatedBy);
                            }
                            
                        });
                    }
                }else{
                    if(dateScheduled==$('#systemDate').val()){
                        $('#secondEdit').attr('disabled', false);
                    }else{
                        
                        $('#secondEdit').attr('disabled', true);
                    }
                    $("#secondEdit").removeClass('btn-info').addClass('btn-default');
                    $("#thirdEdit").removeClass('btn-info').addClass('btn-default');
                    $("#firstEdit").addClass('btn-info').removeClass('btn-default');
                    $('[href="#individualCounselingTabEdit"]').tab('show');
                    
                    $('#studentNameTextEdit').attr('disabled', true);
                    $('#saveEditCounselingForm').attr('disabled', false);
    
                    $('#editEvaluationForm').attr('disabled', true);
                    $('#evaluationTextArea').attr('disabled', false);
                    $('#evaluatedByText').attr('disabled', true);
                    $('#evaluationTextArea').val('');
                    //$('#evaluatedByText').val('');
                    $('#saveEvaluationForm').attr('disabled', false);
                }
            });
        });

        var temp = [];
        var i =0;
        $('#counselingRow-'+counselingid).children('td').each(function(){
            temp[i] = $(this).html();
            i++;
        });
        if($('#counselingTypeOf').val()=='individual'){
            $('#editIndividualCounselingModal').modal('show');
            $('#individualContainer').show();
            $('#groupContainer').hide();
        }else{
            $('#editIndividualCounselingModal').modal('show');
            $('#individualContainer').hide();
            $('#groupContainer').show();
            tableLoader('6', 'studentTableForGroupCounselingEdit');
            $.get('/counseling/group/data/'+counselingid, function(data){
                $('#studentTableForGroupCounselingEdit').html(data.html);
                /*walkinGroupCouncelingEdit
                dateScheduledGroupCounselingEdit
                counselingDateGroupCounselingEdit*/
                $('#dateScheduledGroupCounselingEdit').val(data.dateScheduled);
                $('#counselingDateGroupCounselingEdit').val(data.dateRecorded);
                if(data.walkin == 1){
                    $('#walkinGroupCouncelingEdit').attr('checked', true);
                }else{
                    $('#walkinGroupCouncelingEdit').attr('checked', false);
                }
                console.log(data);
            });
        }
        
  }
  if($('#view').val()=="true"){
    $('#rescheduleButton').attr('disabled', false);
    counselingid = $('#counseling').val();
    var studentid = $('#studentCounseling').val();
    var dateScheduled;
    $('#secondEdit').attr('disabled', true);
    $('#thirdEdit').attr('disabled', true);
    $('#studentNameTextEdit').val(studentid);
    $.get('/counseling/studentdata/'+$('#studentNameTextEdit').val(), function(data){
        //console.log(data);
        if(data.studentType == 'college'){
          $('#courseNameTextEdit').val(data.course);
        }
        $('#yearLevelGradeLevelTextEdit').val(data.gradeLevel);
        $('#homeAddressTextEdit').val(data.cityAddress);      
        $('#studentContactNoTextEdit').val(data.contactNo);
        var yearToday =  (new Date()).getFullYear();
        var yearBirth  = data.dateOfBirth.split('-')[0];
        //console.log(yearToday+' '+yearBirth);
        var age = yearToday - yearBirth;
        $('#studentAgeTextEdit').val(age);
        $('#studentTypeTextEdit').val(data.studentType);
        $('#studentSexTextEdit').val(data.gender);    
    });
    new Promise((resolve, error)=>{
        $.get('/counseling/data/'+counselingid, function(data){
            $('#assistedByTextEdit').val(data.assistedBy);
            $('#statementOfTheProblemTextAreaEdit').val(data.statementOfTheProblem);
            if(data.walkin ==1){
                $('#walkinEdit').attr('checked', true);
            }
            $('#dateScheduledTextEdit').val(data.dateRecorded);
            $('#counselingDateEdit').val(data.dateScheduled);
            dateScheduled = data.dateScheduled; 
            resolve(true);
        });
    }).then((res)=>{
        $('#saveEditCounselingForm').attr('disabled', true);
        $.get('/counseling/checkiffolloweduporevaluated/'+counselingid, function(data){
            tableLoader('4', 'followUpTableIndividual');
            $.get('/counseling/followupdata/'+counselingid, function(mydata){
                $('#followUpTableIndividual').html(mydata);
                if(data =='evaluated'){
                    $('#saveFollowUpFormIndividual').attr('disabled', true);
                    $('.editFollowUpIndividual').attr('disabled', true);
                }else{
                    $('#saveFollowUpFormIndividual').attr('disabled', false);
                    $('.editFollowUpIndividual').attr('disabled', false);
                }
            });
            //console.log(data);
            if(data=='followedup' || data=='evaluated'){
                $('#rescheduleButton').attr('disabled', true);
                $('#saveEditCounselingForm').attr('disabled', true);
                $('#studentNameTextEdit').attr('disabled', true);
                if(data =='followedup'){
                    $('#secondEdit').attr('disabled', false);
                    $('#thirdEdit').attr('disabled', false);

                    $('#saveFollowUpFormIndividual').attr('disabled', false);
                    $("#firstEdit").removeClass('btn-info').addClass('btn-default');
                    $("#thirdEdit").removeClass('btn-info').addClass('btn-default');
                    $("#secondEdit").addClass('btn-info').removeClass('btn-default');
                    $('[href="#followUpTabEdit"]').tab('show');
                    
                    $('#editEvaluationForm').attr('disabled', true);
                    $('#evaluationTextArea').attr('disabled', false);
                    $('#evaluatedByText').attr('disabled', true);
                    $('#saveEvaluationForm').attr('disabled', false);
                }else{
                    $('#secondEdit').attr('disabled', false);
                    $('#thirdEdit').attr('disabled', false);

                    $('#evaluationTextArea').attr('disabled', true);
                    $('#evaluatedByText').attr('disabled', true);
                    $('#saveEvaluationForm').attr('disabled', true);
                    $('#editEvaluationForm').attr('disabled', false);
                    $('#saveFollowUpFormIndividual').attr('disabled', true);
                    $("#secondEdit").removeClass('btn-info').addClass('btn-default');
                    $("#firstEdit").removeClass('btn-info').addClass('btn-default');
                    $("#thirdEdit").addClass('btn-info').removeClass('btn-default');
                    $('[href="#evaluationTabEdit"]').tab('show');
                    $.get('/counseling/evaluation/data/'+counselingid, function(data){
                        if(data!="0"){
                            $('#evaluationTextArea').val(data.remarks);
                            $('#evaluatedByText').val(data.evaluatedBy);
                        }
                        
                    });
                }
            }else{
                if(dateScheduled==$('#systemDate').val()){
                    $('#secondEdit').attr('disabled', false);
                }else{
                    
                    $('#secondEdit').attr('disabled', true);
                }
                $("#secondEdit").removeClass('btn-info').addClass('btn-default');
                $("#thirdEdit").removeClass('btn-info').addClass('btn-default');
                $("#firstEdit").addClass('btn-info').removeClass('btn-default');
                $('[href="#individualCounselingTabEdit"]').tab('show');
                
                $('#studentNameTextEdit').attr('disabled', true);
                $('#saveEditCounselingForm').attr('disabled', false);

                $('#editEvaluationForm').attr('disabled', true);
                $('#evaluationTextArea').attr('disabled', false);
                $('#evaluatedByText').attr('disabled', true);
                $('#evaluationTextArea').val('');
                //$('#evaluatedByText').val('');
                $('#saveEvaluationForm').attr('disabled', false);
            }
        });
    });
    
    var temp = [];
    var i =0;
    $('#counselingRow-'+counselingid).children('td').each(function(){
        temp[i] = $(this).html();
        i++;
    });
    if($('#counselingTypeOf').val()=='individual'){
        $('#editIndividualCounselingModal').modal('show');
        $('#individualContainer').show();
        $('#groupContainer').hide();
    }else{
        $('#editIndividualCounselingModal').modal('show');
        $('#individualContainer').hide();
        $('#groupContainer').show();
        tableLoader('6', 'studentTableForGroupCounselingEdit');
        $.get('/counseling/group/data/'+counselingid, function(data){
            $('#studentTableForGroupCounselingEdit').html(data.html);
            /*walkinGroupCouncelingEdit
            dateScheduledGroupCounselingEdit
            counselingDateGroupCounselingEdit*/
            $('#dateScheduledGroupCounselingEdit').val(data.dateScheduled);
            $('#counselingDateGroupCounselingEdit').val(data.dateRecorded);
            if(data.walkin == 1){
                $('#walkinGroupCouncelingEdit').attr('checked', true);
            }else{
                $('#walkinGroupCouncelingEdit').attr('checked', false);
            }
            console.log(data);
        });
    }
  }
  tableLoader('9', 'counselingTable');
//   $('#viewByStudentType').attr('disabled', false);
//   $('#viewByStudentType').html('');
//   $('#viewByStudentType').html("<option value='all'>all</option>"+
//                                 "<option value='college'>College</option>"+
//                                 "<option value='seniorhigh'>Senior High</option>"+
//                                 "<option value='juniorhigh'>Junior High</option>"+
//                                 "<option value='elementary'>Elementary</option>");
  $('.btn-circle').on('click',function(){ 
    $('.btn-circle.btn-info').removeClass('btn-info').addClass('btn-default');
    $(this).addClass('btn-info').removeClass('btn-default').blur();
  });
  $("#groupCounselingButton").on('click', function(){
      $("#groupCounselingModal").modal("show");
      getAjax('/counseling/group/students/forinsert/'+$('#studentTypeFilterGroup').val()+'/'+$('#schoolYearFilterGroup').val()+'/'+$('#semesterFilterGroup').val()+'/'+$('#courseGradeLevelFilterGroup').val()+'/'+$('#searchStudentsGroupCounseling').val(), 'studentTableForGroupCounseling');
  });
  $("#groupCounselingModal").on('hidden.bs.modal', function(){
    studentGroups=[];
  });
  $('#studentTableForGroupCounseling').on('click', '.checkboxStudent', function(){
    if($(this).prop("checked")){
       studentGroups.push($(this).val());
       //console.log(studentGroups);
    }else{
        var index = studentGroups.indexOf($(this).val());
        studentGroups.splice(index, 1);
        //console.log(studentGroups);
    }
  });
  $("#showSecondTabEdit, #showSecondTab2Edit").on('click', function(){
    $("#firstEdit").removeClass('btn-info').addClass('btn-default');
    $("#thirdEdit").removeClass('btn-info').addClass('btn-default');
    $("#secondEdit").addClass('btn-info').removeClass('btn-default');
    $('[href="#followUpTabEdit"]').tab('show');
    //followUpTabEdit
  });
  $("#showFirstTabEdit").on('click', function(){
    $("#secondEdit").removeClass('btn-info').addClass('btn-default');
    $("#thirdEdit").removeClass('btn-info').addClass('btn-default');
    $("#firstEdit").addClass('btn-info').removeClass('btn-default');
    $('[href="#individualCounselingTabEdit"]').tab('show');
  });
  $("#showLastTabEdit").on('click', function(){
    $("#secondEdit").removeClass('btn-info').addClass('btn-default');
    $("#firstEdit").removeClass('btn-info').addClass('btn-default');
    $("#thirdEdit").addClass('btn-info').removeClass('btn-default');
    $('[href="#evaluationTabEdit"]').tab('show');
  }); 
   
  
  $('#studentNameText').on('change', function(){
    $.get('/counseling/studentdata/'+$('#studentNameText').val(), function(data){
      //console.log(data);
      if(data.studentType == 'college'){
        $('#courseNameText').val(data.course);
      }
      $('#yearLevelGradeLevelText').val(data.gradeLevel);
      $('#homeAddressText').val(data.cityAddress);      
      $('#studentContactNoText').val(data.contactNo);
      var yearToday =  (new Date()).getFullYear();
      var yearBirth  = data.dateOfBirth.split('-')[0];
      //console.log(yearToday+' '+yearBirth);
      var age = yearToday - yearBirth;
      $('#studentAgeText').val(age);
      $('#studentTypeText').val(data.studentType);
      $('#studentSexText').val(data.gender);
      
    });
  });
  $('#counselingAddForm').on('submit', function(e){
    
    e.preventDefault();
    if($('#studentNameText').val()==''){
        new PNotify({
                    title: 'Error!',
                    text: 'Please select a student!',
                    type: 'error',
                    styling: 'bootstrap3'
                });
                return;
    }
    if(validateCounselingDate($('#counselingDate').val(), $('#systemDate').val())==false){
        new PNotify({
            title: 'Error!',
            text: 'Counseling date must not be less than system date!',
            type: 'error',
            styling: 'bootstrap3'
        });
        return;
    }
    if($("#counseledByText").val()==""){
        new PNotify({
            title: 'Error!',
            text: 'Please select a counselor!',
            type: 'error',
            styling: 'bootstrap3'
        });
        return;
    }
    var date = new Date($('#counselingDate').val());
    var scheduledate =  new Date($('#dateScheduledText').val());
    if(date< scheduledate){
        new PNotify({
            title: 'Error!',
            text: 'Counseling Date Must Not be less than scheduled date!',
            type: 'error',
            styling: 'bootstrap3'
        });
        return; 
    }
    var totalRow = 0;
    $('#counselingTable').children('tr').each(function(){
        totalRow++;
    });
    totalRow++;
    form = document.getElementById('counselingAddForm');
    var myform = new FormData(form);
    myform.append('studentID', $('#studentNameText').val());
    myform.append('dateScheduledText', $('#dateScheduledText').val());
    myform.append('assistedByText', $('#assistedByText').val());
    myform.append('rowCounter', totalRow);
    prependPostAjaxErrorCallback('/counseling/student/insert', myform, function(data){
        new PNotify({
            title: 'Success!',
            text: 'Counseling Successfully saved!',
            type: 'success',
            styling: 'bootstrap3'
        });
        $('#counselingTable').append(data);
        $("#counselingTable").find("h2").remove();
        $('#individualCounselingModal').modal('hide');
        $('#individualCounselingModal input[type="text"]').val('');
        $('#individualCounselingModal textarea').val('');
        $("#studentNameText").val("");
        $('#statusMessageModal').modal('show');
        divLoader('messageLoader');
        prependPostAjaxErrorCallback('/counseling/sendmessage', myform, function(data){
            new PNotify({
                title: 'Success!',
                text: 'The student has been notified for this counseling!',
                type: 'success',
                styling: 'bootstrap3'
            });
            $('#messageStatus').html("<h2>All messages sent!</h2>");
            $('#messageLoader').html("");
        });
    });
  });
  $('#statusMessageModal').on('hidden.bs.modal', function(){
    $('#messageStatus').html("<h2>Message is now sending. Please wait, while we send all the messages. This may take a few minutes.</h2>");
  });
  $('#counselingAddGroupForm').on('submit', function(e){
    e.preventDefault();
    var date = new Date($('#counselingDateGroupCounseling').val());
    var scheduledate =  new Date($('#dateScheduledGroupCounseling').val());
    if(scheduledate> date){
        new PNotify({
            title: 'Error!',
            text: 'Scheduled Date Must Not be less than counseling date!',
            type: 'error',
            styling: 'bootstrap3'
        });
        return; 
    }
    if($("#counseledByTextGroup").val()==""){
        new PNotify({
            title: 'Error!',
            text: 'Please select a counselor!',
            type: 'error',
            styling: 'bootstrap3'
        });
        return;
    }
    if(validateCounselingDate($('#counselingDateGroupCounseling').val(), $('#systemDate').val())==false){
        new PNotify({
            title: 'Error!',
            text: 'Counseling date must not be less than system date!',
            type: 'error',
            styling: 'bootstrap3'
        });
        return;
    }
    if (studentGroups.length < 2){
        new PNotify({
            title: 'Error!',
            text: 'Atleast more than one student is needed for group counseling!',
            type: 'error',
            styling: 'bootstrap3'
        });
        return;
    }
    var form = document.getElementById('counselingAddGroupForm');
    var myform = new FormData(form);
    var totalRow = 0;
    $('#counselingTable').children('tr').each(function(){
        totalRow++;
    });
    totalRow++;
    myform.append('students',studentGroups);
    myform.append('rowCounter',totalRow);
    prependPostAjaxErrorCallback('/counseling/student/group/insert', myform, function(data){
        new PNotify({
            title: 'Success!',
            text: 'Counseling Successfully saved!',
            type: 'success',
            styling: 'bootstrap3'
        });
        $('#counselingTable').append(data);
        $('#groupCounselingModal').modal('hide');
        $('#groupCounselingModal input[type="text"]').val('');
        $('#groupCounselingModal textarea').val('');
        $('#statusMessageModal').modal('show');
        divLoader('messageLoader');
        new Promise((resolve, reject)=>{
            var x = 1;
            y = studentGroups.length; 
            for(var y = 0; y<studentGroups.length; y++){
                var thisform = new FormData(form);
                thisform.append("studentID", studentGroups[y]);
                thisform.append("counselingDate", $('#counselingDateGroupCounseling').val());
                prependPostAjaxErrorCallback('/counseling/sendmessage', thisform, function(data){
                    console.log(data);
                   
                    new PNotify({
                        title: 'Success!',
                        text: 'A student has been notified for counseling!',
                        type: 'success',
                        styling: 'bootstrap3'
                    });
                    x++;
                    if(x==y){
                        //console.log(x);
                        resolve(true);
                    }
                });
            }
        }).then((resolve)=>{
            $('#statusMessageModal').modal('hide');
            $('#messageStatus').html("<h2>All messages sent!</h2>");
            $('#messageLoader').html("");
        });
        
    });
  }); 
  $('#counselingTable').on('click', '.viewCounseling', function(){
    $('#rescheduleButton').attr('disabled', false);
    counselingid = $(this).prop('id').split('-')[1];
    var studentid = $(this).attr('data-value');
    var dateScheduled;
    $('#secondEdit').attr('disabled', true);
    $('#thirdEdit').attr('disabled', true);
    $("#showSecondTabEdit").attr('disabled', true);
    $('#studentNameTextEdit').val(studentid);
    $.get('/counseling/studentdata/'+$('#studentNameTextEdit').val(), function(data){
        //console.log(data);
        if(data.studentType == 'college'){
          $('#courseNameTextEdit').val(data.course);
        }
        $('#yearLevelGradeLevelTextEdit').val(data.gradeLevel);
        $('#homeAddressTextEdit').val(data.cityAddress);      
        $('#studentContactNoTextEdit').val(data.contactNo);
        var yearToday =  (new Date()).getFullYear();
        var yearBirth  = data.dateOfBirth.split('-')[0];
        //console.log(yearToday+' '+yearBirth);
        var age = yearToday - yearBirth;
        $('#studentAgeTextEdit').val(age);
        var studtype;
        switch (data.studentType) {
            case "college":
                studtype = 'College';
            break;
            case "seniorhigh":
                studtype = 'Senior High';
            break;
            case "juniorhigh":
                studtype = 'Junior High';
            break;
            case "elementary":
                studtype = 'Elementary';
            break;
            default:
                break;
        }
        $('#studentTypeTextEdit').val(studtype);
        $('#studentSexTextEdit').val(data.gender.capitalize());    
    });
    new Promise((resolve, error)=>{
        $.get('/counseling/data/'+counselingid, function(data){
            $('#assistedByTextEdit').val(data.assistedBy);
            $('#statementOfTheProblemTextAreaEdit').val(data.statementOfTheProblem);
            if(data.walkin ==1){
                $('#walkinEdit').attr('checked', true);
            }
            $('#dateScheduledTextEdit').val(data.dateRecorded);
            $('#counselingDateEdit').val(data.dateScheduled);
            $("#counseledByTextEdit").val(data.counselorID);
            $("#nameOfCounselorEdit").val(data.counseledBy)
            dateScheduled = data.dateScheduled; 
            mycounselingdate = data.dateScheduled;
            resolve(true);
        });
    }).then((res)=>{
        $('#saveEditCounselingForm').attr('disabled', true);
        $.get('/counseling/checkiffolloweduporevaluated/'+counselingid, function(data){
            tableLoader('4', 'followUpTableIndividual');
            $.get('/counseling/followupdata/'+counselingid, function(mydata){
                $('#followUpTableIndividual').html(mydata);
                if(data =='evaluated'){
                    $('#saveFollowUpFormIndividual').attr('disabled', true);
                    $('.editFollowUpIndividual').attr('disabled', true);
                }else{
                    $('#saveFollowUpFormIndividual').attr('disabled', false);
                    $('.editFollowUpIndividual').attr('disabled', false);
                }
            });
            //console.log(data);
            if(data=='followedup' || data=='evaluated'){
                $('#rescheduleButton').attr('disabled', true);
                $('#saveEditCounselingForm').attr('disabled', true);
                $('#studentNameTextEdit').attr('disabled', true);
                if(data =='followedup'){
                    $('#secondEdit').attr('disabled', false);
                    $('#thirdEdit').attr('disabled', false);

                    $('#saveFollowUpFormIndividual').attr('disabled', false);
                    $("#firstEdit").removeClass('btn-info').addClass('btn-default');
                    $("#thirdEdit").removeClass('btn-info').addClass('btn-default');
                    $("#secondEdit").addClass('btn-info').removeClass('btn-default');
                    $('[href="#followUpTabEdit"]').tab('show');
                    
                    $('#editEvaluationForm').attr('disabled', true);
                    $('#evaluationTextArea').attr('disabled', false);
                    $('#evaluatedByText').attr('disabled', true);
                    $('#saveEvaluationForm').attr('disabled', false);
                }else{
                    $('#secondEdit').attr('disabled', false);
                    $('#thirdEdit').attr('disabled', false);

                    $('#evaluationTextArea').attr('disabled', true);
                    $('#evaluatedByText').attr('disabled', true);
                    $('#saveEvaluationForm').attr('disabled', true);
                    $('#editEvaluationForm').attr('disabled', false);
                    $('#saveFollowUpFormIndividual').attr('disabled', true);
                    $("#secondEdit").removeClass('btn-info').addClass('btn-default');
                    $("#firstEdit").removeClass('btn-info').addClass('btn-default');
                    $("#thirdEdit").addClass('btn-info').removeClass('btn-default');
                    $('[href="#evaluationTabEdit"]').tab('show');
                    $.get('/counseling/evaluation/data/'+counselingid, function(data){
                        if(data!="0"){
                            $('#evaluationTextArea').val(data.remarks);
                            $('#evaluatedByText').val(data.evaluatedBy);
                        }
                        
                    });
                } 
            }else{
                if(dateScheduled==$('#systemDate').val()){
                    $('#secondEdit').attr('disabled', false);
                    $("#showSecondTabEdit").attr('disabled', false);
                }else{
                    $("#showSecondTabEdit").attr('disabled', true);
                    $('#secondEdit').attr('disabled', true);
                }
                $("#secondEdit").removeClass('btn-info').addClass('btn-default');
                $("#thirdEdit").removeClass('btn-info').addClass('btn-default');
                $("#firstEdit").addClass('btn-info').removeClass('btn-default');
                $('[href="#individualCounselingTabEdit"]').tab('show');
                
                $('#studentNameTextEdit').attr('disabled', true);
                $('#saveEditCounselingForm').attr('disabled', false);

                $('#editEvaluationForm').attr('disabled', true);
                $('#evaluationTextArea').attr('disabled', false);
                $('#evaluatedByText').attr('disabled', true);
                $('#evaluationTextArea').val('');
                //$('#evaluatedByText').val('');
                $('#saveEvaluationForm').attr('disabled', false);
            }
        });
    });
   
    var temp = [];
    var i =0;
    $('#counselingRow-'+counselingid).children('td').each(function(){
        temp[i] = $(this).html();
        i++;
    });
    
    if(temp[4]=='Individual'){
        $('#editIndividualCounselingModal').modal('show');
        $('#individualContainer').show();
        $('#groupContainer').hide();
    }else{
        $('#editIndividualCounselingModal').modal('show');
        $('#individualContainer').hide();
        $('#groupContainer').show();
        tableLoader('6', 'studentTableForGroupCounselingEdit');
        $.get('/counseling/group/data/'+counselingid, function(data){
            $('#studentTableForGroupCounselingEdit').html(data.html);
            /*walkinGroupCouncelingEdit
            dateScheduledGroupCounselingEdit
            counselingDateGroupCounselingEdit*/
            $('#dateScheduledGroupCounselingEdit').val(data.dateScheduled);
            $('#counselingDateGroupCounselingEdit').val(data.dateRecorded);
            if(data.walkin == 1){
                $('#walkinGroupCouncelingEdit').attr('checked', true);
            }else{
                $('#walkinGroupCouncelingEdit').attr('checked', false);
            }
            //console.log(data);
        });
    }
  });
  $('#editIndividualCounselingModal').on('hidden.bs.modal', function(){
    $("#counselingDateEdit").attr('readonly', true);
  });
  $('#counselingEditForm').on('submit', function(e){
    e.preventDefault();
    var date = new Date($('#counselingDateEdit').val());
    var scheduledate =  new Date($('#counselingDateEdit').val());
   
    var tempCounter = [];
    var row = 0;
    $('#counselingRow-'+counselingid).children('td').each(function(){
        tempCounter[row]= $(this).html();
        row++;
    });
    if(validateCounselingDate($('#counselingDateEdit').val(), $('#systemDate').val())==false){
        new PNotify({
            title: 'Error!',
            text: 'Counseling date must not be less than system date!',
            type: 'error',
            styling: 'bootstrap3'
        });
        return;
    }
    if(validateCounselingDate($('#counselingDateEdit').val(), $('#dateScheduledTextEdit').val())==false){
        new PNotify({
            title: 'Error!',
            text: 'Counseling date must not be less than scheduled date!',
            type: 'error',
            styling: 'bootstrap3'
        });
        return;
    }
    if($('#individualContainer').is(":visible")){
        
        form = document.getElementById('counselingEditForm');
        var myform = new FormData(form);
        myform.append('studentID', $('#studentNameTextEdit').val());
        myform.append('id', counselingid);
        myform.append('rowCounter', tempCounter[0]);
        prependPostAjaxErrorCallback('/counseling/student/update', myform, function(data){
            new PNotify({
                title: 'Success!',
                text: 'Counseling Successfully saved!',
                type: 'success',
                styling: 'bootstrap3'
            });
            if($('#counselingDateEdit').val()== $('#systemDate').val()){ 
                $('#secondEdit').attr('disabled', false);
                $("#showSecondTabEdit").attr('disabled', false);
            }else{
                $('#secondEdit').attr('disabled', true);
                $("#showSecondTabEdit").attr('disabled', true);
            }
            //console.log(data);
            data = data.replace("<tr id ='counselingRow-"+counselingid+"'>", "");
            data = data.replace("</tr>", "");
            $('#counselingRow-'+counselingid).html(data);
            if(mycounselingdate != $("#counselingDateEdit").val()){
                $('#statusMessageModal').modal('show');
                divLoader('messageLoader');
                myform.append("resched", 'true');
                prependPostAjaxErrorCallback('/counseling/sendmessage', myform, function(data){
                    new PNotify({
                        title: 'Success!',
                        text: 'The student has been notified for this counseling!',
                        type: 'success',
                        styling: 'bootstrap3'
                    });
                    $('#messageStatus').html("<h2>All messages sent!</h2>");
                    $('#messageLoader').html("");
                });
            }
        });
    }else{
        //var date = new Date($('#counselingDateGroupCounselingEdit').val());
        //var scheduledate =  new Date($('#dateScheduledGroupCounselingEdit').val());
        form = document.getElementById('counselingEditForm');
        var myform = new FormData(form);
        var students =[];
        var i =0;
        if ($("#groupContainer input:checkbox:checked").length < 2){
            new PNotify({
                title: 'Error!',
                text: 'Atleast more than one student is needed for group counseling!',
                type: 'error',
                styling: 'bootstrap3'
            });
            return;
        }
        $('.checkboxGroupEdit:checkbox:checked').each(function(){
            students[i]=$(this).val();
            i++;
        });
        myform.append('student', students);
        myform.append('id', counselingid);
        myform.append('rowCounter', tempCounter[0]);
        prependPostAjaxErrorCallback('/counseling/student/group/update', myform, function(data){
            new PNotify({
                title: 'Success!',
                text: 'Counseling Successfully saved!',
                type: 'success',
                styling: 'bootstrap3'
            });
            if($('#counselingDateEdit').val()== $('#systemDate').val()){ 
                $('#secondEdit').attr('disabled', false);
                $("#showSecondTabEdit").attr('disabled', false);
            }else{
                $('#secondEdit').attr('disabled', true);
                $("#showSecondTabEdit").attr('disabled', true);
            }
            //console.log(data);
            data = data.replace("<tr id ='counselingRow-"+counselingid+"'>", "");
            data = data.replace("</tr>", "");
            $('#counselingRow-'+counselingid).html(data);
            if(mycounselingdate != $("#counselingDateEdit").val()){
                $('#statusMessageModal').modal('show');
                divLoader('messageLoader');
                new Promise((resolve, reject)=>{
                    var x = 0;
                    //console.log(studentGroups);
                    for(var y = 0; y<students.length; y++){
                        //console.log(y);
                        var thisform = new FormData(form);
                        thisform.append("studentID", students[y]);
                        thisform.append("counselingDate",  $("#counselingDateEdit").val());
                        thisform.append("resched", 'true');
                        prependPostAjaxErrorCallback('/counseling/sendmessage', thisform, function(mydata){
                            //console.log(mydata);
                            
                            new PNotify({
                                title: 'Success!',
                                text: 'A student has been notified for counseling!',
                                type: 'success',
                                styling: 'bootstrap3'
                            });
                            x++;
                            if(x==studentGroups.length){
                                resolve(true);
                            }
                        });
                    }
                }).then((resolve)=>{
                    $('#messageStatus').html("<h2>All messages sent!</h2>");
                    $('#messageLoader').html("");
                });
            }
        });
    }
  }); 
  $('#counselingEditFollowUpForm').on('submit', function(e){
    e.preventDefault();
    form  = document.getElementById('counselingEditFollowUpForm');
    var myform = new FormData(form);
    myform.append('counselingID', counselingid);
    myform.append('assistedBy', $('#assistedByFollowUpIndividualText').val());
    prependPostAjaxErrorCallback('/counseling/followup/insert', myform, function(data){
        new PNotify({
            title: 'Success!',
            text: 'Followup Successfully saved!',
            type: 'success',
            styling: 'bootstrap3'
        });
        $('#followUpTableIndividual').children('h2').each(function(){
            $(this).remove();
        });
        var  i =0;
        $('#counselingRow-'+counselingid).children('td').each(function(){
            if(i==7){
                $(this).html('Followed up');
            } 
            i++;
        });
        $('#rescheduleButton').attr('disabled', true);
        $('#saveEditCounselingForm').attr('disabled', true);
        $('#studentNameTextEdit').attr('disabled', true);
        $('#followUpTableIndividual').prepend(data);
        $('#thirdEdit').attr('disabled', false);
    });
  });
  $('#followUpEditForm').on('submit', function(e){
    e.preventDefault();
    var form = document.getElementById('followUpEditForm');
    var myform = new FormData(form);
    myform.append('id', followupid);
    prependPostAjaxErrorCallback('/counseling/followup/update', myform, function(data){
        new PNotify({
            title: 'Success!',
            text: 'Followup Successfully saved!',
            type: 'success',
            styling: 'bootstrap3'
        });
        data = data.replace("<tr id ='followUpIndividualRow-"+followupid+"'>", "");
        data = data.replace("</tr>", "");
        $('#followUpIndividualRow-'+followupid).html(data);
    });
  });
  $('#followUpTableIndividual').on('click', '.editFollowUpIndividual', function(){
    followupid=$(this).prop('id').split('-')[1];
    
    $.get('/counseling/followupdetails/'+followupid, function(data){
        $('#followUpTextAreaEdit2').val(data);
        var temp =[];
        var i =0;
        $('#followUpIndividualRow-'+followupid).children('td').each(function(){
            temp[i]=$(this).html();
            i++;
        });
        $('#assistedByFollowUpIndividualText2').val(temp[1]);
        $('#editFollowUpModal').modal('show');
    });
  });
  $('#evaluationCounselingForm').on('submit', function(e){
    e.preventDefault();
    var form = document.getElementById('evaluationCounselingForm');
    var myform = new FormData(form);
    myform.append('counselingID', counselingid);
    myform.append('evaluatedBy', $('#evaluatedByText').val());
    prependPostAjaxErrorCallback('/counseling/evaluation/insert', myform, function(data){
        if(data=="This counseling must be followed up first!"){
            new PNotify({
                title: 'Error!',
                text: data,
                type: 'error',
                styling: 'bootstrap3'
            });  
            return;  
        }
        new PNotify({
            title: 'Success!',
            text: 'Evaluation Successfully saved!',
            type: 'success',
            styling: 'bootstrap3'
        });
        $('#evaluationTextArea').attr('disabled', true);
        $('#evaluatedByText').attr('disabled', true);
        $('#saveEvaluationForm').attr('disabled', true);
        $('#editEvaluationForm').attr('disabled', false);
        $('#saveFollowUpFormIndividual').attr('disabled', true);
        $('.editFollowUpIndividual').attr('disabled', true);
        var  i =0;
        $('#counselingRow-'+counselingid).children('td').each(function(){
            if(i==7){
                $(this).html('Evaluated');
            } 
            i++;
        });
    });
  });
  $('#editEvaluationForm').on('click', function(){
    $('#evaluationTextArea').attr('disabled', false);
    $('#evaluatedByText').attr('disabled', false);
    $('#saveEvaluationForm').attr('disabled', false);
  });
 
  $('#studentTypeFilter').on('change', function(){
    if($('#studentTypeFilter').val() == 'all'){
        $('#courseGradeLevelFilter').val('all');
        $('#courseGradeLevelFilter').attr('disabled', true);
        tableLoader('9', 'counselingTable');
        $('#pagination').twbsPagination('destroy');
        $.get('/counseling/pages/1/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/true/'+$('#searchCounseling').val(), function(data){
            initializePages(data);
            $('#pagination').twbsPagination({
                totalPages: totalPages,
                visiblePages: visiblePages,
                onPageClick: function(event, page){
                    mypage = page;
                    tableLoader('9', 'counselingTable');
                    getAjax('/counseling/pages/'+page+'/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/false/'+$('#searchCounseling').val(), 'counselingTable');
                }
            });
        });
      }else{
        $('#courseGradeLevelFilter').attr('disabled', false);
        switch ($('#studentTypeFilter').val()) {
          case 'college':
            $.get('/report/college/course', function(data){
                $('#courseGradeLevelFilter').html("<option value='all'>all</option>");
                $('#courseGradeLevelFilter').append(data);
                tableLoader('9', 'counselingTable');
                $('#pagination').twbsPagination('destroy');
                $.get('/counseling/pages/1/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/true/'+$('#searchCounseling').val(), function(data){
                    initializePages(data);
                    $('#pagination').twbsPagination({
                        totalPages: totalPages,
                        visiblePages: visiblePages,
                        onPageClick: function(event, page){
                            mypage = page;
                            tableLoader('9', 'counselingTable');
                            getAjax('/counseling/pages/'+page+'/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/false/'+$('#searchCounseling').val(), 'counselingTable');
                        }
                    });
                });
            });
          break;
          case 'seniorhigh':
            var html = "<option value ='all'>all</option>"+
                        "<option value ='12'>12</option>"+
                        "<option value ='11'>11</option>";
            $('#courseGradeLevelFilter').html(html);
            tableLoader('9', 'counselingTable');
            $('#pagination').twbsPagination('destroy');
            $.get('/counseling/pages/1/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/true/'+$('#searchCounseling').val(), function(data){
                initializePages(data);
                $('#pagination').twbsPagination({
                    totalPages: totalPages,
                    visiblePages: visiblePages,
                    onPageClick: function(event, page){
                        mypage = page;
                        tableLoader('9', 'counselingTable');
                        getAjax('/counseling/pages/'+page+'/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/false/'+$('#searchCounseling').val(), 'counselingTable');
                    }
                });
            });
          break;
          case 'juniorhigh':
            var html = "<option value ='all'>all</option>"+
                        "<option value ='10'>10</option>"+
                        "<option value ='9'>9</option>"+
                        "<option value ='8'>8</option>"+
                        "<option value ='7'>7</option>";
            $('#courseGradeLevelFilter').html(html);
            tableLoader('9', 'counselingTable');
            $('#pagination').twbsPagination('destroy');
            $.get('/counseling/pages/1/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/true/'+$('#searchCounseling').val(), function(data){
                initializePages(data);
                $('#pagination').twbsPagination({
                    totalPages: totalPages,
                    visiblePages: visiblePages,
                    onPageClick: function(event, page){
                        mypage = page;
                        tableLoader('9', 'counselingTable');
                        getAjax('/counseling/pages/'+page+'/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/false/'+$('#searchCounseling').val(), 'counselingTable');
                    }
                });
            });
          break;
          case 'elementary':
            var html = "<option value ='all'>all</option>"+
                    "<option value ='6'>6</option>"+
                    "<option value ='5'>5</option>"+
                    "<option value ='4'>4</option>"+
                    "<option value ='3'>3</option>"+
                    "<option value ='2'>2</option>"+
                    "<option value ='1'>1</option>";
            $('#courseGradeLevelFilter').html(html);
            tableLoader('9', 'counselingTable');
            $('#pagination').twbsPagination('destroy');
            $.get('/counseling/pages/1/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/true/'+$('#searchCounseling').val(), function(data){
                initializePages(data);
                $('#pagination').twbsPagination({
                    totalPages: totalPages,
                    visiblePages: visiblePages,
                    onPageClick: function(event, page){
                        mypage = page;
                        tableLoader('9', 'counselingTable');
                        getAjax('/counseling/pages/'+page+'/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/false/'+$('#searchCounseling').val(), 'counselingTable');
                    }
                });
            });
          break;
          default:
            break;
        }
    }  
    
  });
  $('#schoolYearFilter').on('change', function(){
    if($('#schoolYearFilter').val() != "all"){
      $.get('/report/schoolyear/'+$('#schoolYearFilter').val(), function(data){
        $('#semesterFilter').html("<option value='all'>all</option>");
        $('#semesterFilter').append(data);
        tableLoader('9', 'counselingTable');
        $('#pagination').twbsPagination('destroy');
        $.get('/counseling/pages/1/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/true/'+$('#searchCounseling').val(), function(data){
            initializePages(data);
            $('#pagination').twbsPagination({
                totalPages: totalPages,
                visiblePages: visiblePages,
                onPageClick: function(event, page){
                    mypage = page;
                    tableLoader('9', 'counselingTable');
                    getAjax('/counseling/pages/'+page+'/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/false/'+$('#searchCounseling').val(), 'counselingTable');
                }
            });
        });
      });
    }else{
        tableLoader('9', 'counselingTable');
        $('#pagination').twbsPagination('destroy');
        $.get('/counseling/pages/1/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/true/'+$('#searchCounseling').val(), function(data){
            initializePages(data);
            $('#pagination').twbsPagination({
                totalPages: totalPages,
                visiblePages: visiblePages,
                onPageClick: function(event, page){
                    mypage = page;
                    tableLoader('9', 'counselingTable');
                    getAjax('/counseling/pages/'+page+'/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/false/'+$('#searchCounseling').val(), 'counselingTable');
                }
            });
        });
    }
  });
  $('#semesterFilter').on('change', function(){
    tableLoader('9', 'counselingTable');
    $('#pagination').twbsPagination('destroy');
    $.get('/counseling/pages/1/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/true/'+$('#searchCounseling').val(), function(data){
        initializePages(data);
        $('#pagination').twbsPagination({
            totalPages: totalPages,
            visiblePages: visiblePages,
            onPageClick: function(event, page){
                mypage = page;
                tableLoader('9', 'counselingTable');
                getAjax('/counseling/pages/'+page+'/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/false/'+$('#searchCounseling').val(), 'counselingTable');
            }
        });
    });
  });
  $('#courseGradeLevelFilter').on('change', function(){
    tableLoader('9', 'counselingTable');
    $('#pagination').twbsPagination('destroy');
    $.get('/counseling/pages/1/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/true/'+$('#searchCounseling').val(), function(data){
        initializePages(data);
        $('#pagination').twbsPagination({
            totalPages: totalPages,
            visiblePages: visiblePages,
            onPageClick: function(event, page){
                mypage = page;
                tableLoader('9', 'counselingTable');
                getAjax('/counseling/pages/'+page+'/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/false/'+$('#searchCounseling').val(), 'counselingTable');
            }
        });
    });
  });
  $('#searchCounseling').on('keydown', function(){
    tableLoader('9', 'counselingTable');
    $('#pagination').twbsPagination('destroy');
    $.get('/counseling/pages/1/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/true/'+$('#searchCounseling').val(), function(data){
        initializePages(data);
        $('#pagination').twbsPagination({
            totalPages: totalPages,
            visiblePages: visiblePages,
            onPageClick: function(event, page){
                mypage = page;
                tableLoader('9', 'counselingTable');
                getAjax('/counseling/pages/'+page+'/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/false/'+$('#searchCounseling').val(), 'counselingTable');
            }
        });
    });
  }); 

  $('#studentTypeFilterGroup').on('change', function(){
    if($('#studentTypeFilterGroup').val() == 'all'){
        $('#courseGradeLevelFilterGroup').val('all');
        $('#courseGradeLevelFilterGroup').attr('disabled', true);
        tableLoader('4', 'studentTableForGroupCounseling');
        $.get('/counseling/group/students/forinsert/'+$('#studentTypeFilterGroup').val()+'/'+$('#schoolYearFilterGroup').val()+'/'+$('#semesterFilterGroup').val()+'/'+$('#courseGradeLevelFilterGroup').val()+'/'+$('#searchStudentsGroupCounseling').val(), function (data) {
            $('#studentTableForGroupCounseling').html(data);
            initializeCheckboxes();
        });
      }else{
        $('#courseGradeLevelFilterGroup').attr('disabled', false);
        switch ($('#studentTypeFilterGroup').val()) {
          case 'college':
            $.get('/report/college/course', function(data){
                $('#courseGradeLevelFilterGroup').html("<option value='all'>all</option>");
                $('#courseGradeLevelFilterGroup').append(data);
                tableLoader('4', 'studentTableForGroupCounseling');
                $.get('/counseling/group/students/forinsert/'+$('#studentTypeFilterGroup').val()+'/'+$('#schoolYearFilterGroup').val()+'/'+$('#semesterFilterGroup').val()+'/'+$('#courseGradeLevelFilterGroup').val()+'/'+$('#searchStudentsGroupCounseling').val(), function (data) {
                    $('#studentTableForGroupCounseling').html(data);
                    initializeCheckboxes();
                });
            });
          break;
          case 'seniorhigh':
            var html = "<option value ='all'>all</option>"+
                        "<option value ='12'>12</option>"+
                        "<option value ='11'>11</option>";
            $('#courseGradeLevelFilterGroup').html(html);
            tableLoader('4', 'studentTableForGroupCounseling');
            $.get('/counseling/group/students/forinsert/'+$('#studentTypeFilterGroup').val()+'/'+$('#schoolYearFilterGroup').val()+'/'+$('#semesterFilterGroup').val()+'/'+$('#courseGradeLevelFilterGroup').val()+'/'+$('#searchStudentsGroupCounseling').val(), function (data) {
                $('#studentTableForGroupCounseling').html(data);
                initializeCheckboxes();
            });
          break;
          case 'juniorhigh':
            var html = "<option value ='all'>all</option>"+
                        "<option value ='10'>10</option>"+
                        "<option value ='9'>9</option>"+
                        "<option value ='8'>8</option>"+
                        "<option value ='7'>7</option>";
            $('#courseGradeLevelFilterGroup').html(html);
            tableLoader('4', 'studentTableForGroupCounseling');
            $.get('/counseling/group/students/forinsert/'+$('#studentTypeFilterGroup').val()+'/'+$('#schoolYearFilterGroup').val()+'/'+$('#semesterFilterGroup').val()+'/'+$('#courseGradeLevelFilterGroup').val()+'/'+$('#searchStudentsGroupCounseling').val(), function (data) {
                $('#studentTableForGroupCounseling').html(data);
                initializeCheckboxes();
            });
          break;
          case 'elementary':
            var html = "<option value ='all'>all</option>"+
                    "<option value ='6'>6</option>"+
                    "<option value ='5'>5</option>"+
                    "<option value ='4'>4</option>"+
                    "<option value ='3'>3</option>"+
                    "<option value ='2'>2</option>"+
                    "<option value ='1'>1</option>";
            $('#courseGradeLevelFilterGroup').html(html);
            tableLoader('4', 'studentTableForGroupCounseling');
            $.get('/counseling/group/students/forinsert/'+$('#studentTypeFilterGroup').val()+'/'+$('#schoolYearFilterGroup').val()+'/'+$('#semesterFilterGroup').val()+'/'+$('#courseGradeLevelFilterGroup').val()+'/'+$('#searchStudentsGroupCounseling').val(), function (data) {
                $('#studentTableForGroupCounseling').html(data);
                initializeCheckboxes();
            });
          break;
          default:
            break;
        }
    }  
    
  });
  $('#schoolYearFilterGroup').on('change', function(){
    if($('#schoolYearFilterGroup').val() != "all"){
      $.get('/report/schoolyear/'+$('#schoolYearFilterGroup').val(), function(data){
          //console.log(data);
        $('#semesterFilterGroup').html("<option value='all'>all</option>");
        $('#semesterFilterGroup').append(data);
        tableLoader('4', 'studentTableForGroupCounseling');
        $.get('/counseling/group/students/forinsert/'+$('#studentTypeFilterGroup').val()+'/'+$('#schoolYearFilterGroup').val()+'/'+$('#semesterFilterGroup').val()+'/'+$('#courseGradeLevelFilterGroup').val()+'/'+$('#searchStudentsGroupCounseling').val(), function (data) {
            $('#studentTableForGroupCounseling').html(data);
            initializeCheckboxes();
        });
      });
    }else{
        tableLoader('4', 'studentTableForGroupCounseling');
        $.get('/counseling/group/students/forinsert/'+$('#studentTypeFilterGroup').val()+'/'+$('#schoolYearFilterGroup').val()+'/'+$('#semesterFilterGroup').val()+'/'+$('#courseGradeLevelFilterGroup').val()+'/'+$('#searchStudentsGroupCounseling').val(), function (data) {
            $('#studentTableForGroupCounseling').html(data);
            initializeCheckboxes();
        });
    }
  });
  $('#semesterFilterGroup').on('change', function(){
    tableLoader('4', 'studentTableForGroupCounseling');
    $.get('/counseling/group/students/forinsert/'+$('#studentTypeFilterGroup').val()+'/'+$('#schoolYearFilterGroup').val()+'/'+$('#semesterFilterGroup').val()+'/'+$('#courseGradeLevelFilterGroup').val()+'/'+$('#searchStudentsGroupCounseling').val(), function (data) {
        $('#studentTableForGroupCounseling').html(data);
        initializeCheckboxes();
    });
  });
  $('#courseGradeLevelFilterGroup').on('change', function(){
    tableLoader('4', 'studentTableForGroupCounseling');
    $.get('/counseling/group/students/forinsert/'+$('#studentTypeFilterGroup').val()+'/'+$('#schoolYearFilterGroup').val()+'/'+$('#semesterFilterGroup').val()+'/'+$('#courseGradeLevelFilterGroup').val()+'/'+$('#searchStudentsGroupCounseling').val(), function (data) {
        $('#studentTableForGroupCounseling').html(data);
        initializeCheckboxes();
    });
  });
  $('#searchStudentsGroupCounseling').on('keydown', function(){
    tableLoader('4', 'studentTableForGroupCounseling');
    $.get('/counseling/group/students/forinsert/'+$('#studentTypeFilterGroup').val()+'/'+$('#schoolYearFilterGroup').val()+'/'+$('#semesterFilterGroup').val()+'/'+$('#courseGradeLevelFilterGroup').val()+'/'+$('#searchStudentsGroupCounseling').val(), function (data) {
        $('#studentTableForGroupCounseling').html(data);
        initializeCheckboxes();
    });
  }); 

  $('#walkin').on('click', function(){
    if($(this).prop('checked')){
        $('#counselingDate').attr('readonly', true);
        $('#counselingDate').val($('#dateScheduledText').val());
    }else{
        $('#counselingDate').attr('readonly', false);
    }
  });
  $('#walkinGroupCounceling').on('click', function(){
    if($(this).prop('checked')){
        $('#counselingDateGroupCounseling').attr('readonly', true);
        $('#counselingDateGroupCounseling').val($('#dateScheduledGroupCounseling').val());
    }else{
        $('#counselingDateGroupCounseling').attr('readonly', false);
    }
  });
  $('#chooseStudent').on('click', function(){

    $('#browseStudentModal').modal('show'); 
    $('#browseStudentModal .modal-title').text("Select a student for counseling");
    tableLoader('7','studentTableReport');
    $.get('/contract/studentsbrowse/all/all/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val()+'/'+$('#searchBrowseStudents').val(), function(data){
      $('#studentTableReport').html(data);
    });
  });
  $('#viewByStudentType').on('change', function(){
    if($('#viewByStudentType').val() == 'all'){
      $('#Course_GradeLevel').val('all');
      $('#Course_GradeLevel').attr('disabled', true);
      tableLoader('5','studentTableReport');

      $.get('/contract/studentsbrowse/all/all/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val()+'/'+$('#searchBrowseStudents').val(), function(data){
        $('#studentTableReport').html(data);
      });
    }else{
      $('#Course_GradeLevel').attr('disabled', false);
      switch ($('#viewByStudentType').val()) {
        case 'college':
        tableLoader('7','studentTableReport');
          $.get('/report/college/course', function(data){
            $('#Course_GradeLevel').html("<option value='all'>all</option>");
            $('#Course_GradeLevel').append(data);
            $.get('/contract/studentsbrowse/college/'+$('#Course_GradeLevel').val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val()+'/'+$('#searchBrowseStudents').val(), function(data){
              $('#studentTableReport').html(data);
            });
          });
        break;
        case 'seniorhigh':
          var html = "<option value ='all'>all</option>"+
                      "<option value ='12'>12</option>"+
                      "<option value ='11'>11</option>";
          $('#Course_GradeLevel').html(html);
          tableLoader('7','studentTableReport');
          
          $.get('/contract/studentsbrowse/seniorhigh/'+$('#Course_GradeLevel').val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val()+'/'+$('#searchBrowseStudents').val(), function(data){
            $('#studentTableReport').html(data);
          });
        break;
        case 'juniorhigh':
          var html = "<option value ='all'>all</option>"+
                      "<option value ='10'>10</option>"+
                      "<option value ='9'>9</option>"+
                      "<option value ='8'>8</option>"+
                      "<option value ='7'>7</option>";
          $('#Course_GradeLevel').html(html);
          tableLoader('7','studentTableReport');
          $.get('/contract/studentsbrowse/juniorhigh/'+$('#Course_GradeLevel').val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val()+'/'+$('#searchBrowseStudents').val(), function(data){
            $('#studentTableReport').html(data);
          });
        break;
        case 'elementary':
          var html = "<option value ='all'>all</option>"+
                  "<option value ='6'>6</option>"+
                  "<option value ='5'>5</option>"+
                  "<option value ='4'>4</option>"+
                  "<option value ='3'>3</option>"+
                  "<option value ='2'>2</option>"+
                  "<option value ='1'>1</option>";
          $('#Course_GradeLevel').html(html);
          tableLoader('7','studentTableReport');
          $.get('/contract/studentsbrowse/elementary/'+$('#Course_GradeLevel').val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val()+'/'+$('#searchBrowseStudents').val(), function(data){
            $('#studentTableReport').html(data);
          });
        break;
        default:
          break;
      }
    }  
  });
  $('#Course_GradeLevel').on('change', function(){
    tableLoader('7','studentTableReport');
    $.get('/contract/studentsbrowse/'+$('#viewByStudentType').val()+'/'+$(this).val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val()+'/'+$('#searchBrowseStudents').val(), function(data){
      $('#studentTableReport').html(data);
    });
  });
  $('#schoolYearStudent').on('change', function(){
    tableLoader('7','studentTableReport');
    if($('#schoolYearStudent').val() != "all"){
      $.get('/report/schoolyear/'+$('#schoolYearStudent').val(), function(data){
        $('#semesterStudent').html("<option value='all'>all</option>");
        $('#semesterStudent').append(data);
        $.get('/contract/studentsbrowse/'+$('#viewByStudentType').val()+'/'+$('#Course_GradeLevel').val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val()+'/'+$('#searchBrowseStudents').val(), function(data){
          $('#studentTableReport').html(data);
        });
      });
    }else{
      $.get('/contract/studentsbrowse/'+$('#viewByStudentType').val()+'/'+$('#Course_GradeLevel').val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val()+'/'+$('#searchBrowseStudents').val(), function(data){
        $('#studentTableReport').html(data);
      });
    }
  });
  $('#semesterStudent').on('change', function(){
    tableLoader('5','studentTableReport');
    $.get('/contract/studentsbrowse/'+$('#viewByStudentType').val()+'/'+$('#Course_GradeLevel').val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val()+'/'+$('#searchBrowseStudents').val(), function(data){
      $('#studentTableReport').html(data);
    });
  });
  $('#searchBrowseStudents').on('keydown', function(){
    tableLoader('7','studentTableReport');
    $.get('/contract/studentsbrowse/college/'+$('#Course_GradeLevel').val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val()+'/'+$('#searchBrowseStudents').val(), function(data){
      $('#studentTableReport').html(data);
    });
  });
  $('#studentTableReport').on('click', '.selectStudent', function(){
    $id = $(this).prop("id").split('-')[1];
    $('#studentNameText').val($id);
    var temp =[];
    var i =0;
    $(this).closest('tr').children("td").each(function(){
      temp[i] = $(this).html();
      i++;
    });
    $('#nameOfStudents').val(temp[1]);
    $.get('/counseling/studentdata/'+$id, function(data){
        //console.log(data);
        if(data.studentType == 'college'){
          $('#courseNameText').val(data.course);
        }
        $('#yearLevelGradeLevelText').val(data.gradeLevel);
        $('#homeAddressText').val(data.cityAddress);      
        $('#studentContactNoText').val(data.contactNo);
        var yearToday =  (new Date()).getFullYear();
        var yearBirth  = data.dateOfBirth.split('-')[0];
        //console.log(yearToday+' '+yearBirth);
        var age = yearToday - yearBirth;
        $('#studentAgeText').val(age);
        var studtype;
        switch (data.studentType) {
            case "college":
                studtype = 'College';
            break;
            case "seniorhigh":
                studtype = 'Senior High';
            break;
            case "juniorhigh":
                studtype = 'Junior High';
            break;
            case "elementary":
                studtype = 'Elementary';
            break;
            default:
                break;
        }
        $('#studentTypeText').val(studtype);
        $('#studentSexText').val(data.gender.capitalize());
    });
    
    $('#browseStudentModal').modal('hide');
  });
 
  $('#rescheduleButton').on('click', function(){
    $('#counselingDateEdit').attr('readonly', false);
  });
  $("#printCounselingStudents").on('click', function(){
    $.get('/reportcounseling/print/'+$('#studentTypeFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#searchCounseling').val(), function(data){
        if(data=="true"){
          window.location.replace("/temp/report.pdf");
        }
      });
  });
  $("#chooseCounselor").on('click', function(){
    tableLoader('4', 'counselorTableForCounseling');
    counselingAction = "addIndividual";
    $("#chooseCounselorModal").modal("show");
    $.get('/counselor/counseling', function(data){
        $("#counselorTableForCounseling").html(data);
    });
  });
  $("#counselorTableForCounseling").on('click', '.selectCounselor', function(){
    var id = $(this).prop("id").split("-")[1];
    var counselorName;
    var i =0;
    $(this).closest("tr").children("td").each(function(){
        if(i==0){
            counselorName = $(this).html();
        }
        i++;
    });
    switch (counselingAction) {
        case 'addIndividual':
            $("#nameOfCounselor").val(counselorName);
            $("#counseledByText").val(id);   
            $("#chooseCounselorModal").modal("hide"); 
        break;
        case 'addGroup':
            $("#nameOfCounselorGroup").val(counselorName);
            $("#counseledByTextGroup").val(id);   
            $("#chooseCounselorModal").modal("hide"); 
        break;
        case 'editIndiv':
            $("#nameOfCounselorEdit").val(counselorName);
            $("#counseledByTextEdit").val(id);   
            $("#chooseCounselorModal").modal("hide");
        break;
        default:
            break;
    }
  });
  $("#chooseCounselorGroup").on("click", function(){
    tableLoader('4', 'counselorTableForCounseling');
    counselingAction = "addGroup";
    $("#chooseCounselorModal").modal("show");
    $.get('/counselor/counseling', function(data){
        $("#counselorTableForCounseling").html(data);
    });
  });
  $("#chooseCounselorEdit").on('click', function(){
    tableLoader('4', 'counselorTableForCounseling');
    counselingAction = "editIndiv";
    $("#chooseCounselorModal").modal("show");
    $.get('/counselor/counseling', function(data){
        $("#counselorTableForCounseling").html(data);
    });
  });
});
$.get('/counseling/pages/1/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/true/'+$('#searchCounseling').val(), function(data){
  initializePages(data);
  $('#pagination').twbsPagination({
      totalPages: totalPages,
      visiblePages: visiblePages,
      onPageClick: function(event, page){
          mypage = page;
          tableLoader('9', 'counselingTable');
          getAjax('/counseling/pages/'+page+'/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/false/'+$('#searchCounseling').val(), 'counselingTable');
      }
  });
  //console.log(data);
});
function initializePages(data){
  totalRow = data;
  if(totalRow<=5){
      totalPages = 1;
  }else{
      var remainder = totalRow%10;
      if(remainder >0){
          totalPages = ((totalRow-remainder)/10)+1;
      }else{
          totalPages = totalRow/10;
      }
      if(totalPages<=5){
          visiblePages =totalPages;
      }else{
          visiblePages = 5;
      }    
  }
}
function initializeCheckboxes(){
    $('.checkboxStudent').each(function(){
        if(studentGroups.indexOf($(this).val()) != -1){
            $(this).attr("checked", true);
        }
    });
}
function validateCounselingDate(counselingDate, systemDate){
    var cDate =  new Date(counselingDate);
    var sDate =  new Date(systemDate);
    if(cDate < sDate){
        return false;
    }else{
        return true;
    }
}
