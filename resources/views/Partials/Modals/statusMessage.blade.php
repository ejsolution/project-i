@component('components.modal')
  @slot('modalid')
    statusMessageModal
  @endslot
  @slot('modalsize')
    modal-sm
  @endslot
  @slot('modaltitle')
    Information
  @endslot
  @slot('modalcontent')
    <div id='messageStatus'>
      <h2>Message is now sending. Please wait, while we send all the messages. This may take a few minutes.</h2>
    </div>
    <div id='messageLoader'>
      
    </div>
  @endslot
  @slot('modalbutton')
      
  @endslot
@endcomponent