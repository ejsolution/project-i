<div id="{{$modalid}}" class="modal fade" role="dialog">
  <div class="modal-dialog {{$modalsize}}">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"></button>
        <h4 class="modal-title">{{$modaltitle}}</h4>
      </div>
      <div class="modal-body">
        <div class='container-fluid'>
          {{$modalcontent}}
        </div>
      </div>
      <div class="modal-footer">
        {{$modalbutton}}
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
