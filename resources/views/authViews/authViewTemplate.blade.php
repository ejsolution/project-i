<!doctype html>
<html>
  <head>
    @include('Partials._metaHead')
    <title>Guidance | @yield('title')</title>
    <link href="{{asset('css/generic.css')}}" rel="stylesheet">
    <link href="{{asset('bootstrap4/css/bootstrap.min.css')}}" rel="stylesheet">
    @yield('css')

  </head>
  <body>
    @yield('content')
    <script src= '{{asset('bootstrap4/js/jquery.js')}}'></script>
		<script src= '{{asset('js/parsley.min.js')}}'></script>
		<script src= '{{asset('bootstrap4/js/tether.min.js')}}'></script>
		<script src= '{{asset('bootstrap4/js/bootstrap.min.js')}}'></script>
    @yield('script')
  </body>
</html>
