<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\model\systemDate;
use App\model\schoolYear;
class systemTimeRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $systemdate = systemDate::all();
        if(count($systemdate)>0){
            $schoolyear =schoolYear::where('status', '=', '1')
                                    ->where('schoolYearStart', '<=', $systemdate[0]->systemDate)
                                    ->where('schoolYearEnd', '>=', $systemdate[0]->systemDate)
                                    ->get();
            //error_log($systemdate);
            if(count($schoolyear)>0){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The system time is not matched with the active school year.';
    }
}
