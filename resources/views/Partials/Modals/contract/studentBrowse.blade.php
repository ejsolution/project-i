@component('components.modal')
  @slot('modalid')
    browseStudentModal
  @endslot
  @slot('modalsize')
    modal-lg
  @endslot
  @slot('modaltitle')
    Select a student for contract
  @endslot
  @slot('modalcontent')
    <div class='row well'>
      <div class='col-md-3'>
        <div>
          <label for 'viewByStudentType'>View By Student Type</label>
        </div>
        <div>
          {{--  <select id = 'viewByStudentType' class='form-control' disabled>
            
            <option value='college'>college</option>
           
          </select>  --}}
          @if(session()->get('user')['userType']=='admin')
          <select id ='viewByStudentType' class='form-control'>
              <option value ='all'>All</option>
              <option value ='college'>College</option>
              <option value ='seniorhigh'>Senior High</option>
              <option value ='juniorhigh'>Junior High</option>
              <option value ='elementary'>Elementary</option>
          @else
              @switch($user[0])
                  @case('college')
                  <select id ='viewByStudentType' class='form-control' readonly>
                      <option value ='college'>College</option>
                  @break
                  @case('seniorhigh')
                  <select id ='viewByStudentType' class='form-control' readonly>
                      <option value ='seniorhigh'>Senior High</option>
                  @break
                  @case('juniorhigh')
                  <select id ='viewByStudentType' class='form-control' readonly>
                      <option value ='juniorhigh'>Junior High</option>
                  @break
                  @case('elementary')
                  <select id ='viewByStudentType' class='form-control' readonly>
                      <option value ='elementary'>Elementary</option>
                  @break
                  @default
                      Default case...
              @endswitch
          @endif
          </select>
        </div>
      </div>
      <div class='col-md-3'>
        <div>
          <label for 'Course_GradeLevel'>Course/ Grade Level</label>
        </div>
        <div> 
          {{--  <select id = 'Course_GradeLevel' class='form-control'>
            <option value='all'>all</option>
          </select>  --}}
          @if(session()->get('user')['userType']=='admin')
                    <select id ='Course_GradeLevel' class='form-control'>
                            <option value ='all'>all</option>
          @else
              @switch($user[0])
                  @case('college')
                  <select id ='Course_GradeLevel' class='form-control'>
                      <option value ='all'>all</option>
                      
                      @foreach($collegeOfUser as $college)
                          <option value = "{{$college->id}}">{{$college->courseName}}</option>
                      @endforeach
                  @break
                  @case('seniorhigh')
                  <select id ='Course_GradeLevel' class='form-control' readonly>
                      <option value ="{{$user[1]}}">{{$user[1]}}</option>
                  @break
                  @case('juniorhigh')
                  <select id ='Course_GradeLevel' class='form-control' readonly>
                      <option value ="{{$user[1]}}">{{$user[1]}}</option>
                  @break
                  @case('elementary')
                  <select id ='Course_GradeLevel' class='form-control' readonly>
                      <option value ="{{$user[1]}}">{{$user[1]}}</option>
                  @break
                  @default
                      Default case...
              @endswitch

          @endif
          </select>
        </div>
      </div>
      <div class='col-md-3'>
        <div>
          <label for 'schoolYearStudent'>School Year</label>
        </div>
        <div>
          <select id = 'schoolYearStudent' class='form-control'>
            <option value='all'>all</option>
            @foreach($schoolyears as $year)
              <option value ='{{$year->id}}'>{{explode('-', $year->schoolYearStart)[0]}} - {{explode('-', $year->schoolYearEnd)[0]}}</option>
            @endforeach
          </select>
        </div>
      </div>
      <div class='col-md-3'>
        <div>
          <label for 'semesterStudent'>Semester</label>
        </div>
        <div>
          <select id = 'semesterStudent' class='form-control'>
            <option value='all'>all</option>
          </select>
        </div>
      </div>
    </div>
    <div class='row'>
      <div class="form-group has-feedback">
        <input type="text" class="form-control"  placeholder="Select students" id ='searchBrowseStudents'/>
        <i class="fa fa-search form-control-feedback"></i>
      </div>
    </div>
    <div class ='row'>
      <div class='col-md-12 well tableContainer'>
        <div class='row'>
          <div class='col-md-12'>
            <table class='table table-condensed'>
              <thead>
                <tr>
                  <th>No</th>
                  <th>Name</th>
                  <th>Student No.</th>
                  <th>Student Type</th>
                  <th id ='courseTH'>Course</th>
                  <th id ='yearLevelTH'>Year Level</th>
                  <th>Select</th>
                </tr>
              </thead>
              <tbody id ='studentTableReport'>
                {{--<tr class= 'rowClickable'>
                  <td>test</td>
                  <td>test</td>
                  <td>test</td>
                  <td>test</td>
                  <td>test</td>
                </tr>--}}
              </tbody>
            </table>
          </div>
        </div>
        </div>
    </div>
  @endslot
  @slot('modalbutton')
     
  @endslot
@endcomponent