@component('components.modal')
  @slot('modalid')
    elementarySettingsModal
  @endslot
  @slot('modalsize')
    modal-md
  @endslot
  @slot('modaltitle')
    Elementary Settings
  @endslot
  @slot('modalcontent')
    <div class='row'>
      <div class="form-group has-feedback">
          <input type="text" class="form-control"
          placeholder="Search elementary level or sections"/>
          <i class="fa fa-search form-control-feedback"></i>
      </div>
    </div>
    <div class='row'>
      <div class='col-md-12 tableModalContainer'>
        <table class='table table-striped'>
          <thead>
            <tr>
              <th>Elementary School Level</th>
              <th>Sections</th>
              <th>Adviser</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
    </div>
  @endslot
  @slot('modalbutton')
      <button type="button" class="btn btn-danger" data-dismiss="modal"><i class='fa fa-save'></i>&nbsp;Add</button>
  @endslot
@endcomponent
