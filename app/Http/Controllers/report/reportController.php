<?php

namespace App\Http\Controllers\report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\student;
use App\model\collegeStudent;
use App\model\SHSStudent;
use App\model\JHStudent;
use App\model\elemStudent;
use App\model\schoolYear;
use App\model\course;
use App\model\courseDepartmentHists;
use App\model\departmentCollegeHist;
use App\model\department;
use App\model\studentUpdates;
use App\model\referral;
use App\model\behaviour;
use App\model\contract;
use App\model\contractGrade; 
use App\model\contractProof;
use App\model\counseling;
use App\model\groupCounseling;
use App\model\followUp;
use App\model\evaluation;
use App\model\systemDate;
use App\model\studentTypeUpdate;
use App\model\adminsetting;
use App\model\behaviourSetting;
use App\model\seniorHighStrand;
use App\model\counselor;
use DB;
use Auth;
use App\Http\Controllers\student\studentController;
use App\Http\Controllers\users\userController;
use App\Http\Controllers\notification\notificationController;
use PDF;
class reportController extends Controller
{
    private $studentsCounter;
    public function __construct(){
        $this->middleware('auth:myuser');
    }
    public function getReportsPage(){
        $schoolyear = schoolYear::all();
        $user =  (new userController)->getUserDesignation(Auth::guard('myuser')->id());
        $college ='';
        if($user[0]=='college'){
            $college = DB::select(DB::raw("SELECT colleges.collegeName, courses.courseName, 
            courses.id FROM colleges INNER JOIN (SELECT collegeID, userID, id FROM 
            assignment_colleges INNER JOIN (SELECT max(id) AS maxid FROM assignment_colleges 
            GROUP BY userID) ac ON ac.maxid = assignment_colleges.id) ac2 ON ac2.collegeID = 
            colleges.id INNER JOIN (SELECT collegeID, departmentID, id FROM 
            department_college_hists INNER JOIN (SELECT max(id) as maxid FROM 
            department_college_hists GROUP BY departmentID) dch ON dch.maxid = 
            department_college_hists.id) dch2 ON dch2.collegeID = colleges.id 
            INNER JOIN (SELECT courseID, departmentID, id FROM course_department_hists 
            INNER JOIN (SELECT max(id) as maxid FROM course_department_hists GROUP BY courseID) 
            cdh ON cdh.maxid = course_department_hists.id) cdh2 ON cdh2.departmentID = 
            dch2.departmentID INNER JOIN courses on courses.id  = cdh2.courseID WHERE 
            ac2.collegeID = (SELECT collegeID FROM assignment_colleges WHERE id = 
            (SELECT max(id) FROM assignment_colleges WHERE userID ='".Auth::guard('myuser')->id()."'))
            GROUP BY courses.id"));
        } 
        $seniorhighstrand = seniorHighStrand::all();
        $notif = (new notificationController)->getNotifs();
        return view('admin/reports/reports', ['schoolyear'=>$schoolyear,
                                                'user'=>$user,
                                                'notif'=>$notif,
                                                'collegeOfUser'=>$college,
                                                'strand' => $seniorhighstrand]);
    }
    public function refferalCollection($studentid, &$student, $key, $studenttype, $courseGradeLevel, $schoolYear, $semester){
        $mystudent =  new studentController();
        $studenttypetemp = $mystudent->getStudentType($studentid);
        if($studenttype != "all"){
           if($studenttypetemp!=$studenttype){
                $student->forget($key);
                return true;
            }
        }
        if($courseGradeLevel != "all"){
            switch($studenttypetemp){
                case 'college':
                    $temp = collegeStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM college_students WHERE studentID = ".$studentid.")"))->get();
                    if(count($temp)>0){
                        if($temp[0]->courseID != $courseGradeLevel){
                            $student->forget($key);
                            return true;
                        }
                    }else{
                        $student->forget($key);
                        return true;
                    }
                break;
                case 'seniorhigh':
                    $temp = SHSStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM s_h_s_students WHERE studentID = ".$studentid.")"))->get();
                    if(count($temp)>0){
                        if($temp[0]->gradeLevel != $courseGradeLevel){
                            $student->forget($key);
                            return true;
                        }
                    }else{
                        $student->forget($key);
                        return true;
                    }
                break;
                case 'juniorhigh':
                    $temp = JHStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM j_h_students WHERE studentID = ".$studentid.")"))->get();
                    if(count($temp)>0){
                        if($temp[0]->gradeLevel != $courseGradeLevel){
                            $student->forget($key);
                            return true;
                        }
                    }else{
                        $student->forget($key);
                        return true;
                    }
                break;
                case 'elementary':
                    $temp = elemStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM elem_students WHERE studentID = ".$studentid.")"))->get();
                    if(count($temp)>0){
                        if($temp[0]->gradeLevel != $courseGradeLevel){
                            $student->forget($key);
                            return true;
                        }
                    }else{
                        $student->forget($key);
                        return true;
                    }
                break;
            }
        }
        return false;
    }
    public function contractCollection($studentid, &$student, $key, $studenttype, $courseGradeLevel, $schoolYear, $semester){
        $mystudent =  new studentController();
        $studenttypetemp = $mystudent->getStudentType($studentid);
        if($studenttype != "all"){
           if($studenttypetemp!=$studenttype){
                $student->forget($key);
                return true;
            }
        }
        if($courseGradeLevel != "all"){
            switch($studenttypetemp){
                case 'college':
                    $temp = collegeStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM college_students WHERE studentID = ".$studentid.")"))->get();
                    if(count($temp)>0){
                        if($temp[0]->courseID != $courseGradeLevel){
                            $student->forget($key);
                            return true;
                        }
                    }else{
                        $student->forget($key);
                        return true;
                    }
                break;
                case 'seniorhigh':
                    $temp = SHSStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM s_h_s_students WHERE studentID = ".$studentid.")"))->get();
                    if(count($temp)>0){
                        if($temp[0]->gradeLevel != $courseGradeLevel){
                            $student->forget($key);
                            return true;
                        }
                    }else{
                        $student->forget($key);
                        return true;
                    }
                break;
                case 'juniorhigh':
                    $temp = JHStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM j_h_students WHERE studentID = ".$studentid.")"))->get();
                    if(count($temp)>0){
                        if($temp[0]->gradeLevel != $courseGradeLevel){
                            $student->forget($key);
                            return true;
                        }
                    }else{
                        $student->forget($key);
                        return true;
                    }
                break;
                case 'elementary':
                    $temp = elemStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM elem_students WHERE studentID = ".$studentid.")"))->get();
                    if(count($temp)>0){
                        if($temp[0]->gradeLevel != $courseGradeLevel){
                            $student->forget($key);
                            return true;
                        }
                    }else{
                        $student->forget($key);
                        return true;
                    }
                break;
            }
        }
        
        return false;
    }
    public function getStudents($studenttype, $courseGradeLevel, $schoolYear, $semester){
        //$systemdate = systemDate::all();
        $mystudent =  new studentController();
        $students;
        switch ($studenttype) {
            case 'all':
                if($courseGradeLevel=='all'){
                    if($schoolYear=='all'){
                        if($semester == 'all'){
                             $students = student::all();
                             //$students = student::where('created_at', '<=', $systemdate[0]->systemDate)->get();
                        }else{
                             $students = student::all();
                             //$students = student::where('created_at', '<=', $systemdate[0]->systemDate)->get();
                            foreach ($students as $key => $value) {
                                switch($mystudent->getStudentType($students[$key]->id)){
                                    case 'college':
                                        $temp = collegeStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM college_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->semesterID != $semester){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                    case 'seniorhigh':
                                        $temp = SHSStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM s_h_s_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->semesterID != $semester){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                    case 'juniorhigh':
                                        $temp = JHStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM j_h_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->semesterID != $semester){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                    case 'elementary':
                                        $temp = elemStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM elem_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->semesterID != $semester){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                }
                            }
                        }
                    }else{
                        //return "Hello";
                        if($semester == 'all'){
                            $students = student::all();
                            //$students = student::where('created_at', '<=', $systemdate[0]->systemDate)->get();
                            foreach ($students as $key => $value) {
                                switch($mystudent->getStudentType($students[$key]->id)){
                                    case 'college':
                                        $temp = collegeStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM college_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->schoolYearID != $schoolYear){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                    case 'seniorhigh':
                                        $temp = SHSStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM s_h_s_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->schoolYearID != $schoolYear){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                    case 'juniorhigh':
                                        $temp = JHStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM j_h_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->schoolYearID != $schoolYear){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                    case 'elementary':
                                        $temp = elemStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM elem_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->schoolYearID != $schoolYear){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                }
                            }
                        }else{
                             $students = student::all();
                             //$students = student::where('created_at', '<=', $systemdate[0]->systemDate)->get();
                            foreach ($students as $key => $value) {
                                switch($mystudent->getStudentType($students[$key]->id)){
                                    case 'college':
                                        $temp = collegeStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM college_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->schoolYearID != $schoolYear || $temp[0]->semesterID != $semester){
                                                $students->forget($key);
                                            }
                                            //return $temp[0]->schoolYearID.' '.$schoolYear;
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                    case 'seniorhigh':
                                        $temp = SHSStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM s_h_s_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->schoolYearID != $schoolYear || $temp[0]->semesterID != $semester){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                    case 'juniorhigh':
                                        $temp = JHStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM j_h_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->schoolYearID != $schoolYear || $temp[0]->semesterID != $semester){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                    case 'elementary':
                                        $temp = elemStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM elem_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->schoolYearID != $schoolYear || $temp[0]->semesterID != $semester){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                }
                            }
                        }
                    }
                }else{
                    if($schoolYear=='all'){
                        if($semester == 'all'){
                             $students = student::all();
                             //$students = student::where('created_at', '<=', $systemdate[0]->systemDate)->get();
                            foreach ($students as $key => $value) {
                                switch($mystudent->getStudentType($students[$key]->id)){
                                    case 'college':
                                        $temp = collegeStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM college_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->courseID != $courseGradeLevel){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                    case 'seniorhigh':
                                        $temp = SHSStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM s_h_s_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->gradeLevel != $courseGradeLevel){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                    case 'juniorhigh':
                                        $temp = JHStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM j_h_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->gradeLevel != $courseGradeLevel){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                    case 'elementary':
                                        $temp = elemStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM elem_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->gradeLevel != $courseGradeLevel){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                }
                            }
                        }else {
                             $students = student::all();
                             //$students = student::where('created_at', '<=', $systemdate[0]->systemDate)->get();
                            foreach ($students as $key => $value) {
                                switch($mystudent->getStudentType($students[$key]->id)){
                                    case 'college':
                                        $temp = collegeStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM college_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->courseID != $courseGradeLevel || $temp[0]->semesterID != $semester){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                    case 'seniorhigh':
                                        $temp = SHSStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM s_h_s_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->gradeLevel != $courseGradeLevel || $temp[0]->semesterID != $semester){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                    case 'juniorhigh':
                                        $temp = JHStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM j_h_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->gradeLevel != $courseGradeLevel || $temp[0]->semesterID != $semester){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                    case 'elementary':
                                        $temp = elemStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM elem_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->gradeLevel != $courseGradeLevel || $temp[0]->semesterID != $semester){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                }
                            }
                        }
                    }else{
                        if($semester == 'all'){
                            $students = student::all();
                            //$students = student::where('created_at', '<=', $systemdate[0]->systemDate)->get();
                            foreach ($students as $key => $value) {
                                switch($mystudent->getStudentType($students[$key]->id)){
                                    case 'college':
                                        $temp = collegeStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM college_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->courseID != $courseGradeLevel || $temp[0]->schoolYearID != $schoolYear){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                    case 'seniorhigh':
                                        $temp = SHSStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM s_h_s_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->gradeLevel != $courseGradeLevel || $temp[0]->schoolYearID != $schoolYear){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                    case 'juniorhigh':
                                        $temp = JHStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM j_h_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->gradeLevel != $courseGradeLevel || $temp[0]->schoolYearID != $schoolYear){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                    case 'elementary':
                                        $temp = elemStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM elem_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->gradeLevel != $courseGradeLevel || $temp[0]->schoolYearID != $schoolYear){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                }
                            }
                        }else {
                             $students = student::all();
                             //$students = student::where('created_at', '<=', $systemdate[0]->systemDate)->get();
                            foreach ($students as $key => $value) {
                                switch($mystudent->getStudentType($students[$key]->id)){
                                    case 'college':
                                        $temp = collegeStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM college_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->courseID != $courseGradeLevel || $temp[0]->schoolYearID != $schoolYear || $temp[0]->semesterID != $semester){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                    case 'seniorhigh':
                                        $temp = SHSStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM s_h_s_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->gradeLevel != $courseGradeLevel || $temp[0]->schoolYearID != $schoolYear || $temp[0]->semesterID != $semester){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                    case 'juniorhigh':
                                        $temp = JHStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM j_h_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->gradeLevel != $courseGradeLevel || $temp[0]->schoolYearID != $schoolYear || $temp[0]->semesterID != $semester){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                    case 'elementary':
                                        $temp = elemStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM elem_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                        if(count($temp)>0){
                                            if($temp[0]->gradeLevel != $courseGradeLevel || $temp[0]->schoolYearID != $schoolYear || $temp[0]->semesterID != $semester){
                                                $students->forget($key);
                                            }
                                        }else{
                                            $students->forget($key);
                                        }
                                    break;
                                }
                            }
                        }
                    }
                } 
            break;
            case 'college':
                if($courseGradeLevel=='all'){
                    if($schoolYear=='all'){
                        if($semester == 'all'){
                             $students = student::all();
                             //$students = student::where('created_at', '<=', $systemdate[0]->systemDate)->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='college'){
                                    $students->forget($key);
                                }
                            }
                        }else{
                             $students = student::all();
                             //$students = student::where('created_at', '<=', $systemdate[0]->systemDate)->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='college'){
                                    $students->forget($key);
                                }else{
                                    $temp = collegeStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM college_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->semesterID != $semester){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }
                    }else{
                        if($semester == 'all'){
                             $students = student::all();
                             //$students = student::where('created_at', '<=', $systemdate[0]->systemDate)->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='college'){
                                    $students->forget($key);
                                }else{
                                    $temp = collegeStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM college_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->schoolYearID != $schoolYear){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }else {
                             $students = student::all();
                             //$students = student::where('created_at', '<=', $systemdate[0]->systemDate)->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='college'){
                                    $students->forget($key);
                                }else{
                                    $temp = collegeStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM college_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->schoolYearID != $schoolYear || $temp[0]->semesterID != $semester){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }
                    }
                }else{
                    if($schoolYear=='all'){
                        if($semester == 'all'){
                             $students = student::all();
                             //$students = student::where('created_at', '<=', $systemdate[0]->systemDate)->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='college'){
                                    $students->forget($key);
                                }else{
                                    $temp = collegeStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM college_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->courseID != $courseGradeLevel){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }else {
                             $students = student::all();
                             //$students = student::where('created_at', '<=', $systemdate[0]->systemDate)->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='college'){
                                    $students->forget($key);
                                }else{
                                    $temp = collegeStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM college_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->courseID != $courseGradeLevel || $temp[0]->semesterID != $semester){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }
                    }else{
                        if($semester == 'all'){
                             $students = student::all();
                             //$students = student::where('created_at', '<=', $systemdate[0]->systemDate)->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='college'){
                                    $students->forget($key);
                                }else{
                                    $temp = collegeStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM college_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->courseID != $courseGradeLevel || $temp[0]->schoolYearID != $schoolYear){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }else {
                             $students = student::all();
                             //$students = student::where('created_at', '<=', $systemdate[0]->systemDate)->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='college'){
                                    $students->forget($key);
                                }else{
                                    $temp = collegeStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM college_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->courseID != $courseGradeLevel || $temp[0]->schoolYearID != $schoolYear || $temp[0]->semesterID != $semester){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }
                    }
                } 
            break;
            case 'seniorhigh':
                if($courseGradeLevel=='all'){
                    if($schoolYear=='all'){
                        if($semester == 'all'){
                             $students = student::all();
                             //$students = student::where('created_at', '<=', $systemdate[0]->systemDate)->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='seniorhigh'){
                                    $students->forget($key);
                                }
                            }
                        }else {
                             $students = student::all();
                             //$students = student::where('created_at', '<=', $systemdate[0]->systemDate)->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='seniorhigh'){
                                    $students->forget($key);
                                }else{
                                    $temp = SHSStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM s_h_s_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->semesterID != $semester){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }
                    }else{
                        if($semester == 'all'){
                             $students = student::all();
                             //$students = student::where('created_at', '<=', $systemdate[0]->systemDate)->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='seniorhigh'){
                                    $students->forget($key);
                                }else{
                                    $temp = SHSStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM s_h_s_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->schoolYearID != $schoolYear){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }else {
                             $students = student::all();
                             //$students = student::where('created_at', '<=', $systemdate[0]->systemDate)->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='seniorhigh'){
                                    $students->forget($key);
                                }else{
                                    $temp = SHSStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM s_h_s_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->schoolYearID != $schoolYear || $temp[0]->semesterID != $semester){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }
                    }
                }else{ 
                    if($schoolYear=='all'){
                        if($semester == 'all'){
                             $students = student::all();
                             //$students = student::where('created_at', '<=', $systemdate[0]->systemDate)->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='seniorhigh'){
                                    $students->forget($key);
                                }else{
                                    $temp = SHSStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM s_h_s_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->gradeLevel != $courseGradeLevel){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }else {
                             $students = student::all();
                             //$students = student::where('created_at', '<=', $systemdate[0]->systemDate)->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='seniorhigh'){
                                    $students->forget($key);
                                }else{
                                    $temp = SHSStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM s_h_s_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->gradeLevel != $courseGradeLevel || $temp[0]->semesterID != $semester){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }
                    }else{
                        if($semester == 'all'){
                             $students = student::all();
                             //$students = student::where('created_at', '<=', $systemdate[0]->systemDate)->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='seniorhigh'){
                                    $students->forget($key);
                                }else{
                                    $temp = SHSStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM s_h_s_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->gradeLevel != $courseGradeLevel || $temp[0]->schoolYearID != $schoolYear){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }else {
                             $students = student::all();
                             //$students = student::where('created_at', '<=', $systemdate[0]->systemDate)->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='seniorhigh'){
                                    $students->forget($key);
                                }else{
                                    $temp = SHSStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM s_h_s_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->gradeLevel != $courseGradeLevel || $temp[0]->schoolYearID != $schoolYear || $temp[0]->semesterID != $semester){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }
                    }
                } 
            break;
            case 'juniorhigh':
                if($courseGradeLevel=='all'){
                    if($schoolYear=='all'){
                        if($semester == 'all'){
                             $students = student::all();
                             //$students = student::where('created_at', '<=', $systemdate[0]->systemDate)->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='juniorhigh'){
                                    $students->forget($key);
                                }
                            }
                        }else {
                             $students = student::all();
                             //$students = student::where('created_at', '<=', $systemdate[0]->systemDate)->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='juniorhigh'){
                                    $students->forget($key);
                                }else{
                                    $temp = JHStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM j_h_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->semesterID != $semester){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }
                    }else{
                        if($semester == 'all'){
                             $students = student::all();
                             //$students = student::where('created_at', '<=', $systemdate[0]->systemDate)->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='juniorhigh'){
                                    $students->forget($key);
                                }else{
                                    $temp = JHStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM j_h_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->schoolYearID != $schoolYear){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }else {
                             $students = student::all();
                             //$students = student::where('created_at', '<=', $systemdate[0]->systemDate)->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='juniorhigh'){
                                    $students->forget($key);
                                }else{
                                    $temp = JHStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM j_h_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->schoolYearID != $schoolYear || $temp[0]->semesterID != $semester){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }
                    }
                }else{
                    if($schoolYear=='all'){
                        if($semester == 'all'){
                             $students = student::all();
                             //$students = student::where('created_at', '<=', $systemdate[0]->systemDate)->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='juniorhigh'){
                                    $students->forget($key);
                                }else{
                                    $temp = JHStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM j_h_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->gradeLevel != $courseGradeLevel){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }else {
                             $students = student::all();
                             //$students = student::where('created_at', '<=', $systemdate[0]->systemDate)->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='juniorhigh'){
                                    $students->forget($key);
                                }else{
                                    $temp = JHStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM j_h_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->gradeLevel != $courseGradeLevel || $temp[0]->semesterID != $semester){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }
                    }else{
                        if($semester == 'all'){
                             $students = student::all();
                             //$students = student::where('created_at', '<=', $systemdate[0]->systemDate)->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='juniorhigh'){
                                    $students->forget($key);
                                }else{
                                    $temp = JHStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM j_h_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->gradeLevel != $courseGradeLevel || $temp[0]->schoolYearID != $schoolYear){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }else {
                             $students = student::all();
                             //$students = student::where('created_at', '<=', $systemdate[0]->systemDate)->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='juniorhigh'){
                                    $students->forget($key);
                                }else{
                                    $temp = JHStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM j_h_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->gradeLevel != $courseGradeLevel || $temp[0]->schoolYearID != $schoolYear || $temp[0]->semesterID != $semester){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }
                    }
                }   
            break;
            case 'elementary':
                if($courseGradeLevel=='all'){
                    if($schoolYear=='all'){
                        if($semester == 'all'){
                             $students = student::all();
                             //$students = student::where('created_at', '<=', $systemdate[0]->systemDate)->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='elementary'){
                                    $students->forget($key);
                                }
                            }
                        }else {
                             $students = student::all();
                             //$students = student::where('created_at', '<=', $systemdate[0]->systemDate)->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='elementary'){
                                    $students->forget($key);
                                }else{
                                    $temp = elemStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM elem_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->semesterID != $semester){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }
                    }else{
                        if($semester == 'all'){
                             $students = student::all();
                             //$students = student::where('created_at', '<=', $systemdate[0]->systemDate)->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='elementary'){
                                    $students->forget($key);
                                }else{
                                    $temp = elemStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM elem_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->schoolYearID != $schoolYear){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }else {
                             $students = student::all();
                             //$students = student::where('created_at', '<=', $systemdate[0]->systemDate)->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='elementary'){
                                    $students->forget($key);
                                }else{
                                    $temp = elemStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM elem_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->schoolYearID != $schoolYear || $temp[0]->semesterID != $semester){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }
                    }
                }else{
                    if($schoolYear=='all'){
                        if($semester == 'all'){
                             $students = student::all();
                             //$students = student::where('created_at', '<=', $systemdate[0]->systemDate)->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='elementary'){
                                    $students->forget($key);
                                }else{
                                    $temp = elemStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM elem_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->gradeLevel != $courseGradeLevel){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }else {
                             $students = student::all();
                             //$students = student::where('created_at', '<=', $systemdate[0]->systemDate)->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='elementary'){
                                    $students->forget($key);
                                }else{
                                    $temp = elemStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM elem_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->gradeLevel != $courseGradeLevel || $temp[0]->semesterID != $semester){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }
                    }else{
                        if($semester == 'all'){
                             $students = student::all();
                             //$students = student::where('created_at', '<=', $systemdate[0]->systemDate)->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='elementary'){
                                    $students->forget($key);
                                }else{
                                    $temp = elemStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM elem_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->gradeLevel != $courseGradeLevel || $temp[0]->schoolYearID != $schoolYear){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }else {
                             $students = student::all();
                             //$students = student::where('created_at', '<=', $systemdate[0]->systemDate)->get();
                            foreach ($students as $key => $value) {
                                if($mystudent->getStudentType($students[$key]->id)!='elementary'){
                                    $students->forget($key);
                                }else{
                                    $temp = elemStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM elem_students WHERE studentID = ".$students[$key]->id.")"))->get();
                                    if(count($temp)>0){
                                        if($temp[0]->gradeLevel != $courseGradeLevel || $temp[0]->schoolYearID != $schoolYear || $temp[0]->semesterID != $semester){
                                            $students->forget($key);
                                        }
                                    }else{
                                        $students->forget($key);
                                    }
                                }
                            }
                        }
                    }
                }
            break;
            default:
                
            break;
        }
        $html = '';
        if(count($students)==0){
            return "<h2>There are no students found!</h2>";
        }
        $this->studentsCounter = 0;
        foreach ($students as $student) {
            $html .= $this->getStudentRow($student);
        }
        return $html;
    }
    public function getStudentsFaster($viewby, $coursegrade, $gradelevel, $schoolyear, $semester){
        $user = new userController();
        $systemdate = systemDate::all();
        $sql='';
        if(session()->get('user')['userType']=='admin'){
            $sql = "SELECT COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) 
                    semesterID, COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) 
                    schoolYearID, COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) studentType, 
                    COALESCE(col.yearLevel, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) courseGrade, 
                    COALESCE(col.studentNo, shs.studNo, jhs.studNo, elem.studNo) studentNo, students.*,
                    COALESCE(col.courseID, shs.seniorHighStrandID) strandCourse,
                    col.courseName, col.yearLevel,
                    shs.strandName
                    FROM students LEFT JOIN (SELECT college_students.studentID, college_students.yearLevel, 
                    college_students.semesterID, college_students.schoolYearID, college_students.id, 'college' AS studentType, 
                    college_students.courseID, courses.courseName, college_students.studentNo FROM college_students 
                    INNER JOIN (SELECT max(id) as maxid FROM college_students GROUP 
                    BY studentID) cs ON cs.maxid = college_students.id
                    INNER JOIN courses ON courses.id = college_students.courseID) col ON col.studentID = students.id LEFT 
                    JOIN (SELECT s_h_s_students.studentID, s_h_s_students.semesterID, s_h_s_students.schoolYearID, s_h_s_students.id, 'seniorhigh' 
                    AS studentType, s_h_s_students.gradeLevel, s_h_s_students.studNo,  senior_high_strands.strandName, s_h_s_students.seniorHighStrandID
                    FROM s_h_s_students INNER JOIN (SELECT max(id) as maxid FROM s_h_s_students GROUP BY studentID) 
                    shs ON shs.maxid = s_h_s_students.id INNER JOIN senior_high_strands ON senior_high_strands.id = 
                    s_h_s_students.seniorHighStrandID) shs ON shs.studentID = students.id
                    LEFT JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'juniorhigh' AS 
                    studentType, gradeLevel, studNo  FROM j_h_students INNER JOIN (SELECT max(id) as maxid FROM j_h_students 
                    GROUP BY studentID) jhs ON jhs.maxid = j_h_students.id) jhs ON jhs.studentID = students.id LEFT 
                    JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'elementary' AS studentType, 
                    gradeLevel, studNo FROM elem_students INNER JOIN (SELECT max(id) as maxid FROM elem_students GROUP 
                    BY studentID) elem ON elem.maxid = elem_students.id) elem ON elem.studentID = students.id
            WHERE DATE(students.created_at) <= '".$systemdate[0]->systemDate."'";
            switch ($viewby) {
                case 'college':
                    $sql .= " AND COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) = 'college'";
                break;
                case 'seniorhigh':
                    $sql .= " AND COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) = 'seniorhigh'";
                break;
                case 'juniorhigh':
                    $sql .= " AND COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) = 'juniorhigh'";
                break;
                case 'elementary':
                    $sql .= " AND COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) = 'elementary'";
                break;
                default:
                   
                break;
            }
            if($schoolyear!="all"){
                $sql .= " AND COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) = '$schoolyear'";
            }
            if($semester!="all"){
                $sql .= " AND COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) = '$semester'";
            }
            if($gradelevel!="all"){
                $sql .= " AND COALESCE(col.yearLevel, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) = '$gradelevel'";
            }
            if($coursegrade!="all"){
                $sql .= " AND COALESCE(col.courseID, shs.seniorHighStrandID) = '$coursegrade'";
            }
        }else{
            $designation = $user->getUserDesignation(Auth::guard('myuser')->id());
            $sql = "SELECT COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) 
                    semesterID, COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) 
                    schoolYearID, COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) studentType, 
                    COALESCE(col.yearLevel, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) courseGrade, 
                    COALESCE(col.studentNo, shs.studNo, jhs.studNo, elem.studNo) studentNo, students.*,
                    COALESCE(col.courseID, shs.seniorHighStrandID) strandCourse,
                    col.courseName, col.yearLevel,
                    shs.strandName
                    FROM students LEFT JOIN (SELECT college_students.studentID, college_students.yearLevel, 
                    college_students.semesterID, college_students.schoolYearID, college_students.id, 'college' AS studentType, 
                    college_students.courseID, courses.courseName, college_students.studentNo FROM college_students 
                    INNER JOIN (SELECT max(id) as maxid FROM college_students GROUP 
                    BY studentID) cs ON cs.maxid = college_students.id
                    INNER JOIN courses ON courses.id = college_students.courseID) col ON col.studentID = students.id LEFT 
                    JOIN (SELECT s_h_s_students.studentID, s_h_s_students.semesterID, s_h_s_students.schoolYearID, s_h_s_students.id, 'seniorhigh' 
                    AS studentType, s_h_s_students.gradeLevel, s_h_s_students.studNo,  senior_high_strands.strandName, s_h_s_students.seniorHighStrandID
                    FROM s_h_s_students INNER JOIN (SELECT max(id) as maxid FROM s_h_s_students GROUP BY studentID) 
                    shs ON shs.maxid = s_h_s_students.id INNER JOIN senior_high_strands ON senior_high_strands.id = 
                    s_h_s_students.seniorHighStrandID) shs ON shs.studentID = students.id
                    LEFT JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'juniorhigh' AS 
                    studentType, gradeLevel, studNo  FROM j_h_students INNER JOIN (SELECT max(id) as maxid FROM j_h_students 
                    GROUP BY studentID) jhs ON jhs.maxid = j_h_students.id) jhs ON jhs.studentID = students.id LEFT 
                    JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'elementary' AS studentType, 
                    gradeLevel, studNo FROM elem_students INNER JOIN (SELECT max(id) as maxid FROM elem_students GROUP 
                    BY studentID) elem ON elem.maxid = elem_students.id) elem ON elem.studentID = students.id
                    WHERE DATE(students.created_at) <= '".$systemdate[0]->systemDate."'";
            switch($designation[0]){
                case 'college':
                    $sql= "SELECT cs2.courseID AS courseGrade, c2.courseName, cs2.schoolYearID, cs2.studentNo, 
                    cs2.yearLevel, students.*, 'college' AS studentType, cs2.semesterID FROM students 
                    INNER JOIN (SELECT studentID, studentNo,
                    id, courseID, schoolYearID, yearLevel, semesterID FROM college_students  INNER JOIN (SELECT max(id) as maxid 
                    FROM college_students 
                    GROUP BY studentID) cs ON cs.maxid = college_students.id) cs2 ON cs2.studentID = students.id 
                    INNER JOIN (SELECT c.id AS courseID, dep.departmentID, c.courseName FROM courses c INNER JOIN (SELECT
                    id, courseID FROM course_department_hists cd INNER JOIN (SELECT max(id) as maxid FROM 
                    course_department_hists GROUP BY courseID) cd2 ON cd2.maxid = cd.id) cdh ON cdh.courseID = c.id
                    INNER JOIN (SELECT departmentID, id FROM course_department_hists INNER JOIN (SELECT
                    max(id) AS maxid FROM course_department_hists GROUP BY courseID) cdh1 ON cdh1.maxid = 
                    course_department_hists.id) dep ON dep.id =cdh.id) c2 ON c2.courseID = cs2.courseID 
                    INNER JOIN (SELECT departmentID, id, collegeID FROM department_college_hists
                    INNER JOIN (SELECT max(id) as maxid FROM department_college_hists GROUP BY departmentID) 
                    depc ON depc.maxid = department_college_hists.id) depc2 ON depc2.departmentID=c2.departmentID 
                    INNER JOIN (SELECT collegeID, userID, id FROM assignment_colleges INNER JOIN (SELECT max(id) 
                    as maxid FROM assignment_colleges GROUP BY userID) assc ON assc.maxid = assignment_colleges.id) 
                    assc2 ON assc2.collegeID = depc2.collegeID WHERE assc2.userID ='".Auth::guard('myuser')->id()."'
                    AND DATE(students.created_at) <= '".$systemdate[0]->systemDate."'";
                    if($schoolyear != "all"){
                        $sql .= " AND cs2.schoolYearID = '$schoolyear'";
                    }
                    if($semester != "all"){
                        $sql .= " AND cs2.semesterID = '$semester'";
                    }
                    if($coursegrade != "all"){
                        $sql .= " AND cs2.courseID = '$coursegrade'";
                    }
                    if($gradelevel!="all"){
                        $sql .= " AND COALESCE(col.yearLevel, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) = '$gradelevel'";
                    }
                break;
                case 'seniorhigh':
                    $sql .= " AND COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) = 'seniorhigh'
                              AND COALESCE(col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) = '".$designation[1]."'";
                     if($schoolyear != "all"){
                        $sql .= " AND COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) = '$schoolyear'";
                    }
                    if($semester != "all"){
                        $sql .= " AND COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) = '$semester'";
                    }
                break;
                case 'juniorhigh':
                    $sql .= " AND COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) = 'juniorhigh'
                                AND COALESCE(col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) = '".$designation[1]."'";
                    if($schoolyear != "all"){
                        $sql .= " AND COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) = '$schoolyear'";
                    }
                    if($semester != "all"){
                        $sql .= " AND COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) = '$semester'";
                    }
                break;
                case 'elementary':
                    $sql .= " AND COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) = 'elementary'
                                AND COALESCE(col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) = '".$designation[1]."'";
                    if($schoolyear != "all"){
                        $sql .= " AND COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) = '$schoolyear'";
                    }
                    if($semester != "all"){
                        $sql .= " AND COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) = '$semester'";
                    }
                break;
            }
        }
        $sql .= " ORDER BY students.lastName ASC";
        $students = DB::select(DB::raw($sql));
        if(count($students)==0){
            return "<h2>There are no students found.</h2>";
        }else{
            $html = '';
            $this->studentsCounter=0;
            foreach ($students as $student) {
                $this->studentsCounter++;
                $html .= $this->getStudentRowFaster($student);
            }
            return $html;
        }    
    }
    public function getStudentRowFaster($student){
        $gradeLevel = '';
        if($student->courseName == ''){
            $gradeLevel = $student->courseGrade;
        }else{
            $gradeLevel = $student->yearLevel;
        }
        $thiscourse = '';
        if($student->courseName == ''){
            $thiscourse = $student->strandName;
        }else{
            $thiscourse = $student->courseName;
        }
        
        return "<tr id ='students-$student->id' class= 'rowClickable'>
                    <td>$this->studentsCounter</td>
                    <td>".$this->getBeautifiedName($student->firstName, $student->middleName, $student->lastName)."</td>
                    <td>$student->studentNo</td>
                    <td>".$this->getBeautifiedStudentType($student->studentType)."</td>
                    <td>$thiscourse</td>
                    <td>$gradeLevel</td>
                </tr>";
    }
    public function getBeautifiedStudentType($studenttype){
        switch ($studenttype) {
            case 'college':
                return "College";
            break;
            case 'seniorhigh':
                return "Senior High";
            break;
            case 'juniorhigh':
                return "Junior High";
            break;
            case 'elementary':
                return "Elementary";
            break;
            default:
                # code...
                break;
        }
    }
    public function getStudentRow($student){
        $this->studentsCounter = $this->studentsCounter+1;
        $mystudent =  new studentController();
        $studenttype;
        $course='';
        $yearlevel='';
        $studentno='';
        $studenttype = $mystudent->getStudentType($student->id);
        if($studenttype=='college'){
            $tempCollege = collegeStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM college_students WHERE studentID = ".$student->id.")"))->get();
            if(count($tempCollege)>0){
                $course = course::find($tempCollege[0]->courseID)->courseName;
                $yearlevel = $tempCollege[0]->yearLevel;
                $studentno = $tempCollege[0]->studentNo;
            }
        }else{
            switch ($studenttype) {
                case 'seniorhigh':
                    $temp = SHSStudent::join('senior_high_strands', 'senior_high_strands.id', '=', 's_h_s_students.seniorHighStrandID')
                                        ->select('senior_high_strands.strandName', 's_h_s_students.*')
                                        ->where('s_h_s_students.id', '=', DB::raw("(SELECT MAX(id) FROM s_h_s_students WHERE studentID = ".$student->id.")"))->get();
                    // error_log(SHSStudent::join('senior_high_strands', 'senior_high_strands.id', '=', 's_h_s_students.seniorHighStrandID')
                    // ->select('senior_high_strands.strandName', 's_h_s_students.*')
                    // ->where('senior_high_strands.id', '=', DB::raw("(SELECT MAX(id) FROM s_h_s_students WHERE studentID = ".$student->id.")"))->toSql());                    
                    if(count($temp)>0){
                        $yearlevel=$temp[0]->gradeLevel;
                        $studentno = $temp[0]->studNo;
                        $course = $temp[0]->strandName;
                    }
                break;
                case 'juniorhigh':
                    $temp = JHStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM j_h_students WHERE studentID = ".$student->id.")"))->get();
                    if(count($temp)>0){
                        $yearlevel=$temp[0]->gradeLevel;
                        $studentno = $temp[0]->studNo;
                    }
                break;
                case 'elementary':
                    $temp = elemStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM elem_students WHERE studentID = ".$student->id.")"))->get();
                    if(count($temp)>0){
                        $yearlevel=$temp[0]->gradeLevel;
                        $studentno = $temp[0]->studNo;
                    }
                break;
                default:
                    # code...
                    break;
            }
        }
        $level = '';
        switch ($yearlevel) {
            case '1':
                $level .= $yearlevel."st";
            break;
            case '2':
                $level .= $yearlevel."nd";
            break;
            case '3':
                $level .= $yearlevel."rd";
            break;
            case '4':
                $level .= $yearlevel."th";
            break;
            case '5':
                $level .= $yearlevel."th";
            break;
            case '6':
                $level .= $yearlevel."th";
            break;
            case '7':
                $level .= $yearlevel."th";
            break;
            case '8':
                $level .= $yearlevel."th";
            break;
            case '9':
                $level .= $yearlevel."th";
            break;
            case '10':
                $level .= $yearlevel."th";
            break;
            case '11':
                $level .= $yearlevel."th";
            break;
            case '12':
                $level .= $yearlevel."th";
            break;
            default:
                # code...
                break;
        }
        $thisstudenttype = '';
        switch ($studenttype) {
            case 'college':
                $thisstudenttype = 'College';
            break;
            case 'seniorhigh':
                $thisstudenttype = 'Senior High';
            break;
            case 'juniorhigh':
                $thisstudenttype = 'Junior High';
            break;
            case 'elementary':
                $thisstudenttype = 'Elementary';
            break;
            default:
                # code...
                break;
        }
        return "<tr id ='students-$student->id' class= 'rowClickable'>
                    <td>$this->studentsCounter</td>
                    <td>$student->firstName $student->lastName</td>
                    <td>$studentno</td>
                    <td>$thisstudenttype</td>
                    <td>$course</td>
                    <td>$level</td>
                </tr>";
    }
    public function getCoursesCollege(){
        $course =course::all();
        $html ='';
        foreach ($course as $courses) {
            $html .= "<option value ='$courses->id'>$courses->courseName</option>";
        }
        return $html;
    }
    public function getSemesterSchoolYear($schoolyearid){
        if($schoolyearid!="all"){
            $schoolyear = schoolYear::find($schoolyearid);

            if($schoolyear->semesterType=='3'){
                return "<option value ='3'>3</option>
                        <option value ='2'>2</option>
                        <option value ='1'>1</option>";
            }else{
                return "<option value ='2'>2</option>
                        <option value ='1'>1</option>";
            }
        }else{
            return "";
        }
    }
    
    public function reportStudents($studenttype, $courseGradeLevel, $gradelevel, $schoolYear, $semester){
        $coursename='all';
        $myschoolyhear= 'all';
        if($schoolYear!='all'){
            $myschoolyhear = schoolYear::find($schoolYear);
            $myschoolyhear = explode('-', $myschoolyhear->schoolYearStart)[0] ." - ". explode('-', $myschoolyhear->schoolYearEnd)[0];
        }
      
        $thisgradelevel = 'all';
        if($gradelevel!= "all"){
            $thisgradelevel = "Year Level: ".$gradelevel;
        }
        if($studenttype=='college'){
            $coursename = course::find($courseGradeLevel)->courseName;
            
        }elseif($studenttype=='seniorhigh'){
            $coursename = seniorHighStrand::find($courseGradeLevel)->strandName;
        }else{
            if($studenttype != 'all'){
                $coursename='N/A';
            }
        } 
        $html = "
            <html>
                <head>
                    <style>
                        table {
                            font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif;
                            border-collapse: collapse;
                            width: 100%;
                        }
                        
                        table td, table th {
                            border: 1px solid #ddd;
                            padding: 8px;
                        }
                        
                        table tr:nth-child(even){background-color: #f2f2f2;}
                        
                        table tr:hover {background-color: #ddd;}
                        
                        table th {
                            padding-top: 12px;
                            padding-bottom: 12px;
                            text-align: left;
                            background-color: #4CAF50;
                            color: white;
                        }
                        .filterContainer{
                            display:inline-block;
                            vertical-align:top;
                            width:22%;
                        }
                        @page{ 
                            margin-left:20px;
                            margin-right:20px;
                            margin-top:160px;
                        }
                    </style>
                </head>
                <body>
                    ".$this->getAdminSetting()."
                    <div style = 'margin-bottom:20px; margin-top:20px;'>
                        <center><h2 style ='text-decoration: underline;'>List Of Students</h2></center>
                    </div>
                    <div>
                        
                            <div style = 'width:500px; margin: 0 auto;'>
                                <div class='filterContainer'>
                                    <label>Student Type</label>
                                    <h3>".ucfirst($studenttype)."</h3>
                                </div>
                                <div class='filterContainer'>
                                    <label>Course/Strand</label>
                                    <h3>".ucfirst($coursename)."</h3>
                                </div>
                                <div class='filterContainer'>
                                    <label>GradeLevel</label>
                                    <h3>".ucfirst($thisgradelevel)."</h3>
                                </div>
                                <div class='filterContainer'>
                                    <label>School Year</label>
                                    <h3>".ucfirst($myschoolyhear)."</h3>
                                </div>
                                <div class='filterContainer'>
                                    <label>Semester</label>
                                    <h3>".ucfirst($semester)."</h3>
                                </div>
                            </div>
                        
                    </div>
                    <table>
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Name</th>
                                <th>Student No.</th>
                                <th>Student Type</th>
                                <th>Course/Strand</th>
                                <th>Year Level</th>
                            </tr>
                        </thead>
                        <tbody> 
        ";
        $html .= $this->getStudentsFaster($studenttype, $courseGradeLevel, $gradelevel, $schoolYear, $semester);
        $html.="</tbody>
            </table>
            </body>
            </html>";
        PDF::loadHTML($html)->save(public_path("temp/".'report.pdf'));
        //return redirect('temp/companylogo.png');
        return "true";
    }
    public function getBeautifiedName($firstname, $middlename, $lastname){
        $middlename2 = '';
        if($middlename != ''){
            $middlename2 = ucfirst(substr($middlename, 0, 1)).".";
        }
        return $lastname.", ".$firstname." $middlename2";
    }
    public function getStudentUpdateData($studentid){
        //$studentupdate = studentUpdates::where('studentID', '=',$studentid)->get();
        $student = student::find($studentid);
        $student->studentType = (new studentController)->getStudentType($studentid);
        switch ($student->studentType) {
            case 'college':
                $temp = collegeStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM college_students WHERE studentID = ".$studentid.")"))->get();
                if(count($temp)>0){
                    $student->course = course::find($temp[0]->courseID)->courseName;
                }else{
                    $student->course ='';
                }
            break;
            case 'seniorhigh':
                $temp = SHSStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM s_h_s_students WHERE studentID = ".$studentid.")"))->get();
                if(count($temp)>0){
                    $student->course = $temp[0]->gradeLevel;
                }else{
                    $student->course ='';
                }
            break;
            case 'juniorhigh':
                $temp = JHStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM j_h_students WHERE studentID = ".$studentid.")"))->get();
                if(count($temp)>0){
                    $student->course = $temp[0]->gradeLevel;
                }else{
                    $student->course ='';
                }
            break;
            case 'elementary':
                $temp = elemStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM elem_students WHERE studentID = ".$studentid.")"))->get();
                if(count($temp)>0){
                    $student->course = $temp[0]->gradeLevel;
                }else{
                    $student->course ='';
                }    
            break;
            default:
                # code...
                break;
        }
        //$student->course = 
        return $student;
    } 
    public function getStudentUpdates($studentid){
        //return "Hello";
        $student = student::find($studentid);
        $studentupdate = studentUpdates::where('studentID', '=', $studentid)
                                        ->get();
        $html = '';
        //error_log(count($studentupdate));
        if(count($studentupdate)>0){
            
            foreach ($studentupdate as $studentupdates) {
                
                $mystudentupdates = '';
                if($studentupdates->firstName != $student->firstName){
                    //error_log("Hello");
                    $mystudentupdates .= "First name: $studentupdates->firstName <br>";
                }
                if($studentupdates->middleName != $student->middleName){
                    $mystudentupdates .= "Middle name: $studentupdates->middleName <br>";
                }
                if($studentupdates->lastName != $student->lastName){
                    $mystudentupdates .= "Last name: $studentupdates->lastName <br>";
                }
                if($studentupdates->dateOfBirth != $student->dateOfBirth){
                    $mystudentupdates .= "Date Of Birth: $studentupdates->dateOfBirth <br>";
                }
                if($studentupdates->gender != $student->gender){
                    $mystudentupdates .= "Gender: $studentupdates->gender <br>";
                }
                if($studentupdates->status != $student->status){
                    $mystudentupdates .= "Status: $studentupdates->status <br>";
                }
                if($studentupdates->nationality != $student->nationality){
                    $mystudentupdates .= "Nationality: $studentupdates->nationality <br>";
                }
                if($studentupdates->religion != $student->religion){
                    $mystudentupdates .= "Religion: $studentupdates->religion <br>";
                }
                if($studentupdates->ethnicity != $student->ethnicity){
                    $mystudentupdates .= "Ethnicity: $studentupdates->ethnicity <br>";
                }
                if($studentupdates->language != $student->language){
                    $mystudentupdates .= "Language: $studentupdates->language <br>";
                }
                if($studentupdates->contact != $student->contact){
                    $mystudentupdates .= "Contact: $studentupdates->contact <br>";
                }
                if($studentupdates->cityAddress != $student->cityAddress){
                    $mystudentupdates .= "City Address: $studentupdates->cityAddress <br>";
                }
                if($studentupdates->provincialAddress != $student->provincialAddress){
                    $mystudentupdates .= "Provincial Address: $studentupdates->provincialAddress <br>";
                }
                if($mystudentupdates != ""){
                    $html .= "<tr data-value ='$studentupdates->updatedBy'>
                                <td>".explode(' ', $studentupdates->created_at)[0]."</td>
                                <td>$mystudentupdates</td>
                            </tr>";
                }
            }
        }
        $studentupdatetype = studentTypeUpdate::where('studentID', '=', $studentid)
                                                ->leftJoin('courses', 'courses.id', '=', 'student_type_updates.courseName')
                                                ->select('student_type_updates.*', 'courses.courseName AS course')
                                                ->get();
        foreach ($studentupdatetype as $update) {
            $studupdate = '';
            if($update->studentType!= ''){
                $studupdate.=" Student Type:".$update->studentType." <br>";
            }
            if($update->strand!= ''){
                $studupdate.=" Strand: ".$update->strand." <br>";
            }
            if($update->gradeLevel!= ''){
                $studupdate.=" Grade Level:".$update->gradeLevel." <br>";
            }
            if($update->course!= ''){
                $studupdate.=" Course:".$update->course." <br>";
            }
            if($studupdate != ""){
                $html .= "<tr data-value ='$update->updatedBy'>
                            <td>".explode(' ', $update->created_at)[0]."</td>
                            <td>$studupdate</td>
                        </tr>";
            }
        }
        //return $html;
        if($html!=''){
            return $html;
        }else{
            return "<h2>This student is not updated yet!</h2>";
        }
    }
    public function getStudentUpdatesUpdatedBy($studentid){
        $student = student::find($studentid);
        $studentupdate = studentUpdates::where('studentID', '=', $studentid)
                                        ->get();
        if(count($studentupdate)>0){
            $html = '';
            foreach ($studentupdate as $studentupdates) {
                $mystudentupdates = '';
                if($studentupdates->firstName != $student->firstName){
                    //error_log("Hello");
                    $mystudentupdates .= "First name: $studentupdates->firstName <br>";
                }
                if($studentupdates->middleName != $student->middleName){
                    $mystudentupdates .= "Middle name: $studentupdates->middleName <br>";
                }
                if($studentupdates->lastName != $student->lastName){
                    $mystudentupdates .= "Last name: $studentupdates->lastName <br>";
                }
                if($studentupdates->dateOfBirth != $student->dateOfBirth){
                    $mystudentupdates .= "Date Of Birth: $studentupdates->dateOfBirth <br>";
                }
                if($studentupdates->gender != $student->gender){
                    $mystudentupdates .= "Gender: $studentupdates->gender <br>";
                }
                if($studentupdates->status != $student->status){
                    $mystudentupdates .= "Status: $studentupdates->status <br>";
                }
                if($studentupdates->nationality != $student->nationality){
                    $mystudentupdates .= "Nationality: $studentupdates->nationality <br>";
                }
                if($studentupdates->religion != $student->religion){
                    $mystudentupdates .= "Religion: $studentupdates->religion <br>";
                }
                if($studentupdates->ethnicity != $student->ethnicity){
                    $mystudentupdates .= "Ethnicity: $studentupdates->ethnicity <br>";
                }
                if($studentupdates->language != $student->language){
                    $mystudentupdates .= "Language: $studentupdates->language <br>";
                }
                if($studentupdates->contact != $student->contact){
                    $mystudentupdates .= "Contact: $studentupdates->contact <br>";
                }
                if($studentupdates->cityAddress != $student->cityAddress){
                    $mystudentupdates .= "City Address: $studentupdates->cityAddress <br>";
                }
                if($studentupdates->provincialAddress != $student->provincialAddress){
                    $mystudentupdates .= "Provincial Address: $studentupdates->provincialAddress <br>";
                }
                if($mystudentupdates != ""){
                    $html .= "<tr data-value ='$studentupdates->updatedBy'>
                                <td>".explode(' ', $studentupdates->created_at)[0]."</td>
                                <td>$mystudentupdates</td>
                                <td>$studentupdates->updatedBy</td>
                            </tr>";
                }
            }
            return $html;
        }else{
            return "<tr><h2>This student is not updated yet!</h2></tr>";
        }
    }

    public function printStudentUpdates($studentid){
        $student = student::find($studentid);
        $studentupdates = studentUpdates::where('studentID', '=', $studentid)->get();
        $myhtml = ''; 
        if(count($studentupdates)==0){
            $myhtml = "<tr><td colspan = '3'><h2>This student still has no updates!</h2></td></tr>";
        }else{
            $myhtml =$this->getStudentUpdatesUpdatedBy($studentid);
        }
        $studentobj = new studentController();
        $studenttype = $studentobj->getStudentType($studentid);
        $studentno = $studentobj->getStudentNo2($studentid, $studenttype);
        $studentGradeLabel = "Course and Grade Level :";
        $thistudenttype =$studenttype;
        if($studenttype != "college"){
            $studentGradeLabel= "Grade Level :";
            switch ($studenttype) {
                case 'seniorhigh':
                    $thistudenttype = "Senior High";
                break;
                case 'juniorhigh':
                    $thistudenttype = "Junior High";
                break;
                case 'elementary':
                    $thistudenttype = "Elementary";
                break;
                default:
                    # code...
                    break;
            }
        }
        $gradelevel = '';
        switch($studenttype){
            case 'college':
                $temp = DB::select(DB::raw("SELECT college_students.studentNo, college_students.yearLevel,
                                    courses.courseName FROM college_students
                                    INNER JOIN courses ON courses.id  = college_students.courseID WHERE college_students.id 
                                = (SELECT max(id) FROM college_students WHERE studentID = '$studentid')"));
                
                $gradelevel = $temp[0]->yearLevel." ".$temp[0]->yearLevel;
            break;
            case 'seniorhigh':
                $temp = DB::select(DB::raw("SELECT studNo, gradeLevel FROM s_h_s_students WHERE id 
                    = (SELECT max(id) FROM s_h_s_students WHERE studentID = '$studentid')"));
                $gradelevel =  $temp[0]->gradeLevel;
            break;
            case 'juniorhigh':
                $temp = DB::select(DB::raw("SELECT studNo, gradeLevel FROM j_h_students WHERE id 
                        = (SELECT max(id) FROM j_h_students WHERE studentID = '$studentid')"));
                $gradelevel =  $temp[0]->gradeLevel;
            break;
            case 'elementary':
                $temp = DB::select(DB::raw("SELECT studNo, gradeLevel FROM elem_students WHERE id 
                        = (SELECT max(id) FROM elem_students WHERE studentID = '$studentid')"));
                $gradelevel =  $temp[0]->gradeLevel;
            break;
        }
        $temp = $this->getStudentBasicData($studentid);
        $html = "
            <html>
                <head>
                    <style>
                        table {
                            font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif;
                            border-collapse: collapse;
                            width: 100%;
                        }
                        
                        table td, table th {
                            border: 1px solid #ddd;
                            padding: 8px;
                        }
                        
                        table tr:nth-child(even){background-color: #f2f2f2;}
                        
                        table tr:hover {background-color: #ddd;}
                        
                        table th {
                            padding-top: 12px;
                            padding-bottom: 12px;
                            text-align: left;
                            background-color: #4CAF50;
                            color: white;
                        }
                        .filterContainer{
                            display:inline-block;
                            width:22%;
                        }
                        @page{
                            margin-left:20px;
                            margin-right:20px;
                            margin-top:160px;
                        }
                        .studentInfoContainer{
                            display:inline-block;
                            vertical-align:top;
                            width:48%;
                        }
                        .inputtext {
                            border-top-style: none;
                            border-right-style: none;
                            border-bottom-style: solid;
                            border-left-style: none;
                            border-width:2px;
                            width: 99%;
                            border-color:black;
                        }
                    </style>
                </head>
                <body>
                    ".$this->getAdminSetting()."
                    <div style = 'margin-bottom:20px; margin-top:20px;'>
                        <center><h2 style = 'text-decoration:underline;'>Student Updates</h2></center>
                    </div>
                    <div style = 'margin-bottom:20px; margin-top:20px;'>
                        <div class='studentInfoContainer'>
                            <div>
                                <div class='studentInfoContainer'>
                                    <label>Student Name :</label>
                                </div>
                                <div class='studentInfoContainer'>
                                    <input type = 'text' value ='$student->lastName, $student->firstName' class='inputtext'>
                                </div>
                            </div>
                            <div>
                                <div class='studentInfoContainer'>
                                    <label>Student No :</label>
                                </div>
                                <div class='studentInfoContainer'>
                                    <input type = 'text' value ='$studentno' class='inputtext'>
                                </div>
                            </div>
                            <div>
                                <div class='studentInfoContainer'>
                                    <label>Gender :</label>
                                </div>
                                <div class='studentInfoContainer'>
                                    <input type = 'text' value ='".ucfirst($student->gender)."' class='inputtext'>
                                </div>
                            </div>
                        </div>
                        <div class='studentInfoContainer'>
                            <div>
                                <div class='studentInfoContainer'>
                                    <label>Student Type :</label>
                                </div>
                                <div class='studentInfoContainer'>
                                    <input type = 'text' value ='".ucfirst($thistudenttype)."' class='inputtext'>
                                </div>
                            </div>
                            <div>
                                <div class='studentInfoContainer'>
                                    <label>$studentGradeLabel</label>
                                </div>
                                <div class='studentInfoContainer'>
                                    <input type = 'text' value ='$gradelevel' class='inputtext'>
                                </div>
                            </div>
                            <div>
                                <div class='studentInfoContainer'>
                                    <label>Address :</label>
                                </div>
                                <div class='studentInfoContainer'>
                                    <input type = 'text' value ='".ucfirst($student->cityAddress)."' class='inputtext'>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <table>
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Columns Updated</th>
                                <th>Updated By</th>
                            </tr>
                        </thead>
                        <tbody> 
        ";
        $html .= $myhtml;
        $html.="</tbody>
            </table>
            </body>
            </html>";
        //PDF::setPaper('A4', 'landscape');
        PDF::loadHTML($html)->save(public_path("temp/".'report.pdf'));
        //return redirect('temp/companylogo.png');
        return "true";
    }
    public function getStudentBasicData($studentid){
        // <th>Student No</th>
        // <th>Student Type</th>
        // <th>Course/Strand</th>
        // <th>Grade Level/Year Level</th>
        $temp = new \stdClass();
        $studenttype = (new studentController)->getStudentType($studentid);
        switch ($studenttype) {
            case 'college':
                $temp2 = collegeStudent::where('studentID', '=', $studentid)
                                        ->join('courses','courses.id', 'college_students.courseID')
                                        ->select('courses.courseName', 'college_students.*')
                                        ->get();
                $temp->studentNo = $temp2[0]->studentNo;
                $temp->studentType = 'College';
                $temp->coursestrand = $temp2[0]->courseName;
                $temp->gradeLevel = $this->getBeautifiedGradeLevel($temp2[0]->yearLevel);

            break;
            case 'seniorhigh':
                $temp2 = SHSStudent::where('studentID', '=', $studentid)
                                        ->join('senior_high_strands','senior_high_strands.id', 's_h_s_students.seniorHighStrandID')
                                        ->select('senior_high_strands.strandName', 's_h_s_students.*')
                                        ->get();
                $temp->studentNo = $temp2[0]->studNo;
                $temp->studentType = 'Senior High';
                $temp->coursestrand = $temp2[0]->strandName;
                $temp->gradeLevel = $this->getBeautifiedGradeLevel($temp2[0]->gradeLevel);
            break;
            case 'juniorhigh':
                $temp2 = JHStudent::where('studentID', '=', $studentid)
             
                ->get();
                $temp->studentNo = $temp2[0]->studNo;
                $temp->studentType = 'Junior High';
                $temp->coursestrand = '';
                $temp->gradeLevel = $this->getBeautifiedGradeLevel($temp2[0]->gradeLevel);
            break;
            case 'elementary':
                $temp2 = elemStudent::where('studentID', '=', $studentid)
                
                ->get();
                $temp->studentNo = $temp2[0]->studNo;
                $temp->studentType = 'Junior High';
                $temp->coursestrand = '';
                $temp->gradeLevel = $this->getBeautifiedGradeLevel($temp2[0]->gradeLevel);
            break;
            default:
                # code...
                break;
        }
        return $temp;
    }
    public function getBeautifiedGradeLevel($yearlevel){
        $level = '';
        switch ($yearlevel) {
            case '1':
                $level .= $yearlevel."st";
            break;
            case '2':
                $level .= $yearlevel."nd";
            break;
            case '3':
                $level .= $yearlevel."rd";
            break;
            case '4':
                $level .= $yearlevel."th";
            break;
            case '5':
                $level .= $yearlevel."th";
            break;
            case '6':
                $level .= $yearlevel."th";
            break;
            case '7':
                $level .= $yearlevel."th";
            break;
            case '8':
                $level .= $yearlevel."th";
            break;
            case '9':
                $level .= $yearlevel."th";
            break;
            default:
                $level .= $yearlevel."th";
                break;
        }
        return $level;
    }
    public function getStudentReferral($studentid){
        $referral = referral::join('students', 'students.id', '=', 'referrals.studentID')
                            ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                            ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd', 
                                    'students.firstName', 'students.lastName', 'referrals.*')
                            ->where('students.id', '=', $studentid)
                            ->get();
        if(count($referral)==0){
            return "<h2>This student doesnt have any referrals yet</h2>";
        }else{
            $html = '';
            foreach ($referral as $referrals) {
                $html .= "<tr id ='studentReferral-$referrals->id'>
                            <td>$referrals->referralDate</td>
                            <td>$referrals->referredBy</td>
                            <td>".explode('-', $referrals->schoolYearStart)[0]." - ".explode('-', $referrals->schoolYearEnd)[0]."</td>
                            <td>$referrals->semesterID</td>
                        </tr>";
            }
            return $html;
        }
    }
    public function getBehaviourReferral($referralid){
        $behavior = behaviour::where('referralID', '=', $referralid)->get();
        $referral = referral::find($referralid);
        if(count($behavior)==0){
            $referral->html = "<h2>This referral doesnt have any behaviour</h2>";
            return $referral;
        }else{
            $html = '';
            foreach ($behavior as $behaviors) {
                $html .= "<tr>
                            <td>$behaviors->behavioural</td>
                            <td>$behaviors->frequency</td>
                            <td>$behaviors->remarks</td>
                          </tr>";
            }
            $referral->html = $html;
            return $referral;
        }
    }

    public function printBehaviourReferral($studentid){
        $student = student::find($studentid);
        $referral = referral::join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                            ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd', 'referrals.*')
                            ->where('referrals.studentID', '=', $studentid)->get();
        $myhtml = '';
        
        $studentobj = new studentController();
        $studenttype = $studentobj->getStudentType($studentid);
        $studentno = $studentobj->getStudentNo2($studentid, $studenttype);
        $studentGradeLabel = "Course and Grade Level :";
        $thistudenttype =$studenttype;
        if($studenttype != "college"){
            $studentGradeLabel= "Grade Level :";
            switch ($studenttype) {
                case 'seniorhigh':
                    $thistudenttype = "Senior High";
                break;
                case 'juniorhigh':
                    $thistudenttype = "Junior High";
                break;
                case 'elementary':
                     $thistudenttype = "Elementary";
                break;
                default:
                    # code...
                    break;
            }
        }
        $gradelevel = '';
        switch($studenttype){ 
            case 'college':
                $temp = DB::select(DB::raw("SELECT college_students.studentNo, college_students.yearLevel,
                                    courses.courseName FROM college_students
                                    INNER JOIN courses ON courses.id  = college_students.courseID WHERE 
                                    college_students.id 
                                = (SELECT max(id) FROM college_students WHERE studentID = '$studentid')"));
                
                $gradelevel = $temp[0]->courseName." ".$temp[0]->yearLevel;
            break;
            case 'seniorhigh':
                $temp = DB::select(DB::raw("SELECT studNo, gradeLevel FROM s_h_s_students WHERE id 
                     = (SELECT max(id) FROM s_h_s_students WHERE studentID = '$studentid')"));
                $gradelevel =  $temp[0]->gradeLevel;
            break;
            case 'juniorhigh':
                $temp = DB::select(DB::raw("SELECT studNo, gradeLevel FROM j_h_students WHERE id 
                        = (SELECT max(id) FROM j_h_students WHERE studentID = '$studentid')"));
                 $gradelevel =  $temp[0]->gradeLevel;
            break;
            case 'elementary':
                $temp = DB::select(DB::raw("SELECT studNo, gradeLevel FROM elem_students WHERE id 
                        = (SELECT max(id) FROM elem_students WHERE studentID = '$studentid')"));
                 $gradelevel =  $temp[0]->gradeLevel;
            break;
        }

        if(count($referral)==0){
            $myhtml = "<h2>This student doesn't have any referrals yet.</h2>";
        }else{
            foreach ($referral as $referrals) {
                $myhtml .= "<div style = 'margin-bottom:10px;'>
                                <div class='filterContainer'>
                                    <h3>School Year:</h3>
                                    <div>".explode('-', $referrals->schoolYearStart)[0]." - ".explode('-', $referrals->schoolYearEnd)[0]."</div>
                                </div>
                                <div class='filterContainer'>
                                    <h3>Semester:</h3>
                                    <div>".$this->getBeautifiedGradeLevel($referrals->semesterID)."</div>
                                </div>
                                <div class='filterContainer'>
                                    <h3>Referral Date:</h3>
                                    <div>".date("F j, Y", strtotime($referrals->referralDate))."</div>
                                </div>
                                <div class='filterContainer'>
                                    <h3>Referred By:</h3>
                                    <div>$referrals->referredBy</div>
                                </div>
                                <div class='filterContainer'>
                                    <h3>Referred To:</h3>
                                    <div>$referrals->referredTo</div>
                                </div>
                            </div>";
                $behaviour = behaviour::where('referralID', '=', $referrals->id)->get();
                if(count($behaviour)==0){
                    $myhtml .= "<table>
                                    <thead>
                                        <tr>
                                            <th>Behaviour</th>
                                            <th>Frequency</th>
                                            <th>Remarks</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr><h2>This referral doesnt have any behaviours yet</h2></tr>
                                    </tbody>
                                </table>";
                }else{
                    $myhtml .= "<table>
                                    <thead>
                                        <tr>
                                            <th>Behaviour</th>
                                            <th>Frequency</th>
                                            <th>Remarks</th>
                                        </tr>
                                    </thead>
                                    <tbody>";
                    foreach($behaviour as $behaviours){
                        $myhtml .= "<tr>
                                        <td>$behaviours->behavioural</td>
                                        <td>$behaviours->frequency</td>
                                        <td>$behaviours->remarks</td>
                                    </tr>";
                    }
                    $myhtml .= "</tbody>
                    </table>";
                }
            }
        }
        $html = "
            <html>
                <head>
                    <style>
                        table {
                            font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif;
                            border-collapse: collapse;
                            width: 100%;
                        }
                        
                        table td, table th {
                            border: 1px solid #ddd;
                            padding: 8px;
                        }
                        
                        table tr:nth-child(even){background-color: #f2f2f2;}
                        
                        table tr:hover {background-color: #ddd;}
                        
                        table th {
                            padding-top: 12px;
                            padding-bottom: 12px;
                            text-align: left;
                            background-color: #4CAF50;
                            color: white;
                        }
                        .filterContainer{
                            display:inline-block;
                            width:18%;
                        }
                        @page{
                            margin-left:20px;
                            margin-right:20px;
                            margin-top:160px;
                        }
                        .studentInfoContainer{
                            display:inline-block;
                            vertical-align:top;
                            width:48%;
                        }
                        .inputtext {
                            border-top-style: none;
                            border-right-style: none;
                            border-bottom-style: solid;
                            border-left-style: none;
                            border-width:2px;
                            width: 99%;
                            border-color:black;
                        }
                        
                    </style>
                </head>
                <body>
                    ".$this->getAdminSetting()."
                    <div style = 'margin-bottom:20px; margin-top:20px;'>
                        <center><h2 style = 'text-decoration:underline;'>Referral Transaction</h2></center>
                    </div>
                    <div style = 'margin-bottom:20px; margin-top:20px;'>
                        <div class='studentInfoContainer'>
                            <div>
                                <div class='studentInfoContainer'>
                                    <label>Student Name :</label>
                                </div>
                                <div class='studentInfoContainer'>
                                    <input type = 'text' value ='$student->lastName, $student->firstName' class='inputtext'>
                                </div>
                            </div>
                            <div>
                                <div class='studentInfoContainer'>
                                    <label>Student No :</label>
                                </div>
                                <div class='studentInfoContainer'>
                                    <input type = 'text' value ='$studentno' class='inputtext'>
                                </div>
                            </div>
                            <div>
                                <div class='studentInfoContainer'>
                                    <label>Gender :</label>
                                </div>
                                <div class='studentInfoContainer'>
                                    <input type = 'text' value ='".ucfirst($student->gender)."' class='inputtext'>
                                </div>
                            </div>
                        </div>
                        <div class='studentInfoContainer'>
                            <div>
                                <div class='studentInfoContainer'>
                                    <label>Student Type :</label>
                                </div>
                                <div class='studentInfoContainer'>
                                    <input type = 'text' value ='".ucfirst($thistudenttype)."' class='inputtext'>
                                </div>
                            </div>
                            <div>
                                <div class='studentInfoContainer'>
                                    <label>$studentGradeLabel</label>
                                </div>
                                <div class='studentInfoContainer'>
                                    <input type = 'text' value ='$gradelevel' class='inputtext'>
                                </div>
                            </div>
                            <div>
                                <div class='studentInfoContainer'>
                                    <label>Address :</label>
                                </div>
                                <div class='studentInfoContainer'>
                                    <input type = 'text' value ='".ucfirst($student->cityAddress)."' class='inputtext'>
                                </div>
                            </div>
                        </div>
                    </div>
                     
        ";
        $html .= $myhtml;
        $html.="
            </body>
            </html>";
        
        PDF::loadHTML($html)->save(public_path("temp/".'report.pdf'));
        //return redirect('temp/companylogo.png');
        return "true";
    }

    public function getContractStudent($studentid){
        $studenttype = (new studentController)->getStudentType($studentid);
        if($studenttype!='college'){
            return "<h2>Contracts are for college students only</h2>";
        }
        $temp = collegeStudent::where('studentID', '=', $studentid)->get();
        //$temp = collegeStudent::
        $html = '';
        if(count($temp)>0){
            foreach ($temp as $temps) {
                $contract = contract::where('collegStudentID', '=', $temps->id)->get();
                foreach ($contract as $contracts) {
                    $html .= "<tr id ='contractStudent-$contracts->id'>
                                <td>$contracts->contractDate</td>
                                <td>".ucfirst($contracts->type)."</td>
                                <td>$contracts->remarks</td>
                                <td>$contracts->notedBy</td>
                              </tr>";
                }
            }
            if($html==''){
                return '<h2>No contracts found for this student</h2>';
            }else{
                return $html;
            }
        }else{
            return "<h2>No contracts found for this student</h2>";
        }
    }
    public function getContractBehaviorProof($contractid){
        $contract = contract::find($contractid);
        $grade = contractGrade::where('contractID', '=', $contractid)->get();
        $html = '';
        if(count($grade)==0){
            $contract->gradeHtml = "<h2>This contract doesnt have grades</h2>";
            $contract->averageGrade = 0;
        }else{
            $contract->averageGrade = $this->getAverageGrade($grade);
            foreach ($grade as $grades) {
                $html .= "<tr>
                            <td>$grades->subjectCode</td>
                            <td>$grades->grade</td>
                            <td>$grades->units</td>
                        </tr>";
            }
            $contract->gradeHtml = $html;
            $html = '';
        }
        $proof = contractProof::where('contractID', '=', $contractid)->get();
        if(count($proof)==0){
            $contract->proofHtml = "<h2>This contract doesnt have any proofs yet</h2>";
        }else{
            foreach ($proof as $proofs) {
                $html .= "<div class='imageWrapper' id ='imageDelete-$proofs->id'>
                            <img src='".asset("images-database/contractproof/".$proofs->id.'.'.$proofs->proofPhotoType)."' class='thisImage'/>
                        </div>";
            }
            $contract->proofHtml = $html;
        }
        
        return $contract;
    }
    public function getAverageGrade($grade){
        $totalSum =0;
        $totalunits=0;
        foreach ($grade as $grades) {
            $totalSum =$totalSum+ ($grades->units * $grades->grade);
            $totalunits += $grades->units;
        }
        return $totalSum/$totalunits;
    }

    public function printContractStudent($studentid){
       
        $myhtml ='';
        $student = student::find($studentid);
        //$studenttype = (new studentController)->getStudentType($studentid);

        $studentobj = new studentController();
        $studenttype = $studentobj->getStudentType($studentid);
        $studentno = $studentobj->getStudentNo2($studentid, $studenttype);
        $studentGradeLabel = "Course and Grade Level :";
        $thistudenttype =$studenttype;
        if($studenttype != "college"){
            $studentGradeLabel= "Grade Level :";
            switch ($studenttype) {
                case 'seniorhigh':
                    $thistudenttype = "Senior High";
                break;
                case 'juniorhigh':
                    $thistudenttype = "Junior High";
                break;
                case 'elementary':
                     $thistudenttype = "Elementary";
                break;
                default:
                    # code...
                    break;
            }
        }
        $gradelevel = '';
        switch($studenttype){
            case 'college':
                $temp = DB::select(DB::raw("SELECT college_students.studentNo, college_students.yearLevel,
                                    courses.courseName FROM college_students
                                    INNER JOIN courses ON courses.id  = college_students.courseID WHERE college_students.id 
                                = (SELECT max(id) FROM college_students WHERE studentID = '$studentid')"));
                
                $gradelevel = $temp[0]->courseName." ".$temp[0]->yearLevel;
            break;
            case 'seniorhigh':
                $temp = DB::select(DB::raw("SELECT studNo, gradeLevel FROM s_h_s_students WHERE id 
                    = (SELECT max(id) FROM s_h_s_students WHERE studentID = '$studentid')"));
                $gradelevel =  $temp[0]->gradeLevel;
            break;
            case 'juniorhigh':
                $temp = DB::select(DB::raw("SELECT studNo, gradeLevel FROM j_h_students WHERE id 
                        = (SELECT max(id) FROM j_h_students WHERE studentID = '$studentid')"));
                $gradelevel =  $temp[0]->gradeLevel;
            break;
            case 'elementary':
                $temp = DB::select(DB::raw("SELECT studNo, gradeLevel FROM elem_students WHERE id 
                        = (SELECT max(id) FROM elem_students WHERE studentID = '$studentid')"));
                $gradelevel =  $temp[0]->gradeLevel;
            break;
        }

        if($studenttype!='college'){
            $myhtml = "<h2>Contracts are for college students only</h2>";
        }else{
            $temp = collegeStudent::where('studentID', '=', $studentid)->get();
            //$html = '';
            if(count($temp)>0){
                foreach ($temp as $temps) {
                    $contract = contract::where('collegStudentID', '=', $temps->id)->get();
                    foreach ($contract as $contracts) {
                        $myhtml .="<div style = 'margin-bottom:10px;'>
                                        <div class='filterContainer'>
                                            <h3>Type:</h3>
                                            <div>".ucfirst($contracts->type)."</div>
                                        </div>
                                        <div class='filterContainer'>
                                            <h3>Remarks:</h3>
                                            <div>$contracts->remarks</div>
                                        </div>
                                        <div class='filterContainer'>
                                            <h3>Noted By:</h3>
                                            <div>$contracts->notedBy</div>
                                        </div>
                                        <div class='filterContainer'>
                                            <h3>Contract Date:</h3>
                                            <div>$contracts->contractDate</div>
                                        </div>
                                    </div>";
                        $myhtml .= "<table>
                                        <thead>
                                            <tr>
                                                <th>Subject Code</th>
                                                <th>Grade</th>
                                                <th>Unit</th>
                                            </tr>
                                        </thead>
                                        <tbody>";
                        $grades = contractGrade::where('contractID', '=', $contracts->id)->get();
                        if(count($grades)==0){
                            $myhtml .= "<tr><h2>This Contract doesnt have subject grades contract.</h2></tr>";
                        }else{
                            foreach ($grades as $grade) {
                                $myhtml .= "<tr>
                                                <td>$grade->subjectCode</td>
                                                <td>$grade->grade</td>
                                                <td>$grade->units</td>
                                            </tr>";
                            }
                        }
                        $myhtml .="</tbody></table>";
                        
                        $proof = contractProof::where('contractID', '=',  $contracts->id)->get();
                        if(count($proof)==0){
                            $myhtml .= "<div style ='margin-top:10px; height:50'>";
                        }elseif(count($proof)<=5){
                            $myhtml .= "<div style ='margin-top:10px; height:200'>";
                        }elseif (count($proof)<=10) {
                            $myhtml .= "<div style ='margin-top:10px; height:400'>";
                        }elseif (count($proof)<=15){
                            $myhtml .= "<div style ='margin-top:10px; height:600'>";
                        }
                        
                        if(count($proof)==0){
                            $myhtml .= "<h2>This Contract doesnt have any proofs.</h2>";
                        }else{
                            foreach ($proof as $proofs) {
                                $myhtml .= "<div class ='imageWrapper'>
                                                <img src='images-database/contractproof/".$proofs->id.'.'.$proofs->proofPhotoType."' class='thisImage'/>
                                            </div>";
                            }
                        }
                        $myhtml .= "</div>";
                    }
                }
            }else{
                $myhtml =  "<h2>No contracts found for this student</h2>";
            }
        }
       
       
        $html = "
            <html>
                <head>
                    <style>
                        table {
                            font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif;
                            border-collapse: collapse;
                            width: 100%;
                        }
                        
                        table td, table th {
                            border: 1px solid #ddd;
                            padding: 8px;
                        }
                        
                        table tr:nth-child(even){background-color: #f2f2f2;}
                        
                        table tr:hover {background-color: #ddd;}
                        
                        table th {
                            padding-top: 12px;
                            padding-bottom: 12px;
                            text-align: left;
                            background-color: #4CAF50;
                            color: white;
                        }
                        .filterContainer{
                            display:inline-block;
                            width:23%;
                            
                        }
                        @page{
                            margin-left:20px;
                            margin-right:20px;
                            margin-top:160px;
                        }
                        .imageWrapper {
                            float:left;
                        
                        }
                        .thisImage{
                        height:200px;
                        width:150px;
                        }
                        .studentInfoContainer{
                            display:inline-block;
                            vertical-align:top;
                            width:48%;
                        }
                        .inputtext {
                            border-top-style: none;
                            border-right-style: none;
                            border-bottom-style: solid;
                            border-left-style: none;
                            border-width:2px;
                            width: 99%;
                            border-color:black;
                        }
                    </style>
                </head>
                <body>
                    ".$this->getAdminSetting()."
                    <div style = 'margin-bottom:20px; margin-top:20px;'>
                        <center><h2 style = 'text-decoration:underline;'>Contract Transaction</h2></center>
                    </div>
                    <div style = 'margin-bottom:20px; margin-top:20px;'>
                        <div class='studentInfoContainer'>
                            <div>
                                <div class='studentInfoContainer'>
                                    <label>Student Name :</label>
                                </div>
                                <div class='studentInfoContainer'>
                                    <input type = 'text' value ='$student->lastName, $student->firstName' class='inputtext'>
                                </div>
                            </div>
                            <div>
                                <div class='studentInfoContainer'>
                                    <label>Student No :</label>
                                </div>
                                <div class='studentInfoContainer'>
                                    <input type = 'text' value ='$studentno' class='inputtext'>
                                </div>
                            </div>
                            <div>
                                <div class='studentInfoContainer'>
                                    <label>Gender :</label>
                                </div>
                                <div class='studentInfoContainer'>
                                    <input type = 'text' value ='".ucfirst($student->gender)."' class='inputtext'>
                                </div>
                            </div>
                        </div>
                        <div class='studentInfoContainer'>
                            <div>
                                <div class='studentInfoContainer'>
                                    <label>Student Type :</label>
                                </div>
                                <div class='studentInfoContainer'>
                                    <input type = 'text' value ='".ucfirst($thistudenttype)."' class='inputtext'>
                                </div>
                            </div>
                            <div>
                                <div class='studentInfoContainer'>
                                    <label>$studentGradeLabel</label>
                                </div>
                                <div class='studentInfoContainer'>
                                    <input type = 'text' value ='$gradelevel' class='inputtext'>
                                </div>
                            </div>
                            <div>
                                <div class='studentInfoContainer'>
                                    <label>Address :</label>
                                </div>
                                <div class='studentInfoContainer'>
                                    <input type = 'text' value ='".ucfirst($student->cityAddress)."' class='inputtext'>
                                </div>
                            </div>
                        </div>
                    </div>
                     
        ";
        $html .= $myhtml;
        $html.="
            </body>
            </html>";
        
        PDF::loadHTML($html)->save(public_path("temp/".'report.pdf'));
        //return redirect('temp/companylogo.png');
        return "true";
        //return $html;
    }

    public function getCounselingOfStudent($studentid){
        $counseling  = counseling::where('studentID', '=', $studentid)->get();
        $counselinggroup = groupCounseling::where('studentID', '=', $studentid)->get();
        if(count($counselinggroup)==0 && count($counseling) == 0){
            return "<h2>This student doesnt have any counselings yet</h2>";
        }

        $html ='';
        foreach ($counseling as $counselings) {
            $status = '';
        
            switch ($counselings->counselingStatus) {
                case '0':
                    $status = 'pending';
                break;
                case '1':
                    $status = 'followed up';
                break;
                case '2':
                    $status = 'evaluated';
                break;
                default:
                    # code...
                    break;
            }
            $problem ='';
            if(strlen($counselings->statementOfTheProblem)>=20){
                $problem = substr($counselings->statementOfTheProblem,0,20)."...";
            }else{
                $problem = $counselings->statementOfTheProblem;
            }
            $html .= "<tr id ='counselingStudent-$counselings->id'>
                        <td>$counselings->dateRecorded</td>
                        <td>$counselings->dateScheduled</td>
                        <td>".explode('-', schoolYear::find($counselings->schoolYearID)->schoolYearStart)[0]." -
                         ".explode('-', schoolYear::find($counselings->schoolYearID)->schoolYearEnd)[0]."</td>
                        <td>$counselings->semesterID</td>
                        <td>".ucfirst($counselings->counselingType)."</td>
                        <td>$counselings->assistedBy</td>
                        <td>".ucfirst($status)."</td>
                        <td>$problem</td>
                        <td>$counselings->counseledBy</td>
                    </tr>";
        }
        foreach ($counselinggroup as $counselinggroups) {
            $counseling = counseling::find($counselinggroups->counselingID);
            $status = '';
        
            switch ($counseling->counselingStatus) {
                case '0':
                    $status = 'pending';
                break;
                case '1':
                    $status = 'followed up';
                break;
                case '2':
                    $status = 'evaluated';
                break;
                default:
                    # code...
                    break;
            }
            $html .= "<tr id ='counselingStudent-$counseling->id'>
                        <td>$counseling->dateRecorded</td>
                        <td>$counseling->dateScheduled</td>
                        <td>".explode('-', schoolYear::find($counseling->schoolYearID)->schoolYearStart)[0]." -
                         ".explode('-', schoolYear::find($counseling->schoolYearID)->schoolYearEnd)[0]."</td>
                        <td>$counseling->semesterID</td>
                        <td>".ucfirst($counseling->counselingType)."</td>
                        <td>$counseling->assistedBy</td>
                        <td>".ucfirst($status)."</td>
                        <td>$counseling->statementOfTheProblem</td>
                        <td>$counseling->counseledBy</td>
                    </tr>";
        }
        return $html;        
    }
    public function getFollowUpCounseling($counselingid){
        $counseling = counseling::find($counselingid);
        $followup = followUp::where('counselingID', '=', $counselingid)->get();
        $html ='';
        if(count($followup)==0){
            $html = "<h2>This counseling doesnt have any followup yet!</h2>";
        }else{
            foreach ($followup as $followups) {
                $html .= "<tr>
                            <td>$followups->details</td>
                            <td>$followups->assistedBy</td>
                            <td>".explode(" ", $followups->created_at)[0]."</td>
                        </tr>";
            }    
        }
        $counseling->html = $html;
        $evaluation = evaluation::where('counselingID', '=', $counselingid)->get();
        if(count($evaluation)==0){
            $counseling->evaluationRemarks = '';
            $counseling->evaluationDate = '';
            $counseling->evaluatedBy = '';
        }else{
            $counseling->evaluationRemarks = $evaluation[0]->remarks;
            $counseling->evaluationDate = $evaluation[0]->evaluationDate;
            $counseling->evaluatedBy = $evaluation[0]->evaluatedBy;
        }
        $schoolyear = explode('-', schoolYear::find($counseling->schoolYearID)->schoolYearStart)[0]." - ".explode('-', schoolYear::find($counseling->schoolYearID)->schoolYearEnd)[0];
        $counseling->schoolyear = $schoolyear;
        return $counseling;
    }
    public function printCounselingOfStudent($studentid){
        $student=  student::find($studentid);
        $counseling  = counseling::where('studentID', '=', $studentid)->get();
        $counselinggroup = groupCounseling::where('studentID', '=', $studentid)->get();
        
        $studentobj = new studentController();
        $studenttype = $studentobj->getStudentType($studentid);
        $studentno = $studentobj->getStudentNo2($studentid, $studenttype);
        $studentGradeLabel = "Course and Grade Level :";
        $thistudenttype =$studenttype;
        if($studenttype != "college"){
            $studentGradeLabel= "Grade Level :";
            switch ($studenttype) {
                case 'seniorhigh':
                    $thistudenttype = "Senior High";
                break;
                case 'juniorhigh':
                    $thistudenttype = "Junior High";
                break;
                case 'elementary':
                     $thistudenttype = "Elementary";
                break;
                default:
                    # code...
                    break;
            }
        }
        $gradelevel = '';
        switch($studenttype){
            case 'college':
                $temp = DB::select(DB::raw("SELECT college_students.studentNo, college_students.yearLevel,
                                    courses.courseName FROM college_students
                                    INNER JOIN courses ON courses.id  = college_students.courseID WHERE college_students.id 
                                = (SELECT max(id) FROM college_students WHERE studentID = '$studentid')"));
                
                $gradelevel = $temp[0]->yearLevel." ".$temp[0]->yearLevel;
            break;
            case 'seniorhigh':
                $temp = DB::select(DB::raw("SELECT studNo, gradeLevel FROM s_h_s_students WHERE id 
                    = (SELECT max(id) FROM s_h_s_students WHERE studentID = '$studentid')"));
                $gradelevel =  $temp[0]->gradeLevel;
            break;
            case 'juniorhigh':
                $temp = DB::select(DB::raw("SELECT studNo, gradeLevel FROM j_h_students WHERE id 
                        = (SELECT max(id) FROM j_h_students WHERE studentID = '$studentid')"));
                $gradelevel =  $temp[0]->gradeLevel;
            break;
            case 'elementary':
                $temp = DB::select(DB::raw("SELECT studNo, gradeLevel FROM elem_students WHERE id 
                        = (SELECT max(id) FROM elem_students WHERE studentID = '$studentid')"));
                $gradelevel =  $temp[0]->gradeLevel;
            break;
        }

        $myhtml = '';
        if(count($counseling)==0 && count($counselinggroup)==0){
            $myhtml = "<h2>This student doesn't have any counselings yet</h2>";
        }else{
            foreach ($counseling as $counselings) {
                $status = '';
        
                switch ($counselings->counselingStatus) {
                    case '0':
                        $status = 'pending';
                    break;
                    case '1':
                        $status = 'followed up';
                    break;
                    case '2':
                        $status = 'evaluated';
                    break;
                    default:
                        # code...
                        break;
                }
                
                $problem = $counselings->statementOfTheProblem;
                
                $htmrow = "<tr id ='counselingStudent-$counselings->id'>
                            <td>".date("F j, Y", strtotime($counselings->dateRecorded))."</td>
                            <td>".date("F j, Y", strtotime($counselings->dateScheduled))."</td>
                            <td>".explode('-', schoolYear::find($counselings->schoolYearID)->schoolYearStart)[0]." -
                            ".explode('-', schoolYear::find($counselings->schoolYearID)->schoolYearEnd)[0]."</td>
                            <td>".$this->getBeautifiedGradeLevel($counselings->semesterID)."</td>
                            <td>".ucfirst($counselings->counselingType)."</td>
                            <td>$counselings->assistedBy</td>
                            <td>".ucfirst($status)."</td>
                            <td>$problem</td>
                        </tr>";
                $myhtml .= "<div class='well'>
                                <h3>Counseling</h3>
                                <table>
                                    <thead>
                                        <tr>
                                            <th>Counseling Date</th>
                                            <th>Date Scheduled</th>
                                            <th>School Year</th>
                                            <th>Semester</th>
                                            <th>Counseling Type</th>
                                            <th>Assisted By</th>
                                            <th>Status</th>
                                            <th>Statement of the problem</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        ".$htmrow."
                                    </tbody>
                                </table>
                                <h3>Counseling Followup</h3>";
                $myhtml .= "<table>
                                <thead>
                                    <tr>
                                    <th>Details</th>
                                    <th>Followed up by</th>
                                    <th>Follow up date</th>
                                    </tr>
                                </thead>
                                <tbody>";   
                $followuphtml ='';
                $followuprow = followUp::where('counselingID', '=', $counselings->id)->get();
                foreach ($followuprow as $followuprows) {
                    $followuphtml .= "<tr>
                                        <td>$followuprows->details</td>
                                        <td>$followuprows->assistedBy</td>
                                        <td>".date("F j, Y", strtotime(explode(' ', $followuprows->created_at)[0]))."</td>
                                    </tr>";
                }         
                $myhtml .=      $followuphtml."
                                </tbody>
                                </table>";
                
                $myhtml .= "<h3>Counseling Evaluation</h3>";
                $myhtml .= "<table>
                                <thead>
                                <tr>
                                    <th>Remarks</th>
                                    <th>Evaluated by</th>
                                    <th>Evaluation date</th>
                                    </tr>
                                    </thead>
                                <tbody>";   
                $evaluationhtml = '';
                $evaluationrow = evaluation::where('counselingID', '=', $counselings->id)->get();
                foreach ($evaluationrow as $evaluationrows) {
                    $evaluationhtml .= "<tr>
                                            <td>$evaluationrows->remarks</td>
                                            <td>$evaluationrows->evaluatedBy</td>
                                            <td>".date("F j, Y", strtotime($evaluationrows->evaluationDate))."</td>
                                        </tr>";
                }
                $myhtml .= $evaluationhtml."</tbody></table>";
                $myhtml .= "</div>";
            }
        }
        
        $html = "
            <html>
                <head>
                    <style>
                        table {
                            font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif;
                            border-collapse: collapse;
                            width: 100%;
                        }
                        .well{
                            min-height: 20px;
                            padding: 19px;
                            margin-bottom: 20px;
                            background-color: #f5f5f5;
                            border: 1px solid #e3e3e3;
                            border-radius: 4px;
                            -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
                            box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
                        }
                        table td, table th {
                            border: 1px solid #ddd;
                            padding: 8px;
                        }
                        
                        table tr:nth-child(even){background-color: #f2f2f2;}
                        
                        table tr:hover {background-color: #ddd;}
                        
                        table th {
                            padding-top: 12px;
                            padding-bottom: 12px;
                            text-align: left;
                            background-color: #4CAF50;
                            color: white;
                        }
                        .filterContainer{
                            display:inline-block;
                            width:23%;
                            
                        }
                        @page{
                            margin-left:20px;
                            margin-right:20px;
                            margin-top:160px;
                          }
                        .imageWrapper {
                            float:left;
                        
                        }
                        .thisImage{
                        height:200px;
                        width:150px;
                        }
                        .studentInfoContainer{
                            display:inline-block;
                            vertical-align:top;
                            width:48%;
                        }
                        .inputtext {
                            border-top-style: none;
                            border-right-style: none;
                            border-bottom-style: solid;
                            border-left-style: none;
                            border-width:2px;
                            width: 99%;
                            border-color:black;
                        }
                        
                    </style>
                </head>
                <body>
                    ".$this->getAdminSetting()."
                    <div style = 'margin-bottom:20px; margin-top:20px;'>
                        <center><h2 style='text-decoration:underline;'>Counseling Transaction</h2></center>
                    </div>
                    <div style = 'margin-bottom:20px; margin-top:20px;'>
                        <div class='studentInfoContainer'>
                            <div>
                                <div class='studentInfoContainer'>
                                    <label>Student Name :</label>
                                </div>
                                <div class='studentInfoContainer'>
                                    <input type = 'text' value ='$student->lastName, $student->firstName' class='inputtext'>
                                </div>
                            </div>
                            <div>
                                <div class='studentInfoContainer'>
                                    <label>Student No :</label>
                                </div>
                                <div class='studentInfoContainer'>
                                    <input type = 'text' value ='$studentno' class='inputtext'>
                                </div>
                            </div>
                            <div>
                                <div class='studentInfoContainer'>
                                    <label>Gender :</label>
                                </div>
                                <div class='studentInfoContainer'>
                                    <input type = 'text' value ='".ucfirst($student->gender)."' class='inputtext'>
                                </div>
                            </div>
                        </div>
                        <div class='studentInfoContainer'>
                            <div>
                                <div class='studentInfoContainer'>
                                    <label>Student Type :</label>
                                </div>
                                <div class='studentInfoContainer'>
                                    <input type = 'text' value ='".ucfirst($thistudenttype)."' class='inputtext'>
                                </div>
                            </div>
                            <div>
                                <div class='studentInfoContainer'>
                                    <label>$studentGradeLabel</label>
                                </div>
                                <div class='studentInfoContainer'>
                                    <input type = 'text' value ='$gradelevel' class='inputtext'>
                                </div>
                            </div>
                            <div>
                                <div class='studentInfoContainer'>
                                    <label>Address :</label>
                                </div>
                                <div class='studentInfoContainer'>
                                    <input type = 'text' value ='".ucfirst($student->cityAddress)."' class='inputtext'>
                                </div>
                            </div>
                        </div>
                    </div>
                     
        ";
        $html .= $myhtml;
        $html.="
            </body>
            </html>";
        
        PDF::loadHTML($html)->save(public_path("temp/".'report.pdf'));
        return "true";
    }
    public function getReferralReport($studenttype, $courseGradeLevel, $schoolYear, $semester){
        $referral;
        $referralCounter = 0;
        if($schoolYear == "all" && $semester == "all"){
            $referral = referral::join('students', 'students.id', '=', 'referrals.studentID')
                            ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                            ->select('referrals.*', 'school_years.schoolYearStart', 'school_years.schoolYearEnd',
                            'students.firstName', 'students.lastName')
                            
                            ->get();
        }elseif ($schoolYear == "all" && $semester != "all") {
            $referral = referral::join('students', 'students.id', '=', 'referrals.studentID')
            ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
            ->select('referrals.*', 'school_years.schoolYearStart', 'school_years.schoolYearEnd',
            'students.firstName', 'students.lastName')
            ->where('referrals.semesterID', '=', $semester)
            ->get();
        }elseif ($schoolYear != "all" && $semester == "all") {
            $referral = referral::join('students', 'students.id', '=', 'referrals.studentID')
            ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
            ->select('referrals.*', 'school_years.schoolYearStart', 'school_years.schoolYearEnd',
            'students.firstName', 'students.lastName')
            ->where('referrals.schoolYearID', '=', $schoolYear)
            ->get();
        }elseif ($schoolYear != "all" && $semester != "all") {
            $referral = referral::join('students', 'students.id', '=', 'referrals.studentID')
            ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
            ->select('referrals.*', 'school_years.schoolYearStart', 'school_years.schoolYearEnd',
            'students.firstName', 'students.lastName')
            ->where('referrals.schoolYearID', '=', $schoolYear)
            ->where('referrals.semesterID', '=', $semester)
            ->get();
        }
        
        $html = '';
        if(count($referral)==0){
            return  "<h2>There are no referrals found</h2>";
        }else{
            foreach ($referral as $key => $value) {
                if($this->refferalCollection($referral[$key]->studentID, $referral, $key, $studenttype, $courseGradeLevel, $schoolYear, $semester)){
                    continue;
                }
                $html .= "<tr>
                            <td>".++$referralCounter."</td>
                            <td>".$referral[$key]->firstName." ".$referral[$key]->lastName."</td>
                            <td>".$referral[$key]->semesterID."</td>
                            <td>".explode('-', $referral[$key]->schoolYearStart)[0]." - ".explode('-', $referral[$key]->schoolYearEnd)[0]."</td>
                            <td>".$referral[$key]->referralDate."</td>
                            <td>".$referral[$key]->referredTo."</td>
                            <td>".$referral[$key]->referredBy."</td>
                        </tr>";
            }
            if($html == ""){
                return "<h2>There are no referrals found</h2>";
            }else{
                return $html;
            }
        }
    }
    public function getReferralFaster($viewby, $coursegrade, $gradelevel, $schoolyear, $semester, $behaviour){
        $user = new userController();
        $systemdate = systemDate::all();
        $sql='';
      
        if(session()->get('user')['userType']=='admin'){
            $sql = "SELECT COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) 
                    semesterID, COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) 
                    schoolYearID, COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) studentTypes,
                    col.studentType, shs.studentType, jhs.studentType, elem.studentType,
                    COALESCE(col.yearLevel, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) courseGrade, 
                    col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel,
                    COALESCE(col.studentNo, shs.studNo, jhs.studNo, elem.studNo) studentNo,  students.firstName, students.lastName,
                    students.middleName,
                    col.courseName, col.yearLevel,
                    school_years.schoolYearStart,
                    school_years.schoolYearEnd,
                    referrals.*,
                    COALESCE(col.courseID, shs.seniorHighStrandID) strandCourse
                    FROM students LEFT JOIN (SELECT college_students.studentID, 
                    college_students.yearLevel, college_students.semesterID, college_students.schoolYearID,
                    college_students.id, 'college' AS studentType, 
                    college_students.courseID, courses.courseName, college_students.studentNo FROM
                    college_students INNER JOIN (SELECT max(id) as maxid FROM college_students GROUP 
                    BY studentID) cs ON cs.maxid = college_students.id
                    INNER JOIN courses ON courses.id = college_students.courseID) col ON 
                    col.studentID = students.id LEFT 
                    JOIN (SELECT s_h_s_students.studentID, s_h_s_students.semesterID, s_h_s_students.schoolYearID, s_h_s_students.id, 'seniorhigh' 
                    AS studentType, s_h_s_students.gradeLevel, s_h_s_students.studNo,  senior_high_strands.strandName, s_h_s_students.seniorHighStrandID
                    FROM s_h_s_students INNER JOIN (SELECT max(id) as maxid FROM s_h_s_students GROUP BY studentID) 
                    shs ON shs.maxid = s_h_s_students.id INNER JOIN senior_high_strands ON senior_high_strands.id = 
                    s_h_s_students.seniorHighStrandID) shs ON shs.studentID = students.id
                    LEFT JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'juniorhigh' AS 
                    studentType, gradeLevel, studNo  FROM j_h_students INNER JOIN (SELECT max(id) as maxid FROM j_h_students 
                    GROUP BY studentID) jhs ON jhs.maxid = j_h_students.id) jhs ON jhs.studentID = students.id LEFT 
                    JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'elementary' AS studentType, 
                    gradeLevel, studNo FROM elem_students INNER JOIN (SELECT max(id) as maxid FROM elem_students GROUP 
                    BY studentID) elem ON elem.maxid = elem_students.id) elem ON elem.studentID = students.id
                    INNER JOIN referrals ON referrals.studentID =  students.id
                    INNER JOIN behaviours ON behaviours.referralID = referrals.id
                    INNER JOIN school_years ON school_years.id = referrals.schoolYearID
            WHERE DATE(students.created_at) <= '".$systemdate[0]->systemDate."'";
            switch ($viewby) {
                case 'college':
                    $sql .= " AND col.studentType = 'college'";
                break;
                case 'seniorhigh':
                    $sql .= " AND shs.studentType = 'seniorhigh'";
                break;
                case 'juniorhigh':
                    $sql .= " AND jhs.studentType = 'juniorhigh'";
                break;
                case 'elementary':
                    $sql .= " AND elem.studentType = 'elementary'";
                break;
                default:
                   
                break;
            }
            if($schoolyear!="all"){
                $sql .= " AND referrals.schoolYearID = '$schoolyear'";
            }
            if($semester!="all"){
                $sql .= " AND referrals.semesterID = '$semester'";
            }
            if($coursegrade!="all"){
                $sql .= " AND COALESCE(col.courseID, shs.seniorHighStrandID) = '$coursegrade'";
            }
            if($gradelevel!="all"){
                $sql .= " AND COALESCE(col.yearLevel, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) = '$gradelevel'";
            }
            if($behaviour!="all"){
                $sql .= " AND behaviours.behavioural = '$behaviour'";
            }
        }else{
            $designation = $user->getUserDesignation(Auth::guard('myuser')->id());
            $sql = "SELECT COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) 
                    semesterID, COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) 
                    schoolYearID, COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) studentTypes,
                    col.studentType, shs.studentType, jhs.studentType, elem.studentType,
                    COALESCE(col.yearLevel, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) courseGrade, 
                    col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel,
                    COALESCE(col.studentNo, shs.studNo, jhs.studNo, elem.studNo) studentNo,  students.firstName, students.lastName,
                    students.middleName,
                    col.courseName, col.yearLevel,
                    school_years.schoolYearStart,
                    school_years.schoolYearEnd,
                    referrals.*,
                    COALESCE(col.courseID, shs.seniorHighStrandID) strandCourse
                    FROM students LEFT JOIN (SELECT college_students.studentID, 
                    college_students.yearLevel, college_students.semesterID, college_students.schoolYearID,
                    college_students.id, 'college' AS studentType, 
                    college_students.courseID, courses.courseName, college_students.studentNo FROM
                    college_students INNER JOIN (SELECT max(id) as maxid FROM college_students GROUP 
                    BY studentID) cs ON cs.maxid = college_students.id
                    INNER JOIN courses ON courses.id = college_students.courseID) col ON 
                    col.studentID = students.id LEFT 
                    JOIN (SELECT s_h_s_students.studentID, s_h_s_students.semesterID, s_h_s_students.schoolYearID, s_h_s_students.id, 'seniorhigh' 
                    AS studentType, s_h_s_students.gradeLevel, s_h_s_students.studNo,  senior_high_strands.strandName, s_h_s_students.seniorHighStrandID
                    FROM s_h_s_students INNER JOIN (SELECT max(id) as maxid FROM s_h_s_students GROUP BY studentID) 
                    shs ON shs.maxid = s_h_s_students.id INNER JOIN senior_high_strands ON senior_high_strands.id = 
                    s_h_s_students.seniorHighStrandID) shs ON shs.studentID = students.id
                    LEFT JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'juniorhigh' AS 
                    studentType, gradeLevel, studNo  FROM j_h_students INNER JOIN (SELECT max(id) as maxid FROM j_h_students 
                    GROUP BY studentID) jhs ON jhs.maxid = j_h_students.id) jhs ON jhs.studentID = students.id LEFT 
                    JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'elementary' AS studentType, 
                    gradeLevel, studNo FROM elem_students INNER JOIN (SELECT max(id) as maxid FROM elem_students GROUP 
                    BY studentID) elem ON elem.maxid = elem_students.id) elem ON elem.studentID = students.id
                    INNER JOIN referrals ON referrals.studentID =  students.id
                    INNER JOIN behaviours ON behaviours.referralID = referrals.id
                    INNER JOIN school_years ON school_years.id = referrals.schoolYearID
                    WHERE DATE(students.created_at) <= '".$systemdate[0]->systemDate."'";
            switch($designation[0]){
                case 'college':
                    $sql= "SELECT cs2.courseID AS courseGrade, c2.courseName, cs2.schoolYearID, cs2.studentNo, 
                    cs2.yearLevel, students.firstName, students.lastName, 'college' AS studentTypes, cs2.semesterID,
                    students.middleName,
                    referrals.*,
                    school_years.schoolYearStart,
                    school_years.schoolYearEnd  FROM students 
                    INNER JOIN (SELECT studentID, studentNo,
                    id, courseID, schoolYearID, yearLevel, semesterID FROM college_students  INNER JOIN (SELECT max(id) as maxid 
                    FROM college_students 
                    GROUP BY studentID) cs ON cs.maxid = college_students.id) cs2 ON cs2.studentID = students.id 
                    INNER JOIN (SELECT c.id AS courseID, dep.departmentID, c.courseName FROM courses c INNER JOIN (SELECT
                    id, courseID FROM course_department_hists cd INNER JOIN (SELECT max(id) as maxid FROM 
                    course_department_hists GROUP BY courseID) cd2 ON cd2.maxid = cd.id) cdh ON cdh.courseID = c.id
                    INNER JOIN (SELECT departmentID, id FROM course_department_hists INNER JOIN (SELECT
                    max(id) AS maxid FROM course_department_hists GROUP BY courseID) cdh1 ON cdh1.maxid = 
                    course_department_hists.id) dep ON dep.id =cdh.id) c2 ON c2.courseID = cs2.courseID 
                    INNER JOIN (SELECT departmentID, id, collegeID FROM department_college_hists
                    INNER JOIN (SELECT max(id) as maxid FROM department_college_hists GROUP BY departmentID) 
                    depc ON depc.maxid = department_college_hists.id) depc2 ON depc2.departmentID=c2.departmentID 
                    INNER JOIN (SELECT collegeID, userID, id FROM assignment_colleges INNER JOIN (SELECT max(id) 
                    as maxid FROM assignment_colleges GROUP BY userID) assc ON assc.maxid = assignment_colleges.id) 
                    assc2 ON assc2.collegeID = depc2.collegeID 
                    INNER JOIN referrals ON referrals.studentID = students.id
                    INNER JOIN behaviours ON behaviours.referralID = referrals.id
                    INNER JOIN school_years ON school_years.id = referrals.schoolYearID
                    WHERE assc2.userID ='".Auth::guard('myuser')->id()."'
                    AND DATE(students.created_at) <= '".$systemdate[0]->systemDate."'";
                    if($schoolyear != "all"){
                        $sql .= " AND referrals.schoolYearID = '$schoolyear'";
                    }
                    if($semester != "all"){
                        $sql .= " AND referrals.semesterID = '$semester'";
                    }
                    if($coursegrade != "all"){
                        $sql .= " AND cs2.courseID = '$coursegrade'";
                    }
                    if($gradelevel != "all"){
                        $sql .= " AND cs2.yearLevel = '$gradelevel'";
                    }
                    if($behaviour!="all"){
                        $sql .= " AND behaviours.behavioural = '$behaviour'";
                    }
                break;
                case 'seniorhigh':
                    $sql .= " AND  shs.studentType = 'seniorhigh' 
                              AND  shs.gradeLevel = '".$designation[1]."'";
                     if($schoolyear != "all"){
                        $sql .= " AND referrals.schoolYearID = '$schoolyear'";
                    }
                    if($semester != "all"){
                        $sql .= " AND referrals.semesterID = '$semester'";
                    }
                    if($coursegrade!="all"){
                        $sql .= " AND COALESCE(col.courseID, shs.seniorHighStrandID) = '$coursegrade'";
                    }
                    if($behaviour!="all"){
                        $sql .= " AND behaviours.behavioural = '$behaviour'";
                    }
                break;
                case 'juniorhigh':
                    $sql .= " AND  jhs.studentType = 'juniorhigh' AND jhs.gradeLevel = '".$designation[1]."'";
                     if($schoolyear != "all"){
                        $sql .= " AND referrals.schoolYearID = '$schoolyear'";
                    }
                    if($semester != "all"){
                        $sql .= " AND referrals.semesterID = '$semester'";
                    }
                    if($behaviour!="all"){
                        $sql .= " AND behaviours.behavioural = '$behaviour'";
                    }
                break;
                case 'elementary':
                    $sql .= " AND elem.studentType = 'elementary' AND  elem.gradeLevel = '".$designation[1]."'";
                    if($schoolyear != "all"){
                        $sql .= " AND referrals.schoolYearID = '$schoolyear'";
                    }
                    if($semester != "all"){
                        $sql .= " AND referrals.semesterID = '$semester'";
                    }
                    if($behaviour!="all"){
                        $sql .= " AND behaviours.behavioural = '$behaviour'";
                    }
                break;
            }
        }
        $sql .= " GROUP BY referrals.id";
       
        $referral = DB::select(DB::raw($sql));
        if(count($referral)==0){
            return "<h2>There are no referrals found.</h2>";
        }else{
            $html = '';
            $referralCounter =0;
            foreach ($referral as $referrals) {
                $referralCounter++;
                $html .= $this->getReferralRowFaster($referrals, $viewby, $referralCounter);
            }
            return $html;
        }
        
    }
    public function getReferralRowFaster($referral, $viewby, $referralCounter){
        $gradelevel = '';
        if($referral->yearLevel ==''){
            $gradelevel = $referral->courseGrade;
        }else{
            $gradelevel = $referral->yearLevel;
        }
        return "<tr data-value = '$referral->studentID'>
                    <td>".$referralCounter."</td>
                    <td>".$this->getBeautifiedName($referral->firstName, $referral->middleName, $referral->lastName)."</td>
                    <td>".$referral->semesterID."</td>
                    <td>".explode('-', $referral->schoolYearStart)[0]." - ".explode('-', $referral->schoolYearEnd)[0]."</td>
                    <td>".$referral->referralDate."</td>
                    <td>".$referral->referredTo."</td>
                    <td>".$referral->referredBy."</td>
                </tr>";
    }
    public function printReferralReport($viewby, $coursegrade, $gradelevel, $schoolyear, $semester, $behaviour){
        $referral;
        $thissemester = 'all Semester';
        $thisschoolyear = 'all School year';
        $user = new userController();
        $systemdate = systemDate::all();
        $sql='';
      
        if(session()->get('user')['userType']=='admin'){
            $sql = "SELECT COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) 
                    semesterID, COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) 
                    schoolYearID, COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) studentTypes,
                    col.studentType, shs.studentType, jhs.studentType, elem.studentType,
                    COALESCE(col.yearLevel, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) courseGrade, 
                    col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel,
                    COALESCE(col.studentNo, shs.studNo, jhs.studNo, elem.studNo) studentNo,  students.firstName, students.lastName,
                    students.middleName,
                    col.courseName, col.yearLevel,
                    school_years.schoolYearStart,
                    school_years.schoolYearEnd,
                    referrals.*,
                    COALESCE(col.courseID, shs.seniorHighStrandID) strandCourse
                    FROM students LEFT JOIN (SELECT college_students.studentID, 
                    college_students.yearLevel, college_students.semesterID, college_students.schoolYearID,
                    college_students.id, 'college' AS studentType, 
                    college_students.courseID, courses.courseName, college_students.studentNo FROM
                    college_students INNER JOIN (SELECT max(id) as maxid FROM college_students GROUP 
                    BY studentID) cs ON cs.maxid = college_students.id
                    INNER JOIN courses ON courses.id = college_students.courseID) col ON 
                    col.studentID = students.id LEFT 
                    JOIN (SELECT s_h_s_students.studentID, s_h_s_students.semesterID, s_h_s_students.schoolYearID, s_h_s_students.id, 'seniorhigh' 
                    AS studentType, s_h_s_students.gradeLevel, s_h_s_students.studNo,  senior_high_strands.strandName, s_h_s_students.seniorHighStrandID
                    FROM s_h_s_students INNER JOIN (SELECT max(id) as maxid FROM s_h_s_students GROUP BY studentID) 
                    shs ON shs.maxid = s_h_s_students.id INNER JOIN senior_high_strands ON senior_high_strands.id = 
                    s_h_s_students.seniorHighStrandID) shs ON shs.studentID = students.id
                    LEFT JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'juniorhigh' AS 
                    studentType, gradeLevel, studNo  FROM j_h_students INNER JOIN (SELECT max(id) as maxid FROM j_h_students 
                    GROUP BY studentID) jhs ON jhs.maxid = j_h_students.id) jhs ON jhs.studentID = students.id LEFT 
                    JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'elementary' AS studentType, 
                    gradeLevel, studNo FROM elem_students INNER JOIN (SELECT max(id) as maxid FROM elem_students GROUP 
                    BY studentID) elem ON elem.maxid = elem_students.id) elem ON elem.studentID = students.id
                    INNER JOIN referrals ON referrals.studentID =  students.id
                    INNER JOIN behaviours ON behaviours.referralID = referrals.id
                    INNER JOIN school_years ON school_years.id = referrals.schoolYearID
            WHERE DATE(students.created_at) <= '".$systemdate[0]->systemDate."'";
            switch ($viewby) {
                case 'college':
                    $sql .= " AND col.studentType = 'college'";
                break;
                case 'seniorhigh':
                    $sql .= " AND shs.studentType = 'seniorhigh'";
                break;
                case 'juniorhigh':
                    $sql .= " AND jhs.studentType = 'juniorhigh'";
                break;
                case 'elementary':
                    $sql .= " AND elem.studentType = 'elementary'";
                break;
                default:
                   
                break;
            }
            if($schoolyear!="all"){
                $sql .= " AND referrals.schoolYearID = '$schoolyear'";
                $thisschoolyear = $this->getYearOfSchoolYear($schoolyear)." School Year";
            }
            if($semester!="all"){
                switch ($semester) {
                    case '1':
                        $thissemester = '1st Semester';
                    break;
                    case '2':
                        $thissemester = '2nd Semester';
                    break;
                    case '3':
                        $thissemester = '3rd Semester';
                    break;
                    default:
                        # code...
                        break;
                }
               
                $sql .= " AND referrals.semesterID = '$semester'";
            }
            if($coursegrade!="all"){
                $sql .= " AND COALESCE(col.courseID, shs.seniorHighStrandID) = '$coursegrade'";
            }
            if($gradelevel!="all"){
                $sql .= " AND COALESCE(col.yearLevel, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) = '$gradelevel'";
            }
            if($behaviour!="all"){
                $sql .= " AND behaviours.behavioural = '$behaviour'";
            }
        }else{
            $designation = $user->getUserDesignation(Auth::guard('myuser')->id());
            $sql = "SELECT COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) 
                    semesterID, COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) 
                    schoolYearID, COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) studentTypes,
                    col.studentType, shs.studentType, jhs.studentType, elem.studentType,
                    COALESCE(col.yearLevel, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) courseGrade, 
                    col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel,
                    COALESCE(col.studentNo, shs.studNo, jhs.studNo, elem.studNo) studentNo,  students.firstName, students.lastName,
                    students.middleName,
                    col.courseName, col.yearLevel,
                    school_years.schoolYearStart,
                    school_years.schoolYearEnd,
                    referrals.*,
                    COALESCE(col.courseID, shs.seniorHighStrandID) strandCourse
                    FROM students LEFT JOIN (SELECT college_students.studentID, 
                    college_students.yearLevel, college_students.semesterID, college_students.schoolYearID,
                    college_students.id, 'college' AS studentType, 
                    college_students.courseID, courses.courseName, college_students.studentNo FROM
                    college_students INNER JOIN (SELECT max(id) as maxid FROM college_students GROUP 
                    BY studentID) cs ON cs.maxid = college_students.id
                    INNER JOIN courses ON courses.id = college_students.courseID) col ON 
                    col.studentID = students.id LEFT 
                    JOIN (SELECT s_h_s_students.studentID, s_h_s_students.semesterID, s_h_s_students.schoolYearID, s_h_s_students.id, 'seniorhigh' 
                    AS studentType, s_h_s_students.gradeLevel, s_h_s_students.studNo,  senior_high_strands.strandName, s_h_s_students.seniorHighStrandID
                    FROM s_h_s_students INNER JOIN (SELECT max(id) as maxid FROM s_h_s_students GROUP BY studentID) 
                    shs ON shs.maxid = s_h_s_students.id INNER JOIN senior_high_strands ON senior_high_strands.id = 
                    s_h_s_students.seniorHighStrandID) shs ON shs.studentID = students.id
                    LEFT JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'juniorhigh' AS 
                    studentType, gradeLevel, studNo  FROM j_h_students INNER JOIN (SELECT max(id) as maxid FROM j_h_students 
                    GROUP BY studentID) jhs ON jhs.maxid = j_h_students.id) jhs ON jhs.studentID = students.id LEFT 
                    JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'elementary' AS studentType, 
                    gradeLevel, studNo FROM elem_students INNER JOIN (SELECT max(id) as maxid FROM elem_students GROUP 
                    BY studentID) elem ON elem.maxid = elem_students.id) elem ON elem.studentID = students.id
                    INNER JOIN referrals ON referrals.studentID =  students.id
                    INNER JOIN behaviours ON behaviours.referralID = referrals.id
                    INNER JOIN school_years ON school_years.id = referrals.schoolYearID
                    WHERE DATE(students.created_at) <= '".$systemdate[0]->systemDate."'";
            switch($designation[0]){
                case 'college':
                    $sql= "SELECT cs2.courseID AS courseGrade, c2.courseName, cs2.schoolYearID, cs2.studentNo, 
                    cs2.yearLevel, students.firstName, students.lastName, 'college' AS studentTypes, cs2.semesterID,
                    students.middleName,
                    referrals.*,
                    school_years.schoolYearStart,
                    school_years.schoolYearEnd  FROM students 
                    INNER JOIN (SELECT studentID, studentNo,
                    id, courseID, schoolYearID, yearLevel, semesterID FROM college_students  INNER JOIN (SELECT max(id) as maxid 
                    FROM college_students 
                    GROUP BY studentID) cs ON cs.maxid = college_students.id) cs2 ON cs2.studentID = students.id 
                    INNER JOIN (SELECT c.id AS courseID, dep.departmentID, c.courseName FROM courses c INNER JOIN (SELECT
                    id, courseID FROM course_department_hists cd INNER JOIN (SELECT max(id) as maxid FROM 
                    course_department_hists GROUP BY courseID) cd2 ON cd2.maxid = cd.id) cdh ON cdh.courseID = c.id
                    INNER JOIN (SELECT departmentID, id FROM course_department_hists INNER JOIN (SELECT
                    max(id) AS maxid FROM course_department_hists GROUP BY courseID) cdh1 ON cdh1.maxid = 
                    course_department_hists.id) dep ON dep.id =cdh.id) c2 ON c2.courseID = cs2.courseID 
                    INNER JOIN (SELECT departmentID, id, collegeID FROM department_college_hists
                    INNER JOIN (SELECT max(id) as maxid FROM department_college_hists GROUP BY departmentID) 
                    depc ON depc.maxid = department_college_hists.id) depc2 ON depc2.departmentID=c2.departmentID 
                    INNER JOIN (SELECT collegeID, userID, id FROM assignment_colleges INNER JOIN (SELECT max(id) 
                    as maxid FROM assignment_colleges GROUP BY userID) assc ON assc.maxid = assignment_colleges.id) 
                    assc2 ON assc2.collegeID = depc2.collegeID 
                    INNER JOIN referrals ON referrals.studentID = students.id
                    INNER JOIN behaviours ON behaviours.referralID = referrals.id
                    INNER JOIN school_years ON school_years.id = referrals.schoolYearID
                    WHERE assc2.userID ='".Auth::guard('myuser')->id()."'
                    AND DATE(students.created_at) <= '".$systemdate[0]->systemDate."'";
                    if($schoolyear != "all"){
                        $sql .= " AND referrals.schoolYearID = '$schoolyear'";
                        $thisschoolyear = $this->getYearOfSchoolYear($schoolyear)." School Year";
                    }
                    if($semester != "all"){
                        switch ($semester) {
                            case '1':
                                $thissemester = '1st Semester';
                            break;
                            case '2':
                                $thissemester = '2nd Semester';
                            break;
                            case '3':
                                $thissemester = '3rd Semester';
                            break;
                            default:
                                # code...
                                break;
                        }
                        $sql .= " AND referrals.semesterID = '$semester'";
                    }
                    if($coursegrade != "all"){
                        $sql .= " AND cs2.courseID = '$coursegrade'";
                    }
                    if($gradelevel != "all"){
                        $sql .= " AND cs2.yearLevel = '$gradelevel'";
                    }
                    if($behaviour!="all"){
                        $sql .= " AND behaviours.behavioural = '$behaviour'";
                    }
                break;
                case 'seniorhigh':
                    $sql .= " AND  shs.studentType = 'seniorhigh' 
                              AND  shs.gradeLevel = '".$designation[1]."'";
                     if($schoolyear != "all"){
                        $sql .= " AND referrals.schoolYearID = '$schoolyear'";
                        $thisschoolyear = $this->getYearOfSchoolYear($schoolyear)." School Year";
                    }
                    if($semester != "all"){
                        switch ($semester) {
                            case '1':
                                $thissemester = '1st Semester';
                            break;
                            case '2':
                                $thissemester = '2nd Semester';
                            break;
                            case '3':
                                $thissemester = '3rd Semester';
                            break;
                            default:
                                # code...
                                break;
                        }
                        $sql .= " AND referrals.semesterID = '$semester'";
                    }
                    if($coursegrade!="all"){
                        $sql .= " AND COALESCE(col.courseID, shs.seniorHighStrandID) = '$coursegrade'";
                    }
                    if($behaviour!="all"){
                        $sql .= " AND behaviours.behavioural = '$behaviour'";
                    }
                break;
                case 'juniorhigh':
                    $sql .= " AND  jhs.studentType = 'juniorhigh' AND jhs.gradeLevel = '".$designation[1]."'";
                     if($schoolyear != "all"){
                        $thisschoolyear = $this->getYearOfSchoolYear($schoolyear)." School Year";
                        $sql .= " AND referrals.schoolYearID = '$schoolyear'";
                    }
                    if($semester != "all"){
                        switch ($semester) {
                            case '1':
                                $thissemester = '1st Semester';
                            break;
                            case '2':
                                $thissemester = '2nd Semester';
                            break;
                            case '3':
                                $thissemester = '3rd Semester';
                            break;
                            default:
                                # code...
                                break;
                        }
                        $sql .= " AND referrals.semesterID = '$semester'";
                    }
                    if($behaviour!="all"){
                        $sql .= " AND behaviours.behavioural = '$behaviour'";
                    }
                break;
                case 'elementary':
                    $sql .= " AND elem.studentType = 'elementary' AND  elem.gradeLevel = '".$designation[1]."'";
                    if($schoolyear != "all"){
                        $thisschoolyear = $this->getYearOfSchoolYear($schoolyear)." School Year";
                        $sql .= " AND referrals.schoolYearID = '$schoolyear'";
                    }
                    if($semester != "all"){
                        switch ($semester) {
                            case '1':
                                $thissemester = '1st Semester';
                            break;
                            case '2':
                                $thissemester = '2nd Semester';
                            break;
                            case '3':
                                $thissemester = '3rd Semester';
                            break;
                            default:
                                # code...
                                break;
                        }
                        $sql .= " AND referrals.semesterID = '$semester'";
                    }
                    if($behaviour!="all"){
                        $sql .= " AND behaviours.behavioural = '$behaviour'";
                    }
                break;
            }
        }
        $html = '';
        $sql .= " GROUP BY referrals.id";
       
        $referral = DB::select(DB::raw($sql));
        if(count($referral)==0){
            $html =  "<tr><h2>There are no referrals found</h2></tr>";
        }else{
            $counter = 0;
            foreach ($referral as $key => $value) {
                
                $html .= "<tr>
                            <td>".(++$counter)."</td>
                            <td>".$referral[$key]->firstName." ".$referral[$key]->lastName."</td>
                            <td>".date("F j, Y", strtotime($referral[$key]->referralDate))."</td>
                            <td>".$referral[$key]->referredTo."</td>
                            <td>".$referral[$key]->referredBy."</td>
                        </tr>";
            }
            if($html == ""){
                $html = "<tr><h2>There are no referrals found</h2></tr>";
            }
        }
        $title = "List of Students with Referrals for ".$thissemester." and ".$thisschoolyear."";
        $myhtml = "
            <html>
                <head>
                    <style>
                        table {
                            font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif;
                            border-collapse: collapse;
                            width: 100%;
                        }
                        .well{
                            min-height: 20px;
                            padding: 19px;
                            margin-bottom: 20px;
                            background-color: #f5f5f5;
                            border: 1px solid #e3e3e3;
                            border-radius: 4px;
                            -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
                            box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
                        }
                        table td, table th {
                            border: 1px solid #ddd;
                            padding: 8px;
                        }
                        
                        table tr:nth-child(even){background-color: #f2f2f2;}
                        
                        table tr:hover {background-color: #ddd;}
                        
                        table th {
                            padding-top: 12px;
                            padding-bottom: 12px;
                            text-align: left;
                            background-color: #4CAF50;
                            color: white;
                        }
                        .filterContainer{
                            display:inline-block;
                            width:23%;
                            
                        }
                        @page{
                            margin-left:20px;
                            margin-right:20px;
                            margin-top:160px;
                        }
                        .imageWrapper {
                            float:left;
                        
                        }
                        .thisImage{
                        height:200px;
                        width:150px;
                        }
                        
                    </style>
                </head>
                <body>
                ".$this->getAdminSetting()."
                    <div style = 'margin-bottom:20px; margin-top:20px;'>
                        <center><h2 style = 'text-decoration:underline;'>$title</h2></center>
                    </div>
                    <table>
                        
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Student</th>
                               
                                <th>Referral Date</th>
                                <th>Referred To</th>
                                <th>Referred By</th>
                            </tr>
                        </thead>
                        <tbody>

                     
        ";
        $myhtml .= $html;
        $html.="
                        </tbody>
                    </table>
                </body>
            </html>";
        
        PDF::loadHTML($myhtml)->save(public_path("temp/".'report.pdf'));
        return "true";
    }
    public function getContractReport($studenttype, $courseGradeLevel, $schoolYear, $semester){
        $contract;
        $contractCounter=0;
        if($schoolYear == "all" && $semester == "all"){
            $contract = contract::join("college_students", 'college_students.id', '=' ,'contracts.collegStudentID')
                                ->join("students", 'students.id', '=', 'college_students.studentID')
                                ->join("school_years", 'school_years.id', '=', 'college_students.schoolYearID')
                                ->select('contracts.*', 'school_years.schoolYearStart', 'school_years.schoolYearEnd',
                                'students.firstName', 'students.lastName', 'college_students.studentID')
                               
                                ->get();
        }elseif ($schoolYear == "all" && $semester != "all") {
            $contract = contract::join("college_students", 'college_students.id', '=' ,'contracts.collegStudentID')
                                ->join("students", 'students.id', '=', 'college_students.studentID')
                                ->join("school_years", 'school_years.id', '=', 'college_students.schoolYearID')
                                ->select('contracts.*', 'school_years.schoolYearStart', 'school_years.schoolYearEnd',
                                'students.firstName', 'students.lastName', 'college_students.studentID')
                              
                                ->where('contracts.semesterID', '=', $semester)
                                ->get();
        }elseif ($schoolYear != "all" && $semester == "all") {
            $contract = contract::join("college_students", 'college_students.id', '=' ,'contracts.collegStudentID')
                                ->join("students", 'students.id', '=', 'college_students.studentID')
                                ->join("school_years", 'school_years.id', '=', 'college_students.schoolYearID')
                                ->select('contracts.*', 'school_years.schoolYearStart', 'school_years.schoolYearEnd',
                                'students.firstName', 'students.lastName', 'college_students.studentID')
                              
                                ->where('contracts.schoolYearID', '=', $semester)
                                ->get();
        }elseif ($schoolYear != "all" && $semester != "all") {
            $contract = contract::join("college_students", 'college_students.id', '=' ,'contracts.collegStudentID')
                                ->join("students", 'students.id', '=', 'college_students.studentID')
                                ->join("school_years", 'school_years.id', '=', 'college_students.schoolYearID')
                                ->select('contracts.*', 'school_years.schoolYearStart', 'school_years.schoolYearEnd',
                                'students.firstName', 'students.lastName', 'college_students.studentID')
                                ->where('contracts.schoolYearID', '=', $schoolYear)
                                ->where('contracts.semesterID', '=', $semester)
                                ->get();
        }
       
                            
        if(count($contract)==0){
            return "<h2>There are no contracts found</h2>";
        }else{
            $html ='';
            foreach ($contract as $key => $value) {
                error_log($contract[$key]->studentID);
                if($this->contractCollection($contract[$key]->studentID, $contract, $key, $studenttype, $courseGradeLevel, $schoolYear, $semester)){
                    continue;
                }
                $html .= "<tr>
                            <td>".++$contractCounter."</td>
                            <td>".$contract[$key]->firstName." ".$contract[$key]->lastName."</td>
                            <td>".$contract[$key]->semesterID."</td>
                            <td>".explode('-', $contract[$key]->schoolYearStart)[0]." - ".explode('-', $contract[$key]->schoolYearEnd)[0]."</td>
                            <td>".date("F j, Y", strtotime($contract[$key]->contractDate))."</td>
                            <td>".$contract[$key]->type."</td>
                            <td>".$contract[$key]->remarks."</td>
                            <td>".$contract[$key]->notedBy."</td>
                        </tr>";
            }
            if($html ==""){
                return "<h2>There are no contracts found</h2>";
            }else{
                return $html;
            }
        }
    }
    public function getContractFaster($studenttype, $course, $gradelevel, $schoolyear, $semester, $contracttype){
        $user = new userController();
        $systemdate = systemDate::all();
        $sql = "SELECT cs2.courseID AS courseGrade, c2.courseName, cs2.schoolYearID, cs2.studentNo, 
        cs2.yearLevel, students.firstName, students.lastName, students.middleName, 'college' AS studentTypes, cs2.semesterID,
        contracts.id, contracts.contractDate, contracts.semesterID, contracts.type, cs2.studentID,
        contracts.remarks,
        contracts.notedBy,
        school_years.schoolYearStart,
        school_years.schoolYearEnd  FROM students 
        INNER JOIN (SELECT studentID, studentNo,
        id, courseID, schoolYearID, yearLevel, semesterID FROM college_students  INNER JOIN (SELECT max(id) as maxid 
        FROM college_students 
        GROUP BY studentID) cs ON cs.maxid = college_students.id) cs2 ON cs2.studentID = students.id 
        INNER JOIN (SELECT c.id AS courseID, cdh.departmentID, c.courseName FROM courses c INNER JOIN (SELECT
        id, courseID, departmentID FROM course_department_hists cd INNER JOIN (SELECT max(id) as maxid FROM 
        course_department_hists GROUP BY courseID) cd2 ON cd2.maxid = cd.id) cdh ON cdh.courseID = c.id) c2 ON c2.courseID = cs2.courseID 
        INNER JOIN (SELECT departmentID, id, collegeID FROM department_college_hists
        INNER JOIN (SELECT max(id) as maxid FROM department_college_hists GROUP BY departmentID) 
        depc ON depc.maxid = department_college_hists.id) depc2 ON depc2.departmentID=c2.departmentID 
        INNER JOIN (SELECT collegeID, userID, id FROM assignment_colleges INNER JOIN (SELECT max(id) 
        as maxid FROM assignment_colleges GROUP BY userID) assc ON assc.maxid = assignment_colleges.id) 
        assc2 ON assc2.collegeID = depc2.collegeID 
        INNER JOIN contracts ON contracts.collegStudentID = cs2.id 
        INNER JOIN school_years ON school_years.id = contracts.schoolYearID
        WHERE  DATE(contracts.created_at) <= '".$systemdate[0]->systemDate."'";
        if(session()->get('user')['userType']=='admin'){
            $sql = "SELECT cs2.courseID AS courseGrade, c2.courseName, cs2.schoolYearID, cs2.studentNo, 
            cs2.yearLevel, students.firstName, students.lastName, students.middleName, 'college' AS studentTypes, cs2.semesterID,
            contracts.id, contracts.contractDate, contracts.semesterID, contracts.type, cs2.studentID,
            school_years.schoolYearStart,
            school_years.schoolYearEnd, contracts.remarks,
            contracts.notedBy  FROM students 
            INNER JOIN (SELECT studentID, studentNo,
            id, courseID, schoolYearID, yearLevel, semesterID FROM college_students  INNER JOIN (SELECT max(id) as maxid 
            FROM college_students 
            GROUP BY studentID) cs ON cs.maxid = college_students.id) cs2 ON cs2.studentID = students.id 
            INNER JOIN (SELECT c.id AS courseID, cdh.departmentID, c.courseName FROM courses c INNER JOIN (SELECT
            id, courseID, departmentID FROM course_department_hists cd INNER JOIN (SELECT max(id) as maxid FROM 
            course_department_hists GROUP BY courseID) cd2 ON cd2.maxid = cd.id) cdh ON cdh.courseID = c.id) c2 ON c2.courseID = cs2.courseID 
            INNER JOIN (SELECT departmentID, id, collegeID FROM department_college_hists
            INNER JOIN (SELECT max(id) as maxid FROM department_college_hists GROUP BY departmentID) 
            depc ON depc.maxid = department_college_hists.id) depc2 ON depc2.departmentID=c2.departmentID 
             
            INNER JOIN contracts ON contracts.collegStudentID = cs2.id
            INNER JOIN school_years ON school_years.id = contracts.schoolYearID
            WHERE  DATE(contracts.created_at) <= '".$systemdate[0]->systemDate."'";
            if($schoolyear != 'all'){
                $sql.=" AND contracts.schoolYearID = '$schoolyear'";
            }
            if($semester != 'all'){
                $sql.=" AND contracts.semesterID = '$semester'";
            }
            if($course != 'all'){
                $sql.=" AND cs2.courseID = '$course'";
            }
            if($gradelevel != "all"){
                $sql.=" AND cs2.yearLevel = '$gradelevel'";
            }
            if($contracttype!="all"){
                if($contracttype == "others"){
                    $sql.=" AND contracts.others <> null";
                }else{
                    $sql.=" AND contracts.type = '$contracttype'";
                }
            }
        }else{
            $sql .= " AND assc2.userID ='".Auth::guard('myuser')->id()."'";
            if($schoolyear != 'all'){
                $sql.=" AND contracts.schoolYearID = '$schoolyear'";
            }
            if($semester != 'all'){
                $sql.=" AND contracts.semesterID = '$semester'";
            }
            if($course != 'all'){
                $sql.=" AND cs2.courseID = '$course'";
            }
            if($gradelevel != "all"){
                $sql.=" AND cs2.yearLevel = '$gradelevel'";
            }
            if($contracttype!="all"){
                if($contracttype == "others"){
                    $sql.=" AND contracts.others <> null";
                }else{
                    $sql.=" AND contracts.type = '$contracttype'";
                }
            }
        }
        $sql .= " GROUP BY contracts.id";
       
        $contract = DB::select(DB::raw($sql));
        if(count($contract)==0){
            return "<h2>No contracts found.</h2>";
        }else{
            $html = '';
            $rowCounter=0;
            foreach ($contract as $contracts) {
                $rowCounter++;
                $html .= $this->getContractRow($contracts, $rowCounter);
            }
            return $html;
        }
    }
    public function getContractRow($contract, $counter){
        
        return "<tr id ='contractRow-$contract->id' data-value ='$contract->studentID'>
                    <td>".$counter."</td>
                    <td>".$this->getBeautifiedName($contract->firstName, $contract->middleName, $contract->lastName)."</td>
                    <td>$contract->semesterID</td>
                    <td>".explode('-', $contract->schoolYearStart)[0]."-".explode('-', $contract->schoolYearEnd)[0]."</td>
                    <td>".date("F j, Y", strtotime($contract->contractDate))."</td>
                    <td>".ucfirst($contract->type)."</td>
                    <td>$contract->remarks</td>
                    <td>$contract->notedBy</td>
                </tr>";
    }
    public function getYearOfActiveSchoolYear(){
        $schoolyear = schoolYear::where('status','=', '1')->get();
        if(count($schoolyear)>0){
            return explode('-', $schoolyear[0]->schoolYearStart)[0]." - ".explode('-', $schoolyear[0]->schoolYearEnd)[0];
        }else{
            return "";
        }
    }
    public function printContractReport($studenttype, $course, $gradelevel, $schoolyear, $semester, $contracttype){
        $user = new userController();
        $systemdate = systemDate::all();
        $thissemester = 'all Semester';
        $thisschoolyear = 'all School year';
        $sql = "SELECT cs2.courseID AS courseGrade, c2.courseName, cs2.schoolYearID, cs2.studentNo, 
        cs2.yearLevel, students.firstName, students.lastName, students.middleName, 'college' AS studentTypes, cs2.semesterID,
        contracts.id, contracts.contractDate, contracts.semesterID, contracts.type, cs2.studentID,
        contracts.remarks,
        contracts.notedBy,
        school_years.schoolYearStart,
        school_years.schoolYearEnd  FROM students 
        INNER JOIN (SELECT studentID, studentNo,
        id, courseID, schoolYearID, yearLevel, semesterID FROM college_students  INNER JOIN (SELECT max(id) as maxid 
        FROM college_students 
        GROUP BY studentID) cs ON cs.maxid = college_students.id) cs2 ON cs2.studentID = students.id 
        INNER JOIN (SELECT c.id AS courseID, cdh.departmentID, c.courseName FROM courses c INNER JOIN (SELECT
        id, courseID, departmentID FROM course_department_hists cd INNER JOIN (SELECT max(id) as maxid FROM 
        course_department_hists GROUP BY courseID) cd2 ON cd2.maxid = cd.id) cdh ON cdh.courseID = c.id) c2 ON c2.courseID = cs2.courseID 
        INNER JOIN (SELECT departmentID, id, collegeID FROM department_college_hists
        INNER JOIN (SELECT max(id) as maxid FROM department_college_hists GROUP BY departmentID) 
        depc ON depc.maxid = department_college_hists.id) depc2 ON depc2.departmentID=c2.departmentID 
        INNER JOIN (SELECT collegeID, userID, id FROM assignment_colleges INNER JOIN (SELECT max(id) 
        as maxid FROM assignment_colleges GROUP BY userID) assc ON assc.maxid = assignment_colleges.id) 
        assc2 ON assc2.collegeID = depc2.collegeID 
        INNER JOIN contracts ON contracts.collegStudentID = cs2.id 
        INNER JOIN school_years ON school_years.id = contracts.schoolYearID
        WHERE  DATE(contracts.created_at) <= '".$systemdate[0]->systemDate."'";
        if(session()->get('user')['userType']=='admin'){
            $sql = "SELECT cs2.courseID AS courseGrade, c2.courseName, cs2.schoolYearID, cs2.studentNo, 
            cs2.yearLevel, students.firstName, students.lastName, students.middleName, 'college' AS studentTypes, cs2.semesterID,
            contracts.id, contracts.contractDate, contracts.semesterID, contracts.type, cs2.studentID,
            school_years.schoolYearStart,
            school_years.schoolYearEnd, contracts.remarks,
            contracts.notedBy  FROM students 
            INNER JOIN (SELECT studentID, studentNo,
            id, courseID, schoolYearID, yearLevel, semesterID FROM college_students  INNER JOIN (SELECT max(id) as maxid 
            FROM college_students 
            GROUP BY studentID) cs ON cs.maxid = college_students.id) cs2 ON cs2.studentID = students.id 
            INNER JOIN (SELECT c.id AS courseID, cdh.departmentID, c.courseName FROM courses c INNER JOIN (SELECT
            id, courseID, departmentID FROM course_department_hists cd INNER JOIN (SELECT max(id) as maxid FROM 
            course_department_hists GROUP BY courseID) cd2 ON cd2.maxid = cd.id) cdh ON cdh.courseID = c.id) c2 ON c2.courseID = cs2.courseID 
            INNER JOIN (SELECT departmentID, id, collegeID FROM department_college_hists
            INNER JOIN (SELECT max(id) as maxid FROM department_college_hists GROUP BY departmentID) 
            depc ON depc.maxid = department_college_hists.id) depc2 ON depc2.departmentID=c2.departmentID 
             
            INNER JOIN contracts ON contracts.collegStudentID = cs2.id
            INNER JOIN school_years ON school_years.id = contracts.schoolYearID
            WHERE  DATE(contracts.created_at) <= '".$systemdate[0]->systemDate."'";
            if($schoolyear != 'all'){
                $sql.=" AND contracts.schoolYearID = '$schoolyear'";
                $thisschoolyear = $this->getYearOfSchoolYear($schoolyear)." School Year";
            }
            if($semester != 'all'){
                $sql.=" AND contracts.semesterID = '$semester'";
                switch ($semester) {
                    case '1':
                        $thissemester = '1st Semester';
                    break;
                    case '2':
                        $thissemester = '2nd Semester';
                    break;
                    case '3':
                        $thissemester = '3rd Semester';
                    break;
                    default:
                        # code...
                        break;
                }
            }
            if($course != 'all'){
                $sql.=" AND cs2.courseID = '$course'";
            }
            if($gradelevel != "all"){
                $sql.=" AND cs2.yearLevel = '$gradelevel'";
            }
            if($contracttype!="all"){
                if($contracttype == "others"){
                    $sql.=" AND contracts.others <> null";
                }else{
                    $sql.=" AND contracts.type = '$contracttype'";
                }
            }
        }else{
            $sql .= " AND assc2.userID ='".Auth::guard('myuser')->id()."'";
            if($schoolyear != 'all'){
                $sql.=" AND contracts.schoolYearID = '$schoolyear'";
                $thisschoolyear = $this->getYearOfSchoolYear($schoolyear)." School Year";
            }
            if($semester != 'all'){
                $sql.=" AND contracts.semesterID = '$semester'";
                switch ($semester) {
                    case '1':
                        $thissemester = '1st Semester';
                    break;
                    case '2':
                        $thissemester = '2nd Semester';
                    break;
                    case '3':
                        $thissemester = '3rd Semester';
                    break;
                    default:
                        # code...
                        break;
                }
            }
            if($course != 'all'){
                $sql.=" AND cs2.courseID = '$course'";
            }
            if($gradelevel != "all"){
                $sql.=" AND cs2.yearLevel = '$gradelevel'";
            }
            if($contracttype!="all"){
                if($contracttype == "others"){
                    $sql.=" AND contracts.others <> null";
                }else{
                    $sql.=" AND contracts.type = '$contracttype'";
                }
            }
        }
        $sql .= " GROUP BY contracts.id";
        //error_log($sql);
        $contract = DB::select(DB::raw($sql));
        

        $html ='';
        if(count($contract)==0){
            $html = "<tr><h2>There are no contracts found</h2></tr>";
        }else{
            $html ='';
            $counter = 0;
            foreach ($contract as $key => $value) {
                // error_log($contract[$key]->studentID);
                // if($this->contractCollection($contract[$key]->studentID, $contract, $key, $studenttype, $course, $schoolyear, $semester)){
                //     continue;
                // }
                $type = '';
                $contracttype = explode(" ", $contract[$key]->type);
                for ($i=0; $i < count($contracttype); $i++) { 
                    $type .= ucfirst($contracttype[$i])." ";
                }
                $html .= "<tr>
                            <td>".(++$counter)."</td>
                            <td>".$contract[$key]->firstName." ".$contract[$key]->lastName."</td>
                            <td>".date("F j, Y", strtotime($contract[$key]->contractDate))."</td>
                            <td>".$type."</td>
                            <td>".$contract[$key]->remarks."</td>
                            <td>".$contract[$key]->notedBy."</td>
                        </tr>";
            }
            if($html ==""){
                $html = "<tr><h2>There are no contracts found</h2></tr>";
            }
        
        }
        $title = "List of Students with Contracts for ".$thissemester." and ".$thisschoolyear;
        $myhtml = "
        <html>
            <head>
                <style>
                    table {
                        font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif;
                        border-collapse: collapse;
                        width: 100%;
                    }
                    .well{
                        min-height: 20px;
                        padding: 19px;
                        margin-bottom: 20px;
                        background-color: #f5f5f5;
                        border: 1px solid #e3e3e3;
                        border-radius: 4px;
                        -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
                        box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
                    }
                    table td, table th {
                        border: 1px solid #ddd;
                        padding: 8px;
                    }
                    
                    table tr:nth-child(even){background-color: #f2f2f2;}
                    
                    table tr:hover {background-color: #ddd;}
                    
                    table th {
                        padding-top: 12px;
                        padding-bottom: 12px;
                        text-align: left;
                        background-color: #4CAF50;
                        color: white;
                    }
                    .filterContainer{
                        display:inline-block;
                        width:23%;
                        
                    }
                    @page{
                        margin-left:20px;
                        margin-right:20px;
                        margin-top:160px;
                      }
                    .imageWrapper {
                        float:left;
                    
                    }
                    .thisImage{
                    height:200px;
                    width:150px;
                    }
                    
                </style>
            </head>
            <body>
                ".$this->getAdminSetting()."
                <div style = 'margin-bottom:20px; margin-top:20px;'>
                    <center><h2 style = 'text-decoration:underline;'>$title</h2></center>
                </div>
                <table>
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Student</th>
                            <th>Contract Date</th>
                            <th>Type</th>
                            <th>Remarks</th>
                            <th>Noted By</th>
                        </tr>
                    </thead>
                    <tbody>

                 
        ";
        $myhtml .= $html;
        $html.="
                        </tbody>
                    </table>
                </body>
            </html>";
        
        PDF::loadHTML($myhtml)->save(public_path("temp/".'report.pdf'));
        return "true";
    }
    public function counselingCollection($studentid, &$student, $key, $studenttype, $courseGradeLevel, $schoolYear, $semester){
        $mystudent =  new studentController();
        $studenttypetemp = $mystudent->getStudentType($studentid);
        if($studenttype != "all"){
           if($studenttypetemp!=$studenttype){
                $student->forget($key);
                return true;
            }
        }
        if($courseGradeLevel != "all"){
            switch($studenttypetemp){
                case 'college':
                    $temp = collegeStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM college_students WHERE studentID = ".$studentid.")"))->get();
                    if(count($temp)>0){
                        if($temp[0]->courseID != $courseGradeLevel){
                            $student->forget($key);
                            return true;
                        }
                    }else{
                        $student->forget($key);
                        return true;
                    }
                break;
                case 'seniorhigh':
                    $temp = SHSStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM s_h_s_students WHERE studentID = ".$studentid.")"))->get();
                    if(count($temp)>0){
                        if($temp[0]->gradeLevel != $courseGradeLevel){
                            $student->forget($key);
                            return true;
                        }
                    }else{
                        $student->forget($key);
                        return true;
                    }
                break;
                case 'juniorhigh':
                    $temp = JHStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM j_h_students WHERE studentID = ".$studentid.")"))->get();
                    if(count($temp)>0){
                        if($temp[0]->gradeLevel != $courseGradeLevel){
                            $student->forget($key);
                            return true;
                        }
                    }else{
                        $student->forget($key);
                        return true;
                    }
                break;
                case 'elementary':
                    $temp = elemStudent::where('id', '=', DB::raw("(SELECT MAX(id) FROM elem_students WHERE studentID = ".$studentid.")"))->get();
                    if(count($temp)>0){
                        if($temp[0]->gradeLevel != $courseGradeLevel){
                            $student->forget($key);
                            return true;
                        }
                    }else{
                        $student->forget($key);
                        return true;
                    }
                break;
            }
        }
        return false;
    }
    
    public function getCounselingReport($studenttype, $courseGradeLevel, $schoolYear, $semester){
        $counselingCounter = 0;
        //$systemdate = systemDate::all();
        $counseling;
        if($schoolYear == "all" && $semester == "all"){
            $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
            ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
            ->select('counselings.*', 'school_years.schoolYearStart', 'school_years.schoolYearEnd',
                    'students.firstName', 'students.lastName')        
            ->get();
        }elseif ($schoolYear == "all" && $semester != "all") {
            $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
            ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
            ->select('counselings.*', 'school_years.schoolYearStart', 'school_years.schoolYearEnd',
                    'students.firstName', 'students.lastName')        
            ->where('counselings.semesterID', '=', $semester)
            ->get();
        }elseif ($schoolYear != "all" && $semester == "all") {
            $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
            ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
            ->select('counselings.*', 'school_years.schoolYearStart', 'school_years.schoolYearEnd',
                    'students.firstName', 'students.lastName')  
            ->where('counselings.schoolYearID', '=', $schoolYear)
            ->get();
        }elseif ($schoolYear != "all" && $semester != "all") {
            $counseling = counseling::join('students', 'students.id', '=', 'counselings.studentID')
            ->join('school_years', 'school_years.id', '=', 'counselings.schoolYearID')
            ->select('counselings.*', 'school_years.schoolYearStart', 'school_years.schoolYearEnd',
                    'students.firstName', 'students.lastName')  
            ->where('counselings.schoolYearID', '=', $schoolYear)
            ->where('counselings.semesterID', '=', $semester)
            ->get();
        }
        if(count($counseling)==0){
            return "<h2>There are no counselings found</h2>";
        }else{
            $html ='';
            foreach ($counseling as $key => $value) {
                $status = '';
                if($this->counselingCollection($counseling[$key]->studentID, $counseling, $key, $studenttype, $courseGradeLevel, $schoolYear, $semester)){
                    continue;
                }
                switch ($counseling[$key]->counselingStatus) {
                    case '0':
                        $status = 'pending';
                    break;
                    case '1':
                        $status = 'followed up';
                    break;
                    case '2':
                        $status = 'evaluated';
                    break;
                    default:
                        # code...
                        break;
                }
                if($counseling[$key]->counselingType == 'group'){
                    $groupcounseling = groupCounseling::join('students', 'students.id', '=', 'group_counselings.studentID')
                                                        ->select('students.firstName', 'students.lastName', 'group_counselings.studentID')
                                                        ->where('counselingID', '=', $counseling[$key]->id)->get();
                    $html .="<tr>
                                <td>".++$counselingCounter."</td>
                                <td>".$counseling[$key]->firstName." ".$counseling[$key]->lastName."</td>
                                <td>".$counseling[$key]->semesterID."</td>
                                <td>".explode('-', $counseling[$key]->schoolYearStart)[0]." - ".explode('-', $counseling[$key]->schoolYearEnd)[0]."</td>
                                <td>".$counseling[$key]->dateScheduled."</td>
                                <td>".$counseling[$key]->dateRecorded."</td>
                                <td>".$status."</td>
                                <td>".$counseling[$key]->assistedBy."</td>
                            </tr>";
                    foreach ($groupcounseling as $key => $value) {
                        if($this->counselingCollection($groupcounseling[$key]->studentID, $groupcounseling, $key, $studenttype, $courseGradeLevel, $schoolYear, $semester)){
                            continue;
                        }
                        $html .="<tr>
                                    <td>".++$counselingCounter."</td>
                                    <td>".$groupcounseling[$key]->firstName." ".$groupcounseling[$key]->lastName."</td>
                                    <td>".$counseling[$key]->semesterID."</td>
                                    <td>".explode('-', $counseling[$key]->schoolYearStart)[0]." - ".explode('-', $counseling[$key]->schoolYearEnd)[0]."</td>
                                    <td>".$counseling[$key]->dateScheduled."</td>
                                    <td>".$counseling[$key]->dateRecorded."</td>
                                    <td>".$status."</td>
                                    <td>".$counseling[$key]->assistedBy."</td>
                                </tr>";
                    }
                }else{
                    $html .="<tr>
                                <td>".++$counselingCounter."</td>
                                <td>".$counseling[$key]->firstName." ".$counseling[$key]->lastName."</td>
                                <td>".$counseling[$key]->semesterID."</td>
                                <td>".explode('-', $counseling[$key]->schoolYearStart)[0]." - ".explode('-', $counseling[$key]->schoolYearEnd)[0]."</td>
                                <td>".$counseling[$key]->dateScheduled."</td>
                                <td>".$counseling[$key]->dateRecorded."</td>
                                <td>".$status."</td>
                                <td>".$counseling[$key]->assistedBy."</td>
                            </tr>";
                }
            }
            if($html == ""){
                return "<h2>There are no counselings found</h2>";
            }
            return $html;
        }
    }
    public function getCounselingFaster($viewby, $coursegrade, $gradelevel, $schoolyear, $semester, $counselingstatus, $counselor){
        $user = new userController();
        $systemdate = systemDate::all();
        $sql='';
        
        if(session()->get('user')['userType']=='admin'){
            $sql = "SELECT COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) 
                    semesterID, COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) 
                    schoolYearID, COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) studentTypes,
                    col.studentType, shs.studentType, jhs.studentType, elem.studentType,
                    COALESCE(col.yearLevel, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) courseGrade, 
                    col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel,
                    COALESCE(col.studentNo, shs.studNo, jhs.studNo, elem.studNo) studentNo,  students.firstName, students.lastName,
                    students.middleName,
                    col.courseName, col.yearLevel,
                    school_years.schoolYearStart,
                    school_years.schoolYearEnd,
                    counselings.*,
                    COALESCE(col.courseID, shs.seniorHighStrandID) strandCourse
                    FROM students LEFT JOIN (SELECT college_students.studentID, 
                    college_students.yearLevel, college_students.semesterID, college_students.schoolYearID,
                    college_students.id, 'college' AS studentType, 
                    college_students.courseID, courses.courseName, college_students.studentNo FROM
                    college_students INNER JOIN (SELECT max(id) as maxid FROM college_students GROUP 
                    BY studentID) cs ON cs.maxid = college_students.id
                    INNER JOIN courses ON courses.id = college_students.courseID) col ON 
                    col.studentID = students.id LEFT 
                    JOIN (SELECT s_h_s_students.studentID, s_h_s_students.semesterID, 
                    s_h_s_students.schoolYearID, s_h_s_students.id, 'seniorhigh' 
                    AS studentType, s_h_s_students.gradeLevel, s_h_s_students.studNo,  
                    senior_high_strands.strandName, s_h_s_students.seniorHighStrandID
                    FROM s_h_s_students INNER JOIN (SELECT max(id) as maxid FROM s_h_s_students GROUP BY studentID) 
                    shs ON shs.maxid = s_h_s_students.id INNER JOIN senior_high_strands ON senior_high_strands.id = 
                    s_h_s_students.seniorHighStrandID) shs ON shs.studentID = students.id
                    LEFT JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'juniorhigh' AS 
                    studentType, gradeLevel, studNo  FROM j_h_students INNER JOIN (SELECT max(id) as maxid FROM j_h_students 
                    GROUP BY studentID) jhs ON jhs.maxid = j_h_students.id) jhs ON jhs.studentID = students.id LEFT 
                    JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'elementary' AS studentType, 
                    gradeLevel, studNo FROM elem_students INNER JOIN (SELECT max(id) as maxid FROM elem_students GROUP 
                    BY studentID) elem ON elem.maxid = elem_students.id) elem ON elem.studentID = students.id
                    INNER JOIN counselings ON counselings.studentID =  students.id
                    INNER JOIN school_years ON school_years.id = counselings.schoolYearID
            WHERE DATE(students.created_at) <= '".$systemdate[0]->systemDate."'";
            switch ($viewby) {
                case 'college':
                    $sql .= " AND col.studentType = 'college'";
                break;
                case 'seniorhigh':
                    $sql .= " AND shs.studentType = 'seniorhigh'";
                break;
                case 'juniorhigh':
                    $sql .= " AND jhs.studentType = 'juniorhigh'";
                break;
                case 'elementary':
                    $sql .= " AND elem.studentType = 'elementary'";
                break;
                default:
                   
                break;
            }
            if($schoolyear!="all"){
                $sql .= " AND counselings.schoolYearID = '$schoolyear'";
            }
            if($semester!="all"){
                $sql .= " AND counselings.semesterID = '$semester'";
            }
            if($coursegrade!="all"){
                $sql .= " AND COALESCE(col.courseID, shs.seniorHighStrandID) = '$coursegrade'";
            }
            if($gradelevel != "all"){
                $sql .= " AND COALESCE(col.yearLevel, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) = '$coursegrade'";
            }
            if($counselingstatus != "all"){
                $sql .= " AND counselings.counselingStatus = '$counselingstatus'";
            }
            if($counselor != "all"){
                $sql .= " AND counselings.counselorID = '$counselor'"; 
            }
        }else{
            $designation = $user->getUserDesignation(Auth::guard('myuser')->id());
            $sql = "SELECT COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) 
                    semesterID, COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) 
                    schoolYearID, COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) studentTypes,
                    col.studentType, shs.studentType, jhs.studentType, elem.studentType, 
                    COALESCE(col.yearLevel, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) courseGrade, 
                    col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel,
                    COALESCE(col.studentNo, shs.studNo, jhs.studNo, elem.studNo) studentNo, students.firstName, students.lastName,
                    students.middleName,
                    school_years.schoolYearStart,
                    school_years.schoolYearEnd,
                    counselings.*,
                    col.courseName, col.yearLevel,
                    COALESCE(col.courseID, shs.seniorHighStrandID) strandCourse
                    FROM students LEFT JOIN (SELECT college_students.studentID, 
                    college_students.yearLevel, college_students.semesterID, college_students.schoolYearID,
                    college_students.id, 'college' AS studentType, 
                    college_students.courseID, courses.courseName, college_students.studentNo FROM
                    college_students INNER JOIN (SELECT max(id) as maxid FROM college_students GROUP 
                    BY studentID) cs ON cs.maxid = college_students.id
                    INNER JOIN courses ON courses.id = college_students.courseID) col ON 
                    col.studentID = students.id LEFT 
                    JOIN (SELECT s_h_s_students.studentID, s_h_s_students.semesterID, 
                    s_h_s_students.schoolYearID, s_h_s_students.id, 'seniorhigh' 
                    AS studentType, s_h_s_students.gradeLevel, s_h_s_students.studNo,  
                    senior_high_strands.strandName, s_h_s_students.seniorHighStrandID
                    FROM s_h_s_students INNER JOIN (SELECT max(id) as maxid FROM s_h_s_students GROUP BY studentID) 
                    shs ON shs.maxid = s_h_s_students.id INNER JOIN senior_high_strands ON senior_high_strands.id = 
                    s_h_s_students.seniorHighStrandID) shs ON shs.studentID = students.id
                    LEFT JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'juniorhigh' AS 
                    studentType, gradeLevel, studNo  FROM j_h_students INNER JOIN (SELECT max(id) as maxid FROM j_h_students 
                    GROUP BY studentID) jhs ON jhs.maxid = j_h_students.id) jhs ON jhs.studentID = students.id LEFT 
                    JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'elementary' AS studentType, 
                    gradeLevel, studNo FROM elem_students INNER JOIN (SELECT max(id) as maxid FROM elem_students GROUP 
                    BY studentID) elem ON elem.maxid = elem_students.id) elem ON elem.studentID = students.id
                    INNER JOIN counselings ON counselings.studentID =  students.id
                    INNER JOIN school_years ON school_years.id = counselings.schoolYearID
                    WHERE  DATE(students.created_at) <= '".$systemdate[0]->systemDate."'";
            switch($designation[0]){
                case 'college':
                    $sql= "SELECT cs2.courseID AS courseGrade, c2.courseName, cs2.schoolYearID, cs2.studentNo, 
                    cs2.yearLevel, students.firstName, students.lastName, 'college' AS studentTypes, cs2.semesterID,
                    students.middleName,
                    counselings.*,
                    school_years.schoolYearStart,
                    school_years.schoolYearEnd  FROM students 
                    INNER JOIN (SELECT studentID, studentNo,
                    id, courseID, schoolYearID, yearLevel, semesterID FROM college_students  INNER JOIN (SELECT max(id) as maxid 
                    FROM college_students 
                    GROUP BY studentID) cs ON cs.maxid = college_students.id) cs2 ON cs2.studentID = students.id 
                    INNER JOIN (SELECT c.id AS courseID, dep.departmentID, c.courseName FROM courses c INNER JOIN (SELECT
                    id, courseID FROM course_department_hists cd INNER JOIN (SELECT max(id) as maxid FROM 
                    course_department_hists GROUP BY courseID) cd2 ON cd2.maxid = cd.id) cdh ON cdh.courseID = c.id
                    INNER JOIN (SELECT departmentID, id FROM course_department_hists INNER JOIN (SELECT
                    max(id) AS maxid FROM course_department_hists GROUP BY courseID) cdh1 ON cdh1.maxid = 
                    course_department_hists.id) dep ON dep.id =cdh.id) c2 ON c2.courseID = cs2.courseID 
                    INNER JOIN (SELECT departmentID, id, collegeID FROM department_college_hists
                    INNER JOIN (SELECT max(id) as maxid FROM department_college_hists GROUP BY departmentID) 
                    depc ON depc.maxid = department_college_hists.id) depc2 ON depc2.departmentID=c2.departmentID 
                    INNER JOIN (SELECT collegeID, userID, id FROM assignment_colleges INNER JOIN (SELECT max(id) 
                    as maxid FROM assignment_colleges GROUP BY userID) assc ON assc.maxid = assignment_colleges.id) 
                    assc2 ON assc2.collegeID = depc2.collegeID 
                    INNER JOIN counselings ON counselings.studentID = students.id
                    INNER JOIN school_years ON school_years.id = counselings.schoolYearID
                    WHERE assc2.userID ='".Auth::guard('myuser')->id()."'
                    AND DATE(students.created_at) <= '".$systemdate[0]->systemDate."'";
                    if($schoolyear != "all"){
                        $sql .= " AND counselings.schoolYearID = '$schoolyear'";
                    }
                    if($semester != "all"){
                        $sql .= " AND counselings.semesterID = '$semester'";
                    }
                    if($coursegrade != "all"){
                        $sql .= " AND cs2.courseID = '$coursegrade'";
                    }
                    if($gradelevel != "all"){
                        $sql .= " AND cs2.yearLevel = '$gradelevel'";
                    }
                    if($counselingstatus != "all"){
                        $sql .= " AND counselings.counselingStatus = '$counselingstatus'";
                    }
                    if($counselor != "all"){
                        $sql .= " AND counselings.counselorID = '$counselor'"; 
                    }
                break;
                case 'seniorhigh':
                    $sql .= " AND  shs.studentType = 'seniorhigh' 
                              AND  shs.gradeLevel = '".$designation[1]."'";
                     if($schoolyear != "all"){
                        $sql .= " AND counselings.schoolYearID = '$schoolyear'";
                    }
                    if($semester != "all"){
                        $sql .= " AND counselings.semesterID = '$semester'";
                    }
                    if($counselingstatus != "all"){
                        $sql .= " AND counselings.counselingStatus = '$counselingstatus'";
                    }
                    if($coursegrade!="all"){
                        $sql .= " AND COALESCE(col.courseID, shs.seniorHighStrandID) = '$coursegrade'";
                    }
                    if($counselor != "all"){
                        $sql .= " AND counselings.counselorID = '$counselor'"; 
                    }
                break;
                case 'juniorhigh':
                    $sql .= " AND  jhs.studentType = 'juniorhigh' AND jhs.gradeLevel = '".$designation[1]."'";
                     if($schoolyear != "all"){
                        $sql .= " AND counselings.schoolYearID = '$schoolyear'";
                    }
                    if($semester != "all"){
                        $sql .= " AND counselings.semesterID = '$semester'";
                    }
                    if($counselingstatus != "all"){
                        $sql .= " AND counselings.counselingStatus = '$counselingstatus'";
                    }
                    if($counselor != "all"){
                        $sql .= " AND counselings.counselorID = '$counselor'"; 
                    }
                break;
                case 'elementary':
                    $sql .= " AND elem.studentType = 'elementary' AND  elem.gradeLevel = '".$designation[1]."'";
                    if($schoolyear != "all"){
                        $sql .= " AND counselings.schoolYearID = '$schoolyear'";
                    }
                    if($semester != "all"){
                        $sql .= " AND counselings.semesterID = '$semester'";
                    }
                    if($counselingstatus != "all"){
                        $sql .= " AND counselings.counselingStatus = '$counselingstatus'";
                    }
                    if($counselor != "all"){
                        $sql .= " AND counselings.counselorID = '$counselor'"; 
                    }
                break;
            }
        }
        
        
        $counseling = DB::select(DB::raw($sql));
        if(count($counseling)==0){
            return "<h2>There are no counselings found.</h2>";
        }else{
            $html = '';
            $rowCounter = 0;
            foreach ($counseling as $counselings) {
                $rowCounter++;
                $html .= $this->getCounselingRowFaster($counselings, $viewby, $rowCounter);
            }
            return $html;
        }
        
    }
    public function getCounselingRowFaster($counseling, $viewby, $rowCounter){
        $studenttype = '';
        if($viewby == 'all'){
            $studenttype = $counseling->studentTypes;
        }else{
            $studenttype =$viewby;
        }
        $gradelevel = '';
        if($counseling->yearLevel ==''){
            $gradelevel = $counseling->courseGrade;
        }else{
            $gradelevel = $counseling->yearLevel;
        }
        $status;
        
        switch ($counseling->counselingStatus) {
            case '0':
                $status = 'pending';
            break;
            case '1':
                $status = 'followed up';
            break;
            case '2':
                $status = 'evaluated';
            break;
            default:
                # code...
                break;
        }
        $schoolyearstart = explode('-', $counseling->schoolYearStart)[0];
        $schoolyearend = explode('-', $counseling->schoolYearEnd)[0];
        
        return "<tr id ='counselingRow-$counseling->id' data-value = '$counseling->studentID'>
                    <td>$rowCounter</td>
                    <td>".$this->getBeautifiedName($counseling->firstName, $counseling->middleName, $counseling->lastName)."</td>
                    <td>$counseling->semesterID</td>
                    <td>$schoolyearstart - $schoolyearend</td>
                    <td>".date("F j, Y", strtotime($counseling->dateScheduled))."</td>
                    <td>".date("F j, Y", strtotime($counseling->dateRecorded))."</td>
                    <td>".ucfirst($status)."</td>
                    <td>$counseling->assistedBy</td>
                    <td>$counseling->counseledBy</td>
                </tr>";
    }
    public function getYearOfSchoolYear($schoolyearid){
        $schoolyear = schoolYear::find($schoolyearid);
        
            return explode('-', $schoolyear->schoolYearStart)[0]." - ".explode('-', $schoolyear->schoolYearEnd)[0];
        
    }
    public function printCounselingReport($viewby, $coursegrade, $gradelevel, $schoolyear, $semester, $counselingstatus, $counselor){
        //$counseling;
        $thissemester = 'all Semester';
        $thisschoolyear = 'all School year';
        
        $user = new userController();
        $systemdate = systemDate::all();
        $sql='';
        
        if(session()->get('user')['userType']=='admin'){
            $sql = "SELECT COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) 
                semesterID, COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) 
                schoolYearID, COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) studentTypes,
                col.studentType, shs.studentType, jhs.studentType, elem.studentType,
                COALESCE(col.yearLevel, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) courseGrade, 
                col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel,
                COALESCE(col.studentNo, shs.studNo, jhs.studNo, elem.studNo) studentNo,  students.firstName, students.lastName,
                students.middleName,
                col.courseName, col.yearLevel,
                school_years.schoolYearStart,
                school_years.schoolYearEnd,
                counselings.*,
                COALESCE(col.courseID, shs.seniorHighStrandID) strandCourse
                FROM students LEFT JOIN (SELECT college_students.studentID, 
                college_students.yearLevel, college_students.semesterID, college_students.schoolYearID,
                college_students.id, 'college' AS studentType, 
                college_students.courseID, courses.courseName, college_students.studentNo FROM
                college_students INNER JOIN (SELECT max(id) as maxid FROM college_students GROUP 
                BY studentID) cs ON cs.maxid = college_students.id
                INNER JOIN courses ON courses.id = college_students.courseID) col ON 
                col.studentID = students.id LEFT 
                JOIN (SELECT s_h_s_students.studentID, s_h_s_students.semesterID, 
                s_h_s_students.schoolYearID, s_h_s_students.id, 'seniorhigh' 
                AS studentType, s_h_s_students.gradeLevel, s_h_s_students.studNo,  
                senior_high_strands.strandName, s_h_s_students.seniorHighStrandID
                FROM s_h_s_students INNER JOIN (SELECT max(id) as maxid FROM s_h_s_students GROUP BY studentID) 
                shs ON shs.maxid = s_h_s_students.id INNER JOIN senior_high_strands ON senior_high_strands.id = 
                s_h_s_students.seniorHighStrandID) shs ON shs.studentID = students.id
                LEFT JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'juniorhigh' AS 
                studentType, gradeLevel, studNo  FROM j_h_students INNER JOIN (SELECT max(id) as maxid FROM j_h_students 
                GROUP BY studentID) jhs ON jhs.maxid = j_h_students.id) jhs ON jhs.studentID = students.id LEFT 
                JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'elementary' AS studentType, 
                gradeLevel, studNo FROM elem_students INNER JOIN (SELECT max(id) as maxid FROM elem_students GROUP 
                BY studentID) elem ON elem.maxid = elem_students.id) elem ON elem.studentID = students.id
                INNER JOIN counselings ON counselings.studentID =  students.id
                INNER JOIN school_years ON school_years.id = counselings.schoolYearID
                WHERE DATE(students.created_at) <= '".$systemdate[0]->systemDate."'";
            switch ($viewby) {
                case 'college':
                    $sql .= " AND col.studentType = 'college'";
                break;
                case 'seniorhigh':
                    $sql .= " AND shs.studentType = 'seniorhigh'";
                break;
                case 'juniorhigh':
                    $sql .= " AND jhs.studentType = 'juniorhigh'";
                break;
                case 'elementary':
                    $sql .= " AND elem.studentType = 'elementary'";
                break;
                default:
                   
                break;
            }
            if($schoolyear!="all"){
                $thisschoolyear = $this->getYearOfSchoolYear($schoolyear)." School Year";
                $sql .= " AND counselings.schoolYearID = '$schoolyear'";
            }
            if($semester!="all"){
                switch ($semester) {
                    case '1':
                        $thissemester = '1st Semester';
                    break;
                    case '2':
                        $thissemester = '2nd Semester';
                    break;
                    case '3':
                        $thissemester = '3rd Semester';
                    break;
                    default:
                        # code...
                        break;
                }
                $sql .= " AND counselings.semesterID = '$semester'";
            }
            if($coursegrade!="all"){
                $sql .= " AND COALESCE(col.courseID, shs.seniorHighStrandID) = '$coursegrade'";
            }
            if($gradelevel != "all"){
                $sql .= " AND COALESCE(col.yearLevel, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) = '$coursegrade'";
            }
            if($counselor != "all"){
                $sql .= " AND counselings.counselorID = '$counselor'"; 
            }
            if($counselingstatus != "all"){
                $sql .= " AND counselings.counselingStatus = '$counselingstatus'";
            }
        }else{
            $designation = $user->getUserDesignation(Auth::guard('myuser')->id());
            $sql = "SELECT COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) 
                semesterID, COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) 
                schoolYearID, COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) studentTypes,
                col.studentType, shs.studentType, jhs.studentType, elem.studentType, 
                COALESCE(col.yearLevel, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) courseGrade, 
                col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel,
                COALESCE(col.studentNo, shs.studNo, jhs.studNo, elem.studNo) studentNo, students.firstName, students.lastName,
                students.middleName,
                school_years.schoolYearStart,
                school_years.schoolYearEnd,
                counselings.*,
                col.courseName, col.yearLevel,
                COALESCE(col.courseID, shs.seniorHighStrandID) strandCourse
                FROM students LEFT JOIN (SELECT college_students.studentID, 
                college_students.yearLevel, college_students.semesterID, college_students.schoolYearID,
                college_students.id, 'college' AS studentType, 
                college_students.courseID, courses.courseName, college_students.studentNo FROM
                college_students INNER JOIN (SELECT max(id) as maxid FROM college_students GROUP 
                BY studentID) cs ON cs.maxid = college_students.id
                INNER JOIN courses ON courses.id = college_students.courseID) col ON 
                col.studentID = students.id LEFT 
                JOIN (SELECT s_h_s_students.studentID, s_h_s_students.semesterID, 
                s_h_s_students.schoolYearID, s_h_s_students.id, 'seniorhigh' 
                AS studentType, s_h_s_students.gradeLevel, s_h_s_students.studNo,  
                senior_high_strands.strandName, s_h_s_students.seniorHighStrandID
                FROM s_h_s_students INNER JOIN (SELECT max(id) as maxid FROM s_h_s_students GROUP BY studentID) 
                shs ON shs.maxid = s_h_s_students.id INNER JOIN senior_high_strands ON senior_high_strands.id = 
                s_h_s_students.seniorHighStrandID) shs ON shs.studentID = students.id
                LEFT JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'juniorhigh' AS 
                studentType, gradeLevel, studNo  FROM j_h_students INNER JOIN (SELECT max(id) as maxid FROM j_h_students 
                GROUP BY studentID) jhs ON jhs.maxid = j_h_students.id) jhs ON jhs.studentID = students.id LEFT 
                JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'elementary' AS studentType, 
                gradeLevel, studNo FROM elem_students INNER JOIN (SELECT max(id) as maxid FROM elem_students GROUP 
                BY studentID) elem ON elem.maxid = elem_students.id) elem ON elem.studentID = students.id
                INNER JOIN counselings ON counselings.studentID =  students.id
                INNER JOIN school_years ON school_years.id = counselings.schoolYearID
                WHERE  DATE(students.created_at) <= '".$systemdate[0]->systemDate."'";
            switch($designation[0]){
                case 'college':
                    $sql= "SELECT cs2.courseID AS courseGrade, c2.courseName, cs2.schoolYearID, cs2.studentNo, 
                    cs2.yearLevel, students.firstName, students.lastName, 'college' AS studentTypes, cs2.semesterID,
                    students.middleName,
                    counselings.*,
                    school_years.schoolYearStart,
                    school_years.schoolYearEnd  FROM students 
                    INNER JOIN (SELECT studentID, studentNo,
                    id, courseID, schoolYearID, yearLevel, semesterID FROM college_students  INNER JOIN (SELECT max(id) as maxid 
                    FROM college_students 
                    GROUP BY studentID) cs ON cs.maxid = college_students.id) cs2 ON cs2.studentID = students.id 
                    INNER JOIN (SELECT c.id AS courseID, dep.departmentID, c.courseName FROM courses c INNER JOIN (SELECT
                    id, courseID FROM course_department_hists cd INNER JOIN (SELECT max(id) as maxid FROM 
                    course_department_hists GROUP BY courseID) cd2 ON cd2.maxid = cd.id) cdh ON cdh.courseID = c.id
                    INNER JOIN (SELECT departmentID, id FROM course_department_hists INNER JOIN (SELECT
                    max(id) AS maxid FROM course_department_hists GROUP BY courseID) cdh1 ON cdh1.maxid = 
                    course_department_hists.id) dep ON dep.id =cdh.id) c2 ON c2.courseID = cs2.courseID 
                    INNER JOIN (SELECT departmentID, id, collegeID FROM department_college_hists
                    INNER JOIN (SELECT max(id) as maxid FROM department_college_hists GROUP BY departmentID) 
                    depc ON depc.maxid = department_college_hists.id) depc2 ON depc2.departmentID=c2.departmentID 
                    INNER JOIN (SELECT collegeID, userID, id FROM assignment_colleges INNER JOIN (SELECT max(id) 
                    as maxid FROM assignment_colleges GROUP BY userID) assc ON assc.maxid = assignment_colleges.id) 
                    assc2 ON assc2.collegeID = depc2.collegeID 
                    INNER JOIN counselings ON counselings.studentID = students.id
                    INNER JOIN school_years ON school_years.id = counselings.schoolYearID
                    WHERE assc2.userID ='".Auth::guard('myuser')->id()."'
                    AND DATE(students.created_at) <= '".$systemdate[0]->systemDate."'";
                    if($schoolyear != "all"){
                        $thisschoolyear = $this->getYearOfSchoolYear($schoolyear)." School Year";
                        $sql .= " AND counselings.schoolYearID = '$schoolyear'";
                    }
                    if($semester != "all"){
                        switch ($semester) {
                            case '1':
                                $thissemester = '1st Semester';
                            break;
                            case '2':
                                $thissemester = '2nd Semester';
                            break;
                            case '3':
                                $thissemester = '3rd Semester';
                            break;
                            default:
                                # code...
                                break;
                        }
                        $sql .= " AND counselings.semesterID = '$semester'";
                    }
                    if($coursegrade != "all"){
                        $sql .= " AND cs2.courseID = '$coursegrade'";
                    }
                 
                    if($gradelevel != "all"){
                        $sql .= " AND COALESCE(col.yearLevel, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) = '$coursegrade'";
                    }
                    if($counselor != "all"){
                        $sql .= " AND counselings.counselorID = '$counselor'"; 
                    }
                    if($counselingstatus != "all"){
                        $sql .= " AND counselings.counselingStatus = '$counselingstatus'";
                    }
                break;
                case 'seniorhigh':
                    $sql .= " AND  shs.studentType = 'seniorhigh' 
                              AND  shs.gradeLevel = '".$designation[1]."'";
                     if($schoolyear != "all"){
                        $thisschoolyear = $this->getYearOfSchoolYear($schoolyear)." School Year";
                        $sql .= " AND counselings.schoolYearID = '$schoolyear'";
                    }
                    if($semester != "all"){
                        switch ($semester) {
                            case '1':
                                $thissemester = '1st Semester';
                            break;
                            case '2':
                                $thissemester = '2nd Semester';
                            break;
                            case '3':
                                $thissemester = '3rd Semester';
                            break;
                            default:
                                # code...
                                break;
                        }
                        $sql .= " AND counselings.semesterID = '$semester'";
                    }
                    if($coursegrade!="all"){
                        $sql .= " AND COALESCE(col.courseID, shs.seniorHighStrandID) = '$coursegrade'";
                    }
                    if($gradelevel != "all"){
                        $sql .= " AND COALESCE(col.yearLevel, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) = '$coursegrade'";
                    }
                    if($counselor != "all"){
                        $sql .= " AND counselings.counselorID = '$counselor'"; 
                    }
                    if($counselingstatus != "all"){
                        $sql .= " AND counselings.counselingStatus = '$counselingstatus'";
                    }
                break;
                case 'juniorhigh':
                    $sql .= " AND  jhs.studentType = 'juniorhigh' AND jhs.gradeLevel = '".$designation[1]."'";
                     if($schoolyear != "all"){
                        $thisschoolyear = $this->getYearOfSchoolYear($schoolyear)." School Year";
                        $sql .= " AND counselings.schoolYearID = '$schoolyear'";
                    }
                    if($semester != "all"){
                        switch ($semester) {
                            case '1':
                                $thissemester = '1st Semester';
                            break;
                            case '2':
                                $thissemester = '2nd Semester';
                            break;
                            case '3':
                                $thissemester = '3rd Semester';
                            break;
                            default:
                                # code...
                                break;
                        }
                        $sql .= " AND counselings.semesterID = '$semester'";
                    }
                  
                    if($gradelevel != "all"){
                        $sql .= " AND COALESCE(col.yearLevel, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) = '$coursegrade'";
                    }
                    if($counselor != "all"){
                        $sql .= " AND counselings.counselorID = '$counselor'"; 
                    }
                    if($counselingstatus != "all"){
                        $sql .= " AND counselings.counselingStatus = '$counselingstatus'";
                    }
                break;
                case 'elementary':
                    $sql .= " AND elem.studentType = 'elementary' AND  elem.gradeLevel = '".$designation[1]."'";
                    if($schoolyear != "all"){
                        $thisschoolyear = $this->getYearOfSchoolYear($schoolyear)." School Year";
                        $sql .= " AND counselings.schoolYearID = '$schoolyear'";
                    }
                    if($semester != "all"){
                        switch ($semester) {
                            case '1':
                                $thissemester = '1st Semester';
                            break;
                            case '2':
                                $thissemester = '2nd Semester';
                            break;
                            case '3':
                                $thissemester = '3rd Semester';
                            break;
                            default:
                                # code...
                                break;
                        }
                        $sql .= " AND counselings.semesterID = '$semester'";
                    }
                    if($gradelevel != "all"){
                        $sql .= " AND COALESCE(col.yearLevel, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) = '$coursegrade'";
                    }
                    if($counselor != "all"){
                        $sql .= " AND counselings.counselorID = '$counselor'"; 
                    }
                    if($counselingstatus != "all"){
                        $sql .= " AND counselings.counselingStatus = '$counselingstatus'";
                    }
                break;
            }
        }
        
        
        $counseling = DB::select(DB::raw($sql));
        $title = "List of Students with Counseling for ".$thissemester." and ".$thisschoolyear."";
        $html ='';
        if(count($counseling)==0){
            $html = "<tr><td colspan='7'><b>There are no counselings found</b></td></tr>";
        }else{
            $counter = 0;
            foreach ($counseling as $key => $value) {
                $status = '';
                // if($this->counselingCollection($counseling[$key]->studentID, $counseling, $key, $studenttype, $courseGradeLevel, $schoolYear, $semester)){
                //     continue;
                // }
                switch ($counseling[$key]->counselingStatus) {
                    case '0':
                        $status = 'pending';
                    break;
                    case '1':
                        $status = 'followed up';
                    break;
                    case '2':
                        $status = 'evaluated';
                    break;
                    default:
                        # code...
                        break;
                }
                if($counseling[$key]->counselingType == 'group'){
                    $groupcounseling = groupCounseling::join('students', 'students.id', '=', 'group_counselings.studentID')
                                                        ->select('students.firstName', 'students.lastName', 'group_counselings.studentID')
                                                        ->where('counselingID', '=', $counseling[$key]->id)->get();
                    $html .="<tr>
                                <td>".(++$counter)."</td>
                                <td>".$counseling[$key]->firstName." ".$counseling[$key]->lastName."</td>
                                
                                <td>".date("F j, Y", strtotime($counseling[$key]->dateScheduled))."</td>
                                <td>".date("F j, Y", strtotime($counseling[$key]->dateRecorded))."</td>
                                <td>".ucfirst($counseling[$key]->counselingType)."</td>
                                <td>".ucfirst($status)."</td>
                                <td>".$counseling[$key]->assistedBy."</td>
                                <td>".$counseling[$key]->counseledBy."</td>
                            </tr>";
                    
                    foreach ($groupcounseling as $skey => $value) {
                        // if($this->counselingCollection($groupcounseling[$skey]->studentID, $groupcounseling, $skey, $studenttype, $courseGradeLevel, $schoolYear, $semester)){
                        //     continue;
                        // }
                        $html .="<tr>
                                    <td>".(++$counter)."</td>
                                    <td>".$groupcounseling[$skey]->firstName." ".$groupcounseling[$skey]->lastName."</td>
                                    
                                    <td>".date("F j, Y", strtotime($counseling[$key]->dateScheduled))."</td>
                                    <td>".date("F j, Y", strtotime($counseling[$key]->dateRecorded))."</td>
                                    <td>".ucfirst($counseling[$key]->counselingType)."</td>
                                    <td>".ucfirst($status)."</td>
                                    <td>".$counseling[$key]->assistedBy."</td>
                                    <td>".$counseling[$key]->counseledBy."</td>
                            </tr>";
                    }
                }else{
                    $html .="<tr>
                                <td>".(++$counter)."</td>
                                <td>".$counseling[$key]->firstName." ".$counseling[$key]->lastName."</td>
                                
                                <td>".date("F j, Y", strtotime($counseling[$key]->dateScheduled))."</td>
                                <td>".date("F j, Y", strtotime($counseling[$key]->dateRecorded))."</td>
                                <td>".ucfirst($counseling[$key]->counselingType)."</td>
                                <td>".ucfirst($status)."</td>
                                <td>".$counseling[$key]->assistedBy."</td>
                                <td>".$counseling[$key]->counseledBy."</td>
                            </tr>";
                }
            }
            if($html == ""){
                $html= "<tr><td colspan='7'><b>There are no counselings found</b></td></tr>";
            }
           
        }
        $myhtml = "
        <html>
            <head>
                <style>
                    table {
                        font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif;
                        border-collapse: collapse;
                        width: 100%;
                    }
                    .well{
                        min-height: 20px;
                        padding: 19px;
                        margin-bottom: 20px;
                        background-color: #f5f5f5;
                        border: 1px solid #e3e3e3;
                        border-radius: 4px;
                        -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
                        box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
                    }
                    table td, table th {
                        border: 1px solid #ddd;
                        padding: 8px;
                    }
                    
                    table tr:nth-child(even){background-color: #f2f2f2;}
                    
                    table tr:hover {background-color: #ddd;}
                    
                    table th {
                        padding-top: 12px;
                        padding-bottom: 12px;
                        text-align: left;
                        background-color: #4CAF50;
                        color: white;
                    }
                    .filterContainer{
                        display:inline-block;
                        width:23%;
                        
                    }
                    @page{
                        margin-left:20px;
                        margin-right:20px;
                        margin-top:160px;
                    }
                    .imageWrapper {
                        float:left;
                    
                    }
                    .thisImage{
                    height:200px;
                    width:150px;
                    }
                    
                </style>
            </head>
            <body>
                ".$this->getAdminSetting()."
                <div style = 'margin-bottom:20px; margin-top:20px;'>
                    <center><h2 style = 'text-decoration:underline;'>$title</h2></center>
                </div>
                <table>
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Student</th>
                           
                            <th>Date Scheduled</th>
                            <th>Date Recorded</th>
                            <th>Counseling Type</th>
                            <th>Status</th>
                            <th>Assisted By</th>
                            <th>Counseled By</th>
                        </tr>
                    </thead>
                    <tbody>

                    
        ";
        $myhtml .= $html;
        $html.="
                        </tbody>
                    </table>
                </body>
            </html>";
        
        PDF::loadHTML($myhtml)->save(public_path("temp/".'report.pdf'));
        return "true";
    }
    public function getAdminSetting(){
        $temp = new \stdClass();
        $count = adminsetting::all();
        if(count($count)>0){
            $adminsetting = adminsetting::find($count[0]->id);
            $html = "<header style = 'position:fixed; top:-100px; left:0px; right:0px; height: 60px;'>
                        <div style = 'width:19%; display:inline-block;  padding: 0px 0px 0px 50px;'>
                            <img style = 'height:100px; width:100px; border-radius:50px;' src = 'images-database/adminsetting/schoolimage/$adminsetting->id.$adminsetting->schoolSealImageType'>
                        </div>
                        <div style = 'width:50%; height:120px; display:inline-block;  margin:0px;'>
                            <center>
                                <h4 class='header' style='color:black; margin:0px;'>$adminsetting->schoolName</h4>
                                <h4 class='header' style='color:black; margin:0px;'>$adminsetting->guidanceName</h4>
                                <h4 class='header' style='color:black; margin:0px;'>$adminsetting->streetAddress, $adminsetting->barangay</h4>
                                <h4 class='header' style='color:black; margin:0px;'>$adminsetting->city</h4>
                                <h4 class='header' style='color:black; margin:0px;'>Philippines, 7000</h4>
                            </center>    
                        </div>
                        <div style = 'width:15%; display:inline-block; padding: 0px 50px 0px 0px;  text-align: right;'>
                            <img style = 'height:100px; width:100px; border-radius:50px;' src = 'images-database/adminsetting/guidanceimage/$adminsetting->id.$adminsetting->guidanceSealImageType'>
                        </div>
                    </header>";
            return $html;
        }else{
            return "";
        }
    }
    public function getCounselorCombo(){
        $counselor = counselor::all();
        $html = '';
        foreach ($counselor as $counselors) {
            $html .= "<option value = '$counselors->id'> $counselors->firstName $counselors->lastName</option>";
        }
        return $html;
    }
    public function getBehaviourSettingReferral(){
        $behaviour = behaviourSetting::all();
        $html = '';
        foreach ($behaviour as $behaviours) {
            $html .= "<option value = '$behaviours->id'>$behaviours->name</option>";
        }
        return $html;
    }
}
