<?php

namespace App\Http\Controllers\referral;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Http\Controllers\student\studentController;
use App\Http\Controllers\activityController;
use App\Http\Controllers\notification\notificationController;
use App\model\schoolYear;
use App\model\student;
use App\model\referral;
use App\model\collegeStudent;
use App\model\SHSStudent;
use App\model\JHStudent;
use App\model\elemStudent;
use App\model\behaviour;
use App\model\systemDate;
use App\model\useraccount;
use App\model\assignmentCollege;
use App\model\family;
use App\model\referralMessage;
use App\model\adminsetting;
use PDF;
use DB;
use Auth;
use App\Rules\systemTimeRule;
use App\Http\Controllers\users\userController;
class referralController extends Controller
{
    private $request; 
    private $html; 
    private $rowCounter;
    public function __construct(){
        $this->middleware('auth:myuser');
    }
    public function getReferralPage(){
        $schoolyearid = (new studentController)->getCurrentSchoolYear();
        $schoolyear = schoolYear::find($schoolyearid);
        //$studentstable = student::all();
        $systemdate = systemDate::all();
       
        $schoolyears = schoolYear::all();
        $nameofuser = useraccount::find(Auth::guard('myuser')->id());
        $studentstable = student::whereDate('created_at', '<=', $systemdate[0]->systemDate)->get();
        $currentsem = (new studentController)->getCurrentSem();
        $user =  (new userController)->getUserDesignation(Auth::guard('myuser')->id());
        $college ='';
        if($user[0]=='college'){
            $college = DB::select(DB::raw("SELECT colleges.collegeName, courses.courseName, 
            courses.id FROM colleges INNER JOIN (SELECT collegeID, userID, id FROM 
            assignment_colleges INNER JOIN (SELECT max(id) AS maxid FROM assignment_colleges 
            GROUP BY userID) ac ON ac.maxid = assignment_colleges.id) ac2 ON ac2.collegeID = 
            colleges.id INNER JOIN (SELECT collegeID, departmentID, id FROM 
            department_college_hists INNER JOIN (SELECT max(id) as maxid FROM 
            department_college_hists GROUP BY departmentID) dch ON dch.maxid = 
            department_college_hists.id) dch2 ON dch2.collegeID = colleges.id 
            INNER JOIN (SELECT courseID, departmentID, id FROM course_department_hists 
            INNER JOIN (SELECT max(id) as maxid FROM course_department_hists GROUP BY courseID) 
            cdh ON cdh.maxid = course_department_hists.id) cdh2 ON cdh2.departmentID = 
            dch2.departmentID INNER JOIN courses on courses.id  = cdh2.courseID WHERE 
            ac2.collegeID = (SELECT collegeID FROM assignment_colleges WHERE id = 
            (SELECT max(id) FROM assignment_colleges WHERE userID ='".Auth::guard('myuser')->id()."'))
            GROUP BY courses.id"));
        }
        $notif = (new notificationController)->getNotifs();
        return view('admin/referral/referral', ['studentstable' =>$studentstable,
                                                'schoolyear' =>explode('-', $schoolyear->schoolYearStart)[0].'-'.explode('-', $schoolyear->schoolYearEnd)[0],
                                                'currentsem' =>$currentsem,
                                                'schoolyears'=>$schoolyears,
                                                'systemdate'=>$systemdate[0]->systemDate,
                                                'nameofuser' => $nameofuser->firstName." ".$nameofuser->lastName,
                                                'user'=>$user,
                                                'collegeOfUser'=>$college,
                                                'notif'=>$notif]);
    }
    public function insertReferral(Request $request){
        $this->validate($request, array(
            'dateReferralFormText'=>['required', new systemTimeRule],
            'referredToReferralFormText'=>'required|max:100',
            'referredByReferralFormText'=>'required|max:100'
        ));
        
        //error_log($arr[0]->frequency);
        $this->request = $request;
        DB::transaction(function() {
            $arr = json_decode($this->request->behavior);
            $referral = new referral;
            $referral->studentID =$this->request->studentID;
            $referral->semesterID = (new studentController)->getCurrentSem();
            $referral->schoolYearID =(new studentController)->getCurrentSchoolYear();
            $referral->referralDate = $this->request->dateReferralFormText;
            $referral->referredTo = $this->request->referredToReferralFormText;
            $referral->referredBy = $this->request->referredByReferralFormText;
            $referral->universityCollegeID = $this->request->unviersityCollege;
            $referral->collegeName = $this->request->collegeName;
            $referral->save();
            $myitems;
            for ($i=0; $i < count($arr); $i++) { 
               
                $myitems[$i]['behavioural']= $arr[$i]->behavior;
                $myitems[$i]['frequency']= $arr[$i]->frequency;
                $myitems[$i]['remarks']= $arr[$i]->remarks;
                $myitems[$i]['referralID']= $referral->id;
            }
            DB::table("behaviours")->insert($myitems);
            $referral->schoolYearEnd = schoolYear::find($referral->schoolYearID)->schoolYearEnd;
            $referral->schoolYearStart = schoolYear::find($referral->schoolYearID)->schoolYearStart;
            $referral->firstName = student::find($referral->studentID)->firstName;
            $referral->lastName = student::find($referral->studentID)->lastName;
            $referral->middleName = student::find($referral->studentID)->middleName;
            $this->rowCounter = $this->request->rowCounter;
            $this->html = $this->getReferralRow($referral);
        });
        activityController::insertActivity('Inserted a referral for student with student id:'.$this->request->studentID);
        return $this->html;
    }
    public function referralCollection($courseGrade, &$referral){
        $mystudent = new studentController;
        foreach ($referral as $key => $value) {
            $studenttype = $mystudent->getStudentType($referral[$key]->studentID);
            switch ($studenttype) {
                case 'college':
                    $temp = collegeStudent::where('studentID', '=', $referral[$key]->studentID)->get();
                    if(count($temp)>0){
                        if($temp[0]->courseID != $courseGrade){
                            $referral->forget($key);
                        }
                    }
                break;
                case 'seniorhigh':
                    $temp = SHSStudent::where('studentID', '=', $referral[$key]->studentID)->get();
                    if(count($temp)>0){
                        if($temp[0]->gradeLevel != $courseGrade){
                            $referral->forget($key);
                        }
                    }
                break;
                case 'juniorhigh':
                    $temp = JHStudent::where('studentID', '=', $referral[$key]->studentID)->get();
                    if(count($temp)>0){
                        if($temp[0]->gradeLevel != $courseGrade){
                            $referral->forget($key);
                        }
                    }
                break;
                case 'elementary':
                    $temp = elemStudent::where('studentID', '=', $referral[$key]->studentID)->get();
                    if(count($temp)>0){
                        if($temp[0]->gradeLevel != $courseGrade){
                            $referral->forget($key);
                        }
                    }
                break;
                default:
                    # code...
                    break;
            }
        }
    }
    public function getBeautifiedStudentType($studenttype){
        switch ($studenttype) {
            case 'college':
                return "College";
            break;
            case 'seniorhigh':
                return "Senior High";
            break;
            case 'juniorhigh':
                return "Junior High";
            break;
            case 'elementary':
                return "Elementary";
            break;
            default:
                # code...
                break;
        }
    }
    public function getReferralTotalRow($viewby, $schoolyear, $semester, $courseGrade, $search = ''){
        $systemdate = systemDate::all();
        $count;
        switch($viewby){
            case 'all':
                
                if($schoolyear == 'all' && $semester == 'all' && $courseGrade == 'all'){
                    $count = referral::join('students', 'students.id', '=', 'referrals.studentID')
                            ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                            ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                            'students.firstName', 'students.lastName', 'referrals.id',
                            'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                            ->where(function($query) use($search){
                                $query->where('students.firstName', 'LIKE', "%$search%");
                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                            })
                            ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                            ->count();
                }elseif ($schoolyear == 'all' && $semester == 'all' && $courseGrade != 'all') {
                   $temp = referral::join('students', 'students.id', '=', 'referrals.studentID')
                            ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                            ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                            'students.firstName', 'students.lastName', 'referrals.id',
                            'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                            ->where(function($query) use($search){
                                $query->where('students.firstName', 'LIKE', "%$search%");
                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                            })
                            ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                            ->get();
                    $this->referralCollection($courseGrade, $temp);
                    $count = count($temp);
                }elseif ($schoolyear == 'all' && $semester != 'all' && $courseGrade == 'all') {
                    $count = referral::join('students', 'students.id', '=', 'referrals.studentID')
                            ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                            ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                            'students.firstName', 'students.lastName', 'referrals.id',
                            'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                            ->where(function($query) use($search){
                                $query->where('students.firstName', 'LIKE', "%$search%");
                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                            })
                            ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                            ->where('referrals.semesterID', '=', $semester)
                            ->count();
                }elseif ($schoolyear == 'all' && $semester != 'all' && $courseGrade != 'all') {
                    $temp = referral::join('students', 'students.id', '=', 'referrals.studentID')
                                    ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                                    ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                                    'students.firstName', 'students.lastName', 'referrals.id',
                                    'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                                    ->where(function($query) use($search){
                                        $query->where('students.firstName', 'LIKE', "%$search%");
                                        $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                        $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                        $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                    })
                                    ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                                    ->where('referrals.semesterID', '=', $semester)
                                    ->get();
                    $this->referralCollection($courseGrade, $temp);
                    $count = count($temp);
                }elseif ($schoolyear != 'all' && $semester == 'all' && $courseGrade == 'all') {
                    $count = referral::join('students', 'students.id', '=', 'referrals.studentID')
                            ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                            ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                            'students.firstName', 'students.lastName', 'referrals.id',
                            'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                            ->where(function($query) use($search){
                                $query->where('students.firstName', 'LIKE', "%$search%");
                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                            })
                            ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                            ->where('referrals.schoolYearID', '=', $schoolyear)
                            ->count();
                }elseif ($schoolyear != 'all' && $semester == 'all' && $courseGrade != 'all') {
                    $temp = referral::join('students', 'students.id', '=', 'referrals.studentID')
                                    ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                                    ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                                    'students.firstName', 'students.lastName', 'referrals.id',
                                    'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                                    ->where(function($query) use($search){
                                        $query->where('students.firstName', 'LIKE', "%$search%");
                                        $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                        $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                        $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                    })
                                    ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                                    ->where('referrals.schoolYearID', '=', $schoolyear)
                                    ->get();
                    $this->referralCollection($courseGrade, $temp);
                    $count = count($temp);
                }elseif ($schoolyear != 'all' && $semester != 'all' && $courseGrade == 'all') {
                    $count = referral::join('students', 'students.id', '=', 'referrals.studentID')
                            ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                            ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                            'students.firstName', 'students.lastName', 'referrals.id',
                            'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                            ->where(function($query) use($search){
                                $query->where('students.firstName', 'LIKE', "%$search%");
                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                            })
                            ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                            ->where('referrals.schoolYearID', '=', $schoolyear)
                            ->where('referrals.semesterID', '=', $semester)
                            ->count();                    
                }elseif ($schoolyear != 'all' && $semester != 'all' && $courseGrade != 'all') {
                    $temp = referral::join('students', 'students.id', '=', 'referrals.studentID')
                                    ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                                    ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                                    'students.firstName', 'students.lastName', 'referrals.id',
                                    'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                                    ->where(function($query) use($search){
                                        $query->where('students.firstName', 'LIKE', "%$search%");
                                        $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                        $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                        $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                    })
                                    ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                                    ->where('referrals.schoolYearID', '=', $schoolyear)
                                    ->where('referrals.semesterID', '=', $semester)
                                    ->get();    
                    $this->referralCollection($courseGrade, $temp);
                    $count = count($temp);
                }else{
                    $count = referral::join('students', 'students.id', '=', 'referrals.studentID')
                                    ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                                    ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                                    'students.firstName', 'students.lastName', 'referrals.id',
                                    'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                                    ->where(function($query) use($search){
                                        $query->where('students.firstName', 'LIKE', "%$search%");
                                        $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                        $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                        $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                    })
                                    ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                                    ->count();
                }       
            break;
            case 'college':
                if($schoolyear == 'all' && $semester == 'all' && $courseGrade == 'all'){
                    $count = referral::join('students', 'students.id', '=', 'referrals.studentID')
                            ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                            ->join('college_students', 'college_students.studentID', '=', 'students.id')
                            ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                            'students.firstName', 'students.lastName', 'referrals.id',
                            'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                            ->where(function($query) use($search){
                                $query->where('students.firstName', 'LIKE', "%$search%");
                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                            })
                            ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                            ->count();
                }elseif ($schoolyear == 'all' && $semester == 'all' && $courseGrade != 'all') {
                    $count = referral::join('students', 'students.id', '=', 'referrals.studentID')
                                ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                                ->join('college_students', 'college_students.studentID', '=', 'students.id')
                                ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                                'students.firstName', 'students.lastName', 'referrals.id',
                                'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                                ->where(function($query) use($search){
                                    $query->where('students.firstName', 'LIKE', "%$search%");
                                    $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                    $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                    $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                })
                                ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                                ->where('college_students.courseID', '=', $courseGrade)
                                ->count();
                        
                }elseif ($schoolyear == 'all' && $semester != 'all' && $courseGrade == 'all') {
                    $count = referral::join('students', 'students.id', '=', 'referrals.studentID')
                            ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                            ->join('college_students', 'college_students.studentID', '=', 'students.id')
                            ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                            'students.firstName', 'students.lastName', 'referrals.id',
                            'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                            ->where(function($query) use($search){
                                $query->where('students.firstName', 'LIKE', "%$search%");
                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                            })
                            ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                            ->where('referrals.semesterID', '=', $semester)
                            ->count();
                }elseif ($schoolyear == 'all' && $semester != 'all' && $courseGrade != 'all') {
                    $count = referral::join('students', 'students.id', '=', 'referrals.studentID')
                                    ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                                    ->join('college_students', 'college_students.studentID', '=', 'students.id')
                                    ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                                    'students.firstName', 'students.lastName', 'referrals.id',
                                    'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                                    ->where(function($query) use($search){
                                        $query->where('students.firstName', 'LIKE', "%$search%");
                                        $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                        $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                        $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                    })
                                    ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                                    ->where('referrals.semesterID', '=', $semester)
                                    ->where('college_students.courseID', '=', $courseGrade)
                                    ->count();
                   
                }elseif ($schoolyear != 'all' && $semester == 'all' && $courseGrade == 'all') {
                    $count = referral::join('students', 'students.id', '=', 'referrals.studentID')
                            ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                            ->join('college_students', 'college_students.studentID', '=', 'students.id')
                            ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                            'students.firstName', 'students.lastName', 'referrals.id',
                            'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                            ->where(function($query) use($search){
                                $query->where('students.firstName', 'LIKE', "%$search%");
                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                            })
                            ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                            ->where('referrals.schoolYearID', '=', $schoolyear)
                            ->count();
                }elseif ($schoolyear != 'all' && $semester == 'all' && $courseGrade != 'all') {
                    $count = referral::join('students', 'students.id', '=', 'referrals.studentID')
                                    ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                                    ->join('college_students', 'college_students.studentID', '=', 'students.id')
                                    ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                                    'students.firstName', 'students.lastName', 'referrals.id',
                                    'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                                    ->where(function($query) use($search){
                                        $query->where('students.firstName', 'LIKE', "%$search%");
                                        $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                        $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                        $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                    })
                                    ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                                    ->where('referrals.schoolYearID', '=', $schoolyear)
                                    ->where('college_students.courseID', '=', $courseGrade)
                                    ->count();
                   
                }elseif ($schoolyear != 'all' && $semester != 'all' && $courseGrade == 'all') {
                    $count = referral::join('students', 'students.id', '=', 'referrals.studentID')
                            ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                            ->join('college_students', 'college_students.studentID', '=', 'students.id')
                            ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                            'students.firstName', 'students.lastName', 'referrals.id',
                            'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                            ->where(function($query) use($search){
                                $query->where('students.firstName', 'LIKE', "%$search%");
                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                            })
                            ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                            ->where('referrals.schoolYearID', '=', $schoolyear)
                            ->where('referrals.semesterID', '=', $semester)
                            ->count();                    
                }elseif ($schoolyear != 'all' && $semester != 'all' && $courseGrade != 'all') {
                    $count = referral::join('students', 'students.id', '=', 'referrals.studentID')
                                    ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                                    ->join('college_students', 'college_students.studentID', '=', 'students.id')
                                    ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                                    'students.firstName', 'students.lastName', 'referrals.id',
                                    'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                                    ->where(function($query) use($search){
                                        $query->where('students.firstName', 'LIKE', "%$search%");
                                        $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                        $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                        $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                    })
                                    ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                                    ->where('referrals.schoolYearID', '=', $schoolyear)
                                    ->where('referrals.semesterID', '=', $semester)
                                    ->where('college_students.courseID', '=', $courseGrade)
                                    ->count();    
                    
                }else{
                    $count = referral::join('students', 'students.id', '=', 'referrals.studentID')
                                    ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                                    ->join('college_students', 'college_students.studentID', '=', 'students.id')
                                    ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                                    'students.firstName', 'students.lastName', 'referrals.id',
                                    'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                                    ->where(function($query) use($search){
                                        $query->where('students.firstName', 'LIKE', "%$search%");
                                        $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                        $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                        $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                    })
                                    ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                                    ->count();
                } 
            break;
            case 'seniorhigh':
                if($schoolyear == 'all' && $semester == 'all' && $courseGrade == 'all'){
                    $count = referral::join('students', 'students.id', '=', 'referrals.studentID')
                            ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                            ->join('s_h_s_students', 's_h_s_students.studentID', '=', 'students.id')
                            ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                            'students.firstName', 'students.lastName', 'referrals.id',
                            'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                            ->where(function($query) use($search){
                                $query->where('students.firstName', 'LIKE', "%$search%");
                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                            })
                            ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                            ->count();
                }elseif ($schoolyear == 'all' && $semester == 'all' && $courseGrade != 'all') {
                    $count = referral::join('students', 'students.id', '=', 'referrals.studentID')
                                ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                                ->join('s_h_s_students', 's_h_s_students.studentID', '=', 'students.id')
                                ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                                'students.firstName', 'students.lastName', 'referrals.id',
                                'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                                ->where(function($query) use($search){
                                    $query->where('students.firstName', 'LIKE', "%$search%");
                                    $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                    $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                    $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                })
                                ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                                ->where('s_h_s_students.gradeLevel', '=', $courseGrade)
                                ->count();
                        
                }elseif ($schoolyear == 'all' && $semester != 'all' && $courseGrade == 'all') {
                    $count = referral::join('students', 'students.id', '=', 'referrals.studentID')
                            ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                            ->join('s_h_s_students', 's_h_s_students.studentID', '=', 'students.id')
                            ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                            'students.firstName', 'students.lastName', 'referrals.id',
                            'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                            ->where(function($query) use($search){
                                $query->where('students.firstName', 'LIKE', "%$search%");
                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                            })
                            ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                            ->where('referrals.semesterID', '=', $semester)
                            ->count();
                }elseif ($schoolyear == 'all' && $semester != 'all' && $courseGrade != 'all') {
                    $count = referral::join('students', 'students.id', '=', 'referrals.studentID')
                                    ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                                    ->join('s_h_s_students', 's_h_s_students.studentID', '=', 'students.id')
                                    ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                                    'students.firstName', 'students.lastName', 'referrals.id',
                                    'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                                    ->where(function($query) use($search){
                                        $query->where('students.firstName', 'LIKE', "%$search%");
                                        $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                        $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                        $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                    })
                                    ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                                    ->where('referrals.semesterID', '=', $semester)
                                    ->where('s_h_s_students.gradeLevel', '=', $courseGrade)
                                    ->count();
                
                }elseif ($schoolyear != 'all' && $semester == 'all' && $courseGrade == 'all') {
                    $count = referral::join('students', 'students.id', '=', 'referrals.studentID')
                            ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                            ->join('s_h_s_students', 's_h_s_students.studentID', '=', 'students.id')
                            ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                            'students.firstName', 'students.lastName', 'referrals.id',
                            'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                            ->where(function($query) use($search){
                                $query->where('students.firstName', 'LIKE', "%$search%");
                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                            })
                            ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                            ->where('referrals.schoolYearID', '=', $schoolyear)
                            ->count();
                }elseif ($schoolyear != 'all' && $semester == 'all' && $courseGrade != 'all') {
                    $count = referral::join('students', 'students.id', '=', 'referrals.studentID')
                                    ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                                    ->join('s_h_s_students', 's_h_s_students.studentID', '=', 'students.id')
                                    ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                                    'students.firstName', 'students.lastName', 'referrals.id',
                                    'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                                    ->where(function($query) use($search){
                                        $query->where('students.firstName', 'LIKE', "%$search%");
                                        $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                        $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                        $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                    })
                                    ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                                    ->where('referrals.schoolYearID', '=', $schoolyear)
                                    ->where('s_h_s_students.gradeLevel', '=', $courseGrade)
                                    ->count();
                
                }elseif ($schoolyear != 'all' && $semester != 'all' && $courseGrade == 'all') {
                    $count = referral::join('students', 'students.id', '=', 'referrals.studentID')
                            ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                            ->join('s_h_s_students', 's_h_s_students.studentID', '=', 'students.id')
                            ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                            'students.firstName', 'students.lastName', 'referrals.id',
                            'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                            ->where(function($query) use($search){
                                $query->where('students.firstName', 'LIKE', "%$search%");
                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                            })
                            ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                            ->where('referrals.schoolYearID', '=', $schoolyear)
                            ->where('referrals.semesterID', '=', $semester)
                            ->count();                    
                }elseif ($schoolyear != 'all' && $semester != 'all' && $courseGrade != 'all') {
                    $count = referral::join('students', 'students.id', '=', 'referrals.studentID')
                                    ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                                    ->join('s_h_s_students', 's_h_s_students.studentID', '=', 'students.id')
                                    ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                                    'students.firstName', 'students.lastName', 'referrals.id',
                                    'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                                    ->where(function($query) use($search){
                                        $query->where('students.firstName', 'LIKE', "%$search%");
                                        $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                        $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                        $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                    })
                                    ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                                    ->where('referrals.schoolYearID', '=', $schoolyear)
                                    ->where('referrals.semesterID', '=', $semester)
                                    ->where('s_h_s_students.gradeLevel', '=', $courseGrade)
                                    ->count();    
                    
                }else{
                    $count = referral::join('students', 'students.id', '=', 'referrals.studentID')
                                    ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                                    ->join('s_h_s_students', 's_h_s_students.studentID', '=', 'students.id')
                                    ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                                                        'students.firstName', 'students.lastName', 'referrals.id',
                                                        'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                                    ->where(function($query) use($search){
                                        $query->where('students.firstName', 'LIKE', "%$search%");
                                        $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                        $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                        $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                    })
                                    ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                                    ->count();
                }

            break;
            case 'juniorhigh':
                if($schoolyear == 'all' && $semester == 'all' && $courseGrade == 'all'){
                    $count = referral::join('students', 'students.id', '=', 'referrals.studentID')
                            ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                            ->join('j_h_students', 'j_h_students.studentID', '=', 'students.id')
                            ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                            'students.firstName', 'students.lastName', 'referrals.id',
                            'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                            ->where(function($query) use($search){
                                $query->where('students.firstName', 'LIKE', "%$search%");
                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                            })
                            ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                            ->count();
                }elseif ($schoolyear == 'all' && $semester == 'all' && $courseGrade != 'all') {
                    $count = referral::join('students', 'students.id', '=', 'referrals.studentID')
                                ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                                ->join('j_h_students', 'j_h_students.studentID', '=', 'students.id')
                                ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                                'students.firstName', 'students.lastName', 'referrals.id',
                                'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                                ->where(function($query) use($search){
                                    $query->where('students.firstName', 'LIKE', "%$search%");
                                    $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                    $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                    $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                })
                                ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                                ->where('j_h_students.gradeLevel', '=', $courseGrade)
                                ->count();
                        
                }elseif ($schoolyear == 'all' && $semester != 'all' && $courseGrade == 'all') {
                    $count = referral::join('students', 'students.id', '=', 'referrals.studentID')
                            ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                            ->join('j_h_students', 'j_h_students.studentID', '=', 'students.id')
                            ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                            'students.firstName', 'students.lastName', 'referrals.id',
                            'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                            ->where(function($query) use($search){
                                $query->where('students.firstName', 'LIKE', "%$search%");
                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                            })
                            ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                            ->where('referrals.semesterID', '=', $semester)
                            ->count();
                }elseif ($schoolyear == 'all' && $semester != 'all' && $courseGrade != 'all') {
                    $count = referral::join('students', 'students.id', '=', 'referrals.studentID')
                                    ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                                    ->join('j_h_students', 'j_h_students.studentID', '=', 'students.id')
                                    ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                                    'students.firstName', 'students.lastName', 'referrals.id',
                                    'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                                    ->where(function($query) use($search){
                                        $query->where('students.firstName', 'LIKE', "%$search%");
                                        $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                        $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                        $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                    })
                                    ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                                    ->where('referrals.semesterID', '=', $semester)
                                    ->where('j_h_students.gradeLevel', '=', $courseGrade)
                                    ->count();
                
                }elseif ($schoolyear != 'all' && $semester == 'all' && $courseGrade == 'all') {
                    $count = referral::join('students', 'students.id', '=', 'referrals.studentID')
                            ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                            ->join('j_h_students', 'j_h_students.studentID', '=', 'students.id')
                            ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                            'students.firstName', 'students.lastName', 'referrals.id',
                            'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                            ->where(function($query) use($search){
                                $query->where('students.firstName', 'LIKE', "%$search%");
                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                            })
                            ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                            ->where('referrals.schoolYearID', '=', $schoolyear)
                            ->count();
                }elseif ($schoolyear != 'all' && $semester == 'all' && $courseGrade != 'all') {
                    $count = referral::join('students', 'students.id', '=', 'referrals.studentID')
                                    ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                                    ->join('j_h_students', 'j_h_students.studentID', '=', 'students.id')
                                    ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                                    'students.firstName', 'students.lastName', 'referrals.id',
                                    'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                                    ->where(function($query) use($search){
                                        $query->where('students.firstName', 'LIKE', "%$search%");
                                        $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                        $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                        $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                    })
                                    ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                                    ->where('referrals.schoolYearID', '=', $schoolyear)
                                    ->where('j_h_students.gradeLevel', '=', $courseGrade)
                                    ->count();
                
                }elseif ($schoolyear != 'all' && $semester != 'all' && $courseGrade == 'all') {
                    $count = referral::join('students', 'students.id', '=', 'referrals.studentID')
                            ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                            ->join('j_h_students', 'j_h_students.studentID', '=', 'students.id')
                            ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                            'students.firstName', 'students.lastName', 'referrals.id',
                            'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                            ->where(function($query) use($search){
                                $query->where('students.firstName', 'LIKE', "%$search%");
                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                            })
                            ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                            ->where('referrals.schoolYearID', '=', $schoolyear)
                            ->where('referrals.semesterID', '=', $semester)
                            ->count();                    
                }elseif ($schoolyear != 'all' && $semester != 'all' && $courseGrade != 'all') {
                    $count = referral::join('students', 'students.id', '=', 'referrals.studentID')
                                    ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                                    ->join('j_h_students', 'j_h_students.studentID', '=', 'students.id')
                                    ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                                    'students.firstName', 'students.lastName', 'referrals.id',
                                    'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                                    ->where(function($query) use($search){
                                        $query->where('students.firstName', 'LIKE', "%$search%");
                                        $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                        $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                        $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                    })
                                    ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                                    ->where('referrals.schoolYearID', '=', $schoolyear)
                                    ->where('referrals.semesterID', '=', $semester)
                                    ->where('j_h_students.gradeLevel', '=', $courseGrade)
                                    ->count();    
                    
                }else{
                    $count = referral::join('students', 'students.id', '=', 'referrals.studentID')
                                    ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                                    ->join('j_h_students', 'j_h_students.studentID', '=', 'students.id')
                                    ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                                                        'students.firstName', 'students.lastName', 'referrals.id',
                                                        'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                                    ->where(function($query) use($search){
                                        $query->where('students.firstName', 'LIKE', "%$search%");
                                        $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                        $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                        $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                    })
                                    ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                                    ->count();
                }
            break;
            case 'elementary':
                if($schoolyear == 'all' && $semester == 'all' && $courseGrade == 'all'){
                    $count = referral::join('students', 'students.id', '=', 'referrals.studentID')
                            ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                            ->join('elem_students', 'elem_students.studentID', '=', 'students.id')
                            ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                            'students.firstName', 'students.lastName', 'referrals.id',
                            'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                            ->where(function($query) use($search){
                                $query->where('students.firstName', 'LIKE', "%$search%");
                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                            })
                            ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                            ->count();
                }elseif ($schoolyear == 'all' && $semester == 'all' && $courseGrade != 'all') {
                    $count = referral::join('students', 'students.id', '=', 'referrals.studentID')
                                ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                                ->join('elem_students', 'elem_students.studentID', '=', 'students.id')
                                ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                                'students.firstName', 'students.lastName', 'referrals.id',
                                'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                                ->where(function($query) use($search){
                                    $query->where('students.firstName', 'LIKE', "%$search%");
                                    $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                    $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                    $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                })
                                ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                                ->where('elem_students.gradeLevel', '=', $courseGrade)
                                ->count();
                        
                }elseif ($schoolyear == 'all' && $semester != 'all' && $courseGrade == 'all') {
                    $count = referral::join('students', 'students.id', '=', 'referrals.studentID')
                            ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                            ->join('elem_students', 'elem_students.studentID', '=', 'students.id')
                            ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                            'students.firstName', 'students.lastName', 'referrals.id',
                            'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                            ->where(function($query) use($search){
                                $query->where('students.firstName', 'LIKE', "%$search%");
                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                            })
                            ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                            ->where('referrals.semesterID', '=', $semester)
                            ->count();
                }elseif ($schoolyear == 'all' && $semester != 'all' && $courseGrade != 'all') {
                    $count = referral::join('students', 'students.id', '=', 'referrals.studentID')
                                    ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                                    ->join('elem_students', 'elem_students.studentID', '=', 'students.id')
                                    ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                                    'students.firstName', 'students.lastName', 'referrals.id',
                                    'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                                    ->where(function($query) use($search){
                                        $query->where('students.firstName', 'LIKE', "%$search%");
                                        $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                        $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                        $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                    })
                                    ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                                    ->where('referrals.semesterID', '=', $semester)
                                    ->where('elem_students.gradeLevel', '=', $courseGrade)
                                    ->count();
                
                }elseif ($schoolyear != 'all' && $semester == 'all' && $courseGrade == 'all') {
                    $count = referral::join('students', 'students.id', '=', 'referrals.studentID')
                            ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                            ->join('elem_students', 'elem_students.studentID', '=', 'students.id')
                            ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                            'students.firstName', 'students.lastName', 'referrals.id',
                            'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                            ->where(function($query) use($search){
                                $query->where('students.firstName', 'LIKE', "%$search%");
                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                            })
                            ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                            ->where('referrals.schoolYearID', '=', $schoolyear)
                            ->count();
                }elseif ($schoolyear != 'all' && $semester == 'all' && $courseGrade != 'all') {
                    $count = referral::join('students', 'students.id', '=', 'referrals.studentID')
                                    ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                                    ->join('elem_students', 'elem_students.studentID', '=', 'students.id')
                                    ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                                    'students.firstName', 'students.lastName', 'referrals.id',
                                    'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                                    ->where(function($query) use($search){
                                        $query->where('students.firstName', 'LIKE', "%$search%");
                                        $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                        $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                        $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                    })
                                    ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                                    ->where('referrals.schoolYearID', '=', $schoolyear)
                                    ->where('elem_students.gradeLevel', '=', $courseGrade)
                                    ->count();
                
                }elseif ($schoolyear != 'all' && $semester != 'all' && $courseGrade == 'all') {
                    $count = referral::join('students', 'students.id', '=', 'referrals.studentID')
                            ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                            ->join('elem_students', 'elem_students.studentID', '=', 'students.id')
                            ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                            'students.firstName', 'students.lastName', 'referrals.id',
                            'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                            ->where(function($query) use($search){
                                $query->where('students.firstName', 'LIKE', "%$search%");
                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                            })
                            ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                            ->where('referrals.schoolYearID', '=', $schoolyear)
                            ->where('referrals.semesterID', '=', $semester)
                            ->count();                    
                }elseif ($schoolyear != 'all' && $semester != 'all' && $courseGrade != 'all') {
                    $count = referral::join('students', 'students.id', '=', 'referrals.studentID')
                                    ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                                    ->join('elem_students', 'elem_students.studentID', '=', 'students.id')
                                    ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                                    'students.firstName', 'students.lastName', 'referrals.id',
                                    'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                                    ->where(function($query) use($search){
                                        $query->where('students.firstName', 'LIKE', "%$search%");
                                        $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                        $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                        $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                    })
                                    ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                                    ->where('referrals.schoolYearID', '=', $schoolyear)
                                    ->where('referrals.semesterID', '=', $semester)
                                    ->where('elem_students.gradeLevel', '=', $courseGrade)
                                    ->count();    
                    
                }else{
                    $count = referral::join('students', 'students.id', '=', 'referrals.studentID')
                                    ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                                    ->join('elem_students', 'elem_students.studentID', '=', 'students.id')
                                    ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                                                        'students.firstName', 'students.lastName', 'referrals.id',
                                                        'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                                    ->where(function($query) use($search){
                                        $query->where('students.firstName', 'LIKE', "%$search%");
                                        $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                        $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                        $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                    })
                                    ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                                    ->count();
                }
            break;
        }
        return $count;
    }
    public function getReferral($page, $viewby, $schoolyear, $semester, $courseGrade, $search = ''){
        $systemdate = systemDate::all();
        $skip = ($page-1)*10;
        $referral; 
        switch($viewby){
            case 'all':
                
                if($schoolyear == 'all' && $semester == 'all' && $courseGrade == 'all'){
                    $referral = referral::join('students', 'students.id', '=', 'referrals.studentID')
                                        ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                                        ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                                        'students.firstName', 'students.lastName', 'referrals.id',
                                        'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                                        ->where(function($query) use($search){
                                            $query->where('students.firstName', 'LIKE', "%$search%");
                                            $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                            $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                            $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                        })
                                        ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                                        ->limit(10)->offset($skip)
                                        ->get();
                }elseif ($schoolyear == 'all' && $semester == 'all' && $courseGrade != 'all') {
                   $temp = referral::join('students', 'students.id', '=', 'referrals.studentID')
                            ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                            ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                            'students.firstName', 'students.lastName', 'referrals.id',
                            'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                            ->where(function($query) use($search){
                                $query->where('students.firstName', 'LIKE', "%$search%");
                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                            })
                            ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                            ->limit(10)->offset($skip)
                            ->get();
                    $this->referralCollection($courseGrade, $temp);
                    $referral = $temp;
                }elseif ($schoolyear == 'all' && $semester != 'all' && $courseGrade == 'all') {
                    $referral = referral::join('students', 'students.id', '=', 'referrals.studentID')
                            ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                            ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                            'students.firstName', 'students.lastName', 'referrals.id',
                            'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                            ->where(function($query) use($search){
                                $query->where('students.firstName', 'LIKE', "%$search%");
                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                            })
                            ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                            ->where('referrals.semesterID', '=', $semester)
                            ->limit(10)->offset($skip)
                            ->get();
                }elseif ($schoolyear == 'all' && $semester != 'all' && $courseGrade != 'all') {
                    $temp = referral::join('students', 'students.id', '=', 'referrals.studentID')
                                    ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                                    ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                                    'students.firstName', 'students.lastName', 'referrals.id',
                                    'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                                    ->where(function($query) use($search){
                                        $query->where('students.firstName', 'LIKE', "%$search%");
                                        $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                        $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                        $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                    })
                                    ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                                    ->where('referrals.semesterID', '=', $semester)
                                    ->limit(10)->offset($skip)
                                    ->get();
                    $this->referralCollection($courseGrade, $temp);
                    $referral = $temp;
                }elseif ($schoolyear != 'all' && $semester == 'all' && $courseGrade == 'all') {
                    $referral = referral::join('students', 'students.id', '=', 'referrals.studentID')
                            ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                            ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                            'students.firstName', 'students.lastName', 'referrals.id',
                            'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                            ->where(function($query) use($search){
                                $query->where('students.firstName', 'LIKE', "%$search%");
                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                            })
                            ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                            ->where('referrals.schoolYearID', '=', $schoolyear)
                            ->limit(10)->offset($skip)
                            ->get();
                }elseif ($schoolyear != 'all' && $semester == 'all' && $courseGrade != 'all') {
                    $temp = referral::join('students', 'students.id', '=', 'referrals.studentID')
                                    ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                                    ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                                    'students.firstName', 'students.lastName', 'referrals.id',
                                    'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                                    ->where(function($query) use($search){
                                        $query->where('students.firstName', 'LIKE', "%$search%");
                                        $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                        $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                        $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                    })
                                    ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                                    ->where('referrals.schoolYearID', '=', $schoolyear)
                                    ->limit(10)->offset($skip)
                                    ->get();
                    $this->referralCollection($courseGrade, $temp);
                    $referral = $temp;
                }elseif ($schoolyear != 'all' && $semester != 'all' && $courseGrade == 'all') {
                    $referral = referral::join('students', 'students.id', '=', 'referrals.studentID')
                            ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                            ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                            'students.firstName', 'students.lastName', 'referrals.id',
                            'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                            ->where(function($query) use($search){
                                $query->where('students.firstName', 'LIKE', "%$search%");
                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                            })
                            ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                            ->where('referrals.schoolYearID', '=', $schoolyear)
                            ->where('referrals.semesterID', '=', $semester)
                            ->limit(10)->offset($skip)
                            ->get();                  
                }elseif ($schoolyear != 'all' && $semester != 'all' && $courseGrade != 'all') {
                    $temp = referral::join('students', 'students.id', '=', 'referrals.studentID')
                                    ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                                    ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                                    'students.firstName', 'students.lastName', 'referrals.id',
                                    'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                                    ->where(function($query) use($search){
                                        $query->where('students.firstName', 'LIKE', "%$search%");
                                        $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                        $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                        $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                    })
                                    ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                                    ->where('referrals.schoolYearID', '=', $schoolyear)
                                    ->where('referrals.semesterID', '=', $semester)
                                    ->limit(10)->offset($skip)
                                    ->get();    
                    $this->referralCollection($courseGrade, $temp);
                    $referral = $temp;
                }else{
                    $referral = referral::join('students', 'students.id', '=', 'referrals.studentID')
                                    ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                                    ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                                    'students.firstName', 'students.lastName', 'referrals.id',
                                    'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                                    ->where(function($query) use($search){
                                        $query->where('students.firstName', 'LIKE', "%$search%");
                                        $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                        $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                        $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                    })
                                    ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                                    ->limit(10)->offset($skip)
                                    ->get();  
                }       
            break;
            case 'college':
                if($schoolyear == 'all' && $semester == 'all' && $courseGrade == 'all'){
                    $referral = referral::join('students', 'students.id', '=', 'referrals.studentID')
                            ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                            ->join('college_students', 'college_students.studentID', '=', 'students.id')
                            ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                            'students.firstName', 'students.lastName', 'referrals.id',
                            'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                            ->where(function($query) use($search){
                                $query->where('students.firstName', 'LIKE', "%$search%");
                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                            })
                            ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                            ->limit(10)->offset($skip)
                            ->get();
                }elseif ($schoolyear == 'all' && $semester == 'all' && $courseGrade != 'all') {
                    $referral = referral::join('students', 'students.id', '=', 'referrals.studentID')
                                ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                                ->join('college_students', 'college_students.studentID', '=', 'students.id')
                                ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                                'students.firstName', 'students.lastName', 'referrals.id',
                                'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                                ->where(function($query) use($search){
                                    $query->where('students.firstName', 'LIKE', "%$search%");
                                    $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                    $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                    $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                })
                                ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                                ->where('college_students.courseID', '=', $courseGrade)
                                ->limit(10)->offset($skip)
                                ->get();
                        
                }elseif ($schoolyear == 'all' && $semester != 'all' && $courseGrade == 'all') {
                    $referral = referral::join('students', 'students.id', '=', 'referrals.studentID')
                            ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                            ->join('college_students', 'college_students.studentID', '=', 'students.id')
                            ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                            'students.firstName', 'students.lastName', 'referrals.id',
                            'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                            ->where(function($query) use($search){
                                $query->where('students.firstName', 'LIKE', "%$search%");
                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                            })
                            ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                            ->where('referrals.semesterID', '=', $semester)
                            ->limit(10)->offset($skip)
                            ->get();
                }elseif ($schoolyear == 'all' && $semester != 'all' && $courseGrade != 'all') {
                    $referral = referral::join('students', 'students.id', '=', 'referrals.studentID')
                                    ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                                    ->join('college_students', 'college_students.studentID', '=', 'students.id')
                                    ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                                    'students.firstName', 'students.lastName', 'referrals.id',
                                    'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                                    ->where(function($query) use($search){
                                        $query->where('students.firstName', 'LIKE', "%$search%");
                                        $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                        $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                        $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                    })
                                    ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                                    ->where('referrals.semesterID', '=', $semester)
                                    ->where('college_students.courseID', '=', $courseGrade)
                                    ->limit(10)->offset($skip)
                                    ->get();
                   
                }elseif ($schoolyear != 'all' && $semester == 'all' && $courseGrade == 'all') {
                    $referral = referral::join('students', 'students.id', '=', 'referrals.studentID')
                            ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                            ->join('college_students', 'college_students.studentID', '=', 'students.id')
                            ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                            'students.firstName', 'students.lastName', 'referrals.id',
                            'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                            ->where(function($query) use($search){
                                $query->where('students.firstName', 'LIKE', "%$search%");
                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                            })
                            ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                            ->where('referrals.schoolYearID', '=', $schoolyear)
                            ->limit(10)->offset($skip)
                            ->get();
                }elseif ($schoolyear != 'all' && $semester == 'all' && $courseGrade != 'all') {
                    $referral = referral::join('students', 'students.id', '=', 'referrals.studentID')
                                    ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                                    ->join('college_students', 'college_students.studentID', '=', 'students.id')
                                    ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                                    'students.firstName', 'students.lastName', 'referrals.id',
                                    'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                                    ->where(function($query) use($search){
                                        $query->where('students.firstName', 'LIKE', "%$search%");
                                        $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                        $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                        $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                    })
                                    ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                                    ->where('referrals.schoolYearID', '=', $schoolyear)
                                    ->where('college_students.courseID', '=', $courseGrade)
                                    ->limit(10)->offset($skip)
                                    ->get();
                   
                }elseif ($schoolyear != 'all' && $semester != 'all' && $courseGrade == 'all') {
                    $referral = referral::join('students', 'students.id', '=', 'referrals.studentID')
                            ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                            ->join('college_students', 'college_students.studentID', '=', 'students.id')
                            ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                            'students.firstName', 'students.lastName', 'referrals.id',
                            'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                            ->where(function($query) use($search){
                                $query->where('students.firstName', 'LIKE', "%$search%");
                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                            })
                            ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                            ->where('referrals.schoolYearID', '=', $schoolyear)
                            ->where('referrals.semesterID', '=', $semester)
                            ->limit(10)->offset($skip)
                            ->get();                 
                }elseif ($schoolyear != 'all' && $semester != 'all' && $courseGrade != 'all') {
                    $referral = referral::join('students', 'students.id', '=', 'referrals.studentID')
                                    ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                                    ->join('college_students', 'college_students.studentID', '=', 'students.id')
                                    ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                                    'students.firstName', 'students.lastName', 'referrals.id',
                                    'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                                    ->where(function($query) use($search){
                                        $query->where('students.firstName', 'LIKE', "%$search%");
                                        $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                        $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                        $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                    })
                                    ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                                    ->where('referrals.schoolYearID', '=', $schoolyear)
                                    ->where('referrals.semesterID', '=', $semester)
                                    ->where('college_students.courseID', '=', $courseGrade)
                                    ->limit(10)->offset($skip)
                                    ->get();    
                    
                }else{
                    $referral = referral::join('students', 'students.id', '=', 'referrals.studentID')
                                    ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                                    ->join('college_students', 'college_students.studentID', '=', 'students.id')
                                    ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                                    'students.firstName', 'students.lastName', 'referrals.id',
                                    'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                                    ->where(function($query) use($search){
                                        $query->where('students.firstName', 'LIKE', "%$search%");
                                        $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                        $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                        $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                    })
                                    ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                                    ->limit(10)->offset($skip)
                                    ->get();
                } 
            break;
            case 'seniorhigh':
                if($schoolyear == 'all' && $semester == 'all' && $courseGrade == 'all'){
                    $referral = referral::join('students', 'students.id', '=', 'referrals.studentID')
                            ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                            ->join('s_h_s_students', 's_h_s_students.studentID', '=', 'students.id')
                            ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                            'students.firstName', 'students.lastName', 'referrals.id',
                            'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                            ->where(function($query) use($search){
                                $query->where('students.firstName', 'LIKE', "%$search%");
                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                            })
                            ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                            ->limit(10)->offset($skip)
                            ->get();
                }elseif ($schoolyear == 'all' && $semester == 'all' && $courseGrade != 'all') {
                    $referral = referral::join('students', 'students.id', '=', 'referrals.studentID')
                                ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                                ->join('s_h_s_students', 's_h_s_students.studentID', '=', 'students.id')
                                ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                                'students.firstName', 'students.lastName', 'referrals.id',
                                'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                                ->where(function($query) use($search){
                                    $query->where('students.firstName', 'LIKE', "%$search%");
                                    $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                    $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                    $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                })
                                ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                                ->where('s_h_s_students.gradeLevel', '=', $courseGrade)
                                ->limit(10)->offset($skip)
                                ->get();
                        
                }elseif ($schoolyear == 'all' && $semester != 'all' && $courseGrade == 'all') {
                    $referral = referral::join('students', 'students.id', '=', 'referrals.studentID')
                            ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                            ->join('s_h_s_students', 's_h_s_students.studentID', '=', 'students.id')
                            ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                            'students.firstName', 'students.lastName', 'referrals.id',
                            'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                            ->where(function($query) use($search){
                                $query->where('students.firstName', 'LIKE', "%$search%");
                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                            })
                            ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                            ->where('referrals.semesterID', '=', $semester)
                            ->limit(10)->offset($skip)
                            ->get();
                }elseif ($schoolyear == 'all' && $semester != 'all' && $courseGrade != 'all') {
                    $referral = referral::join('students', 'students.id', '=', 'referrals.studentID')
                                    ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                                    ->join('s_h_s_students', 's_h_s_students.studentID', '=', 'students.id')
                                    ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                                    'students.firstName', 'students.lastName', 'referrals.id',
                                    'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                                    ->where(function($query) use($search){
                                        $query->where('students.firstName', 'LIKE', "%$search%");
                                        $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                        $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                        $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                    })
                                    ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                                    ->where('referrals.semesterID', '=', $semester)
                                    ->where('s_h_s_students.gradeLevel', '=', $courseGrade)
                                    ->limit(10)->offset($skip)
                                    ->get();
                
                }elseif ($schoolyear != 'all' && $semester == 'all' && $courseGrade == 'all') {
                    $referral = referral::join('students', 'students.id', '=', 'referrals.studentID')
                            ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                            ->join('s_h_s_students', 's_h_s_students.studentID', '=', 'students.id')
                            ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                            'students.firstName', 'students.lastName', 'referrals.id',
                            'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                            ->where(function($query) use($search){
                                $query->where('students.firstName', 'LIKE', "%$search%");
                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                            })
                            ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                            ->where('referrals.schoolYearID', '=', $schoolyear)
                            ->limit(10)->offset($skip)
                            ->get();
                }elseif ($schoolyear != 'all' && $semester == 'all' && $courseGrade != 'all') {
                    $referral = referral::join('students', 'students.id', '=', 'referrals.studentID')
                                    ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                                    ->join('s_h_s_students', 's_h_s_students.studentID', '=', 'students.id')
                                    ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                                    'students.firstName', 'students.lastName', 'referrals.id',
                                    'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                                    ->where(function($query) use($search){
                                        $query->where('students.firstName', 'LIKE', "%$search%");
                                        $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                        $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                        $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                    })
                                    ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                                    ->where('referrals.schoolYearID', '=', $schoolyear)
                                    ->where('s_h_s_students.gradeLevel', '=', $courseGrade)
                                    ->limit(10)->offset($skip)
                                    ->get();
                
                }elseif ($schoolyear != 'all' && $semester != 'all' && $courseGrade == 'all') {
                    $referral = referral::join('students', 'students.id', '=', 'referrals.studentID')
                            ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                            ->join('s_h_s_students', 's_h_s_students.studentID', '=', 'students.id')
                            ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                            'students.firstName', 'students.lastName', 'referrals.id',
                            'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                            ->where(function($query) use($search){
                                $query->where('students.firstName', 'LIKE', "%$search%");
                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                            })
                            ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                            ->where('referrals.schoolYearID', '=', $schoolyear)
                            ->where('referrals.semesterID', '=', $semester)
                            ->limit(10)->offset($skip)
                            ->get();                
                }elseif ($schoolyear != 'all' && $semester != 'all' && $courseGrade != 'all') {
                    $referral = referral::join('students', 'students.id', '=', 'referrals.studentID')
                                    ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                                    ->join('s_h_s_students', 's_h_s_students.studentID', '=', 'students.id')
                                    ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                                    'students.firstName', 'students.lastName', 'referrals.id',
                                    'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                                    ->where(function($query) use($search){
                                        $query->where('students.firstName', 'LIKE', "%$search%");
                                        $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                        $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                        $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                    })
                                    ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                                    ->where('referrals.schoolYearID', '=', $schoolyear)
                                    ->where('referrals.semesterID', '=', $semester)
                                    ->where('s_h_s_students.gradeLevel', '=', $courseGrade)
                                    ->limit(10)->offset($skip)
                                    ->get();
                    
                }else{
                    $referral = referral::join('students', 'students.id', '=', 'referrals.studentID')
                                    ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                                    ->join('s_h_s_students', 's_h_s_students.studentID', '=', 'students.id')
                                    ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                                                        'students.firstName', 'students.lastName', 'referrals.id',
                                                        'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                                    ->where(function($query) use($search){
                                        $query->where('students.firstName', 'LIKE', "%$search%");
                                        $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                        $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                        $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                    })
                                    ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                                    ->limit(10)->offset($skip)
                                    ->get();
                }

            break;
            case 'juniorhigh':
                if($schoolyear == 'all' && $semester == 'all' && $courseGrade == 'all'){
                    $referral = referral::join('students', 'students.id', '=', 'referrals.studentID')
                            ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                            ->join('j_h_students', 'j_h_students.studentID', '=', 'students.id')
                            ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                            'students.firstName', 'students.lastName', 'referrals.id',
                            'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                            ->where(function($query) use($search){
                                $query->where('students.firstName', 'LIKE', "%$search%");
                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                            })
                            ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                            ->limit(10)->offset($skip)
                            ->get();
                }elseif ($schoolyear == 'all' && $semester == 'all' && $courseGrade != 'all') {
                    $referral = referral::join('students', 'students.id', '=', 'referrals.studentID')
                                ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                                ->join('j_h_students', 'j_h_students.studentID', '=', 'students.id')
                                ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                                'students.firstName', 'students.lastName', 'referrals.id',
                                'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                                ->where(function($query) use($search){
                                    $query->where('students.firstName', 'LIKE', "%$search%");
                                    $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                    $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                    $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                })
                                ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                                ->where('j_h_students.gradeLevel', '=', $courseGrade)
                                ->limit(10)->offset($skip)
                                ->get();
                        
                }elseif ($schoolyear == 'all' && $semester != 'all' && $courseGrade == 'all') {
                    $referral = referral::join('students', 'students.id', '=', 'referrals.studentID')
                            ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                            ->join('j_h_students', 'j_h_students.studentID', '=', 'students.id')
                            ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                            'students.firstName', 'students.lastName', 'referrals.id',
                            'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                            ->where(function($query) use($search){
                                $query->where('students.firstName', 'LIKE', "%$search%");
                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                            })
                            ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                            ->where('referrals.semesterID', '=', $semester)
                            ->limit(10)->offset($skip)
                            ->get();
                }elseif ($schoolyear == 'all' && $semester != 'all' && $courseGrade != 'all') {
                    $referral = referral::join('students', 'students.id', '=', 'referrals.studentID')
                                    ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                                    ->join('j_h_students', 'j_h_students.studentID', '=', 'students.id')
                                    ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                                    'students.firstName', 'students.lastName', 'referrals.id',
                                    'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                                    ->where(function($query) use($search){
                                        $query->where('students.firstName', 'LIKE', "%$search%");
                                        $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                        $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                        $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                    })
                                    ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                                    ->where('referrals.semesterID', '=', $semester)
                                    ->where('j_h_students.gradeLevel', '=', $courseGrade)
                                    ->limit(10)->offset($skip)
                                    ->get();
                
                }elseif ($schoolyear != 'all' && $semester == 'all' && $courseGrade == 'all') {
                    $referral = referral::join('students', 'students.id', '=', 'referrals.studentID')
                            ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                            ->join('j_h_students', 'j_h_students.studentID', '=', 'students.id')
                            ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                            'students.firstName', 'students.lastName', 'referrals.id',
                            'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                            ->where(function($query) use($search){
                                $query->where('students.firstName', 'LIKE', "%$search%");
                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                            })
                            ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                            ->where('referrals.schoolYearID', '=', $schoolyear)
                            ->limit(10)->offset($skip)
                            ->get();
                }elseif ($schoolyear != 'all' && $semester == 'all' && $courseGrade != 'all') {
                    $referral = referral::join('students', 'students.id', '=', 'referrals.studentID')
                                    ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                                    ->join('j_h_students', 'j_h_students.studentID', '=', 'students.id')
                                    ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                                    'students.firstName', 'students.lastName', 'referrals.id',
                                    'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                                    ->where(function($query) use($search){
                                        $query->where('students.firstName', 'LIKE', "%$search%");
                                        $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                        $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                        $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                    })
                                    ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                                    ->where('referrals.schoolYearID', '=', $schoolyear)
                                    ->where('j_h_students.gradeLevel', '=', $courseGrade)
                                    ->limit(10)->offset($skip)
                                    ->get();
                
                }elseif ($schoolyear != 'all' && $semester != 'all' && $courseGrade == 'all') {
                    $referral = referral::join('students', 'students.id', '=', 'referrals.studentID')
                            ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                            ->join('j_h_students', 'j_h_students.studentID', '=', 'students.id')
                            ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                            'students.firstName', 'students.lastName', 'referrals.id',
                            'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                            ->where(function($query) use($search){
                                $query->where('students.firstName', 'LIKE', "%$search%");
                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                            })
                            ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                            ->where('referrals.schoolYearID', '=', $schoolyear)
                            ->where('referrals.semesterID', '=', $semester)
                            ->limit(10)->offset($skip)
                            ->get();                
                }elseif ($schoolyear != 'all' && $semester != 'all' && $courseGrade != 'all') {
                    $referral = referral::join('students', 'students.id', '=', 'referrals.studentID')
                                    ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                                    ->join('j_h_students', 'j_h_students.studentID', '=', 'students.id')
                                    ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                                    'students.firstName', 'students.lastName', 'referrals.id',
                                    'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                                    ->where(function($query) use($search){
                                        $query->where('students.firstName', 'LIKE', "%$search%");
                                        $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                        $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                        $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                    })
                                    ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                                    ->where('referrals.schoolYearID', '=', $schoolyear)
                                    ->where('referrals.semesterID', '=', $semester)
                                    ->where('j_h_students.gradeLevel', '=', $courseGrade)
                                    ->limit(10)->offset($skip)
                                    ->get();
                    
                }else{
                    $referral = referral::join('students', 'students.id', '=', 'referrals.studentID')
                                    ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                                    ->join('j_h_students', 'j_h_students.studentID', '=', 'students.id')
                                    ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                                                        'students.firstName', 'students.lastName', 'referrals.id',
                                                        'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                                    ->where(function($query) use($search){
                                        $query->where('students.firstName', 'LIKE', "%$search%");
                                        $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                        $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                        $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                    })
                                    ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                                    ->limit(10)->offset($skip)
                                    ->get();
                }
            break;
            case 'elementary':
                if($schoolyear == 'all' && $semester == 'all' && $courseGrade == 'all'){
                    $referral = referral::join('students', 'students.id', '=', 'referrals.studentID')
                            ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                            ->join('elem_students', 'elem_students.studentID', '=', 'students.id')
                            ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                            'students.firstName', 'students.lastName', 'referrals.id',
                            'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                            ->where(function($query) use($search){
                                $query->where('students.firstName', 'LIKE', "%$search%");
                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                            })
                            ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                            ->limit(10)->offset($skip)
                            ->get();
                }elseif ($schoolyear == 'all' && $semester == 'all' && $courseGrade != 'all') {
                    $referral = referral::join('students', 'students.id', '=', 'referrals.studentID')
                                ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                                ->join('elem_students', 'elem_students.studentID', '=', 'students.id')
                                ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                                'students.firstName', 'students.lastName', 'referrals.id',
                                'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                                ->where(function($query) use($search){
                                    $query->where('students.firstName', 'LIKE', "%$search%");
                                    $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                    $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                    $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                })
                                ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                                ->where('elem_students.gradeLevel', '=', $courseGrade)
                                ->limit(10)->offset($skip)
                                ->get();
                        
                }elseif ($schoolyear == 'all' && $semester != 'all' && $courseGrade == 'all') {
                    $referral = referral::join('students', 'students.id', '=', 'referrals.studentID')
                            ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                            ->join('elem_students', 'elem_students.studentID', '=', 'students.id')
                            ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                            'students.firstName', 'students.lastName', 'referrals.id',
                            'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                            ->where(function($query) use($search){
                                $query->where('students.firstName', 'LIKE', "%$search%");
                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                            })
                            ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                            ->where('referrals.semesterID', '=', $semester)
                            ->limit(10)->offset($skip)
                            ->get();
                }elseif ($schoolyear == 'all' && $semester != 'all' && $courseGrade != 'all') {
                    $referral = referral::join('students', 'students.id', '=', 'referrals.studentID')
                                    ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                                    ->join('elem_students', 'elem_students.studentID', '=', 'students.id')
                                    ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                                    'students.firstName', 'students.lastName', 'referrals.id',
                                    'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                                    ->where(function($query) use($search){
                                        $query->where('students.firstName', 'LIKE', "%$search%");
                                        $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                        $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                        $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                    })
                                    ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                                    ->where('referrals.semesterID', '=', $semester)
                                    ->where('elem_students.gradeLevel', '=', $courseGrade)
                                    ->limit(10)->offset($skip)
                                    ->get();
                
                }elseif ($schoolyear != 'all' && $semester == 'all' && $courseGrade == 'all') {
                    $referral = referral::join('students', 'students.id', '=', 'referrals.studentID')
                            ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                            ->join('elem_students', 'elem_students.studentID', '=', 'students.id')
                            ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                            'students.firstName', 'students.lastName', 'referrals.id',
                            'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                            ->where(function($query) use($search){
                                $query->where('students.firstName', 'LIKE', "%$search%");
                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                            })
                            ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                            ->where('referrals.schoolYearID', '=', $schoolyear)
                            ->limit(10)->offset($skip)
                            ->get();
                }elseif ($schoolyear != 'all' && $semester == 'all' && $courseGrade != 'all') {
                    $referral = referral::join('students', 'students.id', '=', 'referrals.studentID')
                                    ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                                    ->join('elem_students', 'elem_students.studentID', '=', 'students.id')
                                    ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                                    'students.firstName', 'students.lastName', 'referrals.id',
                                    'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                                    ->where(function($query) use($search){
                                        $query->where('students.firstName', 'LIKE', "%$search%");
                                        $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                        $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                        $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                    })
                                    ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                                    ->where('referrals.schoolYearID', '=', $schoolyear)
                                    ->where('elem_students.gradeLevel', '=', $courseGrade)
                                    ->limit(10)->offset($skip)
                                    ->get();
                
                }elseif ($schoolyear != 'all' && $semester != 'all' && $courseGrade == 'all') {
                    $referral = referral::join('students', 'students.id', '=', 'referrals.studentID')
                            ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                            ->join('elem_students', 'elem_students.studentID', '=', 'students.id')
                            ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                            'students.firstName', 'students.lastName', 'referrals.id',
                            'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                            ->where(function($query) use($search){
                                $query->where('students.firstName', 'LIKE', "%$search%");
                                $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                            })
                            ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                            ->where('referrals.schoolYearID', '=', $schoolyear)
                            ->where('referrals.semesterID', '=', $semester)
                            ->limit(10)->offset($skip)
                            ->get();                    
                }elseif ($schoolyear != 'all' && $semester != 'all' && $courseGrade != 'all') {
                    $referral = referral::join('students', 'students.id', '=', 'referrals.studentID')
                                    ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                                    ->join('elem_students', 'elem_students.studentID', '=', 'students.id')
                                    ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                                    'students.firstName', 'students.lastName', 'referrals.id',
                                    'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                                    ->where(function($query) use($search){
                                        $query->where('students.firstName', 'LIKE', "%$search%");
                                        $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                        $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                        $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                    })
                                    ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                                    ->where('referrals.schoolYearID', '=', $schoolyear)
                                    ->where('referrals.semesterID', '=', $semester)
                                    ->where('elem_students.gradeLevel', '=', $courseGrade)
                                    ->limit(10)->offset($skip)
                                    ->get();    
                    
                }else{
                    $referral = referral::join('students', 'students.id', '=', 'referrals.studentID')
                                    ->join('school_years', 'school_years.id', '=', 'referrals.schoolYearID')
                                    ->join('elem_students', 'elem_students.studentID', '=', 'students.id')
                                    ->select('school_years.schoolYearStart', 'school_years.schoolYearEnd',
                                                        'students.firstName', 'students.lastName', 'referrals.id',
                                                        'referrals.studentID', 'referrals.referralDate', 'referrals.semesterID')
                                    ->where(function($query) use($search){
                                        $query->where('students.firstName', 'LIKE', "%$search%");
                                        $query->orWhere('students.lastName', 'LIKE', "%$search%");
                                        $query->orWhere('school_years.schoolYearStart', 'LIKE', "$search%");
                                        $query->orWhere('school_years.schoolYearEnd', 'LIKE', "$search%");
                                    })
                                    ->whereDate('referrals.created_at', '<=', $systemdate[0]->systemDate)
                                    ->limit(10)->offset($skip)
                                    ->get();
                }
            break;
        }
       
        if(count($referral)==0){
            return "<h2>There are no referrals</h2>";
        }
        $html ='';
        $this->rowCounter = 0;
        foreach ($referral as $referrals) {
            $this->rowCounter++;
            $html .=$this->getReferralRow($referrals);
        }
        return $html;
    }
    public function getReferralFaster($page, $viewby, $schoolyear, $semester, $coursegrade, $totalRow, $search = ''){
        $user = new userController();
        $systemdate = systemDate::all();
        $sql='';
        $skip = ($page-1)*10;
        if(session()->get('user')['userType']=='admin'){
            $sql = "SELECT COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) 
                    semesterID, COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) 
                    schoolYearID, COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) studentTypes,
                    col.studentType, shs.studentType, jhs.studentType, elem.studentType,
                    COALESCE(col.yearLevel, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) courseGrade, 
                    col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel,
                    COALESCE(col.studentNo, shs.studNo, jhs.studNo, elem.studNo) studentNo,  students.firstName, students.lastName,
                    students.middleName,
                    col.courseName, col.yearLevel,
                    school_years.schoolYearStart,
                    school_years.schoolYearEnd,
                    shs.strandName,
                    referrals.*,
                    COALESCE(col.courseID, shs.seniorHighStrandID) strandCourse
                    FROM students LEFT JOIN (SELECT college_students.studentID, 
                    college_students.yearLevel, college_students.semesterID, college_students.schoolYearID,
                    college_students.id, 'college' AS studentType, 
                    college_students.courseID, courses.courseName, college_students.studentNo FROM
                    college_students INNER JOIN (SELECT max(id) as maxid FROM college_students GROUP 
                    BY studentID) cs ON cs.maxid = college_students.id
                    INNER JOIN courses ON courses.id = college_students.courseID) col ON 
                    col.studentID = students.id LEFT 
                    JOIN (SELECT s_h_s_students.studentID, s_h_s_students.semesterID, s_h_s_students.schoolYearID, s_h_s_students.id, 'seniorhigh' 
                    AS studentType, s_h_s_students.gradeLevel, s_h_s_students.studNo,  senior_high_strands.strandName, s_h_s_students.seniorHighStrandID
                    FROM s_h_s_students INNER JOIN (SELECT max(id) as maxid FROM s_h_s_students GROUP BY studentID) 
                    shs ON shs.maxid = s_h_s_students.id INNER JOIN senior_high_strands ON senior_high_strands.id = 
                    s_h_s_students.seniorHighStrandID) shs ON shs.studentID = students.id
                    LEFT JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'juniorhigh' AS 
                    studentType, gradeLevel, studNo  FROM j_h_students INNER JOIN (SELECT max(id) as maxid FROM j_h_students 
                    GROUP BY studentID) jhs ON jhs.maxid = j_h_students.id) jhs ON jhs.studentID = students.id LEFT 
                    JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'elementary' AS studentType, 
                    gradeLevel, studNo FROM elem_students INNER JOIN (SELECT max(id) as maxid FROM elem_students GROUP 
                    BY studentID) elem ON elem.maxid = elem_students.id) elem ON elem.studentID = students.id
                    INNER JOIN referrals ON referrals.studentID =  students.id
                    INNER JOIN school_years ON school_years.id = referrals.schoolYearID
                    LEFT JOIN 
                    (SELECT behaviours.referralID, behaviours.remarks, behaviour_settings.name FROM behaviours 						INNER JOIN behaviour_settings ON behaviour_settings.id = behaviours.behavioural
                     INNER JOIN referrals ON referrals.id  = behaviours.referralID) behave
                     ON behave.referralID = referrals.id
            WHERE (students.firstName LIKE '%$search%' OR students.lastName LIKE '%$search%'
                    OR referrals.referredTo LIKE '%$search%' OR referrals.referredBy LIKE '%$search%' OR
                    behave.remarks LIKE '%$search%'  OR behave.name LIKE '%$search%') 
            AND DATE(students.created_at) <= '".$systemdate[0]->systemDate."'";
            switch ($viewby) {
                case 'college':
                    $sql .= " AND col.studentType = 'college'";
                break;
                case 'seniorhigh':
                    $sql .= " AND shs.studentType = 'seniorhigh'";
                break;
                case 'juniorhigh':
                    $sql .= " AND jhs.studentType = 'juniorhigh'";
                break;
                case 'elementary':
                    $sql .= " AND elem.studentType = 'elementary'";
                break;
                default:
                   
                break;
            }
            if($schoolyear!="all"){
                $sql .= " AND referrals.schoolYearID = '$schoolyear'";
            }
            if($semester!="all"){
                $sql .= " AND referrals.semesterID = '$semester'";
            }
            if($coursegrade!="all"){
                switch ($viewby) {
                    case 'college':
                        $sql .= " AND col.courseID = '$coursegrade'";
                    break;
                    case 'seniorhigh':
                        $sql .= "  AND shs.gradeLevel = '$coursegrade'";
                    break;
                    case 'juniorhigh':
                        $sql .= "  AND jhs.gradeLevel = '$coursegrade'";
                    break;
                    case 'elementary':
                        $sql .= "  AND elem.gradeLevel = '$coursegrade'";
                    break;
                    default:
                        $sql .= " AND COALESCE(col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) = '$coursegrade'";
                    break;
                }
            }
        }else{
            $designation = $user->getUserDesignation(Auth::guard('myuser')->id());
            $sql = "SELECT COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) 
                    semesterID, COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) 
                    schoolYearID, COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) studentTypes,
                    col.studentType, shs.studentType, jhs.studentType, elem.studentType,
                    COALESCE(col.yearLevel, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) courseGrade, 
                    col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel,
                    COALESCE(col.studentNo, shs.studNo, jhs.studNo, elem.studNo) studentNo,  students.firstName, students.lastName,
                    students.middleName,
                    col.courseName, col.yearLevel,
                    school_years.schoolYearStart,
                    school_years.schoolYearEnd,
                    shs.strandName,
                    referrals.*,
                    COALESCE(col.courseID, shs.seniorHighStrandID) strandCourse
                    FROM students LEFT JOIN (SELECT college_students.studentID, 
                    college_students.yearLevel, college_students.semesterID, college_students.schoolYearID,
                    college_students.id, 'college' AS studentType, 
                    college_students.courseID, courses.courseName, college_students.studentNo FROM
                    college_students INNER JOIN (SELECT max(id) as maxid FROM college_students GROUP 
                    BY studentID) cs ON cs.maxid = college_students.id
                    INNER JOIN courses ON courses.id = college_students.courseID) col ON 
                    col.studentID = students.id LEFT 
                    JOIN (SELECT s_h_s_students.studentID, s_h_s_students.semesterID, s_h_s_students.schoolYearID, s_h_s_students.id, 'seniorhigh' 
                    AS studentType, s_h_s_students.gradeLevel, s_h_s_students.studNo,  senior_high_strands.strandName, s_h_s_students.seniorHighStrandID
                    FROM s_h_s_students INNER JOIN (SELECT max(id) as maxid FROM s_h_s_students GROUP BY studentID) 
                    shs ON shs.maxid = s_h_s_students.id INNER JOIN senior_high_strands ON senior_high_strands.id = 
                    s_h_s_students.seniorHighStrandID) shs ON shs.studentID = students.id
                    LEFT JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'juniorhigh' AS 
                    studentType, gradeLevel, studNo  FROM j_h_students INNER JOIN (SELECT max(id) as maxid FROM j_h_students 
                    GROUP BY studentID) jhs ON jhs.maxid = j_h_students.id) jhs ON jhs.studentID = students.id LEFT 
                    JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'elementary' AS studentType, 
                    gradeLevel, studNo FROM elem_students INNER JOIN (SELECT max(id) as maxid FROM elem_students GROUP 
                    BY studentID) elem ON elem.maxid = elem_students.id) elem ON elem.studentID = students.id
                    INNER JOIN referrals ON referrals.studentID =  students.id
                    INNER JOIN school_years ON school_years.id = referrals.schoolYearID
                    WHERE (students.firstName LIKE '%$search%' OR students.lastName LIKE '%$search%') 
                    AND DATE(students.created_at) <= '".$systemdate[0]->systemDate."'";
            switch($designation[0]){
                case 'college':
                    $sql= "SELECT cs2.courseID AS courseGrade, c2.courseName, cs2.schoolYearID, cs2.studentNo, 
                    cs2.yearLevel, students.firstName, students.lastName, 'college' AS studentTypes, cs2.semesterID,
                    referrals.*,
                    students.middleName,
                    school_years.schoolYearStart,
                    school_years.schoolYearEnd  FROM students 
                    INNER JOIN (SELECT studentID, studentNo,
                    id, courseID, schoolYearID, yearLevel, semesterID FROM college_students  INNER JOIN (SELECT max(id) as maxid 
                    FROM college_students 
                    GROUP BY studentID) cs ON cs.maxid = college_students.id) cs2 ON cs2.studentID = students.id 
                    INNER JOIN (SELECT c.id AS courseID, dep.departmentID, c.courseName FROM courses c INNER JOIN (SELECT
                    id, courseID FROM course_department_hists cd INNER JOIN (SELECT max(id) as maxid FROM 
                    course_department_hists GROUP BY courseID) cd2 ON cd2.maxid = cd.id) cdh ON cdh.courseID = c.id
                    INNER JOIN (SELECT departmentID, id FROM course_department_hists INNER JOIN (SELECT
                    max(id) AS maxid FROM course_department_hists GROUP BY courseID) cdh1 ON cdh1.maxid = 
                    course_department_hists.id) dep ON dep.id =cdh.id) c2 ON c2.courseID = cs2.courseID 
                    INNER JOIN (SELECT departmentID, id, collegeID FROM department_college_hists
                    INNER JOIN (SELECT max(id) as maxid FROM department_college_hists GROUP BY departmentID) 
                    depc ON depc.maxid = department_college_hists.id) depc2 ON depc2.departmentID=c2.departmentID 
                    INNER JOIN (SELECT collegeID, userID, id FROM assignment_colleges INNER JOIN (SELECT max(id) 
                    as maxid FROM assignment_colleges GROUP BY userID) assc ON assc.maxid = assignment_colleges.id) 
                    assc2 ON assc2.collegeID = depc2.collegeID 
                    INNER JOIN referrals ON referrals.studentID = students.id
                    INNER JOIN school_years ON school_years.id = referrals.schoolYearID
                    WHERE assc2.userID ='".Auth::guard('myuser')->id()."'
                    AND DATE(students.created_at) <= '".$systemdate[0]->systemDate."'
                    AND (students.firstName LIKE '%$search%' OR students.lastName LIKE '%$search%') ";
                    if($schoolyear != "all"){
                        $sql .= " AND referrals.schoolYearID = '$schoolyear'";
                    }
                    if($semester != "all"){
                        $sql .= " AND referrals.semesterID = '$semester'";
                    }
                    if($coursegrade != "all"){
                        $sql .= " AND cs2.courseID = '$coursegrade'";
                    }
                    
                break;
                case 'seniorhigh':
                    $sql .= " AND  shs.studentType = 'seniorhigh' 
                              AND  shs.gradeLevel = '".$designation[1]."'";
                     if($schoolyear != "all"){
                        $sql .= " AND referrals.schoolYearID = '$schoolyear'";
                    }
                    if($semester != "all"){
                        $sql .= " AND referrals.semesterID = '$semester'";
                    }
                break;
                case 'juniorhigh':
                    $sql .= " AND  jhs.studentType = 'juniorhigh' AND jhs.gradeLevel = '".$designation[1]."'";
                     if($schoolyear != "all"){
                        $sql .= " AND referrals.schoolYearID = '$schoolyear'";
                    }
                    if($semester != "all"){
                        $sql .= " AND referrals.semesterID = '$semester'";
                    }
                break;
                case 'elementary':
                    $sql .= " AND elem.studentType = 'elementary' AND  elem.gradeLevel = '".$designation[1]."'";
                    if($schoolyear != "all"){
                        $sql .= " AND referrals.schoolYearID = '$schoolyear'";
                    }
                    if($semester != "all"){
                        $sql .= " AND referrals.semesterID = '$semester'";
                    }
                break;
            }
        }
        $sql .= "GROUP BY referrals.id ORDER BY students.lastName ASC";
        if($totalRow=="true"){
            $referral = DB::select(DB::raw($sql));
            return count($referral);
        }else{
            $sql .=" LIMIT $skip, 10";
            $referral = DB::select(DB::raw($sql));
            if(count($referral)==0){
                return "<h2>There are no referrals found.</h2>";
            }else{
                $html = '';
                $this->rowCounter = 0;
                foreach ($referral as $referrals) {
                    $this->rowCounter++;
                    $html .= $this->getReferralRowFaster($referrals, $viewby);
                }
                return $html;
            }
        }
    }
    public function getReferralRowFaster($referral, $viewby){
        $gradelevel = '';
        $gradelevel = $referral->courseGrade;
        $course =  '';
        $studenttype= '';
        if($viewby == 'all'){
            $studenttype = $referral->studentTypes;
        }else{
            $studenttype =$viewby;
        }
        if($referral->studentTypes =="college"){
            $course =  $referral->courseName;
        }else if ($referral->studentTypes =="seniorhigh") {
            $course =  $referral->strandName;
        }
        return "<tr id ='referralRow-$referral->id'>
                    <td>$this->rowCounter</td>
                    <td>".$this->getBeautifiedName($referral->firstName, $referral->middleName, $referral->lastName)."</td>
                    <td>".$this->getBeautifiedStudentType($studenttype)."</td>
                    <td>$course</td>
                    <td>$gradelevel</td>
                   
                    <td>$referral->referralDate</td>
                    <td>
                        <button class='btn btn-warning messageParentsReferral' id ='messageParentsReferral-$referral->id' data-value ='$referral->studentID'>
                            <i class='fa fa-envelope'></i>
                        </button>
                    </td>
                    <td>
                        <button class='btn btn-dark viewReferral' id ='viewReferral-$referral->id' data-value ='$referral->studentID'>
                            <i class='fa fa-eye'></i>
                        </button>
                    </td>
                </tr>";
    } 
    public function getBeautifiedName($firstname, $middlename, $lastname){
        $middlename2 = '';
        if($middlename != ''){
            $middlename2 = ucfirst(substr($middlename, 0, 1)).".";
        }
        return $lastname.", ".$firstname." $middlename2";
    }
    public function getReferralRow($referral){
        $student = new studentController;
        $studenttype = $student->getStudentType($referral->studentID);
        $gradelevel;
        switch ($studenttype) {
            case 'college':
                $temp = collegeStudent::where('studentID', '=', $referral->studentID)->get();
                if(count($temp)>0){
                    $gradelevel = $temp[0]->yearLevel;
                }else{
                    $gradelevel = 0;
                }
            break;
            case 'seniorhigh':
                $temp = SHSStudent::where('studentID', '=', $referral->studentID)->get();
                if(count($temp)>0){
                    $gradelevel = $temp[0]->gradeLevel;
                }else{
                    $gradelevel = 0;
                }
            break;
            case 'juniorhigh':
                $temp = JHStudent::where('studentID', '=', $referral->studentID)->get();
                if(count($temp)>0){
                    $gradelevel = $temp[0]->gradeLevel;
                }else{
                    $gradelevel = 0;
                }
            break;
            case 'elementary':
                $temp = elemStudent::where('studentID', '=', $referral->studentID)->get();
                if(count($temp)>0){
                    $gradelevel = $temp[0]->gradeLevel;
                }else{
                    $gradelevel = 0;
                }
            break;
            
            default:
                
                break;
        }
        
        return "<tr id ='referralRow-$referral->id'>
                    <td>$this->rowCounter</td>
                    <td>".$this->getBeautifiedName($referral->firstName, $referral->middleName, $referral->lastName)."</td>
                    <td>".$this->getBeautifiedStudentType($studenttype)."</td>
                    <td>$gradelevel</td>
                    <td>".explode('-', $referral->schoolYearStart)[0]."-".explode('-', $referral->schoolYearEnd)[0]."</td>
                    <td>$referral->semesterID</td>
                    <td>$referral->referralDate</td>
                    <td>
                        <button class='btn btn-warning messageParentsReferral' id ='messageParentsReferral-$referral->id' data-value ='$referral->studentID'>
                            <i class='fa fa-envelope'></i>
                        </button>
                    </td>
                    <td>
                        <button class='btn btn-dark viewReferral' id ='viewReferral-$referral->id' data-value ='$referral->studentID'>
                            <i class='fa fa-eye'></i>
                        </button>
                    </td>
                </tr>";
    }
    public function getReferralData($referralid){
        $referral = referral::find($referralid);
        $schoolyear = schoolYear::find($referral->schoolYearID);
        //error_log($referralid);
        $referral->schoolYear = explode('-', $schoolyear->schoolYearStart)[0].'-'.explode('-', $schoolyear->schoolYearEnd)[0];
        $html = '';
        $behaviour = behaviour::join('behaviour_settings', 'behaviour_settings.id', '=', 'behaviours.behavioural')
                                ->select('behaviours.*', 'behaviour_settings.name')
                                ->where('behaviours.referralID', '=', $referralid)->get();
        foreach ($behaviour as $behaviours) {
            $html .= "<tr data-value = '$behaviours->behavioural'>
                        <td>$behaviours->name</td>
                        <td>$behaviours->frequency</td>
                        <td>$behaviours->remarks</td>
                        <td><button class='btn btn-dark removeBehavior'><i class='fa fa-trash-o'></i></button></td>
                      </tr>";
        }
        $referral->html = $html;
        return $referral;
    }
    public function updateReferral(Request $request){
        $this->validate($request, array(
            'referralDate'=>'required',
            'referredToReferralFormText'=>'required|max:100',
            'referredByReferralFormText'=>'required|max:100'
        ));
        $this->request = $request;
        DB::transaction(function() {
            $arr = json_decode($this->request->behavior);
            $referral = referral::find($this->request->id);
            $referral->studentID =$this->request->studentID;
            /*$referral->semesterID = (new studentController)->getCurrentSem();
            $referral->schoolYearID =(new studentController)->getCurrentSchoolYear();*/
            $referral->referralDate = $this->request->referralDate;
            $referral->referredTo = $this->request->referredToReferralFormText;
            $referral->referredBy = $this->request->referredByReferralFormText;
            $referral->universityCollegeID = $this->request->unviersityCollege;
            $referral->collegeName = $this->request->collegeName;
            $referral->save();
            $myitems;
            $mybehavior = behaviour::where('referralID', '=', $this->request->id)->get();
            foreach ($mybehavior as $mybehaviors) {
                $mybehaviors->forceDelete();
            }
            for ($i=0; $i < count($arr); $i++) { 
                $myitems[$i]['behavioural']= $arr[$i]->behavior;
                $myitems[$i]['frequency']= $arr[$i]->frequency;
                $myitems[$i]['remarks']= $arr[$i]->remarks;
                $myitems[$i]['referralID']= $referral->id;
            }
            
            DB::table("behaviours")->insert($myitems);
            $referral->schoolYearEnd = schoolYear::find($referral->schoolYearID)->schoolYearEnd;
            $referral->schoolYearStart = schoolYear::find($referral->schoolYearID)->schoolYearStart;
            $referral->firstName = student::find($referral->studentID)->firstName;
            $referral->lastName = student::find($referral->studentID)->lastName;
            $this->rowCounter = $this->request->rowCounter;
            $this->html = $this->getReferralRow($referral);
        });
        activityController::insertActivity('Updated a referral for student with student id:'.$this->request->studentID);
        return $this->html;
    }
    public function getCoordinators(){
        $users = useraccount::where('status', '=', '1')
                            ->where('userType', '=', 'coordinator')
                            ->get();
        if(count($users)==0){
            return "<h2>No coordinators found!</h2>";
        }else{
            $html = '';
            foreach ($users as $user) {
                $designation = '';
                if($user->designation ==''){
                    $collegedesig = assignmentCollege::join('colleges', 'assignment_colleges.collegeID', '=', 'colleges.id')
                    ->where('assignment_colleges.userID','=', $user->id)
                    ->select('colleges.collegeName')->get();
                    if(count($collegedesig)>0){
                        $designation = $collegedesig[0]->collegeName;
                    }
                }else{
                    $tempDesig = "";
                    switch ($user->designation) {
                        case 'seniorhigh':
                            $tempDesig = "Senior High ";
                        break;
                        case 'juniorhigh':
                            $tempDesig = "Junior High ";
                        break;
                        case 'elementary':
                            $tempDesig = "Elementary ";
                        break;
                        default:
                            # code...
                            break;
                    }
                    $designation = $tempDesig.': '.$user->designationLevel;
                } 
                $html .= "<tr>
                                <td>$user->firstName $user->lastName</td>
                               
                                <td>$designation</td>   
                                <td>
                                    <button class='btn btn-dark selectCoordinator'>
                                        <i class='fa fa-check'></i>
                                    </button>
                                </td>
                            </tr>";
            }
            return $html;
        }
    }
    public function getFamilyOfStudent($studentid){
        $family = family::where('studentID', '=', $studentid)->get();
        if(count($family)==0){
            return "<h2>These student have no families in database</h2>";
        }
        $html = '';
        foreach ($family as $family) {
            $html .=  "<tr id ='familyStudent-$family->id'>
                            <td>
                                <input type='checkbox' id = 'checkFamily-$family->id' value = '$family->id' class='checkFamily'>
                            </td>
                            <td>$family->name</td>
                            <td>".ucfirst($family->relationship)."</td>
                            <td>$family->contactNo</td>
                        </tr>";
        }
        $student = student::find($studentid);
        $html .=  "<tr id ='familyStudent-$student->id'>
                        <td>
                            <input type='checkbox' id = 'checkStudent-$student->id' value = '$student->id' class='checkStudent'>
                        </td>
                        <td>$student->firstName $student->lastName</td>
                        <td>Student</td>
                        <td>$student->contactNo</td>
                    </tr>";
        return $html;
    }
    public function messageReferral(Request $request){
        $this->validate($request, array(
            'message'=>'required'
        ));
        if(isset($request->studentID)){
            $temp = new \stdClass();
            $student = student::find($request->studentID);
            $ch = curl_init();
            $itexmo = array('1' => $student->contactNo, '2' => $request->message, '3' => 'TR-EPHRA940314_T1W6X');
            curl_setopt($ch, CURLOPT_URL,"https://www.itexmo.com/php_api/api.php");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, 
            http_build_query($itexmo));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec ($ch);
            error_log($response);
            curl_close ($ch);
            $message = new referralMessage;
            $message->referralID = $request->referralID;
            $message->familyName = $student->firstName." ".$student->lastName;
            $message->contactNo = $student->contactNo;
            $message->message = $request->message;
            $message->save();
            activityController::insertActivity('Sent a message:'.$request->message);
            $temp->referralID = $request->referralID;
            $student->name  = $student->firstName." ".$student->lastName;

            $student->message = $request->message;
            return $student;
        }else{
            $family = family::find($request->family);
            $family->message = $request->message;
            $ch = curl_init();
            $itexmo = array('1' => $family->contactNo, '2' => $request->message, '3' => 'TR-EPHRA940314_T1W6X');
            curl_setopt($ch, CURLOPT_URL,"https://www.itexmo.com/php_api/api.php");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, 
            http_build_query($itexmo));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec ($ch);
            error_log($response);
            curl_close ($ch);
            $message = new referralMessage;
            $message->referralID = $request->referralID;
            $message->familyName = $family->name;
            $message->contactNo = $family->contactNo;
            $message->message = $request->message;
            $message->save();
            
            activityController::insertActivity('Sent a message:'.$request->message);

            return $family;
        }
    } 
    public function getMessageLogs($referralid){
        $message = referralMessage::where('referralID', '=', $referralid)->get();
        if(count($message)>0){
            $html = '';
            foreach ($message as $messages) {
                $html .= "<tr>
                            <td>$messages->familyName</td>
                            <td>$messages->contactNo</td>
                            <td>$messages->message</td>
                        </tr>";
            }
            return $html;
        }else{
            return "<h2>No messages sent for this student's family yet</h2>";
        }
        // <th>Name</th><th>contactno</th><th>Message</th>
    }
    public function getAdminSetting(){
        $temp = new \stdClass();
        $count = adminsetting::all();
        if(count($count)>0){
            $adminsetting = adminsetting::find($count[0]->id);
            $html = "<header style = 'position:fixed; top:-100px; left:0px; right:0px; height: 60px;'>
                        <div style = 'width:19%; display:inline-block;  padding: 0px 0px 0px 50px;'>
                            <img style = 'height:100px; width:100px; border-radius:50px;' src = 'images-database/adminsetting/schoolimage/$adminsetting->id.$adminsetting->schoolSealImageType'>
                        </div>
                        <div style = 'width:50%; height:120px; display:inline-block;  margin:0px;'>
                            <center>
                                <h4 class='header' style='color:black; margin:0px;'>$adminsetting->schoolName</h4>
                                <h4 class='header' style='color:black; margin:0px;'>$adminsetting->guidanceName</h4>
                                <h4 class='header' style='color:black; margin:0px;'>$adminsetting->streetAddress, $adminsetting->barangay</h4>
                                <h4 class='header' style='color:black; margin:0px;'>$adminsetting->city</h4>
                                <h4 class='header' style='color:black; margin:0px;'>Philippines, 7000</h4>
                            </center>    
                        </div>
                        <div style = 'width:15%; display:inline-block; padding: 0px 50px 0px 0px;  text-align: right;'>
                            <img style = 'height:100px; width:100px; border-radius:50px;' src = 'images-database/adminsetting/guidanceimage/$adminsetting->id.$adminsetting->guidanceSealImageType'>
                        </div>
                    </header>";
            return $html;
        }else{
            return "";
        }
    }
    public function getYearOfActiveSchoolYear(){
        $schoolyear = schoolYear::where('status','=', '1')->get();
        if(count($schoolyear)>0){
            return explode('-', $schoolyear[0]->schoolYearStart)[0]." - ".explode('-', $schoolyear[0]->schoolYearEnd)[0];
        }else{
            return "";
        }
    }
    public function printReferralReport($viewby, $coursegrade, $schoolyear, $semester, $search = ''){
        $referral;
        $thissemester = 'all Semester';
        $thisschoolyear = 'all School year';
        $user = new userController();
        $systemdate = systemDate::all();
        $sql='';
      
        if(session()->get('user')['userType']=='admin'){
            $sql = "SELECT COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) 
            semesterID, COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) 
            schoolYearID, COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) studentTypes,
            col.studentType, shs.studentType, jhs.studentType, elem.studentType,
            COALESCE(col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) courseGrade, 
            col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel,
            COALESCE(col.studentNo, shs.studNo, jhs.studNo, elem.studNo) studentNo,  students.firstName, students.lastName,
            students.middleName,
            col.courseName, col.yearLevel,
            school_years.schoolYearStart,
            school_years.schoolYearEnd,
            referrals.*
            FROM students LEFT JOIN (SELECT college_students.studentID, 
            college_students.yearLevel, college_students.semesterID, college_students.schoolYearID,
            college_students.id, 'college' AS studentType, 
            college_students.courseID, courses.courseName, college_students.studentNo FROM
            college_students INNER JOIN (SELECT max(id) as maxid FROM college_students GROUP 
            BY studentID) cs ON cs.maxid = college_students.id
            INNER JOIN courses ON courses.id = college_students.courseID) col ON 
            col.studentID = students.id LEFT 
            JOIN (SELECT studentID, semesterID, schoolYearID, id, 'seniorhigh' AS studentType, 
            gradeLevel, studNo 
            FROM s_h_s_students INNER JOIN (SELECT max(id) as maxid FROM s_h_s_students GROUP BY studentID) 
            shs ON shs.maxid = s_h_s_students.id) shs ON shs.studentID = students.id
            LEFT JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'juniorhigh' AS 
            studentType, gradeLevel, studNo  FROM j_h_students INNER JOIN (SELECT max(id) as maxid FROM j_h_students 
            GROUP BY studentID) jhs ON jhs.maxid = j_h_students.id) jhs ON jhs.studentID = students.id LEFT 
            JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'elementary' AS studentType, 
            gradeLevel, studNo FROM elem_students INNER JOIN (SELECT max(id) as maxid FROM elem_students GROUP 
            BY studentID) elem ON elem.maxid = elem_students.id) elem ON elem.studentID = students.id
            INNER JOIN referrals ON referrals.studentID =  students.id
            INNER JOIN school_years ON school_years.id = referrals.schoolYearID
            LEFT JOIN behaviours ON behaviours.referralID = referrals.id
        WHERE (students.firstName LIKE '%$search%' OR students.lastName LIKE '%$search%'
                OR referrals.referredTo LIKE '%$search%' OR referrals.referredBy LIKE '%$search%' OR
                behaviours.remarks LIKE '%$search%'  OR behaviours.behavioural LIKE '%$search%') 
        AND DATE(students.created_at) <= '".$systemdate[0]->systemDate."' ";
            switch ($viewby) {
                case 'college':
                    $sql .= " AND col.studentType = 'college'";
                break;
                case 'seniorhigh':
                    $sql .= " AND shs.studentType = 'seniorhigh'";
                break;
                case 'juniorhigh':
                    $sql .= " AND jhs.studentType = 'juniorhigh'";
                break;
                case 'elementary':
                    $sql .= " AND elem.studentType = 'elementary'";
                break;
                default:
                   
                break;
            }
            if($schoolyear!="all"){
                $thisschoolyear = $this->getYearOfActiveSchoolYear()." School Year";
                $sql .= " AND referrals.schoolYearID = '$schoolyear'";
            }
            if($semester!="all"){
                switch ($semester) {
                    case '1':
                        $thissemester = '1st Semester';
                    break;
                    case '2':
                        $thissemester = '2nd Semester';
                    break;
                    case '3':
                        $thissemester = '3rd Semester';
                    break;
                    default:
                        # code...
                        break;
                }
                $sql .= " AND referrals.semesterID = '$semester'";
            }
            if($coursegrade!="all"){
                switch ($viewby) {
                    case 'college':
                        $sql .= " AND col.courseID = '$coursegrade'";
                    break;
                    case 'seniorhigh':
                        $sql .= "  AND shs.gradeLevel = '$coursegrade'";
                    break;
                    case 'juniorhigh':
                        $sql .= "  AND jhs.gradeLevel = '$coursegrade'";
                    break;
                    case 'elementary':
                        $sql .= "  AND elem.gradeLevel = '$coursegrade'";
                    break;
                    default:
                        $sql .= " AND COALESCE(col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) = '$coursegrade'";
                    break;
                }
            }
        }else{
            $designation = $user->getUserDesignation(Auth::guard('myuser')->id());
            $sql = "SELECT COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) 
            semesterID, COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) 
            schoolYearID, COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) studentTypes,
            col.studentType, shs.studentType, jhs.studentType, elem.studentType, 
            COALESCE(col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) courseGrade, 
            col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel,
            COALESCE(col.studentNo, shs.studNo, jhs.studNo, elem.studNo) studentNo, students.firstName, students.lastName,
            school_years.schoolYearStart,
            school_years.schoolYearEnd,
            students.middleName,
            referrals.*,
            col.courseName, col.yearLevel
            FROM students LEFT JOIN (SELECT college_students.studentID, 
            college_students.yearLevel, college_students.semesterID, college_students.schoolYearID,
            college_students.id, 'college' AS studentType, 
            college_students.courseID, courses.courseName, college_students.studentNo FROM
            college_students INNER JOIN (SELECT max(id) as maxid FROM college_students GROUP 
            BY studentID) cs ON cs.maxid = college_students.id
            INNER JOIN courses ON courses.id = college_students.courseID) col ON 
            col.studentID = students.id LEFT 
            JOIN (SELECT studentID, semesterID, schoolYearID, id, 'seniorhigh' AS studentType, 
            gradeLevel, studNo 
            FROM s_h_s_students INNER JOIN (SELECT max(id) as maxid FROM s_h_s_students GROUP BY studentID) 
            shs ON shs.maxid = s_h_s_students.id) shs ON shs.studentID = students.id
            LEFT JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'juniorhigh' AS 
            studentType, gradeLevel, studNo  FROM j_h_students INNER JOIN (SELECT max(id) as maxid FROM j_h_students 
            GROUP BY studentID) jhs ON jhs.maxid = j_h_students.id) jhs ON jhs.studentID = students.id LEFT 
            JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'elementary' AS studentType, 
            gradeLevel, studNo FROM elem_students INNER JOIN (SELECT max(id) as maxid FROM elem_students GROUP 
            BY studentID) elem ON elem.maxid = elem_students.id) elem ON elem.studentID = students.id
            INNER JOIN referrals ON referrals.studentID =  students.id
            INNER JOIN school_years ON school_years.id = referrals.schoolYearID
            WHERE (students.firstName LIKE '%$search%' OR students.lastName LIKE '%$search%') 
            AND DATE(students.created_at) <= '".$systemdate[0]->systemDate."' ";
            switch($designation[0]){
                case 'college':
                    $sql= "SELECT cs2.courseID AS courseGrade, c2.courseName, cs2.schoolYearID, cs2.studentNo, 
                    cs2.yearLevel, students.firstName, students.lastName, 'college' AS studentTypes, cs2.semesterID,
                    referrals.*,
                    students.middleName,
                    school_years.schoolYearStart,
                    school_years.schoolYearEnd  FROM students 
                    INNER JOIN (SELECT studentID, studentNo,
                    id, courseID, schoolYearID, yearLevel, semesterID FROM college_students  INNER JOIN (SELECT max(id) as maxid 
                    FROM college_students 
                    GROUP BY studentID) cs ON cs.maxid = college_students.id) cs2 ON cs2.studentID = students.id 
                    INNER JOIN (SELECT c.id AS courseID, dep.departmentID, c.courseName FROM courses c INNER JOIN (SELECT
                    id, courseID FROM course_department_hists cd INNER JOIN (SELECT max(id) as maxid FROM 
                    course_department_hists GROUP BY courseID) cd2 ON cd2.maxid = cd.id) cdh ON cdh.courseID = c.id
                    INNER JOIN (SELECT departmentID, id FROM course_department_hists INNER JOIN (SELECT
                    max(id) AS maxid FROM course_department_hists GROUP BY courseID) cdh1 ON cdh1.maxid = 
                    course_department_hists.id) dep ON dep.id =cdh.id) c2 ON c2.courseID = cs2.courseID 
                    INNER JOIN (SELECT departmentID, id, collegeID FROM department_college_hists
                    INNER JOIN (SELECT max(id) as maxid FROM department_college_hists GROUP BY departmentID) 
                    depc ON depc.maxid = department_college_hists.id) depc2 ON depc2.departmentID=c2.departmentID 
                    INNER JOIN (SELECT collegeID, userID, id FROM assignment_colleges INNER JOIN (SELECT max(id) 
                    as maxid FROM assignment_colleges GROUP BY userID) assc ON assc.maxid = assignment_colleges.id) 
                    assc2 ON assc2.collegeID = depc2.collegeID 
                    INNER JOIN referrals ON referrals.studentID = students.id
                    INNER JOIN school_years ON school_years.id = referrals.schoolYearID
                    WHERE assc2.userID ='".Auth::guard('myuser')->id()."'
                    AND DATE(students.created_at) <= '".$systemdate[0]->systemDate."'
                    AND (students.firstName LIKE '%$search%' OR students.lastName LIKE '%$search%') ";
                    if($schoolyear != "all"){
                        $thisschoolyear = $this->getYearOfActiveSchoolYear()." School Year";
                        $sql .= " AND referrals.schoolYearID = '$schoolyear'";
                    }
                    if($semester != "all"){
                        switch ($semester) {
                            case '1':
                                $thissemester = '1st Semester';
                            break;
                            case '2':
                                $thissemester = '2nd Semester';
                            break;
                            case '3':
                                $thissemester = '3rd Semester';
                            break;
                            default:
                                # code...
                                break;
                        }
                        $sql .= " AND referrals.semesterID = '$semester'";
                    }
                    if($coursegrade != "all"){
                        $sql .= " AND cs2.courseID = '$coursegrade'";
                    }
                    
                break;
                case 'seniorhigh':
                    $sql .= " AND  shs.studentType = 'seniorhigh' 
                              AND  shs.gradeLevel = '".$designation[1]."'";
                     if($schoolyear != "all"){
                        $thisschoolyear = $this->getYearOfActiveSchoolYear()." School Year";
                        $sql .= " AND referrals.schoolYearID = '$schoolyear'";
                    }
                    if($semester != "all"){
                        switch ($semester) {
                            case '1':
                                $thissemester = '1st Semester';
                            break;
                            case '2':
                                $thissemester = '2nd Semester';
                            break;
                            case '3':
                                $thissemester = '3rd Semester';
                            break;
                            default:
                                # code...
                                break;
                        }
                        $sql .= " AND referrals.semesterID = '$semester'";
                    }
                break;
                case 'juniorhigh':
                    $sql .= " AND  jhs.studentType = 'juniorhigh' AND jhs.gradeLevel = '".$designation[1]."'";
                     if($schoolyear != "all"){
                        $thisschoolyear = $this->getYearOfActiveSchoolYear()." School Year";
                        $sql .= " AND referrals.schoolYearID = '$schoolyear'";
                    }
                    if($semester != "all"){
                        switch ($semester) {
                            case '1':
                                $thissemester = '1st Semester';
                            break;
                            case '2':
                                $thissemester = '2nd Semester';
                            break;
                            case '3':
                                $thissemester = '3rd Semester';
                            break;
                            default:
                                # code...
                                break;
                        }
                        $sql .= " AND referrals.semesterID = '$semester'";
                    }
                break;
                case 'elementary':
                    $sql .= " AND elem.studentType = 'elementary' AND  elem.gradeLevel = '".$designation[1]."'";
                    if($schoolyear != "all"){
                        $thisschoolyear = $this->getYearOfActiveSchoolYear()." School Year";
                        $sql .= " AND referrals.schoolYearID = '$schoolyear'";
                    }
                    if($semester != "all"){
                        switch ($semester) {
                            case '1':
                                $thissemester = '1st Semester';
                            break;
                            case '2':
                                $thissemester = '2nd Semester';
                            break;
                            case '3':
                                $thissemester = '3rd Semester';
                            break;
                            default:
                                # code...
                                break;
                        }
                        $sql .= " AND referrals.semesterID = '$semester'";
                    }
                break;
            }
        }
        $referral = DB::select(DB::raw($sql));
        $html = '';
        if(count($referral)==0){
            $html =  "<tr><h2>There are no referrals found</h2></tr>";
        }else{
            $counter = 0;
            foreach ($referral as $key => $value) {
                
                $html .= "<tr>
                            <td>".(++$counter)."</td>
                            <td>".$referral[$key]->firstName." ".$referral[$key]->lastName."</td>
                            <td>".date("F j, Y", strtotime($referral[$key]->referralDate))."</td>
                            <td>".$referral[$key]->referredTo."</td>
                            <td>".$referral[$key]->referredBy."</td>
                        </tr>";
            }
            if($html == ""){
                $html = "<tr><h2>There are no referrals found</h2></tr>";
            }
        }
        $title = "List of Students with Referrals for ".$thissemester." and ".$thisschoolyear."";
        $myhtml = "
            <html>
                <head>
                    <style>
                        table {
                            font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif;
                            border-collapse: collapse;
                            width: 100%;
                        }
                        .well{
                            min-height: 20px;
                            padding: 19px;
                            margin-bottom: 20px;
                            background-color: #f5f5f5;
                            border: 1px solid #e3e3e3;
                            border-radius: 4px;
                            -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
                            box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
                        }
                        table td, table th {
                            border: 1px solid #ddd;
                            padding: 8px;
                        }
                        
                        table tr:nth-child(even){background-color: #f2f2f2;}
                        
                        table tr:hover {background-color: #ddd;}
                        
                        table th {
                            padding-top: 12px;
                            padding-bottom: 12px;
                            text-align: left;
                            background-color: #4CAF50;
                            color: white;
                        }
                        .filterContainer{
                            display:inline-block;
                            width:23%;
                            
                        }
                        @page{
                            margin-left:20px;
                            margin-right:20px;
                            margin-top:160px;
                        }
                        .imageWrapper {
                            float:left;
                        
                        }
                        .thisImage{
                        height:200px;
                        width:150px;
                        }
                        
                    </style>
                </head>
                <body>
                ".$this->getAdminSetting()."
                    <div style = 'margin-bottom:20px; margin-top:20px;'>
                        <center><h2 style = 'text-decoration:underline;'>$title</h2></center>
                    </div>
                    <table>
                        
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Student</th>
                               
                                <th>Referral Date</th>
                                <th>Referred To</th>
                                <th>Referred By</th>
                            </tr>
                        </thead>
                        <tbody>

                     
        ";
        $myhtml .= $html;
        $html.="
                        </tbody>
                    </table>
                </body>
            </html>";
        
        PDF::loadHTML($myhtml)->save(public_path("temp/".'report.pdf'));
        return "true";
    }
    public function getDefaultCoordinator(){
        if(session()->get('user')['userType']=='admin'){
            return '';
        }else{
            $users = useraccount::find(Auth::guard('myuser')->id());
            if($users->designationLevel ==''){
               
                $coordinator = DB::select(DB::raw("SELECT useraccounts.firstName, useraccounts.lastName 
                FROM useraccounts INNER JOIN (SELECT userID, collegeID FROM assignment_colleges INNER 
                JOIN (SELECT MAX(id) AS maxid FROM assignment_colleges GROUP BY userID) ac ON ac.maxid = 
                assignment_colleges.id) ac2 ON useraccounts.id = ac2.userID WHERE useraccounts.userType = 
                'coordinator' AND ac2.collegeID = (SELECT
                collegeID FROM assignment_colleges WHERE id = (SELECT MAX(id) AS maxid FROM 
                assignment_colleges WHERE userID = '".Auth::guard('myuser')->id()."'))"));
                if(count($coordinator)>0){
                    return $coordinator[0]->firstName." ".$coordinator[0]->lastName;
                }else{
                    return "";
                }
            }else{
                $coordinator = DB::select(DB::raw("
                SELECT firstName, lastName FROM useraccounts WHERE userType = 
                'coordinator' AND designationLevel = (SELECT designationLevel 
                FROM useraccounts WHERE id = '".Auth::guard('myuser')->id()."')"));
                if(count($coordinator)>0){
                    return $coordinator[0]->firstName." ".$coordinator[0]->lastName;
                }else{
                    return "";
                }
            }
        }
    }
}
