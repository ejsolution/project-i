@extends('admin.adminTemplate')

@section('title', 'AdminDashboard')
@section('stylesheet')
  <link href="{{asset('css/admin/settings.css')}}" rel="stylesheet">
  <link href="{{asset('css/admin/activitylogs/activitylogs.css')}}" rel="stylesheet">
@stop
@section('content')
  <div class='container'> 
    <div class='row'>
      <div class = 'col-md-6'>
        
      </div>
      <div class='col-md-6 genericDivPadTop'>
        <div class="form-group has-feedback">
            <input type="text" class="form-control"  placeholder="Search by date, name or activity" id ='searchActivityLog'/>
            <i class="fa fa-search form-control-feedback"></i>
        </div>
      </div>
    </div>
    <div class='row'>
      <div class='col-md-12 tableContainer'>
        <table class='table table-striped' id = 'tableLogs'>
          <thead>
            <tr>
              <th>Name</th>
              <th>Date & Time</th>
              <th>Activity</th>
            </tr>
          </thead>
          <tbody id = 'activityLogsTable'>
          </tbody>
        </table>
      </div>
    </div>
    <div class='genericDivTop'>
        <center><ul id="pagination" class="pagination-sm"></ul></center>
    </div>
    <div class='row'>
      <div class='alignRight col-md-12'>
        <a class='btn btn-dark' href = '{{route('activitylogs.print')}}'>
          <i class='fa fa-print'>&nbsp; Print</i>
        </a>
      </div>
    </div>
  </div>
  
@endSection
@section('scripts')
  <script src = '{{asset('js/jqueryPagination.js')}}'></script>
  <script src = '{{asset('js/activitylogs/activitylogs.js')}}'></script>
@stop
