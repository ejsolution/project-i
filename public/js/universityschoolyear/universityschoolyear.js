$(document).ready(function(){
    var schoolyearid;
    var status;
    tableLoader('7', 'schoolyeartable');
    getAjax('/schoolyear/lists', 'schoolyeartable')
    $('#fromDateSchoolYear').on('change', function(){
        var date  = new Date($(this).val());
        $('#toDateSchoolYear').val((parseInt(date.getFullYear())+1)+'-'+getMonth(date.getMonth())+'-'+getDay(date.getDate()));
        if($('#semesterType').val()=='2'){
            $('#fromFirstSemester').val((date.getFullYear())+'-'+getMonth(date.getMonth())+'-'+getDay(date.getDate()));
            $('#toFirstSemester').val(getDateSem( $('#fromFirstSemester').val()));
            $('#toSecondSemester').val($('#toDateSchoolYear').val());
            $('#fromSecondSemester').val(getDateSemMinus( $('#toSecondSemester').val()));
        }else{
            $('#fromFirstSemester').val((date.getFullYear())+'-'+getMonth(date.getMonth())+'-'+getDay(date.getDate()));
            $('#toFirstSemester').val(getDateSem( $('#fromFirstSemester').val()));  
            $('#fromSecondSemester').val( $('#toFirstSemester').val());
            $('#toSecondSemester').val(getDateSem($('#toFirstSemester').val()));
            $('#fromThirdSemester').val($('#toSecondSemester').val());
            $('#toThirdSemester').val(getDateSem( $('#fromThirdSemester').val()));
        }

    });
    $('#toDateSchoolYear').on('change', function(){
        var date  = new Date($(this).val());
        if($('#semesterType').val()=='2'){
            $('#fromDateSchoolYear').val((parseInt(date.getFullYear())-1)+'-'+getMonth(date.getMonth())+'-'+getDay(date.getDate()));
            $('#toSecondSemester').val($(this).val());
            $('#fromSecondSemester').val(getDateSemMinus( $('#toSecondSemester').val()));
            $('#fromFirstSemester').val($('#fromDateSchoolYear').val());
            $('#toFirstSemester').val(getDateSem( $('#fromFirstSemester').val()));
        }else{
            $('#fromDateSchoolYear').val((parseInt(date.getFullYear())-1)+'-'+getMonth(date.getMonth())+'-'+getDay(date.getDate()));
            $('#toThirdSemester').val($(this).val());
            $('#fromThirdSemester').val(getDateSemMinus($('#toSecondSemester').val()));
            
            $('#toSecondSemester').val($('#fromThirdSemester').val());
            $('#fromSecondSemester').val(getDateSemMinus( $('#toSecondSemester').val()));
            
            $('#fromFirstSemester').val($('#fromDateSchoolYear').val());
            $('#toFirstSemester').val(getDateSem( $('#fromFirstSemester').val()));
        }

    });
    // $('#fromFirstSemester').on('change', function(){
    //     var fromschoolyear = new Date( $('#fromDateSchoolYear').val());
    //     var thisdate =  new Date($(this).val());
    //     if(thisdate<fromschoolyear){
    //         //alert("Semester start date must not be less than starting school year date!");
    //         new PNotify({
    //             title: 'Error!',
    //             text: "Semester start date must not be less than starting school year date!",
    //             type: 'error',
    //             styling: 'bootstrap3'
    //         });
    //         $(this).val('');
    //         return;
    //     }
    //     $('#toFirstSemester').val(getDateSem( $('#fromFirstSemester').val()));
    // });
    // $('#toFirstSemester').on('change', function(){
    //     $('#fromFirstSemester').val(getDateSemMinus( $('#toFirstSemester').val()));
    //     var fromschoolyear = new Date( $('#fromDateSchoolYear').val());
    //     var thisdate =  new Date($('#fromFirstSemester').val());
    //     if(thisdate<fromschoolyear){
    //         //alert("Semester start date must not be less than starting school year date!");
    //         new PNotify({
    //             title: 'Error!',
    //             text: "Semester start date must not be less than starting school year date!",
    //             type: 'error',
    //             styling: 'bootstrap3'
    //         });
    //         $('#fromFirstSemester').val('');
    //         $(this).val('');
    //         return;
    //     }
    // });
    // $('#fromSecondSemester').on('change', function() {
    //     var fromschoolyear = new Date( $('#toFirstSemester').val());
    //     var thisdate =  new Date($(this).val());
    //     if(thisdate<fromschoolyear){
    //         //alert(" Second Semester start date must not be less than first semester end date!");
    //         new PNotify({
    //             title: 'Error!',
    //             text: "Second Semester start date must not be less than first semester end date!",
    //             type: 'error',
    //             styling: 'bootstrap3'
    //         });
    //         $(this).val('');
    //         return;
    //     }
        
    //     $('#toSecondSemester').val(getDateSem( $('#fromSecondSemester').val()));
    //     if($('#semesterType').val()=='3'){
    //         var fromthirdsemester =  new Date($('#fromThirdSemester').val());
    //         var tosecondsemester =  new Date($('#toSecondSemester').val());
    //         if(tosecondsemester >fromthirdsemester){
    //             //alert("Second Semester start date must not be greater than third semester start date");
    //             new PNotify({
    //                 title: 'Error!',
    //                 text: "Second Semester start date must not be greater than third semester start date",
    //                 type: 'error',
    //                 styling: 'bootstrap3'
    //             });
    //             $('#toSecondSemester').val('');
    //             $(this).val('');
    //             return;
    //         }  
    //     }
    // });
    // $('#toSecondSemester').on('change', function() {
    //     var fromschoolyear = new Date( $('#toDateSchoolYear').val());
    //     var thisdate =  new Date($(this).val());
    //     if(thisdate>fromschoolyear){
    //        //alert(" Second Semester end date must not be greater than school year end date!");
    //         new PNotify({
    //             title: 'Error!',
    //             text: "Second Semester end date must not be greater than school year end date!",
    //             type: 'error',
    //             styling: 'bootstrap3'
    //         });
    //         $(this).val('');
    //         return;
    //     }
    //     var tofirstsemester = new Date( $('#toFirstSemester').val());
    //     var thisdate =  new Date($('#toSecondSemester').val());
    //     if(thisdate<fromschoolyear){
    //         //alert("Second Semester start date must not be less than first semester end date");
    //         new PNotify({
    //             title: 'Error!',
    //             text: "Second Semester start date must not be less than first semester end date",
    //             type: 'error',
    //             styling: 'bootstrap3'
    //         });
    //         $('#fromSecondSemester').val('');
    //         $(this).val('');
    //         return;
    //     }
    //     if($('#semesterType').val()=='3'){
    //         var fromthirdsemester =  new Date($('#fromThirdSemester').val());
    //         if(thisdate >fromthirdsemester){
    //             //alert("Second Semester start date must not be greater than third semester start date");
    //             new PNotify({
    //                 title: 'Error!',
    //                 text: "Second Semester start date must not be greater than third semester start date",
    //                 type: 'error',
    //                 styling: 'bootstrap3'
    //             });
    //             $('#fromSecondSemester').val('');
    //             $(this).val('');
    //             return;
    //         }  
    //     }
    //     $('#fromSecondSemester').val(getDateSemMinus( $('#toSecondSemester').val()));
    // });
    // $('#fromThirdSemester').on('change', function() {
    //     var thisdate =  new Date($(this).val());
    //     var endyeardate  =  new Date($('#toDateSchoolYear').val());
    //     var secondsemenddate =  new Date($('#toSecondSemester').val());
    //     if(thisdate<secondsemenddate){
    //         //alert("Third sem start date must not be less than second sem end date");
    //         new PNotify({
    //             title: 'Error!',
    //             text: "Third sem start date must not be less than second sem end date",
    //             type: 'error',
    //             styling: 'bootstrap3'
    //         });
    //         $(this).val('');
    //         return;
    //     }
    //     $('#toThirdSemester').val(getDateSem($(this).val()));
    // });
    // $('#toThirdSemester').on('change', function() {
    //     var thisdate =  new Date($(this).val());
    //     var endyeardate  =  new Date($('#toDateSchoolYear').val());
    //     if(thisdate>endyeardate){
    //         //alert("Third sem end date must not be greater than school year end date!");
    //         new PNotify({
    //             title: 'Error!',
    //             text: "Third sem end date must not be greater than school year end date!",
    //             type: 'error',
    //             styling: 'bootstrap3'
    //         });
    //         $('#toThirdSemester').val('');
    //         return;
    //     }
    //     $('#fromThirdSemester').val(getDateSemMinus($(this).val()));
    // });
    $('#schoolYearForm').on('submit', function(e) {
       e.preventDefault(); 
       var form = document.getElementById('schoolYearForm');
       var formdata =  new FormData(form)
       $('#schoolyeartable').children('h2').each(function(){
        $(this).remove();
       });
       prependPostAjaxErrorCallback('/schoolyear/insert', formdata, function(data){
        if(data=="conflict"){
            new PNotify({
                title: 'Error!',
                text: 'School year conflicts with other schoolyear',
                type: 'error',
                styling: 'bootstrap3'
            });
        }else{
            $('#schoolyeartable').prepend(data);
            new PNotify({
                title: 'Success!',
                text: 'Successfully saved!',
                type: 'success',
                styling: 'bootstrap3'
            });
        }
       });
    });
    $('#semesterType').on('change', function() {
        if($(this).val() == '2'){
            $('#fromThirdSemester').attr('disabled', true).removeAttr('required'); 
            $('#toThirdSemester').attr('disabled', true).removeAttr('required'); 
        }else{
            $('#fromThirdSemester').attr('disabled', false).attr('required', ''); 
            $('#toThirdSemester').attr('disabled', false).attr('required', ''); 
        }
       
    });
    $('#schoolyeartable').on('click', '.editSchoolYear', function(){
        var cell = [];
        var i = 0;
        schoolyearid = $(this).prop('id').split('-')[1];
        $('#schoolYear-'+$(this).prop('id').split('-')[1]).children('td').each(function(){
            cell[i] = $(this).text();
            i++;
        });
        status = cell[4];
        //console.log(cell);
        $.get('/schoolyear/year/'+$(this).prop('id').split('-')[1], function(data){
            $('#fromDateSchoolYearEdit').val(data['schoolYearStart']);
            $('#toDateSchoolYearEdit').val(data['schoolYearEnd']);
        });
        var firstsemstart = cell[1].split('to')[0].trim();
        //console.log(firstsemstart);
        var firstsemend = cell[1].split('to')[1].trim();
        var secondsemstart = cell[2].split('to')[0].trim();
        var secondsemend = cell[2].split('to')[1].trim();
        var thirdsemstart = cell[3].split('to')[0].trim();
        var thirdsemend = cell[3].split('to')[1].trim();
        if(thirdsemstart == ''){
            $('#semesterTypeEdit').val('2');
        }else{
            $('#semesterTypeEdit').val('3');
        }
        $('#fromFirstSemesterEdit').val(firstsemstart);
        $('#toFirstSemesterEdit').val(firstsemend);

        $('#fromSecondSemesterEdit').val(secondsemstart);
        $('#toSecondSemesterEdit').val(secondsemend);

        $('#fromThirdSemesterEdit').val(thirdsemstart);
        $('#toThirdSemesterEdit').val(thirdsemend);
        $('#editSchoolYearModal').modal('show');

    });

    $('#fromDateSchoolYearEdit').on('change', function(){
        var date  = new Date($(this).val());
        $('#toDateSchoolYearEdit').val((parseInt(date.getFullYear())+1)+'-'+getMonth(date.getMonth())+'-'+getDay(date.getDate()));
        if($('#semesterTypeEdit').val()=='2'){
            $('#fromFirstSemesterEdit').val((date.getFullYear())+'-'+getMonth(date.getMonth())+'-'+getDay(date.getDate()));
            $('#toFirstSemesterEdit').val(getDateSem( $('#fromFirstSemesterEdit').val()));
            $('#toSecondSemesterEdit').val($('#toDateSchoolYearEdit').val());
            $('#fromSecondSemesterEdit').val(getDateSemMinus( $('#toSecondSemesterEdit').val()));
        }else{
            $('#fromFirstSemesterEdit').val((date.getFullYear())+'-'+getMonth(date.getMonth())+'-'+getDay(date.getDate()));
            $('#toFirstSemesterEdit').val(getDateSem( $('#fromFirstSemesterEdit').val()));  
            $('#fromSecondSemesterEdit').val( $('#toFirstSemesterEdit').val());
            $('#toSecondSemesterEdit').val(getDateSem($('#toFirstSemesterEdit').val()));
            $('#fromThirdSemesterEdit').val($('#toSecondSemesterEdit').val());
            $('#toThirdSemesterEdit').val(getDateSem( $('#fromThirdSemesterEdit').val()));
        }

    });
    $('#toDateSchoolYearEdit').on('change', function(){
        var date  = new Date($(this).val());
        if($('#semesterTypeEdit').val()=='2'){
            $('#fromDateSchoolYearEdit').val((parseInt(date.getFullYear())-1)+'-'+getMonth(date.getMonth())+'-'+getDay(date.getDate()));
            $('#toSecondSemesterEdit').val($(this).val());
            $('#fromSecondSemesterEdit').val(getDateSemMinus( $('#toSecondSemesterEdit').val()));
            $('#fromFirstSemesterEdit').val($('#fromDateSchoolYearEdit').val());
            $('#toFirstSemesterEdit').val(getDateSem( $('#fromFirstSemesterEdit').val()));
        }else{
            $('#fromDateSchoolYearEdit').val((parseInt(date.getFullYear())-1)+'-'+getMonth(date.getMonth())+'-'+getDay(date.getDate()));
            $('#toThirdSemesterEdit').val($(this).val());
            $('#fromThirdSemesterEdit').val(getDateSemMinus($('#toSecondSemesterEdit').val()));
            
            $('#toSecondSemesterEdit').val($('#fromThirdSemesterEdit').val());
            $('#fromSecondSemesterEdit').val(getDateSemMinus( $('#toSecondSemesterEdit').val()));
            
            $('#fromFirstSemesterEdit').val($('#fromDateSchoolYearEdit').val());
            $('#toFirstSemesterEdit').val(getDateSem( $('#fromFirstSemesterEdit').val()));
        }

    });
    // $('#fromFirstSemesterEdit').on('change', function(){
    //     var fromschoolyear = new Date( $('#fromDateSchoolYearEdit').val());
    //     var thisdate =  new Date($(this).val());
    //     if(thisdate<fromschoolyear){
    //         //alert("Semester start date must not be less than starting school year date!");
    //         new PNotify({
    //             title: 'Error!',
    //             text: "Semester start date must not be less than starting school year date!",
    //             type: 'error',
    //             styling: 'bootstrap3'
    //         });
    //         $(this).val('');
    //         return;
    //     }
    //     $('#toFirstSemesterEdit').val(getDateSem( $('#fromFirstSemesterEdit').val()));
    // });
    // $('#toFirstSemesterEdit').on('change', function(){
    //     $('#fromFirstSemesterEdit').val(getDateSemMinus( $('#toFirstSemesterEdit').val()));
    //     var fromschoolyear = new Date( $('#fromDateSchoolYearEdit').val());
    //     var thisdate =  new Date($('#fromFirstSemesterEdit').val());
    //     if(thisdate<fromschoolyear){
    //         //alert("Semester start date must not be less than starting school year date!");
    //         new PNotify({
    //             title: 'Error!',
    //             text: "Semester start date must not be less than starting school year date!",
    //             type: 'error',
    //             styling: 'bootstrap3'
    //         });
    //         $('#fromFirstSemesterEdit').val('');
    //         $(this).val('');
    //         return;
    //     }
    // });
    // $('#fromSecondSemesterEdit').on('change', function() {
    //     var fromschoolyear = new Date( $('#toFirstSemesterEdit').val());
    //     var thisdate =  new Date($(this).val());
    //     if(thisdate<fromschoolyear){
    //         //alert(" Second Semester start date must not be less than first semester end date!");
    //         new PNotify({
    //             title: 'Error!',
    //             text: "Second Semester start date must not be less than first semester end date!",
    //             type: 'error',
    //             styling: 'bootstrap3'
    //         });
    //         $(this).val('');
    //         return;
    //     }
        
    //     $('#toSecondSemesterEdit').val(getDateSem( $('#fromSecondSemester').val()));
    //     if($('#semesterTypeEdit').val()=='3'){
    //         var fromthirdsemester =  new Date($('#fromThirdSemesterEdit').val());
    //         var tosecondsemester =  new Date($('#toSecondSemesterEdit').val());
    //         if(tosecondsemester >fromthirdsemester){
    //             //alert("Second Semester start date must not be greater than third semester start date");
    //             new PNotify({
    //                 title: 'Error!',
    //                 text: "Second Semester start date must not be greater than third semester start date",
    //                 type: 'error',
    //                 styling: 'bootstrap3'
    //             });
    //             $('#toSecondSemesterEdit').val('');
    //             $(this).val('');
    //             return;
    //         }  
    //     }
    // });
    // $('#toSecondSemesterEdit').on('change', function() {
    //     var fromschoolyear = new Date( $('#toDateSchoolYearEdit').val());
    //     var thisdate =  new Date($(this).val());
    //     if(thisdate>fromschoolyear){
    //        //alert(" Second Semester end date must not be greater than school year end date!");
    //         new PNotify({
    //             title: 'Error!',
    //             text: "Second Semester end date must not be greater than school year end date!",
    //             type: 'error',
    //             styling: 'bootstrap3'
    //         });
    //         $(this).val('');
    //         return;
    //     }
    //     var tofirstsemester = new Date( $('#toFirstSemesterEdit').val());
    //     var thisdate =  new Date($('#toSecondSemesterEdit').val());
    //     if(thisdate<fromschoolyear){
    //         //alert("Second Semester start date must not be less than first semester end date");
    //         new PNotify({
    //             title: 'Error!',
    //             text: "Second Semester start date must not be less than first semester end date",
    //             type: 'error',
    //             styling: 'bootstrap3'
    //         });
    //         $('#fromSecondSemester').val('');
    //         $(this).val('');
    //         return;
    //     }
    //     if($('#semesterTypeEdit').val()=='3'){
    //         var fromthirdsemester =  new Date($('#fromThirdSemesterEdit').val());
    //         if(thisdate >fromthirdsemester){
    //             //alert("Second Semester start date must not be greater than third semester start date");
    //             new PNotify({
    //                 title: 'Error!',
    //                 text: "Second Semester start date must not be greater than third semester start date",
    //                 type: 'error',
    //                 styling: 'bootstrap3'
    //             });
    //             $('#fromSecondSemesterEdit').val('');
    //             $(this).val('');
    //             return;
    //         }  
    //     }
    //     $('#fromSecondSemesterEdit').val(getDateSemMinus( $('#toSecondSemesterEdit').val()));
    // });
    // $('#fromThirdSemesterEdit').on('change', function() {
    //     var thisdate =  new Date($(this).val());
    //     var endyeardate  =  new Date($('#toDateSchoolYearEdit').val());
    //     var secondsemenddate =  new Date($('#toSecondSemesterEdit').val());
    //     if(thisdate<secondsemenddate){
    //         //alert("Third sem start date must not be less than second sem end date");
    //         new PNotify({
    //             title: 'Error!',
    //             text: "Third sem start date must not be less than second sem end date",
    //             type: 'error',
    //             styling: 'bootstrap3'
    //         });
    //         $(this).val('');
    //         return;
    //     }
    //     $('#toThirdSemesterEdit').val(getDateSem($(this).val()));
    // });
    // $('#toThirdSemesterEdit').on('change', function() {
    //     var thisdate =  new Date($(this).val());
    //     var endyeardate  =  new Date($('#toDateSchoolYearEdit').val());
    //     if(thisdate>endyeardate){
    //         //alert("Third sem end date must not be greater than school year end date!");
    //         new PNotify({
    //             title: 'Error!',
    //             text: "Third sem end date must not be greater than school year end date!",
    //             type: 'error',
    //             styling: 'bootstrap3'
    //         });
    //         $('#toThirdSemesterEdit').val('');
    //         return;
    //     }
    //     $('#fromThirdSemesterEdit').val(getDateSemMinus($(this).val()));
    // });
    $('#semesterTypeEdit').on('change', function() {
        if($(this).val() == '2'){
            $('#fromThirdSemesterEdit').attr('disabled', true).removeAttr('required'); 
            $('#toThirdSemesterEdit').attr('disabled', true).removeAttr('required'); 
        }else{
            $('#fromThirdSemesterEdit').attr('disabled', false).attr('required', ''); 
            $('#toThirdSemesterEdit').attr('disabled', false).attr('required', ''); 
        }
       
    });
    $('#editSchoolYearForm').on('submit', function(e){
        e.preventDefault();
       var form = document.getElementById('editSchoolYearForm');
       var formdata =  new FormData(form)
       formdata.append('id', schoolyearid);
       prependPostAjaxErrorCallback('/schoolyear/update', formdata, function(data){
        if(data=="conflict"){
            new PNotify({
                title: 'Error!',
                text: 'School year conflicts with other schoolyear',
                type: 'error',
                styling: 'bootstrap3'
            });
        }else if(data=="cannotupdate"){
            new PNotify({
                title: 'Error!',
                text: 'Cannot update school year because it is already assigned to other data.',
                type: 'error',
                styling: 'bootstrap3'
            });
        }else{
            if(!$('#setActive-'+schoolyearid).attr('disabled')){
                $('#schoolYear-'+schoolyearid).html(
                    "<td>"+$('#fromDateSchoolYearEdit').val().split('-')[0]+
                    "-"+$('#toDateSchoolYearEdit').val().split('-')[0]+"</td>"+
                    "<td>"+$('#fromFirstSemesterEdit').val()+" to "+$('#toFirstSemesterEdit').val()+"</td>"+
                    "<td>"+$('#fromSecondSemesterEdit').val()+" to "+ $('#toSecondSemesterEdit').val()+"</td>"+
                    "<td>"+$('#fromThirdSemesterEdit').val()+" to "+$('#toThirdSemesterEdit').val()+"</td>"+
                    "<td>"+status+"</td>"+
                    "<td><button class='btn btn-dark setActive' id = 'setActive-"+schoolyearid+"'>"+
                        "<i class='fa fa-edit'></i></button></td>"+
                        "<td><button class='btn btn-info editSchoolYear' id = 'editSchoolYear-"+schoolyearid+"'>"+
                        "<i class='fa fa-edit'></i></button></td>"
    
                );
            }else{
                $('#schoolYear-'+schoolyearid).html(
                    "<td>"+$('#fromDateSchoolYearEdit').val().split('-')[0]+
                    "-"+$('#toDateSchoolYearEdit').val().split('-')[0]+"</td>"+
                    "<td>"+$('#fromFirstSemesterEdit').val()+" to "+$('#toFirstSemesterEdit').val()+"</td>"+
                    "<td>"+$('#fromSecondSemesterEdit').val()+" to "+ $('#toSecondSemesterEdit').val()+"</td>"+
                    "<td>"+$('#fromThirdSemesterEdit').val()+" to "+$('#toThirdSemesterEdit').val()+"</td>"+
                    "<td>"+status+"</td>"+
                    "<td><button class='btn btn-dark setActive' id = 'setActive-"+schoolyearid+"' disabled>"+
                        "<i class='fa fa-edit'></i></button></td>"+
                        "<td><button class='btn btn-info editSchoolYear' id = 'editSchoolYear-"+schoolyearid+"'>"+
                        "<i class='fa fa-edit'></i></button></td>"
    
                );
            }
            
            console.log(data);
            new PNotify({
                title: 'Success!',
                text: 'Successfully saved!',
                type: 'success',
                styling: 'bootstrap3'
            });
        }
       });
    });
    $('#schoolyeartable').on('click', '.setActive', function(){
        var id = $(this).prop('id').split("-")[1];
        $(this).attr('disabled', true);
        $.get('/schoolyear/setactive/'+id, function(data){
            //$('#'+tableid).html(data);
            new PNotify({
                title: 'Success!',
                text: 'Successfully set to active!',
                type: 'success',
                styling: 'bootstrap3'
            });
            var i=0;
            var j = 0;
            
            $('#schoolyeartable').children('tr').each(function(){
                j=0;
                if($(this).prop('id').split('-')[1] != id){
                    $(this).children('td').each(function(){
                        if(j==4){
                            if($(this).text()=='active'){
                                $(this).text('inactive')
                            }
                        }
                        if(j==5){
                            $(this).children('button').each(function(){
                                $(this).attr('disabled', false);
                            });
                        }
                        j++;
                    });
                }
            });
           
            $('#schoolYear-'+id).children('td').each(function(){
                if(i==4){
                    $(this).text('active');
                }
                i++;
            });
          });        
    });
    
});
function getDateSem(date) {
    var mydate = new Date(date);
    mydate.setDate(mydate.getDate()+120);  
    return mydate.getFullYear()+'-'+getMonth(mydate.getMonth())+'-'+getDay(mydate.getDate()) 
}
function getDateSemMinus(date) {
    var mydate = new Date(date);
    mydate.setDate(mydate.getDate()-120);  
    return mydate.getFullYear()+'-'+getMonth(mydate.getMonth())+'-'+getDay(mydate.getDate()) 
}
function getDay(myday){
    var day;
    if(myday<10){
        day ='0'+myday;
    }else{
        day =myday;
    }
    return day;
}
function getMonth(mymonth){
    if(parseInt(mymonth)+1 <10){
        month ='0'+(parseInt(mymonth)+1);
    }else{
        month =(parseInt(mymonth)+1);
    }
    return month
}