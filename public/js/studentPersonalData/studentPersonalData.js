$(document).ready(function(){
  var scbcounter = 0;
  var action = '';
  $.get('/student/getcourses/'+$('#collegeComboModal').val(), function(data){
    if(data=="nocourses"){
    }else{
      $('#courseComboModal').html(data);
      if($('#studentTypeComboModal').val()!='college'){
        if($('#studentTypeComboModal').val()=='seniorhigh'){
          var html = "<option value ='12'>12</option>"+
                    "<option value ='11'>11</option>";
          $('#seniorHighStrand').attr('disabled', false);
        }else if($('#studentTypeComboModal').val()=='juniorhigh'){
          $('#seniorHighStrand').attr('disabled', true);
          var html = "<option value ='10'>10</option>"+
                    "<option value ='9'>9</option>"+
                    "<option value ='8'>8</option>"+
                    "<option value ='7'>7</option>";
        }else if($('#studentTypeComboModal').val()=='elementary'){
          $('#seniorHighStrand').attr('disabled', true);
          var html = "<option value ='6'>6</option>"+
                    "<option value ='5'>5</option>"+
                    "<option value ='4'>4</option>"+
                    "<option value ='3'>3</option>"+
                    "<option value ='2'>2</option>"+
                    "<option value ='1'>1</option>";
        }
        $('#yearLevelComboModal').html(html);
      }else{
        $.get('/student/getcourseyear/'+$('#courseComboModal').val(), function(data){
            $('#yearLevelComboModal').html(data);
        });
      }
    } 
  });
  tableLoader('7', 'studentTable');
  //console.log('/student/lists/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/'+$("#gradeLevelFilter").val()+'/'+$('#searchStudents').val());
  getAjax('/student/lists/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/'+$("#gradeLevelFilter").val()+'/false'+'/'+$('#searchStudents').val(), 'studentTable');
  tableLoader('7', 'studentUpgradesTable');
  getAjax('/student/lists/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/'+$("#gradeLevelFilter").val()+'/true'+'/'+$('#searchStudents').val(), 'studentUpgradesTable');
  $("#addStudentButton").on('click', function(){
    $("#studentFormModal").modal("show");
    $("#studentFormModal .modal-title").text("Add A student");
    action ='add';
  });
  $('#collegeComboModal').on('change', function(){
    $.get('/student/getcourses/'+$('#collegeComboModal').val(), function(data){
      if(data=="nocourses"){
        new PNotify({
          title: 'Error!',
          text: 'This college still has no courses assigned on it!',
          type: 'error',
          styling: 'bootstrap3' 
        });
      }else{
        $('#courseComboModal').html(data);
        
        $.get('/student/getcourseyear/'+$('#courseComboModal').val(), function(data){
            $('#yearLevelComboModal').html(data);
        });
      }
    });
  });
  $('#courseComboModal').on('change', function(){
    $.get('/student/getcourseyear/'+$('#courseComboModal').val(), function(data){
      $('#yearLevelComboModal').html(data);
    });
  });
  $('#studentTypeComboModal').on('change', function(){
    if($(this).val()=='college'){
      $('#collegeComboModal').attr('required', '');
      $('#courseComboModal').attr('required', '');
      $('#seniorHighStrand').attr('disabled', true);
      $('#collegeComboModal').attr('readonly', false);
      $('#courseComboModal').attr('readonly', false);
    }else{
      $('#collegeComboModal').attr('readonly', true);
      $('#courseComboModal').attr('readonly', true);
      $('#collegeComboModal').removeAttr('required');
      $('#courseComboModal').removeAttr('required');
      if($(this).val()=='seniorhigh'){
        var html = "<option value ='12'>12</option>"+
                  "<option value ='11'>11</option>";
        $('#seniorHighStrand').attr('disabled', false);
      }else if($(this).val()=='juniorhigh'){
        $('#seniorHighStrand').attr('disabled', true);
        var html = "<option value ='10'>10</option>"+
                   "<option value ='9'>9</option>"+
                   "<option value ='8'>8</option>"+
                   "<option value ='7'>7</option>";
      }else if($(this).val()=='elementary'){
        $('#seniorHighStrand').attr('disabled', true);
        var html = "<option value ='6'>6</option>"+
                   "<option value ='5'>5</option>"+
                   "<option value ='4'>4</option>"+
                   "<option value ='3'>3</option>"+
                   "<option value ='2'>2</option>"+
                   "<option value ='1'>1</option>";
      }
      $('#yearLevelComboModal').html(html);
    }
  });
  $('#studentForm').on('submit', function(e){
    e.preventDefault();
    var form = document.getElementById('studentForm');
    var myform = new FormData(form);
    var totalStudentsCounter = 0;
    $('#studentTable').children('tr').each(function(){
      totalStudentsCounter++;
    });
    totalStudentsCounter++;
    myform.append('rowCounter', totalStudentsCounter);
    if(action=='add'){
      prependPostAjaxErrorCallback('/student/insert', myform, function(data){
        if(data == 'There is no activated school year yet!'){
          new PNotify({
            title: 'Error!',
            text: data,
            type: 'error',
            styling: 'bootstrap3'
          });
        }else if (data == 'There is no assigned courses in this college!') {
          new PNotify({
            title: 'Error!',
            text: data,
            type: 'error',
            styling: 'bootstrap3'
          });
        } else if(data == 'student number already exists!'){
          new PNotify({
            title: 'Error!',
            text: data,
            type: 'error',
            styling: 'bootstrap3'
          });
        }else{
          new PNotify({
            title: 'Success!',
            text: 'Student saved!',
            type: 'success',
            styling: 'bootstrap3'
          });
          $("#studentTable").children("h2").each(function(){
            $(this).remove();
          });
          $('#studentFormModal input[type="text"]').val('');
          $('#studentForm input[type="file"]').val('');
          $("#profileImage").attr("src", "/images/defaultimage.png");
          $("#studentFormModal").modal("hide");
          $('#studentTable').append(data);
        }
        
      });
      
    }
    
    
  });
  $('#removeImage').on('click', function(){
    $('#studentImage').val('');
    $('#profileImage').attr('src', '/images/defaultimage.png');
  }); 
  $('#viewByStudent').on('change', function(){
    tableLoader('7', 'studentTable'); 
    if($(this).val()=='all'){
      getAjax('/student/lists', 'studentTable');
    }else if ($(this).val()=='college') {
      getAjax('/student/lists/viewby/college', 'studentTable');
    }else if ($(this).val()=='seniorhigh') {
      getAjax('/student/lists/viewby/seniorhigh', 'studentTable');
    }else if ($(this).val()=='juniorhigh') {
      getAjax('/student/lists/viewby/juniorhigh', 'studentTable');
    }else if ($(this).val()=='elementary') {
      getAjax('/student/lists/viewby/elementary', 'studentTable');
    }
  });
  $('#addSchoolAttendedButton').on('click', function(){
    $('#schoolsAttendedModal').modal('show');
  }); 
  $('#addSchoolBackgroundButton').on('click', function(){
    if($('#schoolBackgroundHonor').val() == '' && $('#schoolBackgroundYearLevel').val() == ''
       && $('#schoolBackgroundYearAttended').val() == '' && $('#schoolBackgroundName').val() == ''){
        return;
    }
    if($('#schoolBackgroundHonor').val() != '' || $('#schoolBackgroundYearLevel').val() != ''
       || $('#schoolBackgroundYearAttended').val() != '' || $('#schoolBackgroundName').val() != ''){
      if($('#schoolBackgroundHonor').val() == '' || $('#schoolBackgroundYearLevel').val() == ''
      || $('#schoolBackgroundYearAttended').val() == '' || $('#schoolBackgroundName').val() == ''){
        new PNotify({
          title: 'Error!',
          text: 'All fields are required when adding a school background!',
          type: 'error',
          styling: 'bootstrap3'
        });
        return;
      }
    }
    
    $html = "<tr>"+
              "<td>"+$('#schoolBackgroundName').val()+"</td>"+
              "<td>"+$('#schoolBackgroundYearAttended').val()+"</td>"+
              "<td>"+$('#schoolBackgroundYearLevel').val()+"</td>"+
              "<td>"+$('#schoolBackgroundHonor').val()+"</td>"+
              "<td><button type ='button' class='btn btn-warning deleteSchoolBackground'><i class='fa fa-trash-o'></i></button></td>"+
            "</tr>";
    $('#studentSchoolBackgroundTable').append($html);

  });
  $('#studentSchoolBackgroundTable').on('click', '.deleteSchoolBackground', function(){
    $(this).closest('tr').remove();
  });
  $('#addSubjectLikesButton').on('click', function(){
    $('#favoriteSubjectsModal').modal('show');
  });
  $('#addFavSubjectButton').on('click', function(){
    if($('#favSubjectName').val() == '' && $('#favSubjectGrade').val() == ''){
        return;
    }
    if($('#favSubjectName').val() != '' || $('#favSubjectGrade').val() != ''){
      if($('#favSubjectName').val() == '' || $('#favSubjectGrade').val() == ''){
        new PNotify({
          title: 'Error!',
          text: 'All fields are required when adding a favorite subject!',
          type: 'error',
          styling: 'bootstrap3'
        });
        return;
      }
    }
    
    $html = "<tr>"+
              "<td>"+$('#favSubjectName').val()+"</td>"+
              "<td>"+$('#favSubjectGrade').val()+"</td>"+
              "<td><button type ='button' class='btn btn-warning removeFavSubject'><i class='fa fa-trash-o'></i></button></td>"+
            "</tr>";
    $('#favSubjectsTable').append($html);
  });
  $('#favSubjectsTable').on('click', '.removeFavSubject', function(){
    $(this).closest('tr').remove();
  });
  $('#addSubjectDislikesLikesButton').on('click', function(){
    $('#dislikeSubjectsModal').modal('show');
  });
  $('#addDislikeSubjectButton').on('click', function(){
    if($('#dislikeSubjectName').val() == '' && $('#dislikeSubjectGrade').val() == ''){
      return;
    }
    if($('#dislikeSubjectName').val() != '' || $('#dislikeSubjectGrade').val() != ''){
      if($('#dislikeSubjectName').val() == '' || $('#dislikeSubjectGrade').val() == ''){
        new PNotify({
          title: 'Error!',
          text: 'All fields are required when adding a disliked subject!',
          type: 'error',
          styling: 'bootstrap3'
        });
        return;
        }
      }
    
    $html = "<tr>"+
              "<td>"+$('#dislikeSubjectName').val()+"</td>"+
              "<td>"+$('#dislikeSubjectGrade').val()+"</td>"+
              "<td><button type ='button' class='btn btn-warning removeDislikeSubject'><i class='fa fa-trash-o'></i></button></td>"+
            "</tr>";
    $('#subjectsDislikesTable').append($html);

  });
  $('#subjectsDislikesTable').on('click', '.removeDislikeSubject', function(){
    $(this).closest('tr').remove();
  });
  $("#upgradeStudentTab").on('click', function(){
    
    tableLoader('7', 'studentUpgradesTable');
    $.get('/students/activschoolyearsemester', function(data){
      $('#schoolYearFilter').val(data.split(".")[0]);
      $.get('/report/schoolyear/'+$('#schoolYearFilter').val(), function(mydata){
        $('#semesterFilter').html("<option value='all'>all</option>");
        $('#semesterFilter').append(mydata);
        $('#semesterFilter').val(data.split(".")[1]);
        getAjax('/student/lists/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/'+$("#gradeLevelFilter").val()+'/true'+'/'+$('#searchStudents').val(), 'studentUpgradesTable');
        //tableLoader('7', 'studentTable');
        //getAjax('/student/lists/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/'+$("#gradeLevelFilter").val()+'/false'+'/'+$('#searchStudents').val(), 'studentTable');
      });
    });
  });
  $("#checkAllStudents").on('click', function(){
    if($("#checkAllStudents").prop("checked")){
      
      $("#studentUpgradesTable .upgradeStudentCheckbox").prop("checked", true);
    }else{
      $("#studentUpgradesTable .upgradeStudentCheckbox").prop("checked", false);
    }
  });
  $("#upgradeStudentsButton").on('click', function(){
    var a = [];
    $(".upgradeStudentCheckbox:checked").each(function() {
        a.push(this.value);
    });
    if(a.length==0){
      new PNotify({
        title: 'Error!',
        text: 'Please select a student to upgrade!',
        type: 'error',
        styling: 'bootstrap3'
      });
      return;
    }
    $("#statusMessageModal").modal("show");
    $("#messageStatus").text("Please wait while the system upgrade the students.");
    divLoader('messageLoader');
    new Promise((resolve, reject)=>{
      var x = 0;
      var length = a.length;
      for (let index = 0; index < a.length; index++) {
        $.get('/student/upgrade/'+a[index], function(data){
          var obj = JSON.parse(data);
          if(obj.status =='failed'){
            new PNotify({
              title: 'Error!',
              text: 'Error upgrading student!',
              type: 'error',
              styling: 'bootstrap3'
            });
          }else{
            if(obj.status == 'failedWithRemarks'){
              new PNotify({
                title: 'Error!',
                text: obj.remarks,
                type: 'error',
                styling: 'bootstrap3'
              });
            }else{
              new PNotify({
                title: 'Success!',
                text: obj.remarks,
                type: 'success',
                styling: 'bootstrap3'
              });
            }
          }
          x++;
          if(x==length){
            resolve(true);
          }
        });
      }
    }).then((resolve)=>{
      tableLoader('7', 'studentUpgradesTable');
      getAjax('/student/lists/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/'+$("#gradeLevelFilter").val()+'/true'+'/'+$('#searchStudents').val(), 'studentUpgradesTable');
      tableLoader('7', 'studentTable');

      getAjax('/student/lists/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/'+$("#gradeLevelFilter").val()+'/false'+'/'+$('#searchStudents').val(), 'studentTable');
      $("#messageStatus").text("Done processing students!");
      $("#messageLoader").html("");
    });
  });
   
  $('#searchStudents').on('keydown', function(){
    tableLoader('7', 'studentTable');
    tableLoader('7', 'studentUpgradesTable');
    getAjax('/student/lists/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/'+$("#gradeLevelFilter").val()+'/false'+'/'+$('#searchStudents').val(), 'studentTable');
    getAjax('/student/lists/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/'+$("#gradeLevelFilter").val()+'/true'+'/'+$('#searchStudents').val(), 'studentUpgradesTable');
  });
  $('#studentTypeFilter').on('change', function(){
    if($('#studentTypeFilter').val() == 'all'){
        $('#courseGradeLevelFilter').val('all');
        $('#courseGradeLevelFilter').attr('disabled', true);
        $("#gradeLevelFilter").val('all');
        $("#gradeLevelFilter").attr('disabled', true);
        tableLoader('7', 'studentTable');
        tableLoader('7', 'studentUpgradesTable');
        getAjax('/student/lists/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/'+$("#gradeLevelFilter").val()+'/false'+'/'+$('#searchStudents').val(), 'studentTable');
        getAjax('/student/lists/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/'+$("#gradeLevelFilter").val()+'/true'+'/'+$('#searchStudents').val(), 'studentUpgradesTable');
      }else{
        $('#courseGradeLevelFilter').attr('disabled', false);
        if($('#courseGradeLevelFilter')!= "all"){
          $("#gradeLevelFilter").attr('disabled', false);
        }
        switch ($('#studentTypeFilter').val()) {
          case 'college':
          tableLoader('7', 'studentTable');
          tableLoader('7', 'studentUpgradesTable');
            $.get('/report/college/course', function(data){
                $('#courseGradeLevelFilter').html("<option value='all'>all</option>");
                $('#courseGradeLevelFilter').append(data);
               
                getAjax('/student/lists/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/'+$("#gradeLevelFilter").val()+'/false'+'/'+$('#searchStudents').val(), 'studentTable');
                getAjax('/student/lists/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/'+$("#gradeLevelFilter").val()+'/true'+'/'+$('#searchStudents').val(), 'studentUpgradesTable');
            });
          break;
          case 'seniorhigh':
            var html = "<option value ='all'>all</option>"+
                        "<option value ='12'>12</option>"+
                        "<option value ='11'>11</option>";
            $('#gradeLevelFilter').html(html);
            $.get('/student/seniorhigh/strand', function(data){
              $('#courseGradeLevelFilter').html("<option value='all'>all</option>");
              $('#courseGradeLevelFilter').append(data);
              tableLoader('7', 'studentTable');
              tableLoader('7', 'studentUpgradesTable');
              getAjax('/student/lists/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/'+$("#gradeLevelFilter").val()+'/false'+'/'+$('#searchStudents').val(), 'studentTable');
              getAjax('/student/lists/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/'+$("#gradeLevelFilter").val()+'/true'+'/'+$('#searchStudents').val(), 'studentUpgradesTable');
            });
            //$('#courseGradeLevelFilter').html(html);
           
          break;
          case 'juniorhigh':
            var html = "<option value ='all'>all</option>"+
                        "<option value ='10'>10</option>"+
                        "<option value ='9'>9</option>"+
                        "<option value ='8'>8</option>"+
                        "<option value ='7'>7</option>";
            $('#gradeLevelFilter').html(html);
            tableLoader('7', 'studentTable');
            tableLoader('7', 'studentUpgradesTable');
            getAjax('/student/lists/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/'+$("#gradeLevelFilter").val()+'/false'+'/'+$('#searchStudents').val(), 'studentTable');
            getAjax('/student/lists/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/'+$("#gradeLevelFilter").val()+'/true'+'/'+$('#searchStudents').val(), 'studentUpgradesTable');
          break;
          case 'elementary':
            var html = "<option value ='all'>all</option>"+
                    "<option value ='6'>6</option>"+
                    "<option value ='5'>5</option>"+
                    "<option value ='4'>4</option>"+
                    "<option value ='3'>3</option>"+
                    "<option value ='2'>2</option>"+
                    "<option value ='1'>1</option>";
            $('#gradeLevelFilter').html(html);
            tableLoader('7', 'studentTable');
            tableLoader('7', 'studentUpgradesTable');
            getAjax('/student/lists/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/'+$("#gradeLevelFilter").val()+'/false'+'/'+$('#searchStudents').val(), 'studentTable');
            getAjax('/student/lists/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/'+$("#gradeLevelFilter").val()+'/true'+'/'+$('#searchStudents').val(), 'studentUpgradesTable');
          break;
          default:
            break;
        }
    }  
    
  });
  $('#schoolYearFilter').on('change', function(){
    tableLoader('7', 'studentTable');
    tableLoader('7', 'studentUpgradesTable');
    if($('#schoolYearFilter').val() != "all"){
      $.get('/report/schoolyear/'+$('#schoolYearFilter').val(), function(data){
        $('#semesterFilter').html("<option value='all'>all</option>");
        $('#semesterFilter').append(data);
        
        //console.log($('#schoolYearFilter').val());
        getAjax('/student/lists/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/'+$("#gradeLevelFilter").val()+'/false'+'/'+$('#searchStudents').val(), 'studentTable');
        getAjax('/student/lists/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/'+$("#gradeLevelFilter").val()+'/true'+'/'+$('#searchStudents').val(), 'studentUpgradesTable');
      });
    }else{
      
      getAjax('/student/lists/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/'+$("#gradeLevelFilter").val()+'/false'+'/'+$('#searchStudents').val(), 'studentTable');
      getAjax('/student/lists/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/'+$("#gradeLevelFilter").val()+'/true'+'/'+$('#searchStudents').val(), 'studentUpgradesTable');
    }
  });
  $('#semesterFilter').on('change', function(){
    tableLoader('7', 'studentTable');
    tableLoader('7', 'studentUpgradesTable');
    getAjax('/student/lists/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/'+$("#gradeLevelFilter").val()+'/false'+'/'+$('#searchStudents').val(), 'studentTable');
    getAjax('/student/lists/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/'+$("#gradeLevelFilter").val()+'/true'+'/'+$('#searchStudents').val(), 'studentUpgradesTable');
  });
  $('#courseGradeLevelFilter').on('change', function(){
    tableLoader('7', 'studentTable');
    tableLoader('7', 'studentUpgradesTable');
    if($('#courseGradeLevelFilter').val() == 'all'){
      $('#gradeLevelFilter').val("all");
      $('#gradeLevelFilter').attr("disabled", true);
      getAjax('/student/lists/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/'+$("#gradeLevelFilter").val()+'/false'+'/'+$('#searchStudents').val(), 'studentTable');
      getAjax('/student/lists/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/'+$("#gradeLevelFilter").val()+'/true'+'/'+$('#searchStudents').val(), 'studentUpgradesTable');
    }else{
      $('#gradeLevelFilter').attr('disabled', false);
      switch ($('#studentTypeFilter').val()) {
        case 'college':
          $('#gradeLevelFilter').html("<option value ='all'>all</option>");
          $.get('/student/getcourseyear/'+$("#courseGradeLevelFilter").val(), function(data){
            $('#gradeLevelFilter').append(data);
            getAjax('/student/lists/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/'+$("#gradeLevelFilter").val()+'/false'+'/'+$('#searchStudents').val(), 'studentTable');
            getAjax('/student/lists/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/'+$("#gradeLevelFilter").val()+'/true'+'/'+$('#searchStudents').val(), 'studentUpgradesTable');
          });
          
        break;
        case 'seniorhigh':
          var html = "<option value ='all'>all</option>"+
                      "<option value ='12'>12</option>"+
                      "<option value ='11'>11</option>";
          $('#gradeLevelFilter').html(html);
          getAjax('/student/lists/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/'+$("#gradeLevelFilter").val()+'/false'+'/'+$('#searchStudents').val(), 'studentTable');
          getAjax('/student/lists/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/'+$("#gradeLevelFilter").val()+'/true'+'/'+$('#searchStudents').val(), 'studentUpgradesTable');
        break;
        case 'juniorhigh':
          var html = "<option value ='all'>all</option>"+
                      "<option value ='10'>10</option>"+
                      "<option value ='9'>9</option>"+
                      "<option value ='8'>8</option>"+
                      "<option value ='7'>7</option>";
          $('#gradeLevelFilter').html(html);
          getAjax('/student/lists/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/'+$("#gradeLevelFilter").val()+'/false'+'/'+$('#searchStudents').val(), 'studentTable');
          getAjax('/student/lists/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/'+$("#gradeLevelFilter").val()+'/true'+'/'+$('#searchStudents').val(), 'studentUpgradesTable');
        break;
        case 'elementary':
          var html = "<option value ='all'>all</option>"+
                  "<option value ='6'>6</option>"+
                  "<option value ='5'>5</option>"+
                  "<option value ='4'>4</option>"+
                  "<option value ='3'>3</option>"+
                  "<option value ='2'>2</option>"+
                  "<option value ='1'>1</option>";
          $('#gradeLevelFilter').html(html);
          getAjax('/student/lists/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/'+$("#gradeLevelFilter").val()+'/false'+'/'+$('#searchStudents').val(), 'studentTable');
          getAjax('/student/lists/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/'+$("#gradeLevelFilter").val()+'/true'+'/'+$('#searchStudents').val(), 'studentUpgradesTable');
        break;
        default:
          break;
      }
    }  
    // tableLoader('7', 'studentUpgradesTable');
    // getAjax('/student/lists/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/'+$("#gradeLevelFilter").val()+'/true'+'/'+$('#searchStudents').val(), 'studentTable');
  });  
  $("#gradeLevelFilter").on('change', function(){
    tableLoader('7', 'studentTable');
    tableLoader('7', 'studentUpgradesTable');
    getAjax('/student/lists/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/'+$("#gradeLevelFilter").val()+'/false'+'/'+$('#searchStudents').val(), 'studentTable');
    getAjax('/student/lists/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/'+$("#gradeLevelFilter").val()+'/true'+'/'+$('#searchStudents').val(), 'studentUpgradesTable');
  });

  $("#printStudents").on('click', function(){
    //alert("Hello");
    $.get('/studentreportprint/studentsprint/'+$('#studentTypeFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/'+$("#gradeLevelFilter").val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#searchStudents').val(), function(data){
      if(data=="true"){
        window.location.replace("/temp/report.pdf");
      }
    });
  });
});
document.getElementById('studentImage').onchange = function(e) {
  // Get the first file in the FileList object
  var imageFile = this.files[0];
  // get a local URL representation of the image blob
  var url = window.URL.createObjectURL(imageFile);
  // Now use your newly created URL!
  var someImageTag = document.getElementById('profileImage');
  someImageTag.src = url;
}

