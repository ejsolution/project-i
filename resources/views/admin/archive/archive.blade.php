@extends('admin.adminTemplate')

@section('title', 'AdminDashboard')
@section('stylesheet')
	<link href="{{asset('css/admin/archive/archive.css')}}" rel="stylesheet">
@stop
@section('content')
  <div class='container'>
    <div class='row'>
      <div class='col-md-3'>
        <label for = 'viewByStudentTypeCombo'>
          View By Student Type
        </label>
        <select id = 'viewByStudentTypeCombo' class='form-control'></select>
      </div>
      <div class='col-md-3'>
        <label for = 'Course_GradeLevelCombo'>
          Course/Grade Level
        </label>
        <select id = 'Course_GradeLevelCombo' class='form-control'></select>
      </div>
      <div class='col-md-3'>
        <label for = 'schoolYearCombo'>
          School Year
        </label>
        <select id = 'schoolYearCombo' class='form-control'></select>
      </div>
      <div class='col-md-3'>
        <label for = 'semesterCombo'>
          Semester
        </label>
        <select id = 'semester' class='form-control'></select>
      </div>
    </div>
    <div class='row tableContainer' >
      <div class='col-md-12'>
        <table class='table table-condensed'>
          <thead>
            <tr>
              <th>Name</th>
              <th>Student No.</th>
              <th>Student Type</th>
              <th>Course/ Grade Level</th>
              <th>Year Level</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
    </div>
    <div class='row'>
      <div class='col-md-11'>
      </div>
      <div class='col-md-1'>
        <button class='btn btn-danger'><i class='fa fa-print'></i>&nbsp; Print</button>
      </div>
    </div>
  </div>
@endSection
