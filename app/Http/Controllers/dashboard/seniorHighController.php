<?php

namespace App\Http\Controllers\dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\seniorHighStrand;
use App\Http\Controllers\activityController;
use App\model\SHSStudent;
use App\model\systemDate;
class seniorHighController extends Controller
{
  public function __construct(){
    $this->middleware('auth:myuser');
  }
  public function getSeniorHighStrandList(){
    //$systemdate = systemDate::all();
    $strand = seniorHighStrand::orderBy('strandName', 'asc')->get();
    //$strand = seniorHighStrand::where('created_at', '<=', $systemdate[0]->systemDate)->get();
    if(count($strand)==0){
      return "<h2>There are no strands yet</h2>";
    }
    $html = '';
    foreach ($strand as $strands) {
      $html.=$this->seniorHighRow($strands);
    }
    return $html;
  }
  public function seniorHighRow($strand){
    return "<tr id = 'strand-$strand->id'>
              <td>$strand->strandCode</td>
              <td>$strand->strandName</td>
              <td>$strand->description</td>
              <td><button class='editStrandRow btn btn-info' id='editStrandRow-$strand->id'><i class='fa fa-edit'></i></button></td>
              <td><button class='deleteStrandRow btn btn-danger' id='deleteStrandRow-$strand->id'><i class='fa fa-trash-o'></i></button></td>
            </tr>";
  }
  public function insertSeniorHighStrand(Request $request){
    $this->validate($request, array(
          'seniorHighStrandCodeText'=>'required|max:5|unique:senior_high_strands,strandCode',
          'seniorHighStrandNameText'=>'required|max:50',
          'strandDescriptionText'=>'required|max:250'
    ));
    $strand =  new seniorHighStrand;
    $strand->strandCode = $request->seniorHighStrandCodeText;
    $strand->strandName = $request->seniorHighStrandNameText;
    $strand->description = $request->strandDescriptionText;
    $strand->save();
    activityController::insertActivity('Inserted a new strand with code:'.$strand->strandCode);
    return $this->seniorHighRow($strand);
  }
  public function updateSeniorHighStrand(Request $request){
    $this->validate($request, array(
          'strandName'=>'required|max:50',
          'strandDescription'=>'required|max:250'
    ));
    $strand =  seniorHighStrand::find($request->id);
    $strand2 = seniorHighStrand::where('strandCode', $request->strandCode)->get();
    //error_log($college2);
    if(count($strand2)>0){
      if($strand->strandCode == $strand2[0]->strandCode){
        $strand->strandName = $request->strandName;
        $strand->description = $request->strandDescription;
        $strand->save();
      }
    }else{
      $this->validate($request, array(
            'strandCode'=>'required|max:5|unique:senior_high_strands,strandCode'
      ));
      $strand->strandCode = $request->strandCode;
      $strand->strandName = $request->strandName;
      $strand->description = $request->strandDescription;
      $strand->save();
    }
    activityController::insertActivity('Updated a strand with code:'.$strand->strandCode);
  }
  public function deleteStrand($strandid){
    $shsstudent  =  SHSStudent::where('seniorHighStrandID', '=', $strandid)->get();
    if(count($shsstudent)>0){
      return "cannotdelete";
    }else{
      $strand =  seniorHighStrand::find($strandid);
      activityController::insertActivity('deleted a strand with code:'.$strand->strandCode);
      $strand->forceDelete();
      return "deleted";
    }
  }
  public function searchStrand($seach = ''){
    $strand =  seniorHighStrand::where('strandCode','LIKE', "%$seach%")->
                              orWhere('strandName','LIKE', "%$seach%")->get();

    if(count($strand)==0){
      return "<h2>No strands found</h2>";
    }
    $html = '';
    foreach ($strand as $strands) {
      $html.=$this->seniorHighRow($strands);
    }
    return $html;
  }
}
