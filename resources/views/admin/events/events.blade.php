@extends('admin.adminTemplate')

@section('title', 'AdminDashboard')

@section('stylesheet') 

  <link href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.8.0/fullcalendar.min.css" rel="stylesheet">

  <link href="{{asset('css/admin/transactions.css')}}" rel="stylesheet">
  <link href="{{asset('css/admin/events/events.css')}}" rel="stylesheet">
@stop
@section('content')
  <div class='container'>
    <input type = "hidden" value = "{{$eventid}}" id = "idEventView">
    <div class='row genericDivBottom alignRight'>
      <button class='btn btn-dark' id = 'activateAddEventModal'><i class='fa fa-calendar'></i>&nbsp; Add Event</button>
    </div>
    <div>
      <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#eventsCalendar">Events</a></li>
        <li><a data-toggle="tab" href="#eventLists">Event Lists</a></li>
      </ul>
    </div>
    <div class='tab-content'>
      <div class='tab-pane fade in active' id = 'eventsCalendar'>
        @component('components.x_panel')
          @slot('title')
            Calendar
          @endslot
          @slot('content')
            <div id='calendar'></div>
          @endslot
        @endcomponent
      </div>
      <div class='tab-pane fade' id = 'eventLists'>
        <div class='row'>
          <div class='col-md-7'>
          </div>
          <div class='col-md-5'>
            <div class="form-group has-feedback">
                <input type="text" class="form-control"  placeholder="Search By Title Date and Location" id ='searchEvents'/>
                <i class="fa fa-search form-control-feedback"></i>
            </div>
          </div>
        </div>
        <div class='row'>
          <div class='col-md-12 genericDivTop tableContainer'>
            <table class='table table-striped'>
              <thead>
                <tr>
                  <th>Status</th>
                  <th>Title</th> 
                  <th>Date</th>
                  <th>Time</th>
                  <th>Location</th>
                  <th>Announce</th>
                </tr>
              </thead>
              <tbody id ='eventsListTable'> 
              </tbody>
            </table>
          </div>
          <div class='genericDivTop'>
            <center><ul id="pagination" class="pagination-sm"></ul></center>
          </div>
          <div class='genericDivTop alignRight'>
            <a class='btn btn-dark' href = '{{route('event.print')}}'>
              <i class='fa fa-print'>&nbsp; Print</i>
            </a>
          </div>
        </div>
      </div>
    </div>
    @include('Partials.Modals.events.addNewEvent')
    @include('Partials.Modals.events.sendEventModal')
    @component('components.modal')
      @slot('modalid')
        checkModal
      @endslot
      @slot('modalsize')
        modal-sm
      @endslot
      @slot('modaltitle')
        Warning
      @endslot
      @slot('modalcontent')
        <div id='checkMessage'>
        </div>
      @endslot
      @slot('modalbutton')
          <button class="btn btn-danger" id = 'confirmEvent'>Save Anyway</button>
      @endslot
    @endcomponent
    @component('components.modal')
      @slot('modalid')
        eventMessageModal
      @endslot
      @slot('modalsize')
        modal-lg
      @endslot
      @slot('modaltitle')
        Send An Announcement
      @endslot
      @slot('modalcontent')
        <div class='row'>
          <div class='col-md-2'>
              <label for ='studentTypeFilter'>Student Type</label>
              
              
                  @if(session()->get('user')['userType']=='admin')
                  <select id ='studentTypeFilter' class='form-control'>
                      <option value ='all'>all</option>
                      <option value ='college'>college</option>
                      <option value ='seniorhigh'>seniorhigh</option>
                      <option value ='juniorhigh'>juniorhigh</option>
                      <option value ='elementary'>elementary</option>
                  @else
                      @switch($user[0])
                          @case('college')
                          <select id ='studentTypeFilter' class='form-control' readonly>
                              <option value ='college'>college</option>
                          @break
                          @case('seniorhigh')
                          <select id ='studentTypeFilter' class='form-control' readonly>
                              <option value ='seniorhigh'>seniorhigh</option>
                          @break
                          @case('juniorhigh')
                          <select id ='studentTypeFilter' class='form-control' readonly>
                              <option value ='juniorhigh'>juniorhigh</option>
                          @break
                          @case('elementary')
                          <select id ='studentTypeFilter' class='form-control' readonly>
                              <option value ='elementary'>elementary</option>
                          @break
                          @default
                              Default case...
                      @endswitch
                  @endif
              </select>
              
          </div>
          <div class='col-md-2'>
              <label for ='schoolYearFilter'>School Year</label>
              <select id ='schoolYearFilter' class='form-control'>
                  <option value ='all'>all</option>
                  @foreach($schoolyears as $schoolyear)
                    <option value ='{{$schoolyear->id}}'>
                      {{explode("-", $schoolyear->schoolYearStart)[0]}} -  {{explode("-", $schoolyear->schoolYearEnd)[0]}} 
                    </option>
                  @endforeach
                  
              </select>
          </div>
          <div class='col-md-1'>
              <label for ='semesterFilter'>Semester</label>
              <select id ='semesterFilter' class='form-control'>
                  <option value ='all'>all</option>
              </select>
          </div> 
          <div class='col-md-2'>
              <label for ='courseGradeLevelFilter'>Course/Grade Level</label>
              @if(session()->get('user')['userType']=='admin')
                          <select id ='courseGradeLevelFilter' class='form-control'>
                                  <option value ='all'>all</option>
              @else
                  @switch($user[0])
                      @case('college')
                      <select id ='courseGradeLevelFilter' class='form-control'>
                          <option value ='all'>all</option>
                          
                          @foreach($collegeOfUser as $college)
                              <option value = "{{$college->id}}">{{$college->courseName}}</option>
                          @endforeach
                      @break
                      @case('seniorhigh')
                      <select id ='courseGradeLevelFilter' class='form-control' readonly>
                          <option value ="{{$user[1]}}">{{$user[1]}}</option>
                      @break
                      @case('juniorhigh')
                      <select id ='courseGradeLevelFilter' class='form-control' readonly>
                          <option value ="{{$user[1]}}">{{$user[1]}}</option>
                      @break
                      @case('elementary')
                      <select id ='courseGradeLevelFilter' class='form-control' readonly>
                          <option value ="{{$user[1]}}">{{$user[1]}}</option>
                      @break
                      @default
                          Default case...
                  @endswitch
      
              @endif
              </select>
          </div>
        </div>
        <div class='row'>
          <div class= 'col-md-12 tableModalContainer'>
            <strong>Students</strong>
            <table class='table table-striped'>
              <thead>
                <tr>
                  <th><input type ='checkbox' id ='checkAllStudents'>Check All</th>
                  <th>Student Name</th>
                  <th>Student No</th>
                  <th>Student Type</th>
                  <th>Contact No</th>
                </tr>
              </thead>
              <tbody id='tableOfStudents'>
              </tbody>
            </table>
          </div>
        </div>
        <div class='row'>
          <div class='col-md-12'>
            {!! Form::open([
              'id'=>'messageEventForm']) !!}
              <label for ="messageEvent">Message</label>
              <textarea class='form-control' id = 'messageEvent' name ='message' rows='4' required></textarea>
            {!! Form::close() !!}
          </div>
        </div>
        <div class='row'>
          <div class='col-md-12 tableModalContainer'>
            <table class='table table-striped'>
              <thead>
                <tr>
                  <th>Message</th>
                  <th>Date Sent</th>
                </tr>
              </thead>
              <tbody id = 'messageOfEvents'>
              </tbody>
            </table>
          </div>
        </div>
      @endslot
      @slot('modalbutton')
          <button class="btn btn-danger" id = 'sendEvent' form = 'messageEventForm'>Send</button>
      @endslot
    @endcomponent
    @include('Partials.Modals.statusMessage')

  </div>

@endSection

@section('scripts')
  <script src = '{{asset('moment/min/moment.min.js')}}'></script>
  <script src = 'https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.8.0/fullcalendar.min.js'></script>
  <script src = '{{asset('js/jqueryPagination.js')}}'></script>
  <script src = '{{asset('js/events/events.js')}}'></script>
@stop
