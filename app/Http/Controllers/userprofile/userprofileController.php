<?php

namespace App\Http\Controllers\userprofile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\notification\notificationController;
use App\model\useraccount;
use Auth;
use Image;
use Hash;
class userprofileController extends Controller
{
    public function __construct(){
        $this->middleware('auth:myuser');
    }
    public function getUserProfilePage(){
        $user = useraccount::find(Auth::guard('myuser')->id());
        $notif = (new notificationController)->getNotifs();
        return view('admin/userprofile/userprofile', ['user'=>$user,
                                                        'notif'=>$notif]);
    }
    public function editUserProfile(Request $request){
        $this->validate($request, array(
            'firstname'=>'required',
            'lastname'=>'required'
        ));
        $user =useraccount::find(Auth::guard('myuser')->id());
        if($request->userName != $user->userName){
            $this->validate($request, array(
                'userName'=>'required|unique:useraccounts,userName'
            )); 
        }
        $user->userName = $request->userName;
        $user->firstName = $request->firstname;
        $user->lastName = $request->lastname;
        $image ='';
        if( $request->hasFile('uploadProfileImage')){
            $image = $request->file('uploadProfileImage');
            $user->imageType = $image->getClientOriginalExtension();
            $filename = $user->id.'.'.$image->getClientOriginalExtension();
            $location = public_path("images-database/admin/". $filename);
            Image::make($image)->resize(200,200)->save($location);
        }
        $user->save();
        return "saved!";
    }
    public function changePassword(Request $request){
        $this->validate($request, array(
            'newPassword'=>'required',
            'oldPassword'=>'required',
            'confirmPassword'=>'required'
        ));
        $user =useraccount::find(Auth::guard('myuser')->id());
        if (Hash::check($request->oldPassword, $user->password)) {
            $user->password = bcrypt($request->newPassword);
            $user->save();
            return "saved!";
        }else{
            return "Incorrect password!";
        }
    }
}
