<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class pagesController extends Controller
{
    public function getRegisterPage(){
       return view('authViews/register');
    }
    public function getLoginPage(){
       return view('authViews/login');
    }
    public function getAdminHome(){
         return view('admin/adminHome');
    }
    public function getCollegeStudentsPage(){
        //return view('admin/dashboard/collegeStudents');
    }
    public function getSeniorStudentsPage(){
      //return view('admin/dashboard/seniorHighStudents');
    }
    public function getJuniorStudentsPage(){
        //return view('admin/dashboard/juniorHighStudents');
    }
    public function getElementaryStudentsPage(){
       //return view('admin/dashboard/elementary');
    }
    public function getReportsPage(){
      //return view('admin/reports/reports');
    }
    public function getArchivePage(){
      return view('admin/archive/archive');
    }
    public function getMessagePage(){
      //return view('admin/messages/messages');
    }
    public function getStudentPersonalData(){
      //return view('admin/studentPersonalData/studentPersonalData');
    }
    public function getCounseling(){
      //return view('admin/counseling/counseling');
    }
    public function getReferralPage(){
      //return view('admin/referral/referral');
    }
    public function getContractPage(){
      //return view('admin/contract/contract');
    }
    public function getEventsPage(){
      //return view('admin/events/events');
    }
    public function getUserAccountsPage(){
      //return view('admin/useraccounts/useraccounts');
    }
    public function getActivityLogsPage(){
      //return view('admin/activitylogs/activitylogs');
    }
    public function getUniversitySchoolYear(){
      //return view('admin/universityschoolyear/universityschoolyear');
    }
}
