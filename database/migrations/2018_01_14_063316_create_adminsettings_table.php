<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminsettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adminsettings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('schoolName');
            $table->string('guidanceName');
            $table->string('streetAddress');
            $table->string('barangay');
            $table->string('city');
            $table->string('schoolSealImageType');
            $table->string('guidanceSealImageType');
            $table->tinyInteger('status');
            $table->string('description', 200);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adminsettings');
    }
}
