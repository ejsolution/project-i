<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/test', 'Auth\registerUser@getRegisterPage')->name('test');
Route::get('/register', 'Auth\registerUser@getRegisterPage')->name('register');
Route::get('/login', 'Auth\myuserlogin@getLoginPage')->name('login');
Route::get('/logout', 'Auth\myuserlogin@logoutUser')->name('logout');
Route::get('/register/verify', 'Auth\registerUser@getVerifyPage')->name('verify.admin');
Route::get('/verify/{email}/{verify_token}', 'Auth\registerUser@sendEmailDone')->name('verify.admin.email');
Route::get('/register/success/{id}', 'Auth\registerUser@show')->name('show.admin');

Route::prefix('admin')->group(function(){
  Route::prefix('/home')->group(function(){
    Route::get('/', 'dashboard\dashboardController@getAdminHome')->name('admin.home');
    Route::get('/studentlist/college',
    'dashboard\dashboardController@getCollegeStudentsPage')->name('collegeStudents');
    Route::get('/studentlist/seniorhigh',
    'dashboard\dashboardController@getSeniorStudentsPage')->name('seniorHighStudents');
    Route::get('/studentlist/juniorhigh',
    'dashboard\dashboardController@getJuniorStudentsPage')->name('juniorHighStudents');
    Route::get('/studentlist/elementary',
    'dashboard\dashboardController@getElementaryStudentsPage')->name('elementary');
  });

  Route::get('reports',
  'report\reportController@getReportsPage')->name('reports');
  Route::get('archive',
  'pagesController@getArchivePage')->name('archive');
  Route::get('messages',
  'message\messageController@getMessagePage')->name('messages');
  Route::get('studentpersonaldata',
  'student\studentController@getStudentPersonalData')->name('studentpersonaldata');
  Route::get('counseling',
  'counseling\counselingController@getCounseling')->name('counceling');
  Route::get('referral',
  'referral\referralController@getReferralPage')->name('referral');
  Route::get('contract',
  'contract\contractController@getContractPage')->name('contract');
  Route::get('events',
  'events\eventController@getEventsPage')->name('events');
  Route::get('manageuseraccounts',
  'users\userController@getUserAccountsPage')->name('useraccounts');
  Route::get('activitylogs',
  'activitylogs\activitylogsController@getActivityLogsPage')->name('activitylogs');
  Route::get('universityschoolyear',
  'universityschoolyear\schoolyearController@getUniversitySchoolYear')->name('universityschoolyear');
});
 
//posts
  Route::post('/register/admin', 'Auth\registerUser@store')->name('post.admin');
  Route::post('/login/check', 'Auth\myuserlogin@validateUser')->name('login.check');

//college
  Route::post('/college/insert', 'dashboard\collegeController@collegeInsert')->name('college.insert');
  Route::post('/college/update', 'dashboard\collegeController@collegeUpdate')->name('college.update');
  Route::post('/college/department/insert', 'dashboard\collegeController@insertDepartment')->name('deparment.insert');
  Route::post('/college/department/edit', 'dashboard\collegeController@updateDepartment')->name('deparment.update');
  Route::post('/college/course/insert', 'dashboard\collegeController@courseInsert')->name('course.insert');
  Route::post('/college/course/edit', 'dashboard\collegeController@courseEdit')->name('course.update');
  Route::get('/college/list', 'dashboard\collegeController@getCollegeList')->name('college.list');
  Route::get('/college/combolist', 'dashboard\collegeController@getCollegeComboList')->name('college.combo');
  Route::get('/college/department/{collegeid}', 'dashboard\collegeController@getCollegeDepartment')->name('college.department');
  Route::get('/college/course/{courseid}', 'dashboard\collegeController@getCollegeCourse')->name('college.course');

  Route::get('/college/department/select/college/{collegeid}', 'dashboard\collegeController@getCollegeForSelect')->name('college.select');
  Route::get('/college/course/select/department/{departmentid}', 'dashboard\collegeController@getDepartmentsForCourse')->name('college.department.course');

  Route::post('/college/department/select/college/change', 'dashboard\collegeController@changeDepartmentCollege')->name('college.change.department');
  Route::post('/college/course/select/department/change', 'dashboard\collegeController@changeCourseDepartment')->name('course.change.department');
  Route::get('/college/list/search/{search?}', 'dashboard\collegeController@getCollegeListSearch')->name('college.list.search');
  Route::get('/college/list/searchselect/{search}/{collegeid}', 'dashboard\collegeController@getCollegeListSearchSelect')->name('college.list.search.select');

  Route::get('/college/delete/{collegeid}', 'dashboard\collegeController@deleteCollege')->name('college.delete');
  Route::get('/course/delete/{courseid}', 'dashboard\collegeController@deleteCourse')->name('course.delete');
  Route::get('/department/delete/{departmentid}', 'dashboard\collegeController@deleteDepartment')->name('department.delete');

  Route::get('/college/course/searchfor/department/{search}/{departmentid}', 'dashboard\collegeController@searchDepartmentForSelect')->name('college.list.search.select');

//senior high
  Route::get('/seniorhigh/list', 'dashboard\seniorHighController@getSeniorHighStrandList')->name('seniorhigh');
  Route::get('/seniorhigh/delete/{strandid}', 'dashboard\seniorHighController@deleteStrand')->name('seniorhigh.delete');
  Route::get('/seniorhigh/list/search/{strand?}', 'dashboard\seniorHighController@searchStrand')->name('seniorhigh.search');
  Route::post('/seniorhigh/strand/insert', 'dashboard\seniorHighController@insertSeniorHighStrand')->name('seniorhigh.insert');
  Route::post('/seniorhigh/edit', 'dashboard\seniorHighController@updateSeniorHighStrand')->name('seniorhigh.edit');

//useraccounts
  Route::get('/useraccounts/list', 'users\userController@getUsers')->name('users');
  Route::get('/useraccount/get/{userid}', 'users\userController@getUserDistinct')->name('user.get');
  Route::get(' /useraccounts/list/inactive', 'users\userController@getUsersInactive')->name('user.inactive');
  Route::get('/useraccount/setinactive/{userid}','users\userController@setInactiveUser')->name('user.set.inactive');
  Route::get('/useraccount/setactive/{userid}','users\userController@setActiveUser')->name('user.set.inactive');
  Route::get('/useraccount/search/{status}/{search?}', 'users\userController@searchUser')->name('user.search');
  Route::post('/useraccount/insert', 'users\userController@insertUser')->name('insert.user');

//activitylogs
  Route::get('/activitylogs/totalrows/{search?}', 'activitylogs\activitylogsController@totalRows')->name('activitylogs.totalrow');
  Route::get('/activitylogs/pages/{page}/{search?}', 'activitylogs\activitylogsController@getActivityLogs')->name('activitylogs.list');
  Route::get('/activitylogs/print', 'activitylogs\activitylogsController@print')->name('activitylogs.print');
  
//schoolyear
  Route::post('/schoolyear/insert', 'universityschoolyear\schoolyearController@schoolYearInsert')->name('schoolyear.insert');
  Route::post('/schoolyear/update', 'universityschoolyear\schoolyearController@schoolYearUpdate')->name('schoolyear.update');
  Route::get('/schoolyear/lists', 'universityschoolyear\schoolyearController@schoolYearLoad')->name('schoolyear.list');
  Route::get('/schoolyear/year/{schoolyearid}', 'universityschoolyear\schoolyearController@getYears')->name('schoolyear.list');
  Route::get('/schoolyear/setactive/{schoolyearid}', 'universityschoolyear\schoolyearController@setActive')->name('schoolyear.setActive');

//student
  Route::get('/student/getcourses/{collegeid}', 'student\studentController@getCourses')->name('student.courses');
  Route::get('/student/getcourseyear/{courseid}', 'student\studentController@getCourseYear')->name('student.courses.year');
  Route::get('/student/lists/{viewby}/{schoolyear}/{semester}/{courseGrade}/{gradelevel}/{search?}', 'student\studentController@getStudentsFaster')->name('student.lists');
  Route::get('/student/lists/viewby/{viewby}', 'student\studentController@getStudentsFiltered')->name('student.lists.filtered');
  Route::get('/student/lists/search/{viewby}/{search?}', 'student\studentController@getStudentsSearch')->name('student.lists.search');
  Route::get('/student/profile/{profileid}', 'student\studentController@getStudentProfile')->name('student.profile');
  Route::get('/student/upgradeprofile/{profileid}', 'student\studentController@upgradeStudent')->name('student.upgrade');
  Route::get('/studentreportprint/studentsprint/{studenttype}/{gradeLevelCourse}/{gradelevel}/{schoolYear}/{semester}/{search?}', 'student\studentController@printStudents2')->name('print.students.reports');
  Route::get('/studentprofile/getgradelevel/type/{profileid}', 'student\studentController@getStudentTypeToUpgrade')->name('student.upgrade.data');
  Route::get('/student/upgradeprofileseniorhigh/{profileid}/{strandid}', 'student\studentController@upgradeStudentSeniorHigh')->name('student.upgradeseniorhigh.data');
  Route::get('/student/upgradeprofilecollege/{profileid}/{courseid}', 'student\studentController@upgradeStudentCollege')->name('student.upgradecollege.data');
  Route::get('/students/activschoolyearsemester', 'student\studentController@getActiveSchoolYearSemester')->name('active.schoolyear.semester');
  Route::get('/student/seniorhigh/strand', 'student\studentController@getStrands')->name('senior.high.strands');
  Route::get('/student/upgrade/{studentid}', 'student\studentController@upgradeStudentGroup')->name('upgrad.student.group');
  Route::get('/student/profile/number/of/record/{studentid}', 'student\studentController@numberStudentRecord')->name('number.student.record');
  Route::get('/student/record/delete/{studentid}', 'student\studentController@deleteStudentRecord')->name('delete.student.record'); 
  //studentfamily 
    Route::get('/student/family/list/{studentid}', 'student\studentController@getFamilyOfStudent')->name('student.family.list');
    Route::get('/student/profilearray/{profileid}', 'student\studentController@getStudentProfileArray')->name('student.profile.array');
    Route::get('/student/delete/{familyid}', 'student\studentController@deleteFamily')->name('student.family.delete');
    Route::get('/student/getfamilyinfo/{familyid}', 'student\studentController@getFamilyData')->name('student.family.data');
    Route::post('/student/insert', 'student\studentController@studentInsert')->name('student.insert');
    Route::post('/student/update', 'student\studentController@studentUpdate')->name('student.update');
    Route::post('/student/familyinfo/submit', 'student\studentController@studentFamilyInfoInsert')->name('student.faminfo.insert');
    Route::post('/student/family/add', 'student\studentController@familyInsertUpdate')->name('student.family.insert.update');
  //studentsaveall 
    Route::post('/student/saveall', 'student\studentController@saveAllStudent')->name('student.saveall');
  //studenteducationalbackground
    Route::get('/student/educationalbackground/list/{studentid}', 'student\studentController@getEducationalBackgroundList')->name('student.educationalbackground.list');
    Route::get('/educationalbackground/delete/{backgroundid}', 'student\studentController@educationalBackgroundDelete')->name('student.educationalbackground.delete');
    Route::post('/student/educationalbackground/insert', 'student\studentController@educationalBackgroundInsert')->name('student.educationalbackground.insert');
    Route::post('/student/schoolbackground/update ', 'student\studentController@educationalBackgroundUpdate')->name('student.educationalbackground.update');
    //favsubject
      Route::get('/student/favsubject/list/{studentid}', 'student\studentController@getFavSubject')->name('student.favsubject.list');
      Route::get('/subject/list/combo', 'student\studentController@getSubjectsCombo')->name('student.subject.combo');
      Route::get('/favoritesubject/delete/{subjid}', 'student\studentController@deleteFavSubject')->name('student.subject.delete');
      Route::post('/student/favsubject/insert', 'student\studentController@favSubjectInsert')->name('subject.favsubject.insert');
      Route::post('/student/favsubject/update', 'student\studentController@favSubjectUpdate')->name('subject.favsubject.update');
    //disliked subject
      Route::get('/student/disliked/list/{studentid}', 'student\studentController@getDislikedSubjects')->name('student.disliked.list');
      Route::post('/student/disliked/insert', 'student\studentController@dislikedSubjectInsert')->name('subject.dislikedfavsubject.insert');
      Route::post('/student/disliked/update', 'student\studentController@dislikedSubjectUpdate')->name('subject.dislikedfavsubject.update');
      Route::get('/dislikedsubject/delete/{subjid}', 'student\studentController@deleteDislikedSubject')->name('subject.dislikedfavsubject.delete');
    //studentleisure
      Route::post('/student/leisure/insert', 'student\studentController@insertStudentLeisure')->name('student.leisure.insert');
      Route::post('/student/leisure/update', 'student\studentController@updateStudentLeisure')->name('student.leisure.update');
      Route::get('/student/leisure/list/{studentid}', 'student\studentController@studentLeisureList')->name('student.leisure.insert');
      Route::get('/student/leisureinfo/{leisurid}/{studentid}', 'student\studentController@studentLeisureData')->name('student.leisure.data');
      Route::get('/studentleisure/delete/{leisureid}/{studentid}', 'student\studentController@studentLeisureDelete')->name('student.leisure.delete');
    //studenthealthrecord
      Route::get('/student/healthrecord/list/{studentid}', 'student\studentController@studentHealthRecordList')->name('student.healthrecord.list');
      Route::get('/healthrecord/delete/{healthrecordid}', 'student\studentController@deleteHealthRecord')->name('student.healthrecord.delete');
      Route::post('/student/healthrecord/insert', 'student\studentController@insertHealthRecord')->name('student.healthrecord.insert');
      Route::post('/student/healthrecord/update', 'student\studentController@updateHealthRecord')->name('student.healthrecord.update');
    //makeup assistance
      Route::get('/makeupassistance/list/{studentid}', 'student\studentController@getMakeUpAssistance')->name('makeupassistance.list');
      Route::get('/makeupassistance/data/{makeupid}', 'student\studentController@getMakeUpData')->name('makeupassistance.data');
      Route::get('/makeupassistance/delete/{makeupid}', 'student\studentController@deleteMakeUp')->name('makeupassistance.delete');
      Route::post('/makeupassistance/insert', 'student\studentController@insertMakeUpAssistance')->name('makeupassistance.insert');
      Route::post('/makeupassistance/update', 'student\studentController@updateMakeUpAssistance')->name('makeupassistance.update');

      
      
      


//subject
  Route::get('/subject/list', 'dashboard\subjectController@getSubjects')->name('subject.lists');
  Route::get('/subject/delete/{subjectid}', 'dashboard\subjectController@deleteSubject')->name('subject.delete');
  Route::post('/subject/edit', 'dashboard\subjectController@subjectUpdate')->name('subject.update');
  Route::post('/subject/insert', 'dashboard\subjectController@subjectInsert')->name('subject.insert');
//personality
  Route::get('/personality/list', 'personality\personalityController@getPersonality')->name('personality.lists');
  Route::get('/personality/delete/{personalityid}', 'personality\personalityController@deletePersonality')->name('personality.delete');
  Route::get('/personality/checkbox/list', 'personality\personalityController@getPersonalityCheckbox')->name('personality.lists.checkbox');
  Route::post('/personality/insert', 'personality\personalityController@personalityInsert')->name('personality.insert');
  Route::post('/personality/edit', 'personality\personalityController@personalityUpdate')->name('personality.edit');
//counseling
  //Route::get('/counseling/totalrows/{viewby}/{schoolyear}/{semester}/{courseGradeLevel}/{search?}', 'counseling\counselingController@getCounselingTotalRow')->name('counseling.totalrow');
  //Route::get('/counseling/pages/{page}/{viewby}/{schoolyear}/{semester}/{courseGradeLevel}/{search?}', 'counseling\counselingController@getCounselingPage')->name('counseling.page');
  Route::get('/counseling/pages/{page}/{viewby}/{schoolyear}/{semester}/{courseGradeLevel}/{totalrow}/{search?}', 'counseling\counselingController@getCounselingFaster')->name('counseling.page');
  Route::get('/counseling/data/{counselingid}', 'counseling\counselingController@getCounselingData')->name('counseling.data');
  Route::get('/counseling/studentdata/{studentid}', 'counseling\counselingController@getStudentData')->name('counseling.student.data');
  Route::get('/counseling/checkiffolloweduporevaluated/{counselingid}', 'counseling\counselingController@checkIfFollowedUpOrEvaluated')->name('counseling.check.data');
  Route::get('/counseling/followupdata/{counselingid}', 'counseling\counselingController@getFollowUpData')->name('counseling.followup.data');
  Route::get('/counseling/followupdetails/{followupid}', 'counseling\counselingController@getFollowUpDetails')->name('counseling.followup.details');
  Route::get('/counseling/evaluation/data/{counselingid}', 'counseling\counselingController@getEvaluationData')->name('counseling.evaluation.data');
  Route::get('/counseling/group/data/{counselingid}', 'counseling\counselingController@getCounselingGroupData')->name('counseling.group.data');
  Route::get('/counseling/group/students/forinsert/{viewby}/{schoolyear}/{semester}/{courseGrade}/{search?}', 'counseling\counselingController@getStudentsForGroupCounseling')->name('counseling.group.students.data');
  Route::get('/reportcounseling/print/{studenttype}/{gradeLevelCourse}/{schoolYear}/{semester}/{search?}', 'counseling\counselingController@printCounselingReport')->name('reportcounselings.print');
  Route::post('/counseling/student/insert', 'counseling\counselingController@counselingInsert')->name('counseling.insert');
  Route::post('/counseling/student/group/insert', 'counseling\counselingController@counselingGroupInsert')->name('counseling.group.insert');
  Route::post('/counseling/student/update', 'counseling\counselingController@counselingIndividualUpdate')->name('counseling.individual.update');
  Route::post('/counseling/followup/insert', 'counseling\counselingController@followupInsert')->name('counseling.folloup.insert');
  Route::post('/counseling/followup/update', 'counseling\counselingController@followupUpdate')->name('counseling.folloup.update');
  Route::post('/counseling/evaluation/insert', 'counseling\counselingController@evaluationForm')->name('counseling.evaluationform');
  Route::post('/counseling/student/group/update', 'counseling\counselingController@groupCounselingUpdate')->name('counseling.group.update');
  Route::post('/counseling/sendmessage', 'counseling\counselingController@sendMessageCounseling')->name('counseling.message');
  Route::post('/counseling/messagegroup', 'counseling\counselingController@groupMessageCounseling')->name('counseling.groupmessage');

  
//referral 
  Route::post('/referral/insert', 'referral\referralController@insertReferral')->name('referral.insert');
  Route::post('/referral/update', 'referral\referralController@updateReferral')->name('referral.update');
  Route::post('/referral/message', 'referral\referralController@messageReferral')->name('referral.message');
  Route::get('/reportreferral/print/{studenttype}/{gradeLevelCourse}/{schoolYear}/{semester}/{search?}', 'referral\referralController@printReferralReport')->name('report.referral.print');
  Route::get('/referral/list/{page}/{viewby}/{schoolyear}/{semester}/{courseGrade}/{row}/{search?}', 'referral\referralController@getReferralFaster')->name('referral.totalrow');
  Route::get('/referral/getcoordinators', 'referral\referralController@getCoordinators')->name('referral.coordinators');
  Route::get('/referral/familyofstudent/{studentid}', 'referral\referralController@getFamilyOfStudent')->name('referral.message');
  Route::get('/referral/messagelogs/{referralid}', 'referral\referralController@getMessageLogs')->name('referral.messagelogs');
  Route::get('/referral/data/{referralid}', 'referral\referralController@getReferralData')->name('referral.data');
  Route::get('/referral/defaultcoordinator/', 'referral\referralController@getDefaultCoordinator')->name('referral.coordinator');
//contract 
  Route::get('/contract/pages/{page}/{schoolyear}/{semester}/{course}/{row}/{search?}', 'contract\contractController@getContractFaster')->name('contract.list');
  Route::get('/contract/data/{contractid}', 'contract\contractController@getContractData')->name('contract.data');
  Route::get('/contract/delete/proof/{proofid}', 'contract\contractController@deleteProof')->name('contract.delete.proof');
  Route::get('/contracts/collegestudent/{studentid}', 'contract\contractController@getCollegeStudentData')->name('contract.student.data');
  Route::get('/contract/studentsbrowse/{studenttype}/{gradeLevelCourse}/{schoolYear}/{semester}/{search?}', 'contract\contractController@browseStudentsFaster')->name('contract.browse.students');
  Route::get('/contract/remarks/consecutive', 'contract\contractController@getConsecutiveRemarks')->name('contract.consecutive');
  Route::get('/contractreport/print/{studenttype}/{gradeLevelCourse}/{schoolYear}/{semester}/{search?}', 'contract\contractController@contractReportPrint')->name('reportcontracts.print');
  Route::post('/contract/insert', 'contract\contractController@insertContract')->name('contract.insert');
  Route::post('/contract/update', 'contract\contractController@updateContract')->name('contract.update');
  Route::post('/contract/remarks/insert', 'contract\contractController@contractRemarksInsert')->name('contract.remarks.insert');
  
//event
  Route::get('/events/list/{month}', 'events\eventController@getEventList')->name('event.list');
  Route::get('/events/data/{eventid}', 'events\eventController@getEventData')->name('event.data');
  Route::get('/events/delete/{eventid}', 'events\eventController@deleteEvent')->name('event.delete');
  Route::get('/events/listtable/totalrow/{search?}', 'events\eventController@getTotalRow')->name('event.list.totalrow');
  Route::get('/events/listtable/{page}/{search?}', 'events\eventController@getEventsListTable')->name('event.list.table');
  Route::get('/events/print', 'events\eventController@printEventsList')->name('event.print');
  Route::get('/event/group/students/message/{viewby}/{schoolyear}/{semester}/{courseGrade}/{search?}', 'events\eventController@getStudentsForMessage')->name('events.students');
  Route::get('/event/messages/{eventid}', 'events\eventController@getMessages')->name('event.message.lists');
  Route::post('/event/insert', 'events\eventController@insertEvent')->name('event.insert');
  Route::post('/event/checkconflick', 'events\eventController@eventCheck')->name('event.check');
  Route::post('/event/update', 'events\eventController@updateEvent')->name('event.update');
  Route::post('/event/checkconflict/update', 'events\eventController@checkConflictUpdate')->name('event.check.update');
  Route::post('/event/sendmessage', 'events\eventController@sendMessage')->name('event.send.message');
  

//messages
  Route::get('/messages/schoolyear/type/{schoolyearid}', 'message\messageController@getSchoolYearType')->name('message.schoolyear.type');
  Route::get('/messages/list/{viewby}/{search?}/{semester?}', 'message\messageController@messageList')->name('message.list');
  Route::get('/messages/list2/{viewby}/{semester?}', 'message\messageController@messageListWithNoSearch')->name('message.list2');
  Route::get('/messages/studentsgroup/grouplist/{studenttype}', 'message\messageController@getGroupList')->name('message.list.group');

  Route::get('/messages/students/recipient/{selectby}/{search?}', 'message\messageController@studentRecipientList')->name('message.students.recipent');
  Route::get('/messages/students/guardian/recipient/{viewby}/{search?}', 'message\messageController@getStudentFamilyList')->name('message.guardian.recipent');
  Route::post('/message/sendindividual', 'message\messageController@sendMessageIndividual')->name('message.send.individual');
  Route::post('/message/sendgroup', 'message\messageController@sendMessageGroup')->name('message.send.group');
  
//report 
  Route::get('/report/students/{studenttype}/{gradeLevelCourse}/{gradelevel}/{schoolYear}/{semester}', 'report\reportController@getStudentsFaster')->name('report.students');
  Route::get('/report/studentsprint/{studenttype}/{gradeLevelCourse}/{gradelevel}/{schoolYear}/{semester}', 'report\reportController@reportStudents')->name('report.students');
  Route::get('/report/college/course', 'report\reportController@getCoursesCollege')->name('report.college.courses');
  Route::get('/report/schoolyear/{schoolyearid}', 'report\reportController@getSemesterSchoolYear')->name('report.schoolyear');
  Route::get('/report/studentdata/{studentid}', 'report\reportController@getStudentUpdateData')->name('report.student.data');
  Route::get('/report/updatestudentdata/{studentid}', 'report\reportController@getStudentUpdates')->name('report.student.updates');
  Route::get('/report/studentsprintupdates/{studentid}', 'report\reportController@printStudentUpdates')->name('report.student.updates');
  Route::get('/report/referralstudents/{studentid}', 'report\reportController@getStudentReferral')->name('report.student.referral');
  Route::get('/report/behaviourreferralstudent/{referralid}', 'report\reportController@getBehaviourReferral')->name('report.student.behavioral');
  Route::get('/report/behaviourprint/{studentid}', 'report\reportController@printBehaviourReferral')->name('report.referral.print');
  Route::get('/report/contractstudent/{studentid}', 'report\reportController@getContractStudent')->name('report.contract');
  Route::get('/report/contractbehaviourandproof/{contractid}', 'report\reportController@getContractBehaviorProof')->name('report.contract.behaviour');
  Route::get('/report/contractprintstudent/{studentid}', 'report\reportController@printContractStudent')->name('report.contract.print.student');
  Route::get('/report/counselingstudent/{studentid}', 'report\reportController@getCounselingOfStudent')->name('report.counseling.student');
  Route::get('/report/counselingfollowup/{counselingid}', 'report\reportController@getFollowUpCounseling')->name('report.counseling.followup');
  Route::get('/report/counselingprintstudent/{studentid}', 'report\reportController@printCounselingOfStudent')->name('report.counseling.print');
  Route::get('/report/referral/list/{studenttype}/{gradeLevelCourse}/{gradelevel}/{schoolYear}/{semester}/{behaviour}', 'report\reportController@getReferralFaster')->name('report.referral.list');
  Route::get('/report/referral/print/{studenttype}/{gradeLevelCourse}/{gradelevel}/{schoolYear}/{semester}/{behaviour}', 'report\reportController@printReferralReport')->name('report.referral.print');
  Route::get('/report/contract/list/{studenttype}/{gradeLevelCourse}/{gradelevel}/{schoolYear}/{semester}/{contracttype}', 'report\reportController@getContractFaster')->name('report.contract.list');
  Route::get('/report/contract/print/{studenttype}/{gradeLevelCourse}/{gradelevel}/{schoolYear}/{semester}/{contracttype}', 'report\reportController@printContractReport')->name('report.contracts.print');
  Route::get('/report/counseling/list/{studenttype}/{gradeLevelCourse}/{gradelevel}/{schoolYear}/{semester}/{counselingstatus}/{counselor}', 'report\reportController@getCounselingFaster')->name('report.counseling.list');
  Route::get('/report/counseling/print/{studenttype}/{gradeLevelCourse}/{gradelevel}/{schoolYear}/{semester}/{counselingstatus}/{counselor}', 'report\reportController@printCounselingReport')->name('report.counselings.print');
  Route::get('/report/referralbehaviour/combo/', 'report\reportController@getBehaviourSettingReferral')->name('referral.behaviour');
  Route::get('/report/counselingcounselor/combo/', 'report\reportController@getCounselorCombo')->name('counseling.counselor.combo');
//userprofile
  Route::get('/userprofile/get', 'userprofile\userprofileController@getUserProfilePage')->name('userprofile');
  Route::post('/userprofile/edit', 'userprofile\userprofileController@editUserProfile')->name('userprofile.edit');
  Route::post('/userprofile/changepassword', 'userprofile\userprofileController@changePassword')->name('userprofile.changepassword');
  
//systemdate 
  Route::post('/systemdate/update', 'systemDateController@updateSystemDate')->name('systemDate.update');

//dashboard students more info lists
  Route::get('/dashboard/collegestudents/{schoolyear}/{semester}/{course}/{search?}', 'dashboard\dashboardController@getCollegeStudentsList')->name('dashboard.collegestudent.list');  
  Route::get('/dashboard/seniorhighstudents/{schoolyear}/{semester}/{course}/{search?}', 'dashboard\dashboardController@getSeniorHighStudentsList')->name('dashboard.seniorhigh.list');  
  Route::get('/dashboard/juniorhighstudents/{schoolyear}/{semester}/{course}/{search?}', 'dashboard\dashboardController@getJuniorHighList')->name('dashboard.juniorhigh.list');  
  Route::get('/dashboard/elementarystudents/{schoolyear}/{semester}/{course}/{search?}', 'dashboard\dashboardController@getElementaryList')->name('dashboard.elementary.list');  
  Route::get('/contract/top5/', 'dashboard\dashboardController@getStatisticsContract')->name('dashboard.contract.stat');  
  Route::get('/counselingActive', 'dashboard\dashboardController@getStatisticsCounseling')->name('dashboard.counseling.stat');  
  Route::get('/referralStatistics', 'dashboard\dashboardController@getStatisticsReferral')->name('dashboard.referral.stat');  
  Route::get('/upcomingevents', 'dashboard\dashboardController@getUpcomingEvents')->name('dashboard.events.upcoming');  
  Route::get('/eventview/{eventid}', 'dashboard\dashboardController@viewEvent')->name('event.view');  
//notif
  Route::get('/notification/resched/{counselingid}', 'notification\notificationController@redirectReschedNotification')->name('notification.resched.counseling');  
  Route::get('/notification/accept/{counselingid}/{notifid}', 'notification\notificationController@redirectAcceptNotification')->name('notification.accept.counseling');
  Route::get('/notification/view/{counselingid}', 'notification\notificationController@viewCounseling')->name('notification.view.counseling');
  Route::get('/notification/contract/{contractid}', 'notification\notificationController@viewContract')->name('notification.view.contract');
//setting
  Route::post('/settings/insert', 'settings\settingsController@insertSettings')->name('settings.insert');
  Route::get('/adminsettings/get', 'settings\settingsController@getSetting')->name('event.message.lists');
//behaviour
  Route::get('/behaviour/combolist', 'behaviour\behaviourController@getBehaviourComboList')->name('behaviour.combolists');
  Route::get('/behaviour/list', 'behaviour\behaviourController@getBehaviourList')->name('behaviour.lists');
  Route::get('/behaviour/delete/{behaviourid}', 'behaviour\behaviourController@deleteBehaviour')->name('behaviour.delete');
  Route::post('/behaviour/insert', 'behaviour\behaviourController@insertBehaviour')->name('behaviour.insert');
  Route::post('/behaviour/edit', 'behaviour\behaviourController@editBehaviour')->name('behaviour.edit');
//counselor
  Route::get('/counselor/list', 'counselor\counselorController@getCounselors')->name('counselor.lists');
  Route::get('/counselor/delete/{counselorID}', 'counselor\counselorController@deleteCounselor')->name('counselor.delete');
  Route::get('/counselor/counseling', 'counselor\counselorController@getCounselorsForCounseling')->name('counselor.get.counseling');
  Route::get('/counselor/restore/{counselorID}', 'counselor\counselorController@restoreCounselor')->name('counselor.restore');
  
  Route::post('/counselor/insert', 'counselor\counselorController@insertCounselor')->name('counselor.insert');
  Route::post('/counselor/edit', 'counselor\counselorController@editCounselor')->name('counselor.update');
  
  
  

  
  
 
  
  
  

 
