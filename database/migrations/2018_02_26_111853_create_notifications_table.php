<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('studentID')->unsigned()->nullable();
            $table->string('studentName')->nullable();
            $table->string('studentType')->nullable();
            $table->string('userTypeFrom')->nullable();
            $table->string('userNameFrom')->nullable();
            $table->integer('userIDFrom')->unsigned()->nullable();
            $table->string('designationFrom')->nullable();
            $table->string('designationLevelFrom')->nullable();
            $table->string('userTypeTo')->nullable();
            $table->string('userNameTo')->nullable();
            $table->integer('userIDTo')->unsigned()->nullable();
            $table->string('designationTo')->nullable();
            $table->string('designationLevelTo')->nullable();
            $table->tinyInteger('viewed')->nullable();
            $table->mediumText('message')->nullable();
            $table->string('type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
