@extends('admin.adminTemplate')

@section('title', 'AdminDashboard')
@section('stylesheet')
  <link href="{{asset('css/admin/transactions.css')}}" rel="stylesheet">
  <link href="{{asset('css/admin/studentPersonalData/studentPersonalData.css')}}" rel="stylesheet">
@stop
@section('content')
  <div class='container'>
    @component('components.headerSearchFilter2', ['user'=>$user,
                                                'collegeOfUser'=>$collegeOfUser,
                                                'strand'=>$strand])
      @slot('comboLabel') 
        View By Student Type
      @endslot
      @slot('schoolyears')
        @foreach($schoolyears as $year)
          <option value ='{{$year->id}}'>{{explode('-', $year->schoolYearStart)[0]}} - {{explode('-', $year->schoolYearEnd)[0]}}</option>
        @endforeach
      @endslot
      @slot('searchID')
        searchStudents
      @endslot
      @slot('placeholder')
        Search By Student
      @endslot
      @slot('button')
        <button class='btn btn-block btn-dark' id = 'addStudentButton'>Add</button>
      @endslot
    @endcomponent
    <div class='row'>
      <div class='col-md-12'>
        <ul class="nav nav-tabs">
          <li class="active"><a data-toggle="tab" href="#studentLists">Student Lists</a></li>
          <li id = 'upgradeStudentTab'><a data-toggle="tab" href="#upgradeStudents">Upgrade Students</a></li>
        </ul>
      </div>
    </div>
    <div class="tab-content">
      <div id = 'studentLists' class="tab-pane fade in active">
        <div class='col-md-12 well tableContainer'>
          <table class='table table-striped'>
            <thead>
              <tr>
                <th>No</th>
                <th>Name</th>
                <th>Student No.</th>
                <th>Student Type</th>
                <th>Course/Strand</th>
                <th>Grade Level/Year Level</th>
                <th>View Profile</th>
              </tr>
            </thead>
            <tbody id ='studentTable'>
            </tbody>
          </table>
  
        </div>
        <div class='alignRight'>
          <button class='btn btn-dark' id = 'printStudents'>
            <i class='fa fa-print'>&nbsp; Print</i>
          </button>
        </div>
      </div>
      <div id = 'upgradeStudents' class="tab-pane fade">
        <table class='table table-striped'>
          <thead>
            <tr>
              <th><input type = 'checkbox' id = 'checkAllStudents'>Check All</th>
              <th>No</th>
              <th>Name</th>
              <th>Student No.</th>
              <th>Student Type</th>
              <th>Course/Strand</th>
              <th>Grade Level/Year Level</th> 
            </tr>
          </thead>
          <tbody id ='studentUpgradesTable'>
          </tbody>
        </table>
        <div class='genericDivTop alignRight'>
            <button class='btn btn-dark' id = 'upgradeStudentsButton'>
            Upgrade
            </button>
        </div>
      </div>
    </div>
    @include('Partials.Modals.studentPersonalData.studentPersonalDataModal2')
    @component('components.modal')
      @slot('modalid')
        schoolsAttendedModal
      @endslot
      @slot('modalsize')
        modal-md
      @endslot
      @slot('modaltitle')
        Add A School Background
      @endslot
      @slot('modalcontent')
        <div class='row'>
          <div class='col-md-6'>
            <label for ='schoolBackgroundName'>School Background Name</label>
            <input type='text' id ='schoolBackgroundName' name='schoolBackgroundName' class='form-control'>
          </div>
          <div class='col-md-6'>
            <label for ='schoolBackgroundYearAttended'>Year Graduated/transfered</label>
            <input type='number' id ='schoolBackgroundYearAttended' name='schoolBackgroundYearAttended' class='form-control'>
          </div>
        </div>
        <div class='row'>
          <div class='col-md-6'>
            <label for ='schoolBackgroundYearLevel'>Year Level</label>
            <input type='number' id ='schoolBackgroundYearLevel' name='schoolBackgroundYearLevel' class='form-control'>
          </div>
          <div class='col-md-6'>
            <label for ='schoolBackgroundHonor'>Honor/Awards</label>
            <input type='text' id ='schoolBackgroundHonor' name='schoolBackgroundHonor' class='form-control'>
          </div>
        </div>
      @endslot
      @slot('modalbutton')
          <button class="btn btn-danger" data-dismiss="modal" id = 'addSchoolBackgroundButton'><i class='fa fa-plus'></i>&nbsp;Add</button>
      @endslot
    @endcomponent
    @component('components.modal')
      @slot('modalid')
        favoriteSubjectsModal
      @endslot
      @slot('modalsize')
        modal-md
      @endslot
      @slot('modaltitle')
        Add A Favorite Subject
      @endslot
      @slot('modalcontent')
        <div class='row'>
          <div class='col-md-6'>
            <label for ='favSubjectName'>Favorite Subject Name</label>
            <input type='text' id ='favSubjectName' name='favSubjectName' class='form-control'>
          </div>
          <div class='col-md-6'>
            <label for ='favSubjectGrade'>Grade</label>
            <input type='number' id ='favSubjectGrade' name='favSubjectGrade' class='form-control'>
          </div>
        </div>
      @endslot
      @slot('modalbutton')
          <button class="btn btn-danger" data-dismiss="modal" id = 'addFavSubjectButton'><i class='fa fa-plus'></i>&nbsp;Add</button>
      @endslot
    @endcomponent

    @component('components.modal')
      @slot('modalid')
        dislikeSubjectsModal
      @endslot
      @slot('modalsize')
        modal-md
      @endslot
      @slot('modaltitle')
        Add a subject the student dislikes
      @endslot
      @slot('modalcontent')
        <div class='row'>
          <div class='col-md-6'>
            <label for ='dislikeSubjectName'>Disliked Favorite Subject Name</label>
            <input type='text' id ='dislikeSubjectName' name='dislikeSubjectName' class='form-control'>
          </div>
          <div class='col-md-6'>
            <label for ='dislikeSubjectGrade'>Grade</label>
            <input type='number' id ='dislikeSubjectGrade' name='dislikeSubjectGrade' class='form-control'>
          </div>
        </div>
      @endslot
      @slot('modalbutton')
          <button class="btn btn-danger" data-dismiss="modal" id = 'addDislikeSubjectButton'><i class='fa fa-plus'></i>&nbsp;Add</button>
      @endslot
    @endcomponent
    @include('Partials.Modals.statusMessage')
  </div>
@endSection
@section('scripts')
  <script src = '{{asset('js/parsley.min.js')}}'></script>
  <script src = '{{asset('js/studentPersonalData/studentPersonalData.js')}}'></script>
@stop
