@extends('admin.adminTemplate')

@section('title', 'AdminDashboard')
@section('stylesheet')
	<link href="{{asset('css/admin/messages/messages.css')}}" rel="stylesheet">
@stop
@section('content')
  <div class='container'>
    <div class='row'>
      <div class='col-md-6'>
        <button class='btn btn-block btn-info' id = 'newMessageButton'>New Message</button>
      </div>
      <div class='col-md-6'>
        <button class='btn btn-block btn-info' id = 'newGroupMessageButton'>Group Message</button>
      </div>
    </div>
    <div class='row'>
      <div class='col-md-12'>
        <div>
          <ul class="nav nav-tabs">
            <!--li class="active"><a data-toggle="tab" href="#inbox">Inbox</a></li-->
            <li><a data-toggle="tab" href="#sent">Sent</a></li>
            <!--li><a data-toggle="tab" href="#events">Events</a></li-->
            <!--li><a data-toggle="tab" href="#referral">Referral</a></li-->
          </ul>
        </div>
        <div class="tab-content">
          <div class='tab-pane fade in active' id = 'sent'>
            <div class='container-fluid'>
              <div class='row'>
                <div class='col-md-7'>
                  <div class='row'>
                    <div class='col-md-10'>
                      <div class='row'>
                        <div class='col-md-6'>
                          <label for = 'sentSchoolYearCombo'>School Year</label>
                          <select id = 'sentSchoolYearCombo' class='form-control'>
                              <option value = 'all'>All</option>
                            @foreach($schoolYear as $schoolyears)
                              <option value = '{{$schoolyears->id}}'>{{explode('-', $schoolyears->schoolYearStart)[0]}} - {{explode('-', $schoolyears->schoolYearEnd)[0]}}</option>
                            @endforeach
                          </select>
                        </div>
                        <div class='col-md-6'>
                          <label for = 'sentSemesterCombo'>Semester</label>
                          <select id = 'sentSemesterCombo' class='form-control'></select>
                        </div>
                      </div>
                      <div class='row genericDivTop'>
                        <div class='col-md-12'>
                          <div class="form-group has-feedback" style = 'padding-top:0px;'>
                              <input type="text" class="form-control" id = 'searchSentText' placeholder="Search By Date, time or recipient"/>
                              <i class="fa fa-search form-control-feedback"></i>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class='col-md-2 genericDivPadTop'>
                      <button class='btn-default btn btn-block' style = 'padding-top:100%; position:relative;'>
                        <i class='fa fa-print' style = 'position: absolute;
                        top: 40%;
                        left: 0;
                        bottom: 0;
                        right: 0;'></i>
                      </button>
                    </div>
                  </div>
                  <div class='row'>
                    <div class='col-md-12 tableContainer'>
                      <table class='table table-hover'>
                        <thead>
                          <tr>
                            <th>Date & Time</th>
                            <th>Receiver</th>
                            <th>Message</th>
                          </tr>
                        </thead>
                        <tbody id ='sentMessagesTable'>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div class='col-md-5 well'>
                  <div class='row'>
                    <div class='col-md-12'>
                      <div class='row'>
                        <strong>From: Sender</strong>
                      </div>
                      <div class='alignRight'>
                        Date and Time Sent
                        <button class='btn btn-default' id ='buttonForwardMessage'>
                          <i class='fa fa-share'></i>
                        </button>
                      </div>
                      <div>
                        <label>Message</label>
                        <textarea class='form-control' rows = '8' id = 'messageOfSenderInbox' maxlength= '100' disabled>
                        </textarea>

                      </div>
                      <div>
                        <div>
                          from: <strong></strong>
                        </div>
                        <div>
                          <strong>Position:</strong>
                        </div>
                        <div>
                          <strong>College:</strong>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!--div class='tab-pane fade' id = 'events'>
            <h1>event Tab</h1>
          </div>
          <div class='tab-pane fade' id = 'referral'>
            <h1>referral Tab</h1>
          </div-->
        </div>
      </div> 
    </div>

    @include('Partials.Modals.messages.newMessageModal')
    @include('Partials.Modals.messages.contactNoModal')
    @include('Partials.Modals.messages.groupMessage')
		  @include('Partials.Modals.messages.selectManyRecipients')
  </div>
@endSection
@section('scripts')
  <script src= '{{asset('js/messages/messages.js')}}'></script>
@stop
