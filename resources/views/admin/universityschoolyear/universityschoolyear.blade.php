@extends('admin.adminTemplate')

@section('title', 'AdminDashboard')
@section('stylesheet')
  <link href="{{asset('css/admin/settings.css')}}" rel="stylesheet">
  <link href="{{asset('css/admin/universityschoolyear/universityschoolyear.css')}}" rel="stylesheet">
@stop
@section('content')
  <div class='container'>
    @component('components.x_panel')
      @slot('title')
          School Year Information
      @endslot
      @slot('content')
      {!! Form::open([
        'id'=>'schoolYearForm']) !!}
        <div class='row'>
          <div class='col-md-12'>
            <label>School Year</label>
            <div class='row well'>
              <div class='col-md-6'>
                <label for = 'fromDateSchoolYear'>From</label>
                <input type = 'date' id = 'fromDateSchoolYear' name = 'fromDateSchoolYear' class='form-control' required>
              </div>
              <div class='col-md-6'>
                <label for = 'toDateSchoolYear'>to</label>
                <input type = 'date' id = 'toDateSchoolYear' name = 'toDateSchoolYear' class='form-control' required>
              </div>
            </div>

          </div>
          <div class='col-md-6'>
            <label>1st Semester</label>
            <div class='row well'>
              <div class='col-md-6'>
                <label for = 'fromFirstSemester'>From</label>
                <input type = 'date' id = 'fromFirstSemester' name = 'fromFirstSemester' class='form-control' required>
              </div>
              <div class='col-md-6'>
                <label for = 'toFirstSemester'>To</label>
                <input type = 'date' id = 'toFirstSemester' name = 'toFirstSemester' class='form-control' required>
              </div>
            </div>

          </div>
          <div class='col-md-6'>
            <label>2nd Semester</label>
            <div class='row well'>
              <div class='col-md-6'>
                <label for = 'fromSecondSemester'>From</label>
                <input type = 'date' id = 'fromSecondSemester' name = 'fromSecondSemester' class='form-control' required>
              </div>
              <div class='col-md-6'>
                <label for = 'toSecondSemester'>To</label>
                <input type = 'date' id = 'toSecondSemester' name = 'toSecondSemester' class='form-control' required>
              </div>
            </div>

          </div>
        </div>
        <div class='row'>
            <div class='col-md-6' disabled>
                <label>3rd Semester</label>
                <div class='row well'>
                  <div class='col-md-6'>
                    <label for = 'fromThirdSemester'>From</label>
                    <input type = 'date' id = 'fromThirdSemester' name = 'fromThirdSemester' class='form-control' disabled>
                  </div>
                  <div class='col-md-6'>
                    <label for = 'toThirdSemester'>To</label>
                    <input type = 'date' id = 'toThirdSemester' name = 'toThirdSemester' class='form-control' disabled>
                  </div>
                </div>
            </div>
            <div class='col-md-6'>
                <label>Semester Type</label>
                <div class='row well'>
                  <select id ='semesterType' name = 'semesterType' class='form-control'>
                    <option value = '2'>2 sem</option>
                    <option value = '3'>3 sem</option>
                  </select>
                </div>
            </div>
        </div>
        <div class='row'>
          <div class='col-md-12 alignRight'>
            <button class='btn btn-dark' id = 'saveSchoolYearButton'><i class='fa fa-save'></i>Save</button>
          </div>
        </div>
        
        {!! Form::close() !!}
      @endslot
    @endcomponent
    <div class='row'>
      <div class='col-md-12 tableContainer'>
        <table class='table table-striped'>
          <thead>
            <tr>
              <th>School Year</th>
              <th>1st Semester</th>
              <th>2nd Semester</th>
              <th>3rd Semester</th>
              <th>Status</th>
              <th>set Active</th>
              <th>Edit</th>
            </tr>
          </thead>
          <tbody id = 'schoolyeartable'>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  @component('components.modal')
    @slot('modalid')
      editSchoolYearModal
    @endslot
    @slot('modalsize')
      modal-md
    @endslot
    @slot('modaltitle')
      Edit School Year
    @endslot
    @slot('modalcontent')
    {!! Form::open([
      'id'=>'editSchoolYearForm']) !!}
      <div class='row'>
        <div class='col-md-12'>
          <label>School Year</label>
          <div class='row well'>
            <div class='col-md-6'>
              <label for = 'fromDateSchoolYearEdit'>From</label>
              <input type = 'date' id = 'fromDateSchoolYearEdit' name = 'fromDateSchoolYearEdit' class='form-control' required>
            </div>
            <div class='col-md-6'>
              <label for = 'toDateSchoolYearEdit'>to</label>
              <input type = 'date' id = 'toDateSchoolYearEdit' name = 'toDateSchoolYearEdit' class='form-control' required>
            </div>
          </div>

        </div>
        <div class='col-md-12'>
          <label>1st Semester</label>
          <div class='row well'>
            <div class='col-md-6'>
              <label for = 'fromFirstSemesterEdit'>From</label>
              <input type = 'date' id = 'fromFirstSemesterEdit' name = 'fromFirstSemesterEdit' class='form-control' required>
            </div>
            <div class='col-md-6'>
              <label for = 'toFirstSemester'>To</label>
              <input type = 'date' id = 'toFirstSemesterEdit' name = 'toFirstSemesterEdit' class='form-control' required>
            </div>
          </div>

        </div>
        <div class='col-md-12'>
          <label>2nd Semester</label>
          <div class='row well'>
            <div class='col-md-6'>
              <label for = 'fromSecondSemesterEdit'>From</label>
              <input type = 'date' id = 'fromSecondSemesterEdit' name = 'fromSecondSemesterEdit' class='form-control' required>
            </div>
            <div class='col-md-6'>
              <label for = 'toSecondSemester'>To</label>
              <input type = 'date' id = 'toSecondSemesterEdit' name = 'toSecondSemesterEdit' class='form-control' required>
            </div>
          </div>

        </div>
      </div>
      <div class='row'>
          <div class='col-md-12' disabled>
              <label>3rd Semester</label>
              <div class='row well'>
                <div class='col-md-6'>
                  <label for = 'fromThirdSemesterEdit'>From</label>
                  <input type = 'date' id = 'fromThirdSemesterEdit' name = 'fromThirdSemesterEdit' class='form-control' disabled>
                </div>
                <div class='col-md-6'>
                  <label for = 'toThirdSemester'>To</label>
                  <input type = 'date' id = 'toThirdSemesterEdit' name = 'toThirdSemesterEdit' class='form-control' disabled>
                </div>
              </div>
          </div>
          <div class='col-md-12'>
              <label>Semester Type</label>
              <div class='row well'>
                <select id ='semesterTypeEdit' name = 'semesterTypeEdit' class='form-control'>
                  <option value = '2'>2 sem</option>
                  <option value = '3'>3 sem</option>
                </select>
              </div>
          </div>
      </div>
      
      {!! Form::close() !!}
    @endslot
    @slot('modalbutton')
        <button  class="btn btn-danger"  id ='editSchoolYearButton' form = 'editSchoolYearForm'><i class='fa fa-save'></i>&nbsp;Save</button>
    @endslot
  @endcomponent

@endSection
@section('scripts')

  <script src = '{{asset('js/universityschoolyear/universityschoolyear.js')}}'></script>
@stop
