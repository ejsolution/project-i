@component('components.modal')
  @slot('modalid')
    addContractModal
  @endslot
  @slot('modalsize')
    modal-lg
  @endslot
  @slot('modaltitle')
    Add Contract
  @endslot
  @slot('modalcontent')
    <div class='row'>
      <div class='col-md-6'>
        <label>Student</label> 
        <div class="input-group">
          <input type="text" id = 'nameOfStudents' name='nameOfStudents' disabled class="form-control">
          <input type='hidden' name = 'studentID' id ='studentHiddenID' form ='contractAddForm'>
          <span class="input-group-btn">
            <button type="button" class="btn btn-primary" id ='chooseStudent'><i class='fa fa-plus'></i></button>
          </span>
        </div>
        <label for = 'courseContract'>Course</label>
        <input type = 'text' id = 'courseContract' name ='courseName' class='form-control' disabled>
        <label for = 'courseYearContract'>Course/Year</label>
        <input type = 'text' id = 'courseYearContract' name ='courseYearContract' class='form-control' disabled>

        @component('components.x_panel')
          @slot('title')
              Contract Information 
          @endslot
          @slot('content')
            <label for ='contractDate'>Contract Date</label>
      <input type= 'date' class='form-control' id = 'contractDate' name = 'contractDate' readonly value ="{{$systemDate}}" form='contractAddForm'>
            <label for ='schoolYearContract'>School Year</label>
            <input type= 'text' class='form-control' id = 'schoolYearContract' name = 'schoolYearContract'
            disabled value ='{{$schoolyear}}'>
            <label for ='semesterContract'>Semester</label>
            <input type= 'text' class='form-control' id = 'semesterContract' name = 'semesterContract'
            disabled value ='{{$currentsem}}'>
            <label for ='typeOfContract'>Type Of Contract</label>
            {{--  <input type ='text' class='form-control' id = 'typeOfContract' name = 'typeOfContract' required maxlength ='100' form='contractAddForm'>  --}}
            <select class='form-control' id = 'typeOfContract' name = 'typeOfContract' required form='contractAddForm'>
              <option value ='failing grades'>Failing Grades</option>
              <option value ='absences'>Absences</option>
              <option value ='poor study habits'>Poor Study Habits</option>
              <option value ='illness or health problems'>Illness or Health Problems</option>
              <option value ='evidence of emotional disturbance'>Evidence of Emotional Disturbance</option>
              <option value ='Deviant Behavior'>Deviant Behavior</option>
              <option value ='career/course choice problem'>Career/Course Choice Problem</option>
              <option value ='scholarship/students asst. problem'>Scholarship/Students Asst. Problem</option>
              <option value ='social relationship'>Social Relationship</option>
              <option value ='others'>Others</option>
            </select>
            <input type = 'text' class='form-control' placeholder="others" id = 'othersTextAdd' name = 'others' form='contractAddForm' readonly>
            <div class='well genericDivTop'>
                {!! Form::open([
                  'id'=>'addGradeForm']) !!}
                  <div class='row'>
                    <div class='col-md-6'>
                      <label for='gradeContract'>Grade</label>
                      <input type ='number' class='form-control' id = 'gradeContract' name = 'gradeContract' required>
                    </div>
                    <div class='col-md-6'>
                      <label for='unitContract'>Units</label>
                      <input type ='number' class='form-control' id = 'unitContract' name = 'unitContract' required>
                    </div>
                  </div>
                  <div class='row'>
                    <div class='col-md-12'>
                      <label for ='subjectCodeText'>Subject Code</label>
                      <input type ='text' required id ='subjectCodeText' class='form-control'>
                    </div>
                  </div>
                
                  <div class='row genericDivTop genericDivBottom'>
                    <button class='btn btn-sm btn-block btn-dark'>Add</button>
                  </div>
              {!! Form::close() !!}
              <div class='row tableModalSmallContainer'>
                <table class='table table-striped'>
                  <thead>
                    <tr>
                      <th>Subject Code</th>
                      <th>Grade</th>
                      <th>Units</th>
                      <th>Remove</th>
                    </tr>
                  </thead>
                  <tbody id ='gradesTable'>
                  </tbody>
                </table>
              </div>
            <div class='genericDivTop'>
              {{--  <label>Total Grades</label>
              <input type ='text' disabled id ='totalGrades' class='form-control'>  --}}
            </div>
            </div> 
          @endslot
        @endcomponent
      </div>
      {!! Form::open([
        'id'=>'contractAddForm',
        'files'=>true]) !!}
      <div class='col-md-6'>
        <label for = 'uploadPhotosContract'>Select photos to be uploaded</label>
        <input type = 'file' id = 'uploadPhotosContract' name ='uploadPhotosContract[]'
        class='form-control' multiple required>
        <!--label for = 'photosQuantityContract'>Number of photos uploaded</label>
        <input type = 'number' id = 'photosQuantityContract' name ='photosQuantityContract' disabled
        class='form-control'-->

        <label>Photos Uploaded</label>
        <div class='tableModalContainer bordered' id ='displayImagesToBeUploaded'>
        </div>
        <div class='row genericDivTop'>
          <label for = 'remarksContract'>Remarks</label>
          <textarea id = 'remarksContract' name = 'remarksContract' class='form-control' rows='4' required></textarea>
        </div>
        <div class='row'>
          <label for = 'notedByContract'>Noted By:</label>
          <input type = 'text' id = 'notedByContract' name = 'notedByContract' class='form-control' readonly required value = "{{$nameofuser}}">
        </div>
      </div> 
      {!! Form::close() !!}
    </div>
  @endslot
  @slot('modalbutton')
      <button class="btn btn-warning"  form='contractAddForm'><i class='fa fa-save'></i>&nbsp;Save</button>
  @endslot
@endcomponent
@component('components.modal')
  @slot('modalid')
    editContractModal
  @endslot
  @slot('modalsize')
    modal-lg
  @endslot
  @slot('modaltitle')
    Edit A Contract
  @endslot
  @slot('modalcontent')
    <div class='row'>
      <div class='col-md-6'>
        <label>Student</label>
        <select class='form-control' id ='studentsComboBoxEdit' name ='studentID' form ='contractEditForm' required readonly>
          @foreach($collegeStudents as $college)
            <option value ='{{$college->id}}'>{{$college->firstName}} {{$college->lastName}}</option>
          @endforeach
        </select>
        <label for = 'courseContractEdit'>Course</label>
        <input type = 'text' id = 'courseContractEdit' name ='courseName' class='form-control' readonly>
        <label for = 'courseYearContractEdit'>Course/Year</label>
        <input type = 'text' id = 'courseYearContractEdit' name ='courseYearContract' class='form-control' readonly>

        @component('components.x_panel')
          @slot('title')
              Contract Information
          @endslot
          @slot('content')
            <label for ='contractDateEdit'>Contract Date</label>
            <input type= 'date' class='form-control' id = 'contractDateEdit' name = 'contractDate' required form='contractEditForm' readonly>
            <label for ='schoolYearContractEdit'>School Year</label>
            <input type= 'text' class='form-control' id = 'schoolYearContractEdit' name = 'schoolYearContract'
            disabled value ='{{$schoolyear}}'>
            <label for ='semesterContractEdit'>Semester</label>
            <input type= 'text' class='form-control' id = 'semesterContractEdit' name = 'semesterContract'
            disabled value ='{{$currentsem}}'>
            <label for ='typeOfContractEdit'>Type Of Contract</label>
            {{--  <input type ='text' class='form-control' id = 'typeOfContractEdit' name = 'typeOfContract' required maxlength ='100' form='contractEditForm'>  --}}
            <select class='form-control' id = 'typeOfContractEdit' name = 'typeOfContract' required  form='contractEditForm'>
              <option value ='failing grades'>Failing Grades</option>
              <option value ='absences'>Absences</option>
              <option value ='poor study habits'>Poor Study Habits</option>
              <option value ='illness or health problems'>Illness or Health Problems</option>
              <option value ='evidence of emotional disturbance'>Evidence of Emotional Disturbance</option>
              <option value ='Deviant Behavior'>Deviant Behavior</option>
              <option value ='career/course choice problem'>Career/Course Choice Problem</option>
              <option value ='scholarship/students asst. problem'>Scholarship/Students Asst. Problem</option>
              <option value ='social relationship'>Social Relationship</option>
              <option value ='others'>Others</option>
            </select>
            <input type = 'text' class='form-control' placeholder="others" id = 'othersTextEdit' name = 'others' form='contractEditForm'>
            <div class='well genericDivTop'>
                {!! Form::open([
                  'id'=>'editGradeForm']) !!}
                  <div class='row'>
                    <div class='col-md-6'>
                      <label for='gradeContractEdit'>Grade</label>
                      <input type ='number' class='form-control' id = 'gradeContractEdit' name = 'gradeContract' required>
                    </div>
                    <div class='col-md-6'>
                      <label for='unitContractEdit'>Units</label>
                      <input type ='number' class='form-control' id = 'unitContractEdit' name = 'unitContract' required>
                    </div>
                  </div>
                  <div class='row'>
                    <div class='col-md-12'>
                      <label for ='subjectCodeTextEdit'>Subject Code</label>
                      <input type ='text' required id ='subjectCodeTextEdit' class='form-control'>
                    </div>
                  </div>
                
                  <div class='row genericDivTop genericDivBottom'>
                    <button class='btn btn-sm btn-block btn-dark'>Add</button>
                  </div>
              {!! Form::close() !!}
              <div class='row tableModalSmallContainer'>
                <table class='table table-striped'>
                  <thead>
                    <tr>
                      <th>Subject Code</th>
                      <th>Grade</th>
                      <th>Units</th>
                      <th>Remove</th>
                    </tr>
                  </thead>
                  <tbody id ='gradesTableEdit'>
                  </tbody>
                </table>
              </div>
              <div class='genericDivTop'>
                  {{--  <label>Total Grades</label>  --}}
                  {{--  <input type ='text' disabled id ='totalGradesEdit' class='form-control'>  --}}
              </div>
            </div>
          @endslot
        @endcomponent
      </div>
      {!! Form::open([
        'id'=>'contractEditForm',
        'files'=>true]) !!}
      <div class='col-md-6'>
        <label for = 'uploadPhotosContractEdit'>Upload new photos to this contract</label>
        <input type = 'file' id = 'uploadPhotosContractEdit' name ='uploadPhotosContract[]'
        class='form-control' multiple>
        <!--label for = 'photosQuantityContract'>Number of photos uploaded</label>
        <input type = 'number' id = 'photosQuantityContract' name ='photosQuantityContract' disabled
        class='form-control'-->

        <label>Photos To be Uploaded</label>
        <div class='tableModalContainer bordered' id ='displayImagesToBeUploadedEdit'>
        
        </div>
        <label>Photos Currently Uploaded</label>
        <div class='tableModalContainer bordered' id ='photosCurrentlyUploaded'>
        
        </div>
        <div class='row genericDivTop'>
          <label for = 'remarksContractEdit'>Remarks</label>
          <textarea id = 'remarksContractEdit' name = 'remarksContract' class='form-control' rows='4' required></textarea>
        </div>
        <div class='row'>
          <label for = 'notedByContractEdit'>Noted By:</label>
          <input type = 'text' id = 'notedByContractEdit' name = 'notedByContract' class='form-control' required readonly>
        </div>
      </div>
      {!! Form::close() !!}
    </div>
  @endslot
  @slot('modalbutton')
      <button class="btn btn-warning"  form='contractEditForm'><i class='fa fa-save'></i>&nbsp;Save</button>
  @endslot
@endcomponent

@component('components.modal')
  @slot('modalid')
    remarksContractModal
  @endslot
  @slot('modalsize')
    modal-lg
  @endslot
  @slot('modaltitle')
    Remarks for contract
  @endslot
  @slot('modalcontent')
    <div>
    {!! Form::open([
      'id'=>'contractRemarksForm']) !!}
        <div class='row'>
          <div class='col-md-12'>
            <h4 id = 'studentNameRemarks'></h4>
          </div>
        </div>
        <div class='row'>
          <div class='col-md-12'>
            <label for= 'remarksStudentContract'>Remarks</label>
            <textarea id = 'remarksStudentContract' name = 'remarksStudentContract' class='form-control' required rows='3'></textarea>
          </div>
        </div>
    {!! Form::close() !!}
    </div>
  @endslot
  @slot('modalbutton')
      <button class="btn btn-warning"  form='contractRemarksForm'><i class='fa fa-save'></i>&nbsp;Save</button>
  @endslot
@endcomponent

