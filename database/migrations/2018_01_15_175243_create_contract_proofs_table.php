<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractProofsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contract_proofs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('contractID')->unsigned();
            $table->foreign('contractID')->references('id')->on('contracts');
            $table->string('proofPhotoType');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contract_proofs');
    }
}
