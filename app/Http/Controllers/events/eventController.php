<?php

namespace App\Http\Controllers\events;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\event;
use App\model\systemDate;
use App\model\schoolYear; 
use App\model\eventMessage;
use App\model\student;
use App\model\adminsetting;
use Auth; 
use App\Http\Controllers\activityController;
use App\Http\Controllers\notification\notificationController;
use App\Http\Controllers\users\userController;
use DB;
use PDF;
class eventController extends Controller
{
    public function __construct(){
        $this->middleware('auth:myuser');
    }
    public function getEventsPage(){
        $schoolyears = schoolYear::all();
        $notif = (new notificationController)->getNotifs();

        $user =  (new userController)->getUserDesignation(Auth::guard('myuser')->id());
        $college ='';
        if($user[0]=='college'){
            $college = DB::select(DB::raw("SELECT colleges.collegeName, courses.courseName, 
            courses.id FROM colleges INNER JOIN (SELECT collegeID, userID, id FROM 
            assignment_colleges INNER JOIN (SELECT max(id) AS maxid FROM assignment_colleges 
            GROUP BY userID) ac ON ac.maxid = assignment_colleges.id) ac2 ON ac2.collegeID = 
            colleges.id INNER JOIN (SELECT collegeID, departmentID, id FROM 
            department_college_hists INNER JOIN (SELECT max(id) as maxid FROM 
            department_college_hists GROUP BY departmentID) dch ON dch.maxid = 
            department_college_hists.id) dch2 ON dch2.collegeID = colleges.id 
            INNER JOIN (SELECT courseID, departmentID, id FROM course_department_hists 
            INNER JOIN (SELECT max(id) as maxid FROM course_department_hists GROUP BY courseID) 
            cdh ON cdh.maxid = course_department_hists.id) cdh2 ON cdh2.departmentID = 
            dch2.departmentID INNER JOIN courses on courses.id  = cdh2.courseID WHERE 
            ac2.collegeID = (SELECT collegeID FROM assignment_colleges WHERE id = 
            (SELECT max(id) FROM assignment_colleges WHERE userID ='".Auth::guard('myuser')->id()."'))
            GROUP BY courses.id"));
        }
        
        return view('admin/events/events', ['notif'=>$notif,
                                            'user'=>$user,
                                            'collegeOfUser'=>$college,
                                            'schoolyears'=>$schoolyears,
                                            'eventid' => '']);
    } 
    public function insertEvent(Request $request){
        $this->validate($request, array(
            'titleEventText'=>'required|max:100',
            'dateEventStart'=>'required',
            'dateEventEnd'=>'required',
            'timeEventStart'=>'required',
            'timeEventEnd'=>'required',
            'locationEventText'=>'required|max:100',
            'descriptionEventTextArea' => 'required|max:500'
        ));
      $event = new event;
      $event->eventsTitle = $request->titleEventText;
      $event->dateStart = $request->dateEventStart;
      $event->dateEnd = $request->dateEventEnd;
      $event->timeStart = $request->timeEventStart;
      $event->timeEnd = $request->timeEventEnd;
      $event->location = $request->locationEventText;
      $event->details = $request->descriptionEventTextArea;
      $event->save();
      $event->id = "event-$event->id";
      $event->title = $event->eventsTitle;
      $event->allDay = "true";
      $event->start = $event->dateStart;
      $event->end = $event->dateEnd;
      activityController::insertActivity('Inserted an event with title:'.$event->title);
      return $event;
    }
    public function eventCheck(Request $request){
        $systemdate = systemDate::all();
        $this->validate($request, array(
            'titleEventText'=>'required|max:100',
            'dateEventStart'=>'required',
            'dateEventEnd'=>'required',
            'timeEventStart'=>'required',
            'timeEventEnd'=>'required',
            'locationEventText'=>'required|max:100',
            'descriptionEventTextArea' => 'required|max:500'
        ));
        $events = DB::select(DB::raw("SELECT id, eventsTitle AS title, 'true' AS 
        allDay, dateStart AS start, dateEnd AS end FROM events WHERE ( 
        (dateStart <= '$request->dateEventStart' AND dateEnd >='$request->dateEventStart')
        OR (dateStart <= '$request->dateEventEnd' AND dateEnd >='$request->dateEventEnd')
        OR (dateStart <= '$request->dateEventStart' AND dateEnd >='$request->dateEventEnd')
        OR (dateStart >= '$request->dateEventStart' AND dateEnd <='$request->dateEventEnd'))
        AND location = '$request->locationEventText'
        AND DATE(created_at) <= '$systemdate[0]->systemDate'"));
        if(count($events)>0){
            return "false";
        }else{
            return "true";
        }
    }
    public function checkConflictUpdate(Request $request){
        $systemdate = systemDate::all();
        $this->validate($request, array(
            'titleEventText'=>'required|max:100',
            'dateEventStart'=>'required',
            'dateEventEnd'=>'required',
            'timeEventStart'=>'required',
            'timeEventEnd'=>'required',
            'locationEventText'=>'required|max:100',
            'descriptionEventTextArea' => 'required|max:500'
        ));
        $events = DB::select(DB::raw("SELECT id, eventsTitle AS title, 'true' AS 
        allDay, dateStart AS start, dateEnd AS end FROM events WHERE ( 
        (dateStart <= '$request->dateEventStart' AND dateEnd >='$request->dateEventStart')
        OR (dateStart <= '$request->dateEventEnd' AND dateEnd >='$request->dateEventEnd')
        OR (dateStart <= '$request->dateEventStart' AND dateEnd >='$request->dateEventEnd')
        OR (dateStart >= '$request->dateEventStart' AND dateEnd <='$request->dateEventEnd'))
        AND location = '$request->locationEventText' AND id <> '$request->id'
        AND DATE(created_at) <= '$systemdate[0]->systemDate'"));
        if(count($events)>0){
            return "false";
        }else{
            return "true";
        }
    }
    public function getEventList($month){
        $systemdate = systemDate::all();
        //$events = event('')
        $events = DB::select(DB::raw("SELECT id, eventsTitle AS title, 'true' AS 
        allDay, dateStart AS start, dateEnd AS end FROM events WHERE 
        (MONTH(dateStart) = '$month' OR MONTH(dateEnd) ='$month')
        AND DATE(created_at) <= '".$systemdate[0]->systemDate."'"));
        return $events;
    }
    public function getEventData($eventid){
        return event::find($eventid);
    }
    public function deleteEvent($eventid){
        $event =event::find($eventid);
        activityController::insertActivity('Updated an event with title:'.$event->eventsTitle);
        $event->forceDelete();
        return "deleted";
    }
    public function updateEvent(Request $request){
        $this->validate($request, array(
            'titleEventText'=>'required|max:100',
            'dateEventStart'=>'required',
            'dateEventEnd'=>'required',
            'timeEventStart'=>'required',
            'timeEventEnd'=>'required',
            'locationEventText'=>'required|max:100',
            'descriptionEventTextArea' => 'required|max:500'
        ));
        $event = event::find($request->id);
        $event->eventsTitle = $request->titleEventText;
        $event->dateStart = $request->dateEventStart;
        $event->dateEnd = $request->dateEventEnd;
        $event->timeStart = $request->timeEventStart;
        $event->timeEnd = $request->timeEventEnd;
        $event->location = $request->locationEventText;
        $event->details = $request->descriptionEventTextArea;
        $event->save();
        /*var event = {
            id:'myevent',
            title:'ThisEvent',
            allDay:true,
            start:started,
            end:ended
        };*/
        //$events= new \stdClass();
        $event->id = "event-$event->id";
        $event->title = $event->eventsTitle;
        $event->allDay = "true";
        $event->start = $event->dateStart;
        $event->end = $event->dateEnd;
        activityController::insertActivity('Updated an event with title:'.$event->title);
        return $event;
    }
    public function getTotalRow($search = ''){
        $systemdate = systemDate::all();
        $event =event::where(function($query) use($search){
                            $query->where('eventsTitle', 'LIKE', "%$search%");
                            $query->orWhere('location', 'LIKE', "%$search%");
                            $query->orWhere('dateStart', 'LIKE', "$search%");
                            $query->orWhere('dateEnd', 'LIKE', "$search%");
                        })
                        ->whereDate('created_at', '<=', $systemdate[0]->systemDate)
                        ->count();
        return $event; 
    }
    public function getEventsListTable($page, $search = ''){
        $systemdate = systemDate::all();
        $skip = ($page-1)*10;
        $event =event::where(function($query) use($search){
                            $query->where('eventsTitle', 'LIKE', "%$search%");
                            $query->orWhere('location', 'LIKE', "%$search%");
                            $query->orWhere('dateStart', 'LIKE', "$search%");
                            $query->orWhere('dateEnd', 'LIKE', "$search%");
                        })
                        ->whereDate('created_at', '<=', $systemdate[0]->systemDate)
                        ->orderBy('dateStart', 'desc')
                        ->limit(10)->offset($skip)
                        ->get();
        $html ='';
        if(count($event)==0){
            return "<h2>No events yet</h2>";
        }else{
            $status;
            foreach ($event as $events) {
                if(strtotime($events->dateStart)>strtotime($systemdate[0]->systemDate)){
                    $status = "expired";
                }else{
                    if((strtotime($events->dateStart)<strtotime($systemdate[0]->systemDate) && 
                    strtotime($events->dateEnd)>strtotime($systemdate[0]->systemDate))||
                    (strtotime($events->dateStart)==strtotime($systemdate[0]->systemDate) || 
                    strtotime($events->dateEnd)==strtotime($systemdate[0]->systemDate))){
                        $status = "ongoing";
                    }elseif (strtotime($events->dateEnd)<strtotime($systemdate[0]->systemDate)) {
                        $status = 'pending';
                    }
                }
                $disabled ='';
                if($status == "expired"){
                    $disabled = "disabled";
                }
                $html .="<tr>
                            <td>".ucfirst($status)."</td>
                            <td>$events->eventsTitle</td>
                            <td>$events->dateStart - $events->dateEnd</td>
                            <td>$events->timeStart - $events->timeEnd</td>
                            <td>$events->location</td>
                            <td>
                                <button class='sendMessage btn btn-warning' id ='eventMessage-$events->id' $disabled>
                                    <i class='fa fa-envelope'></i>
                                </button>
                            </td>
                        </tr>";
            }
        }
        return $html;
    }
    public function printEventsList(){
        $html = "
            <html>
                <head>
                    <style>
                        table {
                            font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif;
                            border-collapse: collapse;
                            width: 100%;
                        }
                        
                        table td, table th {
                            border: 1px solid #ddd;
                            padding: 8px;
                        }
                        
                        table tr:nth-child(even){background-color: #f2f2f2;}
                        
                        table tr:hover {background-color: #ddd;}
                        
                        table th {
                            padding-top: 12px;
                            padding-bottom: 12px;
                            text-align: left;
                            background-color: #4CAF50;
                            color: white;
                        }
                        @page{
                            margin-left:20px;
                            margin-right:20px;
                            margin-top:160px;
                        }
                    </style>
                </head>
                <body>
                    ".$this->getAdminSetting()."
                    <div style = 'margin-bottom:20px; margin-top:20px;'>
                        <center><h3>Events</h3></center>
                    </div>
                    <table>
                        <thead>
                            <tr>
                                <th>Status</th>
                                <th>Title</th>
                                <th>Date</th>
                                <th>Time</th>
                                <th>Location</th>
                            </tr>
                        </thead>
                        <tbody> 
        ";
        $event = event::all();
        $status;
        foreach ($event as $events) {
            if(strtotime($events->dateEnd)>strtotime(date('Y-m-d'))){
                $status = "expired";
            }else{
                if(strtotime($events->dateStart)<=strtotime(date('Y-m-d')) && 
                strtotime($events->dateEnd)>=strtotime(date('Y-m-d'))){
                    $status = "ongoing";
                }elseif (strtotime($events->dateStart)>strtotime(date('Y-m-d'))) {
                    $status = 'pending';
                }
            }
            $html.="<tr>
                        <td>$status</td>
                        <td>$events->eventsTitle</td>
                        <td>$events->dateStart - $events->dateEnd</td>
                        <td>$events->timeStart - $events->timeEnd</td>
                        <td>$events->location</td>
                    </tr>";
        }
        $html.="</tbody>
            </table>
            </body>
            </html>";
        PDF::loadHTML($html)->save(public_path("temp/".'report.pdf'));
        //return redirect('temp/companylogo.png');
        return redirect('temp/report.pdf');
    }
    public function getStudentsForMessage($viewby, $schoolyear, $semester, $coursegrade, $search=''){
        $user = new userController();
        $systemdate = systemDate::all();
        $sql='';
        if(session()->get('user')['userType']=='admin'){
            $sql = "SELECT COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) 
                    semesterID, COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) 
                    schoolYearID, COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) 					studentType, 
                    COALESCE(col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) courseGrade, 
                    COALESCE(col.studentNo, shs.studNo, jhs.studNo, elem.studNo) studentNo, students.*,
                    col.courseName, col.yearLevel
                    FROM students LEFT JOIN (SELECT college_students.studentID, college_students.yearLevel, college_students.semesterID, college_students.schoolYearID, college_students.id, 'college' AS studentType, 
                    college_students.courseID, courses.courseName, college_students.studentNo FROM college_students INNER JOIN (SELECT max(id) as maxid FROM college_students GROUP 
                    BY studentID) cs ON cs.maxid = college_students.id
                    INNER JOIN courses ON courses.id = college_students.courseID) col ON col.studentID = students.id LEFT 
                    JOIN (SELECT studentID, semesterID, schoolYearID, id, 'seniorhigh' AS studentType, gradeLevel, studNo 
                    FROM s_h_s_students INNER JOIN (SELECT max(id) as maxid FROM s_h_s_students GROUP BY studentID) 
                    shs ON shs.maxid = s_h_s_students.id) shs ON shs.studentID = students.id
                    LEFT JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'juniorhigh' AS 
                    studentType, gradeLevel, studNo  FROM j_h_students INNER JOIN (SELECT max(id) as maxid FROM j_h_students 
                    GROUP BY studentID) jhs ON jhs.maxid = j_h_students.id) jhs ON jhs.studentID = students.id LEFT 
                    JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'elementary' AS studentType, 
                    gradeLevel, studNo FROM elem_students INNER JOIN (SELECT max(id) as maxid FROM elem_students GROUP 
                    BY studentID) elem ON elem.maxid = elem_students.id) elem ON elem.studentID = students.id
            WHERE (students.firstName LIKE '%$search%' OR students.lastName LIKE '%$search%') 
            AND DATE(students.created_at) <= '".$systemdate[0]->systemDate."'";
            switch ($viewby) {
                case 'college':
                    $sql .= " AND COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) = 'college'";
                break;
                case 'seniorhigh':
                    $sql .= " AND COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) = 'seniorhigh'";
                break;
                case 'juniorhigh':
                    $sql .= " AND COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) = 'juniorhigh'";
                break;
                case 'elementary':
                    $sql .= " AND COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) = 'elementary'";
                break;
                default:
                   
                break;
            }
            if($schoolyear!="all"){
                $sql .= " AND COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) = '$schoolyear'";
            }
            if($semester!="all"){
                $sql .= " AND COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) = '$semester'";
            }
            if($coursegrade!="all"){
                $sql .= " AND COALESCE(col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) = '$coursegrade'";
            }
        }else{
            $designation = $user->getUserDesignation(Auth::guard('myuser')->id());
            $sql = "SELECT COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) 
                    semesterID, COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) 
                    schoolYearID, COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) 					studentType, 
                    COALESCE(col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) courseGrade, 
                    COALESCE(col.studentNo, shs.studNo, jhs.studNo, elem.studNo) studentNo, students.*,
                    col.courseName, col.yearLevel
                    FROM students LEFT JOIN (SELECT college_students.studentID, college_students.yearLevel, college_students.semesterID, college_students.schoolYearID, college_students.id, 'college' AS studentType, 
                    college_students.courseID, courses.courseName, college_students.studentNo FROM college_students INNER JOIN (SELECT max(id) as maxid FROM college_students GROUP 
                    BY studentID) cs ON cs.maxid = college_students.id
                    INNER JOIN courses ON courses.id = college_students.courseID) col ON col.studentID = students.id LEFT 
                    JOIN (SELECT studentID, semesterID, schoolYearID, id, 'seniorhigh' AS studentType, gradeLevel, studNo 
                    FROM s_h_s_students INNER JOIN (SELECT max(id) as maxid FROM s_h_s_students GROUP BY studentID) 
                    shs ON shs.maxid = s_h_s_students.id) shs ON shs.studentID = students.id
                    LEFT JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'juniorhigh' AS 
                    studentType, gradeLevel, studNo  FROM j_h_students INNER JOIN (SELECT max(id) as maxid FROM j_h_students 
                    GROUP BY studentID) jhs ON jhs.maxid = j_h_students.id) jhs ON jhs.studentID = students.id LEFT 
                    JOIN (SELECT studentID, null as semesterID, schoolYearID, id, 'elementary' AS studentType, 
                    gradeLevel, studNo FROM elem_students INNER JOIN (SELECT max(id) as maxid FROM elem_students GROUP 
                    BY studentID) elem ON elem.maxid = elem_students.id) elem ON elem.studentID = students.id
                    WHERE (students.firstName LIKE '%$search%' OR students.lastName LIKE '%$search%') 
                    AND DATE(students.created_at) <= '".$systemdate[0]->systemDate."'";
            switch($designation[0]){
                case 'college':
                    $sql= "SELECT cs2.courseID AS courseGrade, c2.courseName, cs2.schoolYearID, cs2.studentNo, 
                    cs2.yearLevel, students.*, 'college' AS studentType, cs2.semesterID FROM students 
                    INNER JOIN (SELECT studentID, studentNo,
                    id, courseID, schoolYearID, yearLevel, semesterID FROM college_students  INNER JOIN (SELECT max(id) as maxid 
                    FROM college_students 
                    GROUP BY studentID) cs ON cs.maxid = college_students.id) cs2 ON cs2.studentID = students.id 
                    INNER JOIN (SELECT c.id AS courseID, dep.departmentID, c.courseName FROM courses c INNER JOIN (SELECT
                    id, courseID FROM course_department_hists cd INNER JOIN (SELECT max(id) as maxid FROM 
                    course_department_hists GROUP BY courseID) cd2 ON cd2.maxid = cd.id) cdh ON cdh.courseID = c.id
                    INNER JOIN (SELECT departmentID, id FROM course_department_hists INNER JOIN (SELECT
                    max(id) AS maxid FROM course_department_hists GROUP BY courseID) cdh1 ON cdh1.maxid = 
                    course_department_hists.id) dep ON dep.id =cdh.id) c2 ON c2.courseID = cs2.courseID 
                    INNER JOIN (SELECT departmentID, id, collegeID FROM department_college_hists
                    INNER JOIN (SELECT max(id) as maxid FROM department_college_hists GROUP BY departmentID) 
                    depc ON depc.maxid = department_college_hists.id) depc2 ON depc2.departmentID=c2.departmentID 
                    INNER JOIN (SELECT collegeID, userID, id FROM assignment_colleges INNER JOIN (SELECT max(id) 
                    as maxid FROM assignment_colleges GROUP BY userID) assc ON assc.maxid = assignment_colleges.id) 
                    assc2 ON assc2.collegeID = depc2.collegeID WHERE assc2.userID ='".Auth::guard('myuser')->id()."'
                    AND DATE(students.created_at) <= '".$systemdate[0]->systemDate."'
                    AND (students.firstName LIKE '%$search%' OR students.lastName LIKE '%$search%') ";
                    if($schoolyear != "all"){
                        $sql .= " AND cs2.schoolYearID = '$schoolyear'";
                    }
                    if($semester != "all"){
                        $sql .= " AND cs2.semesterID = '$semester'";
                    }
                    if($coursegrade != "all"){
                        $sql .= " AND cs2.courseID = '$coursegrade'";
                    }
                    
                break;
                case 'seniorhigh':
                    $sql .= " AND COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) = 'seniorhigh'
                              AND COALESCE(col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) = '".$designation[1]."'";
                     if($schoolyear != "all"){
                        $sql .= " AND COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) = '$schoolyear'";
                    }
                    if($semester != "all"){
                        $sql .= " AND COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) = '$semester'";
                    }
                break;
                case 'juniorhigh':
                    $sql .= " AND COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) = 'juniorhigh'
                                AND COALESCE(col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) = '".$designation[1]."'";
                    if($schoolyear != "all"){
                        $sql .= " AND COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) = '$schoolyear'";
                    }
                    if($semester != "all"){
                        $sql .= " AND COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) = '$semester'";
                    }
                break;
                case 'elementary':
                    $sql .= " AND COALESCE(col.studentType, shs.studentType, jhs.studentType, elem.studentType) = 'elementary'
                                AND COALESCE(col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) = '".$designation[1]."'";
                    if($schoolyear != "all"){
                        $sql .= " AND COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, elem.schoolYearID) = '$schoolyear'";
                    }
                    if($semester != "all"){
                        $sql .= " AND COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) = '$semester'";
                    }
                break;
            }
        }
        $students = DB::select(DB::raw($sql));
        if(count($students)==0){
            return "<h2>There are no students found.</h2>";
        }else{
            $html = '';
            $this->rowCounter=0;
            foreach ($students as $student) {
                $this->rowCounter++;
                $html .= $this->getStudentRowFaster($student);
            }
            return $html;
        }    
    }
    public function getStudentRowFaster($student){
        return "<tr id  = 'student-$student->id'>
                    <td><input type ='checkbox' class ='checkboxStudent' 
                    id ='studentevent-$student->id' value ='$student->id'></td>
                    <td>$student->lastName, $student->firstName</td>
                    <td>$student->studentNo</td>
                    <td>$student->studentType</td>
                    <td>$student->contactNo</td>
                </tr>";
    }
    public function getMessages($eventid){
        $messages = eventMessage::where("eventID", '=', $eventid)->get();
        if(count($messages)>0){
            $html = '';
            $mes='';
            foreach ($messages as $message) {
                if(strlen($message->message)>30){
                    $mes =  substr($message->message, 0, 30)."...";
                }else{
                    $mes = $message->message;
                }
                return "<tr>
                            <td>$mes</td>
                            <td>$message->created_at</td>
                        </tr>";
            }
        }else{
            return "<h2>No messages yet</h2>";
        }
    }
    public function sendMessage(Request $request){
        $this->validate($request, array(
            'message'=>'required'
        ));
        $systemdate = systemDate::all();
        $student = student::find($request->studentID);
        $ch = curl_init();
        $itexmo = array('1' => $student->contactNo, '2' => $request->message, '3' => 'TR-EPHRA940314_T1W6X');
        curl_setopt($ch, CURLOPT_URL,"https://www.itexmo.com/php_api/api.php");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, 
        http_build_query($itexmo));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec ($ch);
        error_log($response);
        curl_close ($ch);
        $message  =  new eventMessage;
        $message->eventID = $request->eventID;
        $message->message = $request->message;
        $message->created_at = $systemdate[0]->systemDate;
        $message->save();
        return $student->firstName." ".$student->lastName;
    }
    public function getAdminSetting(){
        $temp = new \stdClass();
        $count = adminsetting::all();
        if(count($count)>0){
            $adminsetting = adminsetting::find($count[0]->id);
            $html = "<header style = 'position:fixed; top:-100px; left:0px; right:0px; height: 60px;'>
                        <div style = 'width:19%; display:inline-block;  padding: 0px 0px 0px 50px;'>
                            <img style = 'height:100px; width:100px; border-radius:50px;' src = 'images-database/adminsetting/schoolimage/$adminsetting->id.$adminsetting->schoolSealImageType'>
                        </div>
                        <div style = 'width:50%; height:120px; display:inline-block;  margin:0px;'>
                            <center>
                                <h4 class='header' style='color:black; margin:0px;'>$adminsetting->schoolName</h4>
                                <h4 class='header' style='color:black; margin:0px;'>$adminsetting->guidanceName</h4>
                                <h4 class='header' style='color:black; margin:0px;'>$adminsetting->streetAddress, $adminsetting->barangay</h4>
                                <h4 class='header' style='color:black; margin:0px;'>$adminsetting->city</h4>
                                <h4 class='header' style='color:black; margin:0px;'>Philippines, 7000</h4>
                            </center>    
                        </div>
                        <div style = 'width:15%; display:inline-block; padding: 0px 50px 0px 0px;  text-align: right;'>
                            <img style = 'height:100px; width:100px; border-radius:50px;' src = 'images-database/adminsetting/guidanceimage/$adminsetting->id.$adminsetting->guidanceSealImageType'>
                        </div>
                    </header>";
            return $html;
        }else{
            return "";
        }
    }
}
