$(document).ready(function(){
  $("#contractFilterContainer").hide();
  $("#counselingFilterContainer").hide();
  $("#referralBehaviourContainer").hide();
  $("#counselingCounselorFilterContainer").hide();
  var studentid;
  var referralid; 
  var contractid; 
  var counselingid;
  tableLoader('6','studentTableReport');
  tableLoader('7','referralTableRows');
  tableLoader('8','contractTableRows');
  tableLoader('8','counselingTableRows');
  $.get('/report/referralbehaviour/combo/', function(data){
    $("#referralBehaviourFilter").html("<option value = 'all'>all</option>"+data);
  });
  $.get('/report/counselingcounselor/combo/', function(data){
    $("#counselingCounselorFilter").html("<option value = 'all'>all</option>"+data);
  });
  $.get('/report/counseling/list/all/all/all/all/all/all/all', function(data){
    $('#counselingTableRows').html(data);
  });
  $.get('/report/contract/list/all/all/all/all/all/all', function(data){
    $('#contractTableRows').html(data);
  });
  $.get('/report/referral/list/all/all/all/all/all/all', function(data){
    $('#referralTableRows').html(data);
  });
  $.get('/report/students/all/all/all/all/all', function(data){
    $('#studentTableReport').html(data);
  });
  $.get('/contract/remarks/consecutive', function(data){
    $("#contractRemarksTables").html(data);
  });
  // $('#viewByStudentType').on('change', function(){
    //   if($('#viewByStudentType').val() == 'all'){
    //     $('#Course_GradeLevel').val('all');
    //     $('#Course_GradeLevel').attr('disabled', true);
    //     tableLoader('5','studentTableReport');
    //     tableLoader('7','referralTableRows');
    //     tableLoader('8','contractTableRows');
    //     tableLoader('8','counselingTableRows');
    //     $.get('/report/referral/list/all/all/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val(), function(data){
    //       $('#referralTableRows').html(data);
    //     });
    //     $.get('/report/students/all/all/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val(), function(data){
    //       $('#studentTableReport').html(data);
    //     });
    //     $.get('/report/contract/list/all/all/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val()+'/'+$("#contractTypeFilter").val(), function(data){
    //       $('#contractTableRows').html(data);
    //     });
    //     $.get('/report/counseling/list/'+$('#viewByStudentType').val()+'/'+$('#Course_GradeLevel').val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val()+'/'+$("#counseligStatusFilter").val(), function(data){
    //       $('#counselingTableRows').html(data);
    //     });
    //   }else{
    //     $('#Course_GradeLevel').attr('disabled', false);
    //     switch ($('#viewByStudentType').val()) {
    //       case 'college':
    //       tableLoader('5','studentTableReport');
    //       tableLoader('7','referralTableRows');
    //       tableLoader('8','contractTableRows');
    //       tableLoader('8','counselingTableRows');
    //         $.get('/report/college/course', function(data){
    //           $('#Course_GradeLevel').html("<option value='all'>all</option>");
    //           $('#Course_GradeLevel').append(data);
    //           $.get('/report/students/college/'+$('#Course_GradeLevel').val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val(), function(data){
    //             $('#studentTableReport').html(data);
    //           });
    //           $.get('/report/referral/list/college/'+$('#Course_GradeLevel').val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val(), function(data){
    //             $('#referralTableRows').html(data);
    //           });
    //           $.get('/report/contract/list/college/'+$('#Course_GradeLevel').val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val()+'/'+$("#contractTypeFilter").val(), function(data){
    //             $('#contractTableRows').html(data);
    //           });
              
    //           $.get('/report/counseling/list/'+$('#viewByStudentType').val()+'/'+$('#Course_GradeLevel').val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val()+'/'+$("#counseligStatusFilter").val(), function(data){
    //             $('#counselingTableRows').html(data);
    //           });
    //         });
    //       break;
    //       case 'seniorhigh':
    //         var html = "<option value ='all'>all</option>"+
    //                     "<option value ='12'>12</option>"+
    //                     "<option value ='11'>11</option>";
    //         $('#Course_GradeLevel').html(html);
    //         tableLoader('5','studentTableReport');
            
    //         $.get('/report/students/seniorhigh/'+$('#Course_GradeLevel').val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val(), function(data){
    //           $('#studentTableReport').html(data);
    //         });
    //         tableLoader('7','referralTableRows');
    //         $.get('/report/referral/list/seniorhigh/'+$('#Course_GradeLevel').val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val(), function(data){
    //           $('#referralTableRows').html(data);
    //         });
    //         tableLoader('8','contractTableRows');
    //         $.get('/report/contract/list/seniorhigh/'+$('#Course_GradeLevel').val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val()+'/'+$("#contractTypeFilter").val(), function(data){
    //           $('#contractTableRows').html(data);
    //         });
    //         tableLoader('8','counselingTableRows');
    //         $.get('/report/counseling/list/'+$('#viewByStudentType').val()+'/'+$('#Course_GradeLevel').val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val()+'/'+$("#counseligStatusFilter").val(), function(data){
    //           $('#counselingTableRows').html(data);
    //         });
    //       break;
    //       case 'juniorhigh':
    //         var html = "<option value ='all'>all</option>"+
    //                     "<option value ='10'>10</option>"+
    //                     "<option value ='9'>9</option>"+
    //                     "<option value ='8'>8</option>"+
    //                     "<option value ='7'>7</option>";
    //         $('#Course_GradeLevel').html(html);
    //         tableLoader('5','studentTableReport');
    //         $.get('/report/students/juniorhigh/'+$('#Course_GradeLevel').val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val(), function(data){
    //           $('#studentTableReport').html(data);
    //         });
    //         tableLoader('7','referralTableRows');
    //         $.get('/report/referral/list/juniorhigh/'+$('#Course_GradeLevel').val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val(), function(data){
    //           $('#referralTableRows').html(data);
    //         });
    //         tableLoader('8','contractTableRows');
    //         $.get('/report/contract/list/juniorhigh/'+$('#Course_GradeLevel').val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val()+'/'+$("#contractTypeFilter").val(), function(data){
    //           $('#contractTableRows').html(data);
    //         });
    //         tableLoader('8','counselingTableRows');
    //         $.get('/report/counseling/list/'+$('#viewByStudentType').val()+'/'+$('#Course_GradeLevel').val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val()+'/'+$("#counseligStatusFilter").val(), function(data){
    //           $('#counselingTableRows').html(data);
    //         });
    //       break;
    //       case 'elementary':
    //         var html = "<option value ='all'>all</option>"+
    //                 "<option value ='6'>6</option>"+
    //                 "<option value ='5'>5</option>"+
    //                 "<option value ='4'>4</option>"+
    //                 "<option value ='3'>3</option>"+
    //                 "<option value ='2'>2</option>"+
    //                 "<option value ='1'>1</option>";
    //         $('#Course_GradeLevel').html(html);
    //         tableLoader('5','studentTableReport');
    //         $.get('/report/students/elementary/'+$('#Course_GradeLevel').val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val(), function(data){
    //           $('#studentTableReport').html(data);
    //         });
    //         tableLoader('7','referralTableRows');
    //         $.get('/report/referral/list/elementary/'+$('#Course_GradeLevel').val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val(), function(data){
    //           $('#referralTableRows').html(data);
    //         });
    //         tableLoader('8','contractTableRows');
    //         $.get('/report/contract/list/elementary/'+$('#Course_GradeLevel').val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val()+'/'+$("#contractTypeFilter").val(), function(data){
    //           $('#contractTableRows').html(data);
    //         });
    //         tableLoader('8','counselingTableRows');
    //         $.get('/report/counseling/list/'+$('#viewByStudentType').val()+'/'+$('#Course_GradeLevel').val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val()+'/'+$("#counseligStatusFilter").val(), function(data){
    //           $('#counselingTableRows').html(data);
    //         });
    //       break;
    //       default:
    //         break; 
    //     }
    //   }  
    // });
    // $('#Course_GradeLevel').on('change', function(){
    //   tableLoader('5','studentTableReport');
    //   $.get('/report/students/'+$('#viewByStudentType').val()+'/'+$(this).val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val(), function(data){
    //     $('#studentTableReport').html(data);
    //   });
    //   tableLoader('7','referralTableRows');
    //   $.get('/report/referral/list/'+$('#viewByStudentType').val()+'/'+$(this).val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val(), function(data){
    //     $('#referralTableRows').html(data);
    //   });
    //   tableLoader('8','contractTableRows');
    //   $.get('/report/contract/list/'+$('#viewByStudentType').val()+'/'+$(this).val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val()+'/'+$("#contractTypeFilter").val(), function(data){
    //     $('#contractTableRows').html(data);
    //   });
    //   tableLoader('8','counselingTableRows');
    //   $.get('/report/counseling/list/'+$('#viewByStudentType').val()+'/'+$('#Course_GradeLevel').val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val()+'/'+$("#counseligStatusFilter").val(), function(data){
    //     $('#counselingTableRows').html(data);
    //   });
    // });
    // $('#schoolYearStudent').on('change', function(){
    //   tableLoader('5','studentTableReport');
    //   tableLoader('7','referralTableRows');
    //   tableLoader('8','contractTableRows');
    //   tableLoader('8','counselingTableRows');
    //   if($('#schoolYearStudent').val() != "all"){
    //     $.get('/report/schoolyear/'+$('#schoolYearStudent').val(), function(data){
    //       $('#semesterStudent').html("<option value='all'>all</option>");
    //       $('#semesterStudent').append(data);
    //       $.get('/report/students/'+$('#viewByStudentType').val()+'/'+$('#Course_GradeLevel').val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val(), function(data){
    //         $('#studentTableReport').html(data);
    //       });
    //       $.get('/report/referral/list/'+$('#viewByStudentType').val()+'/'+$('#Course_GradeLevel').val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val(), function(data){
    //         $('#referralTableRows').html(data);
    //       });
    //       $.get('/report/contract/list/'+$('#viewByStudentType').val()+'/'+$('#Course_GradeLevel').val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val()+'/'+$("#contractTypeFilter").val(), function(data){
    //         $('#contractTableRows').html(data);
    //       });
    //       $.get('/report/counseling/list/'+$('#viewByStudentType').val()+'/'+$('#Course_GradeLevel').val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val()+'/'+$("#counseligStatusFilter").val(), function(data){
    //         $('#counselingTableRows').html(data);
    //       });
    //     });
    //   }else{
    //     $.get('/report/students/'+$('#viewByStudentType').val()+'/'+$('#Course_GradeLevel').val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val(), function(data){
    //       $('#studentTableReport').html(data);
    //     });
    //     $.get('/report/referral/list/'+$('#viewByStudentType').val()+'/'+$('#Course_GradeLevel').val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val(), function(data){
    //       $('#referralTableRows').html(data);
    //     });
    //     $.get('/report/contract/list/'+$('#viewByStudentType').val()+'/'+$('#Course_GradeLevel').val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val()+'/'+$("#contractTypeFilter").val(), function(data){
    //       $('#contractTableRows').html(data);
    //     });
    //     $.get('/report/counseling/list/'+$('#viewByStudentType').val()+'/'+$('#Course_GradeLevel').val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val()+'/'+$("#counseligStatusFilter").val(), function(data){
    //       $('#counselingTableRows').html(data);
    //     });
    //   }
      
    // });
    // $('#semesterStudent').on('change', function(){
    //   tableLoader('5','studentTableReport');
    //   $.get('/report/students/'+$('#viewByStudentType').val()+'/'+$('#Course_GradeLevel').val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val(), function(data){
    //     $('#studentTableReport').html(data);
    //   });
    //   tableLoader('7','referralTableRows');
    //   $.get('/report/referral/list/'+$('#viewByStudentType').val()+'/'+$('#Course_GradeLevel').val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val(), function(data){
    //     $('#referralTableRows').html(data);
    //   });
    //   tableLoader('8','contractTableRows');
    //   $.get('/report/contract/list/'+$('#viewByStudentType').val()+'/'+$('#Course_GradeLevel').val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val()+'/'+$("#contractTypeFilter").val(), function(data){
    //     $('#contractTableRows').html(data);
    //   });
    //   tableLoader('8','counselingTableRows');
    //   $.get('/report/counseling/list/'+$('#viewByStudentType').val()+'/'+$('#Course_GradeLevel').val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val()+'/'+$("#counseligStatusFilter").val(), function(data){
    //     $('#counselingTableRows').html(data);
    //   });
    // });
  
  $('#studentTypeFilter').on('change', function(){
    if($('#studentTypeFilter').val() == 'all'){
        $('#courseGradeLevelFilter').val('all');
        $('#courseGradeLevelFilter').attr('disabled', true);
        $("#gradeLevelFilter").val('all');
        $("#gradeLevelFilter").attr('disabled', true);
        tableLoader('5','studentTableReport');
        tableLoader('7','referralTableRows');
        tableLoader('8','contractTableRows');
        tableLoader('8','counselingTableRows');
        //get students
        getAjax('/report/students/'+
        $('#studentTypeFilter').val()+
        '/'+$('#courseGradeLevelFilter').val()+
        '/'+$("#gradeLevelFilter").val()+
        '/'+$('#schoolYearFilter').val()+
        '/'+$('#semesterFilter').val(), 
        'studentTableReport');
        //get referrals
        getAjax('/report/referral/list/'
        +$('#studentTypeFilter').val()
        +'/'+$('#courseGradeLevelFilter').val()
        +'/'+$("#gradeLevelFilter").val()
        +'/'+$('#schoolYearFilter').val()
        +'/'+$('#semesterFilter').val()
        +'/'+$("#referralBehaviourFilter").val(), 'referralTableRows');
        //get contracts
        getAjax('/report/contract/list/'
        +$('#studentTypeFilter').val()
        +'/'+$('#courseGradeLevelFilter').val()
        +'/'+$("#gradeLevelFilter").val()
        +'/'+$('#schoolYearFilter').val()
        +'/'+$('#semesterFilter').val()
        +'/'+$("#contractTypeFilter").val(), 'contractTableRows');
        //get counseling
        getAjax('/report/counseling/list/'
        +$('#studentTypeFilter').val()
        +'/'+$('#courseGradeLevelFilter').val()
        +'/'+$("#gradeLevelFilter").val()
        +'/'+$('#schoolYearFilter').val()
        +'/'+$('#semesterFilter').val()
        +'/'+$("#counseligStatusFilter").val()
        +'/'+$("#counselingCounselorFilter").val(), 'counselingTableRows');
      }else{
        $('#courseGradeLevelFilter').attr('disabled', false);
        if($('#courseGradeLevelFilter')!= "all"){
          $("#gradeLevelFilter").attr('disabled', false);
        }
        switch ($('#studentTypeFilter').val()) {
          case 'college':
            tableLoader('5','studentTableReport');
            tableLoader('7','referralTableRows');
            tableLoader('8','contractTableRows');
            tableLoader('8','counselingTableRows');
            $.get('/report/college/course', function(data){
              $('#courseGradeLevelFilter').html("<option value='all'>all</option>");
              $('#courseGradeLevelFilter').append(data);
              //get students
              getAjax('/report/students/'+
              $('#studentTypeFilter').val()+
              '/'+$('#courseGradeLevelFilter').val()+
              '/'+$("#gradeLevelFilter").val()+
              '/'+$('#schoolYearFilter').val()+
              '/'+$('#semesterFilter').val(), 
              'studentTableReport');
              //get referral
              getAjax('/report/referral/list/'
              +$('#studentTypeFilter').val()
              +'/'+$('#courseGradeLevelFilter').val()
              +'/'+$("#gradeLevelFilter").val()
              +'/'+$('#schoolYearFilter').val()
              +'/'+$('#semesterFilter').val()
              +'/'+$("#referralBehaviourFilter").val(), 'referralTableRows');
              //get contracts
              getAjax('/report/contract/list/'
              +$('#studentTypeFilter').val()
              +'/'+$('#courseGradeLevelFilter').val()
              +'/'+$("#gradeLevelFilter").val()
              +'/'+$('#schoolYearFilter').val()
              +'/'+$('#semesterFilter').val()
              +'/'+$("#contractTypeFilter").val(), 'contractTableRows');
              //get counseling
              getAjax('/report/counseling/list/'
              +$('#studentTypeFilter').val()
              +'/'+$('#courseGradeLevelFilter').val()
              +'/'+$("#gradeLevelFilter").val()
              +'/'+$('#schoolYearFilter').val()
              +'/'+$('#semesterFilter').val()
              +'/'+$("#counseligStatusFilter").val()
              +'/'+$("#counselingCounselorFilter").val(), 'counselingTableRows');
            });
          break;
          case 'seniorhigh':
            tableLoader('5','studentTableReport');
            tableLoader('7','referralTableRows');
            tableLoader('8','contractTableRows');
            tableLoader('8','counselingTableRows');
            var html = "<option value ='all'>all</option>"+
                        "<option value ='12'>12</option>"+
                        "<option value ='11'>11</option>";
            $('#gradeLevelFilter').html(html);
            $.get('/student/seniorhigh/strand', function(data){
              $('#courseGradeLevelFilter').html("<option value='all'>all</option>");
              $('#courseGradeLevelFilter').append(data);
              //getstudents
              getAjax('/report/students/'+
              $('#studentTypeFilter').val()+
              '/'+$('#courseGradeLevelFilter').val()+
              '/'+$("#gradeLevelFilter").val()+
              '/'+$('#schoolYearFilter').val()+
              '/'+$('#semesterFilter').val(), 
              'studentTableReport');
              //get referral
              getAjax('/report/referral/list/'
              +$('#studentTypeFilter').val()
              +'/'+$('#courseGradeLevelFilter').val()
              +'/'+$("#gradeLevelFilter").val()
              +'/'+$('#schoolYearFilter').val()
              +'/'+$('#semesterFilter').val()
              +'/'+$("#referralBehaviourFilter").val(), 'referralTableRows');
               //get contracts
               getAjax('/report/contract/list/'
               +$('#studentTypeFilter').val()
               +'/'+$('#courseGradeLevelFilter').val()
               +'/'+$("#gradeLevelFilter").val()
               +'/'+$('#schoolYearFilter').val()
               +'/'+$('#semesterFilter').val()
               +'/'+$("#contractTypeFilter").val(), 'contractTableRows');
              //get counseling
              getAjax('/report/counseling/list/'
              +$('#studentTypeFilter').val()
              +'/'+$('#courseGradeLevelFilter').val()
              +'/'+$("#gradeLevelFilter").val()
              +'/'+$('#schoolYearFilter').val()
              +'/'+$('#semesterFilter').val()
              +'/'+$("#counseligStatusFilter").val()
              +'/'+$("#counselingCounselorFilter").val(), 'counselingTableRows');
            });
          break;
          case 'juniorhigh':
            tableLoader('5','studentTableReport');
            tableLoader('7','referralTableRows');
            tableLoader('8','contractTableRows');
            tableLoader('8','counselingTableRows');
            var html = "<option value ='all'>all</option>"+
                        "<option value ='10'>10</option>"+
                        "<option value ='9'>9</option>"+
                        "<option value ='8'>8</option>"+
                        "<option value ='7'>7</option>";
            $('#gradeLevelFilter').html(html);
            //getstudents
            getAjax('/report/students/'+
            $('#studentTypeFilter').val()+
            '/'+$('#courseGradeLevelFilter').val()+
            '/'+$("#gradeLevelFilter").val()+
            '/'+$('#schoolYearFilter').val()+
            '/'+$('#semesterFilter').val(), 
            'studentTableReport');
            //get referral
            getAjax('/report/referral/list/'
            +$('#studentTypeFilter').val()
            +'/'+$('#courseGradeLevelFilter').val()
            +'/'+$("#gradeLevelFilter").val()
            +'/'+$('#schoolYearFilter').val()
            +'/'+$('#semesterFilter').val()
            +'/'+$("#referralBehaviourFilter").val(), 'referralTableRows')
            //get contracts
            getAjax('/report/contract/list/'
            +$('#studentTypeFilter').val()
            +'/'+$('#courseGradeLevelFilter').val()
            +'/'+$("#gradeLevelFilter").val()
            +'/'+$('#schoolYearFilter').val()
            +'/'+$('#semesterFilter').val()
            +'/'+$("#contractTypeFilter").val(), 'contractTableRows');
            //get counseling
            getAjax('/report/counseling/list/'
            +$('#studentTypeFilter').val()
            +'/'+$('#courseGradeLevelFilter').val()
            +'/'+$("#gradeLevelFilter").val()
            +'/'+$('#schoolYearFilter').val()
            +'/'+$('#semesterFilter').val()
            +'/'+$("#counseligStatusFilter").val()
            +'/'+$("#counselingCounselorFilter").val(), 'counselingTableRows');
          break;
          case 'elementary':
            tableLoader('5','studentTableReport');
            tableLoader('7','referralTableRows');
            tableLoader('8','contractTableRows');
            tableLoader('8','counselingTableRows');
            var html = "<option value ='all'>all</option>"+
                    "<option value ='6'>6</option>"+
                    "<option value ='5'>5</option>"+
                    "<option value ='4'>4</option>"+
                    "<option value ='3'>3</option>"+
                    "<option value ='2'>2</option>"+
                    "<option value ='1'>1</option>";
            $('#gradeLevelFilter').html(html);
            //getstudents
            getAjax('/report/students/'+
            $('#studentTypeFilter').val()+
            '/'+$('#courseGradeLevelFilter').val()+
            '/'+$("#gradeLevelFilter").val()+
            '/'+$('#schoolYearFilter').val()+
            '/'+$('#semesterFilter').val(), 
            'studentTableReport');
            //get referral
            getAjax('/report/referral/list/'
            +$('#studentTypeFilter').val()
            +'/'+$('#courseGradeLevelFilter').val()
            +'/'+$("#gradeLevelFilter").val()
            +'/'+$('#schoolYearFilter').val()
            +'/'+$('#semesterFilter').val()
            +'/'+$("#referralBehaviourFilter").val(), 'referralTableRows');
            //get contracts
            getAjax('/report/contract/list/'
            +$('#studentTypeFilter').val()
            +'/'+$('#courseGradeLevelFilter').val()
            +'/'+$("#gradeLevelFilter").val()
            +'/'+$('#schoolYearFilter').val()
            +'/'+$('#semesterFilter').val()
            +'/'+$("#contractTypeFilter").val(), 'contractTableRows');
            //get counseling
            getAjax('/report/counseling/list/'
            +$('#studentTypeFilter').val()
            +'/'+$('#courseGradeLevelFilter').val()
            +'/'+$("#gradeLevelFilter").val()
            +'/'+$('#schoolYearFilter').val()
            +'/'+$('#semesterFilter').val()
            +'/'+$("#counseligStatusFilter").val()
            +'/'+$("#counselingCounselorFilter").val(), 'counselingTableRows');
          break;
          default:
            break;
        }
    }  
      
  });
  $('#schoolYearFilter').on('change', function(){
    tableLoader('5','studentTableReport');
    tableLoader('7','referralTableRows');
    tableLoader('8','contractTableRows');
    tableLoader('8','counselingTableRows');
    if($('#schoolYearFilter').val() != "all"){
      $.get('/report/schoolyear/'+$('#schoolYearFilter').val(), function(data){
        $('#semesterFilter').html("<option value='all'>all</option>");
        $('#semesterFilter').append(data);
        //getstudents
        getAjax('/report/students/'+
            $('#studentTypeFilter').val()+
            '/'+$('#courseGradeLevelFilter').val()+
            '/'+$("#gradeLevelFilter").val()+
            '/'+$('#schoolYearFilter').val()+
            '/'+$('#semesterFilter').val(), 
            'studentTableReport');
        //get referral
        getAjax('/report/referral/list/'
        +$('#studentTypeFilter').val()
        +'/'+$('#courseGradeLevelFilter').val()
        +'/'+$("#gradeLevelFilter").val()
        +'/'+$('#schoolYearFilter').val()
        +'/'+$('#semesterFilter').val()
        +'/'+$("#referralBehaviourFilter").val(), 'referralTableRows');
        //get contracts
        getAjax('/report/contract/list/'
        +$('#studentTypeFilter').val()
        +'/'+$('#courseGradeLevelFilter').val()
        +'/'+$("#gradeLevelFilter").val()
        +'/'+$('#schoolYearFilter').val()
        +'/'+$('#semesterFilter').val()
        +'/'+$("#contractTypeFilter").val(), 'contractTableRows');
        //get counseling
        getAjax('/report/counseling/list/'
        +$('#studentTypeFilter').val()
        +'/'+$('#courseGradeLevelFilter').val()
        +'/'+$("#gradeLevelFilter").val()
        +'/'+$('#schoolYearFilter').val()
        +'/'+$('#semesterFilter').val()
        +'/'+$("#counseligStatusFilter").val()
        +'/'+$("#counselingCounselorFilter").val(), 'counselingTableRows');
      });
    }else{
       //getstudents
      getAjax('/report/students/'+
        $('#studentTypeFilter').val()+
        '/'+$('#courseGradeLevelFilter').val()+
        '/'+$("#gradeLevelFilter").val()+
        '/'+$('#schoolYearFilter').val()+
        '/'+$('#semesterFilter').val(), 
        'studentTableReport');
      //get referral
      getAjax('/report/referral/list/'
      +$('#studentTypeFilter').val()
      +'/'+$('#courseGradeLevelFilter').val()
      +'/'+$("#gradeLevelFilter").val()
      +'/'+$('#schoolYearFilter').val()
      +'/'+$('#semesterFilter').val()
      +'/'+$("#referralBehaviourFilter").val(), 'referralTableRows');
      //get contracts
      getAjax('/report/contract/list/'
      +$('#studentTypeFilter').val()
      +'/'+$('#courseGradeLevelFilter').val()
      +'/'+$("#gradeLevelFilter").val()
      +'/'+$('#schoolYearFilter').val()
      +'/'+$('#semesterFilter').val()
      +'/'+$("#contractTypeFilter").val(), 'contractTableRows');
      //get counseling
      getAjax('/report/counseling/list/'
      +$('#studentTypeFilter').val()
      +'/'+$('#courseGradeLevelFilter').val()
      +'/'+$("#gradeLevelFilter").val()
      +'/'+$('#schoolYearFilter').val()
      +'/'+$('#semesterFilter').val()
      +'/'+$("#counseligStatusFilter").val()
      +'/'+$("#counselingCounselorFilter").val(), 'counselingTableRows');
     
    }
  });
  $('#semesterFilter').on('change', function(){
    tableLoader('5','studentTableReport');
    tableLoader('7','referralTableRows');
    tableLoader('8','contractTableRows');
    tableLoader('8','counselingTableRows');
    //getstudents
    getAjax('/report/students/'+
      $('#studentTypeFilter').val()+
      '/'+$('#courseGradeLevelFilter').val()+
      '/'+$("#gradeLevelFilter").val()+
      '/'+$('#schoolYearFilter').val()+
      '/'+$('#semesterFilter').val(), 
      'studentTableReport');
    //get referral
    getAjax('/report/referral/list/'
    +$('#studentTypeFilter').val()
    +'/'+$('#courseGradeLevelFilter').val()
    +'/'+$("#gradeLevelFilter").val()
    +'/'+$('#schoolYearFilter').val()
    +'/'+$('#semesterFilter').val()
    +'/'+$("#referralBehaviourFilter").val(), 'referralTableRows');
    //get contracts
    getAjax('/report/contract/list/'
    +$('#studentTypeFilter').val()
    +'/'+$('#courseGradeLevelFilter').val()
    +'/'+$("#gradeLevelFilter").val()
    +'/'+$('#schoolYearFilter').val()
    +'/'+$('#semesterFilter').val()
    +'/'+$("#contractTypeFilter").val(), 'contractTableRows');
    //get counseling
    getAjax('/report/counseling/list/'
    +$('#studentTypeFilter').val()
    +'/'+$('#courseGradeLevelFilter').val()
    +'/'+$("#gradeLevelFilter").val()
    +'/'+$('#schoolYearFilter').val()
    +'/'+$('#semesterFilter').val()
    +'/'+$("#counseligStatusFilter").val()
    +'/'+$("#counselingCounselorFilter").val(), 'counselingTableRows');
  });
  $('#courseGradeLevelFilter').on('change', function(){
    tableLoader('5','studentTableReport');
    tableLoader('7','referralTableRows');
    tableLoader('8','contractTableRows');
    tableLoader('8','counselingTableRows');
    if($('#courseGradeLevelFilter').val() == 'all'){
      $('#gradeLevelFilter').val("all");
      //$('#gradeLevelFilter').attr("disabled", true);
      
      //getstudents
      getAjax('/report/students/'+
        $('#studentTypeFilter').val()+
        '/'+$('#courseGradeLevelFilter').val()+
        '/'+$("#gradeLevelFilter").val()+
        '/'+$('#schoolYearFilter').val()+
        '/'+$('#semesterFilter').val(), 
        'studentTableReport');
      //get referral
      getAjax('/report/referral/list/'
      +$('#studentTypeFilter').val()
      +'/'+$('#courseGradeLevelFilter').val()
      +'/'+$("#gradeLevelFilter").val()
      +'/'+$('#schoolYearFilter').val()
      +'/'+$('#semesterFilter').val()
      +'/'+$("#referralBehaviourFilter").val(), 'referralTableRows');
      //get contracts
      getAjax('/report/contract/list/'
      +$('#studentTypeFilter').val()
      +'/'+$('#courseGradeLevelFilter').val()
      +'/'+$("#gradeLevelFilter").val()
      +'/'+$('#schoolYearFilter').val()
      +'/'+$('#semesterFilter').val()
      +'/'+$("#contractTypeFilter").val(), 'contractTableRows');
      //get counseling
      getAjax('/report/counseling/list/'
      +$('#studentTypeFilter').val()
      +'/'+$('#courseGradeLevelFilter').val()
      +'/'+$("#gradeLevelFilter").val()
      +'/'+$('#schoolYearFilter').val()
      +'/'+$('#semesterFilter').val()
      +'/'+$("#counseligStatusFilter").val()
      +'/'+$("#counselingCounselorFilter").val(), 'counselingTableRows');
    }else{
      $('#gradeLevelFilter').attr('disabled', false);
      switch ($('#studentTypeFilter').val()) {
        case 'college':
          $('#gradeLevelFilter').html("<option value ='all'>all</option>");
          $.get('/student/getcourseyear/'+$("#courseGradeLevelFilter").val(), function(data){
            $('#gradeLevelFilter').append(data);
            //getstudents
            getAjax('/report/students/'+
              $('#studentTypeFilter').val()+
              '/'+$('#courseGradeLevelFilter').val()+
              '/'+$("#gradeLevelFilter").val()+
              '/'+$('#schoolYearFilter').val()+
              '/'+$('#semesterFilter').val(), 
              'studentTableReport');
            //get referrals
            getAjax('/report/referral/list/'
              +$('#studentTypeFilter').val()
              +'/'+$('#courseGradeLevelFilter').val()
              +'/'+$("#gradeLevelFilter").val()
              +'/'+$('#schoolYearFilter').val()
              +'/'+$('#semesterFilter').val()
              +'/'+$("#referralBehaviourFilter").val(), 'referralTableRows');
            //get contracts
            getAjax('/report/contract/list/'
            +$('#studentTypeFilter').val()
            +'/'+$('#courseGradeLevelFilter').val()
            +'/'+$("#gradeLevelFilter").val()
            +'/'+$('#schoolYearFilter').val()
            +'/'+$('#semesterFilter').val()
            +'/'+$("#contractTypeFilter").val(), 'contractTableRows');
            //get counseling
            getAjax('/report/counseling/list/'
            +$('#studentTypeFilter').val()
            +'/'+$('#courseGradeLevelFilter').val()
            +'/'+$("#gradeLevelFilter").val()
            +'/'+$('#schoolYearFilter').val()
            +'/'+$('#semesterFilter').val()
            +'/'+$("#counseligStatusFilter").val()
            +'/'+$("#counselingCounselorFilter").val(), 'counselingTableRows');
          });
          
        break;
        case 'seniorhigh':
          var html = "<option value ='all'>all</option>"+
                      "<option value ='12'>12</option>"+
                      "<option value ='11'>11</option>";
          $('#gradeLevelFilter').html(html);
          //getstudents
          getAjax('/report/students/'+
          $('#studentTypeFilter').val()+
          '/'+$('#courseGradeLevelFilter').val()+
          '/'+$("#gradeLevelFilter").val()+
          '/'+$('#schoolYearFilter').val()+
          '/'+$('#semesterFilter').val(), 
          'studentTableReport');
          //get referrals
          getAjax('/report/referral/list/'
          +$('#studentTypeFilter').val()
          +'/'+$('#courseGradeLevelFilter').val()
          +'/'+$("#gradeLevelFilter").val()
          +'/'+$('#schoolYearFilter').val()
          +'/'+$('#semesterFilter').val()
          +'/'+$("#referralBehaviourFilter").val(), 'referralTableRows');
          //get contracts
          getAjax('/report/contract/list/'
          +$('#studentTypeFilter').val()
          +'/'+$('#courseGradeLevelFilter').val()
          +'/'+$("#gradeLevelFilter").val()
          +'/'+$('#schoolYearFilter').val()
          +'/'+$('#semesterFilter').val()
          +'/'+$("#contractTypeFilter").val(), 'contractTableRows');
          //get counseling
          getAjax('/report/counseling/list/'
          +$('#studentTypeFilter').val()
          +'/'+$('#courseGradeLevelFilter').val()
          +'/'+$("#gradeLevelFilter").val()
          +'/'+$('#schoolYearFilter').val()
          +'/'+$('#semesterFilter').val()
          +'/'+$("#counseligStatusFilter").val()
          +'/'+$("#counselingCounselorFilter").val(), 'counselingTableRows');
        break;
        case 'juniorhigh':
          var html = "<option value ='all'>all</option>"+
                      "<option value ='10'>10</option>"+
                      "<option value ='9'>9</option>"+
                      "<option value ='8'>8</option>"+
                      "<option value ='7'>7</option>";
          $('#gradeLevelFilter').html(html);
          //getstudents
          getAjax('/report/students/'+
          $('#studentTypeFilter').val()+
          '/'+$('#courseGradeLevelFilter').val()+
          '/'+$("#gradeLevelFilter").val()+
          '/'+$('#schoolYearFilter').val()+
          '/'+$('#semesterFilter').val(), 
          'studentTableReport');
          //get referrals
          getAjax('/report/referral/list/'
          +$('#studentTypeFilter').val()
          +'/'+$('#courseGradeLevelFilter').val()
          +'/'+$("#gradeLevelFilter").val()
          +'/'+$('#schoolYearFilter').val()
          +'/'+$('#semesterFilter').val()
          +'/'+$("#referralBehaviourFilter").val(), 'referralTableRows');
          //get contracts
          getAjax('/report/contract/list/'
          +$('#studentTypeFilter').val()
          +'/'+$('#courseGradeLevelFilter').val()
          +'/'+$("#gradeLevelFilter").val()
          +'/'+$('#schoolYearFilter').val()
          +'/'+$('#semesterFilter').val()
          +'/'+$("#contractTypeFilter").val(), 'contractTableRows');
          //get counseling
          getAjax('/report/counseling/list/'
          +$('#studentTypeFilter').val()
          +'/'+$('#courseGradeLevelFilter').val()
          +'/'+$("#gradeLevelFilter").val()
          +'/'+$('#schoolYearFilter').val()
          +'/'+$('#semesterFilter').val()
          +'/'+$("#counseligStatusFilter").val()
          +'/'+$("#counselingCounselorFilter").val(), 'counselingTableRows');
        break;
        case 'elementary':
          var html = "<option value ='all'>all</option>"+
                  "<option value ='6'>6</option>"+
                  "<option value ='5'>5</option>"+
                  "<option value ='4'>4</option>"+
                  "<option value ='3'>3</option>"+
                  "<option value ='2'>2</option>"+
                  "<option value ='1'>1</option>";
          $('#gradeLevelFilter').html(html);
          //getstudents
          getAjax('/report/students/'+
          $('#studentTypeFilter').val()+
          '/'+$('#courseGradeLevelFilter').val()+
          '/'+$("#gradeLevelFilter").val()+
          '/'+$('#schoolYearFilter').val()+
          '/'+$('#semesterFilter').val(), 
          'studentTableReport');
          //get referrals
          getAjax('/report/referral/list/'
          +$('#studentTypeFilter').val()
          +'/'+$('#courseGradeLevelFilter').val()
          +'/'+$("#gradeLevelFilter").val()
          +'/'+$('#schoolYearFilter').val()
          +'/'+$('#semesterFilter').val()
          +'/'+$("#referralBehaviourFilter").val(), 'referralTableRows');
          //get contracts
          getAjax('/report/contract/list/'
          +$('#studentTypeFilter').val()
          +'/'+$('#courseGradeLevelFilter').val()
          +'/'+$("#gradeLevelFilter").val()
          +'/'+$('#schoolYearFilter').val()
          +'/'+$('#semesterFilter').val()
          +'/'+$("#contractTypeFilter").val(), 'contractTableRows');
          //get counseling
          getAjax('/report/counseling/list/'
          +$('#studentTypeFilter').val()
          +'/'+$('#courseGradeLevelFilter').val()
          +'/'+$("#gradeLevelFilter").val()
          +'/'+$('#schoolYearFilter').val()
          +'/'+$('#semesterFilter').val()
          +'/'+$("#counseligStatusFilter").val()
          +'/'+$("#counselingCounselorFilter").val(), 'counselingTableRows');
        break;
        default:
          break;
      }
    }  
   
  });  
  $("#gradeLevelFilter").on('change', function(){
    tableLoader('7','referralTableRows');
    tableLoader('5','studentTableReport');
    tableLoader('8','contractTableRows');
    tableLoader('8','counselingTableRows');
    //getstudents
    getAjax('/report/students/'+
    $('#studentTypeFilter').val()+
    '/'+$('#courseGradeLevelFilter').val()+
    '/'+$("#gradeLevelFilter").val()+
    '/'+$('#schoolYearFilter').val()+
    '/'+$('#semesterFilter').val(), 
    'studentTableReport');
    //get referrals
    getAjax('/report/referral/list/'
    +$('#studentTypeFilter').val()
    +'/'+$('#courseGradeLevelFilter').val()
    +'/'+$("#gradeLevelFilter").val()
    +'/'+$('#schoolYearFilter').val()
    +'/'+$('#semesterFilter').val()
    +'/'+$("#referralBehaviourFilter").val(), 'referralTableRows');
    //get contracts
    getAjax('/report/contract/list/'
    +$('#studentTypeFilter').val()
    +'/'+$('#courseGradeLevelFilter').val()
    +'/'+$("#gradeLevelFilter").val()
    +'/'+$('#schoolYearFilter').val()
    +'/'+$('#semesterFilter').val()
    +'/'+$("#contractTypeFilter").val(), 'contractTableRows');
    //get counseling
    getAjax('/report/counseling/list/'
    +$('#studentTypeFilter').val()
    +'/'+$('#courseGradeLevelFilter').val()
    +'/'+$("#gradeLevelFilter").val()
    +'/'+$('#schoolYearFilter').val()
    +'/'+$('#semesterFilter').val()
    +'/'+$("#counseligStatusFilter").val()
    +'/'+$("#counselingCounselorFilter").val(), 'counselingTableRows');
  });
  $("#referralBehaviourFilter").on('change', function(){
    tableLoader('7','referralTableRows');
    getAjax('/report/referral/list/'
    +$('#studentTypeFilter').val()
    +'/'+$('#courseGradeLevelFilter').val()
    +'/'+$("#gradeLevelFilter").val()
    +'/'+$('#schoolYearFilter').val()
    +'/'+$('#semesterFilter').val()
    +'/'+$("#referralBehaviourFilter").val(), 'referralTableRows');
  });
  $("#contractTypeFilter").on('change', function(){
    tableLoader('8','contractTableRows');
    //get contracts
    getAjax('/report/contract/list/'
    +$('#studentTypeFilter').val()
    +'/'+$('#courseGradeLevelFilter').val()
    +'/'+$("#gradeLevelFilter").val()
    +'/'+$('#schoolYearFilter').val()
    +'/'+$('#semesterFilter').val()
    +'/'+$("#contractTypeFilter").val(), 'contractTableRows');
  });
  $("#counseligStatusFilter").on('change', function(){
    tableLoader('8','counselingTableRows');
     //get counseling
     getAjax('/report/counseling/list/'
     +$('#studentTypeFilter').val()
     +'/'+$('#courseGradeLevelFilter').val()
     +'/'+$("#gradeLevelFilter").val()
     +'/'+$('#schoolYearFilter').val()
     +'/'+$('#semesterFilter').val()
     +'/'+$("#counseligStatusFilter").val()
     +'/'+$("#counselingCounselorFilter").val(), 'counselingTableRows');
  });
  $("#counselingCounselorFilter").on('change', function(){
    tableLoader('8','counselingTableRows');
     //get counseling
     getAjax('/report/counseling/list/'
     +$('#studentTypeFilter').val()
     +'/'+$('#courseGradeLevelFilter').val()
     +'/'+$("#gradeLevelFilter").val()
     +'/'+$('#schoolYearFilter').val()
     +'/'+$('#semesterFilter').val()
     +'/'+$("#counseligStatusFilter").val()
     +'/'+$("#counselingCounselorFilter").val(), 'counselingTableRows');
  });
  $('#printStudents').on('click', function(){
    $.get('/report/studentsprint/'+
      $('#studentTypeFilter').val()+
      '/'+$('#courseGradeLevelFilter').val()+
      '/'+$("#gradeLevelFilter").val()+
      '/'+$('#schoolYearFilter').val()+
      '/'+$('#semesterFilter').val(), function(data){
      if(data=="true"){
        window.location.replace("/temp/report.pdf");
      }
    });
  });
  $('#printRefferals').on('click', function(){
    $.get('/report/referral/print/'
    +$('#studentTypeFilter').val()
    +'/'+$('#courseGradeLevelFilter').val()
    +'/'+$("#gradeLevelFilter").val()
    +'/'+$('#schoolYearFilter').val()
    +'/'+$('#semesterFilter').val()
    +'/'+$("#referralBehaviourFilter").val(), function(data){
      if(data=="true"){
        window.location.replace("/temp/report.pdf");
      } 
    });
  });
  $('#printContracts').on('click', function(){
    $.get('/report/contract/print/'+
    $('#studentTypeFilter').val()+
    '/'+$('#courseGradeLevelFilter').val()+
    '/'+$('#gradeLevelFilter').val()+
    '/'+$('#schoolYearFilter').val()+
    '/'+$('#semesterFilter').val()+
    '/'+$("#contractTypeFilter").val(), function(data){
      if(data=="true"){
        window.location.replace("/temp/report.pdf");
      }
    });
  });
  $('#printCounseling').on('click', function(){
    // $.get('/report/counseling/print/'+$('#viewByStudentType').val()+'/'+$('#Course_GradeLevel').val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val()+'/'+$("#counseligStatusFilter").val(), function(data){
    //   if(data=="true"){
    //     window.location.replace("/temp/report.pdf");
    //   }
    // });
   
    $.get('/report/counseling/print/'
     +$('#studentTypeFilter').val()
     +'/'+$('#courseGradeLevelFilter').val()
     +'/'+$("#gradeLevelFilter").val()
     +'/'+$('#schoolYearFilter').val()
     +'/'+$('#semesterFilter').val()
     +'/'+$("#counseligStatusFilter").val()
     +'/'+$("#counselingCounselorFilter").val(), function(data){
      if(data=="true"){
        window.location.replace("/temp/report.pdf");
      }
    });
  });
  $('#studentTableReport').on('click', 'tr', function(){
    var id =  $(this).prop('id').split('-')[1];
    studentid = id;
    $("#StudentTransactions").modal("show");
    tableLoader('2','tableForStudentUpdates');
    tableLoader('5','tableReferralOfStudent');
    tableLoader('4','contractTableForStudents');
    tableLoader('8','counselingStudentTable');
    $.get('/report/counselingstudent/'+id, function(data){
      $('#counselingStudentTable').html(data);
    });
    $.get('/report/contractstudent/'+id, function(data){
      $('#contractTableForStudents').html(data);
    });
    $.get('/report/studentdata/'+id, function(data){
      $('#nameTextModal').val(data.firstName+ ' '+data.lastName);
      var studenttype = '';
      switch (data.studentType) {
        case 'college':
          studenttype = 'College';
        break;
        case 'seniorhigh':
          studenttype = 'Senior High';
        break;
        case 'juniorhigh':
          studenttype = 'Junior High';
        break;
        case 'elementary':
          studenttype = 'Elementary';
        break;
        default:
          break;
      }
      $('#studentTypeTextModal').val(studenttype);
      $('#courseTextModal').val(data.course);
      var gender = '';
      switch (data.gender) {
        case 'male':
          gender = 'M';
        break;
        case 'female':
          gender = 'F';
        break;
        default:
          break;
      }
      $('#sexTextModal').val(gender);
      $('#homeAddressTextModal').val(data.cityAddress);
    });
    $.get('/report/updatestudentdata/'+id, function(data){
      $('#tableForStudentUpdates').html(data);
    });
    $.get('/report/referralstudents/'+id, function(data){
      $('#tableReferralOfStudent').html(data);
    });
  });
  $('#tableForStudentUpdates').on('click', 'tr', function(){
    var temp = [];
    var  i =0;
    var that = this;
    $(this).children('td').each(function(){
      temp[i]=$(this).html();
      i++;
    });
    //console.log()
    $('#dateStudentUpdatedDataTextModal').val(temp[0]);
    $('#updateStudentDataTextModal').val(temp[1].replace(/<br>/g, ""));
    $('#updateByTextModal').val($(that).attr('data-value'));
  });
  $('#printStudentDataUpdates').on('click', function(){
    $.get('/report/studentsprintupdates/'+studentid, function(data){
      if(data=="true"){
        window.location.replace("/temp/report.pdf");
      }
    });
  });
  $('#tableReferralOfStudent').on('click', 'tr', function(){
    referralid = $(this).prop('id').split('-')[1];
    tableLoader('3','behaviourOfReferralStudent');
    $.get('/report/behaviourreferralstudent/'+referralid, function(data){
      $('#behaviourOfReferralStudent').html(data.html);
      $('#referredToTextModal').val(data.referredTo);
      $('#referredByTextModal').val(data.referredBy + " ("+data.collegeName+")"); 
    });
  });
  $('#printReferralOfStudent').on('click', function(){
    $.get('/report/behaviourprint/'+studentid, function(data){
      if(data == "true"){
        window.location.replace("/temp/report.pdf");
      }
    });
  });
  $('#contractTableForStudents').on('click', 'tr', function(){
    contractid = $(this).prop('id').split('-')[1];
    tableLoader('3','subjectGradesContract');
    divLoader('proofsContract');
    $.get('/report/contractbehaviourandproof/'+contractid, function(data){
      $('#proofsContract').html(data.proofHtml);
      $('#subjectGradesContract').html(data.gradeHtml);
      $('#contractDateTextModal').val(data.contractDate);
      $('#contractTypeTextModal').val(capitalizeFirstLetter(data.type));
      $('#remarksContractTextModal').val(data.remarks);
      $('#notedByContractTextModal').val(data.notedBy);
      $('#averageGrade').val(data.averageGrade);
      
    });
  });
  $('#printContracStudent').on('click', function(){
    
    $.get('/report/contractprintstudent/'+studentid, function(data){
      console.log("hello");
      console.log(data);
      if(data=="true"){
        window.location.replace("/temp/report.pdf");
      }
      
    });
  });
  $('#counselingStudentTable').on('click', 'tr', function(){
    counselingid = $(this).prop('id').split('-')[1];
    tableLoader('3','followUpOfCounseling');  
    $.get('/report/counselingfollowup/'+counselingid, function(data){
      $('#followUpOfCounseling').html(data.html);
      //console.log(data);
      $('#counselingDateTextModal').val(data.dateRecorded);
      $('#schoolYearCounselingTextModal').val(data.schoolyear);
      $('#semesterCounselingTextModal').val(data.semesterID);
      $('#councelingTypeTextModal').val(capitalizeFirstLetter(data.counselingType));
      $('#reasonCounselingTextModal').val(data.statementOfTheProblem);
      $('#assistedByTextModal').val(data.assistedBy);
      $('#evaluationDateCounselingModal').val(data.evaluationDate);
      $('#recommendationCounselingTextModal').val(data.evaluationRemarks);
      $('#evaluatedByTextModal').val(data.evaluatedBy);
    });
  });
  $('#printCounselingOfStudent').on('click', function(){
    $.get('/report/counselingprintstudent/'+studentid, function(data){
      if(data == "true"){
        window.location.replace("/temp/report.pdf");
      }
    });
  });
  $("#counselingTableRows").on('click', 'tr', function(){
    var id =  $(this).attr("data-value");
    studentid = id;
    $("#StudentTransactions").modal("show");
    tableLoader('2','tableForStudentUpdates');
    tableLoader('5','tableReferralOfStudent');
    tableLoader('4','contractTableForStudents');
    tableLoader('8','counselingStudentTable');
    $.get('/report/counselingstudent/'+id, function(data){
      $('#counselingStudentTable').html(data);
    });
    $.get('/report/contractstudent/'+id, function(data){
      $('#contractTableForStudents').html(data);
    });
    $.get('/report/studentdata/'+id, function(data){
      $('#nameTextModal').val(data.firstName+ ' '+data.lastName);
      var studenttype = '';
      switch (data.studentType) {
        case 'college':
          studenttype = 'College';
        break;
        case 'seniorhigh':
          studenttype = 'Senior High';
        break;
        case 'juniorhigh':
          studenttype = 'Junior High';
        break;
        case 'elementary':
          studenttype = 'Elementary';
        break;
        default:
          break;
      }
      $('#studentTypeTextModal').val(studenttype);
      $('#courseTextModal').val(data.course);
      var gender = '';
      switch (data.gender) {
        case 'male':
          gender = 'M';
        break;
        case 'female':
          gender = 'F';
        break;
        default:
          break;
      }
      $('#sexTextModal').val(gender);
      $('#homeAddressTextModal').val(data.cityAddress);
    });
    $.get('/report/updatestudentdata/'+id, function(data){
      $('#tableForStudentUpdates').html(data);
    });
    $.get('/report/referralstudents/'+id, function(data){
      $('#tableReferralOfStudent').html(data);
    });
  });
  $("#referralTableRows").on('click', 'tr', function(){
    var id =  $(this).attr("data-value");
    studentid = id;
    $("#StudentTransactions").modal("show");
    tableLoader('2','tableForStudentUpdates');
    tableLoader('5','tableReferralOfStudent');
    tableLoader('4','contractTableForStudents');
    tableLoader('8','counselingStudentTable');
    $.get('/report/counselingstudent/'+id, function(data){
      $('#counselingStudentTable').html(data);
    });
    $.get('/report/contractstudent/'+id, function(data){
      $('#contractTableForStudents').html(data);
    });
    $.get('/report/studentdata/'+id, function(data){
      $('#nameTextModal').val(data.firstName+ ' '+data.lastName);
      var studenttype = '';
      switch (data.studentType) {
        case 'college':
          studenttype = 'College';
        break;
        case 'seniorhigh':
          studenttype = 'Senior High';
        break;
        case 'juniorhigh':
          studenttype = 'Junior High';
        break;
        case 'elementary':
          studenttype = 'Elementary';
        break;
        default:
          break;
      }
      $('#studentTypeTextModal').val(studenttype);
      $('#courseTextModal').val(data.course);
      var gender = '';
      switch (data.gender) {
        case 'male':
          gender = 'M';
        break;
        case 'female':
          gender = 'F';
        break;
        default:
          break;
      }
      $('#sexTextModal').val(gender);
      $('#homeAddressTextModal').val(data.cityAddress);
    });
    $.get('/report/updatestudentdata/'+id, function(data){
      $('#tableForStudentUpdates').html(data);
    });
    $.get('/report/referralstudents/'+id, function(data){
      $('#tableReferralOfStudent').html(data);
    });
  });
  $("#contractTableRows").on('click', 'tr', function(){
    var id =  $(this).attr("data-value");
    studentid = id;
    $("#StudentTransactions").modal("show");
    tableLoader('2','tableForStudentUpdates');
    tableLoader('5','tableReferralOfStudent');
    tableLoader('4','contractTableForStudents');
    tableLoader('8','counselingStudentTable');
    $.get('/report/counselingstudent/'+id, function(data){
      $('#counselingStudentTable').html(data);
    });
    $.get('/report/contractstudent/'+id, function(data){
      $('#contractTableForStudents').html(data);
    });
    $.get('/report/studentdata/'+id, function(data){
      $('#nameTextModal').val(data.firstName+ ' '+data.lastName);
      var studenttype = '';
      switch (data.studentType) {
        case 'college':
          studenttype = 'College';
        break;
        case 'seniorhigh':
          studenttype = 'Senior High';
        break;
        case 'juniorhigh':
          studenttype = 'Junior High';
        break;
        case 'elementary':
          studenttype = 'Elementary';
        break;
        default:
          break;
      }
      $('#studentTypeTextModal').val(studenttype);
      $('#courseTextModal').val(data.course);
      var gender = '';
      switch (data.gender) {
        case 'male':
          gender = 'M';
        break;
        case 'female':
          gender = 'F';
        break;
        default:
          break;
      }
      $('#sexTextModal').val(gender);
      $('#homeAddressTextModal').val(data.cityAddress);
    });
    $.get('/report/updatestudentdata/'+id, function(data){
      $('#tableForStudentUpdates').html(data);
    });
    $.get('/report/referralstudents/'+id, function(data){
      $('#tableReferralOfStudent').html(data);
    });
  });
  $("#studentListsTab").on('click', function(){
    $("#counselingFilterContainer").hide();
    $("#contractFilterContainer").hide();
    $("#referralBehaviourContainer").hide();
    $("#counselingCounselorFilterContainer").hide();
  });
  $("#referralListTab").on('click', function(){
    $("#counselingFilterContainer").hide();
    $("#contractFilterContainer").hide();
    $("#referralBehaviourContainer").show();
    $("#counselingCounselorFilterContainer").hide();
  });
  $("#contractListTab").on('click', function(){
    $("#counselingFilterContainer").hide();
    $("#contractFilterContainer").show();
    $("#referralBehaviourContainer").hide();
    $("#counselingCounselorFilterContainer").hide();
  });
  $("#counselingListTab").on('click', function(){
    $("#counselingFilterContainer").show();
    $("#counselingCounselorFilterContainer").show();
    $("#contractFilterContainer").hide();
    $("#referralBehaviourContainer").hide();
  });
}); 
function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}
