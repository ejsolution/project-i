<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use App\model\systemDate;
class contract extends Model
{
    public function setCreatedAtAttribute(){
        $systemdate = systemDate::all();
        if(count($systemdate)>0){
            $this->attributes['created_at'] = $systemdate[0]->systemDate." ".date("h:i:s");
        }
    }
}
