<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSHSStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('s_h_s_students', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('studentID')->unsigned();
            $table->foreign('studentID')->references('id')->on('students');
            $table->string('studNo');
            $table->integer('schoolYearID')->unsigned();
            $table->foreign('schoolYearID')->references('id')->on('school_years');
            $table->integer('semesterID')->unsigned();
            $table->foreign('semesterID')->references('id')->on('semesters');
            $table->integer('seniorHighStrandID')->unsigned();
            $table->foreign('seniorHighStrandID')->references('id')->on('senior_high_strands');
            $table->string('gradeLevel');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('s_h_s_students');
    }
}
