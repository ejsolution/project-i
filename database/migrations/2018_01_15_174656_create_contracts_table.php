<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contracts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('collegStudentID')->unsigned();
            $table->foreign('collegStudentID')->references('id')->on('college_students');
            $table->integer('semesterID')->unsigned();
            $table->foreign('semesterID')->references('id')->on('semesters');
            $table->date('contractDate');
            $table->string('type');
            $table->mediumText('remarks')->nullable();
            $table->string('notedBy');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contracts');
    }
}
