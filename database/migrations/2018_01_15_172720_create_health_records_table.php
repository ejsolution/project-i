<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHealthRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('health_records', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('studentID')->unsigned();
            $table->foreign('studentID')->references('id')->on('students');
            $table->string('height');
            $table->decimal('weight', 8, 2);
            $table->string('complexion')->nullable();
            $table->smallInteger('wearGlasess');
            $table->mediumText('programsParticipated')->nullable();
            $table->string('physicalAilment');
            $table->mediumText('live');
            $table->integer('numPeopleHouse');
            $table->integer('numPeopleRoom');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('health_records');
    }
}
