<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\model\collegeStudent;
use App\model\SHSStudent;
use App\model\JHStudent;
use App\model\elemStudent;

class studentNoRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $temp = collegeStudent::where('studentNo', '=', $value)->count();
        if($temp>0){
            return false;
        }
        $temp = SHSStudent::where('studNo', '=', $value)->count();
        if($temp>0){
            return false;
        }
        $temp = JHStudent::where('studNo', '=', $value)->count();
        if($temp>0){
            return false;
        }
        $temp = elemStudent::where('studNo', '=', $value)->count();
        if($temp>0){
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Student No Already Exists!';
    }
}
