@extends('admin.adminTemplate')

@section('title', 'AdminDashboard')
@section('stylesheet')
	<link href="{{asset('css/admin/reports/reports.css')}}" rel="stylesheet">
@stop
@section('content')
	<div class='container-fluid'>
    <div class='row'>
			<h1>Reports</h1>
		</div>
    <div class='row'>
      <div class='col-md-12'>
        <ul class="nav nav-tabs">
          <li class="active" id = 'studentListsTab'><a data-toggle="tab" href="#studentLists">Student Lists</a></li>
          <li id = 'referralListTab'><a data-toggle="tab" href="#referralList">Referral Lists</a></li>
          <li id = 'contractListTab'><a data-toggle="tab" href="#contractList">Contract Lists</a></li>
          <li id = 'counselingListTab'><a data-toggle="tab" href="#counselingList">Counseling Lists</a></li>
        </ul>
      </div>
    </div>
    <div class='row well'>
      
        <div class='col-md-2'>
          <label for ='studentTypeFilter'>Student Type</label>
          
          
              @if(session()->get('user')['userType']=='admin')
              <select id ='studentTypeFilter' class='form-control'>
                  <option value ='all'>All</option>
                  <option value ='college'>College</option>
                  <option value ='seniorhigh'>Senior High</option>
                  <option value ='juniorhigh'>Junior High</option>
                  <option value ='elementary'>Elementary</option>
              @else
                  @switch($user[0])
                      @case('college')
                          <select id ='studentTypeFilter' class='form-control' readonly>
                              <option value ='college'>College</option>
                      @break
                      @case('seniorhigh')
                          <select id ='studentTypeFilter' class='form-control' readonly>
                              <option value ='seniorhigh'>Senior High</option>
                      @break
                      @case('juniorhigh')
                          <select id ='studentTypeFilter' class='form-control' readonly>
                              <option value ='juniorhigh'>Junior High</option>
                      @break
                      @case('elementary')
                          <select id ='studentTypeFilter' class='form-control' readonly>
                              <option value ='elementary'>Elementary</option>
                      @break
                      @default
                          Default case...
                  @endswitch
              @endif
          </select>
          
      </div>
      <div class='col-md-2'>
          <label for ='schoolYearFilter'>School Year</label>
          <select id ='schoolYearFilter' class='form-control'>
            <option value='all'>all</option>
            @foreach($schoolyear as $year)
              <option value ='{{$year->id}}'>{{explode('-', $year->schoolYearStart)[0]}} - {{explode('-', $year->schoolYearEnd)[0]}}</option>
            @endforeach
              
          </select>
      </div>
      <div class='col-md-2'>
          <label for ='semesterFilter'>Semester</label>
          <select id ='semesterFilter' class='form-control'>
              <option value ='all'>all</option>
          </select>
      </div> 
      <div class='col-md-2'>
          <label for ='courseGradeLevelFilter'>Course/Strand</label>
          @if(session()->get('user')['userType']=='admin')
                      <select id ='courseGradeLevelFilter' class='form-control'>
                        <option value ='all'>all</option>
          @else
              @switch($user[0])
                  @case('college')
                  <select id ='courseGradeLevelFilter' class='form-control'>
                      <option value ='all'>all</option>
                      
                      @foreach($collegeOfUser as $college)
                          <option value = "{{$college->id}}">{{$college->courseName}}</option>
                      @endforeach
                  @break
                  @case('seniorhigh')
                      <select id ='courseGradeLevelFilter' class='form-control' readonly>
                          @foreach($strand as $strands)
                              <option value = "{{$strands->id}}">{{$strands->strandName}}</option>
                          @endforeach
                      </select>
                  {{--  <select id ='courseGradeLevelFilter' class='form-control' readonly>
                      <option value ="{{$user[1]}}">{{$user[1]}}</option>  --}}
                  @break
                  @case('juniorhigh')
                      <select id ='courseGradeLevelFilter' class='form-control' readonly>
                          <option value ="all">all</option>
                  @break
                  @case('elementary')
                      <select id ='courseGradeLevelFilter' class='form-control' readonly>
                          <option value ="all">all</option>
                  @break
                  @default
                      Default case...
              @endswitch
          @endif
          </select>
      </div>
      <div class='col-md-2'>
          <label for ='gradeLevelFilter'>Year Level/Grade Level</label>
          @if(session()->get('user')['userType']=='admin')
                      <select id ='gradeLevelFilter' class='form-control'>
                              <option value ='all'>all</option>
          @else
              @switch($user[0])
                  @case('college')
                  <select id ='gradeLevelFilter' class='form-control'>
                      <option value ='all'>all</option>
                      @for($i=1; $i<$collegeOfUser->courseYear; $i++)
                          <option value ="{{$i}}">{{$i}}</option>
                      @endfor
                  @break
                  @case('seniorhigh')
                      <select id ='gradeLevelFilter' class='form-control' readonly>
                          <option value ="{{$user[1]}}">{{$user[1]}}</option>
                  @break
                  @case('juniorhigh')
                      <select id ='gradeLevelFilter' class='form-control' readonly>
                          <option value ="{{$user[1]}}">{{$user[1]}}</option>
                  @break
                  @case('elementary')
                      <select id ='gradeLevelFilter' class='form-control' readonly>
                          <option value ="{{$user[1]}}">{{$user[1]}}</option>
                  @break
                  @default
                      Default case...
              @endswitch
          @endif
          </select>
      </div>
      <div class='col-md-2' id = 'referralBehaviourContainer'>
        <div>
          <label for = 'referralBehaviourFilter'>Behaviour</label>
          <select id = 'referralBehaviourFilter' class='form-control'>
            
          </select>
        </div>
      </div>
      <div class='col-md-2' id ='contractFilterContainer'>
        <div>
          <label for ='contractTypeFilter'>Contract Type</label>
        </div>
        <div>
          <select id = 'contractTypeFilter' class='form-control'>
            <option value ='all'>all</option>
            <option value ='failing grades'>Failing Grades</option>
            <option value ='absences'>Absences</option>
            <option value ='poor study habits'>Poor Study Habits</option>
            <option value ='illness or health problems'>Illness or Health Problems</option>
            <option value ='evidence of emotional disturbance'>Evidence of Emotional Disturbance</option>
            <option value ='Deviant Behavior'>Deviant Behavior</option>
            <option value ='career/course choice problem'>Career/Course Choice Problem</option>
            <option value ='scholarship/students asst. problem'>Scholarship/Students Asst. Problem</option>
            <option value ='social relationship'>Social Relationship</option>
            <option value ='others'>Others</option>
          </select>
        </div>
      </div>
      <div class='col-md-2' id ='counselingFilterContainer'>
        <div>
          <label for ='counseligStatusFilter'>Counseling Status</label>
        </div>
        <div>
          <select id = 'counseligStatusFilter' class='form-control'>
            <option value = 'all'>all</option>
            <option value = '0'>Pending</option>
            <option value = '1'>Followed Up</option>
            <option value = '2'>Evaluated</option>
          </select>
        </div> 
      </div>
      <div class='col-md-2' id = 'counselingCounselorFilterContainer'>
        <div>
            <label for = 'counselingCounselorFilter'>Counselor</label>
            
        </div>
        <div>
          <select id = 'counselingCounselorFilter' class='form-control'>
            
          </select>
        </div>
      </div>
    </div>
    <div class="tab-content">
      <div  id = 'studentLists' class="tab-pane fade in active">
        <div class='col-md-12 well tableContainer'>
          
          <div class='row'>
            <div class='col-md-12'>
              <table class='table table-condensed'> 
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th>Student No.</th>
                    <th>Student Type</th>
                    <th>Course</th>
                    <th>Year Level/Grade Level</th>
                  </tr>
                </thead>
                <tbody id ='studentTableReport'>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class='row'>
          <div class='col-md-11'>
          </div>
          <div class='col-md-1'>
            <button class='btn btn-dark' id ='printStudents'><i class='fa fa-print'></i>Print</button>
          </div>
        </div>
      </div>
      <div class="tab-pane fade" id = 'referralList'>
        <table class='table table-striped'>
          <thead>
            <tr>
              <th>No</th>
              <th>Student</th>
              <th>Semester</th>
              <th>School Year</th>
              <th>Referral date</th>
              <th>Referred To</th>
              <th>Referred By</th>
            </tr>
          </thead>
          <tbody id ='referralTableRows'>
          </tbody>
        </table>
        <div class='genericDivTop alignRight'>
          {{--  <a class='btn btn-warning' href = "{{route('report.referral.print')}}"><i class='fa fa-print'></i>Print</a>  --}}
          <button class='btn btn-dark' id ='printRefferals'><i class='fa fa-print'></i>Print</button>
        </div>
      </div>
      <div class="tab-pane fade" id = 'contractList'>
        <div class='row'>
          <div class='col-md-12'>
            <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#contractListTable">Contract Lists</a></li>
              <li><a data-toggle="tab" href="#contractRemarksTable">Contract Remarks</a></li>
            </ul>
          </div>
        </div>
        <div class='tab-content'>
          <div class='tab-pane fade in active' id = 'contractListTable'>
            <table class='table table-striped'>
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Student</th>
                    <th>Semester</th>
                    <th>School Year</th>
                    <th>Contract date</th>
                    <th>Type</th>
                    <th>Remarks</th>
                    <th>Noted By</th>
                  </tr>
                </thead>
                <tbody id ='contractTableRows'>
                </tbody>
            </table>
            <div class='genericDivTop alignRight'>
            {{--  <a class='btn btn-warning' href = "{{route('report.contracts.print')}}"><i class='fa fa-print'></i>Print</a>  --}}
              <button class='btn btn-dark' id ='printContracts'><i class='fa fa-print'></i>Print</button>
            </div>
          </div>
          <div class='tab-pane fade' id = 'contractRemarksTable'>
            <table class='table table-striped'>
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Student</th>
                    <th>Remarks</th>
                    <th>Consecutive Contracts</th>
                  </tr>
                </thead>
                <tbody id ='contractRemarksTables'>
                </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="tab-pane fade" id = 'counselingList'>
        <table class='table table-striped'>
            <thead>
              <tr>
                <th>No</th>
                <th>Student</th>
                <th>Semester</th>
                <th>School Year</th>
                <th>Date Scheduled</th>
                <th>Date Recorded</th>
                <th>Status</th>
                <th>Assisted By</th>
                <th>Counselor</th>
              </tr>
            </thead>
            <tbody id ='counselingTableRows'>
            </tbody>
        </table>
        <div class='genericDivTop alignRight'>
          {{--  <a class='btn btn-warning' href = "{{route('report.counselings.print')}}"><i class='fa fa-print'></i>Print</a>  --}}
          <button class='btn btn-dark' id ='printCounseling'><i class='fa fa-print'></i>Print</button>
        </div>
      </div>
    </div>
	</div>
  @include('Partials.Modals.reports.studentTransaction')
@endSection
@section('scripts')
  <script src= '{{asset('js/reports/reports.js')}}'></script>
@stop
