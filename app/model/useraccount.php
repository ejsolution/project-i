<?php

namespace App\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class useraccount extends Authenticatable
{
 protected $guard = 'myuser';
 protected $fillable = [
     'userName', 'email', 'password', 'verify_token', 'userType',
 ];
 protected $table = 'useraccounts';
 /**
  * The attributes that should be hidden for arrays.
  *
  * @var array
  */
 protected $hidden = [
     'password', 'remember_token',
 ];
}
