@component('components.modal')
  @slot('modalid')
    addEventModal
  @endslot
  @slot('modalsize')
    modal-md
  @endslot
  @slot('modaltitle')
    Add Event
  @endslot
  @slot('modalcontent')
  {!! Form::open([
    'id'=>'eventAddForm']) !!}
      <div class='row genericDivBottom'>
        <div class='col-md-12'>
          <label for = 'titleEventText'>
            Title
          </label>
          <input type = 'text' class='form-control' name = 'titleEventText' id = 'titleEventText' required maxlength ='100'>
        </div>
      </div>
      <div class='row genericDivBottom'>
        <div class='col-md-5'>
          <input type = 'date' class='form-control' id = 'dateEventStart' name = 'dateEventStart'  required>
        </div>
        <div class='col-md-2'>
          <center>
            To
          </center>
        </div>
        <div class='col-md-5'>
          <input type = 'date' class='form-control' id = 'dateEventEnd' name = 'dateEventEnd' required>
        </div>
      </div>
      <div class='row genericDivBottom'>
        <div class='col-md-5'>
          <input type = 'time' class='form-control' id = 'timeEventStart' name = 'timeEventStart' required>
        </div>
        <div class='col-md-2'>
          <center>
            To
          </center>
        </div>
        <div class='col-md-5'>
          <input type = 'time' class='form-control' id = 'timeEventEnd' name = 'timeEventEnd' required>
        </div>
      </div>
      <div class='row'>
        <div class='col-md-12'>
          <label for = 'locationEventText'>Location</label>
          <input type = 'text' id = 'locationEventText' name ='locationEventText' class='form-control' required maxlength='100'>
        </div>
      </div>
      <div class='row'>
        <div class='col-md-12'>
          <label for = 'descriptionEventTextArea'>Description</label>
          <textarea class='form-control' rows='4' id = 'descriptionEventTextArea' name = 'descriptionEventTextArea' required maxlength='500'></textarea>
        </div>
      </div>
    {!! Form::close() !!}
  @endslot
  @slot('modalbutton')
      <button class="btn btn-danger" id ='saveEventButton' form ='eventAddForm'><i class='fa fa-save'></i>&nbsp;Save</button>
  @endslot
@endcomponent
@component('components.modal')
  @slot('modalid')
    editEventModal
  @endslot
  @slot('modalsize')
    modal-md
  @endslot
  @slot('modaltitle')
    Edit or Delete an event
  @endslot
  @slot('modalcontent')
  {!! Form::open([
    'id'=>'eventEditForm']) !!}
      <div class='row genericDivBottom'>
        <div class='col-md-12'>
          <label for = 'titleEventText'>
            Title
          </label>
          <input type = 'text' class='form-control' name = 'titleEventText' id = 'titleEventTextEdit' required maxlength ='100'>
        </div>
      </div>
      <div class='row genericDivBottom'>
        <div class='col-md-5'>
          <input type = 'date' class='form-control' id = 'dateEventStartEdit' name = 'dateEventStart'  required>
        </div>
        <div class='col-md-2'>
          <center>
            To
          </center>
        </div>
        <div class='col-md-5'>
          <input type = 'date' class='form-control' id = 'dateEventEndEdit' name = 'dateEventEnd' required>
        </div>
      </div>
      <div class='row genericDivBottom'>
        <div class='col-md-5'>
          <input type = 'time' class='form-control' id = 'timeEventStartEdit' name = 'timeEventStart' required>
        </div>
        <div class='col-md-2'>
          <center>
            To
          </center>
        </div>
        <div class='col-md-5'>
          <input type = 'time' class='form-control' id = 'timeEventEndEdit' name = 'timeEventEnd' required>
        </div>
      </div>
      <div class='row'>
        <div class='col-md-12'>
          <label for = 'locationEventTextEdit'>Location</label>
          <input type = 'text' id = 'locationEventTextEdit' name ='locationEventText' class='form-control' required maxlength='100'>
        </div>
      </div>
      <div class='row'>
        <div class='col-md-12'>
          <label for = 'descriptionEventTextAreaEdit'>Description</label>
          <textarea class='form-control' rows='4' id = 'descriptionEventTextAreaEdit' name = 'descriptionEventTextArea' required maxlength='500'></textarea>
        </div>
      </div>
    {!! Form::close() !!}
  @endslot
  @slot('modalbutton')
      <button class="btn btn-warning" id ='saveEventButton' form ='eventEditForm'><i class='fa fa-save'></i>&nbsp;Save</button>
      <button class="btn btn-danger" id ='deleteEventButtom'><i class='fa fa-trash-o'></i>&nbsp;Delete</button>
  @endslot
@endcomponent

