var totalRow;
var totalPages;
var visiblePages;
var mypage;
var actionReferral;
$(document).ready(function(){
  var action;
  var referralid;
  getAjax('/behaviour/combolist', 'observableBehaviourText');
  $.get('/college/combolist', function(data){
    $("#refferedByUniversityCollege").html(data);
    $("#refferedByUniversityCollegeEdit").html(data);
  });
  $("#courseTH").text("Course/Strand");
  $("#yearLevelTH").text("Year Level/Grade Level");
  $("#addStudentButton").on('click', function(){
    $.get('/referral/defaultcoordinator/', function(data){
      $("#referredToReferralFormText").val(data);
    });
    $("#referralFormModal").modal('show');
  });
  $("#addBehavior").on('click', function(){
    action = 'insert';
    $("#addBehaviourModal").modal('show');
  });  
  $('#addBehaviourModal').on('hidden.bs.modal', function () {
    //$('body').addClass('modal-open');
  });
  $('#studentNameReferralFormText').on('change', function(){
    $.get('/counseling/studentdata/'+$('#studentNameReferralFormText').val(), function(data){
      if(data.studentType == 'college'){
        $('#courseReferralFormText').val(data.course);
      }
      $('#yearLevelReferralFormText').val(data.gradeLevel);
      $('#homeAddressReferralFormText').val(data.cityAddress);      
      $('#contactReferralFormText').val(data.contactNo);      
    });
  });
  $('#behaviorAddForm').on('submit', function(e){
    e.preventDefault();
    if(action =='insert'){
      var html = "<tr data-value = '"+$('#observableBehaviourText').val()+"'>"+
                    "<td>"+$("#observableBehaviourText option:selected").text()+"</td>"+
                    "<td>"+$('#frequencyText').val()+"</td>"+
                    "<td>"+$('#behaviourRemarksText').val()+"</td>"+
                    "<td><button class='btn btn-dark removeBehavior'><i class='fa fa-trash-o'></i></button></td>"+
                  "</tr>";
      $('#behaviorReferralTable').prepend(html);
      $('#addBehaviourModal').modal('hide');
      //$('#observableBehaviourText').val('');
      $('#frequencyText').val('');
      $('#behaviourRemarksText').val('');
    }else{
      var html = "<tr data-value = '"+$('#observableBehaviourText').val()+"'>"+
                    "<td>"+$("#observableBehaviourText option:selected").text() +"</td>"+
                    "<td>"+$('#frequencyText').val()+"</td>"+
                    "<td>"+$('#behaviourRemarksText').val()+"</td>"+
                    "<td><button class='btn btn-dark removeBehavior'><i class='fa fa-trash-o'></i></button></td>"+
                  "</tr>";
      $('#behaviorReferralTableEdit').prepend(html);
      $('#addBehaviourModal').modal('hide');
      //$('#observableBehaviourText').val('');
      $('#frequencyText').val('');
      $('#behaviourRemarksText').val('');
    }
  });
  $('#behaviorReferralTable').on('click', '.removeBehavior', function(){
    $(this).closest('tr').remove();
  });
  $('#behaviorReferralTableEdit').on('click', '.removeBehavior', function(){
    $(this).closest('tr').remove();
  });
  $('#referralAddForm').on('submit', function(e){
    e.preventDefault();
    var form = document.getElementById('referralAddForm');
    var myform = new FormData(form);
    myform.append("collegeName", $("#refferedByUniversityCollege option:selected").text());
    if($('#nameOfStudents').val()==""){
      new PNotify({
        title: 'Error!',
        text: 'Please select a student!',
        type: 'error',
        styling: 'bootstrap3'
      });
      return;
    }
    if($('#referredToReferralFormText').val()==""){
      new PNotify({
        title: 'Error!',
        text: 'Referred to text field is required!',
        type: 'error',
        styling: 'bootstrap3'
      });
      return;
    }
    if($('#studentNameReferralFormText').val()==''){
      new PNotify({
        title: 'Error!',
        text: 'There are still no students saved, please insert some on students page!',
        type: 'error',
        styling: 'bootstrap3'
      });
      return;
    }
    myform.append('studentID', $('#studentNameReferralFormText').val());
    var behaviors=[];
    var i =0;
    var y = 0;
    var behavior;
    var frequency;
    var remarks;
    var observablebehaviour;
    $('#behaviorReferralTable').children('tr').each(function(){
      observablebehaviour = $(this).attr("data-value");
      $(this).children('td').each(function(){
        switch(y){
          case 0:
            behavior = observablebehaviour;
          break;
          case 1:
            frequency= $(this).html();
          break;         
          case 2:
            remarks = $(this).html(); 
          break;
        }
        y++;
      }); 
      behaviors.push({
        behavior:behavior,
        frequency:frequency,
        remarks:remarks
      });
      i++;  
      y=0;
    });
    if(i==0){
      new PNotify({
        title: 'Error!',
        text: 'Please select a behavior for the student!',
        type: 'error',
        styling: 'bootstrap3'
      });
    }
   
    var totalRowsReferral = 0;
    $('#referralsTable').children('tr').each(function(){
      totalRowsReferral++;
    });
    totalRowsReferral++;
    var json_arr = JSON.stringify(behaviors);
    //console.log(json_arr);
    myform.append('behavior', json_arr);
    myform.append('dateReferralFormText', $('#dateReferralFormText').val());
    myform.append('referredToReferralFormText', $('#referredToReferralFormText').val());
    myform.append('rowCounter', totalRowsReferral);
    prependPostAjaxErrorCallback('/referral/insert', myform, function(data){
      new PNotify({
          title: 'Success!',
          text: 'Referral Successfully saved!',
          type: 'success',
          styling: 'bootstrap3' 
      });
      $('#referralsTable').children('h2').each(function(){
        $(this).remove();
      });
      $('#referralsTable').append(data);
      $('#referralFormModal').modal('hide');
      $('#behaviorReferralTable').html("");
      $('#referralFormModal input[type="text"]').val('');
      $('#studentNameReferralFormText').val("");
    });
  }); 
  
  $('#viewByStudent').on('change', function(){
    $('#pagination').twbsPagination('destroy');

    $.get('/referral/count/'+$('#viewByStudent').val()+'/'+$('#searchReferral').val(), function(data){
      initializePages(data);
      $('#pagination').twbsPagination({
          totalPages: totalPages,
          visiblePages: visiblePages,
          onPageClick: function(event, page){
              mypage = page;
              tableLoader('8', 'referralsTable');
              getAjax('/referral/list/'+page+'/'+$('#viewByStudent').val()+'/'+$('#searchReferral').val(), 'referralsTable');
          }
      });
      //console.log(data);
    });
  });
  $('#addBehaviorEdit').on('click', function(){
    action = 'update';
    $("#addBehaviourModal").modal('show');
  });
  $('#referralsTable').on('click', '.viewReferral', function(){
    referralid = $(this).prop('id').split('-')[1];
    var studentid = $(this).attr('data-value');
    $('#studentNameReferralFormTextEdit').val(studentid);
    $.get('/counseling/studentdata/'+studentid, function(data){
      if(data.studentType == 'college'){
        $('#courseReferralFormTextEdit').val(data.course);
      }
      
      $('#yearLevelReferralFormTextEdit').val(data.gradeLevel);
      $('#homeAddressReferralFormTextEdit').val(data.cityAddress);      
      $('#contactReferralFormTextEdit').val(data.contactNo);  
     
     
    });
    $.get('/referral/data/'+referralid, function(data){
     
      $('#referralFormModalEdit').modal('show');
      $('#dateReferralFormTextEdit').val(data.referralDate);
      $('#behaviorReferralTableEdit').html(data.html);
      $('#referredToReferralFormTextEdit').val(data.referredTo);
      $('#referredByReferralFormTextEdit').val(data.referredBy);
      $('#semesterReferralFormTextEdit').val(data.semesterID);    
      $('#schoolYearReferralFormTextEdit').val(data.schoolYear);   
      $('#refferedByUniversityCollegeEdit').val(data.universityCollegeID);    
    });
    
  });
  $('#studentNameReferralFormTextEdit').on('change', function(){
    $.get('/counseling/studentdata/'+$('#studentNameReferralFormTextEdit').val(), function(data){
      if(data.studentType == 'college'){
        $('#courseReferralFormTextEdit').val(data.course);
      }
      $('#yearLevelReferralFormTextEdit').val(data.gradeLevel);
      $('#homeAddressReferralFormTextEdit').val(data.cityAddress);      
      $('#contactReferralFormTextEdit').val(data.contactNo);      
    });
  });
  $('#referralEditForm').on('submit', function(e){
    e.preventDefault();
    var form = document.getElementById('referralEditForm');
    var myform = new FormData(form);
    myform.append('id', referralid);
    myform.append("collegeName", $("#refferedByUniversityCollegeEdit option:selected").text());
    if($('#studentNameReferralFormTextEdit').val()==''){
      new PNotify({
        title: 'Error!',
        text: 'There are still no students saved, please insert some on students page!',
        type: 'error',
        styling: 'bootstrap3'
      });
      return;
    }
    myform.append('studentID', $('#studentNameReferralFormTextEdit').val());
    var behaviors=[];
    var i =0;
    var y = 0;
    var behavior;
    var frequency;
    var remarks;
    var observablebehaviour;
    $('#behaviorReferralTableEdit').children('tr').each(function(){
      observablebehaviour = $(this).attr("data-value");
      $(this).children('td').each(function(){
        switch(y){
          case 0:
            behavior = observablebehaviour;
          break;
          case 1:
            frequency= $(this).html();
          break;         
          case 2:
            remarks = $(this).html(); 
          break;
        }
        y++;
      });
      behaviors.push({
        behavior:behavior,
        frequency:frequency,
        remarks:remarks
      });
      i++;  
      y=0;
    });
    if(i==0){
      new PNotify({
        title: 'Error!',
        text: 'Please select a behavior for the student!',
        type: 'error',
        styling: 'bootstrap3'
      });
      return;
    }
    var referralRowCounter = 0;
    $('#referralRow-'+referralid).children('td').each(function(){
      referralRowCounter = $(this).html();
      return false;
    });
    var json_arr = JSON.stringify(behaviors);
    //console.log(json_arr); 
    myform.append('behavior', json_arr);
    myform.append('rowCounter', referralRowCounter);
    prependPostAjaxErrorCallback('/referral/update', myform, function(data){
      new PNotify({
          title: 'Success!',
          text: 'Referral Successfully saved!',
          type: 'success',
          styling: 'bootstrap3'
      });
      data = data.replace("<tr id ='referralRow-"+referralid+"'>", "");
      data =  data.replace("</tr>", "");
      $('#referralRow-'+referralid).html(data);
      $('#referralFormModal').modal('hide');
    });
  });


  $('#chooseStudent').on('click', function(){
    actionReferral = "add";
    $('#browseStudentModal').modal('show');
    $('#browseStudentModal .modal-title').text("Select a student for referral");
    tableLoader('7','studentTableReport');
    $.get('/contract/studentsbrowse/all/all/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val()+'/'+$('#searchBrowseStudents').val(), function(data){
      $('#studentTableReport').html(data);
    });
    
  });
  $('#viewByStudentType').on('change', function(){
    if($('#viewByStudentType').val() == 'all'){
      $('#Course_GradeLevel').val('all');
      $('#Course_GradeLevel').attr('disabled', true);
      tableLoader('5','studentTableReport');

      $.get('/contract/studentsbrowse/all/all/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val()+'/'+$('#searchBrowseStudents').val(), function(data){
        $('#studentTableReport').html(data);
      });
    }else{
      $('#Course_GradeLevel').attr('disabled', false);
      switch ($('#viewByStudentType').val()) {
        case 'college':
        tableLoader('7','studentTableReport');
          $.get('/report/college/course', function(data){
            $('#Course_GradeLevel').html("<option value='all'>all</option>");
            $('#Course_GradeLevel').append(data);
            $.get('/contract/studentsbrowse/college/'+$('#Course_GradeLevel').val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val()+'/'+$('#searchBrowseStudents').val(), function(data){
              $('#studentTableReport').html(data);
            });
          });
        break;
        case 'seniorhigh':
          var html = "<option value ='all'>all</option>"+
                      "<option value ='12'>12</option>"+
                      "<option value ='11'>11</option>";
          $('#Course_GradeLevel').html(html);
          tableLoader('7','studentTableReport');
          
          $.get('/contract/studentsbrowse/seniorhigh/'+$('#Course_GradeLevel').val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val()+'/'+$('#searchBrowseStudents').val(), function(data){
            $('#studentTableReport').html(data);
          });
        break;
        case 'juniorhigh':
          var html = "<option value ='all'>all</option>"+
                      "<option value ='10'>10</option>"+
                      "<option value ='9'>9</option>"+
                      "<option value ='8'>8</option>"+
                      "<option value ='7'>7</option>";
          $('#Course_GradeLevel').html(html);
          tableLoader('7','studentTableReport');
          $.get('/contract/studentsbrowse/juniorhigh/'+$('#Course_GradeLevel').val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val()+'/'+$('#searchBrowseStudents').val(), function(data){
            $('#studentTableReport').html(data);
          });
        break;
        case 'elementary':
          var html = "<option value ='all'>all</option>"+
                  "<option value ='6'>6</option>"+
                  "<option value ='5'>5</option>"+
                  "<option value ='4'>4</option>"+
                  "<option value ='3'>3</option>"+
                  "<option value ='2'>2</option>"+
                  "<option value ='1'>1</option>";
          $('#Course_GradeLevel').html(html);
          tableLoader('7','studentTableReport');
          $.get('/contract/studentsbrowse/elementary/'+$('#Course_GradeLevel').val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val()+'/'+$('#searchBrowseStudents').val(), function(data){
            $('#studentTableReport').html(data);
          });
        break;
        default:
          break;
      }
    }  
  });
  $('#Course_GradeLevel').on('change', function(){
    tableLoader('7','studentTableReport');
    $.get('/contract/studentsbrowse/'+$('#viewByStudentType').val()+'/'+$(this).val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val()+'/'+$('#searchBrowseStudents').val(), function(data){
      $('#studentTableReport').html(data);
    });
  });
  $('#schoolYearStudent').on('change', function(){
    tableLoader('7','studentTableReport');
    if($('#schoolYearStudent').val() != "all"){
      $.get('/report/schoolyear/'+$('#schoolYearStudent').val(), function(data){
        $('#semesterStudent').html("<option value='all'>all</option>");
        $('#semesterStudent').append(data);
        $.get('/contract/studentsbrowse/'+$('#viewByStudentType').val()+'/'+$('#Course_GradeLevel').val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val()+'/'+$('#searchBrowseStudents').val(), function(data){
          $('#studentTableReport').html(data);
        });
      });
    }else{
      $.get('/contract/studentsbrowse/'+$('#viewByStudentType').val()+'/'+$('#Course_GradeLevel').val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val()+'/'+$('#searchBrowseStudents').val(), function(data){
        $('#studentTableReport').html(data);
      });
    }
  });
  $('#semesterStudent').on('change', function(){
    tableLoader('5','studentTableReport');
    $.get('/contract/studentsbrowse/'+$('#viewByStudentType').val()+'/'+$('#Course_GradeLevel').val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val()+'/'+$('#searchBrowseStudents').val(), function(data){
      $('#studentTableReport').html(data);
    });
  });
  $('#searchBrowseStudents').on('keydown', function(){
    tableLoader('7','studentTableReport');
    $.get('/contract/studentsbrowse/college/'+$('#Course_GradeLevel').val()+'/'+$('#schoolYearStudent').val()+'/'+$('#semesterStudent').val()+'/'+$('#searchBrowseStudents').val(), function(data){
      $('#studentTableReport').html(data);
    });
  });
  $('#studentTableReport').on('click', '.selectStudent', function(){
    if(actionReferral == "add"){
      var $id = $(this).prop("id").split('-')[1];
      $('#studentNameReferralFormText').val($id);
      var temp =[];
      var i =0;
      $(this).closest('tr').children("td").each(function(){
        temp[i] = $(this).html();
        i++;
      });
      $('#nameOfStudents').val(temp[1]);
      $.get('/counseling/studentdata/'+$id, function(data){
        //console.log(data);
        if(data.studentType == 'college'){
          $('#courseReferralFormText').val(data.course);
        }
        $('#yearLevelReferralFormText').val(data.gradeLevel);
        $('#homeAddressReferralFormText').val(data.cityAddress);      
        $('#contactReferralFormText').val(data.contactNo);      
      });
      
      $('#browseStudentModal').modal('hide');
    }else{
      var temp =[];
      var i =0;
      $(this).closest('tr').children("td").each(function(){
        temp[i] = $(this).html();
        i++;
      });
      $('#referredToReferralFormText').val(temp[1]);
      $('#browseStudentModal').modal('hide');
    }
  });
  $('#chooseStudentReferredTo').on('click', function(){
    $('#selectCoordinatorModal').modal('show');
    tableLoader('5', 'selectCoordinatorsTable');
    $.get('/referral/getcoordinators', function(data){
      $('#selectCoordinatorsTable').html(data);
    });
  });
  $('#selectCoordinatorsTable').on('click', '.selectCoordinator', function() {
    var i =0;
    $(this).closest('tr').children('td').each(function(){
      if(i ==0){
        $('#referredToReferralFormText').val($(this).html());
      }
      i++;
    });
    $('#selectCoordinatorModal').modal('hide');
  });

  $('#studentTypeFilter').on('change', function(){
    if($('#studentTypeFilter').val() == 'all'){
        $('#courseGradeLevelFilter').val('all');
        $('#courseGradeLevelFilter').attr('disabled', true);
        tableLoader('8', 'referralsTable');
        $('#pagination').twbsPagination('destroy');
        $.get('/referral/list/1/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/true/'+$('#searchReferral').val(), function(data){
          initializePages(data);
          $('#pagination').twbsPagination({
              totalPages: totalPages,
              visiblePages: visiblePages,
              onPageClick: function(event, page){
                  mypage = page;
                  getAjax('/referral/list/'+page+'/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/false/'+$('#searchReferral').val(), 'referralsTable');
              }
          });
        });
      }else{
        $('#courseGradeLevelFilter').attr('disabled', false);
        switch ($('#studentTypeFilter').val()) {
          case 'college':
            $.get('/report/college/course', function(data){
                $('#courseGradeLevelFilter').html("<option value='all'>all</option>");
                $('#courseGradeLevelFilter').append(data);
                tableLoader('8', 'referralsTable');
                $('#pagination').twbsPagination('destroy');
                $.get('/referral/list/1/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/true/'+$('#searchReferral').val(), function(data){
                  initializePages(data);
                  $('#pagination').twbsPagination({
                      totalPages: totalPages,
                      visiblePages: visiblePages,
                      onPageClick: function(event, page){
                          mypage = page;
                          getAjax('/referral/list/'+page+'/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/false/'+$('#searchReferral').val(), 'referralsTable');
                      }
                  });
                });
            });
          break;
          case 'seniorhigh':
            var html = "<option value ='all'>all</option>"+
                        "<option value ='12'>12</option>"+
                        "<option value ='11'>11</option>";
            $('#courseGradeLevelFilter').html(html);
            tableLoader('8', 'referralsTable');
            $('#pagination').twbsPagination('destroy');
            $.get('/referral/list/1/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/true/'+$('#searchReferral').val(), function(data){
              initializePages(data);
              $('#pagination').twbsPagination({
                  totalPages: totalPages,
                  visiblePages: visiblePages,
                  onPageClick: function(event, page){
                      mypage = page;
                      getAjax('/referral/list/'+page+'/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/false/'+$('#searchReferral').val(), 'referralsTable');
                  }
              });
            });
          break;
          case 'juniorhigh':
            var html = "<option value ='all'>all</option>"+
                        "<option value ='10'>10</option>"+
                        "<option value ='9'>9</option>"+
                        "<option value ='8'>8</option>"+
                        "<option value ='7'>7</option>";
            $('#courseGradeLevelFilter').html(html);
            tableLoader('8', 'referralsTable');
            $('#pagination').twbsPagination('destroy');
            $.get('/referral/list/1/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/true/'+$('#searchReferral').val(), function(data){
              initializePages(data);
              $('#pagination').twbsPagination({
                  totalPages: totalPages,
                  visiblePages: visiblePages,
                  onPageClick: function(event, page){
                      mypage = page;
                      getAjax('/referral/list/'+page+'/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/false/'+$('#searchReferral').val(), 'referralsTable');
                  }
              });
            });
          break;
          case 'elementary':
            var html = "<option value ='all'>all</option>"+
                    "<option value ='6'>6</option>"+
                    "<option value ='5'>5</option>"+
                    "<option value ='4'>4</option>"+
                    "<option value ='3'>3</option>"+
                    "<option value ='2'>2</option>"+
                    "<option value ='1'>1</option>";
            $('#courseGradeLevelFilter').html(html);
            tableLoader('8', 'referralsTable');
            $('#pagination').twbsPagination('destroy');
            $.get('/referral/list/1/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/true/'+$('#searchReferral').val(), function(data){
              initializePages(data);
              $('#pagination').twbsPagination({
                  totalPages: totalPages,
                  visiblePages: visiblePages,
                  onPageClick: function(event, page){
                      mypage = page;
                      getAjax('/referral/list/'+page+'/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/false/'+$('#searchReferral').val(), 'referralsTable');
                  }
              });
            });
          break;
          default:
            break;
        }
    }  
    
  });
  $('#schoolYearFilter').on('change', function(){
    if($('#schoolYearFilter').val() != "all"){
      $.get('/report/schoolyear/'+$('#schoolYearFilter').val(), function(data){
        $('#semesterFilter').html("<option value='all'>all</option>");
        $('#semesterFilter').append(data);
        tableLoader('8', 'referralsTable');
        $('#pagination').twbsPagination('destroy');
        $.get('/referral/list/1/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/true/'+$('#searchReferral').val(), function(data){
          initializePages(data);
          $('#pagination').twbsPagination({
              totalPages: totalPages,
              visiblePages: visiblePages,
              onPageClick: function(event, page){
                  mypage = page;
                  getAjax('/referral/list/'+page+'/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/false/'+$('#searchReferral').val(), 'referralsTable');
              }
          });
        });
      });
    }else{
      tableLoader('8', 'referralsTable');
      $('#pagination').twbsPagination('destroy');
      $.get('/referral/list/1/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/true/'+$('#searchReferral').val(), function(data){
        initializePages(data);
        $('#pagination').twbsPagination({
            totalPages: totalPages,
            visiblePages: visiblePages,
            onPageClick: function(event, page){
                mypage = page;
                getAjax('/referral/list/'+page+'/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/false/'+$('#searchReferral').val(), 'referralsTable');
            }
        });
      });
    }
  });
  $('#semesterFilter').on('change', function(){
    tableLoader('8', 'referralsTable');
    $('#pagination').twbsPagination('destroy');
    $.get('/referral/list/1/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/true/'+$('#searchReferral').val(), function(data){
      initializePages(data);
      $('#pagination').twbsPagination({
          totalPages: totalPages,
          visiblePages: visiblePages,
          onPageClick: function(event, page){
              mypage = page;
              getAjax('/referral/list/'+page+'/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/false/'+$('#searchReferral').val(), 'referralsTable');
          }
      });
    });
  });
  $('#courseGradeLevelFilter').on('change', function(){
    tableLoader('8', 'referralsTable');
    $('#pagination').twbsPagination('destroy');
    $.get('/referral/list/1/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/true/'+$('#searchReferral').val(), function(data){
      initializePages(data);
      $('#pagination').twbsPagination({
          totalPages: totalPages,
          visiblePages: visiblePages,
          onPageClick: function(event, page){
              mypage = page;
              getAjax('/referral/list/'+page+'/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/false/'+$('#searchReferral').val(), 'referralsTable');
          }
      });
    });
  });
  $('#searchReferral').on('keydown', function(){
    tableLoader('8', 'referralsTable');
    $('#pagination').twbsPagination('destroy');
    $.get('/referral/list/1/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/true/'+$('#searchReferral').val(), function(data){
      initializePages(data);
      $('#pagination').twbsPagination({
          totalPages: totalPages,
          visiblePages: visiblePages,
          onPageClick: function(event, page){
              mypage = page;
              getAjax('/referral/list/'+page+'/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/false/'+$('#searchReferral').val(), 'referralsTable');
          }
      });
    });
  });
  $('#referralsTable').on('click', '.messageParentsReferral', function(){
    $('#referralMessage').modal("show");
    tableLoader('4', 'familyTableOfStudents');
    $.get('/referral/familyofstudent/'+$(this).attr('data-value'), function(data){
      $('#familyTableOfStudents').html(data);
    });
    referralid = $(this).prop('id').split('-')[1];
    $.get('/referral/messagelogs/'+$(this).prop('id').split('-')[1], function(data){
      $('#referralSentMessagesTable').html(data);
    });
  });  
  $('#sendMessageReferral').on('submit', function(e){
    e.preventDefault();
    var i=0;
    var guardians = [];
    $('.checkFamily:checkbox:checked').each(function () {
      guardians[i] = $(this).val();
      i++; 
    });
    if(i==0 && !$(".checkStudent").prop("checked")){
      new PNotify({
        title: 'Error!',
        text: 'Please select a contact to send this message to.',
        type: 'error',
        styling: 'bootstrap3'
      });
      return;
    }
    
    var mydata = document.getElementById("sendMessageReferral");
   
    var x=0;
    $('#statusMessageModal').modal('show');
    divLoader('messageLoader');
    new Promise((resolve, reject) => {
      for(var y=0; y<i; y++){
        var formdata = new FormData(mydata);
        formdata.append("family", guardians[y]);
        formdata.append("referralID", referralid);
        prependPostAjaxErrorCallback('/referral/message', formdata, function(data){
          new PNotify({
              title: 'Success!',
              text: 'Message Sent to '+data.name,
              type: 'success',
              styling: 'bootstrap3'
          });
          var mymessage = '';
          if(data.message.length>30){
            mymessage = data.message.substring(0,30)+"...";;
          }else{
            mymessage = data.message;
          }
          var row = "<tr><td>"+data.name+"</td><td>"+data.contactNo+"</td><td>"+mymessage+"</td></tr>";
          //console.log(row);
          $('#referralSentMessagesTable').children("h2").each(function(){
            $(this).remove();
          });
          $('#referralSentMessagesTable').append(row);
          x++;
          if(x==i){
            resolve(true);
          }
        });
      }
      if($(".checkStudent").prop("checked")){
        var formdata = new FormData(mydata);
        formdata.append("studentID", $(".checkStudent").val());
        formdata.append("referralID", referralid);
        prependPostAjaxErrorCallback('/referral/message', formdata, function(data){
          new PNotify({
              title: 'Success!',
              text: 'Message Sent to '+data.name,
              type: 'success',
              styling: 'bootstrap3'
          });
          var mymessage = '';
          if(data.message.length>30){
            mymessage = data.message.substring(0,30)+"...";;
          }else{
            mymessage = data.message;
          }
          var row = "<tr><td>"+data.name+"</td><td>"+data.contactNo+"</td><td>"+mymessage+"</td></tr>";
          //console.log(row);
          $('#referralSentMessagesTable').children("h2").each(function(){
            $(this).remove();
          });
          $('#referralSentMessagesTable').append(row);
          x++;
         
            resolve(true);
          
        });
      }
    }).then((resolve)=>{
      $('#messageStatus').html("<h2>All messages sent!</h2>");
      $('#messageLoader').html("");
      $('#sendMessageReferral textarea').val("");
    });
  });
  $('#statusMessageModal').on('hidden.bs.modal', function(){
    $('#messageStatus').html("<h2>Message is now sending. Please wait, while we send all the messages. This may take a few minutes.</h2>");
  }); 
  $('#printReferralStudents').on('click', function(){
    $.get('/reportreferral/print/'+$('#studentTypeFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#searchReferral').val(), function(data){
      if(data=="true"){
        window.location.replace("/temp/report.pdf");
      }
    });
  });
});
tableLoader('8', 'referralsTable');
$.get('/referral/list/1/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/true/'+$('#searchReferral').val(), function(data){
  initializePages(data);
  $('#pagination').twbsPagination({
      totalPages: totalPages,
      visiblePages: visiblePages,
      onPageClick: function(event, page){
          mypage = page;
          getAjax('/referral/list/'+page+'/'+$('#studentTypeFilter').val()+'/'+$('#schoolYearFilter').val()+'/'+$('#semesterFilter').val()+'/'+$('#courseGradeLevelFilter').val()+'/false/'+$('#searchReferral').val(), 'referralsTable');
      }
  });
});
function initializePages(data){
  totalRow = data;
  if(totalRow<=5){
      totalPages = 1;
  }else{
      var remainder = totalRow%10;
      if(remainder >0){
          totalPages = ((totalRow-remainder)/10)+1;
      }else{
          totalPages = totalRow/10;
      }
      if(totalPages<=5){
          visiblePages =totalPages;
      }else{
          visiblePages = 5;
      }    
  }
}

