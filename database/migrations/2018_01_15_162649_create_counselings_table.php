<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCounselingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('counselings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('studentID')->unsigned();
            $table->foreign('studentID')->references('id')->on('students');
            $table->integer('schoolYearID')->unsigned();
            $table->foreign('schoolYearID')->references('id')->on('school_years');
            $table->integer('semesterID')->unsigned();
            $table->foreign('semesterID')->references('id')->on('semesters');
            $table->string('assistedBy');
            $table->string('counselingType');
            $table->date('dateRecorded');
            $table->date('dateScheduled');
            $table->string('counselingStatus');
            $table->smallInteger('walkin');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('counselings');
    }
}
