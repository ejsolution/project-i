var prefixUrl = 'localhost:8000/';
var mydelete;
var globalUserId;
var globalfamilyid;
var globalschoolbackgroundid;
var globalfavsubjid;
var globaldislikedsubjid;
var globalleisureid;
var globalleisurestudentid;
var globalhealthrecordid;
var globalmakeupassistanceid;
var globalcontractproofid;
var globaleventid;
function getAjax(url, tableid){
  $.get(url, function(data){
    $('#'+tableid).html(data);
  });
}
function prependPostAjax(url, formdata, element, error, success){
  $.ajax({
     url: url, // Url to which the request is send
     type: 'POST',             // Type of request to be send, called as method
     data: formdata, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
     contentType: false,       // The content type used when sending data to the server.
     cache: false,             // To unable request pages to be cached
     processData:false,        // To send DOMDocument or non processed data file it is set to false
     success: function(data)   // A function to be called if request succeeds
     {
        $(element).prepend(data);
        new PNotify({
                    title: 'Success!',
                    text: success,
                    type: 'success',
                    styling: 'bootstrap3'
                });
        $(element+" h2").remove();
          //console.log(data);
     },
     statusCode: {
    422: function() {
      new PNotify({
                  title: 'Error!',
                  text: error,
                  type: 'error',
                  styling: 'bootstrap3'
              });
    },
    404: function() {
      new PNotify({
                  title: 'Error!',
                  text: 'Request not found',
                  type: 'error',
                  styling: 'bootstrap3'
              });
    },
    500: function(data){
      console.log(data);
    }
  }
 });
}
function prependPostAjaxErrorCallback(url, formdata, callback){
  $.ajax({
     url: url, // Url to which the request is send
     type: 'POST',             // Type of request to be send, called as method
     data: formdata, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
     contentType: false,       // The content type used when sending data to the server.
     cache: false,             // To unable request pages to be cached
     processData:false,        // To send DOMDocument or non processed data file it is set to false
     success: function(data)   // A function to be called if request succeeds
     {
          callback(data);
    
    },
    statusCode: {
      422: function(data) {
        //callback(data);
        for(var error in data.responseJSON.errors){
          if (data.responseJSON.errors.hasOwnProperty(error)) {
            new PNotify({
              title: 'Error!',
              text: data.responseJSON.errors[error][0],
              type: 'error',
              styling: 'bootstrap3'
            });
          } 
        }
      },
      500: function(data){
        console.log(data);
      },
      419: function(data){
        console.log(data);
      }
    }
 });
}
function prependPostAjaxCallbackElement(url, formdata, element, error, success, callback){
  $.ajax({
     url: url, // Url to which the request is send
     type: 'POST',             // Type of request to be send, called as method
     data: formdata, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
     contentType: false,       // The content type used when sending data to the server.
     cache: false,             // To unable request pages to be cached
     processData:false,        // To send DOMDocument or non processed data file it is set to false
     success: function(data)   // A function to be called if request succeeds
     {
          $(element+" h2").remove();
          $(element).prepend(data);
          new PNotify({
                      title: 'Success!',
                      text: success,
                      type: 'success',
                      styling: 'bootstrap3'
                  });
          //console.log(data);
          callback(true);
     },
     statusCode: {
    422: function() {
      new PNotify({
                  title: 'Error!',
                  text: error,
                  type: 'error',
                  styling: 'bootstrap3'
              });
      callback(false);
    },
    404: function() {
      new PNotify({
                  title: 'Error!',
                  text: 'Request not found',
                  type: 'error',
                  styling: 'bootstrap3'
              });
      callback(false);
    }
  }
 });
}
function prependPostAjaxCallback(url, formdata, error, success, callback){
  $.ajax({
     url: url, // Url to which the request is send
     type: 'POST',             // Type of request to be send, called as method
     data: formdata, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
     contentType: false,       // The content type used when sending data to the server.
     cache: false,             // To unable request pages to be cached
     processData:false,        // To send DOMDocument or non processed data file it is set to false
     success: function(data)   // A function to be called if request succeeds
     {
          //$(element).prepend(data);
          new PNotify({
                      title: 'Success!',
                      text: success,
                      type: 'success',
                      styling: 'bootstrap3'
                  });
          //console.log(data);
          callback(true);
     },
     statusCode: {
    422: function(data) {
          for(var error in data.responseJSON.errors){
            if (data.responseJSON.errors.hasOwnProperty(error)) {
              new PNotify({
                title: 'Error!',
                text: data.responseJSON.errors[error][0],
                type: 'error',
                styling: 'bootstrap3'
              });
            } 
          }
          callback(false);
    },
    404: function() {
      new PNotify({
                  title: 'Error!',
                  text: 'Request not found',
                  type: 'error',
                  styling: 'bootstrap3'
              });
          callback(false);
    },
    500:(data)=>{
      new PNotify({
                  title: 'Error!',
                  text: 'Something went wrong. please try again later.',
                  type: 'error',
                  styling: 'bootstrap3'
              });
          console.log(data);
          callback(false);

    }
  }
 });
}
function tableLoader(colspan, tableid){
  var html = "<td colspan='"+colspan+"'>"+
            "<div class='cssload-thecube'>"+
          	"<div class='cssload-cube cssload-c1'></div>"+
          	"<div class='cssload-cube cssload-c2'></div>"+
          	"<div class='cssload-cube cssload-c4'></div>"+
          	"<div class='cssload-cube cssload-c3'></div>"+
          "</div></td>";
  $('#'+tableid).html(html);
}
function divLoader(divid){
  var html = "<center>"+
            "<div class='cssload-thecube'>"+
          	"<div class='cssload-cube cssload-c1'></div>"+
          	"<div class='cssload-cube cssload-c2'></div>"+
          	"<div class='cssload-cube cssload-c4'></div>"+
          	"<div class='cssload-cube cssload-c3'></div>"+
          "</div></center>";
  $('#'+divid).html(html);
}

$(document).ready(function(){
  var editCollegeButton;
  var college={code:'', name:'', description:''};
  var editDepartmentButton;
  var department = {name:'', code:''}
  var course = {name:'', code:'', year:''}
  var departmentid;
  var editCourseButton;
  var collegeid;
  var courseid;
  var editStrandButton;
  var strand = {name:'', code:'', description:''};
  var strandid;
  var subjectid;
  var editSubjectButton;
  var subject ={name:'', code:'', description:''};
  var editPersonalityButton;
  var personality = {name:'', description:''};
  var personalityid; 
  var behaviorid;
  var behaviourObj = {name:'', description:''};
  var counselorid;
  var counselorObj = {firstName:'',lastName:'', designation:'', counselorType:''};
  //var counselorHtml;
  tableLoader('6', 'collegeTableBody');
  tableLoader('5', 'seniorHighTable');
  tableLoader('5', 'subjectTable');
  tableLoader('4', 'personalityTable');
  tableLoader('4', 'behaviourTable');
  tableLoader('5', 'counselorTable');
  getAjax('/personality/list', 'personalityTable');
  getAjax('/behaviour/list', 'behaviourTable');
  getAjax('/subject/list', 'subjectTable');
  getAjax('/seniorhigh/list', 'seniorHighTable');
  getAjax('/college/list', 'collegeTableBody');
  getAjax('/college/combolist', 'collegeComboBox');
  getAjax('/counselor/list', 'counselorTable');
  $("#counselorDesignation").attr("disabled", true);
  $.get('/college/combolist', function(data){
    var html = "<option value = 'seniorhigh'>Senior High</option>"+
               "<option value = 'juniorhigh'>Junior High</option>"+
               "<option value ='elementary'>Elementary</option>";
    $("#counselorDesignation").html(html+data);
  });
  $("#collegeAddForm").on('submit', function(e){
    e.preventDefault();
    var form = document.getElementById('collegeAddForm');
    var myform = new FormData(form);
    new Promise((res)=>{
      prependPostAjaxCallbackElement("/college/insert", myform, '#collegeTableBody', 'College Code Already Exist!',
      'College Successfully Saved!', function(data){
          if(data){
            res(true);
          }
      });
    }).then((res)=>{
      $('#collegeTableBody').children('tr').each(function () {
          //console.log("Hello");
          var rowid = $(this).prop('id');
          var collegeName;
          var i=0;
          $('#'+rowid).children('td').each(function () {
              //console.log("Hi");
            if(i==1){
              collegeName = $(this).text();
              $('#collegeComboBox').append("<option value = '"+rowid.split('-')[1]+"'>"+collegeName+"</option>")
              return false;
            }
            i++;
          });
        return false;
      });
      $("#collegeAddForm input[type = 'text']").val("");
      $("#collegeAddForm textarea").val("");
    });


  });
  $("#collegeDepartmentForm").on('submit', function(e){
    e.preventDefault();
    var myform = document.getElementById('collegeDepartmentForm');
    var departmentForm = new FormData(myform);
    prependPostAjax("/college/department/insert", departmentForm, '#departmentsTable', 'Department Code should be unique.',
    'Department Successfully Saved!');
    $("#collegeDepartmentForm input[type = 'text']").val("");
  });
  $('#collegeEditForm').on('submit', function(e){
    e.preventDefault();
    $('#'+editCollegeButton).html("<i class='fa fa-edit'></i>");
    $('#'+editCollegeButton).attr("form", '');
    var myform = document.getElementById('collegeEditForm');
    var editform = new FormData(myform);
    editform.append('collegeCodeText', $('#editCollege-0').val());
    editform.append('collegeNameText', $('#editCollege-1').val());
    editform.append('collegeDescription', $('#editCollege-2').val());
    editform.append('id', editCollegeButton.split('-')[1]);
    var i=0;
    new Promise((res, rej)=>{
      prependPostAjaxCallback("/college/update", editform, 'College Code Already Exist!',
      'College Successfully Saved!', function(value){
        if(value){
          res(true);
        }else{
          rej(false);
        }
      });
    }).then((res)=>{
      $('#college-'+editCollegeButton.split('-')[1]).children('td').each(function () {
        if(i>2){
          return;
        }
        switch (i) {
          case 0:
          //alert("Hello");
            $(this).html($('#editCollege-0').val());
            break;
          case 1:
            $(this).html($('#editCollege-1').val());
            break;
          case 2:
            $(this).html($('#editCollege-2').val());
            break;
          default:
        }
        i++;
      });
    }).catch((err)=>{
      $('#college-'+editCollegeButton.split('-')[1]).children('td').each(function () {
        if(i>2){
          return;
        }
        switch (i) {
          case 0:
          //alert("Hello");
            $(this).html(college.code);
            break;
          case 1:
            $(this).html(college.name);
            break;
          case 2:
            $(this).html(college.description);
            break;
          default:
        }
        i++;
      });
    });
  });
  $('#collegeTableBody').on('click', '.editCollege',function(){

    var id =  $(this).prop('id');
    editCollegeButton = id;
    $('.editCollege').each(function(i, obj) {
       if(id!=$(this).prop('id')){
         $(this).attr('form', '');
         $(this).html("<i class ='fa fa-edit'></i>");
         //console.log("hi");
       }
    });
    $('#collegeTableBody').children('tr').each(function (){
      if(editCollegeButton.split('-')[1] == $(this).prop('id').split('-')[1]){
        return;
      }else{
        $(this).children('td').each(function(){
          var that = this;
          $(this).children('input').each(function(){
            $(that).html($(this).val());
          });
        });
      }
    });
    //$('#'+id).html("save");
    if($('#'+id).text()!='save'){
      //console.log("hez");
        var i =0;
        var temp;
        $('#college-'+id.split('-')[1]).children('td').each(function () {
          if(i>2){
            return;
          }else{
            temp = $(this).text();
            switch (i) {
              case 0:
                $(this).html("<input type='text' required maxlength='5' class='form-control' id = 'editCollege-"+i+"' value = '"+temp+"' form='collegeEditForm'>");
                college.code = temp;
                break;
              case 1:
                $(this).html("<input type='text' required maxlength='50' class='form-control' id = 'editCollege-"+i+"' value = '"+temp+"' form='collegeEditForm'>");
                college.name = temp;
                break;
              case 2:
                $(this).html("<input type='text' required maxlength='250' class='form-control' id = 'editCollege-"+i+"' value = '"+temp+"' form='collegeEditForm'>");
                college.description = temp;
                break;
              default:
            }
          }
          i++;
        });
        $('#'+id).html("save");
        //$('#'+id).addClass("willSaveCollege");
        $('#'+id).attr("form", 'collegeEditForm');
        return false;
    }else{
      $('#collegeEditForm').parsley().validate();
      $('#collegeEditForm').trigger('submit');
    }
  });
  $('#collegeTableBody').on('click', '.viewDepartmentCollege', function(){
    //divLoader('departmentsTable');
    tableLoader('6', 'departmentsTable');
    var id = $(this).prop('id');
    collegeid = id;
    $('#collegeComboBox option[value="'+id.split('-')[1]+'"]').prop("selected", true);
    if(!$('#addDepartmentBody').hasClass('in')){
      $( "#AddDepartmentTitle" ).trigger( "click" );
    }
    getAjax('/college/department/'+id.split('-')[1], 'departmentsTable');
  });
  $('#collegeComboBox').on('change', function(){
    tableLoader('5', 'departmentsTable');
    getAjax('/college/department/'+$('#collegeComboBox').val(), 'departmentsTable');
  });
  $('#departmentsTable').on('click', '.editDepartment', function(){
    var id =  $(this).prop('id');
    editDepartmentButton = id;
     $('.viewCoursesDepartment').attr('disabled', false);
     $('.changeCollege').attr('disabled', false);

    $('.editDepartment').each(function(i, obj) {
       if(id!=$(this).prop('id')){
         $(this).attr('form', '');
         $(this).html("<i class ='fa fa-edit'></i>");
         //console.log("hi");
       }
    });
    $('#departmentsTable').children('tr').each(function (){
      if(editDepartmentButton.split('-')[1] == $(this).prop('id').split('-')[1]){
        return;
      }else{
        $(this).children('td').each(function(){
          var that = this;
          $(this).children('input').each(function(){
            $(that).html($(this).val());
          });
        });
      }
    });

    if($('#'+id).text()!='save'){
        var i =0;
        var temp;
        $('#viewCoursesDepartment-'+id.split('-')[1]).attr('disabled', true);
        $('#changeCollege-'+id.split('-')[1]).attr('disabled', true);

        $('#department-'+id.split('-')[1]).children('td').each(function () {
          if(i>1){
            return;
          }else{
            temp = $(this).text();
            switch (i) {
              case 0:
                $(this).html("<input type='text' required maxlength='5' class='form-control' id = 'editDepartment-"+i+"' value = '"+temp+"' form='collegeEditDepartmentForm'>");
                department.code = temp;
                break;
              case 1:
                $(this).html("<input type='text' required maxlength='50' class='form-control' id = 'editDepartment-"+i+"' value = '"+temp+"' form='collegeEditDepartmentForm'>");
                department.name = temp;
                break;
              default:
            }
          }
          i++;
        });
        $('#'+id).html("save");
        $('#'+id).attr("form", 'collegeEditDepartmentForm');
        return false;
    }else{
      $('#collegeEditDepartmentForm').parsley().validate();
      $('#collegeEditDepartmentForm').trigger('submit');
    }
  });
  $('#collegeEditDepartmentForm').on('submit', function(e){
    e.preventDefault();
    $('#'+editDepartmentButton).html("<i class='fa fa-edit'></i>");
    $('#'+editDepartmentButton).attr("form", '');
    $('#viewCoursesDepartment-'+editDepartmentButton.split('-')[1]).attr('disabled', false);
    $('#changeCollege-'+editDepartmentButton.split('-')[1]).attr('disabled', false);

    var myform = document.getElementById('collegeEditDepartmentForm');
    var editform = new FormData(myform);
    editform.append('departmentCode', $('#editDepartment-0').val());
    editform.append('departmentName', $('#editDepartment-1').val());
    editform.append('id', editDepartmentButton.split('-')[1]);
    var i=0;
    new Promise((res, rej)=>{
      prependPostAjaxCallback("/college/department/edit", editform, 'Department Code Already Exist!',
      'College Successfully Saved!', function(value){
        if(value){
          res(true);
        }else{
          rej(false);
        }
      });
    }).then((res)=>{
      $('#department-'+editDepartmentButton.split('-')[1]).children('td').each(function () {
        if(i>1){
          return;
        }
        switch (i) {
          case 0:
            $(this).html($('#editDepartment-0').val());
            break;
          case 1:
            $(this).html($('#editDepartment-1').val());
            break;
          default:
        }
        i++;
      });
    }).catch((err)=>{
      $('#department-'+editDepartmentButton.split('-')[1]).children('td').each(function () {
        if(i>1){
          return;
        }
        switch (i) {
          case 0:
          //alert("Hello");
            $(this).html(department.code);
            break;
          case 1:
            $(this).html(department.name);
            break;
          default:
        }
        i++;
      });
    });
  });
  $('#departmentsTable').on('click', '.viewCoursesDepartment', function(){
    $('#addCourseModal').modal('show');
    var id = $(this).prop('id').split('-')[1];
    departmentid = id;
    var doms=[];
    var i = 0;
    $('#department-'+id).children('td').each(function(){
      doms[i]=$(this).html();
      i++;
    });
    $('#addCourseModal .modal-title').text(doms[1]);
    tableLoader('6', 'coursesTable');
    getAjax('/college/course/'+id

    , 'coursesTable');
  });
  $('#courseAddForm').on('submit', function(e){
    e.preventDefault();
    var form= document.getElementById('courseAddForm');
    var courseForm = new FormData(form);
    courseForm.append('id', departmentid);
    $('#coursesTable h2').remove();
    prependPostAjax("/college/course/insert", courseForm, '#coursesTable', 'Course Code should be unique.',
    'Course Successfully Saved!');
  });
  $('#coursesTable').on('click', '.editCourse', function(){
    var id =  $(this).prop('id');
    editCourseButton = id;
    //console.log(id);
    $('.changeDepartment').attr('disabled', false);
    $('.editCourse').each(function(i, obj) {
       if(id!=$(this).prop('id')){
         $(this).attr('form', '');
         $(this).html("<i class ='fa fa-edit'></i>");
         //console.log("hi");
       }
    });
    $('#coursesTable').children('tr').each(function (){
      if(editCourseButton.split('-')[1] == $(this).prop('id').split('-')[1]){
        return;
      }else{
        $(this).children('td').each(function(){
          var that = this;
          $(this).children('input').each(function(){
            $(that).html($(this).val());
          });
        });
      }
    });
    //$('#'+id).html("save");
    if($('#'+id).text()!='save'){
      //console.log("hez");
      $('#changeDepartment-'+id.split('-')[1]).attr('disabled', true);
        var i =0;
        var temp;
        $('#course-'+id.split('-')[1]).children('td').each(function () {
          if(i>2){
            return;
          }else{
            temp = $(this).text();
            switch (i) {
              case 0:
                $(this).html("<input type='text' required maxlength='5' class='form-control' id = 'editCourse-"+i+"' value = '"+temp+"' form='courseEditForm'>");
                course.code = temp;
                break;
              case 1:
                $(this).html("<input type='text' required maxlength='50' class='form-control' id = 'editCourse-"+i+"' value = '"+temp+"' form='courseEditForm'>");
                course.name = temp;
                break;
              case 2:
                $(this).html("<input type='number' required class='form-control' id = 'editCourse-"+i+"' value = '"+temp+"' form='courseEditForm'>");
                course.year = temp;
                break;
              default:
            }
          }
          i++;
        });
        //console.log(id);

        $('#'+id).html("save");
        $('#'+id).attr("form", 'courseEditForm');
        return false;
    }else{
      $('#courseEditForm').parsley().validate();
      $('#courseEditForm').trigger('submit');
    }
  });
  $('#courseEditForm').on('submit', function(e){
    e.preventDefault();
    $('#'+editCourseButton).html("<i class='fa fa-edit'></i>");
    $('#'+editCourseButton).attr("form", '');
    $('#changeDepartment-'+editCourseButton.split('-')[1]).attr('disabled', false);
    var myform = document.getElementById('courseEditForm');
    var editform = new FormData(myform);
    editform.append('courseCode', $('#editCourse-0').val());
    editform.append('courseName', $('#editCourse-1').val());
    editform.append('courseYear', $('#editCourse-2').val());
    editform.append('id', editCourseButton.split('-')[1]);
    var i=0;
    new Promise((res, rej)=>{
      prependPostAjaxCallback("/college/course/edit", editform, 'Course Code Already Exist!',
      'College Successfully Saved!', function(value){
        if(value){
          res(true);
        }else{
          rej(false);
        }
      });
    }).then((res)=>{
      $('#course-'+editCourseButton.split('-')[1]).children('td').each(function () {
        if(i>2){
          return;
        }
        switch (i) {
          case 0:
            $(this).html($('#editCourse-0').val());
            break;
          case 1:
            $(this).html($('#editCourse-1').val());
            break;
          case 2:
            $(this).html($('#editCourse-2').val());
            break;
          default:
        }
        i++;
      });
    }).catch((err)=>{
      $('#course-'+editCourseButton.split('-')[1]).children('td').each(function () {
        if(i>2){
          return;
        }
        switch (i) {
          case 0:
          //alert("Hello");
            $(this).html(course.code);
            break;
          case 1:
            $(this).html(course.name);
            break;
          case 2:
            $(this).html(course.year);
            break;
          default:
        }
        i++;
      });
    });
  });
  $('#departmentsTable').on('click', '.changeCollege', function(){
    $('#selectCollegeModal').modal('show');
    var id = $(this).prop('id').split('-')[1];
    departmentid = id;
    var doms=[];
    var i = 0;
    $('#department-'+id).children('td').each(function(){
      doms[i]=$(this).html();
      i++;
    });
    $('#selectCollegeModal .modal-title').text(doms[1]);
    tableLoader('3', 'collegeSelectTable');
    getAjax('/college/department/select/college/'+collegeid.split('-')[1]
    , 'collegeSelectTable');
  });
  $('#collegeSelectTable').on('click', '.collegeSelectButton', function(){
    var collegeid = $(this).prop('id');
    $('#selectCollegeModal').modal('hide');
    collegeSelectForm
    var form = document.getElementById('collegeSelectForm');
    var myform = new FormData(form);
    myform.append('collegeID', collegeid.split('-')[1]);
    myform.append('departmentID', departmentid);
    prependPostAjaxCallback('/college/department/select/college/change', myform,
    'Something went wrong. please try again later!',
    'Department Successfully Transfered!', function(data){
      if(data){
        $('#department-'+departmentid).remove();
      }
    });

  });
  $('#coursesTable').on('click', '.changeDepartment', function(){
    $('#departmentSelectModal').modal('show');
    var id = $(this).prop('id').split('-')[1];
    courseid = id;
    var doms=[];
    var i = 0;
    $('#course-'+id).children('td').each(function(){
      doms[i]=$(this).html();
      i++;
    });
    $('#departmentSelectModal .modal-title').text(doms[1]);
    tableLoader('3', 'departmentSelectTable');
    getAjax('/college/course/select/department/'+departmentid
    , 'departmentSelectTable');
  });
  $('#departmentSelectTable').on('click', '.departmentSelectButton', function(){
    var form = document.getElementById('departmentSelectForm');
    var myform = new FormData(form);
    $('#departmentSelectModal').modal('hide');
    myform.append('departmentID', $(this).prop('id').split('-')[1]);
    myform.append('courseID', courseid);
    prependPostAjaxCallback('/college/course/select/department/change', myform,
    'Something went wrong. please try again later!',
    'College Successfully Transfered!', function(data){
      if(data){
        $('#course-'+courseid).remove();
      }
    });
  });
  $('#searchCollegeModal').on('keydown', function(){
    tableLoader('6', 'collegeTableBody');
    getAjax('/college/list/search/'+$('#searchCollegeModal').val(), 'collegeTableBody');
  });
  $('#selectCollegeSearch').on('keydown', function(){
    tableLoader('6', 'collegeSelectTable');
    getAjax('/college/list/searchselect/'+$('#selectCollegeSearch').val()+'/'+collegeid.split('-')[1], 'collegeSelectTable');
  });
  $('#searchDepartmentsSelect').on('keydown', function(){
    tableLoader('3', 'departmentSelectTable');
    getAjax('/college/course/searchfor/department/'+$('#searchDepartmentsSelect').val()+'/'+departmentid
    , 'departmentSelectTable');
  });
  $('#collegeTableBody').on('click', '.deleteCollege', function(){
    $("#deleteModal").modal('show');
    collegeid = $(this).prop('id').split('-')[1];
    $("#deleteMessage").html("<h3>Are you sure you want to delete this college?</h3>");
    mydelete ="deleteCollege";
  });
  $('#departmentsTable').on('click', '.deleteDepartment',function(){
    $("#deleteModal").modal('show');
    departmentid = $(this).prop('id').split('-')[1];
    $("#deleteMessage").html("<h3>Are you sure you want to delete this department?</h3>");
    mydelete ="deleteDepartment";
  });
  $('#coursesTable').on('click','.deleteCourse', function(){
    $("#deleteModal").modal('show');
    courseid = $(this).prop('id').split('-')[1];
    $("#deleteMessage").html("<h3>Are you sure you want to delete this course?</h3>");
    mydelete ="deleteCourse";
  });
  $('#deleteButton').on('click', function(){
    switch (mydelete) {
      case "deleteCollege":
        $.get('/college/delete/'+collegeid, function(data){
          console.log(data);
          if(data == 'deleted'){
            new PNotify({
                        title: 'Warning',
                        text: 'College Successfully deleted.',
                        styling: 'bootstrap3'
                    });
                    $("#college-"+collegeid).remove();
          }else{
            new PNotify({
                        title: 'Warning',
                        text: 'Cannot delete college.',
                        styling: 'bootstrap3'
                    });
          }
        });
        break;
      case 'deleteDepartment':
        $.get('/department/delete/'+departmentid, function(data){
          console.log(data);
          if(data == 'deleted'){
            new PNotify({
                        title: 'Warning',
                        text: 'Department Successfully deleted.',
                        styling: 'bootstrap3'
                    });
                    $("#department-"+departmentid).remove();
          }else{
            new PNotify({
                        title: 'Warning',
                        text: 'Cannot delete department.',
                        styling: 'bootstrap3'
                    });
          }
        });
        break;
      case 'deleteCourse':
        $.get('/course/delete/'+courseid, function(data){
          console.log(data);
          if(data == 'deleted'){
            new PNotify({
                        title: 'Warning',
                        text: 'Course Successfully deleted.',
                        styling: 'bootstrap3'
                    });
                    $("#course-"+courseid).remove();
          }else{
            new PNotify({
                        title: 'Warning',
                        text: 'Cannot delete course.',
                        styling: 'bootstrap3'
                    });
          }
        });
        break;
      case 'deleteStrand':
        $.get('/seniorhigh/delete/'+strandid, function(data){
          //console.log(data);
          if(data == 'deleted'){
            $('#strand-'+strandid).remove();
            new PNotify({
                        title: 'Warning',
                        text: 'Strand Successfully deleted.',
                        styling: 'bootstrap3'
                    });
          }else{
            new PNotify({
                        title: 'Warning',
                        text: 'Cannot delete strand.',
                        styling: 'bootstrap3'
                    });

          }
        });
      break;
      case 'setInactiveUser':
      $.get('/useraccount/setinactive/'+globalUserId, function(data){
        //console.log(data);
          if(data == "errorCoordinator"){
            new PNotify({
              title: 'Error',
              text: "A coordinator is currently assigned to this user's designation.",
              type: 'error',
              styling: 'bootstrap3'
            });
          }else{
            $('#useraccount-'+globalUserId).remove();
            new PNotify({
                        title: 'Warning',
                        text: 'User Successfully set to inactive.',
                        styling: 'bootstrap3'
            });
          }
      });
      break;
      case 'setActiveUser':
      $.get('/useraccount/setactive/'+globalUserId, function(data){
          $('#useraccount-'+globalUserId).remove();
          new PNotify({
                      title: 'Warning',
                      text: 'Strand Successfully set to active.',
                      styling: 'bootstrap3'
          });
        });
      break;
      case 'deletefamily':
        $.get('/student/delete/'+globalfamilyid, function(data){
          $('#familyStudent-'+globalfamilyid).remove();
          new PNotify({
                      title: 'Warning',
                      text: 'Family successfully deleted.',
                      styling: 'bootstrap3'
          });
        });
      break;
      case 'deleteSubject':
        $.get('/subject/delete/'+subjectid, function(data){
          if(data =="cannotdelete"){
            new PNotify({
                        title: 'Error',
                        text: 'Subject cannot delete because it has existing records.',
                        styling: 'bootstrap3'
            });
          }else{
            $('#subjectRow-'+subjectid).remove();
            new PNotify({
                        title: 'Warning',
                        text: 'Subject successfully deleted.',
                        styling: 'bootstrap3'
            });
          }
          
        });
      break;
      case 'deleteeducationalbackground':
        $.get('/educationalbackground/delete/'+globalschoolbackgroundid, function(data){
          
            $('#educationalBackground-'+globalschoolbackgroundid).remove();
            new PNotify({
                        title: 'Warning',
                        text: 'Educational background successfully deleted.',
                        styling: 'bootstrap3'
            });

        });
      break;
      case 'deletefavoritesubject':
        $.get('/favoritesubject/delete/'+globalfavsubjid, function(data){
            
          $('#favsubject-'+globalfavsubjid).remove();
          new PNotify({
                      title: 'Warning',
                      text: 'Favorite subject successfully deleted.',
                      styling: 'bootstrap3'
          });
        });
      break;
      case 'deletedislikedsubject':
        $.get('/dislikedsubject/delete/'+globaldislikedsubjid, function(data){
              
          $('#dislikedsubject-'+globaldislikedsubjid).remove();
          new PNotify({
                      title: 'Warning',
                      text: 'Disliked subject successfully deleted.',
                      styling: 'bootstrap3'
          });
        });
      case 'deleteleisure':
        $.get('/studentleisure/delete/'+globalleisureid+'/'+globalleisurestudentid, function(data){
          //console.log(data);
          $('#leisurerow-'+globalleisureid).remove();
          new PNotify({
                      title: 'Warning',
                      text: 'Disliked subject successfully deleted.',
                      styling: 'bootstrap3'
          });
        });
      break;
      case 'deletehealthrecord':
        $.get('/healthrecord/delete/'+globalhealthrecordid, function(data){
          //console.log(data);
          $('#healthrecord-'+globalhealthrecordid).remove();
          new PNotify({
                      title: 'Warning',
                      text: 'Health record successfully deleted.',
                      styling: 'bootstrap3'
          });
        });
      break;
      case 'deletePersonality':
        $.get('/personality/delete/'+personalityid, function(data){
          //console.log(data);
          if(data == 'cannotdelete'){
            new PNotify({
              title: 'Warning',
              text: 'This personality is already assigned to other records!.',
              styling: 'bootstrap3'
            });
            return;  
          }
          $('#personalityrow-'+personalityid).remove();
          $('#personalityCheckRow-'+personalityid).remove();
          
          new PNotify({
                      title: 'Warning',
                      text: 'Personality successfully deleted.',
                      styling: 'bootstrap3'
          });
        });
      break;
      case 'deletemakeupassistance':
        $.get('/makeupassistance/delete/'+globalmakeupassistanceid, function(data){
          
          $('#makeUpAssistanceRow-'+globalmakeupassistanceid).remove(); 
          new PNotify({
                      title: 'Warning',
                      text: 'MakeupAssistance successfully deleted.',
                      styling: 'bootstrap3'
          });
        });
      break;
      case 'deleteContractProof':
        $.get('/contract/delete/proof/'+globalcontractproofid, function(data){
            
          $('#imageDelete-'+globalcontractproofid).remove(); 
          new PNotify({
                      title: 'Warning',
                      text: 'Proof successfully deleted.',
                      styling: 'bootstrap3'
          });
        });
      break;
      case 'deleteEvent':
        $.get('/events/delete/'+globaleventid, function(data){
          $("#calendar").fullCalendar('removeEvents', 'event-'+globaleventid);
          $('#editEventModal').modal('hide');
          //$('#imageDelete-'+globalcontractproofid).remove(); 
          new PNotify({
            title: 'Warning',
            text: 'Event successfully deleted.',
            styling: 'bootstrap3'
          });
        });
      break;
      case 'deleteBehaviour':
        $.get('/behaviour/delete/'+behaviorid.split("-")[1], function(data){
          if(data != 'true'){
            new PNotify({
              title: 'Failed',
              text: 'Delete failed because it has existing data in other table.',
              styling: 'bootstrap3'
            });
          }else{
            new PNotify({
              title: 'Warning',
              text: 'Behaviour successfully deleted.',
              styling: 'bootstrap3'
            });
            $('#behaviourRow-'+behaviorid.split("-")[1]).remove();
            $("#observableBehaviourText option[value='"+behaviorid.split("-")[1]+"']").remove();
          }
        });
      break;
      case 'deleteCounselor':
        $.get('/counselor/delete/'+counselorid, function(data){
          if(data != 'true'){
            new PNotify({
              title: 'Failed',
              text: 'Delete failed because it has existing data in other table.',
              styling: 'bootstrap3'
            });
          }else{
            new PNotify({
              title: 'Warning',
              text: 'Counselor successfully set to inactive.',
              styling: 'bootstrap3'
            });
            //alert(counselorid);
          $("#deleteCounselor-"+counselorid).closest("td").html("<button class='btn btn-warning restoreCounselor' id = 'deleteCounselor-"+counselorid+"'>"+
                                                                  "<i class='fa fa-undo'></i>"+
                                                                  "</button>");
          }
        });
      break;
      case 'deletestudentrecord':
        window.location.href = "/student/record/delete/"+$('#studentid').val();
      break;
      default:
      break;
    }
  });
  $('#seniorHighStrandAddForm').on('submit', function(e){
    e.preventDefault();
    var form =  document.getElementById('seniorHighStrandAddForm');
    var myform = new FormData(form);
    $('#seniorHighTable').children('h2').each(function(){
      $(this).remove();
    });
    $("#seniorHighStrandAddForm input[type = 'text']").val("");
    $("#seniorHighStrandAddForm textarea").val("");
    prependPostAjax('/seniorhigh/strand/insert', myform, '#seniorHighTable', 'Make sure strand code is unique!', 'Successfully saved!');
  });
  $('#seniorHighEditForm').on('submit', function(e){
    e.preventDefault();
    $('#'+editStrandButton).html("<i class='fa fa-edit'></i>");
    $('#'+editStrandButton).attr("form", '');


    var myform = document.getElementById('seniorHighEditForm');
    var editform = new FormData(myform);
    editform.append('strandCode', $('#editStrand-0').val());
    editform.append('strandName', $('#editStrand-1').val());
    editform.append('strandDescription', $('#editStrand-2').val());
    editform.append('id', editStrandButton.split('-')[1]);
    var i=0;
    new Promise((res, rej)=>{
      prependPostAjaxCallback("/seniorhigh/edit", editform, 'Strand Code Already Exist!',
      'Strand Successfully Saved!', function(value){
        if(value){
          res(true);
        }else{
          rej(false);
        }
      });
    }).then((res)=>{
      $('#strand-'+editStrandButton.split('-')[1]).children('td').each(function () {
        if(i>2){
          return;
        }
        switch (i) {
          case 0:
            $(this).html($('#editStrand-0').val());
            break;
          case 1:
            $(this).html($('#editStrand-1').val());
            break;
          case 2:
            $(this).html($('#editStrand-2').val());
            break;
          default:
        }
        i++;
      });
    }).catch((err)=>{
      $('#strand-'+editStrandButton.split('-')[1]).children('td').each(function () {
        if(i>2){
          return;
        }
        switch (i) {
          case 0:
          //alert("Hello");
            $(this).html(strand.code);
            break;
          case 1:
            $(this).html(strand.name);
            break;
          case 2:
            $(this).html(strand.description);
            break;
          default:
        }
        i++;
      });
    });
  });
  $('#seniorHighTable').on('click', '.editStrandRow', function(){
    var id =  $(this).prop('id');
    editStrandButton = id;

    $('.editStrandRow').each(function(i, obj) {
       if(id!=$(this).prop('id')){
         $(this).attr('form', '');
         $(this).html("<i class ='fa fa-edit'></i>");
         //console.log("hi");
       }
    });
    $('#seniorHighTable').children('tr').each(function (){
      if(editStrandButton.split('-')[1] == $(this).prop('id').split('-')[1]){
        return;
      }else{
        $(this).children('td').each(function(){
          var that = this;
          $(this).children('input').each(function(){
            $(that).html($(this).val());
          });
        });
      }
    });

    if($('#'+id).text()!='save'){
        var i =0;
        var temp;

        $('#strand-'+id.split('-')[1]).children('td').each(function () {
          if(i>2){
            return;
          }else{
            temp = $(this).text();
            switch (i) {
              case 0:
                $(this).html("<input type='text' required maxlength='5' class='form-control' id = 'editStrand-"+i+"' value = '"+temp+"' form='seniorHighEditForm'>");
                strand.code = temp;
                break;
              case 1:
                $(this).html("<input type='text' required maxlength='50' class='form-control' id = 'editStrand-"+i+"' value = '"+temp+"' form='seniorHighEditForm'>");
                strand.name = temp;
                break;
              case 2:
                $(this).html("<input type='text' required maxlength='250' class='form-control' id = 'editStrand-"+i+"' value = '"+temp+"' form='seniorHighEditForm'>");
                strand.description = temp;
                break;
              default:
            }
          }
          i++;
        });
        $('#'+id).html("save");
        $('#'+id).attr("form", 'seniorHighEditForm');
        return false;
    }else{
      $('#seniorHighEditForm').parsley().validate();
      $('#seniorHighEditForm').trigger('submit');
    }
  });
  $('#seniorHighTable').on('click', '.deleteStrandRow', function(){
    $("#deleteModal").modal('show');
    strandid = $(this).prop('id').split('-')[1];
    $("#deleteMessage").html("<h3>Are you sure you want to delete this strand?</h3>");
    mydelete ="deleteStrand";
    //var id = $(this).prop('id');

  });
  $('#searchSeniorHighStrand').on('keydown', function(){
    tableLoader('5', 'seniorHighTable');
    getAjax('/seniorhigh/list/search/'+$('#searchSeniorHighStrand').val(), 'seniorHighTable');
  });
  $('#subjectAddForm').on('submit', function(e){
    e.preventDefault();
    var form = document.getElementById('subjectAddForm');
    var myform = new FormData(form);
    prependPostAjaxErrorCallback('/subject/insert', myform, function(data){
      $('#subjectTable').children('h2').each(function(){
        $(this).remove();
      });
      $("#subjectAddForm input[type = 'text']").val("");
      $("#subjectAddForm textarea").val("");
      $('#subjectTable').prepend(data.html);
      $('#likedSubjectsCombo').prepend("<option value ='"+data.id+"'>"+data.subjectName+"</option>");
      $('#dislikedSubjectsCombo').prepend("<option value ='"+data.id+"'>"+data.subjectName+"</option>");
      new PNotify({
        title: 'Success!',
        text: "Successfully saved a subject.",
        type: 'success',
        styling: 'bootstrap3'
      });
    });
  });
  $('#subjectTable').on('click', '.editSubjectRow', function(){
    var id =  $(this).prop('id');
    editSubjectButton = id;
    
    $('.editSubjectRow').each(function(i, obj) {
       if(id!=$(this).prop('id')){
         $(this).attr('form', '');
         $(this).html("<i class ='fa fa-edit'></i>");
       }
    });
    $('#subjectTable').children('tr').each(function (){
      if(editSubjectButton.split('-')[1] == $(this).prop('id').split('-')[1]){
        return;
      }else{
        $(this).children('td').each(function(){
          var that = this;
          $(this).children('input').each(function(){
            $(that).html($(this).val());
          });
        });
      }
    });
    //$('#'+id).html("save");
    if($('#'+id).text()!='save'){
      //console.log("hez");
     
        var i =0;
        var temp;
        $('#subjectRow-'+id.split('-')[1]).children('td').each(function () {
          if(i>2){
            return;
          }else{
            temp = $(this).text();
            switch (i) {
              case 0:
                $(this).html("<input type='text' required maxlength='5' class='form-control' id = 'editSubject-"+i+"' value = '"+temp+"' form='subjectEditForm'>");
                subject.name = temp;
                break;
              case 1:
                $(this).html("<input type='text' required maxlength='50' class='form-control' id = 'editSubject-"+i+"' value = '"+temp+"' form='subjectEditForm'>");
                subject.code = temp;
                break;
              case 2:
                $(this).html("<input type='text' required class='form-control' id = 'editSubject-"+i+"' value = '"+temp+"' form='subjectEditForm'>");
                subject.description = temp;
                break;
              default:
              break;
            }
          }
          i++;
        });
        //console.log(id);

        $('#'+id).html("save");
        $('#'+id).attr("form", 'subjectEditForm');
        return false;
      }else{
        $('#subjectEditForm').parsley().validate();
        $('#subjectEditForm').trigger('submit');
      }
  });
  $('#subjectEditForm').on('submit', function(e){
    e.preventDefault();
    $('#'+editSubjectButton).html("<i class='fa fa-edit'></i>");
    $('#'+editSubjectButton).attr("form", '');
    
    var myform = document.getElementById('subjectEditForm');
    var editform = new FormData(myform);
    editform.append('subjectName', $('#editSubject-0').val());
    editform.append('subjectCode', $('#editSubject-1').val());
    editform.append('description', $('#editSubject-2').val());
    editform.append('id', editSubjectButton.split('-')[1]);
    var i=0;
    new Promise((res, rej)=>{
      prependPostAjaxCallback("/subject/edit", editform, 'Subject Code Already Exist!',
      'Subject Successfully Saved!', function(value){
        if(value){
          res(true);
        }else{
          rej(false);
        }
      });
    }).then((res)=>{
      $('#subjectRow-'+editSubjectButton.split('-')[1]).children('td').each(function () {
        if(i>2){
          return;
        }
        switch (i) {
          case 0:
            $(this).html($('#editSubject-0').val());
            break;
          case 1:
            $(this).html($('#editSubject-1').val());
            break;
          case 2:
            $(this).html($('#editSubject-2').val());
            break;
          default:
          break;
        }
        i++;
      });
    }).catch((err)=>{
      $('#subjectRow-'+editSubjectButton.split('-')[1]).children('td').each(function () {
        if(i>2){
          return;
        }
        switch (i) {
          case 0:
          //alert("Hello");
            $(this).html(subject.name);
            break;
          case 1:
            $(this).html(subject.code);
            break;
          case 2:
            $(this).html(subject.description);
            break;
          default:
          break;
        }
        i++;
      });
    });
  });
  $('#subjectTable').on('click', '.deleteSubjectRow', function(){
    subjectid = $(this).prop("id").split("-")[1];
    $("#deleteModal").modal('show');
    $("#deleteMessage").html("<h3>Are you sure you want to delete this subject?</h3>");
    mydelete ="deleteSubject";
  });
  $('#personalityAddForm').on('submit', function(e){
    e.preventDefault();
    var form = document.getElementById('personalityAddForm');
    var myform = new FormData(form);
    prependPostAjaxErrorCallback('/personality/insert', myform, function(data){
      $('#personalityTable').children('h2').each(function(){
        $(this).remove();
      });
      $("#personalityAddForm input[type = 'text']").val("");
      $("#personalityAddForm textarea").val("");
      $('#personalityTable').prepend(data.html);
      $('#personalityTableCheck').prepend("<tr id ='personalityCheckRow-"+data.id+"'>"+
                                              "<td><input type ='radio' class='radioPersonality' id ='radioPersonality-"+data.id+"'"+
                                              "name ='radioPersonality' required value ='"+data.id+"'></td>"+
                                              "<td>"+data.name+"</td>"+
                                              "<td>"+data.description+"</td>"+
                                            "</tr>");
      
      new PNotify({
        title: 'Success!',
        text: "Personality Saved.",
        type: 'success',
        styling: 'bootstrap3'
      });
    });
  });
  $('#personalityTable').on('click', '.editPersonalityRow', function(){
    var id =  $(this).prop('id');
    editPersonalityButton = id;
    
    $('.editPersonalityRow').each(function(i, obj) {
       if(id!=$(this).prop('id')){
         $(this).attr('form', '');
         $(this).html("<i class ='fa fa-edit'></i>");
       }
    });
    $('#personalityTable').children('tr').each(function (){
      if(editPersonalityButton.split('-')[1] == $(this).prop('id').split('-')[1]){
        return;
      }else{
        $(this).children('td').each(function(){
          var that = this;
          $(this).children('input').each(function(){
            $(that).html($(this).val());
          });
        });
      }
    });
    //$('#'+id).html("save");
    if($('#'+id).text()!='save'){
      //console.log("hez");
     
        var i =0;
        var temp;
        $('#personalityrow-'+id.split('-')[1]).children('td').each(function () {
          if(i>1){
            return;
          }else{
            temp = $(this).text();
            switch (i) {
              case 0:
                $(this).html("<input type='text' required maxlength='100' class='form-control' id = 'editPersonality-"+i+"' value = '"+temp+"' form='personalityEditForm'>");
                personality.name = temp;
                break;
              case 1:
                $(this).html("<input type='text' required maxlength='100' class='form-control' id = 'editPersonality-"+i+"' value = '"+temp+"' form='personalityEditForm'>");
                personality.description = temp;
                break;
            }
          }
          i++;
        });
        //console.log(id);

        $('#'+id).html("save");
        $('#'+id).attr("form", 'personalityEditForm');
        return false;
      }else{
        $('#personalityEditForm').parsley().validate();
        $('#personalityEditForm').trigger('submit');
      }
  });
  $('#personalityEditForm').on('submit', function(e){
    e.preventDefault();
    $('#'+editPersonalityButton).html("<i class='fa fa-edit'></i>");
    $('#'+editPersonalityButton).attr("form", '');
    
    var myform = document.getElementById('personalityEditForm');
    var editform = new FormData(myform);
    editform.append('name', $('#editPersonality-0').val());
    
    editform.append('description', $('#editPersonality-1').val());
    editform.append('id', editPersonalityButton.split('-')[1]);
    var i=0;
    new Promise((res, rej)=>{
      prependPostAjaxCallback("/personality/edit", editform, '',
      'Personality Successfully Saved!', function(value){
        if(value){
          res(true);
        }else{
          rej(false);
        }
      });
    }).then((res)=>{
      $('#personalityrow-'+editPersonalityButton.split('-')[1]).children('td').each(function () {
        if(i>1){
          return;
        }
        switch (i) {
          case 0:
            $(this).html($('#editPersonality-0').val());
            break;
          case 1:
            $(this).html($('#editPersonality-1').val());
            break;
        }
        i++;
      });
    }).catch((err)=>{
      $('#personalityrow-'+editPersonalityButton.split('-')[1]).children('td').each(function () {
        if(i>1){
          return;
        }
        switch (i) {
          case 0:
          //alert("Hello");
            $(this).html(personality.name);
            break;
          case 1:
            $(this).html(personality.description);
            break;

        }
        i++;
      });
    });
  });
  $('#personalityTable').on('click', '.deletePersonalityRow', function(){
    personalityid = $(this).prop("id").split("-")[1];
    $("#deleteModal").modal('show');
    $("#deleteMessage").html("<h3>Are you sure you want to delete this personality?</h3>");
    mydelete ="deletePersonality";
  });
  $('#systemDateForm').on('submit', function(e){
    e.preventDefault();
    var form = document.getElementById('systemDateForm');
    var myform = new FormData(form);
    prependPostAjaxErrorCallback('/systemdate/update', myform, function(data){
      new PNotify({
        title: 'Success!',
        text: 'System Time Successfully Updated!',
        type: 'success',
        styling: 'bootstrap3'
      });
      $('#changeSystemDateButton').attr('disabled', true);
    });
  });
  $('#systemDate').on('change', function(){
    $('#changeSystemDateButton').attr('disabled', false);
  });
  $("#manageLayoutForm").on('submit', function(e){
    e.preventDefault();
    var mydata = document.getElementById("manageLayoutForm");
    var formdata = new FormData(mydata);
    prependPostAjaxErrorCallback('/settings/insert', formdata, function(){
      new PNotify({
        title: 'Success',
        text: 'Setting saved!.',
        type: 'success',
        styling: 'bootstrap3'
      });
    });
  });
  $("#removeSchoolSealImage").on('click', function(){
    $("#schoolSealImage").attr("src", "/images/defaultimage.png");
  });
  $("#removeGuidanceSealImage").on('click', function(){
    
    $("#guidanceSealImage").attr("src", "/images/defaultimage.png")
  });
  $("#behaviorAddForms").on('submit', function(e){
    //alert
    e.preventDefault();
    var mydata = document.getElementById("behaviorAddForms");
    var formdata= new FormData(mydata);
    prependPostAjaxErrorCallback('/behaviour/insert', formdata, function(data){
      new PNotify({
        title: 'Success',
        text: 'Behaviour successfully saved!',
        type:'success',
        styling: 'bootstrap3'
      });
      var obj = JSON.parse(data);
      $("#behaviourTable h2").remove();
      $("#behaviourTable").prepend(obj.row);
      $("#observableBehaviourText").prepend(obj.combo);
      $("#behaviorAddForms input[type='text']").val("");
      $("#behaviorAddForms textarea").val("");
    });
  });
  $("#behaviourTable").on('click', '.editBehaviour', function(){
      var id =  $(this).prop('id');
      behaviorid = id;
      
      $('.editBehaviour').each(function(i, obj) {
        if(id!=$(this).prop('id')){
          $(this).attr('form', '');
          $(this).html("<i class ='fa fa-edit'></i>");
        }
      });
      $('#behaviourTable').children('tr').each(function (){
        if(behaviorid.split('-')[1] == $(this).prop('id').split('-')[1]){
          return;
        }else{
          $(this).children('td').each(function(){
            var that = this;
            $(this).children('input').each(function(){
              $(that).html($(this).val());
            });
          });
        }
      });
      //$('#'+id).html("save");
      if($('#'+id).text()!='save'){
      //console.log("hez");
     
        var i =0;
        var temp;
        $('#behaviourRow-'+id.split('-')[1]).children('td').each(function () {
          if(i>1){
            return;
          }else{
            temp = $(this).text();
            switch (i) {
              case 0:
                $(this).html("<input type='text' required maxlength='100' class='form-control' id = 'editBehaviours-"+i+"' value = '"+temp+"' form='behaviorEditForm'>");
                behaviourObj.name = temp;
                break;
              case 1:
                $(this).html("<input type='text' required maxlength='100' class='form-control' id = 'editBehaviours-"+i+"' value = '"+temp+"' form='behaviorEditForm'>");
                behaviourObj.description = temp;
                break;
            }
          }
          i++;
        });
        //console.log(id);

        $('#'+id).html("save");
        $('#'+id).attr("form", 'behaviorEditForm');
        return false;
      }else{
        $('#behaviorEditForm').parsley().validate();
        $('#behaviorEditForm').trigger('submit');
      }
  });
  $("#behaviorEditForm").on('submit', function(e){
    e.preventDefault();
    $('#'+behaviorid).html("<i class='fa fa-edit'></i>");
    $('#'+behaviorid).attr("form", '');
    
    var myform = document.getElementById('behaviorEditForm');
    var editform = new FormData(myform);
    editform.append('name', $('#editBehaviours-0').val());
    
    editform.append('description', $('#editBehaviours-1').val());
    editform.append('id', behaviorid.split('-')[1]);
    var i=0;
    new Promise((res, rej)=>{
      prependPostAjaxCallback("/behaviour/edit", editform, '',
      'Behaviour Successfully Saved!', function(value){
        //$("#behaviorAddForms input[type='text']").val("");
        //console.log(value);
        if(value){
          res(true);
        }else{
          rej(false);
        }
      });
    }).then((res)=>{
      $('#behaviourRow-'+behaviorid.split('-')[1]).children('td').each(function () {
        if(i>1){
          return;
        }
        switch (i) {
          case 0:
            $(this).html($('#editBehaviours-0').val());
            break;
          case 1:
            $(this).html($('#editBehaviours-1').val());
            break;
        }
        i++;
      });
    }).catch((err)=>{
      $('#behaviourRow-'+behaviorid.split('-')[1]).children('td').each(function () {
        if(i>1){
          return;
        }
        switch (i) {
          case 0:
          //alert("Hello");
            $(this).html(behaviourObj.name);
            break;
          case 1:
            $(this).html(behaviourObj.description);
            break;

        }
        i++;
      });
    });
  });
  $("#behaviourTable").on('click', '.deleteBehaviour', function(){
    var id =  $(this).prop('id');
    behaviorid = id;
    mydelete = "deleteBehaviour";
    $("#deleteModal").modal('show');
    $("#deleteMessage").html("<h3>Are you sure you want to delete this behaviour?</h3>");
  });
  $("#counselorAddForm").on('submit', function(e){
    e.preventDefault();
    $("#counselorDesignation").removeAttr("disabled");
    var mydata = document.getElementById("counselorAddForm");
    var formdata= new FormData(mydata);
    $("#counselorDesignation").attr("disabled", true);
    $("#counselorAddForm input[type='text']").val("");
    prependPostAjaxErrorCallback('/counselor/insert', formdata, function(data){
      new PNotify({
        title: 'Success',
        text: 'Counselor successfully saved!',
        type:'success',
        styling: 'bootstrap3'
      });
      //var obj = JSON.parse(data);
      $("#counselorTable h2").remove();
      $("#counselorTable").prepend(data);
      $("#counselorDesignation").removeAttr("disabled");
    });
  });
  $("#counselorTable").on('click', '.editCounselor', function(){
    // var counselorid;
    // var counselorObj = {firstName:'',lastName:'', designation:'', counselorType:''};
    var id =  $(this).prop('id');
    counselorid = id;
    
    $('.editCounselor').each(function(i, obj) {
      if(id!=$(this).prop('id')){
        $(this).attr('form', '');
        $(this).html("<i class ='fa fa-edit'></i>");
      }
    });
   
    $('#counselorTable').children('tr').each(function (){
      if(counselorid.split('-')[1] == $(this).prop('id').split('-')[1]){
        return;
      }else{
        var y = 0;
        $(this).children('td').each(function(){
          var that = this;
          switch(y){
            case 0:
              var name = '';
              var x=0;
              var passed = false;
              $(this).children('input').each(function(){
                // $(that).html($(this).val());
                passed = true;
                if(x==0){
                  name =name + $(this).val()+", ";
                }else{
                  name =name + $(this).val();
                }
                x++;
              });
              if(passed){
                $(that).html(name);
              }
              
            break;
            case 1:
              
              $(this).children('select').each(function(){
                if($(this).val()=="universityCounselor"){
                  $(that).html("University Counselor");
                }else{
                  $(that).html("Counselor");
                }
                
              });
            break;
            case 2:
              $(this).children('select').each(function(){
                //console.log($(this).val());
                switch($(this).val()){
                  case 'seniorhigh':
                    if($(this).val()!= ''){
                      $(that).html("Senior High");
                    }else{
                      $(that).html('');
                    }
                  break;
                  case 'juniorhigh':
                    if($(this).val()!= ''){
                      $(that).html("Junior High");
                    }else{
                      $(that).html('');
                    }
                  break;
                  case 'elementary':
                    if($(this).val()!= ''){
                      $(that).html("Elementary");
                    }else{
                      $(that).html('');
                    }
                  break;
                  default:
                    if($(this).val()!= ''){
                      $(that).html($(that).attr("data-college"));
                    }else{
                      $(that).html('');
                    }
                  break;
                }
                
              });
            break;
            default:
            break;
          }
          
          y++;
        });
      }
    });
    //$('#'+id).html("save");
    if($('#'+id).text()!='save'){
    //console.log("hez");
   
      var i =0;
      var temp;
      $('#counselorRow-'+id.split('-')[1]).children('td').each(function () {
        if(i>2){
          return;
        }else{
          temp = $(this).text();
          switch (i) {
            case 0:
              firstname = temp.split(" ")[1];
              lastname = temp.split(" ")[0];
              //console.log(name);
              $(this).html("<input type='text' required maxlength='100' class='form-control' id = 'editCounselorsLastName-"+i+"' value = '"+lastname.replace(",", "")+"' form='counselorEditForm'>"+
                           "<input type='text' required maxlength='100' class='form-control' id = 'editCounselorsFirstName-"+i+"' value = '"+firstname+"' form='counselorEditForm'>");
              counselorObj.firstName = firstname;
              counselorObj.lastName = lastname.replace(",", "");
            break;
            case 1:
              $(this).html("<select required class='form-control' id = 'editCounselors-"+i+"' form='counselorEditForm'>"+
                           "<option value = 'universityCounselor'>University Counselor</option>"+
                           "<option value = 'counselor'>Counselor</option></select>");
              if(temp == "University Counselor"){
                $("#editCounselors-"+i).val("universityCounselor");  
              }else{
                $("#editCounselors-"+i).val("counselor");
              }
              counselorObj.counselorType = temp;
            break;
            case 2:
              var check = false;
              if($(this).html()!=""){
                check = true;
              }
              counselorObj.designation = temp;
              $(this).html("<select required class='form-control' id = 'editCounselors-"+i+"' form='counselorEditForm'></select>");
              
              $("#editCounselors-"+i).html($("#counselorDesignation").html());
              if(check){
                $("#editCounselors-"+i).val($(this).attr("data-value"));
              }else{
                $("#editCounselors-"+i).val("");
              }
              //$("#counselorDesignation").html();
            break;
          }
        }
        i++;
      });
      //console.log(id);

      $('#'+id).html("save");
      $('#'+id).attr("form", 'counselorEditForm');
      return false;
    }else{
      $('#counselorEditForm').parsley().validate();
      $('#counselorEditForm').trigger('submit');
    }
  });
  $("#counselorEditForm").on('submit', function(e){
    e.preventDefault();
    //$("#counselorDesignation").removeAttr("disabled");
    $('#'+counselorid).html("<i class='fa fa-edit'></i>");
    $('#'+counselorid).attr("form", '');
    
    var myform = document.getElementById('counselorEditForm');
    var editform = new FormData(myform);
    editform.append('firstName', $('#editCounselorsFirstName-0').val());
    editform.append('lastName', $('#editCounselorsLastName-0').val());
    editform.append('designation', $('#editCounselors-2').val());
    editform.append('counselorType', $('#editCounselors-1').val());
    editform.append('id', counselorid.split('-')[1]);
    var i=0;
    new Promise((res, rej)=>{
      prependPostAjaxCallback("/counselor/edit", editform, '',
      'Counselor Successfully Saved!', function(value){
        //console.log(value);
        if(value){
          res(true);
        }else{
          rej(false);
        }
      });
    }).then((res)=>{
      $('#counselorRow-'+counselorid.split('-')[1]).children('td').each(function () {
        if(i>2){
          return;
        }
        switch (i) {
          case 0:
            $(this).html($('#editCounselorsLastName-0').val()+", "+$('#editCounselorsFirstName-0').val());
            break;
          case 1:
            $(this).html($('#editCounselors-1').val().capitalize());
            break;
          case 2:
            $(this).html($("#editCounselors-2 option[value='"+$('#editCounselors-2').val()+"']").text());
          break;
        }
        i++;
      });
    }).catch((err)=>{
      $('#counselorRow-'+counselorid.split('-')[1]).children('td').each(function () {
        if(i>2){
          return;
        }
        switch (i) {
          case 0:
          //alert("Hello");
            $(this).html(counselorObj.lastName +", "+counselorObj.firstName);
            break;
          case 1:
            $(this).html(counselorObj.counselorType);
            break;
          case 2:
            $(this).html(counselorObj.designation);
          break;
        }
        i++;
      });
    });
  });
  $("#counselorTable").on('click', '.deleteCounselor', function(){
    counselorid= $(this).prop("id").split("-")[1];
    mydelete = "deleteCounselor";
    $("#deleteModal").modal('show');
    $("#deleteMessage").html("<h3>Are you sure you want to set this councelor to inactive?</h3>");
  });
  $("#counselorTable").on('click', '.restoreCounselor', function(){
    counselorid= $(this).prop("id").split("-")[1];
    $.get('/counselor/restore/'+counselorid, function(data){
      if(data != 'true'){
        new PNotify({
          title: 'Failed',
          text: 'Delete failed because it has existing data in other table.',
          styling: 'bootstrap3'
        });
      }else{
        new PNotify({
          title: 'Warning',
          text: 'Counselor successfully set to active.',
          styling: 'bootstrap3'
        });
        //alert(counselorid);
      $("#deleteCounselor-"+counselorid).closest("td").html("<button class='btn btn-danger deleteCounselor' id = 'deleteCounselor-"+counselorid+"'>"+
                                                              "<i class='fa fa-trash-o'></i>"+
                                                              "</button>");
      }
    });
  });
  $("#counselorType").on('change', function(){
    if($(this).val() == "universityCounselor"){
      $("#counselorDesignation").attr("disabled", true);
    }else{
      $("#counselorDesignation").attr("disabled", false);
    }
  });
  $.get('/adminsettings/get', function(data){
    //console.log(data);
    if(data.check != "null"){
      $("#guidanceNameTextManageLayout").val(data.guidanceName);
      $("#schoolNameTextManageLayout").val(data.schoolName);
      $("#streetTextManageLayout").val( data.streetAddress);
      $("#barangayTextManageLayout").val(data.barangay);
      $("#cityTextManageLayout").val(data.city);
      $("#schoolSealImage").attr("src", "/images-database/adminsetting/schoolimage/"+data.id+"."+data.schoolSealImageType);
      $("#guidanceSealImage").attr("src", "/images-database/adminsetting/guidanceimage/"+data.id+"."+data.guidanceSealImageType);
      $("#consecutiveContract").val(data.contract);
      $("#schoolSeal").attr("required", false);
      $("#guidanceSeal").attr("required", false);
    }
  });
});
document.getElementById('guidanceSeal').onchange = function(e) {
  // Get the first file in the FileList object
  var imageFile = this.files[0];
  // get a local URL representation of the image blob
  var url = window.URL.createObjectURL(imageFile);
  // Now use your newly created URL!
  var someImageTag = document.getElementById('guidanceSealImage');
  someImageTag.src = url;
}
document.getElementById('schoolSeal').onchange = function(e) {
  // Get the first file in the FileList object
  var imageFile = this.files[0];
  // get a local URL representation of the image blob
  var url = window.URL.createObjectURL(imageFile);
  // Now use your newly created URL!
  var someImageTag = document.getElementById('schoolSealImage');
  someImageTag.src = url;
}
String.prototype.capitalize = function() {
  return this.charAt(0).toUpperCase() + this.slice(1);
}
