<div id="contactNoModal" class="modal fade" role="dialog" >
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content" style = 'z-index:200;'>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Contact No.</h4>
      </div>
      <div class="modal-body">
        <div class='container-fluid'>
          <div class='row'>
            <div class='col-md-12'>
              <div>
                <ul class="nav nav-tabs">
                  <li class="active"><a data-toggle="tab" href="#studentContactNoTab">Student Contact No.</a></li>
                  <li><a data-toggle="tab" href="#studentWithGuardianContactNoTab">Student With Family Contact No.</a></li>
                </ul>
              </div>
              <div class="tab-content">
                <div class='tab-pane fade in active' id = 'studentContactNoTab'>
                  <div class='container-fluid'>
                    <div class='row'>
                      <div class='col-md-6'>
                        <label>
                          View By Student Type
                        </label>
                        <select class='form-control' id ='selectByStudentTypeRecipent'>
                          <option value ='all'>all</option>
                          <option value ='college'>College</option>
                          <option value ='seniorhigh'>Senior High</option>
                          <option value ='juniorhigh'>Junior High</option>
                          <option value ='elementary'>Elementary</option>
                        </select>
                      </div>
                      <div class='col-md-6' style = 'padding-top:24px;'>
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control" id = 'searchStudentsRecipient' placeholder="Search By Student Name"/>
                            <i class="fa fa-search form-control-feedback"></i>
                        </div>
                      </div>
                    </div>
                    <div class='row'>
                      <div class='tableModalContainer'>
                        <table class='table table-striped'>
                          <thead>
                            <tr>
                              <th>Student Name</th>
                              <th>Student Contact No.</th>
                              <th>Select</th>
                            </tr>
                          </thead>
                          <tbody id ='selectStudentsForRecipent'>
                          </tbody>
                        </table>
                      </div>
                      <!--div class='genericDivTop alignRight'>
                        <button class='btn btn-dark'>Add Recipient</button>
                      </div-->
                      <div class='genericDivTop tableModalContainer'>
                        <strong>Selected Recipients</strong>
                        <table class='table table-striped'>
                          <thead>
                            <tr>
                              <th>Student Name</th>
                              <th>Student Contact No.</th>
                              <th>Remove</th>
                            </tr>
                          </thead>
                          <tbody id ='tableForStudentRecipent'>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
                <div class='tab-pane fade' id = 'studentWithGuardianContactNoTab'>
                  <div class='container-fluid'>
                    <div class='row'>
                      <div class='col-md-6'>
                        <label>
                          View By Student Type
                        </label>
                        <select class='form-control' id ='viewBtStudentGuardianType'>
                          <option value ='all'>all</option>
                          <option value ='college'>College</option>
                          <option value ='seniorhigh'>Senior High</option>
                          <option value ='juniorhigh'>Junior High</option>
                          <option value ='elementary'>Elementary</option>
                        </select>
                      </div>
                      <div class='col-md-6' style = 'padding-top:24px;'>
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control" id = 'searchGuardianRecipient' placeholder="Search By Student Name Or Guardian Name"/>
                            <i class="fa fa-search form-control-feedback"></i>
                        </div>
                      </div>
                    </div>
                    <div class='row'>
                      <div class='tableModalContainer'>
                        <table class='table table-striped'>
                          <thead>
                            <tr>
                              <th>Student Name</th>
                              <th>Student's Family Name</th>
                              <th>Relationship</th>
                              <th>Family Contact No.</th>
                              <th>Select</th>
                            </tr>
                          </thead>
                          <tbody id ='tableForGuardianSelectRecipent'>
                          </tbody>
                        </table>
                      </div>
                      <!--div class='genericDivTop alignRight'>
                        <button class='btn btn-dark'>Add Recipient</button>
                      </div-->
                      <div class='genericDivTop tableModalContainer'>
                        <strong>Selected Recipients</strong>
                        <table class='table table-striped'>
                          <thead>
                            <tr>
                              <th>Student Name</th>
                              <th>Student's Family Name</th>
                              <th>Relationship</th>
                              <th>Family Contact No.</th>
                              <th>Remove</th>
                            </tr>
                          </thead>
                          <tbody id ='tableForGuardianRecipent'>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-danger" id ='addAllRecipients'><i class='fa fa-add'></i>&nbsp;Add Recipient</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>

  </div>
</div>
@component('components.modal')
  @slot('modalid')
    statusMessageModal
  @endslot
  @slot('modalsize')
    modal-sm
  @endslot
  @slot('modaltitle')
    Information
  @endslot
  @slot('modalcontent')
    <div id='messageStatus'>
      <h2>Message is now sending. Please wait, while we send all the messages. This may take a few minutes.</h2>
    </div>
    <div id='messageLoader'>
      
    </div>
  @endslot
  @slot('modalbutton')
      
  @endslot
@endcomponent

