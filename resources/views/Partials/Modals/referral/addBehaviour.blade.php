@component('components.modal')
  @slot('modalid')
    addBehaviourModal
  @endslot
  @slot('modalsize')
    modal-md
  @endslot
  @slot('modaltitle') 
    Add Behaviour
  @endslot
  @slot('modalcontent')
    <div class='row'>
        {!! Form::open([
          'id'=>'behaviorAddForm']) !!}
        
      <div class='col-md-6'>
        <label for = 'observableBehaviourText'>Observable Behavior</label>
        <select id = 'observableBehaviourText' name = 'observableBehaviourText'
        class='form-control' required>
          
        </select>
        {{--  <input type ='text' id = 'observableBehaviourText' name = 'observableBehaviourText'
        class='form-control' required>  --}}
        <label for = 'frequencyText'>Frequency</label>
        <input type ='text' id = 'frequencyText' name = 'frequencyText'
        class='form-control' required>
      </div>
      <div class='col-md-6'>
        <label for='behaviourRemarksText'>Remarks</label>
        <textarea id ='behaviourRemarksText' name ='behaviourRemarksText' class='form-control' rows='4' required></textarea>
      </div>
      {!! Form::close() !!}
    </div>
  @endslot
  @slot('modalbutton')
      <button form ='behaviorAddForm' class="btn btn-warning" id ='addBehaviourButton'><i class='fa fa-save'></i>&nbsp;Save</button>
  @endslot
@endcomponent
