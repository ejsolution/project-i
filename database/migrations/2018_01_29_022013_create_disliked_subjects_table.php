<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDislikedSubjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('disliked_subjects', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('studentID')->unsigned();
            $table->foreign('studentID')->references('id')->on('students');
            $table->integer('subjectID')->unsigned();
            $table->foreign('subjectID')->references('id')->on('subjects');
            $table->mediumText('remarks')->nullable();
            $table->decimal('grade', 8, 2);
            $table->date('dateAssigned');
            $table->timestamps();
        });
        Schema::table('fav_subject_lists', function (Blueprint $table) {
            $table->decimal('grade', 8, 2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('disliked_subjects');
    }
}
