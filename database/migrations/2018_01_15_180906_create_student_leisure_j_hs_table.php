<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentLeisureJHsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_leisure_j_hs', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('JHStudentID')->unsigned();
          $table->foreign('JHStudentID')->references('id')->on('j_h_students');
          $table->integer('average');
          $table->integer('rankClass');
          $table->string('schoolChoice');
          $table->string('getInfo')->nullable();
          $table->string('financialSupport');
          $table->mediumText('selfEval1');
          $table->mediumText('selfEval2');
          $table->mediumText('selfEval3');
          $table->mediumText('selfEval4');
          $table->mediumText('selfEval5');
          $table->mediumText('selfEval6');
          $table->mediumText('otherRemarks');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_leisure_j_hs');
    }
}
