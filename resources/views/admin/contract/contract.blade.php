@extends('admin.adminTemplate')

@section('title', 'AdminDashboard')
@section('stylesheet')
  <link href="{{asset('css/admin/transactions.css')}}" rel="stylesheet">
  <link href="{{asset('css/admin/contract/contract.css')}}" rel="stylesheet">
@stop
@section('content')
  <div class='container'>
    <input type = 'hidden' value = "{{$viewContract}}" id = 'viewContract'>
    <input type = 'hidden' value = "{{$contractID}}" id = 'contractIDView'>
    @component('components.headerSearchFilter', ['user'=>$user,
                                                'collegeOfUser'=>$collegeOfUser])
      @slot('comboLabel')
        View By Student Type 
      @endslot 
      @slot('schoolyears')
        @foreach($schoolyears as $year)
          <option value ='{{$year->id}}'>{{explode('-', $year->schoolYearStart)[0]}} - {{explode('-', $year->schoolYearEnd)[0]}}</option>
        @endforeach
      @endslot
      @slot('searchID')
        searchContract
      @endslot
      @slot('placeholder')
        Search By Student Or School Year
      @endslot
      @slot('button')
        <button class='btn btn-block btn-dark' id = 'addStudentButton'>Add</button>
      @endslot
    @endcomponent
    <div class='row'>
      <div class='col-md-12'> 
        <div>
          <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#forContract">Contract</a></li>
            <li><a data-toggle="tab" href="#consecutiveContracts">Consecutive Contracts</a></li>
            <!--li><a data-toggle="tab" href="#listOfStudents">List Of Students with Contract</a></li-->
          </ul>
        </div>
      </div>
    </div>
    <div class='tab-content'>
      <div class='tab-pane fade in active' id = 'forContract'>
        <div class='row'>
          <div class='col-md-12 tableContainer'>
            <table class='table table-striped'>
              <thead>
                <tr>
                  <th>No</th>
                  <th>Student</th>
                  <th>Date Of Contract</th>
                  <th>School Year</th>
                  <th>Type Of Contract</th> 
                  <th>Semester</th>
                  <th>Course</th>
                  <th>View</th>
                </tr>
              </thead>
              <tbody id ='contractTable'>
              </tbody>
            </table>
          </div> 
        </div>
        <div class='genericDivTop'>
          <center><ul id="pagination" class="pagination-sm"></ul></center>
        </div>
        <div class='alignRight'>
          <button class='btn btn-dark' id = 'printContractStudents'>
            <i class='fa fa-print'>&nbsp; Print</i>
          </button>
        </div>
      </div>
      <div class='tab-pane fade' id = 'consecutiveContracts'>
          <div class='row'>
            <div class='col-md-12 tableContainer'>
              <table class='table table-striped'>
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Student</th>
                    <th>Remarks</th>
                    <th>Consecutive Contracts</th>
                  </tr>
                </thead>
                <tbody id ='consecutiveContractsTable'>
                </tbody>
              </table>
            </div>  
          </div>
          <div class='genericDivTop'>
            <center><ul id="pagination" class="pagination-sm"></ul></center>
          </div>

        </div>
      <!--div class='tab-pane fade' id = 'listOfStudents'>
      </div-->
    </div>
    <div class='row'>
      <div class='alignRight col-md-12'>
        {{--  <button class='btn btn-dark'>
          <i class='fa fa-print'>&nbsp; Print</i> 
        </button>  --}}
      </div>
    </div>
  </div>
  @include('Partials.Modals.contract.addContract')
  @include('Partials.Modals.contract.studentBrowse')
@endSection
@section('scripts')
  <script src = '{{asset('js/parsley.min.js')}}'></script>
  <script src = '{{asset('js/jqueryPagination.js')}}'></script>
  <script src = '{{asset('js/contract/contract.js')}}'></script>
@stop
