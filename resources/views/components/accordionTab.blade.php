
  <div class='panel'>
    <a class="panel-heading" role="tab" id="{{$headerid}}" data-toggle="collapse" data-parent="#{{$accordionid}}" href="#{{$bodyid}}" aria-expanded="false" aria-controls="{{$bodyid}}">
      <h4 class="panel-title">{{$title}}</h4>
    </a>
    <div id="{{$bodyid}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="{{$headerid}}" aria-expanded="false" style="">
      <div class="panel-body">
        {{$content}}
      </div>
    </div>
  </div>
