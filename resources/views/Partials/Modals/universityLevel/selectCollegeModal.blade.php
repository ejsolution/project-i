@component('components.modal')
  @slot('modalid')
    selectCollegeModal
  @endslot
  @slot('modalsize')
    modal-md
  @endslot
  @slot('modaltitle')
    college
  @endslot
  @slot('modalcontent')
    <div class='row'>
      <div class="form-group has-feedback">
          <input type="text" class="form-control"
          placeholder="Search College Code Or College Name" id = 'selectCollegeSearch'/>
          <i class="fa fa-search form-control-feedback"></i>
      </div>
    </div>
    {!! Form::open([
            'id'=>'collegeSelectForm']) !!}

    {!! Form::close() !!}
    <div class='row tableModalBigContainer'>
      <table class='table table-striped'>
        <thead>
          <tr>
            <th>College Code</th>
            <th>College Name</th>
            <th>Assign</th>
          </tr>
        </thead>
        <tbody id = 'collegeSelectTable'>
        </tbody>
      </table>
    </div>
  @endslot
  @slot('modalbutton')

  @endslot
@endcomponent
