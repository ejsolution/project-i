$(document).ready(function(){
    
    $('#editProfile').on('click', function(){
        $("#editProfileForm :input").attr("disabled", false);
    });
    $('#editProfileForm').on('submit', function(e){
        e.preventDefault();
        var form = document.getElementById('editProfileForm');
        var myform = new FormData(form);
        prependPostAjaxErrorCallback('/userprofile/edit', myform, function(){
            $("#editProfileForm :input").attr("disabled", true);
            new PNotify({
                title: 'Success!',
                text: 'Profile saved!',
                type: 'success',
                styling: 'bootstrap3'
              });
        });
    });
    $('#changePasswordLink').on('click', function(){
        $('#changePasswordModal').modal('show');
    });
    $('#changePasswordForm').on('submit', function(e){
        e.preventDefault();
        var password = document.getElementById("newPassword")
        , confirm_password = document.getElementById("confirmPassword");

        if(password.value != confirm_password.value) {
            confirm_password.setCustomValidity("Passwords Don't Match");
            return;
        } else {
            confirm_password.setCustomValidity('');
        }
        var form = document.getElementById('changePasswordForm');
        var myform = new FormData(form);
        prependPostAjaxErrorCallback('/userprofile/changepassword', myform, function(data){
            //console.log(data);
            if(data == "Incorrect password!"){
                new PNotify({
                    title: 'Error!',
                    text: data,
                    type: 'error',
                    styling: 'bootstrap3'
                });
            }else{
                new PNotify({
                    title: 'Success!',
                    text: 'Password changed!',
                    type: 'success',
                    styling: 'bootstrap3'
                });
                $('#changePasswordForm :input').val();
                $('#changePasswordModal').modal("hide");
            } 
        });      
    });
});
document.getElementById('uploadProfileImage').onchange = function(e) {
    // Get the first file in the FileList object
    var imageFile = this.files[0];
    // get a local URL representation of the image blob
    var url = window.URL.createObjectURL(imageFile);
    // Now use your newly created URL!
    var someImageTag = document.getElementById('profileImage');
    someImageTag.src = url;
}