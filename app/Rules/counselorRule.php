<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\model\counselor;
class counselorRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if($value == 'universityCounselor'){
            $counselor = counselor::where('counselorType', '=', 'universityCounselor')
                                    ->get();
            if(count($counselor)>0){
                return false;
            }else{
                return true;
            }
        }else{
            return true;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'There should be only one university counselor.';
    }
}
