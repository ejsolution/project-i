@component('components.modal')
    @slot('modalid')
    counselorSettingModal
    @endslot
    @slot('modalsize')
    modal-lg
    @endslot
    @slot('modaltitle')
    Counselors
    @endslot
    @slot('modalcontent')
    {!! Form::open([
    'id'=>'counselorAddForm']) !!}
    @component('components.x_panel')
        @slot('title')
        Add A Counselor
        @endslot 
        @slot('content')
         <div class='row'>
            <div class='col-md-6'>
                <label for = 'counselorFirstName'>First Name</label>
                <input type = 'text' id = 'counselorFirstName' name = 'firstName' required class='form-control'>
            </div>
            <div class='col-md-6'>
                <label for = 'counselorLastName'>Last Name</label>
                <input type = 'text' id = 'counselorLastName' name = 'lastName' required class='form-control'>
            </div>
         </div>
        <div class='row'>
            <div class='col-md-6'>
                <label for = 'counselorType'>Counselor Type</label>
                <select id = 'counselorType' name = 'counselorType' class='form-control' required>
                    <option value = 'universityCounselor'>University Counselor</option>
                    <option value = 'counselor'>Counselor</option>
                </select>
            </div>
            <div class='col-md-6'>
                <label for = 'counselorDesignation'>Designation</label>
                <select id = 'counselorDesignation' name = 'designation' class='form-control'>
                    
                </select>
            </div>
        </div>
        <div class='row'>
            <div class='col-md-12 genericDivTop alignRight'>
                <button class='btn btn-dark'>Save</button>
            </div>
        </div>
        @endslot
    @endcomponent
    {!! Form::close() !!}
    {!! Form::open([
        'id'=>'counselorEditForm']) !!}
    {!! Form::close() !!}
    <table class='table table-striped'>
        <thead>
            <tr>
                <th>Name</th>
                <th>Counselor Type</th>
                <th>Designation</th>
                <th>Edit</th>
                <th>Set Active/Inactive</th>
            </tr>
        </thead>
        <tbody id ='counselorTable'>
        </tbody>
    </table>
    @endslot
    @slot('modalbutton')
   
    @endslot
@endcomponent