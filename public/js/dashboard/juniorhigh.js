$(document).ready(function(){
    tableLoader('5', 'juniorHighStudentsTableLists');
    $('#searchJuniorHighStudent').closest('div').parent().next().remove();
    $('#searchJuniorHighStudent').closest('div').parent().removeClass('col-md-3').addClass('col-md-4');
    $('#studentTypeFilter').html("<option value ='juniorhigh'>Junior High</option>").attr('readonly', true);
    $('#semesterFilter').closest('div').remove();
    $('#schoolYearFilter').children('option').each(function(){
        var temp = $(this).text().split(' ');
        console.log(temp);
        if(temp.length == 4){
           // alert("Hello");
            $('#schoolYearFilter').val($(this).val());
        }
    });
    getAjax('/dashboard/juniorhighstudents/'+$('#schoolYearFilter').val()+'/all/'+$('#courseGradeLevelFilter').val()+'/'+$('#searchJuniorHighStudent').val(), 'juniorHighStudentsTableLists');
    var html = "<option value ='all'>all</option>"+
                "<option value ='10'>10</option>"+
                "<option value ='9'>9</option>"+
                "<option value ='8'>8</option>"+
                "<option value ='7'>7</option>";
            $('#courseGradeLevelFilter').html(html);
    $('#schoolYearFilter').on('change', function(){
        if($('#schoolYearFilter').val() != "all"){
          $.get('/report/schoolyear/'+$('#schoolYearFilter').val(), function(data){
            $('#semesterFilter').html("<option value='all'>all</option>");
            $('#semesterFilter').append(data);
            tableLoader('5', 'juniorHighStudentsTableLists');
            getAjax('/dashboard/juniorhighstudents/'+$('#schoolYearFilter').val()+'/all/'+$('#courseGradeLevelFilter').val()+'/'+$('#searchJuniorHighStudent').val(), 'juniorHighStudentsTableLists');
          });
        }else{
            tableLoader('5', 'juniorHighStudentsTableLists');
            getAjax('/dashboard/juniorhighstudents/'+$('#schoolYearFilter').val()+'/all/'+$('#courseGradeLevelFilter').val()+'/'+$('#searchJuniorHighStudent').val(), 'juniorHighStudentsTableLists');
        }
    });
    $('#semesterFilter').on('change', function(){
        tableLoader('5', 'juniorHighStudentsTableLists');
        getAjax('/dashboard/juniorhighstudents/'+$('#schoolYearFilter').val()+'/all/'+$('#courseGradeLevelFilter').val()+'/'+$('#searchJuniorHighStudent').val(), 'juniorHighStudentsTableLists');
    });
    $('#courseGradeLevelFilter').on('change', function(){
        tableLoader('5', 'juniorHighStudentsTableLists');
        getAjax('/dashboard/juniorhighstudents/'+$('#schoolYearFilter').val()+'/all/'+$('#courseGradeLevelFilter').val()+'/'+$('#searchJuniorHighStudent').val(), 'juniorHighStudentsTableLists');
    });
    $('#searchJuniorHighStudent').on('keydown', function(){
        tableLoader('5', 'juniorHighStudentsTableLists');
        getAjax('/dashboard/juniorhighstudents/'+$('#schoolYearFilter').val()+'/all/'+$('#courseGradeLevelFilter').val()+'/'+$('#searchJuniorHighStudent').val(), 'juniorHighStudentsTableLists');
    }); 
});