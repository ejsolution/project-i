<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateElemStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('elem_students', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('studentID')->unsigned();
          $table->foreign('studentID')->references('id')->on('students');
          $table->string('studNo');
          $table->integer('schoolYearID')->unsigned();
          $table->foreign('schoolYearID')->references('id')->on('school_years');
          $table->string('gradeLevel');
          $table->date('dateEnrolled');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('elem_students');
    }
}
