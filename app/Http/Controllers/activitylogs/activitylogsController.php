<?php

namespace App\Http\Controllers\activitylogs;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\notification\notificationController;
use App\model\activityLogs;
use App\model\systemDate;
use PDF;
class activitylogsController extends Controller
{
    public function __construct(){
        $this->middleware('auth:myuser');
    }
    public function getActivityLogsPage(){
        $notif = (new notificationController)->getNotifs();
        return view('admin/activitylogs/activitylogs', ['notif'=>$notif]);
    }
    public function totalRows($search = ''){
        //$systemdate = systemDate::all();
        $activitylogs = activityLogs::where(function($query) use ($search){
                                            $query->where('name', 'LIKE', "%$search%");
                                            $query->orWhere('activity', 'LIKE', "%$search%");
                                            $query->orWhere('created_at', 'LIKE', "$search%");
                                        })
                                        //->where('created_at', '<=', $systemdate[0]->systemDate)
                                      ->count();
        return $activitylogs;
    }
    public function activityRow($activity){
        return "<tr>
            <td>$activity->name</td>
            <td>$activity->created_at</td>
            <td>$activity->activity</td>
        </tr>";
    }
    public function getActivityLogs($page, $search=''){
        $skip = ($page-1)*10;
        //$systemdate = systemDate::all();
        $activitylogs = activityLogs::where(function($query) use ($search){
                                        $query->where('name', 'LIKE', "%$search%");
                                        $query->orWhere('activity', 'LIKE', "%$search%");
                                        $query->orWhere('created_at', 'LIKE', "$search%");
                                      })
                                      //->where('created_at', '<=', $systemdate[0]->systemDate)
                                      ->limit(10)->offset($skip)
                                      ->orderBy('id', 'desc')->get();
        if(count($activitylogs)==0){
            return "<h2>There are no activity logs yet</h2>";
        }else{
            $html = "";
            foreach ($activitylogs as $activity) {
                $html.=$this->activityRow($activity);
            }
            return $html;
        }
    }
    public function print(){
        $html = "
            <html>
                <head>
                    <style>
                        table {
                            font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif;
                            border-collapse: collapse;
                            width: 100%;
                        }
                        
                        table td, table th {
                            border: 1px solid #ddd;
                            padding: 8px;
                        }
                        
                        table tr:nth-child(even){background-color: #f2f2f2;}
                        
                        table tr:hover {background-color: #ddd;}
                        
                        table th {
                            padding-top: 12px;
                            padding-bottom: 12px;
                            text-align: left;
                            background-color: #4CAF50;
                            color: white;
                        }
                    </style>
                </head>
                <body>
                    <div style = 'margin-bottom:20px;'>
                        <center><h3>Activity Logs</h3></center>
                    </div>
                    <table>
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Date & Time</th>
                                <th>Activity</th>
                            </tr>
                        </thead>
                        <tbody> 
        ";
        $activity = activityLogs::all();
        foreach ($activity as $activities) {
            $html.=$this->activityRow($activities);
        }
        $html.="</tbody>
            </table>
            </body>
            </html>";
        PDF::loadHTML($html)->save(public_path("temp/".'report.pdf'));
        //return redirect('temp/companylogo.png');
        return redirect('temp/report.pdf');
    }
}
