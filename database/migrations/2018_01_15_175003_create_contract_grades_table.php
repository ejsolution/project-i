<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractGradesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contract_grades', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('contractID')->unsigned();
            $table->foreign('contractID')->references('id')->on('contracts');
            $table->decimal('grade',8,2);
            $table->string('subjectCode');
            $table->string('units');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contract_grades');
    }
}
