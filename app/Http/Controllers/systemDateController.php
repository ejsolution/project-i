<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\model\systemDate;
use App\model\schoolYear;
use session;
class systemDateController extends Controller
{
    public function updateSystemDate(Request $request){
        $this->validate($request, [
            'systemDate'=>'required'
        ]);
        $schoolyear = schoolYear::where('schoolYearStart', '<=', $request->systemDate)
        ->where('schoolYearEnd', '>=',$request->systemDate)->get();
        if(count($schoolyear)>0){
            $schoolyeartemp = schoolYear::where('status', '=','1')->get();
            if(count($schoolyeartemp)>0){
              if($schoolyeartemp[0]->id != $schoolyear[0]->id){
                $schoolyeartemp[0]->status = '0';
                $schoolyeartemp[0]->save();
              }
            }
            $schoolyear[0]->status = '1';
            $schoolyear[0]->save();   
        }
        $systemdate  = systemDate::all();
        $systemdate[0]->systemDate = $request->systemDate;
        $systemdate[0]->save();
        session(['systemDate'=>$systemdate[0]->systemDate]);
        return "saved!";
    }
}
