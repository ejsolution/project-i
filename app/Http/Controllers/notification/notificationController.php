<?php

namespace App\Http\Controllers\notification;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\users\userController;
use App\Http\Controllers\student\studentController;
use App\model\notification;
use App\model\systemDate;
use App\model\useraccount;
use App\model\student;
use App\model\schoolYear;
use App\model\counseling;
use App\model\collegeStudent;
use App\model\contractSetting;
use Session;
use Auth;
use DB;
class notificationController extends Controller
{
    public function insertCounselingNotif($id, $scheduleddate){
        
        $systemdate =  systemDate::all();
        $user = (new userController)->getUsersOfSameDesignation(Auth::guard('myuser')->id());
        if($scheduleddate ==  $systemdate[0]->systemDate){
            $notif =  new notification;
            $notif->designationTo = $user['designation'];
            $notif->designationLevelTo = $user['level'];
            $notif->message = "A counseling is scheduled for today!";
            $notif->type = 'counseling';
            $notif->counselingID = $id;
            $notif->accepted = 0;
            $notif->created_at = $systemdate[0]->systemDate;
            $notif->save();
        }
    }
    public function checkCounselingsForNotif(){
        $systemdate =  systemDate::all();
        $sql = "SELECT COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) 
                        semesterID, 
                COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, 
                elem.schoolYearID)                                           schoolYearID 
                , col.collegeName, col.collegeID,
                COALESCE(col.studentType, shs.studentType, jhs.studentType, 
                elem.studentType) 
                studentTypes, 
                COALESCE(col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) 
                        courseGrade, 
                COALESCE(col.studentno, shs.studNo, jhs.studNo, elem.studNo) studentNo, 
                counselings.* 
                FROM   students 
                LEFT JOIN (SELECT college_students.studentID, 
                college_students.yearLevel, 
                college_students.semesterID, 
                college_students.schoolYearID, 
                'college' AS studentType, 
                college_students.courseID, 
                courses.courseName, 
                college_students.studentNo,
                colleges.collegeName, colleges.id AS collegeID
                FROM   college_students 
                INNER JOIN (SELECT MAX(id) AS maxid 
                FROM   college_students 
                GROUP  BY studentID) cs 
                ON cs.maxid = college_students.id 
                INNER JOIN courses 
                ON courses.id = college_students.courseID
                INNER JOIN (SELECT courseID, id, departmentID FROM 
                course_department_hists INNER JOIN (SELECT MAX(id) AS 
                maxid FROM course_department_hists GROUP BY courseID) cdh ON cdh.maxid 
                = course_department_hists.id) cdh2 ON cdh2.courseID = courses.id 
                INNER JOIN (SELECT departmentID, collegeID, id FROM department_college_hists 
                INNER JOIN (SELECT MAX(id) AS maxid FROM department_college_hists GROUP BY 
                departmentID) dch ON dch.maxid = department_college_hists.id) dch2 ON 
                dch2.departmentID = cdh2.departmentID
                INNER JOIN colleges ON colleges.id = dch2.collegeID) col 
                ON col.studentID = students.id 
                LEFT JOIN (SELECT studentID, 
                semesterID, 
                schoolYearID, 
                id, 
                'seniorhigh' AS studentType, 
                gradeLevel, 
                studNo 
                FROM   s_h_s_students 
                INNER JOIN (SELECT MAX(id) AS maxid 
                FROM   s_h_s_students 
                GROUP  BY studentID) shs 
                ON shs.maxid = s_h_s_students.id) shs 
                ON shs.studentID = students.id 
                LEFT JOIN (SELECT studentID, 
                NULL         AS semesterID, 
                schoolYearID, 
                id, 
                'juniorhigh' AS studentType, 
                gradeLevel, 
                studNo 
                FROM   j_h_students 
                INNER JOIN (SELECT MAX(id) AS maxid 
                FROM   j_h_students 
                GROUP  BY studentID) jhs 
                ON jhs.maxid = j_h_students.id) jhs 
                ON jhs.studentID = students.id 
                LEFT JOIN (SELECT studentID, 
                NULL         AS semesterID, 
                schoolYearID, 
                id, 
                'elementary' AS studentType, 
                gradeLevel, 
                studNo 
                FROM   elem_students 
                INNER JOIN (SELECT MAX(id) AS maxid 
                FROM   elem_students 
                GROUP  BY studentID) elem 
                ON elem.maxid = elem_students.id) elem 
                ON elem.studentID = students.id 
                INNER JOIN counselings 
                ON counselings.studentID = students.id 
                INNER JOIN school_years 
                ON school_years.id = counselings.schoolYearID 
                WHERE  Date(students.created_at) <= '".$systemdate[0]->systemDate."'
                AND DATE(counselings.created_at)  <= '".$systemdate[0]->systemDate."'
                AND school_years.status = '1'
                AND counselings.dateScheduled = '".$systemdate[0]->systemDate."'
                AND counselings.counselingStatus = '0'
                AND NOT EXISTS
                (
                SELECT  null 
                FROM    notifications d
                WHERE   d.counselingID = counselings.id
                )";
        $counselings = DB::select(DB::raw($sql));
        error_log("HELLO ".count($counselings));
        $myitems;
        $i=0;
        foreach ($counselings as $counseling){
            $myitems[$i]['designationTo']= $counseling->studentTypes;
            if( $counseling->studentTypes != "college"){
                $myitems[$i]['designationLevelTo']= $counseling->collegeID;
            }else{
                $myitems[$i]['designationLevelTo']= $counseling->courseGrade;
            }
            $myitems[$i]['message']= "A counseling is scheduled for today!";
            $myitems[$i]['type']= 'counseling';
            $myitems[$i]['counselingID']= $counseling->id;
            $myitems[$i]['accepted']= 0;
            $myitems[$i]['created_at']= $systemdate[0]->systemDate;
            $i++;
        }
        if($i>0){
            DB::table("notifications")->insert($myitems);
        }
    }
    public function getNotifs(){
        $notif;
        $ignored;
        $notifContract;
        $systemdate =  systemDate::all();
        $sql = "SELECT COALESCE(col.semesterID, shs.semesterID, jhs.semesterID, elem.semesterID) 
                semesterID, 
                COALESCE(col.schoolYearID, shs.schoolYearID, jhs.schoolYearID, 
                elem.schoolYearID)                                           schoolYearID 
                , col.collegeName, col.collegeID,
                COALESCE(col.studentType, shs.studentType, jhs.studentType, 
                elem.studentType) 
                studentTypes, 
                COALESCE(col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) 
                        courseGrade, 
                COALESCE(col.studentno, shs.studNo, jhs.studNo, elem.studNo) studentNo, 
                notifications.* 
                FROM   students 
                LEFT JOIN (SELECT college_students.studentID, 
                college_students.yearlevel, 
                college_students.semesterID, 
                college_students.id AS collegeStudentID,
                college_students.schoolYearID, 
                'college' AS studentType, 
                college_students.courseID, 
                courses.coursename, 
                college_students.studentno,
                colleges.collegeName, colleges.id AS collegeID
                FROM   college_students 
                INNER JOIN (SELECT MAX(id) AS maxid 
                FROM   college_students 
                GROUP  BY studentID) cs 
                ON cs.maxid = college_students.id 
                INNER JOIN courses 
                ON courses.id = college_students.courseID
                INNER JOIN (SELECT courseID, id, departmentID FROM 
                course_department_hists INNER JOIN (SELECT MAX(id) AS 
                maxid FROM course_department_hists GROUP BY courseID) cdh ON cdh.maxid 
                = course_department_hists.id) cdh2 ON cdh2.courseID = courses.id 
                INNER JOIN (SELECT departmentID, collegeID, id FROM department_college_hists 
                INNER JOIN (SELECT MAX(id) AS maxid FROM department_college_hists GROUP BY 
                departmentID) dch ON dch.maxid = department_college_hists.id) dch2 ON 
                dch2.departmentID = cdh2.departmentID
                INNER JOIN colleges ON colleges.id = dch2.collegeID) col 
                ON col.studentID = students.id 
                LEFT JOIN (SELECT studentID, 
                semesterID, 
                schoolYearID, 
                id, 
                'seniorhigh' AS studentType, 
                gradeLevel, 
                studNo 
                FROM   s_h_s_students 
                INNER JOIN (SELECT MAX(id) AS maxid 
                FROM   s_h_s_students 
                GROUP  BY studentID) shs 
                ON shs.maxid = s_h_s_students.id) shs 
                ON shs.studentID = students.id 
                LEFT JOIN (SELECT studentID, 
                NULL         AS semesterID, 
                schoolYearID, 
                id, 
                'juniorhigh' AS studentType, 
                gradeLevel, 
                studNo 
                FROM   j_h_students 
                INNER JOIN (SELECT MAX(id) AS maxid 
                FROM   j_h_students 
                GROUP  BY studentID) jhs 
                ON jhs.maxid = j_h_students.id) jhs 
                ON jhs.studentID = students.id 
                LEFT JOIN (SELECT studentID, 
                NULL         AS semesterID, 
                schoolYearID, 
                id, 
                'elementary' AS studentType, 
                gradeLevel, 
                studNo 
                FROM   elem_students 
                INNER JOIN (SELECT MAX(id) AS maxid 
                FROM   elem_students 
                GROUP  BY studentID) elem 
                ON elem.maxid = elem_students.id) elem 
                ON elem.studentID = students.id";
               
        $sqlCounseling = $sql." "." INNER JOIN counselings 
                            ON counselings.studentID = students.id 
                            INNER JOIN school_years 
                            ON school_years.id = counselings.schoolYearID 
                            INNER JOIN notifications ON notifications.counselingID
                            = counselings.id
                            WHERE  Date(students.created_at) <= '".$systemdate[0]->systemDate."'
                            AND DATE(counselings.created_at)  <= '".$systemdate[0]->systemDate."'
                            AND school_years.status = '1'
                            AND counselings.dateScheduled = '".$systemdate[0]->systemDate."'";
        $sqlContract =  $sql." "." INNER JOIN contracts 
                        ON contracts.collegStudentID = col.collegeStudentID 
                        INNER JOIN school_years 
                        ON school_years.id = contracts.schoolYearID 
                        INNER JOIN notifications ON notifications.contractID
                        = contracts.id
                        WHERE  Date(students.created_at) <= '".$systemdate[0]->systemDate."'
                        AND DATE(contracts.created_at)  = '".$systemdate[0]->systemDate."'
                        AND school_years.status = '1'
                        AND NOT EXISTS
                 (
                 SELECT  null 
                 FROM    contract_remarks d
                 WHERE   d.contractID = contracts.id
                 )";
        if(session()->get('user')['userType']=='admin'){
            $notif = DB::select(DB::raw($sqlCounseling));
            $notifContract = DB::select(DB::raw($sqlContract));
            $sqlContract.=" AND notifications.type = 'contract'
            ";
            $sqlCounseling.=" AND notifications.accepted = '0' AND notifications.type = 'counseling'";
            //error_log("CONTRACT: ". count(DB::select(DB::raw($sqlContract))));
            $ignored =  count(DB::select(DB::raw($sqlCounseling)));
            $ignored += count(DB::select(DB::raw($sqlContract)));
        }else{
            $user = (new userController)->getUsersOfSameDesignation(Auth::guard('myuser')->id());
            //error_log("Hello ".$user['designation']);
            if($user['designation']=='college'){
                $sqlContract .= " AND col.collegeID = '".$user['level']."'";
                $sqlCounseling .= " AND col.collegeID = '".$user['level']."'";
                $notifContract = DB::select(DB::raw($sqlContract));
                $notif = DB::select(DB::raw($sqlCounseling));
                $sqlContract.=" AND notifications.type = 'contract'
                ";
                $sqlCounseling.=" AND notifications.accepted = '0' AND notifications.type = 'counseling'";
                $ignored =  count(DB::select(DB::raw($sqlCounseling)));
                $ignored += count(DB::select(DB::raw($sqlContract)));
            }else{
                $sqlContract    .= " AND COALESCE(col.studentType, shs.studentType, jhs.studentType, 
                elem.studentType) = '".$user['designation']."'
                AND COALESCE(col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) = 
                '".$user['level']."'";
                $sqlCounseling .= " AND COALESCE(col.studentType, shs.studentType, jhs.studentType, 
                elem.studentType) = '".$user['designation']."'
                AND COALESCE(col.courseID, shs.gradeLevel, jhs.gradeLevel, elem.gradeLevel) = 
                '".$user['level']."'";
                $notif = DB::select(DB::raw($sqlCounseling));
                $notifContract = DB::select(DB::raw($sqlContract));
                $sqlContract.=" AND notifications.type = 'contract'
                ";
                $sqlCounseling.=" AND notifications.accepted = '0' AND notifications.type = 'counseling'";
                $ignored =  count(DB::select(DB::raw($sqlCounseling)));
                $ignored += count(DB::select(DB::raw($sqlContract)));
            }
           
        }
        
        return ["ignored"=>$ignored, "notif"=>$notif, "notifContract"=>$notifContract];
    }
    public function redirectReschedNotification($id){
         $student = student::all();
        $schoolYearID = (new studentController)->getCurrentSchoolYear();
        $schoolyear = schoolYear::find($schoolYearID);
        $schoolyears = schoolYear::all();
        //$studentstable = student::all(); 
        $systemdate = systemDate::all(); 
        
        $nameofuser = useraccount::find(Auth::guard('myuser')->id());
       
        $user =  (new userController)->getUserDesignation(Auth::guard('myuser')->id());
        $college ='';
        if($user[0]=='college'){
            $college = DB::select(DB::raw("SELECT colleges.collegeName, courses.courseName, 
            courses.id FROM colleges INNER JOIN (SELECT collegeID, userID, id FROM 
            assignment_colleges INNER JOIN (SELECT MAX(id) AS maxid FROM assignment_colleges 
            GROUP BY userID) ac ON ac.maxid = assignment_colleges.id) ac2 ON ac2.collegeID = 
            colleges.id INNER JOIN (SELECT collegeID, departmentID, id FROM 
            department_college_hists INNER JOIN (SELECT MAX(id) as maxid FROM 
            department_college_hists GROUP BY departmentID) dch ON dch.maxid = 
            department_college_hists.id) dch2 ON dch2.collegeID = colleges.id 
            INNER JOIN (SELECT courseID, departmentID, id FROM course_department_hists 
            INNER JOIN (SELECT MAX(id) as maxid FROM course_department_hists GROUP BY courseID) 
            cdh ON cdh.maxid = course_department_hists.id) cdh2 ON cdh2.departmentID = 
            dch2.departmentID INNER JOIN courses on courses.id  = cdh2.courseID WHERE 
            ac2.collegeID = (SELECT collegeID FROM assignment_colleges WHERE id = 
            (SELECT MAX(id) FROM assignment_colleges WHERE userID ='".Auth::guard('myuser')->id()."'))
            GROUP BY courses.id"));
        }
        
        $notif = $this->getNotifs();
        
        $currentsem = (new studentController)->getCurrentSem();
        $counseling  = counseling::find($id);
        return view('admin/counseling/counseling', ['student' =>$student,
                                                    'schoolyear' =>explode('-', $schoolyear->schoolYearStart)[0].'-'.explode('-', $schoolyear->schoolYearEnd)[0],
                                                    'currentsem' =>$currentsem,
                                                    'schoolyears'=>$schoolyears,
                                                    'systemdate'=>$systemdate[0]->systemDate,
                                                    'nameofuser' => $nameofuser->firstName." ".$nameofuser->lastName,
                                                    'user'=>$user,
                                                    'collegeOfUser'=>$college,
                                                    'notif'=>$notif,
                                                    'resched'=>'true',
                                                    'accept'=>'false',
                                                    'view'=>'false',
                                                    'counselingID'=>$id,
                                                    'counselingType'=> $counseling->counselingType,
                                                    'studentID'=>$counseling->studentID]);
    }
    public function redirectAcceptNotification($counselingid, $notifid){
        $notif = notification::find($notifid);
        $notif->accepted = '1';
        $notif->save();
        $student = student::all();
        $schoolYearID = (new studentController)->getCurrentSchoolYear();
        $schoolyear = schoolYear::find($schoolYearID);
        $schoolyears = schoolYear::all();
        //$studentstable = student::all(); 
        $systemdate = systemDate::all(); 
        $nameofuser = useraccount::find(Auth::guard('myuser')->id());
        
        $user =  (new userController)->getUserDesignation(Auth::guard('myuser')->id());
        $college ='';
        if($user[0]=='college'){
            $college = DB::select(DB::raw("SELECT colleges.collegeName, courses.courseName, 
            courses.id FROM colleges INNER JOIN (SELECT collegeID, userID, id FROM 
            assignment_colleges INNER JOIN (SELECT MAX(id) AS maxid FROM assignment_colleges 
            GROUP BY userID) ac ON ac.maxid = assignment_colleges.id) ac2 ON ac2.collegeID = 
            colleges.id INNER JOIN (SELECT collegeID, departmentID, id FROM 
            department_college_hists INNER JOIN (SELECT MAX(id) as maxid FROM 
            department_college_hists GROUP BY departmentID) dch ON dch.maxid = 
            department_college_hists.id) dch2 ON dch2.collegeID = colleges.id 
            INNER JOIN (SELECT courseID, departmentID, id FROM course_department_hists 
            INNER JOIN (SELECT MAX(id) as maxid FROM course_department_hists GROUP BY courseID) 
            cdh ON cdh.maxid = course_department_hists.id) cdh2 ON cdh2.departmentID = 
            dch2.departmentID INNER JOIN courses on courses.id  = cdh2.courseID WHERE 
            ac2.collegeID = (SELECT collegeID FROM assignment_colleges WHERE id = 
            (SELECT MAX(id) FROM assignment_colleges WHERE userID ='".Auth::guard('myuser')->id()."'))
            GROUP BY courses.id"));
        }
        
        $notif = $this->getNotifs();
        
        $currentsem = (new studentController)->getCurrentSem();
        $counseling  = counseling::find($counselingid);
        return view('admin/counseling/counseling', ['student' =>$student,
                                                    'schoolyear' =>explode('-', $schoolyear->schoolYearStart)[0].'-'.explode('-', $schoolyear->schoolYearEnd)[0],
                                                    'currentsem' =>$currentsem,
                                                    'schoolyears'=>$schoolyears,
                                                    'systemdate'=>$systemdate[0]->systemDate,
                                                    'nameofuser' => $nameofuser->firstName." ".$nameofuser->lastName,
                                                    'user'=>$user,
                                                    'collegeOfUser'=>$college,
                                                    'notif'=>$notif,
                                                    'resched'=>'false',
                                                    'accept'=>'true',
                                                    'view'=>'false',
                                                    'counselingID'=>$counselingid,
                                                    'counselingType'=> $counseling->counselingType,
                                                    'studentID'=>$counseling->studentID]);
    }
    public function viewCounseling($id){
        $student = student::all();
        $schoolYearID = (new studentController)->getCurrentSchoolYear();
        $schoolyear = schoolYear::find($schoolYearID);
        $schoolyears = schoolYear::all();
        //$studentstable = student::all(); 
        $systemdate = systemDate::all(); 
        $nameofuser = useraccount::find(Auth::guard('myuser')->id());
        
        $user =  (new userController)->getUserDesignation(Auth::guard('myuser')->id());
        $college ='';
        if($user[0]=='college'){
            $college = DB::select(DB::raw("SELECT colleges.collegeName, courses.courseName, 
            courses.id FROM colleges INNER JOIN (SELECT collegeID, userID, id FROM 
            assignment_colleges INNER JOIN (SELECT MAX(id) AS maxid FROM assignment_colleges 
            GROUP BY userID) ac ON ac.maxid = assignment_colleges.id) ac2 ON ac2.collegeID = 
            colleges.id INNER JOIN (SELECT collegeID, departmentID, id FROM 
            department_college_hists INNER JOIN (SELECT MAX(id) as maxid FROM 
            department_college_hists GROUP BY departmentID) dch ON dch.maxid = 
            department_college_hists.id) dch2 ON dch2.collegeID = colleges.id 
            INNER JOIN (SELECT courseID, departmentID, id FROM course_department_hists 
            INNER JOIN (SELECT MAX(id) as maxid FROM course_department_hists GROUP BY courseID) 
            cdh ON cdh.maxid = course_department_hists.id) cdh2 ON cdh2.departmentID = 
            dch2.departmentID INNER JOIN courses on courses.id  = cdh2.courseID WHERE 
            ac2.collegeID = (SELECT collegeID FROM assignment_colleges WHERE id = 
            (SELECT MAX(id) FROM assignment_colleges WHERE userID ='".Auth::guard('myuser')->id()."'))
            GROUP BY courses.id"));
        }
        
        $notif = $this->getNotifs();
        
        $currentsem = (new studentController)->getCurrentSem();
        $counseling  = counseling::find($id);
        return view('admin/counseling/counseling', ['student' =>$student,
                                                    'schoolyear' =>explode('-', $schoolyear->schoolYearStart)[0].'-'.explode('-', $schoolyear->schoolYearEnd)[0],
                                                    'currentsem' =>$currentsem,
                                                    'schoolyears'=>$schoolyears,
                                                    'systemdate'=>$systemdate[0]->systemDate,
                                                    'nameofuser' => $nameofuser->firstName." ".$nameofuser->lastName,
                                                    'user'=>$user,
                                                    'collegeOfUser'=>$college,
                                                    'notif'=>$notif,
                                                    'resched'=>'false',
                                                    'accept'=>'false',
                                                    'view'=>'true',
                                                    'counselingID'=>$id,
                                                    'counselingType'=> $counseling->counselingType,
                                                    'studentID'=>$counseling->studentID]);
    }
    public function checkContractNotif($contractid){
        $systemdate =  systemDate::all();
        $sql = "SELECT collegStudentID FROM contracts WHERE collegStudentID = (SELECT collegStudentID
                FROM contracts WHERE id = '$contractid')
                AND DATE(created_at) <= '".$systemdate[0]->systemDate."'
                 AND NOT EXISTS
                 (
                 SELECT  null 
                 FROM    contract_remarks d
                 WHERE   d.contractID = contracts.id
                 )";
        $contract = DB::select(DB::raw($sql));
        
        $student = student::join('college_students', 'college_students.studentID', '=','students.id')
                            ->select('students.firstName', 'students.lastName')
                            ->where('college_students.id', '=', $contract[0]->collegStudentID)
                            ->get();
        $setting = contractSetting::all();
        error_log(count($contract)." Hello");
        if(count($setting)>0){
            if(count($contract)%$setting[0]->consecutive==0){
                $systemdate =  systemDate::all();
                    $notif =  new notification;
                    $notif->message = $student[0]->firstName." ".$student[0]->lastName." has 3 consecutive contracts!";
                    $notif->type = 'contract';
                    $notif->contractID = $contractid;
                    $notif->accepted = 0;
                    $notif->created_at = $systemdate[0]->systemDate;
                    $notif->save();
            }
        }else{
            if(count($contract)%3==0){
                $systemdate =  systemDate::all();
                    $notif =  new notification;
                    $notif->message = $student[0]->firstName." ".$student[0]->lastName." has 3 consecutive contracts!";
                    $notif->type = 'contract';
                    $notif->contractID = $contractid;
                    $notif->accepted = 0;
                    $notif->created_at = $systemdate[0]->systemDate;
                    $notif->save();
            }
        }
    }
    public function viewContract($contractid){
        $currentsem = (new studentController)->getCurrentSem();
        $schoolyearid = (new studentController)->getCurrentSchoolYear();
        $schoolyear = schoolYear::find($schoolyearid);
        $systemdate = systemDate::all();
        $schoolyears = schoolYear::all(); 
        $nameofuser = useraccount::find(Auth::guard('myuser')->id());
        $student = student::join('college_students', 'college_students.studentID', '=', 'students.id')
                            ->whereDate('students.created_at', '<=', $systemdate[0]->systemDate)
                            ->select('students.*')->get();
        $user =  (new userController)->getUserDesignation(Auth::guard('myuser')->id());
        $college ='';
        if($user[0]=='college'){
            $college = DB::select(DB::raw("SELECT colleges.collegeName, courses.courseName, 
            courses.id FROM colleges INNER JOIN (SELECT collegeID, userID, id FROM 
            assignment_colleges INNER JOIN (SELECT max(id) AS maxid FROM assignment_colleges 
            GROUP BY userID) ac ON ac.maxid = assignment_colleges.id) ac2 ON ac2.collegeID = 
            colleges.id INNER JOIN (SELECT collegeID, departmentID, id FROM 
            department_college_hists INNER JOIN (SELECT max(id) as maxid FROM 
            department_college_hists GROUP BY departmentID) dch ON dch.maxid = 
            department_college_hists.id) dch2 ON dch2.collegeID = colleges.id 
            INNER JOIN (SELECT courseID, departmentID, id FROM course_department_hists 
            INNER JOIN (SELECT max(id) as maxid FROM course_department_hists GROUP BY courseID) 
            cdh ON cdh.maxid = course_department_hists.id) cdh2 ON cdh2.departmentID = 
            dch2.departmentID INNER JOIN courses on courses.id  = cdh2.courseID WHERE 
            ac2.collegeID = (SELECT collegeID FROM assignment_colleges WHERE id = 
            (SELECT max(id) FROM assignment_colleges WHERE userID ='".Auth::guard('myuser')->id()."'))
            GROUP BY courses.id"));
        }
        $notif = (new notificationController)->getNotifs();
        return view('admin/contract/contract',['collegeStudents' => $student,
                                                'schoolyear' =>explode('-', $schoolyear->schoolYearStart)[0].'-'.explode('-', $schoolyear->schoolYearEnd)[0],
                                                'currentsem' =>$currentsem,
                                                'schoolyears'=>$schoolyears,
                                                'nameofuser' => $nameofuser->firstName." ".$nameofuser->lastName,
                                                'systemDate' =>  $systemdate[0]->systemDate,
                                                'user'=>$user,
                                                'collegeOfUser'=>$college,
                                                'notif'=>$notif,
                                                'viewContract'=>'true',
                                                'contractID'=>$contractid]);
    }
}