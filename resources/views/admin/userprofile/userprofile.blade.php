@extends('admin.adminTemplate')

@section('title', 'AdminDashboard')

@section('stylesheet')
 
  <link href="{{asset('css/admin/userprofile/userprofile.css')}}" rel="stylesheet">
@stop
@section('content')
  <div class='container'> 
    <div class='row'>
        <div class='col-md-7 well'>
        {!! Form::open([
        'files'=>true,
        'id'=>'editProfileForm']) !!}
          <div>
            <center>
              <img src="{{asset('images-database/admin/'.Auth::guard('myuser')->id().'.'.session()->get('user')['imageType'])}}" id = 'profileImage' class="genericImageCircle" alt="Cinque Terre">
            </center>
          </div>
          <div>
          {{Form::label('uploadProfileImage', 'Upload Profile Picture:')}}
          {{Form::file('uploadProfileImage', array('accept'=>
            '.png,.jpg',
            'disabled'=>''
          ))}}
        </div>
          <div class='row'>
            <div class='col-lg-6'>
              {{Form:: label('firstname', 'First Name:')}}
              {{Form:: text('firstname', $user->firstName, array(
                                'placeholder'=>'First Name',
                                'class'=>'form-control',
                                'required'=>'',
                                'maxlength'=>'100',
                                'disabled'=>''
                                ))}}
            </div>
            <div class='col-lg-6'>
              {{Form:: label('lastname', 'Last Name:')}}
              {{Form:: text('lastname', $user->lastName,array(
                                'placeholder'=>'Last Name',
                                'class'=>'form-control',
                                'required'=>'',
                                'maxlength'=>'100',
                                'disabled'=>''))}}
            </div>
          </div>
          {{Form:: label('userName', 'Username:')}}
          {{Form:: text('userName', $user->userName, array(
                            'placeholder'=>'Username',
                            'class'=>'form-control',
                            'required'=>'',
                            'maxlength'=>'100',
                            'disabled'=>''))}}
  
  
          {{--  <div class='row'>
            <div class='col-lg-6'>
              {{Form:: label('password', 'Password:')}}
              {{Form:: password('password', array(
                                'placeholder'=>'Password',
                                'class'=>'form-control',
                                'required'=>'',
                                'maxlength'=>'100',
                                'data-parsley-equalto'=>"#password_confirmation"))}}
            </div>
            <div class='col-lg-6'>
              {{Form:: label('password_confirmation', 'Confirm Password:')}}
              {{Form:: password('password_confirmation', array(
                                'placeholder'=>'Confirm Password',
                                'class'=>'form-control',
                                'required'=>'',
                                'maxlength'=>'100',
                                'data-parsley-equalto'=>"#password"))}}
            </div>
          </div>  --}}

          {{--  {{Form:: label('email', 'Email:')}}
          {{Form:: email('email', null, array(
                            'placeholder'=>'Email',
                            'class'=>'form-control',
                            'required'=>'',
                            'maxlength'=>'100',
                            'data-parsley-type='=>"email"))}}  --}}
          
                            <div class='row'>
                                <div class='col-md-6 genericDivTop'>
                                    <button input type = "button" class='btn btn-warning btn-block' id ='editProfile'>Edit</button>
                                </div>
                                <div class='col-md-6'>
                                    {{Form::submit('Save Profile', array('class'=>'btn btn-success btn-block',
                                    'style'=>'margin-top:20px;'))}}
                                </div>
                            </div>
          
        {!! Form::close() !!}
      
        </div>
    </div>
    <a id ='changePasswordLink'>Change password</a>
    @component('components.modal')
      @slot('modalid')
        changePasswordModal
      @endslot
      @slot('modalsize')
        modal-sm
      @endslot
      @slot('modaltitle')
        Change Password
      @endslot
      @slot('modalcontent')
      {!! Form::open([
        'id'=>'changePasswordForm']) !!}
        <div class='row'>
          <div class='col-md-12'>
            <label for ='oldPassword'>Old Password:</label>
            <input type ='password' id ='oldPassword' name='oldPassword' class='form-control' required>
          </div>
          <div class='col-md-12'>
            <label for ='newPassword'>New Password:</label>
            <input type ='password' id ='newPassword' name='newPassword' class='form-control' required>
          </div>
          <div class='col-md-12'>
            <label for ='confirmPassword'>Old Password:</label>
            <input type ='password' id ='confirmPassword' name='confirmPassword' class='form-control' required>
          </div>
        </div>
      {!! Form::close() !!}
      @endslot
      @slot('modalbutton')
          <button class="btn btn-danger" form ='changePasswordForm' id = 'changePasswordButton'>Save</button>
      @endslot
    @endcomponent
  </div>
@endSection

@section('scripts')

  <script src = '{{asset('js/userprofile/userprofile.js')}}'></script>
@stop
