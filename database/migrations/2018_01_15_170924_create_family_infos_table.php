<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFamilyInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('family_infos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('studentID')->unsigned();
            $table->foreign('studentID')->references('id')->on('students');
            $table->smallInteger('marriedChurch')->default(0);
            $table->smallInteger('marriedCivil')->default(1);
            $table->smallInteger('livingTogether')->default(0);
            $table->smallInteger('seperated')->default(0);
            $table->smallInteger('fatherRemarried')->default(0);
            $table->smallInteger('motherRemarried')->default(0);
            $table->integer('relatives');
            $table->integer('helpers');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('family_infos');
    }
}
