<div id="studentPersonalDataModal" class="modal fade" role="dialog" >
  <div class="modal-dialog modal-lg"  id = 'studentPersonalDataModalDialog'>

    <!-- Modal content-->
    <div class="modal-content" style = 'z-index:200;' id = 'studentPersonalDataModalContent'>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Student Personal Data</h4>
      </div>
      <div class="modal-body">
        <div class='container-fluid'> 
          <div class='row'>
            <div class='col-md-12'>
              <h4>Note: Field marked with * is required.</h4>
            </div>
          </div>
          {!! Form::open([
                  'files'=>true,
                  'id'=>'studentForm']) !!}
          <div class='row'>
            <div class='col-md-9'>
              <div>

                <div class='accordion' role = 'tablist' aria-multiselectable = 'true' id = 'accordion'>
                  <div class='panel'>
                    <a class="panel-heading" role="tab" id="studentInformationHead" data-toggle="collapse" data-parent="#accordion" href="#studentInformationBody" aria-expanded="false" aria-controls="studentInformationBody">
                      <h4 class="panel-title">Student Information</h4>
                    </a>
                    <div id="studentInformationBody" class="panel-collapse collapse" role="tabpanel" aria-labelledby="studentInformationHead" aria-expanded="false" style="">
                      <div class="panel-body">
                        <div class='container-fluid'>

                          <div class='row'>
                            <div class='col-md-12'>
                              <div class="x_panel">
                                <div class="x_title">
                                  <h2>School Information</h2>
                                  <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                  </ul>
                                  <div class="clearfix"></div>
                                </div>
                                <div class="x_content" style="display: block;">
                                  <br>
                                  
                                  <div class='row'>
                                    <div class='col-md-6'>
                                      <label for = 'schoolYearSemester'>A/SY Semester(*):</label>
                                      <input type = 'text' id = 'schoolYearSemester' name = 'schoolYearSemester' disabled class='form-control' 
                                      maxlength='100' value = '{{$currentsem}}' required>
                                    </div>
                                    <div class='col-md-6'>
                                      <label for = 'collegeComboModal'>College(*):</label>
                                      <select class='form-control' id = 'collegeComboModal' name = 'collegeComboModal' required>
                                        @foreach($college as $colleges)
                                          <option value = '{{$colleges->id}}'>{{$colleges->collegeName}}</option>
                                        @endforeach
                                      </select>
                                    </div>
                                  </div>
                                  <div class='row'>
                                    <div class='col-md-6'>
                                      <label for = 'courseComboModal'>Course(*):</label>
                                      <select class='form-control' id = 'courseComboModal' name = 'courseComboModal' required></select>
                                    </div>
                                    <div class='col-md-6'>
                                      <label for = 'studentTypeComboModal'>Student Type(*):</label>
                                      <select class='form-control' id = 'studentTypeComboModal' name = 'studentTypeComboModal' required>
                                        <option value ='college'>College</option>
                                        <option value ='seniorhigh'>Senior High</option>
                                        <option value ='juniorhigh'>Junior High</option>
                                        <option value ='elementary'>Elementary</option>
                                      </select>
                                    </div>
                                  </div>
                                  <div class='row'>
                                      <div class='col-md-12'>
                                          <label for = 'yearLevelComboModal'>Year Level(*):</label>
                                          <select class='form-control' id = 'yearLevelComboModal' name = 'yearLevelComboModal' required></select>
                                    </div>
                                  </div>
                                  <!--div class='row'>
                                    <div class='col-md-6'>
                                      <label for = 'departmentComboModal'>Department(*):</label>
                                      <select class='form-control' id = 'departmentComboModal' name = 'departmentComboModal' required></select>
                                    </div>
                                    <div class='col-md-6'>
                                      <label for = 'yearLevelComboModal'>Year Level(*):</label>
                                      <select class='form-control' id = 'yearLevelComboModal' name = 'yearLevelComboModal' required></select>
                                    </div>
                                  </div-->
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class='row'>
                            <div class='col-md-12'>
                              <div class="x_panel">
                                <div class="x_title">
                                  <h2>Student Information</h2>
                                  <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                  </ul>
                                  <div class="clearfix"></div>
                                </div>
                                <div class="x_content" style="display: block;">
                                  <br>
                                  <div class='row'>
                                    <div class='col-md-6'>
                                      <label for = 'studentNoTextModal'>Student No(*):</label>
                                      <input type = 'text' id = 'studentNoTextModal' name = 'studentNoTextModal'
                                      required class='form-control' maxlength = '5'>
                                    </div>
                                    <div class='col-md-6'>
                                      <label for = 'dobTextModal'>Date Of Birth(*):</label>
                                      <input type = 'date' id = 'dobTextModal' name = 'dobTextModal'
                                      required class='form-control'>
                                    </div>
                                  </div>
                                  <div class='row'>
                                    <div class='col-md-6'>
                                      <label for = 'nationalityTextModal'>Nationality(*):</label>
                                      <input type = 'text' id = 'nationalityTextModal' name = 'nationalityTextModal'
                                      required class='form-control' maxlength='50'>
                                    </div>
                                    <div class='col-md-6'>
                                      <label for = 'lastNameTextModal'>Last Name(*):</label>
                                      <input type = 'text' id = 'lastNameTextModal' name = 'lastNameTextModal'
                                      required class='form-control' maxlength='50'>
                                    </div>
                                  </div>
                                  <div class='row'>
                                    <div class='col-md-6'>
                                      <label>First Name(*):</label>
                                      <input type = 'text' id = 'firstNameTextModal' name = 'firstNameTextModal'
                                      required maxlength='50' class='form-control'>
                                    </div>
                                    <div class='col-md-6'>
                                      <label for = 'religionTextModal'>Religion:</label>
                                      <input list="religionDatalist" id = 'religionTextModal' name = 'religionTextModal'
                                      class='form-control' maxlength="50">
                                      <datalist id="religionDatalist">
                                        @foreach($religion as $students)
                                        <option value="{{$students->religion}}">
                                        @endforeach
                                      </datalist>
                                    </div>
                                  </div>
                                  <div class='row'>
                                    <div class='col-md-6'>
                                      <label for = 'middleNameTextModal'>Middle Name:</label>
                                      <input type= 'text' id = 'middleNameTextModal' name = 'middleNameTextModal'
                                      maxlength='50' class='form-control'>
                                    </div>
                                    <div class='col-md-6'>
                                      <label for = 'ethnicityTextModal'>Ethnicity:</label>
                                      <input list="ethnicityDatalist" id = 'c' name = 'ethnicityTextModal'
                                      class='form-control' maxlength="50">
                                      <datalist id="ethnicityDatalist">
                                        @foreach($ethnicity as $students)
                                        <option value="{{$students->ethnicity}}">
                                        @endforeach
                                      </datalist>
                                    </div>
                                  </div>
                                  <div class='row'>
                                    <!--div class='col-md-6'>
                                      <label for = 'extensionNameTextModal'>Extension Name:</label>
                                      <input type= 'text' id = 'extensionNameTextModal' name = 'extensionNameTextModal'
                                      maxlength='50' class='form-control'>
                                    </div-->
                                    <div class='col-md-6'>
                                      <label for = 'languageSpokenTextModal'>Language Spoken:</label>
                                      <input list="languageSpokenDatalist" id = 'languageSpokenTextModal' name = 'languageSpokenTextModal'
                                      class='form-control' maxlength="50">
                                      <datalist id="languageSpokenDatalist">
                                        @foreach($language as $students)
                                        <option value="{{$students->language}}">
                                        @endforeach
                                      </datalist>
                                    </div>
                                  </div>
                                  <div class='row'>
                                    <div class='col-md-6'>
                                      <label for = 'birthplaceTextModal'>Birthplace</label>
                                      <input type = 'text' id = 'birthplaceTextModal' name = 'birthplaceTextModal' class='form-control'
                                      maxlength='100'>
                                    </div>
                                    <div class='col-md-6'>
                                      <label for = 'maritalStatusComboModal'>Marital Status</label>
                                      <select id = 'maritalStatusComboModal' name = 'maritalStatusComboModal' class='form-control'>
                                        <option value ='single'>Single</option>
                                        <option value ='married'>Married</option>
                                      </select>
                                    </div>
                                  </div>
                                  <div class='row'>
                                    <div class='col-md-6'>
                                      <label for = 'contactNoTextModal'>Contact No:</label>
                                      <input type = 'text' id = 'contactNoTextModal' name = 'contactNoTextModal' class='form-control'
                                      maxlength='100'>
                                    </div>
                                    <div class='col-md-6'>
                                      <label for = 'telephoneNoTextModal'>Telephone No:</label>
                                      <input type = 'text' id = 'telephoneNoTextModal' name = 'telephoneNoTextModal' class='form-control'
                                      maxlength='100'>
                                    </div>
                                  </div>
                                  <div class='row'>
                                    <div class='col-md-6'>
                                      <input type = 'radio' id = 'femaleRadioButton' value ='female' name = 'genderRadioButton'>Female
                                    </div>
                                    <div class='col-md-6'>
                                      <input type = 'radio' id = 'maleRadioButton' value ='male' name = 'genderRadioButton' checked>Male
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class='row'>
                            <div class='col-md-12'>
                              <div class="x_panel">
                                <div class="x_title">
                                  <h2>Address Information</h2>
                                  <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                  </ul>
                                  <div class="clearfix"></div>
                                </div>
                                <div class="x_content" style="display: block;">
                                  <br>
                                  <div class='row'>
                                    <div class='col-md-6'>
                                      <label for ='cityAddressTextModal'>City Address</label>
                                      <input type = 'text' maxlength = '100' id = 'cityAddressTextModal'
                                       name = 'cityAddressTextModal' class = 'form-control'>
                                    </div>
                                    <div class='col-md-6'>
                                      <label for = 'provincialAddressTextModal'>Provincial Address:</label>
                                      <input type = 'text' maxlength = '100' id = 'provincialAddressTextModal'
                                       name = 'provincialAddressTextModal' class = 'form-control'>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class='panel'>
                    <a class="panel-heading" role="tab" id="familiyInformationHead" data-toggle="collapse" data-parent="#accordion" href="#familyInformationBody" aria-expanded="false" aria-controls="familyInformationBody">
                      <h4 class="panel-title">Family Information</h4>
                    </a>
                    <div id="familyInformationBody" class="panel-collapse collapse" role="tabpanel" aria-labelledby="familiyInformationHead" aria-expanded="false" style="">
                      <div class="panel-body">
                        <div class='container-fluid'>
                          <div>
                            <ul class="nav nav-tabs">
                              <li class="active"><a data-toggle="tab" href="#parentInformationTab">Parents Information</a></li>
                              <li><a data-toggle="tab" href="#guardianInformationTab">Spouse/Guardian Information</a></li>
                              <li><a data-toggle="tab" href="#otherFamilyMemberTab">Other Family Member</a></li>
                            </ul>
                          </div>
                          <div class="tab-content">
                            <div class='tab-pane fade in active' id = 'parentInformationTab'>
                              <div class='row'>
                                <div class='col-md-12'>
                                  <div class="x_panel">
                                    <div class="x_title">
                                      <h2>Father Information</h2>
                                      <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                      </ul>
                                      <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content" style="display: block;">
                                      <br>
                                      <div class='row'>
                                        <div class='col-md-6'>
                                          <label for = 'fatherNameTextModal'>Name:</label>
                                          <input type = 'text' maxlength = '100' id = 'fatherNameTextModal'
                                           name = 'fatherNameTextModal' class = 'form-control'>
                                        </div>
                                        <div class='col-md-6'>
                                          <label for = 'fatherAddressNameTextModal'>Address:</label>
                                          <input type = 'text' maxlength = '100' id = 'fatherAddressNameTextModal'
                                           name = 'fatherAddressNameTextModal' class = 'form-control'>
                                        </div>
                                      </div>
                                      <div class='row'>
                                        <div class='col-md-6'>
                                          <label for = 'fatherBirthdayTextModal'>Birthday:</label>
                                          <input type = 'date' id = 'fatherBirthdayTextModal'
                                           name = 'fatherBirthdayTextModal' class = 'form-control'>
                                        </div>
                                        <div class='col-md-6'>
                                          <label for = 'fatherContactNoTextModal'>Contact No:</label>
                                          <input type = 'text' maxlength = '50' id = 'fatherContactNoTextModal'
                                           name = 'fatherContactNoTextModal' class = 'form-control'>
                                        </div>
                                      </div>
                                      <div class='row'>
                                        <div class='col-md-6'>
                                          <label for = 'fatherReligionTextModal'>Religion:</label>
                                          <input list="religionDatalist" id = 'fatherReligionTextModal' name = 'fatherReligionTextModal'
                                          class='form-control' maxlength="50">
                                        </div>
                                        <div class='col-md-6'>
                                          <label for = 'fatherNationalityTextModal'>Nationality:</label>
                                          <input type = 'text' maxlength = '50' id = 'fatherNationalityTextModal'
                                           name = 'fatherNationalityTextModal' class = 'form-control'>
                                        </div>
                                      </div>
                                      <div class='row'>
                                        <div class='col-md-6'>
                                          <label for = 'fatherOccupationTextModal'>Occupation:</label>
                                          <input type = 'text' id = 'fatherOccupationTextModal'
                                           name = 'fatherOccupationTextModal' class = 'form-control' mex-length = '100'>
                                        </div>
                                        <div class='col-md-6'>
                                          <label for = 'fatherEmployerTextModal'>Employer:</label>
                                          <input type = 'text' maxlength = '50' id = 'fatherEmployerTextModal'
                                           name = 'fatherEmployerTextModal' class = 'form-control'>
                                        </div>
                                      </div>
                                      <div class='row'>
                                        <div class='col-md-6'>
                                          <input type = 'radio' name = 'fatherDeadRadio' id ='fatherDeceased' value = 'deceased'>Deceased
                                        </div>
                                        <div class='col-md-6'>
                                          <input type = 'radio' name = 'fatherDeadRadio' id ='fatherNotDeceased' value = 'notdeceased'>Not Deceased
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class='row'>
                                <div class='col-md-12'>
                                  <div class="x_panel">
                                    <div class="x_title">
                                      <h2>Mother Information</h2>
                                      <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                      </ul>
                                      <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content" style="display: block;">
                                      <br>
                                      <div class='row'>
                                        <div class='col-md-6'>
                                          <label for = 'motherNameTextModal'>Name:</label>
                                          <input type = 'text' maxlength = '100' id = 'motherNameTextModal'
                                           name = 'motherNameTextModal' class = 'form-control'>
                                        </div>
                                        <div class='col-md-6'>
                                          <label for = 'motherAddressNameTextModal'>Address:</label>
                                          <input type = 'text' maxlength = '100' id = 'motherAddressNameTextModal'
                                           name = 'motherAddressNameTextModal' class = 'form-control'>
                                        </div>
                                      </div>
                                      <div class='row'>
                                        <div class='col-md-6'>
                                          <label for = 'motherBirthdayTextModal'>Birthday:</label>
                                          <input type = 'date' id = 'motherBirthdayTextModal'
                                           name = 'motherBirthdayTextModal' class = 'form-control'>
                                        </div>
                                        <div class='col-md-6'>
                                          <label for = 'motherAddressNameTextModal'>Contact No:</label>
                                          <input type = 'text' maxlength = '50' id = 'motherAddressNameTextModal'
                                           name = 'motherAddressNameTextModal' class = 'form-control'>
                                        </div>
                                      </div>
                                      <div class='row'>
                                        <div class='col-md-6'>
                                          <label for = 'motherReligionTextModal'>Religion:</label>
                                          <input list="religionDatalist" id = 'motherReligionTextModal' name = 'motherReligionTextModal'
                                          class='form-control' maxlength="50">
                                        </div>
                                        <div class='col-md-6'>
                                          <label for = 'motherNationalityTextModal'>Nationality:</label>
                                          <input type = 'text' maxlength = '50' id = 'motherNationalityTextModal'
                                           name = 'motherNationalityTextModal' class = 'form-control'>
                                        </div>
                                      </div>
                                      <div class='row'>
                                        <div class='col-md-6'>
                                          <label for = 'motherOccupationTextModal'>Occupation:</label>
                                          <input type = 'text' id = 'motherOccupationTextModal'
                                           name = 'motherOccupationTextModal' class = 'form-control' mex-length = '100'>
                                        </div>
                                        <div class='col-md-6'>
                                          <label for = 'motherEmployerTextModal'>Employer:</label>
                                          <input type = 'text' maxlength = '50' id = 'motherEmployerTextModal'
                                           name = 'motherEmployerTextModal' class = 'form-control'>
                                        </div>
                                      </div>
                                      <div class='row'>
                                        <div class='col-md-6'>
                                          <input type = 'radio' name = 'motherDeadRadio' id ='motherDeceased' value = 'deceased'>Deceased
                                        </div>
                                        <div class='col-md-6'>
                                          <input type = 'radio' name = 'motherDeadRadio' id ='motherNotDeceased' value = 'notdeceased'>Not Deceased
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class='row'>
                                <div class='col-md-12'>
                                  <div class="x_panel">
                                    <div class="x_title">
                                      <h2>Parents Marital Status</h2>
                                      <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                      </ul>
                                      <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content" style="display: block;">
                                      <br>
                                      <div class='row'>
                                        <div>
                                          <input type = 'radio' name = 'maritalStatus' value = 'MarriedInChurch'>Parents Maried in Church
                                        </div>
                                        <div class='genericDivTop'>
                                          <input type = 'radio' name = 'maritalStatus' value = 'MarriedCivilly'>Parents Maried Civilly
                                        </div>
                                        <div class='genericDivTop'>
                                          <input type = 'radio' name = 'maritalStatus' value = 'ParentsLivingTogether'>Parents Living Together
                                        </div>
                                        <div class='genericDivTop'>
                                          <input type = 'radio' name = 'maritalStatus' value = 'ParentsSeperated'>Parents are Seperated
                                        </div>
                                        <div class='genericDivTop'>
                                          <input type = 'radio' name = 'maritalStatus' value = 'FatherRemarried'>Father Remarried
                                        </div>
                                        <div class='genericDivTop'>
                                          <input type = 'radio' name = 'maritalStatus' value = 'MotherRemarried'>Mother Remarried
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class='row'>
                                <div class='col-md-12'>
                                  <div class="x_panel">
                                    <div class="x_title">
                                      <h2>No. Of People Living At Home</h2>
                                      <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                      </ul>
                                      <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content" style="display: block;">
                                      <br>
                                      <div class='row'>
                                        <div class='col-md-12'>
                                          <label for = 'numberFamilyMembersTextModal'>Family Members:</label>
                                          <input type = 'number' class='form-control' id ='numberFamilyMembersTextModal'
                                          name = 'numberFamilyMembersTextModal'>
                                        </div>
                                        <div class='col-md-12'>
                                          <label for = 'numberRelativeTextModal'>Relative:</label>
                                          <input type = 'number' class='form-control' id ='numberRelativeTextModal'
                                          name = 'numberRelativeTextModal'>
                                        </div>
                                        <div class='col-md-12'>
                                          <label for = 'numberHelpersTextModal'>Helpers:</label>
                                          <input type = 'number' class='form-control' id ='numberHelpersTextModal'
                                          name = 'numberHelpersTextModal'>
                                        </div>
                                      </div>

                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class='tab-pane fade' id = 'guardianInformationTab'>
                              <h4>Guardian Information Tab</h4>
                            </div>
                            <div class='tab-pane fade' id = 'otherFamilyMemberTab'>
                              <h4>Other Family Member Tab</h4>
                            </div>
                          </div>

                        </div>
                      </div>
                    </div>
                  </div>
                  <div class='panel'>
                    <a class="panel-heading" role="tab" id="educationalBackgroundHead" data-toggle="collapse" data-parent="#accordion" href="#educationalBackgroundBody" aria-expanded="false" aria-controls="educationalBackgroundBody">
                      <h4 class="panel-title">Educational Background</h4>
                    </a>
                    <div id="educationalBackgroundBody" class="panel-collapse collapse" role="tabpanel" aria-labelledby="educationalBackgroundHead" aria-expanded="false" style="">
                      <div class="panel-body">
                        <div class='container-fluid'>
                          <div class='col-md-12'>
                            <div class='row'>
                              <div class='col-md-12'>
                                <div class="x_panel">
                                  <div class="x_title">
                                    <h2>List Of All Schools Attended</h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                      </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                  </div>
                                  <div class="x_content" style="display: block;">
                                    <br>
                                    <div class='row'>
                                      <div class='col-md-12 alignRight'>
                                        <button id = 'addSchoolAttendedButton' class='btn btn-dark' type ='button'>Add</button>
                                      </div>
                                    </div>
                                    <div class='row'>
                                      <div class='col-md-12 tableModalContainer'>
                                        <table class='table table-striped'>
                                          <thead>
                                            <tr>
                                              <th>School</th>
                                              <th>Year Transfered/Graduated</th>
                                              <th>Year Level</th>
                                              <th>Honor/Awards</th>
                                              <th>Remove</th>
                                            </tr>
                                          </thead>
                                          <tbody id ='studentSchoolBackgroundTable'>

                                          </tbody>
                                        </table>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class='row'>
                              <div class='col-md-12'>
                                <div class="x_panel">
                                  <div class="x_title">
                                    <h2>Subject Likes</h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                      </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                  </div>
                                  <div class="x_content" style="display: block;">
                                    <br>
                                    <div class='row'>
                                      <div class='col-md-12 alignRight'>
                                        <button  id = 'addSubjectLikesButton' class='btn btn-dark' type = 'button'>Add</button>
                                      </div>
                                    </div>
                                    <div class='row'>
                                      <div class='col-md-12 tableModalContainer'>
                                        <table class='table table-striped'>
                                          <thead>
                                            <tr>
                                              <th>Subject</th>
                                              <th>Grade</th>
                                              <th>Remove</th>
                                            </tr>
                                          </thead>
                                          <tbody id ='favSubjectsTable'>
                                          </tbody>
                                        </table>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class='row'>
                              <div class='col-md-12'>
                                <div class="x_panel">
                                  <div class="x_title">
                                    <h2>Subject Dislikes</h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                      </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                  </div>
                                  <div class="x_content" style="display: block;">
                                    <br>
                                    <div class='row'>
                                      <div class='col-md-12 alignRight'>
                                        <button type = 'button' id = 'addSubjectDislikesLikesButton' class='btn btn-dark'>Add</button>
                                      </div>
                                    </div>
                                    <div class='row'>
                                      <div class='col-md-12 tableModalContainer'>
                                        <table class='table table-striped'>
                                          <thead>
                                            <tr>
                                              <th>Subject</th>
                                              <th>Grade</th>
                                              <th>Remove</th>
                                            </tr>
                                          </thead>
                                          <tbody id = 'subjectsDislikesTable'>
                                          </tbody>
                                        </table>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class='row'>
                              <div class='col-md-12'>
                                <div class="x_panel">
                                  <div class="x_title">
                                    <h2>Membership On Organization<!-- And Hobbies--></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                      </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                  </div>
                                  <div class="x_content" style="display: block;">
                                    <br>

                                    <div class='row'>
                                      <div class='col-md-6'>
                                        <label for = 'awardsReceivedTextModal'>Awards Received</label>
                                        <input type = 'text' id ='awardsReceivedTextModal' name ='awardsReceivedTextModal'
                                        class='form-control' maxlength = '100'>
                                      </div>
                                      <div class='col-md-6'>
                                        <label for = 'offCampusTextModal'>Off Campus</label>
                                        <input type = 'text' id ='offCampusTextModal' name ='offCampusTextModal'
                                        class='form-control' maxlength = '100'>
                                      </div>
                                    </div>
                                    <div class='row'>
                                      <div class='col-md-6'>
                                        <label for = 'organizationTextModal'>Organization</label>
                                        <input type = 'text' id ='organizationTextModal' name ='organizationTextModal'
                                        class='form-control' maxlength = '100'>
                                      </div>
                                      <div class='col-md-6'>
                                        <label for = 'onCampusTextModal'>On Campus</label>
                                        <input type = 'text' id ='onCampusTextModal' name ='onCampusTextModal'
                                        class='form-control' maxlength = '100'>
                                      </div>
                                    </div>
                                    <!--div class='row'>
                                      <div class='col-md-6'>
                                        <label for = 'hobbiesTextModal'>Hobbies/Interests:</label>
                                        <input type = 'text' id ='hobbiesTextModal' name ='hobbiesTextModal'
                                        class='form-control' maxlength = '100'>
                                      </div>
                                    </div-->
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class='panel'>
                    <a class="panel-heading" role="tab" id="studentLeisureHead" data-toggle="collapse" data-parent="#accordion" href="#studentLeisureBody" aria-expanded="false" aria-controls="studentLeisureBody">
                      <h4 class="panel-title">Student Leisure</h4>
                    </a>
                    <div id="studentLeisureBody" class="panel-collapse collapse" role="tabpanel" aria-labelledby="studentLeisureHead" aria-expanded="false" style="">
                      <div class="panel-body">
                        <div class='row'>
                          <div class='col-md-4'>
                            <label for = 'approxHighSchoolAverageTextModal'>Approx. High School Ave.</label>
                            <input type = 'number' id = 'approxHighSchoolAverageTextModal'
                            name = 'approxHighSchoolAverageTextModal' class='form-control'>
                          </div>
                          <div class='col-md-4'>
                            <label for = 'rankInClassTextModal'>Rank in class:</label>
                            <input type = 'text' id = 'rankInClassTextModal' name = 'rankInClassTextModal'
                            class='form-control' maxlength='100'>
                          </div>
                          <div class='col-md-4'>
                            <label for = 'majorTextModal'>Major</label>
                            <input type = 'text' id = 'majorTextModal' name ='majorTextModal'
                            class='form-control' maxlength = '100'>
                          </div>
                        </div>
                        <div class='row'>
                          <div class='col-md-12'>
                            <label for = 'presentEducationalPlans'>Present educational and vocational plans</label>
                            <input type = 'text' id = 'presentEducationalPlans' name='presentEducationalPlans'
                            class='form-control' maxlength="500">
                          </div>
                        </div>
                        <div class='row'>
                          <div class='col-md-12'>
                            <label for = 'howDidYouMakeThisChoiceTextModal'>How did you make this choice?</label>
                           
                            <input list="howDidYouMakeThisChoiceList" id = 'howDidYouMakeThisChoiceTextModal' name = 'howDidYouMakeThisChoiceTextModal'
                                      class='form-control' maxlength="150">
                            <datalist id="howDidYouMakeThisChoiceList">
                                @foreach($howdidyoumakethischoice as $howdidyoumakethischoice)
                                <option value="{{$howdidyoumakethischoice->selfEval1}}">
                                @endforeach
                            </datalist>
                          </div>
                          <!--div class='col-md-6'>
                            <label for = 'notListedTextModal'>If not listed please input in the text provided</label>
                            <input type = 'text' id = 'notListedTextModal' name ='notListedTextModal' maxlength="500" class='form-control'>
                          </div-->
                        </div>
                        <div class='row'>
                          <div class='col-md-12'>
                            <label for = 'ifChoiceNotYourOwnTextModal'>If choice was not your own, what course would you rather take up?</label>
                            <input type = 'text' id = 'ifChoiceNotYourOwnTextModal' name ='ifChoiceNotYourOwnTextModal' maxlength="100" class='form-control'>
                            
                          </div>
                        </div>
                        <div class='row'>
                          <div class='col-md-12'>
                            <label for = 'howDidYouComeToThisSchoolTextModal'>How did you come to this school?</label>
                            <!--select id = 'howDidYouComeToThisSchoolTextModal' name = 'howDidYouComeToThisSchoolTextModal'
                            class='form-control'></select-->
                            <input list="howDidYouComeToThisSchoolList" id = 'howDidYouComeToThisSchoolTextModal' name = 'howDidYouComeToThisSchoolTextModal'
                                      class='form-control' maxlength="150">
                            <datalist id="howDidYouComeToThisSchoolList">
                              @foreach($howdidyoumakethischoice as $howdidyoumakethischoice)
                              <option value="{{$howdidyoucometothisschool->selfEval3}}">
                              @endforeach
                            </datalist>
                          </div>
                          <!--div class='col-md-6'>
                            <label for = 'notListedHowYouComeToThisSchoolTextModal'>If not listed please input in the text provided</label>
                            <input type = 'text' id = 'notListedHowYouComeToThisSchoolTextModal' name ='notListedHowYouComeToThisSchoolTextModal' maxlength="100" class='form-control'>
                          </div-->
                        </div>
                        <div class='row'>
                          <div class='col-md-12 genericDivBottom'>
                            <label>How much information do you have about the requirements you're taking up?</label>
                          </div>
                          <div>
                            <div class='col-md-3'>
                              <input type = 'radio' name = 'infoRequirementsRadio' value = 'verymuch'>Very Much
                            </div>
                            <div class='col-md-2'>
                              <input type = 'radio' name = 'infoRequirementsRadio' value = 'much'>Much
                            </div>
                            <div class='col-md-2'>
                              <input type = 'radio' name = 'infoRequirementsRadio' value = 'enough'>Enough
                            </div>
                            <div class='col-md-2'>
                              <input type = 'radio' name = 'infoRequirementsRadio' value = 'verylittle'>Very Little
                            </div>
                            <div class='col-md-2'>
                              <input type = 'radio' name = 'infoRequirementsRadio' value = 'none'>None
                            </div>
                          </div>
                        </div>
                        <div class='row'>
                          <div class='col-md-12'>
                            <label for = 'whereDidYouGetThisInformationTextModal'>Where did you get this information?</label>
                            <input type = 'text' id = 'whereDidYouGetThisInformationTextModal' name ='whereDidYouGetThisInformationTextModal' maxlength="500" class='form-control'>
                          </div>
                        </div>
                        <div class='row'>
                          <div class='col-md-6'>
                            <label for = 'financialSupportComboModal'>Source of financial support in college:</label>
                            <!--select id = 'financialSupportComboModal' name = 'financialSupportComboModal'
                            class='form-control'></select-->
                            <input list="financialSupporList" id = 'financialSupportComboModal' name = 'financialSupportComboModal'
                                      class='form-control' maxlength="150">
                            <datalist id="financialSupporList">
                              @foreach($howdidyoumakethischoice as $howdidyoumakethischoice)
                              <option value="{{$financialsupport->financialSupport}}">
                              @endforeach
                            </datalist>
                          </div>
                          <div class='col-md-6'>
                            <label for = 'specifyScholarshipTextModal'>Specify Scholarship:</label>
                            <input type = 'text' id = 'specifyScholarshipTextModal' name ='specifyScholarshipTextModal' maxlength="500" class='form-control'>
                          </div>
                        </div>
                        <div class='row'>
                          <div class='col-md-12'>
                            <label>Self-evaluation regarding scholastic standing. Check the following which apply to you</label>
                          </div>
                        </div>
                        <div class='row'>
                          <div class='col-md-4'>
                            <label for = 'barelyPassedSubjectsCheck'>
                              <input type = 'checkbox' id = 'barelyPassedSubjectsCheck' name = 'barelyPassedSubjectsCheck'
                              value = 'true'>
                              I barely pass all my subjects
                            </label>
                            <label for = 'failedSubjectsCheck'>
                              <input type = 'checkbox' id = 'failedSubjectsCheck' name = 'failedSubjectsCheck'
                              value = 'true'>
                              I failed most of my subjects
                            </label>
                          </div>
                          <div class='col-md-4'>
                            <label for = 'barelyPassedSubjectsCheck'>
                              <input type = 'checkbox' id = 'barelyPassedSubjectsCheck' name = 'barelyPassedSubjectsCheck'
                              value = 'true'>
                              I fear im going to fail this semester
                            </label>
                            <label for = 'failedSubjects'>
                              <input type = 'checkbox' id = 'failedSubjectsCheck' name = 'failedSubjectsCheck'
                              value = 'true'>
                              I am having a hard time passing my subjects
                            </label>

                          </div>
                          <div class='col-md-4'>
                            <label for = 'dificultySubjectsCheck'>
                              <input type = 'checkbox' id = 'dificultySubjectsCheck' name = 'dificultySubjectsCheck'
                              value = 'true'>
                              I have difficulty with some of my subjects
                            </label>
                            <label for = 'confidentCheck'>
                              <input type = 'checkbox' id = 'confidentCheck' name = 'confidentCheck'
                              value = 'true'>
                              I am confident i can finish my course
                            </label>
                          </div>
                        </div>
                        <div class='row'>
                          <div class='col-md-12'>
                            <label for = 'leisureRemarksTextModal'>Remarks:</label>
                            <input type = 'text' id = 'leisureRemarksTextModal' name = 'leisureRemarksTextModal'
                            class='form-control'>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class='panel'>
                    <a class="panel-heading" role="tab" id="healthRecordsHead" data-toggle="collapse" data-parent="#accordion" href="#healthRecordsBody" aria-expanded="false" aria-controls="healthRecordsBody">
                      <h4 class="panel-title">Health Records</h4>
                    </a>
                    <div id="healthRecordsBody" class="panel-collapse collapse" role="tabpanel" aria-labelledby="healthRecordsHead" aria-expanded="false" style="">
                      <div class="panel-body">
                        <div class='row'>
                          <div class='col-md-4'>
                            <label for = 'heightTextModal'>Height(cm)(*):</label>
                            <input type = 'number' id = 'heightTextModal' name = 'heightTextModal'
                            class='form-control' required>
                          </div>
                          <div class='col-md-4'>
                            <label for = 'weightTextModal'>Weight(kg)(*):</label>
                            <input type = 'number' id = 'weightTextModal' name = 'weightTextModal'
                            class='form-control' required>
                          </div>
                          <div class='col-md-4'>
                            <label for = 'complexionComboModal'>Complexion:</label>
                            <select id = 'complexionComboModal' name = 'complexionComboModal'
                            class='form-control'>
                              <option value ='lightskin'>Light skin</option>
                              <option value ='fairskin'>Fair Skin</option>
                              <option value ='mediumskin'>Medium Skin</option>
                              <option value ='tanbrown'>Tan Brown</option>
                              <option value ='blackbrown'>Black Brown</option>
                            </select>
                          </div>
                        </div>
                        <div class='row'>
                          <div class='col-md-12'>
                            Wearing Glasses?
                          </div>
                          <div class='col-md-6'>
                            <label for = 'yesWearGlassesRadio'>
                              <input type = 'radio' id ='yesWearGlassesRadio' name = 'wearGlasses' value = 'yes'>
                              Yes
                            </label>
                          </div>
                          <div class='col-md-6'>
                            <label for = 'noWearGlassesRadio'>
                              <input type = 'radio' id ='noWearGlassesRadio' name = 'wearGlasses' value = 'no'>
                              No
                            </label>
                          </div>
                        </div>
                        <div class='row'>
                          <div class='col-md-12'>
                            <label for = 'physicalProgramsParticipated'>Physical Programs Participated:</label>
                            <!--select id = 'physicalProgramsParticipated' name = 'physicalProgramsParticipated'
                            class='form-control'></select-->
                            <input list="physicalProgramsDatalist" id = 'physicalProgramsParticipated' name = 'physicalProgramsParticipated'
                                      class='form-control' maxlength="50">
                            <datalist id="physicalProgramsDatalist">
                                @foreach($programs as $programs)
                                <option value="{{$programs->programsParticipated}}">
                                @endforeach
                            </datalist>
                          </div>
                        </div>
                        <!--div class='row'>
                          <div class='col-md-12'>
                            <label for = 'textProvidedPhysicalProgramsParticipated'>If not listed above please input in the text provided:</label>
                            <input type = 'text' id = 'textProvidedPhysicalProgramsParticipated' name = 'textProvidedPhysicalProgramsParticipated'
                            class='form-control'>
                          </div>
                        </div-->
                        <div class='row'>
                          <div class='col-md-12'>
                            <label for = 'sufferingFromPhysicalAilmentCombo'>Suffering From Phyical Ailment:</label>
                            <!--select id = 'sufferingFromPhysicalAilmentCombo' name = 'sufferingFromPhysicalAilmentCombo'
                            class='form-control'></select-->
                            <input list="physicalAilmentList" id = 'sufferingFromPhysicalAilmentCombo' name = 'sufferingFromPhysicalAilmentCombo'
                            class='form-control' maxlength="50">
                            <datalist id="physicalAilmentList">
                                @foreach($ailments as $ailment)
                                <option value="{{$ailment->physicalAilment}}">
                                @endforeach
                            </datalist>
                          </div>
                        </div>
                        <!--div class='row'>
                          <div class='col-md-12'>
                            <label for = 'textProvidedPhysicalAilment'>If not listed above please input in the text provided:</label>
                            <input type = 'text' id = 'textProvidedPhysicalAilment' name = 'textProvidedPhysicalAilment'
                            class='form-control'>
                          </div>
                        </div-->
                        <div class='row'>
                          <div class='col-md-12'>
                            <label for = 'whereDoYouLiveText'>Where do you live?:</label>
                            <input type = 'text' id = 'whereDoYouLiveText' name = 'whereDoYouLiveText'
                            class='form-control'>
                          </div>
                        </div>
                        <div class='row'>
                          <div class='col-md-6'>
                            <label for = 'totalQuantityInPresentPlace'>How many are you in your present place now?</label>
                            <input type = 'number' id = 'totalQuantityInPresentPlace' name = 'totalQuantityInPresentPlace'
                            class='form-control'>
                          </div>
                          <div class='col-md-6'>
                            <label for = 'totalPersonSharingRoom'>How many person share the room with you?</label>
                            <input type = 'number' id = 'totalPersonSharingRoom' name = 'totalPersonSharingRoom'
                            class='form-control'>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class='panel'> 
                    <a class="panel-heading" role="tab" id="studentAssistanceHead" data-toggle="collapse" data-parent="#accordion" href="#studentAssistanceBody" aria-expanded="false" aria-controls="studentAssistanceBody">
                      <h4 class="panel-title">Student Make up/ Assistance</h4>
                    </a>
                    <div id="studentAssistanceBody" class="panel-collapse collapse" role="tabpanel" aria-labelledby="studentAssistanceHead" aria-expanded="false" style="">
                      <div class="panel-body">
                        <div class='row'>
                          <div class='col-md-12'>
                            <label>Select one or more of the following words which you feel describe your general personality make-up:</label>
                          </div>
                        </div>
                        <div class='row'>
                          <div class='col-md-12 tableModalContainer'>
                            <table class='table table-striped'>
                              <thead>
                                <tr>
                                  <th>Check</th>
                                  <th>Personality</th>
                                  <th>Description</th>
                                </tr>
                              </thead>
                              <tbody id = 'studentPersonalityTable'>
                                  @foreach($personality as $personalities)
                                    <tr id = 'personality-{{$personalities->id}}'>
                                      <td><input type = 'checkbox' id = 'checkPers-{{$personalities->id}}' value ='checked'></td>
                                      <td>{{$personalities->name}}</td>
                                      <td>{{$personalities->description}}</td>
                                    </tr>
                                  @endforeach
                              </tbody>
                            </table>

                          </div>
                        </div>
                        <div class='row'>
                          <div class='col-md-12'>
                            <label for = 'providedPersonality'>If not listed above, please provide in the textbox provided:</label>
                            <input type = 'text' id = 'providedPersonality' name = 'providedPersonality'
                            class='form-control'>
                          </div>
                        </div>
                        <div class='row'>
                          <div class='col-md-12'>
                            <label for = 'significantEvents'>Significant events in your life(explain briefly):</label>
                            <textarea id = 'significantEvents' name = 'significantEvents'
                            class='form-control'></textarea>
                          </div>
                        </div>
                        <div class='row'>
                          <div class='col-md-12'>
                            <label for = 'helpFromGuidance'>What help do you want to obtain from Guidance and Counseling Center?:</label>
                            <textarea id = 'helpFromGuidance' name = 'helpFromGuidance'
                            class='form-control'></textarea>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
            </div>
            <div class='col-md-3'>
              <img src = '{{asset('images/defaultimage.png')}}'
              class='img-circle profile_img' id ='profileImage'>
              <div class='row'>
                <div class='col-md-12'>
                    <input type = 'file' class='form-control' id = 'studentImage' name ='studentImage' required>
                </div>
                <div class='col-md-12'>
                  <button class='btn btn-block btn-danger' id = 'removeImage' type = 'button'>Remove</button>
                </div>
              </div>
              <div class='genericDivTop row'>
                <center>
                  <button type ='button' class='btn btn-dark'><i class='fa fa-print'>&nbsp; Print profile</i></button>
                </center>
              </div>
              <div>
              </div>
            </div>
          </div>
          {!! Form::close() !!}
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-danger" form = 'studentForm'><i class='fa fa-save'></i>&nbsp;Save</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>

  </div>
</div>
