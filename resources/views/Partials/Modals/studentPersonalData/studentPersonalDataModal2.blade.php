@component('components.modal')
  @slot('modalid')
    studentFormModal
  @endslot
  @slot('modalsize')
    modal-md
  @endslot
  @slot('modaltitle')
    Update a student
  @endslot
  @slot('modalcontent')
    {!! Form::open([
    'files'=>true,
    'id'=>'studentForm']) !!}
        <div class='row'>
            <center><img src = '{{asset('images/defaultimage.png')}}'
            class='myimage-md' id ='profileImage'></center>
            <div class='row'>
              <div class='col-md-12'>
                  <input type = 'file' class='form-control' id = 'studentImage' name ='studentImage'>
              </div>
              <div class='col-md-12'>
                  <button class='btn btn-block btn-danger' id = 'removeImage' type = 'button'>Remove</button>
              </div>
            </div>
            
        </div>
        <div id="studentInformationBody">
          <div class="panel-body">
            <div class='container-fluid'>

              <div class='row'>
                <div class='col-md-12'>
                  <div class="x_panel">
                    <div class="x_title">
                      <h2>School Information</h2>
                      <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                      </ul>
                      <div class="clearfix"></div>
                    </div>
                    <div class="x_content" style="display: block;">
                      <br>
                      
                      <div class='row'>
                        
                        <div class='col-md-12'>
                          <label for = 'studentTypeComboModal'>Student Type(*):</label>
                          @if(session()->get('user')['userType']=='admin')
                            <select class='form-control' id = 'studentTypeComboModal' name = 'studentTypeComboModal' required>
                              <option value ='college'>College</option>
                              <option value ='seniorhigh'>Senior High</option>
                              <option value ='juniorhigh'>Junior High</option>
                              <option value ='elementary'>Elementary</option>
                          @else
                            @switch($user[0])
                              @case('college')
                              <select id ='studentTypeComboModal' class='form-control' name = 'studentTypeComboModal' readonly>
                                  <option value ='college'>college</option>
                              @break
                              @case('seniorhigh')
                              <select id ='studentTypeComboModal' class='form-control' name = 'studentTypeComboModal' readonly>
                                  <option value ='seniorhigh'>seniorhigh</option>
                              @break
                              @case('juniorhigh')
                              <select id ='studentTypeComboModal' class='form-control' name = 'studentTypeComboModal' readonly>
                                  <option value ='juniorhigh'>juniorhigh</option>
                              @break
                              @case('elementary')
                              <select id ='studentTypeComboModal' class='form-control' name = 'studentTypeComboModal' readonly>
                                  <option value ='elementary'>elementary</option>
                              @break
                              @default
                                  Default case...
                            @endswitch
                          @endif
                          </select> 
                        </div>
                      </div>
                      <div class='row'>
                        <div class='col-md-6'>
                          <label for = 'courseComboModal'>Course(*):</label>
                          <select class='form-control' id = 'courseComboModal' name = 'courseComboModal' required>
                            @if($collegeOfUser!='')
                              @foreach($collegeOfUser as $collegess)
                                <option value = "{{$collegess->id}}">{{$collegess->courseName}}</option>
                              @endforeach
                            @endif
                          </select>
                        </div>
                        <div class='col-md-6'>
                          <label for = 'collegeComboModal'>College(*):</label>
                          @if(session()->get('user')['userType'] == 'admin')
                            <select class='form-control' id = 'collegeComboModal' name = 'collegeComboModal' required>
                          @else
                            <select class='form-control' id = 'collegeComboModal' name = 'collegeComboModal' required readonly>
                          @endif
                          
                            @foreach($college as $colleges)
                              @if($collegeOfNotAdmin == $colleges->id)
                                <option value = '{{$colleges->id}}' selected>{{$colleges->collegeName}}</option>
                              @else
                                <option value = '{{$colleges->id}}'>{{$colleges->collegeName}}</option>
                              @endif
                            @endforeach
                         
                          </select>
                        </div>
                      </div> 
                      <div class='row'>
                          <div class='col-md-6'>
                              <label for = 'yearLevelComboModal'>Year Level(*):</label>
                              <select class='form-control' id = 'yearLevelComboModal' name = 'yearLevelComboModal' required></select>
                          </div>
                          <div class='col-md-6'>
                            <label for = 'dateEnrolledText'>Date Enrolled(*):</label>
                            
                            <input type = 'date' id = 'dateEnrolledText' name = 'dateEnrolledText'
                             required class='form-control'>
                          </div>
                      </div>
                      <div>
                        <div class='col-md-12'>
                            <label for='seniorHighStrand'>Senior High Strand</label>
                            <select class='form-control' id ='seniorHighStrand' name ='seniorHighStrand' disabled>
                                @foreach($strands as $strands)
                                    <option value = '{{$strands->id}}'>{{$strands->strandName}}</option>
                                @endforeach
                            </select>
                        </div>
                      </div>
                      <!--div class='row'>
                        <div class='col-md-6'>
                          <label for = 'departmentComboModal'>Department(*):</label>
                          <select class='form-control' id = 'departmentComboModal' name = 'departmentComboModal' required></select>
                        </div>
                        <div class='col-md-6'>
                          <label for = 'yearLevelComboModal'>Year Level(*):</label>
                          <select class='form-control' id = 'yearLevelComboModal' name = 'yearLevelComboModal' required></select>
                        </div>
                      </div-->
                    </div>
                  </div>
                </div>
              </div>
              <div class='row'>
                <div class='col-md-12'>
                  <div class="x_panel">
                    <div class="x_title">
                      <h2>Student Information</h2>
                      <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                      </ul>
                      <div class="clearfix"></div>
                    </div>
                    <div class="x_content" style="display: block;">
                      <br>
                      <div class='row'>
                        <div class='col-md-6'>
                          <label for = 'studentNoTextModal'>Student No(*):</label>
                          <input type = 'text' id = 'studentNoTextModal' name = 'studentNoTextModal'
                          required class='form-control'>
                        </div>
                        <div class='col-md-6'>
                          <label for = 'dobTextModal'>Date Of Birth(*):</label>
                          <input type = 'date' id = 'dobTextModal' name = 'dobTextModal'
                          required class='form-control'>
                        </div>
                      </div>
                      <div class='row'>
                        <div class='col-md-6'>
                          <label for = 'nationalityTextModal'>Nationality(*):</label>
                          <input type = 'text' id = 'nationalityTextModal' name = 'nationalityTextModal'
                          required class='form-control' maxlength='50'>
                        </div>
                        <div class='col-md-6'>
                          <label for = 'lastNameTextModal'>Last Name(*):</label>
                          <input type = 'text' id = 'lastNameTextModal' name = 'lastNameTextModal'
                          required class='form-control' maxlength='50'>
                        </div>
                      </div>
                      <div class='row'>
                        <div class='col-md-6'>
                          <label>First Name(*):</label>
                          <input type = 'text' id = 'firstNameTextModal' name = 'firstNameTextModal'
                          required maxlength='50' class='form-control'>
                        </div>
                        <div class='col-md-6'>
                          <label for = 'religionTextModal'>Religion:</label>
                          <input list="religionDatalist" id = 'religionTextModal' name = 'religionTextModal'
                          class='form-control' maxlength="50">
                          <datalist id="religionDatalist">
                            @foreach($religion as $students)
                            <option value="{{$students->religion}}">
                            @endforeach
                            
                          </datalist>
                        </div>
                      </div>
                      <div class='row'> 
                        <div class='col-md-6'>
                          <label for = 'middleNameTextModal'>Middle Name:</label>
                          <input type= 'text' id = 'middleNameTextModal' name = 'middleNameTextModal'
                          maxlength='50' class='form-control'>
                        </div>
                        <div class='col-md-6'>
                          <label for = 'ethnicityTextModal'>Ethnicity:</label>
                          <input list="ethnicityDatalist" id = 'ethnicityTextModal' name = 'ethnicityTextModal'
                          class='form-control' maxlength="50">
                          <datalist id="ethnicityDatalist">
                            @foreach($ethnicity as $students)
                            <option value="{{$students->ethnicity}}">
                            @endforeach
                          </datalist>
                        </div>
                      </div> 
                      <div class='row'>
                        <!--div class='col-md-6'>
                          <label for = 'extensionNameTextModal'>Extension Name:</label>
                          <input type= 'text' id = 'extensionNameTextModal' name = 'extensionNameTextModal'
                          maxlength='50' class='form-control'>
                        </div-->
                        <div class='col-md-12'>
                          <label for = 'languageSpokenTextModal'>Language Spoken:</label>
                          <input list="languageSpokenDatalist" id = 'languageSpokenTextModal' name = 'languageSpokenTextModal'
                          class='form-control' maxlength="50">
                          <datalist id="languageSpokenDatalist">
                            @foreach($language as $students)
                            <option value="{{$students->language}}">
                            @endforeach
                          </datalist>
                        </div>
                      </div>
                      <div class='row'>
                        <!--div class='col-md-6'>
                          <label for = 'birthplaceTextModal'>Birthplace</label>
                          <input type = 'text' id = 'birthplaceTextModal' name = 'birthplaceTextModal' class='form-control'
                          maxlength='100'>
                        </div-->
                        <div class='col-md-12'>
                          <label for = 'maritalStatusComboModal'>Marital Status</label>
                          <select id = 'maritalStatusComboModal' name = 'maritalStatusComboModal' class='form-control'>
                            <option value ='single'>Single</option>
                            <option value ='married'>Married</option>
                          </select>
                        </div>
                      </div>
                      <div class='row'>
                        <div class='col-md-12'>
                          <label for = 'contactNoTextModal'>Contact No:</label>
                          <input type = 'text' id = 'contactNoTextModal' name = 'contactNoTextModal' class='form-control'
                          maxlength='100' required>
                        </div>
                        <!--div class='col-md-6'>
                          <label for = 'telephoneNoTextModal'>Telephone No:</label>
                          <input type = 'text' id = 'telephoneNoTextModal' name = 'telephoneNoTextModal' class='form-control'
                          maxlength='100'>
                        </div-->
                      </div>
                      <div class='row'>
                        <div class='col-md-6'>
                          <input type = 'radio' id = 'femaleRadioButton' value ='female' name = 'genderRadioButton'>Female
                        </div>
                        <div class='col-md-6'>
                          <input type = 'radio' id = 'maleRadioButton' value ='male' name = 'genderRadioButton' checked>Male
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class='row'>
                <div class='col-md-12'>
                  <div class="x_panel">
                    <div class="x_title">
                      <h2>Address Information</h2>
                      <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                      </ul>
                      <div class="clearfix"></div>
                    </div>
                    <div class="x_content" style="display: block;">
                      <br>
                      <div class='row'>
                        <div class='col-md-6'>
                          <label for ='cityAddressTextModal'>City Address</label>
                          <input type = 'text' maxlength = '100' id = 'cityAddressTextModal'
                           name = 'cityAddressTextModal' class = 'form-control' required>
                        </div>
                        <div class='col-md-6'>
                          <label for = 'provincialAddressTextModal'>Provincial Address:</label>
                          <input type = 'text' maxlength = '100' id = 'provincialAddressTextModal'
                           name = 'provincialAddressTextModal' class = 'form-control' required>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      
    {!! Form::close() !!}
  @endslot
  @slot('modalbutton')
  <button class="btn btn-warning" form = 'studentForm'><i class='fa fa-save'></i>&nbsp;Save</button>
  @endslot
@endcomponent
