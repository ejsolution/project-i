<?php

namespace App\Http\Controllers\behaviour;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\behaviourSetting;
use App\model\behaviour;
class behaviourController extends Controller
{
    public function getBehaviourList(){
        $behavior = behaviourSetting::all();
        if(count($behavior)==0){
            return "<h2>No behaviours yet</h2>";
        }else{
            $html = '';
            foreach ($behavior as $behaviors) {
                $html .= "<tr id = 'behaviourRow-$behaviors->id'>
                            <td>$behaviors->name</td>
                            <td>$behaviors->description</td>
                            <td>
                                <button class='btn btn-warning editBehaviour' id = 'editBehaviour-$behaviors->id'>
                                    <i class='fa fa-edit'></i>
                                </button>
                            </td>
                            <td>
                                <button class='btn btn-danger deleteBehaviour' id = 'deleteBehaviour-$behaviors->id'>
                                    <i class='fa fa-trash-o'></i>
                                </button>
                            </td>
                        </tr>";
            }
            return $html;
        }
    }
    public function getBehaviourComboList(){
        $behavior = behaviourSetting::all();
        
            $html = '';
            foreach ($behavior as $behaviors) {
                $html .= "<option value = '$behaviors->id'>
                            $behaviors->name
                        </option>";
            }
            return $html;
        
    }
    public function insertBehaviour(Request $request){
        $this->validate($request, array(
            'name'=>'required|max:100|unique:behaviour_settings,name',
            'description'=>'required|max:500'
        ));
        $behaviour = new behaviourSetting;
        $behaviour->name = $request->name;
        $behaviour->description = $request->description;
        $behaviour->save();
        $std = new \stdClass;
        $html = "<tr id = 'behaviourRow-$behaviour->id'>
                        <td>$behaviour->name</td>
                        <td>$behaviour->description</td>
                        <td>
                            <button class='btn btn-warning editBehaviour' id = 'editBehaviour-$behaviour->id'>
                                <i class='fa fa-edit'></i>
                            </button>
                        </td>
                        <td>
                            <button class='btn btn-danger deleteBehaviour' id = 'deleteBehaviour-$behaviour->id'>
                                <i class='fa fa-trash-o'></i>
                            </button>
                        </td>
                    </tr>";
        $std->row =$html;
        $std->combo = "<option value = '$behaviour->id'>
                            $behaviour->name
                        </option>";
        return json_encode($std);
    }
    public function editBehaviour(Request $request){
        $this->validate($request, array(
            'name'=>'required|max:100',
            'description'=>'required|max:500'
        ));
        $behaviour = behaviourSetting::find($request->id);
        if($behaviour->name != $request->name){
            $this->validate($request, array(
                'name'=>'required|max:100|unique:behaviour_settings,name',
                'description'=>'required|max:500'
            ));
        }
        $behaviour->name = $request->name;
        $behaviour->description = $request->description;
        $behaviour->save();
        return "saved!";
        //return $request->description;
    }
    public function deleteBehaviour($behaviourid){
        $check =behaviour::where('behavioural', '=', $behaviourid)->get();
        if(count($check)>0){
            return "failed";
        }else{
            $behaviour  = behaviourSetting::find($behaviourid);
            $behaviour->forceDelete();
            return "true";
        }
    }
}
